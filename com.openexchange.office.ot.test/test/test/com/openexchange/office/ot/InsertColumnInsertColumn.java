/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertColumnInsertColumn {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2, 2], gridPosition: 3, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(3);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 2, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 2, 2, 2]);
             */
    }


    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 2, 2], gridPosition: 3, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 2, 2], gridPosition: 4, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 2, 2], gridPosition: 3, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 3, 3, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(4);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 3, 3, 2, 2]l);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3], gridPosition: 3, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 3], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 3], gridPosition: 4, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3], gridPosition: 3, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 3, 3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(4);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 3, 3, 3]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3], gridPosition: 3, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 6, 6], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 3], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 3], gridPosition: 4, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3], gridPosition: 3, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 6, 6], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 3, 3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(4);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 3, 3, 3]); // only use values from external grid
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5, 5], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5, 5], gridPosition: 6, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 3, 3, 1, 1, 5, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(6);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 3, 3, 1, 1, 5, 5]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5, 5], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5, 5], gridPosition: 6, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 3, 3, 1, 1, 5, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(6);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 3, 3, 1, 1, 5, 5]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5, 5], gridPosition: 6, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5, 5], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 3, 1, 1, 5], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(6);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 3, 3, 1, 1, 5, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 3, 3, 1, 1, 5, 5]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(5);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 3, 1, 1, 5, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 1]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 3, 1, 1, 5, 5], gridPosition: 5, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(5);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 3, 1, 1, 5, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 1]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [2, 2, 4, 4], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2, 2], gridPosition: 3, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [2, 2, 4, 4], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 2, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(3);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 2, 2, 2]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2, 2], gridPosition: 3, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2, 2], gridPosition: 2, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(3);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 2, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 2, 2, 2]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 1, 1, 1]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 6, 1, 2], tableGrid: [2, 2, 3, 3], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [2, 2, 2, 3], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [2, 2, 2, 3], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 6, 1, 2], tableGrid: [2, 2, 3, 3], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 6, 1, 2], tableGrid: [2, 2, 3, 3], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [2, 2, 2, 3], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(1);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([2, 2, 2, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 6, 1, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([2, 2, 3, 3]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 6, 1, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 1, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 1, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3, 6, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 6, 1, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 1, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(1);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 6, 2, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 6, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 6, 3, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 6, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(1);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 6, 3, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 0, 1, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 0, 1, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 0, 1, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 1, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3, 5, 3, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 3, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 2, 2]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 2, 3, 3, 3], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 2, 3, 3, 3], gridPosition: 3, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 2, 3, 3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 3, 0, 1]);
                expect(oneOperation.gridPosition).to.equal(3);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 3, 3]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 5, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 3, 5, 1]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 5, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 5, 1]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 3, 3], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 3, 3], gridPosition: 3, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 3], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 4, 2]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 2, 3, 3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 3, 4, 2]);
                expect(oneOperation.gridPosition).to.equal(3);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 3, 3]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 3, 4, 2], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 4, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 4, 1]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 3, 4, 2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 3, 1, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 1, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 3, 1, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 3, 1, 1]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 2, 0, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 2, 0, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 2, 0, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 3, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 2, 0, 1]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 2, 0, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1, 2, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [1, 2, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [2, 2, 0, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 2, 0, 1], tableGrid: [1, 2, 3, 4], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1, 2, 0, 1], tableGrid: [1, 1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 0, 1]);
                expect(localActions[0].operations[0].gridPosition).to.equal(2);
                expect(localActions[0].operations[0].tableGrid).to.deep.equal([1, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 2, 0, 1]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation.tableGrid).to.deep.equal([1, 2, 3, 4]);
             */
    }
}
