/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertParaSplitPara {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'splitParagraph', start: [3, 1] }",
            "{ name: 'splitParagraph', start: [4, 1] }",
            "{ name: 'insertTable', start: [3] }");
           /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'splitParagraph', start: [3, 1] }",
            "{ name: 'splitParagraph', start: [4, 1] }",
            "{ name: 'insertTable', start: [2] }");
           /*
                oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'splitParagraph', start: [3, 1] }",
            "{ name: 'splitParagraph', start: [3, 1] }",
            "{ name: 'insertTable', start: [5] }");
           /*
                oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 2, 2] }",
            "{ name: 'splitParagraph', start: [4, 1, 2, 2, 2] }",
            "{ name: 'insertTable', start: [3] }");
           /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'splitParagraph', start: [2, 1, 2, 2, 2] }",
            "{ name: 'splitParagraph', start: [2, 1, 2, 2, 2] }",
            "{ name: 'insertTable', start: [3] }");
           /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'splitParagraph', start: [4, 1, 2, 2, 2] }",
            "{ name: 'splitParagraph', start: [5, 1, 2, 2, 2] }",
            "{ name: 'insertTable', start: [3] }");
           /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [4, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4, 1, 2, 2] }",
            "{ name: 'splitParagraph', start: [4, 1, 2, 2, 2] }",
            "{ name: 'splitParagraph', start: [4, 1, 2, 3, 2] }",
            "{ name: 'insertTable', start: [4, 1, 2, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [4, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [4, 1, 2, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 3, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4, 1, 2, 2] }",
            "{ name: 'splitParagraph', start: [4, 1, 2, 4, 2] }",
            "{ name: 'splitParagraph', start: [4, 1, 2, 5, 2] }",
            "{ name: 'insertTable', start: [4, 1, 2, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [4, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [4, 1, 2, 4, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 5, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4, 1, 2, 2] }",
            "{ name: 'splitParagraph', start: [4, 1, 2, 1, 2] }",
            "{ name: 'splitParagraph', start: [4, 1, 2, 1, 2] }",
            "{ name: 'insertTable', start: [4, 1, 2, 3] }");
           /*
                oneOperation = { name: 'insertTable', start: [4, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [4, 1, 2, 1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 3]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4, 1, 2, 2] }",
            "{ name: 'splitParagraph', start: [4, 1, 1, 1, 2] }",
            "{ name: 'splitParagraph', start: [4, 1, 1, 1, 2] }",
            "{ name: 'insertTable', start: [4, 1, 2, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [4, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [4, 1, 1, 1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 1, 1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4, 1, 2, 2] }",
            "{ name: 'splitParagraph', start: [2, 1] }",
            "{ name: 'splitParagraph', start: [2, 1] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [4, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2, 2] }",
            "{ name: 'splitParagraph', start: [2, 1] }",
            "{ name: 'splitParagraph', start: [2, 1] }",
            "{ name: 'insertTable', start: [1, 1, 2, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 5, 1] }",
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'insertTable', start: [2, 3, 1] }");
           /*
                oneOperation = { name: 'insertTable', start: [1, 5, 1], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 3, 1]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 5, 1] }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'insertTable', start: [1, 5, 1] }");
           /*
                oneOperation = { name: 'insertTable', start: [1, 5, 1], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 5, 1]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 5, 1] }",
            "{ name: 'splitParagraph', start: [1, 5] }",
            "{ name: 'splitParagraph', start: [1, 5] }",
            "{ name: 'insertTable', start: [2, 0, 1] }");
           /*
                oneOperation = { name: 'insertTable', start: [1, 5, 1], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 0, 1]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'splitParagraph', start: [1, 2, 2, 3] }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 3] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', start: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1]);
             */
    }
}
