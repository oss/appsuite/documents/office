/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsInsertRows {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 11], attrs: { character: { italic: true } } }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 11]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], count: 2  }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 10, 4, 2], end: [1, 10, 4, 5], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 2], count: 2 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 2 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10, 4, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10, 4, 5]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [2, 1], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 22]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [2, 1], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 16]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 3], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 16]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 5], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 1], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 1], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 1, 2, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 1, 2, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1, 2, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 4], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 1, 2, 6]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 4], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 1, 2, 6]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 1], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 1], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 0], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [3, 1, 2, 3, 0], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 0], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 0]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 7], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [3, 1, 2, 3], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 7]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 4, 5], end: [3, 1, 2, 4, 9], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 7, 5], end: [3, 1, 2, 7, 9], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [3, 1, 2, 3], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4, 5], end:  [3, 1, 2, 4, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 7, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 2, 7, 9]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [3, 1, 2, 3], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [3, 1, 2, 3], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 4], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [2, 4], count: 3 }");                                                                          // expected external
            /*
                // local change inside one paragraph and several following paragraphs completely and an external insertRows in the middle of the paragraphs
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 4]); // external operation is not modified
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 3], count: 3 }");                                                                          // expected external
            /*
                // local change of several paragraphs completely and an external insertRows before the first of the paragraphs
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1, 3]); // external operation is not modified
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [3, 1], count: 3 }");                                                                          // expected external
            /*
                // local change of several paragraphs completely and an external insertRows in the last of the paragraphs
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([3]); // external operation is not modified
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1, 4, 5, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [2, 1, 4, 5, 2], count: 3 }");                                                                          // expected external
            /*
                // local change of several paragraphs and an external insertRows inside the table
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 4, 5, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 1, 4, 5, 2]); // external operation is not modified
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 5], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertRows', start: [1, 2], count: 3 }");                                                                          // expected external
            /*
                // local change of one complete paragraph and an external insertRows before this paragraph
                oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1, 2]); // external operation is not modified
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 2, 8], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 2, 11], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 11]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 8, 2], count: 2 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes',start: [1, 8, 6, 2], end: [1, 8, 6, 5], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 8, 2], count: 2 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 8, 2], count: 2 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 8, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 6, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 6, 5]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [2, 1, 1], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 22]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [2, 1, 1], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 16]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 3, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 3, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 16]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 2, 6], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 6]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 2, 2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 2, 5], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 5]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 2, 1], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 2, 1], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 1]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 1, 3], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 1, 3], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 3]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 2, 2, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 1, 2, 2, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2, 2, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    public void test44() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 4, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 4, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 1, 2, 6]);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 0, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 1, 2, 3, 0, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 0, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 0, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3, 0, 2], end: [3, 1, 2, 3, 0, 4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3, 0, 5], end: [3, 1, 2, 3, 0, 7], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 1, 2, 3, 0, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 0, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 0, 2], end: [3, 1, 2, 3, 0, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 0, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 2, 3, 0, 7]);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 0, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3, 0, 1], end: [3, 1, 2, 3, 0, 4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3 ,0, 1], end: [3, 1, 2, 3, 0, 7], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 1, 2, 3, 0, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 0, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 0, 1], end: [3, 1, 2, 3, 0, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 2, 3, 0, 7]);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2], count: 3 }");                                                                          // expected external
            /*
                    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
                    localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                    otManager.transformOperation(oneOperation, localActions);
                    expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
                    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3, 2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3, 5], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 5]);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3, 5], end: [3, 1, 2, 3, 9], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 3, 8], end: [3, 1, 2, 3, 12], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 5], end:  [3, 1, 2, 3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 8]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 2, 3, 12]);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
     public void test54() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 4, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [2, 4, 2], count: 3 }");                                                                          // expected external
            /*
              oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 4, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 4, 2]); // external operation is not modified
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 3, 3], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1, 3, 3]); // external operation is not modified
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [3, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([3, 2, 2]); // external operation is not modified
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 4, 2, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [2, 1, 4, 2, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 4, 2, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 1, 4, 2, 2, 2]); // external operation is not modified
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 2, 2], attrs: { character: { italic: true } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 2, 5], attrs: { character: { italic: true } } }",                                       // expected local
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");                                                                          // expected external
            /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
             */
    }
}

