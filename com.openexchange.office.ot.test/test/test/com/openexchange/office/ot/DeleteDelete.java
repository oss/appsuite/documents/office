/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class DeleteDelete {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",               // local operations
            "{ name: 'delete', start: [3, 7], end: [3, 7] }",               // external operations
            "{ name: 'delete', start: [3, 6], end: [3, 6] }",               // expected local
            "{ name: 'delete', start: [3, 4], end: [3, 4] }");              // expected external
            /*
                // the external delete is in front of the local delete in the same paragraph (no ranges)
                oneOperation = { name: 'delete', start: [3, 4], end: [3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [3, 7] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 6]); // modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 6]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 4]); // not modified
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",               // local operations
            "{ name: 'delete', start: [3, 7], end: [3, 9] }",               // external operations
            "{ name: 'delete', start: [3, 4], end: [3, 6] }",               // expected local
            "{ name: 'delete', start: [3, 2], end: [3, 4] }");              // expected external
            /*
                // the external delete is in front of the local delete in the same paragraph (with ranges)
                oneOperation = { name: 'delete', start: [3, 2], end: [3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [3, 9] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 6]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 4]); // not modified
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 6], end: [3, 6] }",               // local operations
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",               // external operations
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",               // expected local
            "{ name: 'delete', start: [3, 5], end: [3, 5] }");              // expected external
            /*
                // the external delete is behind the local delete in the same paragraph (no ranges)
                oneOperation = { name: 'delete', start: [3, 6], end: [3, 6] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 5]); // modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // modified
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 6], end: [3, 8] }",               // local operations
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",               // external operations
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",               // expected local
            "{ name: 'delete', start: [3, 3], end: [3, 5] }");              // expected external
            /*
                // the external delete is behind the local delete in the same paragraph (with ranges)
                oneOperation = { name: 'delete', start: [3, 6], end: [3, 8] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 3]); // modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // modified
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2, 6], end: [2, 8] }",               // local operations
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",               // external operations
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",               // expected local
            "{ name: 'delete', start: [2, 6], end: [2, 8] }");              // expected external
            /*
                // the external delete is in a paragraph before the local delete
                oneOperation = { name: 'delete', start: [2, 6], end: [2, 8] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 8]); // not modified
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'delete', start: [5, 6], end: [5, 8] }",               // local operations
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",               // external operations
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",               // expected local
            "{ name: 'delete', start: [5, 6], end: [5, 8] }");              // expected external
            /*
                // the external delete is in a paragraph behind the local delete
                oneOperation = { name: 'delete', start: [5, 6], end: [5, 8] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([5, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([5, 8]); // not modified
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'delete', start: [5, 6], end: [5, 8] }",               // local operations
            "{ name: 'delete', start: [1, 6], end: [3, 4] }",               // external operations
            "{ name: 'delete', start: [1, 6], end: [3, 4] }",               // expected local
            "{ name: 'delete', start: [4, 6], end: [4, 8] }");              // expected external
            /*
                // the external delete is in a paragraph behind the local delete over several paragraphs (without following merge)
                oneOperation = { name: 'delete', start: [5, 6], end: [5, 8] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 6]); // modified
                expect(oneOperation.end).to.deep.equal([4, 8]); // modified
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'delete', start: [5, 6], end: [5, 8] }",               // local operations
            "[{ name: 'delete', start: [1, 6], end: [3, 4] }, { name: 'mergeParagraph', start: [1], paralength:6 }]",   // external operations
            "[{ name: 'delete', start: [1, 6], end: [3, 4] }, { name: 'mergeParagraph', start: [1], paralength:6 }]",   // expected local
            "{ name: 'delete', start: [3, 6], end: [3, 8] }");              // expected external
            /*
                // the external delete is in a paragraph behind the local delete over several paragraphs (with following merge)
                oneOperation = { name: 'delete', start: [5, 6], end: [5, 8] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4] }, { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(2); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 6]); // modified
                expect(oneOperation.end).to.deep.equal([3, 8]); // modified
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'delete', start: [5, 6], end: [5, 8] }",               // local operations
            "{ name: 'delete', start: [1], end: [3] }",                     // external operations
            "{ name: 'delete', start: [1], end: [3] }",                     // expected local
            "{ name: 'delete', start: [2, 6], end: [2, 8] }");              // expected external
            /*
                // the external delete is in a paragraph behind the local delete over several complete paragraphs (without following merge)
                oneOperation = { name: 'delete', start: [5, 6], end: [5, 8] };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 8]); // not modified
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'delete', start: [5, 6], end: [5, 8] }",               // local operations
            "[{ name: 'delete', start: [1, 3], end: [3] }, { name: 'mergeParagraph', start: [1], paralength: 3 }]", // external operations
            "[{ name: 'delete', start: [1, 3], end: [3] }, { name: 'mergeParagraph', start: [1], paralength: 3 }]", // expected local
            "{ name: 'delete', start: [2, 6], end: [2, 8] }");              // expected external
            /*
                // the external delete is in a paragraph behind the local delete over several complete paragraphs (with following merge)
                oneOperation = { name: 'delete', start: [5, 6], end: [5, 8] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 3], end: [3] }, { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[1].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[1].paralength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(2); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 8]); // not modified
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 6], end: [5] }",                  // local operations
            "{ name: 'delete', start: [1, 6], end: [3, 4] }",               // external operations
            "{ name: 'delete', start: [1, 6], end: [3, 4] }",               // expected local
            "{ name: 'delete', start: [2, 1], end: [4] }");                 // expected external
            /*
                // the external delete is inside the last paragraph behind the local delete over several paragraphs (without following merge)
                oneOperation = { name: 'delete', start: [3, 6], end: [5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1]); // modified (5 characters inside the paragraph deleted)
                expect(oneOperation.end).to.deep.equal([4]); // modified
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 6], end: [5] }",               // local operations
            "[{ name: 'delete', start: [1, 6], end: [3, 4] }, { name: 'mergeParagraph', start: [1], paralength: 6 }]",  // external operations
            "[{ name: 'delete', start: [1, 6], end: [3, 4] }, { name: 'mergeParagraph', start: [1], paralength: 6 }]",  // expected local
            "{ name: 'delete', start: [1, 7], end: [3] }");              // expected external
            /*
                // the external delete is in a paragraph behind the local delete over several paragraphs (with following merge)
                oneOperation = { name: 'delete', start: [3, 6], end: [5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4] }, { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[1].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[1].paralength).to.equal(6); // not modified
                expect(localActions[0].operations.length).to.equal(2); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 7]); // modified (deleting the 8th character after merge)
                expect(oneOperation.end).to.deep.equal([3]); // modified
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2], end: [3, 2] }",               // local operations
            "{ name: 'delete', start: [3, 7], end: [6] }",               // external operations
            "{ name: 'delete', start: [2, 4], end: [5] }",               // expected local
            "{ name: 'delete', start: [2], end: [3, 2] }");              // expected external
            /*
                // the external delete ends in the first paragraph of the local delete (without following merge)
                oneOperation = { name: 'delete', start: [2], end: [3, 2] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [6] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4]); // modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 2]); // not modified
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2], end: [3] }",                 // local operations
            "{ name: 'delete', start: [4, 7], end: [6] }",              // external operations
            "{ name: 'delete', start: [2, 7], end: [4] }",              // expected local
            "{ name: 'delete', start: [2], end: [3] }");                // expected external
            /*
                // the external delete removes several paragraphs before the local delete
                oneOperation = { name: 'delete', start: [2], end: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7], end: [6] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 7]); // modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2], end: [3] }",                         // local operations
            "{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6] }", // external operations
            "{ name: 'delete', start: [2, 7, 2, 2, 1], end: [2, 7, 2, 2, 6] }", // expected local
            "{ name: 'delete', start: [2], end: [3] }");                        // expected external
            /*
                // the external delete removes several top level paragraphs before the local delete inside a table
                oneOperation = { name: 'delete', start: [2], end: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 7, 2, 2, 1]); // modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 7, 2, 2, 6]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'delete', start: [5], end: [6] }",                         // local operations
            "{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6] }", // external operations
            "{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6] }", // expected local
            "{ name: 'delete', start: [5], end: [6] }");                        // expected external
            /*
                // the external delete removes several top level paragraphs behind the local delete inside a table
                oneOperation = { name: 'delete', start: [5], end: [6] };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 2, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([5]); // not modified
                expect(oneOperation.end).to.deep.equal([6]); // not modified
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 6] }",   // local operations
            "{ name: 'delete', start: [5], end: [6] }",                     // external operations
            "{ name: 'delete', start: [5], end: [6] }",                     // expected local
            "{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 6] }");  // expected external
            /*
                // the external delete removes several paragraphs inside a table in front of the local deleted top level paragraphs
                oneOperation = { name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 6] };
                localActions = [{ operations: [{ name: 'delete', start: [5], end: [6] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 6]); // not modified
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] }",       // local operations
            "{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8] }", // external operations
            "{ name: 'delete', start: [4, 7, 2, 2, 2], end: [4, 7, 2, 2, 8] }", // expected local
            "{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] }");      // expected external
            /*
                // the external delete removes several paragraphs inside a table in front of the local deleted paragraphs in the same table cell
                oneOperation = { name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 2, 2]); // modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 2, 8]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 3]); // not modified
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] }",       // local operations
            "{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8] }", // external operations
            "{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8] }", // expected local
            "{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] }");      // expected external
            /*
                // the external delete removes several paragraphs inside a table in front of the local deleted paragraphs in another cell
                oneOperation = { name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 3, 4, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 3, 4, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 3]); // not modified
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] }",   // local operations
            "{ name: 'delete', start: [4, 2], end: [4, 3] }",               // external operations
            "{ name: 'delete', start: [4, 2], end: [4, 3] }",               // expected local
            "{ name: 'delete', start: [4, 5, 2, 2], end: [4, 5, 2, 3] }");  // expected external
            /*
                oneOperation = { name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [4, 2], end: [4, 3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([4, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 5, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 5, 2, 3]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }",   // local operations
            "{ name: 'delete', start: [4, 2], end: [4, 3] }",               // external operations
            "{ name: 'delete', start: [4, 2], end: [4, 3] }",               // expected local
            "{ name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }");  // expected external
            /*
                oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [4, 2], end: [4, 3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([4, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 1, 2, 3]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }",   // local operastions
            "{ name: 'delete', start: [4, 1], end: [4, 3] }",               // external operations
            "{ name: 'delete', start: [4, 1], end: [4, 3] }",               // expected local
            "[]");                                                          // expected external
            /*
                oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [4, 1], end: [4, 3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([4, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 1, 2, 3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }",   // local operations
            "{ name: 'delete', start: [3] }",                               // external operations
            "{ name: 'delete', start: [3] }",                               // expected local
            "{ name: 'delete', start: [3, 1, 2, 2], end: [3, 1, 2, 3] }");  // expected external
            /*
                oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }",   // local operations
            "{ name: 'delete', start: [3], end: [4] }",                     // external operations
            "{ name: 'delete', start: [3], end: [4] }",                     // expected local
            "[]");                                                          // expected external
            /*
                oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [3], end: [4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 1, 2, 3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }",   // local operations
            "{ name: 'delete', start: [3, 1], end: [3, 3] }",               // external operations
            "{ name: 'delete', start: [3, 1], end: [3, 3] }",               // expected local
            "{ name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }");  // expected external
            /*
                oneOperation = { name: 'delete', start: [4, 1, 2, 2], end: [4, 1, 2, 3] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 1, 2, 3]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",               // local operations
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",               // external operations
            "[]",                                                           // expected local
            "[]");                                                          // expected external
            /*
                // delete and local delete with identical delete ranges (handleDeleteDelete)
                it('should calculate valid transformed delete operation after local delete operation in identical delete ranges', function () {

                    oneOperation = { name: 'delete', start: [3, 4], end: [3, 4] };
                    localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4] }] }];
                    otManager.transformOperation(oneOperation, localActions);
                    expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
                    expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4], end: [4] }",                     // local operations
            "{ name: 'delete', start: [4], end: [4] }",                     // external operations
            "[]",                                                           // expected local
            "[]");                                                          // expected external
            /*
                oneOperation = { name: 'delete', start: [4], end: [4] };
                localActions = [{ operations: [{ name: 'delete', start: [4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4] }", // local operations
            "{ name: 'delete', start: [4, 3, 2, 2, 4] }",                       // external operations
            "[]",                                                               // expected local
            "[]");                                                              // expected external
            /*
                oneOperation = { name: 'delete', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [4, 3, 2, 2, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4, 3, 2, 2] }",                      // local operations
            "{ name: 'delete', start: [4, 3, 2, 2] }",                      // external operations
            "[]",                                                           // expected local
            "[]");                                                          // expected external
            /*
                oneOperation = { name: 'delete', start: [4, 3, 2, 2] };
                localActions = [{ operations: [{ name: 'delete', start: [4, 3, 2, 2] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",               // local operations
            "{ name: 'delete', start: [3, 3], end: [3, 6] }",               // external operations
            "{ name: 'delete', start: [3, 3], end: [3, 5] }",               // expected local
            "[]");                                                          // expected external
            /*
                // delete and local delete in surrounding delete ranges (handleDeleteDelete)
                it('should calculate valid transformed delete operation after local delete operation in surrounding delete ranges', function () {

                    // the external delete is surrounded by the local delete in the same paragraph
                    oneOperation = { name: 'delete', start: [3, 4], end: [3, 4] };
                    localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 6] }] }];
                    otManager.transformOperation(oneOperation, localActions);
                    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // modified
                    expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // modified
                    expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                    expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external delete operation has the marker for ignoring
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 5], end: [3, 8] }",               // local operations
            "{ name: 'delete', start: [3, 5], end: [3, 7] }",               // external operations
            "[]",                                                           // expected local
            "{ name: 'delete', start: [3, 5], end: [3, 5] }");              // expected external
            /*
                // the external delete surrounds the local delete in the same paragraph
                oneOperation = { name: 'delete', start: [3, 5], end: [3, 8] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 5], end: [3, 7] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 5]); // modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3] }",                               // local operations
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",               // external operations
            "[]",                                                           // expected local
            "{ name: 'delete', start: [3] }");                              // expected external
            /*
                // the external delete surrounds the local delete because it is an ancestor node
                oneOperation = { name: 'delete', start: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.equal(undefined); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // local operation got marker for ignoring
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3], end: [3] }",                     // local operations
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",               // external operations
            "[]",                                                           // expected local
            "{ name: 'delete', start: [3] }");                              // expected external
            /*
                // the external delete surrounds the local delete because it is an ancestor node
                oneOperation = { name: 'delete', start: [3], end: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // local operation got marker for ignoring
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2], end: [3] }",                     // local operations
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",               // external operations
            "[]",                                                           // expected local
            "{ name: 'delete', start: [2], end: [3] }");                    // expected external
            /*
                // the external delete surrounds the local delete because its final paragraph is an ancestor node
                oneOperation = { name: 'delete', start: [2], end: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // local operation got marker for ignoring
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2], end: [3] }",                         // local operations
            "{ name: 'delete', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6] }", // external operations
            "[]",                                                               // expected local
            "{ name: 'delete', start: [2], end: [3] }");                        // expected external
            /*
                // the external delete surrounds the local delete because its final table is an ancestor node
                oneOperation = { name: 'delete', start: [2], end: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // local operation got marker for ignoring
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",               // local operations
            "{ name: 'delete', start: [2], end: [5] }",                     // external operations
            "{ name: 'delete', start: [2], end: [5] }",                     // expected local
            "[]");                                                          // expected external
            /*
                // the external delete is surrounded by the local delete because several paragraphs are removed
                oneOperation = { name: 'delete', start: [3, 4], end: [3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [5] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external delete operation has the marker for ignoring
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3], end: [4] }",                     // local operations
            "{ name: 'delete', start: [2], end: [5] }",                     // external operations
            "{ name: 'delete', start: [2], end: [3] }",                     // expected local
            "[]");                                                          // expected external
            /*
                // the external delete of paragraphs is surrounded by the local delete because several paragraphs are removed
                oneOperation = { name: 'delete', start: [3], end: [4] };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [5] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external delete operation has the marker for ignoring
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3] }",                               // local operations
            "{ name: 'delete', start: [3, 5, 2, 1], end: [3, 5, 2, 4] }",   // external operations
            "[]",                                                           // expected local
            "{ name: 'delete', start: [3] }");                              // expected external
            /*
                // the external delete surrounds the local delete because that is an ancestor table
                oneOperation = { name: 'delete', start: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 5, 2, 1], end: [3, 5, 2, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // operation got marker for ignoring
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.equal(undefined); // not modified
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 4], end: [3, 6] }",               // local operations
            "{ name: 'delete', start: [3, 3], end: [3, 5] }",               // external operations
            "{ name: 'delete', start: [3, 3], end: [3, 3] }",               // expected local
            "{ name: 'delete', start: [3, 3], end: [3, 3] }");              // expected external
            /*
                // delete and local delete in overlapping delete ranges (handleDeleteDelete)
                it('should calculate valid transformed delete operation after local delete operation in overlapping delete ranges', function () {

                    // the two delete ranges overlap each other inside the same paragraph (local operation before external operation)
                    oneOperation = { name: 'delete', start: [3, 4], end: [3, 6] };
                    localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 5] }] }];
                    otManager.transformOperation(oneOperation, localActions);
                    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]); // modified
                    expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                    expect(oneOperation.start).to.deep.equal([3, 3]); // modified, only one character must be removed by the external operation
                    expect(oneOperation.end).to.deep.equal([3, 3]); // modified
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 4], end: [3, 6] }",               // local operations
            "{ name: 'delete', start: [3, 1], end: [3, 5] }",               // external operations
            "{ name: 'delete', start: [3, 1], end: [3, 3] }",               // expected local
            "{ name: 'delete', start: [3, 1], end: [3, 1] }");              // expected external
            /*
                // the two delete ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'delete', start: [3, 4], end: [3, 6] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 5] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 3]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 1]); // modified, only one character must be removed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 1]); // modified
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",               // local operations
            "{ name: 'delete', start: [0, 1], end: [0, 5] }",               // external operations
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",               // expected local
            "{ name: 'delete', start: [0, 1], end: [0, 3] }");              // expected external
            /*
                // the two delete ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'delete', start: [0, 3], end: [0, 8] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 5] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([0, 2]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([0, 1]); // modified, only three characters must be removed by the external operation
                expect(oneOperation.end).to.deep.equal([0, 3]); // modified
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4], end: [9] }",                     // local operations
            "{ name: 'delete', start: [1], end: [4] }",                     // external operations
            "{ name: 'delete', start: [1], end: [3] }",                     // expected local
            "{ name: 'delete', start: [1], end: [5] }");                    // expected external
            /*
                // the two delete ranges overlap on paragraph level (local operation before external operation)
                oneOperation = { name: 'delete', start: [4], end: [9] };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // modified, 5 instead of 6 paragraphs must be removed by the external operation
                expect(oneOperation.end).to.deep.equal([5]); // modified
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 2], end: [3, 5] }",               // local operations
            "{ name: 'delete', start: [3, 3], end: [3, 6] }",               // external operations
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",               // expected local
            "{ name: 'delete', start: [3, 2], end: [3, 2] }");              // expected external
            /*
                // the two delete ranges overlap each other inside the same paragraph (local operation after external operation)
                oneOperation = { name: 'delete', start: [3, 2], end: [3, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 6] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 2]); // modified, only one character must be removed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 2]); // modified
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1], end: [3, 5] }",               // local operations
            "{ name: 'delete', start: [3, 4], end: [3, 10] }",              // external operations
            "{ name: 'delete', start: [3, 1], end: [3, 5] }",              // expected local
            "{ name: 'delete', start: [3, 1], end: [3, 3] }");              // expected external
            /*
                // the two delete ranges overlap each other inside the same paragraph (local operation after external operation)
                oneOperation = { name: 'delete', start: [3, 1], end: [3, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 1]); // modified, only three characters must be removed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 3]); // modified
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1], end: [4] }",                     // local operations
            "{ name: 'delete', start: [3], end: [8] }",                     // external operations
            "{ name: 'delete', start: [1], end: [4] }",                     // expected local
            "{ name: 'delete', start: [1], end: [2] }");                    // expected external
            /*
                // the two delete ranges overlap on paragraph level (local operation after external operation)
                oneOperation = { name: 'delete', start: [1], end: [4] };
                localActions = [{ operations: [{ name: 'delete', start: [3], end: [8] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // modified, 2 instead of 4 paragraphs must be removed by the external operation
                expect(oneOperation.end).to.deep.equal([2]); // modified
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'delete', start: [0, 2], end: [0, 5] }",               // local operations
            "{ name: 'delete', start: [0, 4], end: [0, 8] }",               // external operations
            "{ name: 'delete', start: [0, 2], end: [0, 4] }",               // expected local
            "{ name: 'delete', start: [0, 2], end: [0, 3] }");              // expected external
            /*
                    // the two delete ranges overlap each other inside the same paragraph (local operation after external operation)
                    oneOperation = { name: 'delete', start: [0, 2], end: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [0, 8] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 4]);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([0, 2]); // modified, only two characters must be removed by the external operation
                expect(oneOperation.end).to.deep.equal([0, 3]); // modified
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 3], end: [3, 5] }",               // local operations
            "{ name: 'delete', start: [3, 4], end: [3, 6] }",               // external operations
            "{ name: 'delete', start: [3, 3], end: [3, 3] }",               // expected local
            "{ name: 'delete', start: [3, 3], end: [3, 3] }");              // expected external
            /*
                // the two delete ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'delete', start: [3, 3], end: [3, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 3]); // modified, only one character must be removed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 3]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }
}
