/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class UngroupGeneric {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 4, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'splitParagraph', start: [3, 6, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 6, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 4, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'splitParagraph', start: [3, 9, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 9, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 2, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'splitParagraph', start: [3, 4, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 2, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 4, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 4, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8] }",
            "{ name: 'splitParagraph', start: [3, 9, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 9, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 4, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9] }",
            "{ name: 'splitParagraph', start: [3, 6, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 6, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 2, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9] }",
            "{ name: 'splitParagraph', start: [3, 4, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 2, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 4, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 2, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 7, 8, 9] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 7, 8, 9] }",
            "{ name: 'splitParagraph', start: [3, 2, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 2, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 7, 8, 9], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 7, 8, 9], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 2, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [2, 4, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'splitParagraph', start: [2, 4, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [2, 4, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [2, 4, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [4, 4, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'splitParagraph', start: [4, 4, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [4, 4, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [4, 4, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 4, 0, 0], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'splitParagraph', start: [3, 4, 0, 0], target: '123456' }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 0], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 4, 0, 0], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 4, 0, 0], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456' }",
            "{ name: 'splitParagraph', start: [3, 9, 0, 0], target: '123456' }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 4, 0, 0], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 9, 0, 0], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 0, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'splitParagraph', start: [3, 0, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 0, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 0, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 1, 3, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4] }",
            "{ name: 'splitParagraph', start: [3, 4, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 1, 3, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 4, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 1, 2, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4] }",
            "{ name: 'splitParagraph', start: [3, 4, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 1, 2, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 4, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 1, 1, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4] }",
            "{ name: 'splitParagraph', start: [3, 4, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 1, 1, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 4, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 1, 0, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'splitParagraph', start: [3, 1, 0, 0] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 1, 0, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 1, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'splitParagraph', start: [3, 1, 0, 2, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 2] }");
            /*
    oneOperation = { name: 'splitParagraph', start: [3, 1, 0, 2, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'splitParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'splitParagraph', start: [3, 0, 2, 2] }",
            "{ name: 'splitParagraph', start: [3, 0, 2, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 0, 2, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 0, 2, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'splitParagraph', start: [3, 4, 2, 2] }",
            "{ name: 'splitParagraph', start: [3, 6, 2, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 4, 2, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 6, 2, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'splitParagraph', start: [3, 3, 2, 2] }",
            "{ name: 'splitParagraph', start: [3, 5, 2, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 3, 2, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 5, 2, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }",
            "{ name: 'splitParagraph', start: [3, 3, 2, 2] }",
            "{ name: 'splitParagraph', start: [3, 4, 2, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 3, 2, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 4, 2, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }",
            "{ name: 'splitParagraph', start: [3, 1, 0, 2, 2] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 0, 2, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }",
            "{ name: 'splitParagraph', start: [3, 1, 1, 2, 2] }",
            "{ name: 'splitParagraph', start: [3, 3, 2, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 1, 2, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 3, 2, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 4, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'mergeParagraph', start: [3, 6, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 6, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 4, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'mergeParagraph', start: [3, 9, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 9, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 2, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 2, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [2, 4, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'mergeParagraph', start: [2, 4, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [2, 4, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [2, 4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [4, 4, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'mergeParagraph', start: [4, 4, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [4, 4, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [4, 4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 4, 0], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0], target: '123456' }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 4, 0], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 4, 0], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456' }",
            "{ name: 'mergeParagraph', start: [3, 9, 0], target: '123456' }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6, 7], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 9, 0], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'mergeParagraph', start: [3, 0, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 1, 3, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 1, 2, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 2, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 1, 1, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 1, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 1, 0, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'mergeParagraph', start: [3, 1, 0] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 0, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 1, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 1, 0, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2] }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 0, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'mergeParagraph', start: [3, 1, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'mergeParagraph', start: [3, 0, 2] }",
            "{ name: 'mergeParagraph', start: [3, 0, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 0, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'mergeParagraph', start: [3, 0, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'mergeParagraph', start: [3, 4, 2] }",
            "{ name: 'mergeParagraph', start: [3, 6, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 4, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'mergeParagraph', start: [3, 6, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8] }",
            "{ name: 'mergeParagraph', start: [3, 4, 2] }",
            "{ name: 'mergeParagraph', start: [3, 9, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 4, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'mergeParagraph', start: [3, 9, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9] }",
            "{ name: 'mergeParagraph', start: [3, 4, 2] }",
            "{ name: 'mergeParagraph', start: [3, 6, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 4, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'mergeParagraph', start: [3, 6, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9] }",
            "{ name: 'mergeParagraph', start: [3, 2, 2] }",
            "{ name: 'mergeParagraph', start: [3, 4, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 2, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'mergeParagraph', start: [3, 4, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8, 9], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'mergeParagraph', start: [3, 3, 2] }",
            "{ name: 'mergeParagraph', start: [3, 5, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'mergeParagraph', start: [3, 5, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }",
            "{ name: 'mergeParagraph', start: [3, 1, 1, 2] }",
            "{ name: 'mergeParagraph', start: [3, 3, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 1, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }",
            "{ name: 'mergeParagraph', start: [3, 1, 0, 2] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 0, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }
}
