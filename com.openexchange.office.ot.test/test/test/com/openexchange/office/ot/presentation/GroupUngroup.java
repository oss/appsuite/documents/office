/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class GroupUngroup {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [2, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [2, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'group', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [4, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [4, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'group', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 2], drawings: [2, 3, 4] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 2], drawings: [2, 3, 4], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'ungroup', start: [3, 2], drawings: [2, 3, 4] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 2], drawings: [2, 3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'ungroup', start: [3, 4], drawings: [4, 6, 8] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 6, 8], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 7] }",
            "{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 7], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 2], drawings: [2, 3, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 2], drawings: [2, 3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 2], drawings: [2, 4, 6] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 6, 8] }");
            /*
    oneOperation = { name: 'group', start: [3, 2], drawings: [2, 4, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 6, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 2], drawings: [2, 4, 6] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'group', start: [3, 2], drawings: [2, 4, 6], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 7] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 6], drawings: [6, 7, 8] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 6], drawings: [6, 7, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 7] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 7], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456' }");
            /*
    oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '234567' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '234567' }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456' }");
            /*
    oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '234567', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456' }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [4, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [4, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [4, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 4, 5] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 2], drawings: [2, 3, 4] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 2], drawings: [2, 3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 6], drawings: [6, 7, 8] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 6], drawings: [6, 7, 8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 4] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 6, 8] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 4], drawings: [4, 6, 8], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 7] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 7], opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 2], drawings: [2, 3, 4] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 2], drawings: [2, 3, 4], opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 2], drawings: [2, 4, 6] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 2], drawings: [2, 4, 6], opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 2], drawings: [2, 4, 6] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 4] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 2], drawings: [2, 4, 6], opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 2], drawings: [2, 3, 4] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 2], drawings: [2, 3, 4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 4] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 4], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 2, 7] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 1], drawings: [1, 2, 7], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "{ name: 'group', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'group', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456' }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "{ name: 'group', start: [3, 1], drawings: [1, 4, 5], target: '234567' }",
            "{ name: 'group', start: [3, 1], drawings: [1, 4, 5], target: '234567' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456' }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4, 5], target: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 1], drawings: [1, 4, 5], target: '234567', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "{ name: 'group', start: [3, 1], drawings: [1, 4, 5], target: '123456' }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
                localOperation = { name: 'group', start: [3, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }
}
