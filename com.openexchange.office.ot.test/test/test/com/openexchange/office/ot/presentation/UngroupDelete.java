/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class UngroupDelete {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5] }",
            "{ name: 'delete', start: [3, 8, 0, 0], end: [3, 8, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 8, 0, 0], end: [3, 8, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5] }",
            "{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 1, 3, 0, 0], end: [3, 1, 3, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5] }",
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 1, 3, 0, 0], end: [3, 1, 3, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 1, 1, 0, 0], end: [3, 1, 1, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 1, 1, 0, 0], end: [3, 1, 1, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'delete', start: [3, 1, 0, 0], end: [3, 1, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 1, 0, 0], end: [3, 1, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456' }",
            "{ name: 'delete', start: [3, 1, 0, 0], end: [3, 1, 0, 5], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 1, 0, 0], end: [3, 1, 0, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6] }",
            "{ name: 'delete', start: [3, 8, 0, 0], end: [3, 8, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 8, 0, 0], end: [3, 8, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8] }",
            "{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [2] }");
            /*
    oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [4] }");
            /*
    oneOperation = { name: 'delete', start: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "[]",
            "{ name: 'delete', start: [3] }");
            /*
    oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567' }",
            "{ name: 'delete', start: [3], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "[]",
            "{ name: 'delete', start: [3], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }


    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 1] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [2, 1] }");
            /*
    oneOperation = { name: 'delete', start: [2, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 1], end: [2, 1] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [2, 1], end: [2, 1] }");
            /*
    oneOperation = { name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [4, 1] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [4, 1] }");
            /*
    oneOperation = { name: 'delete', start: [4, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [4, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 0], end: [3, 0] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 0], drawings: [0, 1, 2] }",
            "{ name: 'delete', start: [3, 0], end: [3, 0] }");
            /*
    oneOperation = { name: 'delete', start: [3, 0], end: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 0], drawings: [0, 1, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 0], end: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3, 6], end: [3, 6] }");
            /*
    oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 6], end: [3, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6] }",
            "{ name: 'delete', start: [3, 8], end: [3, 8] }");
            /*
    oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 8], end: [3, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7] }",
            "{ name: 'delete', start: [3, 6], end: [3, 6] }");
            /*
    oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 6], end: [3, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7] }",
            "{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], target: '123456' }",
            "{ name: 'delete', start: [3, 8], end: [3, 8], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 8], end: [3, 8], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 1], end: [3, 1] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'delete', start: [3, 1], end: [3, 1], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 1] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'delete', start: [3, 1], opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 };
                localOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }",
            "{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5] }",
            "{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [4, 4, 0, 0], end: [4, 4, 0, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5] }",
            "{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [2, 4, 0, 0], end: [2, 4, 0, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5] }",
            "{ name: 'delete', start: [3, 1, 3, 0, 0], end: [3, 1, 3, 0, 5] }",
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 1, 3, 0, 0], end: [3, 1, 3, 0, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'delete', start: [3, 1, 1, 0, 0], end: [3, 1, 1, 0, 5] }",
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 0, 0], end: [3, 1, 1, 0, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5] }",
            "{ name: 'delete', start: [3, 1, 0, 0], end: [3, 1, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 1, 0, 0, 0], end: [3, 1, 0, 0, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 1, 0, 0], end: [3, 1, 0, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }",
            "{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5] }",
            "{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 0, 0, 0], end: [3, 0, 0, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8] }",
            "{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5] }",
            "{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 4, 0, 0], end: [3, 4, 0, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 6, 0, 0], end: [3, 6, 0, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }


    @Test
    public void test41() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [2, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3] }",
            "{ name: 'delete', start: [3] }",
            "[]");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3], target: '123456' }",
            "{ name: 'delete', start: [3], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567' }",
            "{ name: 'delete', start: [3], target: '123456' }",
            "{ name: 'delete', start: [3], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567' }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '234567', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "{ name: 'delete', start: [3], target: '123456' }",
            "{ name: 'delete', start: [3], target: '123456' }",
            "[]");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }


    @Test
    public void test47() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [2, 1] }",
            "{ name: 'delete', start: [2, 1] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [2, 1], end: [2, 1] }",
            "{ name: 'delete', start: [2, 1], end: [2, 1] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [4, 1] }",
            "{ name: 'delete', start: [4, 1] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [4, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [4, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3, 0], end: [3, 0] }",
            "{ name: 'delete', start: [3, 0], end: [3, 0] }",
            "{ name: 'ungroup', start: [3, 0], drawings: [0, 1, 2] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 0], drawings: [0, 1, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'delete', start: [3, 6], end: [3, 6] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 6], end: [3, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7] }",
            "{ name: 'delete', start: [3, 4], end: [3, 45] }",
            "{ name: 'delete', start: [3, 8], end: [3, 8] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 8], end: [3, 8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8] }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'delete', start: [3, 6], end: [3, 6] }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 7, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 6], end: [3, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 6, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7] }",
            "{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456' }",
            "{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], target: '123456' }",
            "{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456' }",
            "{ name: 'delete', start: [3, 8], end: [3, 8], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], target: '123456' }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 5, 7], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 8], end: [3, 8], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3, 4, 6], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }


    @Test
    public void test56() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3, 1], end: [3, 1] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                localOperation = { name: 'delete', start: [3, 1], end: [3, 1], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3, 1] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
                localOperation = { name: 'delete', start: [3, 1], opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }",
            "{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456' }",
            "{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456' }",
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3] }");
            /*
    oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456' }",
            "{ name: 'delete', start: [3, 1], end: [3, 1], target: '123456' }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
                oneOperation = { name: 'ungroup', start: [3, 1], drawings: [1, 2, 3], target: '123456', opl: 1, osn: 1 };
                localOperation = { name: 'delete', start: [3, 1], end: [3, 1], target: '123456', opl: 1, osn: 1 };
                testRunner.expectBidiError(oneOperation, localOperation);
             */
    }
}
