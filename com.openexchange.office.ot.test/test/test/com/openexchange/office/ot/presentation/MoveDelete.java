/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveDelete {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3, 2, 0, 1], end: [3, 2, 0, 6] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 0] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 0] }",
            "{ name: 'delete', start: [3, 3, 0, 1], end: [3, 3, 0, 6] }");
            /*
    oneOperation = { name: 'delete', start: [3, 2, 0, 1], end: [3, 2, 0, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 0, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 3, 0, 6]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 2], end: [2, 2] }",
            "{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 7] }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 6] }",
            "{ name: 'delete', start: [2, 2], end: [2, 2] }");
            /*
    oneOperation = { name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 6]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 2]);
    expect(transformedOps[0].end).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 2], end: [2, 2] }",
            "{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 0] }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 0] }",
            "{ name: 'delete', start: [2, 3], end: [2, 3] }");
            /*
    oneOperation = { name: 'delete', start: [2, 2], end: [2, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3]);
    expect(transformedOps[0].end).to.deep.equal([2, 3]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 5], end: [2, 5] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 0] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 0] }",
            "{ name: 'delete', start: [2, 5], end: [2, 5] }");
            /*
    oneOperation = { name: 'delete', start: [2, 5], end: [2, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 5]);
    expect(transformedOps[0].end).to.deep.equal([2, 5]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 1], end: [2, 1] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1] }",
            "{ name: 'delete', start: [2, 2], end: [2, 2] }");
            /*
    oneOperation = { name: 'delete', start: [2, 1], end: [2, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 2]);
    expect(transformedOps[0].end).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 4], end: [2, 4] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "[]",
            "{ name: 'delete', start: [2, 1], end: [2, 1] }");
            /*
    oneOperation = { name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 1]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [1] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'move', start: [1, 4], end: [1, 4], to: [1, 1] }",
            "{ name: 'delete', start: [1] }");
            /*
    oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "[]",
            "{ name: 'delete', start: [2] }");
            /*
    oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], target: '123456' }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], target: '123456' }",
            "{ name: 'delete', start: [2] }");
            /*
    oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [0], target: '123456' }",
            "{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 1], target: '234567' }",
            "{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 1], target: '234567' }",
            "{ name: 'delete', start: [0], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [0], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 1], target: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([0, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([0, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([0, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [0], target: '123456' }",
            "{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 1], target: '123456' }",
            "[]",
            "{ name: 'delete', start: [0], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [0], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [0, 4], end: [0, 4], to: [0, 1], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([0, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([0, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([0, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [3] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'delete', start: [3] }");
            /*
    oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 4, 1], end: [2, 4, 3] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'delete', start: [2, 1, 1], end: [2, 1, 3] }");
            /*
    oneOperation = { name: 'delete', start: [2, 4, 1], end: [2, 4, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 1, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 1, 3]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'delete', start: [2, 4, 1], end: [2, 4, 3] }");
            /*
    oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 4, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 4, 3]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 5] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 5] }",
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }");
            /*
    oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 3, 3]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 0] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 0] }",
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }");
            /*
    oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 3, 3]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5] }",
            "{ name: 'delete', start: [2, 2, 1], end: [2, 2, 3] }");
            /*
    oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 2, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 2, 3]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }");
            /*
    oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 3, 3]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }",
            "{ name: 'move', start: [1, 2], end: [1, 2], to: [1, 5] }",
            "{ name: 'move', start: [1, 2], end: [1, 2], to: [1, 5] }",
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3] }");
            /*
    oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 2], end: [1, 2], to: [1, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 5]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 3, 3]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5] }",
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 3, 3]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], target: '234567' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], target: '234567' }",
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], target: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 3, 3]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], target: '123456' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], target: '123456' }",
            "{ name: 'delete', start: [2, 2, 1], end: [2, 2, 3], target: '123456' }");
            /*
    oneOperation = { name: 'delete', start: [2, 3, 1], end: [2, 3, 3], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 5], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 2, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 2, 3]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 1] }",
            "{ name: 'delete', start: [2, 4], end: [2, 4] }",
            "{ name: 'delete', start: [2, 5], end: [2, 5] }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 1] }");
            /*
    oneOperation = { name: 'move', start: [2, 6], end: [2, 6], to: [2, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 5]);
    expect(transformedOps[0].end).to.deep.equal([2, 5]);
    expect(transformedOps[0].to).to.deep.equal([2, 1]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'delete', start: [2, 4], end: [2, 4] }",
            "{ name: 'delete', start: [2, 1], end: [2, 1] }",
            "[]");
            /*
    oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2, 4], end: [2, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 1] }",
            "{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3] }",
            "{ name: 'delete', start: [2, 5, 1, 1], end: [2, 5, 1, 3] }",
            "{ name: 'move', start: [2, 6], end: [2, 6], to: [2, 1] }");
            /*
    oneOperation = { name: 'move', start: [2, 6], end: [2, 6], to: [2, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 5, 1, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 6]);
    expect(transformedOps[0].end).to.deep.equal([2, 6]);
    expect(transformedOps[0].to).to.deep.equal([2, 1]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1] }",
            "{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3] }",
            "{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3] }",
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1] }");
            /*
    oneOperation = { name: 'move', start: [2, 3], end: [2, 3], to: [2, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 1, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3]);
    expect(transformedOps[0].end).to.deep.equal([2, 3]);
    expect(transformedOps[0].to).to.deep.equal([2, 1]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 6] }",
            "{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3] }",
            "{ name: 'delete', start: [2, 3, 1, 1], end: [2, 3, 1, 3] }",
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 6] }");
            /*
    oneOperation = { name: 'move', start: [2, 3], end: [2, 3], to: [2, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 3, 1, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3]);
    expect(transformedOps[0].end).to.deep.equal([2, 3]);
    expect(transformedOps[0].to).to.deep.equal([2, 6]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3] }",
            "{ name: 'delete', start: [2, 1, 1, 1], end: [2, 1, 1, 3] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }");
            /*
    oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [2, 4, 1, 1], end: [2, 4, 1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 4]);
    expect(transformedOps[0].end).to.deep.equal([2, 4]);
    expect(transformedOps[0].to).to.deep.equal([2, 1]);
             */
    }
}
