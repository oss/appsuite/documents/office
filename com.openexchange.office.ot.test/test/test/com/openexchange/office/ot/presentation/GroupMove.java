/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class GroupMove {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 3] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 3] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 4] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 4] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 4] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 6, 7] }",
            "{ name: 'move', start: [3, 2], to: [3, 4] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 6, 7] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 7] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 7] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 8] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 9] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 6] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 9], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 6, 8, 9] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 8, 9] }",
            "{ name: 'move', start: [3, 2], to: [3, 6] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 6, 8, 9], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 5, 8, 9], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 8] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 2], to: [3, 6] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 8] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 8] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 7] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 7] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 6], target: '123456' }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 6], target: '123456' }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], to: [3, 6], target: '123456' }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6], target: '123456' }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456' }",
            "{ name: 'move', start: [3, 2], to: [3, 5], target: '123456' }");
            /*
    oneOperation = { name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 4], to: [3, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 4], to: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 7], to: [3, 2] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 5], to: [3, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 7], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 8], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 7] }",
            "{ name: 'move', start: [3, 6], to: [3, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 5], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 7, 8] }",
            "[{ name: 'move', start: [3, 6], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 7, 8] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 7, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 7, 8], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 5], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 8] }",
            "[{ name: 'move', start: [3, 6], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 8] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 8], to: [3, 9] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 6], to: [3, 7] }");
            /*
    oneOperation = { name: 'move', start: [3, 8], to: [3, 9], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 6], to: [3, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 9], to: [3, 8] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 7], to: [3, 6] }");
            /*
    oneOperation = { name: 'move', start: [3, 9], to: [3, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 7], to: [3, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 8] }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 8] }",
            "{ name: 'move', start: [3, 5], to: [3, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 5], drawings: [5, 6, 8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 7] }",
            "{ name: 'move', start: [3, 5], to: [3, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], to: [3, 2], target: '123456' }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 6], to: [3, 2], target: '123456' }");
            /*
    oneOperation = { name: 'move', start: [3, 6], to: [3, 2], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 6], to: [3, 2], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], to: [3, 2], target: '123456' }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7], target: '123456' }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 7], target: '123456' }",
            "{ name: 'move', start: [3, 5], to: [3, 2], target: '123456' }");
            /*
    oneOperation = { name: 'move', start: [3, 6], to: [3, 2], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 5], drawings: [5, 6, 7], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 5], to: [3, 2], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 4], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "[{ name: 'move', start: [3, 6], to: [3, 4] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 4], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 4], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 4], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "[{ name: 'move', start: [3, 2], to: [3, 4] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 4], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 4], to: [3, 8] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "[{ name: 'move', start: [3, 8], to: [3, 4] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 4], to: [3, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 8], to: [3, 4], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 5], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "[{ name: 'move', start: [3, 6], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 5], to: [3, 7] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "[{ name: 'move', start: [3, 7], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 5], to: [3, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions); // !
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 7], to: [3, 5] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "[{ name: 'move', start: [3, 5], to: [3, 7] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 7], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions); // !
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 7], to: [3, 8] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "[{ name: 'move', start: [3, 8], to: [3, 7] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 7], to: [3, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 8], to: [3, 7], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions); // !
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "[{ name: 'move', start: [3, 2], to: [3, 6] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }",
            "[{ name: 'move', start: [3, 2], to: [3, 6] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 5], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "[{ name: 'move', start: [3, 2], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 5], to: [3, 4] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "[{ name: 'move', start: [3, 4], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 5], to: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 4], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], to: [3, 5] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "[{ name: 'move', start: [3, 5], to: [3, 6] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 5], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "[{ name: 'move', start: [3, 6], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 5], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "[{ name: 'move', start: [3, 2], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 3], to: [3, 6] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "[{ name: 'move', start: [3, 6], to: [3, 3] }, { name: 'group', start: [3, 3], drawings: [3, 4, 5] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 3], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 3], opl: 1, osn: 1 }, { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 3], to: [3, 1] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "[{ name: 'move', start: [3, 1], to: [3, 3] }, { name: 'group', start: [3, 3], drawings: [3, 4, 5] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 3], to: [3, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 1], to: [3, 3], opl: 1, osn: 1 }, { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 3], to: [3, 6], target: '123456' }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6] }",
            "{ name: 'move', start: [3, 3], to: [3, 6], target: '123456' }");
            /*
    oneOperation = { name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 3], to: [3, 6], target: '123456' }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456' }",
            "[{ name: 'move', start: [3, 6], to: [3, 3], target: '123456' }, { name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456' }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 3], target: '123456', opl: 1, osn: 1 }, { name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

 // move and external group operation (handleGroupMove) in OX Presentation
 // { name: group, start: [2, 4], drawings: [4, 5, 7, 8] }
 // { name: move, start: [2, 2], to: [2, 6] }
    @Test
    public void test47() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 3] }",
            "{ name: 'move', start: [3, 2], to: [3, 3] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 3], to: [3, 2] }",
            "{ name: 'move', start: [3, 3], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 3], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 4] }",
            "{ name: 'move', start: [3, 2], to: [3, 4] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }",
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 7] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 8] }",
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 4] }",
            "{ name: 'move', start: [3, 2], to: [3, 4] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 0] }",
            "{ name: 'move', start: [3, 2], to: [3, 0] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 8] }",
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 8] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 2], to: [3, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 7] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], to: [3, 5] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 7] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 2], to: [3, 6], target: '123456' }",
            "{ name: 'move', start: [3, 2], to: [3, 6], target: '123456' }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6], target: '123456' }",
            "{ name: 'move', start: [3, 2], to: [3, 6], target: '123456' }",
            "{ name: 'move', start: [3, 2], to: [3, 5], target: '123456' }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456' }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], to: [3, 5], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "{ name: 'move', start: [3, 4], to: [3, 2] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 4], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 7], to: [3, 6] }",
            "{ name: 'move', start: [3, 5], to: [3, 4] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 7], to: [3, 5] }",
            "{ name: 'move', start: [3, 5], to: [3, 4] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 8] }",
            "{ name: 'move', start: [3, 9], to: [3, 5] }",
            "{ name: 'move', start: [3, 7], to: [3, 5] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 6, 9] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 5, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 9], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 6, 9], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 8] }",
            "{ name: 'move', start: [3, 7], to: [3, 5] }",
            "[]",
            "[{ name: 'move', start: [3, 5], to: [3, 7] }, { name: 'group', start: [3, 3], drawings: [3, 5, 8] }]");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 5, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 5], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 5], to: [3, 7], opl: 1, osn: 1 }, { name: 'group', start: [3, 3], drawings: [3, 5, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 8] }",
            "{ name: 'move', start: [3, 7], to: [3, 9] }",
            "{ name: 'move', start: [3, 6], to: [3, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 7] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 5, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 9], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 8, 10] }",
            "{ name: 'move', start: [3, 7], to: [3, 9] }",
            "[]",
            "[{ name: 'move', start: [3, 9], to: [3, 7] }, { name: 'group', start: [3, 3], drawings: [3, 5, 8, 10] }]");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 5, 8, 10], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 9], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 9], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 9], to: [3, 7], opl: 1, osn: 1 }, { name: 'group', start: [3, 3], drawings: [3, 5, 8, 10], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test68() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 8, 9] }",
            "{ name: 'move', start: [3, 7], to: [3, 10] }",
            "{ name: 'move', start: [3, 6], to: [3, 7] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 7, 8] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 5, 8, 9], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 10], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 5, 7, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test69() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 7], to: [3, 4] }",
            "{ name: 'move', start: [3, 5], to: [3, 4] }",
            "{ name: 'group', start: [3, 3], drawings: [3, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test70() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 6], to: [3, 3] }",
            "{ name: 'move', start: [3, 4], to: [3, 3] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 4], to: [3, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test71() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 8], to: [3, 2] }",
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 7] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 8], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test72() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 8] }",
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "{ name: 'move', start: [3, 5], to: [3, 2] }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 8] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 5], drawings: [5, 6, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test73() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "{ name: 'move', start: [3, 5], to: [3, 2] }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 7] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 5], drawings: [5, 6, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test74() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 7, 8] }",
            "{ name: 'move', start: [3, 5], to: [3, 6] }",
            "[]",
            "[{ name: 'move', start: [3, 6], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 7, 8] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 7, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 7, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test75() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 8], to: [3, 9] }",
            "{ name: 'move', start: [3, 6], to: [3, 7] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 8], to: [3, 9], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 7], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test76() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 9], to: [3, 8] }",
            "{ name: 'move', start: [3, 7], to: [3, 6] }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 9], to: [3, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test77() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 7], to: [3, 2], target: '123456' }",
            "{ name: 'move', start: [3, 7], to: [3, 2], target: '123456' }",
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 2], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 2], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test78() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6], target: '123456' }",
            "{ name: 'move', start: [3, 7], to: [3, 2], target: '123456' }",
            "{ name: 'move', start: [3, 5], to: [3, 2], target: '123456' }",
            "{ name: 'group', start: [3, 5], drawings: [5, 6, 7], target: '123456' }");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 2], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 5], drawings: [5, 6, 7], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test79() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "[]",
            "[{ name: 'move', start: [3, 2], to: [3, 6] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test80() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }",
            "{ name: 'move', start: [3, 6], to: [3, 2] }",
            "[]",
            "[{ name: 'move', start: [3, 2], to: [3, 6] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6, 7] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 2], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 6], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test81() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 5], to: [3, 2] }",
            "[]",
            "[{ name: 'move', start: [3, 2], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test82() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 5], to: [3, 4] }",
            "[]",
            "[{ name: 'move', start: [3, 4], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 4], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 4], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test83() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 6], to: [3, 5] }",
            "[]",
            "[{ name: 'move', start: [3, 5], to: [3, 6] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 6], to: [3, 5], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test84() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 6] }",
            "{ name: 'move', start: [3, 5], to: [3, 6] }",
            "[]",
            "[{ name: 'move', start: [3, 6], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 6] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test85() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 5], to: [3, 2] }",
            "[]",
            "[{ name: 'move', start: [3, 2], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 2], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test86() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 8] }",
            "{ name: 'move', start: [3, 5], to: [3, 6] }",
            "[]",
            "[{ name: 'move', start: [3, 6], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 8] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test87() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 5], to: [3, 6] }",
            "[]",
            "[{ name: 'move', start: [3, 6], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 6], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 6], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test88() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 5], to: [3, 7] }",
            "[]",
            "[{ name: 'move', start: [3, 7], to: [3, 5] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], to: [3, 7], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 5], to: [3, 7], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test89() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 7], to: [3, 5] }",
            "[]",
            "[{ name: 'move', start: [3, 5], to: [3, 7] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 5], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 5], to: [3, 7], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test90() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 4], drawings: [4, 5, 7] }",
            "{ name: 'move', start: [3, 7], to: [3, 8] }",
            "[]",
            "[{ name: 'move', start: [3, 8], to: [3, 7] }, { name: 'group', start: [3, 4], drawings: [4, 5, 7] }]");
            /*
    oneOperation = { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 7], to: [3, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 7], to: [3, 8], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 8], to: [3, 7], opl: 1, osn: 1 }, { name: 'group', start: [3, 4], drawings: [4, 5, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test91() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 3], to: [3, 6] }",
            "[]",
            "[{ name: 'move', start: [3, 6], to: [3, 3] }, { name: 'group', start: [3, 3], drawings: [3, 4, 5] }]");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 6], to: [3, 3], opl: 1, osn: 1 }, { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test92() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 3], to: [3, 1] }",
            "[]",
            "[{ name: 'move', start: [3, 1], to: [3, 3] }, { name: 'group', start: [3, 3], drawings: [3, 4, 5] }]");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 3], to: [3, 1], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 1], to: [3, 3], opl: 1, osn: 1 }, { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test93() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }",
            "{ name: 'move', start: [3, 3], to: [3, 6], target: '123456' }",
            "{ name: 'move', start: [3, 3], to: [3, 6], target: '123456' }",
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5] }");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'group', start: [3, 3], drawings: [3, 4, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test94() {
        TransformerTest.transformPresentation(
            "{ name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456' }",
            "{ name: 'move', start: [3, 3], to: [3, 6], target: '123456' }",
            "[]",
            "[{ name: 'move', start: [3, 6], to: [3, 3], target: '123456' }, { name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456' }]");
            /*
    oneOperation = { name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 3], to: [3, 6], target: '123456', _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [3, 6], to: [3, 3], target: '123456', opl: 1, osn: 1 }, { name: 'group', start: [3, 3], drawings: [3, 4, 5], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }
}
