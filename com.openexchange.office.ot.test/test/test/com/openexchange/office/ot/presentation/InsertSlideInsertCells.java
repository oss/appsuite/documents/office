/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertSlideInsertCells {

    // insertCells and local insertSlide operation (handleInsertCompInsertRows)
    // it('should calculate valid transformed insertCells operation after local insertSlide operation', function () {

    @Test
    public void test01a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [3, 0, 0, 1] }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertCells', start: [4, 0, 0, 1] }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 0, 0, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertCells', start: [4, 0, 0, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [3, 0, 0, 1] }",
            "{ name: 'insertSlide', start: [1], target: '123' }",
            "{ name: 'insertSlide', start: [1], target: '123' }",
            "{ name: 'insertCells', start: [4, 0, 0, 1] }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 0, 0, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertCells', start: [4, 0, 0, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [3, 0, 0, 1] }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertCells', start: [3, 0, 0, 1] }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 0, 0, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertCells', start: [3, 0, 0, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [3, 0, 0, 1], target: '123456' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertCells', start: [3, 0, 0, 1], target: '123456' }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 0, 0, 1], target: '123456', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertCells', start: [3, 0, 0, 1], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    // insertCells and external insertSlide operation (handleInsertCompInsertRows)
    // it('should calculate valid transformed insertCells operation after external insertSlide operation', function () {

    @Test
    public void test01b() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertCells', start: [3, 0, 0, 1] }",
            "{ name: 'insertCells', start: [4, 0, 0, 1] }",
            "{ name: 'insertSlide', start: [3], target: '123' }");
            /*
                oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 0, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertCells', start: [4, 0, 0, 1], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02b() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [1], target: '123' }",
            "{ name: 'insertCells', start: [3, 0, 0, 1] }",
            "{ name: 'insertCells', start: [4, 0, 0, 1] }",
            "{ name: 'insertSlide', start: [1], target: '123' }");
            /*
                oneOperation = { name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 0, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertCells', start: [4, 0, 0, 1], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertSlide', start: [1], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03b() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertCells', start: [3, 0, 0, 1] }",
            "{ name: 'insertCells', start: [3, 0, 0, 1] }",
            "{ name: 'insertSlide', start: [4], target: '123' }");
            /*
                oneOperation = { name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 0, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertCells', start: [3, 0, 0, 1], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04b() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertCells', start: [3, 0, 0, 1], target: '123456' }",
            "{ name: 'insertCells', start: [3, 0, 0, 1], target: '123456' }",
            "{ name: 'insertSlide', start: [3], target: '123' }");
            /*
                oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 0, 1], target: '123456', opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertCells', start: [3, 0, 0, 1], target: '123456', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }
}
