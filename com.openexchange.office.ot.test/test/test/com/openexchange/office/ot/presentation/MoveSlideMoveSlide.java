/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveSlideMoveSlide {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [1], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }], transformedOps);
         */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [5] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [5] }");
            /*
    oneOperation = { name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [1], end: [5] }",
            "{ name: 'moveSlide', start: [1], end: [5] }",
            "{ name: 'moveSlide', start: [2], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [5] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [2], end: [8] }",
            "{ name: 'moveSlide', start: [1], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [2], end: [8] }",
            "{ name: 'moveSlide', start: [1], end: [2] }");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [8], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [2], end: [8] }",
            "{ name: 'moveSlide', start: [1], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [6] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [3], end: [5] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [2], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [5], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'moveSlide', start: [5], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [5] }",
            "{ name: 'moveSlide', start: [1], end: [5] }",
            "{ name: 'moveSlide', start: [3], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [5], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [4], end: [8] }",
            "{ name: 'moveSlide', start: [4], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [5], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [4], end: [8] }",
            "{ name: 'moveSlide', start: [3], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [6], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [5], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [6], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [5], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [6], end: [4], target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [6], end: [4], target: '123456' }");
            /*
    oneOperation = { name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [6], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [8], target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [8], target: '123456' }",
            "{ name: 'moveSlide', start: [6], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [6], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [6], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [6], end: [4], target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [8], target: '234567' }",
            "{ name: 'moveSlide', start: [3], end: [8], target: '234567' }",
            "{ name: 'moveSlide', start: [6], end: [4], target: '123456' }");
            /*
    oneOperation = { name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '234567', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [6], end: [4], target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [8], target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [8], target: '123456' }",
            "{ name: 'moveSlide', start: [5], end: [3], target: '123456' }");
            /*
    oneOperation = { name: 'moveSlide', start: [6], end: [4], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [8], target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [5], end: [3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [1] }",
            "[]",
            "[]");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [8] }",
            "{ name: 'moveSlide', start: [1], end: [8] }",
            "[]");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [8], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [8], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [1], target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [1], target: '123456' }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [1], target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [1], target: '234567' }",
            "{ name: 'moveSlide', start: [3], end: [1], target: '234567' }",
            "{ name: 'moveSlide', start: [3], end: [1], target: '123456' }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], target: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], target: '234567', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [1], target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [1], target: '123456' }",
            "[]",
            "[]");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], target: '123456', _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [0] }",
            "{ name: 'moveSlide', start: [0], end: [1] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [1], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [1] }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [3], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [4] }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [5] }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [5], end: [3] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [3], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [1] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "{ name: 'moveSlide', start: [3], end: [2] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [3], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [5] }",
            "{ name: 'moveSlide', start: [3], end: [5] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [5], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [2], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [4] }",
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [5] }",
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "{ name: 'moveSlide', start: [5], end: [2] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [1], end: [5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [5], end: [2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [2], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }
}
