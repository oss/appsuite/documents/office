/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class Comments {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], parent: 0 }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], parent: 0 }",
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', parent: 0, pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', parent: 0, pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], parent: 0 }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], parent: 0 }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', parent: 0, pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', parent: 0, pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], parent: 0 }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], parent: 0 }",
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], parent: 0 }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], parent: 0 }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', parent: 0, pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', parent: 0, pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 2], text: '2222', parent: 0, pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', parent: 0, pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 3], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 3], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 3], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 3], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "[]");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [2, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [2, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }


    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "[]",
            "[]");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 2] }",
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 2] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 1] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'deleteComment', start: [2, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'deleteComment', start: [3, 1] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 1] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [2, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [4, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 2] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 3] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 2] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 3] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [4, 2] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [4, 2] }");
            /*
    oneOperation = { name: 'deleteComment', start: [4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [4, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 2] }",
            "{ name: 'deleteComment', start: [3, 3] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [3, 2] }",
            "{ name: 'deleteComment', start: [3, 3] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'deleteComment', start: [4, 2] }",
            "{ name: 'deleteComment', start: [4, 2] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [4, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [4, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);             */
    }


    @Test
    public void test36() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 2], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 2], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
           */
    }
      @Test
    public void test43() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'changeComment', start: [3, 2], text: '3333' }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 2], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 1], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 2], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "[]",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'deleteComment', start: [3, 1] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'deleteComment', start: [4, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test52() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 2] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'deleteComment', start: [3, 2] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "[]");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'deleteComment', start: [3, 1] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test58()  {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 1], text: '3333' }",
            "{ name: 'deleteComment', start: [3, 2] }",
            "{ name: 'deleteComment', start: [3, 2] }",
            "{ name: 'changeComment', start: [3, 1], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 1], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }
}
