/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertCellsDelete {

    // insertCells and local delete operation
    // it('should calculate valid transformed insertCells operation after local deleteColumns operation in OX Presentation and forcing reload', function () {

    @Test
    public void test01a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [2, 1, 1], count: 1 }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', start: [2, 1, 1], count: 1, opl: 1, osn: 1 };
    localOperation = { name: 'delete', start: [1], end: [2], opl: 1, osn: 1 };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test02a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [1, 4, 1], count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 4] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 1], count: 3 };
    localOperation = { name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test03a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 3, 1] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
    localOperation = { name: 'delete', start: [3, 1, 1, 3, 1], opl: 1, osn: 1 };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test04a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [1, 8] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 8] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test05a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [1, 1, 0], end: [1, 1, 8] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1, 0], end: [1, 1, 8] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test06a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [3, 3] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test07a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test08a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [1] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test09a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [2, 1, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*

    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 2], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test10a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [2, 1, 1, 4, 1, 2], count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*

    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 4, 1, 2], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test11a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 3], count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1], end: [2, 1, 1, 2, 2] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*

    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 3], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], end: [2, 1, 1, 2, 2] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test12a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 4], count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1, 2], end: [2, 1, 1, 2, 1, 6] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 4], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 2], end: [2, 1, 1, 2, 1, 6] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test13a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test14a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [2, 1, 1], count: 1 }",
            "{ name: 'delete', start: [1], end: [2, 5] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 5] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test15a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [2, 1, 4, 5, 2], count: 3 }",
            "{ name: 'delete', start: [1, 4], end: [3, 8] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 4, 5, 2], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test16a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [1] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
    localOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test17a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [1, 2, 2, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*

    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], count: 3 }; // in a text frame
    localOperation = { name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test18a() {
        TransformerTest.transformPresentation(
            "{ name: 'insertCells', start: [1, 2, 2, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
           /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], count: 3 }; // in a text frame
    localOperation = { name: 'delete', start: [1], end: [2], opl: 1, osn: 1 };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }
}
