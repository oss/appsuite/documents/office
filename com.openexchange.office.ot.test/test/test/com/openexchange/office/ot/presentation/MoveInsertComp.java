/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveInsertComp {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 1, 1] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4] }",
            "{ name: 'insertParagraph', start: [3, 0, 1] }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 0, 1]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 1, 1] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 4] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 4] }",
            "{ name: 'insertParagraph', start: [3, 1, 1] }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 3, 1] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2] }",
            "{ name: 'insertParagraph', start: [3, 3, 1] }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 3, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 1]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 1, 1] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }",
            "{ name: 'insertParagraph', start: [3, 2, 1] }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 2, 1]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123456' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123456' }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 1, 1], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123456' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], target: '234567' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], target: '234567' }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123456' }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 1, 1], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], target: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123456' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], target: '123456' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], target: '123456' }",
            "{ name: 'insertParagraph', start: [3, 2, 1], target: '123456' }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 1, 1], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 2, 1]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 4, 1] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }",
            "{ name: 'insertParagraph', start: [3, 0, 1] }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 4, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 0, 1]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4] }",
            "{ name: 'insertParagraph', start: [3, 1, 1] }",
            "{ name: 'insertParagraph', start: [3, 0, 1] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 0]);
    expect(transformedOps[0].end).to.deep.equal([3, 0]);
    expect(transformedOps[0].to).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 1], end: [3, 1], to: [3, 4] }",
            "{ name: 'insertParagraph', start: [3, 1, 1] }",
            "{ name: 'insertParagraph', start: [3, 4, 1] }",
            "{ name: 'move', start: [3, 1], end: [3, 1], to: [3, 4] }");
            /*
    oneOperation = { name: 'move', start: [3, 1], end: [3, 1], to: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 1]);
    expect(transformedOps[0].to).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 1], to: [3, 4] }",
            "{ name: 'insertParagraph', start: [3, 1, 1] }",
            "{ name: 'insertParagraph', start: [3, 4, 1] }",
            "{ name: 'move', start: [3, 1], to: [3, 4] }");
            /*
    oneOperation = { name: 'move', start: [3, 1], to: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1]);
    expect(transformedOps[0].end).to.equal(undefined);
    expect(transformedOps[0].to).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], to: [3, 4] }",
            "{ name: 'insertParagraph', start: [3, 1, 1] }",
            "{ name: 'insertParagraph', start: [3, 0, 1] }",
            "{ name: 'move', start: [3, 0], to: [3, 4] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], to: [3, 4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 0]);
    expect(transformedOps[0].end).to.equal(undefined);
    expect(transformedOps[0].to).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [4, 3] }",
            "{ name: 'insertSlide', start: [3], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [0], target: '123' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [4, 3] }",
            "{ name: 'insertSlide', start: [0], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'insertSlide', start: [4], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [4, 3] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([4, 0]);
    expect(transformedOps[0].end).to.deep.equal([4, 0]);
    expect(transformedOps[0].to).to.deep.equal([4, 3]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'insertSlide', start: [0], target: '123' }",
            "{ name: 'insertSlide', start: [0], target: '123' }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [4, 3] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [0], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([4, 0]);
    expect(transformedOps[0].end).to.deep.equal([4, 0]);
    expect(transformedOps[0].to).to.deep.equal([4, 3]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 0]);
    expect(transformedOps[0].end).to.deep.equal([3, 0]);
    expect(transformedOps[0].to).to.deep.equal([3, 3]);
             */
    }
}
