/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveSlideGeneric {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'insertText', start: [4, 0, 0, 1], text: 'o' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'insertText', start: [4, 0, 0, 1], text: 'o' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'insertText', start: [2, 0, 0, 1], text: 'o' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [1], end: [4] }",
            "{ name: 'moveSlide', start: [1], end: [4] }",
            "{ name: 'insertText', start: [2, 0, 0, 1], text: 'o' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'insertText', start: [4, 0, 0, 1], text: 'o' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'insertText', start: [4, 0, 0, 1], text: 'o' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [0], end: [3] }",
            "{ name: 'moveSlide', start: [0], end: [3] }",
            "{ name: 'insertText', start: [2, 0, 0, 1], text: 'o' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'insertText', start: [4, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [3], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'insertText', start: [4, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456' }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456' }",
            "{ name: 'moveSlide', start: [3], end: [0] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [2] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [1], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [1], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [3] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'insertText', start: [2, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [1], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [1], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [4] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'insertText', start: [2, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [1], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', start: [2, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [1], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'insertText', start: [4, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [4], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'insertText', start: [4, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', start: [4, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'insertText', start: [3, 0, 0, 1], text: 'o' }",
            "{ name: 'moveSlide', start: [4], end: [5] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', start: [3, 0, 0, 1], text: 'o', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20a() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'move', start: [4, 0], to: [4, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20b() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'move', start: [4, 0], to: [4, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'move', start: [0, 0], to: [0, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [0, 0], to: [0, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'moveSlide', start: [4], end: [0] }",
            "{ name: 'moveSlide', start: [4], end: [0] }",
            "{ name: 'move', start: [4, 0], to: [4, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'move', start: [2, 0], to: [2, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'move', start: [2, 0], to: [2, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'move', start: [4, 0], to: [4, 2] }",
            "{ name: 'moveSlide', start: [3], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'move', start: [4, 0], to: [4, 2] }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'move', start: [0, 0], to: [0, 2] }",
            "{ name: 'moveSlide', start: [3], end: [0] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [0, 0], to: [0, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [0] }",
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'move', start: [4, 0], to: [4, 2] }",
            "{ name: 'moveSlide', start: [4], end: [0] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'move', start: [3, 0], to: [3, 2] }",
            "{ name: 'move', start: [2, 0], to: [2, 2] }",
            "{ name: 'moveSlide', start: [0], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [2, 0], to: [2, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 } }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 } }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'setAttributes', start: [0, 0], end: [0, 2], attrs: { abc: 1 } }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [0, 0], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [4], end: [0] }",
            "{ name: 'moveSlide', start: [4], end: [0] }",
            "{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 } }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'setAttributes', start: [2, 0], end: [2, 2], attrs: { abc: 1 } }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [2, 0], end: [2, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [3], end: [1] }",
            "{ name: 'moveSlide', start: [3], end: [1] }",
            "{ name: 'setAttributes', start: [1], attrs: { abc: 1 } }");
            /*
    oneOperation = { name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [1], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'setAttributes', start: [2], attrs: { abc: 1 } }");
            /*
    oneOperation = { name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [0], end: [2] }",
            "{ name: 'moveSlide', start: [0], end: [2] }",
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }");
            /*
    oneOperation = { name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [0], end: [3] }",
            "{ name: 'moveSlide', start: [0], end: [3] }",
            "{ name: 'setAttributes', start: [2], attrs: { abc: 1 } }");
            /*
    oneOperation = { name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [2], attrs: { abc: 1 }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [3], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'setAttributes', start: [0, 0], end: [0, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [3], end: [0] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'setAttributes', start: [0, 0], end: [0, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [0] }",
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [4], end: [0] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'setAttributes', start: [4, 0], end: [4, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 } }",
            "{ name: 'setAttributes', start: [2, 0], end: [2, 2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [0], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'setAttributes', start: [2, 0], end: [2, 2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [1] }",
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }",
            "{ name: 'setAttributes', start: [1], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [3], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'setAttributes', start: [1], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }",
            "{ name: 'setAttributes', start: [2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [0], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'setAttributes', start: [2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [0], end: [2] }",
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }",
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [0], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [0], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [0], end: [3] }",
            "{ name: 'setAttributes', start: [3], attrs: { abc: 1 } }",
            "{ name: 'setAttributes', start: [2], attrs: { abc: 1 } }",
            "{ name: 'moveSlide', start: [0], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3], attrs: { abc: 1 }, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'setAttributes', start: [2], attrs: { abc: 1 }, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [0], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [3], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [4], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [2], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [2], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [2], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [4], end: [5] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [3], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [4], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [2, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [2], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [2, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [2], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [2], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [4], end: [5] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'moveSlide', start: [4], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'moveSlide', start: [2], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [2, 0] }",
            "{ name: 'moveSlide', start: [2], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'moveSlide', start: [2], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'moveSlide', start: [4], end: [5] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test68() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test69() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test70() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test71() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test72() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [2, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test73() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test74() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529] }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test75() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test76() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test77() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test78() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'changeComment', start: [2, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test79() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'changeComment', start: [2, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [2, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test80() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test81() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test82() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'deleteComment', start: [4, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test83() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'deleteComment', start: [4, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test84() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'deleteComment', start: [4, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test85() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'deleteComment', start: [2, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test86() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'deleteComment', start: [2, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [2, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test87() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test88() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test89() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [3], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test90() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test91() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [4], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test92() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [2], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [2], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [2], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test93() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [2], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [2], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [2], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test94() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [2], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test95() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [4], end: [5] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test96() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }",
            "{ name: 'moveSlide', start: [3], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test97() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test98() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }",
            "{ name: 'moveSlide', start: [4], end: [2] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test99() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [2], target: 'master_id' }",
            "{ name: 'moveSlide', start: [2], end: [4] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [2], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test100() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [2], target: 'master_id' }",
            "{ name: 'moveSlide', start: [2], end: [3] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [2], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test101() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'moveSlide', start: [2], end: [1] }");
            /*
    oneOperation = { name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test102() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'moveSlide', start: [4], end: [5] }");
            /*
    oneOperation = { name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test103() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test104() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test105() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test106() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'changeLayout', start: [2], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [2], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test107() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'changeLayout', start: [2], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [2], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test108() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test109() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test110() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test111() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test112() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'moveSlide', start: [4], end: [2] }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test113() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'moveSlide', start: [2], end: [4] }",
            "{ name: 'changeMaster', start: [2], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [2], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test114() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'moveSlide', start: [2], end: [3] }",
            "{ name: 'changeMaster', start: [2], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [2], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test115() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'moveSlide', start: [2], end: [1] }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [2], end: [1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test116() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'moveSlide', start: [4], end: [5] }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    // ungroup and local moveSlide operation (handleMoveSlideGeneric)
    // it('should calculate valid transformed ungroup operation after local moveSlide operation', function () {

    @Test
    public void test117() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'ungroup', start: [4, 0] }");
            /*
        oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    public void test118() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'ungroup', start: [4, 0] }");
            /*
        oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    public void test119() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'ungroup', start: [4, 0] }");
            /*
        oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'ungroup', start: [0, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    public void test120() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'ungroup', start: [4, 0] }");
            /*
        oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    public void test121() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'ungroup', start: [4, 0] }");
            /*
        oneOperation = { name: 'ungroup', start: [3, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'ungroup', start: [2, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    public void test122() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'ungroup', start: [4, 0] }");
            /*
        oneOperation = { name: 'ungroup', start: [1, 1], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    public void test123() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'ungroup', start: [4, 0] }");
            /*
        oneOperation = { name: 'ungroup', start: [1, 1], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    public void test124() {
        TransformerTest.transformPresentation(
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'ungroup', start: [4, 0] }");
            /*
        oneOperation = { name: 'ungroup', start: [0, 1], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'ungroup', start: [1, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    // ungroup and external moveSlide operation (handleMoveSlideGeneric)
    // it('should calculate valid transformed ungroup operation after external moveSlide operation', function () {

    @Test
    public void test125() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [4] }",
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'ungroup', start: [4, 0] }",
            "{ name: 'moveSlide', start: [3], end: [4] }");
            /*
        oneOperation = { name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'moveSlide', start: [3], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test126() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [3] }",
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'ungroup', start: [4, 0] }",
            "{ name: 'moveSlide', start: [4], end: [3] }");
            /*
        oneOperation = { name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'moveSlide', start: [4], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test127() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [3], end: [0] }",
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'ungroup', start: [0, 0] }",
            "{ name: 'moveSlide', start: [3], end: [0] }");
            /*
        oneOperation = { name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'ungroup', start: [0, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'moveSlide', start: [3], end: [0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test128() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [4], end: [0] }",
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'ungroup', start: [4, 0] }",
            "{ name: 'moveSlide', start: [4], end: [0] }");
            /*
        oneOperation = { name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'ungroup', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'moveSlide', start: [4], end: [0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test129() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [0], end: [4] }",
            "{ name: 'ungroup', start: [3, 0] }",
            "{ name: 'ungroup', start: [2, 0] }",
            "{ name: 'moveSlide', start: [0], end: [4] }");
            /*
        oneOperation = { name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'ungroup', start: [3, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'ungroup', start: [2, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'moveSlide', start: [0], end: [4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test130() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [0], end: [1] }",
            "{ name: 'ungroup', start: [1, 1] }",
            "{ name: 'ungroup', start: [0, 1] }",
            "{ name: 'moveSlide', start: [0], end: [1] }");
            /*
        oneOperation = { name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'ungroup', start: [1, 1], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'moveSlide', start: [0], end: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test131() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [0] }",
            "{ name: 'ungroup', start: [1, 1] }",
            "{ name: 'ungroup', start: [0, 1] }",
            "{ name: 'moveSlide', start: [1], end: [0] }");
            /*
        oneOperation = { name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'ungroup', start: [1, 1], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test132() {
        TransformerTest.transformPresentation(
            "{ name: 'moveSlide', start: [1], end: [0] }",
            "{ name: 'ungroup', start: [0, 1] }",
            "{ name: 'ungroup', start: [1, 1] }",
            "{ name: 'moveSlide', start: [1], end: [0] }");
            /*
        oneOperation = { name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'ungroup', start: [0, 1], opl: 1, osn: 1 }] }];
        transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'ungroup', start: [1, 1], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'moveSlide', start: [1], end: [0], opl: 1, osn: 1 }], transformedOps);
             */
    }
}
