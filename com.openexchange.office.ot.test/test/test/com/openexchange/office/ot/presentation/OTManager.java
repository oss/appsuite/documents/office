/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class OTManager {

    @Test
    public void test01() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertDrawing', start: [0, 31], type: 'shape' }," +
                "{ name: 'insertParagraph', start: [0, 31, 0] }," +
                "{ name: 'insertText', start: [0, 31, 0, 0], text: '123' }]",
            "[{ name: 'changeLayout', start: [0], target: '2147483649' }," +
                "{ name: 'insertDrawing', start: [0, 32], type: 'shape', drawing: { name: 'ctrTitle 1' } }," +
                "{ name: 'insertParagraph', start: [0, 32, 0] }]",
            "[{ name: 'changeLayout', start: [0], target: '2147483649' }," +
                "{ name: 'insertDrawing', start: [0, 33], type: 'shape', drawing: { name: 'ctrTitle 1' } }," +
                "{ name: 'insertParagraph', start: [0, 33, 0] }]",
            "[{ name: 'insertDrawing', start: [0, 31], type: 'shape' }," +
                "{ name: 'insertParagraph', start: [0, 31, 0] }," +
                "{ name: 'insertText', start: [0, 31, 0, 0], text: '123' }]");

    }

    @Test
    public void test02() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertDrawing', start: [0, 31], type: 'shape' }," +
                "{ name: 'insertParagraph', start: [0, 31, 0] }," +
                "{ name: 'insertText', start: [0, 31, 0, 0], text: '123' }]",
            "[{ name: 'insertDrawing', start: [0, 32], type: 'shape', drawing: { name: 'ctrTitle 1' } }," +
                "{ name: 'insertParagraph', start: [0, 32, 0] }]",
            "[{ name: 'insertDrawing', start: [0, 33], type: 'shape', drawing: { name: 'ctrTitle 1' } }," +
                "{ name: 'insertParagraph', start: [0, 33, 0] }]",
            "[{ name: 'insertDrawing', start: [0, 31], type: 'shape' }," +
                "{ name: 'insertParagraph', start: [0, 31, 0] }," +
                "{ name: 'insertText', start: [0, 31, 0, 0], text: '123' }]");

    }

    @Test
    public void test03() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertParagraph', start: [0, 31, 0] }]",
            "[{ name: 'insertDrawing', start: [0, 32], type: 'shape', drawing: { name: 'ctrTitle 1' } }," +
                "{ name: 'insertParagraph', start: [0, 32, 0] }]",
            "[{ name: 'insertDrawing', start: [0, 32], type: 'shape', drawing: { name: 'ctrTitle 1' } }," +
                "{ name: 'insertParagraph', start: [0, 32, 0] }]",
            "[{ name: 'insertParagraph', start: [0, 31, 0] }]");

    }

    @Test
    public void test04() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertSlide', start: [5], target: '214' }]",
            "[{ name: 'changeLayout', start: [0], target: '123456' }]",
            "[{ name: 'changeLayout', start: [0], target: '123456' }]",
            "[{ name: 'insertSlide', start: [5], target: '214' }]");

    }

    @Test
    public void test05() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertDrawing', start: [0, 5] }]",
            "[{ name: 'changeLayout', start: [0], target: '123456' }]",
            "[{ name: 'changeLayout', start: [0], target: '123456' }]",
            "[{ name: 'insertDrawing', start: [0, 5] }]");

    }

    @Test
    public void test06() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertParagraph', start: [0, 5, 0] }]",
            "[{ name: 'changeLayout', start: [0], target: '123456' }]",
            "[{ name: 'changeLayout', start: [0], target: '123456' }]",
            "[{ name: 'insertParagraph', start: [0, 5, 0] }]");

    }

    @Test
    public void test07() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }]",
            "[{ name: 'changeLayout', start: [0], target: '123456' }]",
            "[{ name: 'changeLayout', start: [0], target: '123456' }]",
            "[{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }]");

    }

    @Test
    public void test08() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertSlide', start: [5], target: '214' }]",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "[{ name: 'insertSlide', start: [5], target: '214' }]");

    }

    @Test
    public void test09() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertDrawing', start: [0, 5] }]",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "[{ name: 'insertDrawing', start: [0, 5] }]");

    }

    @Test
    public void test10() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertParagraph', start: [0, 5, 0] }]",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "[{ name: 'insertParagraph', start: [0, 5, 0] }]");

    }

    @Test
    public void test11() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }]",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "[{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }]");

    }


    @Test
    public void test12() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertSlide', start: [5], target: '214' }]",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "[{ name: 'insertSlide', start: [5], target: '214' }]");

    }

    @Test
    public void test13() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertDrawing', start: [0, 5] }]",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "[{ name: 'insertDrawing', start: [0, 5] }]");

    }

    @Test
    public void test14() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertParagraph', start: [0, 5, 0] }]",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "[{ name: 'insertParagraph', start: [0, 5, 0] }]");

    }

    @Test
    public void test15() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }]",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "[{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }]");

    }

    @Test
    public void test16() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertSlide', start: [5], target: '214' }]",
            "{ name: 'deleteTargetSlide', id: 'layout_2' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2' }",
            "[{ name: 'insertSlide', start: [5], target: '214' }]");

    }

    @Test
    public void test17() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertDrawing', start: [0, 5] }]",
            "{ name: 'deleteTargetSlide', id: 'layout_2' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2' }",
            "[{ name: 'insertDrawing', start: [0, 5] }]");

    }

    @Test
    public void test18() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertParagraph', start: [0, 5, 0] }]",
            "{ name: 'deleteTargetSlide', id: 'layout_2' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2' }",
            "[{ name: 'insertParagraph', start: [0, 5, 0] }]");

    }

    @Test
    public void test19() {

        TransformerTest.transformPresentation(
            "[{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }]",
            "{ name: 'deleteTargetSlide', id: 'layout_2' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2' }",
            "[{ name: 'insertText', start: [0, 5, 0, 0], text: 'abc' }]");

    }

}
