/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertLayoutSlideDeleteTargetSlide {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'master_2', index: 2 }",
            "{ name: 'deleteTargetSlide', id: 'master_2', index: 2 }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'master_1', index: 2 }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localOperation = { name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2' }");
            /*
    oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_2', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1' }");
            /*
    oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1' }");
            /*
    oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 3, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1' }");
            /*
    oneOperation = { name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteTargetSlide', id: 'layout_2', index: 2, parenttarget: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteTargetSlide', id: 'master_2', index: 2 }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'deleteTargetSlide', id: 'master_2', index: 2 }");
            /*
    oneOperation = { name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteTargetSlide', id: 'master_2', index: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteTargetSlide', id: 'master_1', index: 2 }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "[]");
            /*
    oneOperation = { name: 'deleteTargetSlide', id: 'master_1', index: 2, opl: 1, osn: 1 };
    localOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    testRunner.expectBidiError(oneOperation, localOperation);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'master_2' }",
            "{ name: 'delete', start: [0], target: 'master_2' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }");
            /*
    oneOperation = { name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [0], target: 'layout_2', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }");
            /*
    oneOperation = { name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }");
            /*
    oneOperation = { name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [0], target: 'layout_2' }",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'layout_2' }");
            /*
    oneOperation = { name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [0], id: 'layout_2', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [0], target: 'master_2' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'master_2' }");
            /*
    oneOperation = { name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [0], target: 'master_2', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'delete', start: [0], target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1' }",
            "{ name: 'delete', start: [0], target: 'master_1' }");
            /*
    oneOperation = { name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [0], target: 'master_1', opl: 1, osn: 1 }], transformedOps);
             */
    }
}
