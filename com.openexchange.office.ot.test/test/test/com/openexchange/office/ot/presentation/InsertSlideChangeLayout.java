/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertSlideChangeLayout {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [2], target: '123', count: 1, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [2], target: '123', count: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }");
            /*
    oneOperation = { name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', count: 1, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [4], target: '123', count: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }",
            "{ name: 'insertSlide', start: [3], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [4], target: 'layout_id' }",
            "{ name: 'insertSlide', start: [2], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [2], target: '123', count: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [4], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [2], target: '123', count: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'changeLayout', start: [3], target: 'layout_id' }",
            "{ name: 'insertSlide', start: [4], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [4], target: '123', count: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeLayout', start: [3], target: 'layout_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [4], target: '123', count: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [2], target: '123', count: 1, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [2], target: '123', count: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }");
            /*
    oneOperation = { name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }",
            "{ name: 'insertSlide', start: [3], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [3], target: '123', count: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [4], target: 'master_id' }",
            "{ name: 'insertSlide', start: [2], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [2], target: '123', count: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [4], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [2], target: '123', count: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'changeMaster', start: [3], target: 'master_id' }",
            "{ name: 'insertSlide', start: [4], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [4], target: '123', count: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeMaster', start: [3], target: 'master_id', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [4], target: '123', count: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }
}
