/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertSlideComment {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'insertComment', start: [3, 0], text: '2222' }",
            "{ name: 'insertComment', start: [4, 0], text: '2222' }",
            "{ name: 'insertSlide', start: [2], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertComment', start: [3, 0], text: '2222' }",
            "{ name: 'insertComment', start: [4, 0], text: '2222' }",
            "{ name: 'insertSlide', start: [3], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertComment', start: [3, 0], text: '2222' }",
            "{ name: 'insertComment', start: [3, 0], text: '2222' }",
            "{ name: 'insertSlide', start: [4], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [3, 1, 1], target: '123' }",
            "{ name: 'insertComment', start: [4, 0], text: '2222' }",
            "{ name: 'insertComment', start: [4, 0], text: '2222' }",
            "{ name: 'insertSlide', start: [3, 1, 1], target: '123' }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }",
            "{ name: 'insertSlide', start: [2], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }",
            "{ name: 'insertSlide', start: [3], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'insertSlide', start: [4], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'deleteComment', start: [3, 0], text: '3333' }",
            "{ name: 'deleteComment', start: [4, 0], text: '3333' }",
            "{ name: 'insertSlide', start: [2], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'deleteComment', start: [3, 0], text: '3333' }",
            "{ name: 'deleteComment', start: [4, 0], text: '3333' }",
            "{ name: 'insertSlide', start: [3], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'deleteComment', start: [3, 0], text: '3333' }",
            "{ name: 'deleteComment', start: [3, 0], text: '3333' }",
            "{ name: 'insertSlide', start: [4], target: '123' }");
            /*
    oneOperation = { name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }",
            "{ name: 'deleteComment', start: [4, 0], text: '3333' }",
            "{ name: 'deleteComment', start: [4, 0], text: '3333' }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }");
            /*
    oneOperation = { name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222' }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'insertComment', start: [4, 0], text: '2222' }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertComment', start: [4, 0], text: '2222' }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [3, 0], text: '2222' }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertComment', start: [3, 0], text: '2222' }");
            /*
    oneOperation = { name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [3, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'insertComment', start: [4, 0], text: '2222' }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }",
            "{ name: 'insertComment', start: [4, 0], text: '2222' }");
            /*
    oneOperation = { name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertComment', start: [4, 0], text: '2222', pos: [529, 529], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [3, 0], text: '3333' }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'changeComment', start: [3, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [3, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'changeComment', start: [4, 0], text: '3333' }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }",
            "{ name: 'changeComment', start: [4, 0], text: '3333' }");
            /*
    oneOperation = { name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'changeComment', start: [4, 0], text: '3333', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'insertSlide', start: [2], target: '123' }",
            "{ name: 'deleteComment', start: [4, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [2], target: '123', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'insertSlide', start: [3], target: '123' }",
            "{ name: 'deleteComment', start: [4, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [3], target: '123', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [3, 0] }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'insertSlide', start: [4], target: '123' }",
            "{ name: 'deleteComment', start: [3, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertSlide', start: [4], target: '123', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteComment', start: [4, 0] }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }",
            "{ name: 'insertParagraph', start: [3, 1, 1], target: '123' }",
            "{ name: 'deleteComment', start: [4, 0] }");
            /*
    oneOperation = { name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertParagraph', start: [3, 1, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'deleteComment', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }
}
