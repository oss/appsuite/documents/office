/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveSetAttrs {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'setAttributes', start: [3, 3], end: [3, 4] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3]);
    expect(transformedOps[0].end).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3], end: [3] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'setAttributes', start: [3], end: [3] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
    expect(transformedOps[0].end).to.deep.equal([3]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [2], end: [2] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'setAttributes', start: [2], end: [2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [2], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2]);
    expect(transformedOps[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'setAttributes', start: [3, 3, 1], end: [3, 3, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 3, 2]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456' }",
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 4, 2]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '234567' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456' }",
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '234567' }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '234567', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 4, 2]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '123456' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456' }",
            "{ name: 'setAttributes', start: [3, 3, 1], end: [3, 3, 2], target: '123456' }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 3, 2]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 6] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 6] }",
            "{ name: 'setAttributes', start: [3, 6, 1], end: [3, 6, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 6, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 6, 2]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 4], to: [3, 6] }",
            "{ name: 'move', start: [3, 4], to: [3, 6] }",
            "{ name: 'setAttributes', start: [3, 6, 1], end: [3, 6, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.equal(undefined);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 6, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 6, 2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 6] }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 6] }",
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 4, 2]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [3, 6] }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [3, 6] }",
            "{ name: 'setAttributes', start: [3, 3, 1], end: [3, 3, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 3, 2]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 3] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 3] }",
            "{ name: 'setAttributes', start: [3, 5, 1], end: [3, 5, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 5, 2]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 4] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 4] }",
            "{ name: 'setAttributes', start: [3, 5, 1], end: [3, 5, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 5, 2]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 5] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 5] }",
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 4, 2]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 3] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 3] }",
            "{ name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2] }");
            /*
    oneOperation = { name: 'setAttributes', start: [3, 4, 1], end: [3, 4, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 1]);
    expect(transformedOps[0].end).to.deep.equal([3, 4, 2]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [4, 0] }",
            "{ name: 'move', start: [4, 1], to: [4, 0] }",
            "{ name: 'move', start: [4, 1], to: [4, 0] }",
            "{ name: 'setAttributes', start: [4, 1] }");
            /*
    oneOperation = { name: 'setAttributes', start: [4, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 1], to: [4, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [4, 1], to: [4, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [4, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [4, 1] }",
            "{ name: 'move', start: [4, 0], to: [4, 2] }",
            "{ name: 'move', start: [4, 0], to: [4, 2] }",
            "{ name: 'setAttributes', start: [4, 0] }");
            /*
    oneOperation = { name: 'setAttributes', start: [4, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'setAttributes', start: [4, 1] }",
            "{ name: 'move', start: [4, 0], to: [4, 1] }",
            "{ name: 'move', start: [4, 0], to: [4, 1] }",
            "{ name: 'setAttributes', start: [4, 0] }");
            /*
    oneOperation = { name: 'setAttributes', start: [4, 1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], to: [4, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [4, 0], to: [4, 1], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', start: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5] }",
            "{ name: 'setAttributes', start: [3, 3], end: [3, 4] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 0]);
    expect(transformedOps[0].end).to.deep.equal([3, 0]);
    expect(transformedOps[0].to).to.deep.equal([3, 6]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 0] }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5] }",
            "{ name: 'setAttributes', start: [3, 5], end: [3, 6] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 0] }");
            /*
    oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 6]);
    expect(transformedOps[0].end).to.deep.equal([3, 6]);
    expect(transformedOps[0].to).to.deep.equal([3, 0]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 0] }",
            "{ name: 'setAttributes', start: [3] }",
            "{ name: 'setAttributes', start: [3] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 0] }");
            /*
    oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'setAttributes', start: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].end).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 6]);
    expect(transformedOps[0].end).to.deep.equal([3, 6]);
    expect(transformedOps[0].to).to.deep.equal([3, 0]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'updateField', start: [3, 2, 1, 1], representation: 'abc' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'updateField', start: [3, 1, 1, 1], representation: 'abc' }");
            /*
    oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 1, 1, 1], representation: 'abc' }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'updateField', start: [3, 2, 1, 1], representation: 'abc' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }",
            "{ name: 'updateField', start: [3, 3, 1, 1], representation: 'abc' }");
            /*
    oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 3, 1, 1], representation: 'abc' }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'updateField', start: [3, 2, 1, 1], representation: 'abc' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0] }",
            "{ name: 'updateField', start: [3, 0, 1, 1], representation: 'abc' }");
            /*
    oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [3, 0, 1, 1], representation: 'abc' }], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }",
            "{ name: 'updateField', start: [3, 2, 1, 1], representation: 'abc' }",
            "{ name: 'updateField', start: [3, 1, 1, 1], representation: 'abc' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6] }");
            /*
    oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 1, 1, 1], representation: 'abc' }] }], localActions);
    expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }",
            "{ name: 'updateField', start: [3, 2, 1, 1], representation: 'abc' }",
            "{ name: 'updateField', start: [3, 3, 1, 1], representation: 'abc' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0] }");
            /*
    oneOperation = { name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 3, 1, 1], representation: 'abc' }] }], localActions);
    expectOp([{ name: 'move', start: [3, 4], end: [3, 4], to: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0] }",
            "{ name: 'updateField', start: [3, 2, 1, 1], representation: 'abc' }",
            "{ name: 'updateField', start: [3, 0, 1, 1], representation: 'abc' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 2, 1, 1], representation: 'abc' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [3, 0, 1, 1], representation: 'abc' }] }], localActions);
    expectOp([{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }
}
