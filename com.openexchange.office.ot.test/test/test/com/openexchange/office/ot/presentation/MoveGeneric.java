/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveGeneric {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'insertRows', start: [3, 4, 0], count: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'insertRows', start: [3, 3, 0], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 0]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'insertRows', start: [3, 2, 0], count: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'insertRows', start: [3, 5, 0], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 2, 0], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'insertRows', start: [3, 1, 0], count: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'insertRows', start: [3, 1, 0], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1, 0], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1, 0]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'insertRows', start: [3, 5, 0], count: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'insertRows', start: [3, 5, 0], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 5, 0], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'insertRows', start: [3, 4, 0], count: 3 }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }",
            "{ name: 'insertRows', start: [3, 5, 0], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteColumns', start: [3, 4], startGrid: 1, endGrid: 1 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'deleteColumns', start: [3, 3], startGrid: 1, endGrid: 1 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteColumns', start: [3, 2], startGrid: 1, endGrid: 1 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'deleteColumns', start: [3, 5], startGrid: 1, endGrid: 1 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteColumns', start: [3, 1], startGrid: 1, endGrid: 1 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'deleteColumns', start: [3, 1], startGrid: 1, endGrid: 1 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 1], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteColumns', start: [3, 5], startGrid: 1, endGrid: 1 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'deleteColumns', start: [3, 5], startGrid: 1, endGrid: 1 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 5], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'deleteColumns', start: [3, 4], startGrid: 1, endGrid: 1 }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }",
            "{ name: 'deleteColumns', start: [3, 5], startGrid: 1, endGrid: 1 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'insertColumn', start: [3, 4], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'insertColumn', start: [3, 3], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'insertColumn', start: [3, 2], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'insertColumn', start: [3, 5], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 2], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'insertColumn', start: [3, 1], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'insertColumn', start: [3, 1], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 1], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'insertColumn', start: [3, 5], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'insertColumn', start: [3, 5], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 5], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'insertColumn', start: [3, 4], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }",
            "{ name: 'insertColumn', start: [3, 5], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4], tableGrid: [600, 600, 600], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'mergeParagraph', start: [3, 3, 0], paralength: 3 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 0]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 2, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 2, 0], paralength: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 1, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'mergeParagraph', start: [3, 1, 0], paralength: 3 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 0], paralength: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1, 0]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 5, 0], paralength: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }",
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [3, 3, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 0]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 2]);
    expect(transformedOps[0].end).to.deep.equal([3, 2]);
    expect(transformedOps[0].to).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'mergeParagraph', start: [3, 2, 0], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 2, 0], paralength: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 0]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 2]);
    expect(transformedOps[0].end).to.deep.equal([3, 2]);
    expect(transformedOps[0].to).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'mergeParagraph', start: [3, 1, 0], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 0], paralength: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 0]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 2]);
    expect(transformedOps[0].end).to.deep.equal([3, 2]);
    expect(transformedOps[0].to).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }",
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 5] }");
            /*
    oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 0]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 2]);
    expect(transformedOps[0].end).to.deep.equal([3, 2]);
    expect(transformedOps[0].to).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 3 }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [3, 2] }");
            /*
    oneOperation = { name: 'move', start: [3, 5], end: [3, 5], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 0]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5]);
    expect(transformedOps[0].end).to.deep.equal([3, 5]);
    expect(transformedOps[0].to).to.deep.equal([3, 2]);
             */
    }
}
