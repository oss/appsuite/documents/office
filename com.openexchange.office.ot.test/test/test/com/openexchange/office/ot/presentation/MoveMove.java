/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveMove {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }");
            /*
                oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'move', start: [1, 0], end: [1, 0], to: [1, 4] }",
            "{ name: 'move', start: [1, 0], end: [1, 0], to: [1, 4] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }");
            /*
                oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 0], end: [1, 0], to: [1, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [1, 0], end: [1, 0], to: [1, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 3] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 4] }",
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }");
            /*
                oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 1] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 6] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 6] }",
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1] }");
            /*
                oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 6], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 6], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 6] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 3] }",
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 6] }");
            /*
                oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 3], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [2, 3] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4] }",
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 2] }");
            /*
                oneOperation = { name: 'move', start: [2, 4], end: [2, 4], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 5] }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 2] }");
            /*
                oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 5], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], target: '123456' }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], target: '123456' }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3] }");
            /*
                oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], target: '123456', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 4], target: '123456', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 5], to: [2, 3] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 5], to: [2, 3] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], to: [2, 3], target: '123456' }",
            "{ name: 'move', start: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 5], to: [2, 3], target: '123456' }");
            /*
                oneOperation = { name: 'move', start: [2, 5], to: [2, 3], target: '123456', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 5], to: [2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 0] }",
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 0] }",
            "[]");
            /*
                oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 0] }",
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 0] }",
            "[]");
            /*
                oneOperation = { name: 'move', start: [2, 5], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 3] }",
            "{ name: 'move', start: [2, 5], to: [2, 0] }",
            "{ name: 'move', start: [2, 3], to: [2, 0] }",
            "[]");
            /*
                oneOperation = { name: 'move', start: [2, 5], end: [2, 5], to: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 3], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'move', start: [2, 5], to: [2, 3], target: '123456' }",
            "{ name: 'move', start: [2, 5], to: [2, 4] }",
            "{ name: 'move', start: [2, 5], to: [2, 4] }",
            "{ name: 'move', start: [2, 5], to: [2, 3], target: '123456' }");
            /*
                oneOperation = { name: 'move', start: [2, 5], to: [2, 3], target: '123456', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], to: [2, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 5], to: [2, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 5], to: [2, 3], target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }
}
