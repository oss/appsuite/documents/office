/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertLayoutSlideMoveLayoutSlide {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 4, oldtarget: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 5, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 4, oldtarget: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 5, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 5, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 1, oldtarget: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 5, target: '123456', oldindex: 1, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 5, target: '123456', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 2, oldtarget: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 3, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 2, oldtarget: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 3, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 1, oldtarget: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 2, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 2, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 0, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 0, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 1, oldtarget: '456789' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 1, oldtarget: '456789' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }");
            /*
    oneOperation = { name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }


    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '234567', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '234567', oldindex: 3, oldtarget: '567890', opl: 1, osn: 1 }], transformedOps);
             */
    }


    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '234567', oldindex: 4, oldtarget: '234567', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 4, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 5, oldtarget: '123456' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 4, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 5, oldtarget: '123456' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 5, oldtarget: '123456' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 5, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 6, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 4, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 1, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 5, target: '123456', oldindex: 1, oldtarget: '123456' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 4, target: '123456', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 5, target: '123456', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 2, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 3, oldtarget: '123456' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 2, oldtarget: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '456789', oldindex: 3, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 1, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 2, oldtarget: '123456' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 1, oldtarget: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 2, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 0, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 0, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '456789', oldindex: 0, oldtarget: '123456', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 2, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 0, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 1, oldtarget: '456789' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 1, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456' }",
            "{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 1, oldtarget: '456789' }");
            /*
    oneOperation = { name: 'moveLayoutSlide', id: '345678', start: 2, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertLayoutSlide', id: '456789', start: 1, target: '123456', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', id: '345678', start: 3, target: '123456', oldindex: 1, oldtarget: '456789', opl: 1, osn: 1 }], transformedOps);
             */
    }
}
