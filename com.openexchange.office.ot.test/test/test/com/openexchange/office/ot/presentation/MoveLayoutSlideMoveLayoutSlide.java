/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.presentation;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveLayoutSlideMoveLayoutSlide {

    @Test
    public void test01() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "[]",
            "[]");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "[]");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "[]");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 1 }",
            "[]");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 5 }",
            "[]");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 5, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 5 }",
            "[]");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 5, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_2', oldindex: 5 }",
            "[]");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_2', oldindex: 5, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }");
            /*
                oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }");
            /*
                oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }];
                transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "[]",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "[]",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "[]",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 1, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1 }",
            "[]");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "[]");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 6, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 6, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 4, id: 'layout_2', target: 'master_1', oldtarget: 'master_1', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_4', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_4', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_4', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_4', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_3', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_3', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 5, id: 'layout_1', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 2, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 4 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 4 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 0, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 4, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 3, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 1, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 3, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transformPresentation(
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1 }",
            "{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3 }",
            "{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4 }",
            "{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1 }");
            /*
    oneOperation = { name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'moveLayoutSlide', start: 2, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManagerPresentation.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'moveLayoutSlide', start: 1, id: 'layout_1', target: 'master_2', oldtarget: 'master_1', oldindex: 4, opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'moveLayoutSlide', start: 3, id: 'layout_2', target: 'master_1', oldtarget: 'master_2', oldindex: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }
}
