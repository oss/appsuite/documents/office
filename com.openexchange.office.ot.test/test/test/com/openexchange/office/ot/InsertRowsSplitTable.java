/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertRowsSplitTable {

    // it('should calculate valid transformed splitTable operation after insertRows operation', function () {
    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1] }",
            "{ name: 'insertRows', start: [2, 1], count: 2, referenceRow: 0 }",
            "{ name: 'insertRows', start: [3, 1], count: 2, referenceRow: 0 }",
            "{ name: 'splitTable', start: [1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 1], count: 2, referenceRow: 0 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]);
                expect(localActions[0].operations[0].referenceRow).to.equal(0);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [1, 1], count: 2, referenceRow: 0 }",
            "{ name: 'insertRows', start: [1, 1], count: 2, referenceRow: 0 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 2, referenceRow: 0 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].referenceRow).to.equal(0);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [2, 0], count: 2, referenceRow: 0 }",
            "{ name: 'insertRows', start: [2, 0], count: 2, referenceRow: 0 }",
            "{ name: 'splitTable', start: [2, 3] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0], count: 2, referenceRow: 0 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
                expect(localActions[0].operations[0].referenceRow).to.equal(0);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'insertRows', start: [2, 6], count: 2, referenceRow: 5 }",
            "{ name: 'insertRows', start: [3, 3], count: 2, referenceRow: 2 }",
            "{ name: 'splitTable', start: [2, 3] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 6], count: 2, referenceRow: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
                expect(localActions[0].operations[0].referenceRow).to.equal(2);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 0 }",
            "{ name: 'insertRows', start: [3, 0], count: 2 }",
            "{ name: 'splitTable', start: [2, 3] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
                expect(localActions[0].operations[0].referenceRow).to.equal(0);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'insertRows', start: [2, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [2, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 5] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1] }",
            "{ name: 'insertRows', start: [1, 0, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [1, 0, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 0, 1, 0, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1, 0, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [2, 5, 1, 0, 1], count: 2, referenceRow: 0 }",
            "{ name: 'insertRows', start: [3, 4, 1, 0, 1], count: 2, referenceRow: 0 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 5, 1, 0, 1], count: 2, referenceRow: 0 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 1, 0, 1]);
                expect(localActions[0].operations[0].referenceRow).to.equal(0);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'insertRows', start: [2, 2, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [3, 0, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2, 1, 0, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1, 0, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 4] }",
            "{ name: 'insertRows', start: [2, 8, 1, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [2, 8, 1, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 8, 1, 6] }");
           /*
                    oneOperation = { name: 'splitTable', start: [2, 8, 1, 4], opl: 1, osn: 1 }; // table in drawing
                    localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 8, 1, 2], count: 2, referenceRow: 1 }] }];
                    otManager.transformOperation(oneOperation, localActions);
                    expect(oneOperation.start).to.deep.equal([2, 8, 1, 6]);
                    expect(localActions[0].operations[0].start).to.deep.equal([2, 8, 1, 2]);
                    expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1] }",
            "{ name: 'insertRows', start: [1, 4], count: 2, referenceRow: 3 }",
            "{ name: 'insertRows', start: [1, 4], count: 2, referenceRow: 3 }",
            "{ name: 'splitTable', start: [2, 8, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], count: 2, referenceRow: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].referenceRow).to.equal(3);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1] }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 8, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }",
            "{ name: 'insertRows', start: [2, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [2, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 10, 1, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 10, 1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }",
            "{ name: 'insertRows', start: [1, 4], count: 2, referenceRow: 3 }",
            "{ name: 'insertRows', start: [1, 4], count: 2, referenceRow: 3 }",
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], count: 2, referenceRow: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].referenceRow).to.equal(3);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 0, 1, 2] }",
            "{ name: 'insertRows', start: [2, 1], count: 2, referenceRow: 0 }",
            "{ name: 'insertRows', start: [2, 1], count: 2, referenceRow: 0 }",
            "{ name: 'splitTable', start: [2, 3, 0, 1, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 2], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 1], count: 2, referenceRow: 0 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3, 0, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].referenceRow).to.equal(0);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 0, 1, 2] }",
            "{ name: 'insertRows', start: [2, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [2, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 1, 0, 1, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 2], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 0, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }",
            "{ name: 'insertRows', start: [2, 0, 2, 0, 1], count: 2, referenceRow: 0 }",
            "{ name: 'insertRows', start: [2, 0, 2, 0, 1], count: 2, referenceRow: 0 }",
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 2], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0, 2, 0, 1], count: 2, referenceRow: 0 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 0, 1]);
                expect(localActions[0].operations[0].referenceRow).to.equal(0);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }",
            "{ name: 'insertRows', start: [2, 0, 2, 1, 4, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [2, 0, 2, 2, 3, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0, 2, 1, 4, 1, 0, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 2, 3, 1, 0, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }",
            "{ name: 'insertRows', start: [2, 0, 2, 1, 0, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [2, 0, 2, 1, 0, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0, 2, 1, 0, 1, 0, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 1, 0, 1, 0, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 1, 1, 1] }",
            "{ name: 'insertRows', start: [2, 0, 2, 1, 2, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [2, 0, 2, 1, 2, 1, 0, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 0, 1, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 0, 1, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 0, 2, 1, 2, 1, 0, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 1, 2, 1, 0, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [4, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [5, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [4, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 2]);
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 0, 1, 1] }",
            "{ name: 'insertRows', start: [4, 2], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [4, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 1, 0, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [4, 2], count: 2, referenceRow: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 0, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 2]); // -> not modified
                expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    // splitTable and local insertRows (handleInsertRowsSplitTable)
    // it('should calculate valid transformed splitTable operation after insertRows operation with special handling of property referenceRow', function () {
    @Test
    public void test01a() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 2 }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 1 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02a() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 1 }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 0 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 0 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03a() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 0 }",
            "{ name: 'insertRows', start: [3, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 0 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2 }] }], localActions);
            expect(localActions[0].operations[0].referenceRow).to.equal(undefined);
            expectOp([{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04a() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 4 }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 3 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
            oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 3 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05a() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 4] }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 2 }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 2 }",
            "{ name: 'splitTable', start: [2, 6] }");
           /*
            oneOperation = { name: 'splitTable', start: [2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }] }], localActions);
            expectOp([{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06a() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 4] }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 4 }",
            "{ name: 'insertRows', start: [2, 3], count: 2 }",
            "{ name: 'splitTable', start: [2, 6] }");
           /*
            oneOperation = { name: 'splitTable', start: [2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2 }] }], localActions);
            expect(localActions[0].operations[0].referenceRow).to.equal(undefined);
            expectOp([{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07a() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 4] }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 5 }",
            "{ name: 'insertRows', start: [2, 3], count: 2 }",
            "{ name: 'splitTable', start: [2, 6] }");
           /*
            oneOperation = { name: 'splitTable', start: [2, 4], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 5 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2 }] }], localActions);
            expect(localActions[0].operations[0].referenceRow).to.equal(undefined);
            expectOp([{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

    // insertRows and local splitTable (handleInsertRowsSplitTable)
    // it('should calculate valid transformed insertRows operation after splitTable operation with special handling of property referenceRow', function () {
    @Test
    public void test01b() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 2 }",
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 1 }], transformedOps);
             */
    }

    @Test
    public void test02b() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 1 }",
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 0 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 1 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 0 }], transformedOps);
             */
    }

    @Test
    public void test03b() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 0 }",
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [3, 2], count: 2 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 0 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2 }], transformedOps);
            expect(transformedOps[0].referenceRow).to.equal(undefined);
             */
    }

    @Test
    public void test04b() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 4 }",
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [3, 2], count: 2, referenceRow: 3 }], transformedOps);
             */
    }

    @Test
    public void test05b() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 2 }",
            "{ name: 'splitTable', start: [2, 4] }",
            "{ name: 'splitTable', start: [2, 6] }",
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 2 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 2 }], transformedOps);
             */
    }

    @Test
    public void test06b() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 4 }",
            "{ name: 'splitTable', start: [2, 4] }",
            "{ name: 'splitTable', start: [2, 6] }",
            "{ name: 'insertRows', start: [2, 3], count: 2 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2 }], transformedOps);
            expect(transformedOps[0].referenceRow).to.equal(undefined);
             */
    }

    @Test
    public void test07b() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 3], count: 2, referenceRow: 5 }",
            "{ name: 'splitTable', start: [2, 4] }",
            "{ name: 'splitTable', start: [2, 6] }",
            "{ name: 'insertRows', start: [2, 3], count: 2 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2, referenceRow: 5 };
            localActions = [{ operations: [{ name: 'splitTable', start: [2, 4], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'splitTable', start: [2, 6], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 3], count: 2 }], transformedOps);
            expect(transformedOps[0].referenceRow).to.equal(undefined);
             */
    }

    // splitTable and local insertCells (handleInsertRowsSplitTable)
    // it('should calculate valid transformed splitTable operation after insertCells operation', function () {
    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1] }",
            "{ name: 'insertCells', start: [2, 1, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 1, 2], count: 2 }",
            "{ name: 'splitTable', start: [1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'insertCells', start: [2, 2, 1], count: 2 }",
            "{ name: 'insertCells', start: [3, 0, 1], count: 2 }",
            "{ name: 'splitTable', start: [2, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 1], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'insertCells', start: [2, 1, 1], count: 2 }",
            "{ name: 'insertCells', start: [2, 1, 1], count: 2 }",
            "{ name: 'splitTable', start: [2, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertCells', start: [2, 1, 1], count: 2 }",
            "{ name: 'insertCells', start: [3, 0, 1], count: 2 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
        oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 2 }] }];
        otManager.transformOperation(oneOperation, localActions);
        expect(oneOperation.start).to.deep.equal([2, 1]);
        expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 3] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'insertCells', start: [2, 3, 3], count: 2 }",
            "{ name: 'insertCells', start: [3, 0, 3], count: 2 }",
            "{ name: 'splitTable', start: [2, 3] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 3, 3], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 3]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1] }",
            "{ name: 'insertCells', start: [1, 0, 1, 0, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 0, 1, 0, 2], count: 2 }",
            "{ name: 'splitTable', start: [1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 0, 1, 0, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1, 0, 2]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1] }",
            "{ name: 'insertCells', start: [1, 1, 1, 0, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 0, 1, 0, 2], count: 2 }",
            "{ name: 'splitTable', start: [1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 1, 0, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 1, 0, 2]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertCells', start: [2, 5, 1, 0, 1, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 4, 1, 0, 1, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 5, 1, 0, 1, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 1, 0, 1, 2]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'insertCells', start: [2, 2, 1, 0, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 0, 1, 0, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 1, 0, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1, 0, 2, 2]);
            */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2, 3, 2, 1] }",
            "{ name: 'insertCells', start: [2, 2, 1], count: 2 }",
            "{ name: 'insertCells', start: [2, 2, 1], count: 2 }",
            "{ name: 'splitTable', start: [2, 2, 5, 2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 1], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 5, 2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2, 3, 2, 1] }",
            "{ name: 'insertCells', start: [2, 2, 3], count: 2 }",
            "{ name: 'insertCells', start: [2, 2, 3], count: 2 }",
            "{ name: 'splitTable', start: [2, 2, 5, 2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 3], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 5, 2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 3]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2, 3, 2, 1] }",
            "{ name: 'insertCells', start: [2, 2, 4], count: 2 }",
            "{ name: 'insertCells', start: [2, 2, 4], count: 2 }",
            "{ name: 'splitTable', start: [2, 2, 3, 2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 4], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 3, 2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 4]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2, 3, 2, 1] }",
            "{ name: 'insertCells', start: [2, 2, 4], count: 2 }",
            "{ name: 'insertCells', start: [2, 2, 4], count: 2 }",
            "{ name: 'splitTable', start: [2, 2, 3, 2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 4], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 3, 2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 4]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2, 3, 2, 1] }",
            "{ name: 'insertCells', start: [1, 2, 1], count: 2 }",
            "{ name: 'insertCells', start: [1, 2, 1], count: 2 }",
            "{ name: 'splitTable', start: [2, 2, 3, 2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 2, 3, 2, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 1], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 3, 2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 1]);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 4] }",
            "{ name: 'insertCells', start: [2, 8, 1, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 8, 1, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 8, 1, 4] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 4], opl: 1, osn: 1 }; // table in drawing
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 8, 1, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 8, 1, 2, 2]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 2] }",
            "{ name: 'insertCells', start: [2, 8, 1, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 8, 2, 0 ,2], count: 2 }",
            "{ name: 'splitTable', start: [2, 8, 1, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 2], opl: 1, osn: 1 }; // table in drawing
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 8, 1, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 8, 2, 0, 2]);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1] }",
            "{ name: 'insertCells', start: [1, 4, 3], count: 2 }",
            "{ name: 'insertCells', start: [1, 4, 3], count: 2 }",
            "{ name: 'splitTable', start: [2, 8, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 3], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 3]);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1] }",
            "{ name: 'insertCells', start: [3, 2, 3], count: 2 }",
            "{ name: 'insertCells', start: [3, 2, 3], count: 2 }",
            "{ name: 'splitTable', start: [2, 8, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [3, 2, 3], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 3]);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2]);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }",
            "{ name: 'insertCells', start: [2, 8, 1], count: 2 }",
            "{ name: 'insertCells', start: [2, 8, 1], count: 2 }",
            "{ name: 'splitTable', start: [2, 8, 3, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 8, 1], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 3, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 8, 1]);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }",
            "{ name: 'insertCells', start: [1, 4, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 4, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 2]);
            */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }",
            "{ name: 'insertCells', start: [3, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 8, 1, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [3, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2]);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 0, 1, 2] }",
            "{ name: 'insertCells', start: [2, 1, 0], count: 2 }",
            "{ name: 'insertCells', start: [2, 1, 0], count: 2 }",
            "{ name: 'splitTable', start: [2, 1, 2, 1, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 2], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 0], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 0]);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 0, 1, 2] }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 1, 0, 1, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 2], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 0, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2]);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }",
            "{ name: 'insertCells', start: [2, 0, 2, 0, 1, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 0, 2, 0, 1, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 2], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 0, 2, 0, 1, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 0, 1, 2]);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }",
            "{ name: 'insertCells', start: [2, 0, 2, 1, 4, 1, 0, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 0, 2, 2, 3, 1, 0, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }");
           /*
                    oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
                    localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 0, 2, 1, 4, 1, 0, 2, 2], count: 2 }] }];
                    otManager.transformOperation(oneOperation, localActions);
                    expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 1]);
                    expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 2, 3, 1, 0, 2, 2]);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }",
            "{ name: 'insertCells', start: [2, 0, 2, 1, 0, 1, 0, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 0, 2, 1, 0, 1, 0, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 0, 2, 1, 0, 1, 0, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 1, 0, 1, 0, 2, 2]);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 1, 1, 1] }",
            "{ name: 'insertCells', start: [2, 0, 2, 1, 2, 1, 0, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [2, 0, 2, 1, 2, 1, 0, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 0, 1, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 0, 1, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 0, 2, 1, 2, 1, 0, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 1, 2, 1, 0, 2, 2]);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'insertCells', start: [4, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [5, 2, 2], count: 2 }",
            "{ name: 'splitTable', start: [2, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [4, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 2, 2]);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 0, 1, 1] }",
            "{ name: 'insertCells', start: [4, 2, 2], count: 6 }",
            "{ name: 'insertCells', start: [4, 2, 2], count: 6 }",
            "{ name: 'splitTable', start: [2, 1, 0, 1, 1] }");
           /*
                oneOperation = { name: 'splitTable', start: [2, 1, 0, 1, 1], opl: 1, osn: 1 }; // table in table
                localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [4, 2, 2], count: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 0, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 2]); // -> not modified
             */
    }
}
