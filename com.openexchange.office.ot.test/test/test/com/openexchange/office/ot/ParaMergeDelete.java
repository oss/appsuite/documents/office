/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ParaMergeDelete {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.paralength).to.equal(9);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]); // the locally saved operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 10 }",
            "{ name: 'delete', start: [1, 22] }",
            "{ name: 'delete', start: [1, 22] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 10 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 22], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 22]); // the locally saved operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [0], paralength: 10 }",
            "{ name: 'delete', start: [1, 12], end: [1, 16] }",
            "{ name: 'delete', start: [0, 22], end: [0, 26] }",
            "{ name: 'mergeParagraph', start: [0], paralength: 10 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 12], end: [1, 16], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 22]); // the locally saved delete operation is modified
                expect(localActions[0].operations[0].end).to.deep.equal([0, 26]); // the locally saved delete operation is modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [0], paralength: 10 }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'mergeParagraph', start: [0], paralength: 10 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved delete operation is modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }",
            "{ name: 'delete', start: [0], end: [1] }",
            "{ name: 'delete', start: [0], end: [1] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 10 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0], end: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0]); // the locally saved delete operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([1]); // the locally saved delete operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }",
            "{ name: 'delete', start: [0, 4], end: [1, 3] }",
            "{ name: 'delete', start: [0, 4], end: [1, 3] }",
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 4]); // the locally saved delete operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]); // the locally saved delete operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }",
            "[{ name: 'delete', start: [0, 4], end: [1, 3] }, { name: 'mergeParagraph', start: [0], paralength: 4 }]",
            "[{ name: 'delete', start: [0, 4], end: [1, 3] }, { name: 'mergeParagraph', start: [0], paralength: 4 }]",
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }");
            /*
                // this test includes a following mergeParagraph operation
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 3], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [0], op2: 1, osn: 1, paralength: 4 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3]); // the external operation is modified
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 4]); // the locally saved delete operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]); // the locally saved delete operation is not modified
                expect(localActions[0].operations[1].start).to.deep.equal([0]);
                expect(localActions[0].operations[1].paralength).to.equal(4);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3, 1, 2, 3], paralength: 10 }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'mergeParagraph', start: [2, 1, 2, 3], paralength: 10 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 3]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // the locally saved delete operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3, 1, 2, 3], paralength: 10 }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 10 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before merge
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]); // the locally saved delete operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }",
            "{ name: 'delete', start: [2] }",
            "[{ name: 'splitParagraph', start: [1, 5], opl: 1, osn: 1 }, { name: 'delete', start: [2] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 5], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }


    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'delete', start: [2], end: [4] }",
            "[{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'delete', start: [2], end: [4, 5] }",
            "[{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [4, 5], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'delete', start: [2], end: [4, 5, 6, 2] }",
            "[{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5, 6, 2] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [4, 5, 6, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5, 6, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3, 1, 2, 3], paralength: 5 }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "[{ name: 'splitParagraph', start: [3, 1, 2, 3, 5], opl: 1, osn: 1 }, { name: 'delete', start: [3, 1, 2, 4] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [3, 1, 2, 3], paralength: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 3, 5], opl: 1, osn: 1 }, { name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the locally saved delete operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 12 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]); // the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.paralength).to.equal(8);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.paralength).to.equal(5);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is inside the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is inside the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is behind the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]); // only paragraph [2] is removed completely
                expect(oneOperation.paralength).to.equal(1); // from the 3 characters in paragraph [3], the first 2 are removed
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has NOT the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is inside the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is inside the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is inside the deleted range in a completely removed paragraph
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1]); // the end position becomes a text position (2 characters are removed)
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is inside the only completely removed paragraph
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0]); // the end position becomes a text position (4 characters are removed)
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]); // the end position becomes a text position (4 characters are removed)
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is inside the first of two completely removed paragraphs
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]); // start and end position are the same
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is inside the second of three completely removed paragraphs
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [2, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is directly behind the deleted range (that goes to the end of the paragraph)
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.paralength).to.equal(1); // from the 5 characters in paragraph [1], all except the first are removed
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has NOT the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // the merge is inside a removed range, but no complete paragraph is removed
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [1, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 4]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps.length).to.equal(1);
                // the external mergeParagraph operation has NOT the marker for ignoring, because a delete operation
                // from [0,1] to [1,1] does not remove any paragraph. So it is wrong to reduce the delete operation
                // and therefore ignore the merge operation -> this merge is still required!
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'delete', start: [2] }",
            "[{ name: 'splitParagraph', start: [1, 3] }, { name: 'delete', start: [2] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'delete', start: [2], end: [3] }",
            "[{ name: 'splitParagraph', start: [1, 3] }, { name: 'delete', start: [2], end: [3] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // this test includes no following mergeParagraph operation
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 6], end: [1, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 14]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation not has the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // this test includes a following local mergeParagraph operation
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [0], op2: 1, osn: 1, paralength: 4 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 14]);
                expect(localActions[0].operations[1].start).to.deep.equal([0]);
                expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(1); // the local operation also got the  marker for ignoring!
                expect(localActions[0].operations.length).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
                // the external mergeParagraph operation got the marker from the mergeParagraph-mergeParagraph handler
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                // this test includes no following mergeParagraph operation, but really removes one paragraph locally, so that merge can be ignored
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [0], paralength: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 6], end: [2, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.paralength).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2], paralength: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [0], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2], paralength: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [0], end: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2], paralength: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2], paralength: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 6, 4], paralength: 4 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 2], end: [1, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]);
                expect(oneOperation.start).to.deep.equal([1, 3, 4]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 6, 4], paralength: 4 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 7], end: [1, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 7]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8]);
                expect(oneOperation.start).to.deep.equal([1, 6, 4]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }");
            /*
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 6, 4], paralength: 4 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [0, 7], end: [0, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
                expect(oneOperation.start).to.deep.equal([1, 6, 4]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test42A() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'delete', start: [1, 4], end: [5, 4] }",
            "{ name: 'delete', start: [1, 4], end: [4, 4] }",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4], end: [5, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1, 4], end: [4, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test42B() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'delete', start: [1, 4], end: [4, 4] }",
            "{ name: 'delete', start: [1, 4], end: [3, 7] }",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4], end: [4, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1, 4], end: [3, 7], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 7] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 17]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 12 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(11); // the length is modified
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 12 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(oneOperation.end).to.deep.equal([1, 14]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(12);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the length is modified
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]); // the position is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]); // the position is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 3]); // the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 18]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 6], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4]); // the position is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5]); // the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
        /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1, paralength: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 8]); // the position is modified
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]); // the locally saved operation is not modified
            expect(localActions[0].operations[0].paralength).to.equal(10); // the length is not modified
         */
    }
    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 12], end: [4, 6] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 22]); // the position is modified
                expect(oneOperation.end).to.deep.equal([3, 6]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 14], end: [2, 18] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 24]); // the position is modified
                expect(oneOperation.end).to.deep.equal([1, 28]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 22 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 14]); // the position is not modified
                expect(oneOperation.end).to.deep.equal([1, 18]); // the position is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved start position of the operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(17); // the length is modified
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 14], end: [2, 18] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 14]); // the position is modified
                expect(oneOperation.end).to.deep.equal([1, 18]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([0]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                // the paragraph is merged directly behind the last deleted position
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 4] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]); // no modification
                expect(oneOperation.end).to.deep.equal([1, 4]); // no modification
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(1); // the length is modified
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "[]",
            "[{ name: 'splitParagraph', start: [1, 3] }, { name: 'delete', start: [2] }]");
            /*
                oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2], end: [3] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "[]",
            "[{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3] }]");
            /*
                oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test58A() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4], end: [5, 4] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "[]",
            "{ name: 'delete', start: [1, 4], end: [4, 4] }");
            /*
                oneOperation = { name: 'delete', start: [1, 4], end: [5, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'delete', start: [1, 4], end: [4, 4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test58B() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4], end: [4, 4] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "[]",
            "{ name: 'delete', start: [1, 4], end: [3, 7] }");
            /*
                oneOperation = { name: 'delete', start: [1, 4], end: [4, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'delete', start: [1, 4], end: [3, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.rowcount).to.equal(9);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8, 6, 4, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 6, 4, 2]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [2], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 22], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 22]); // the locally saved operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 12], end: [1, 16], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 22]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 26]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 2, 0, 0, 6], end: [1, 2, 0, 0, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 12, 0, 0, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 12, 0, 0, 8]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [2, 2, 0, 0, 6], end: [2, 2, 0, 0, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 0, 0, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2, 0, 0, 8]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 9, 0, 0, 6], end: [0, 9, 0, 0, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 9, 0, 0, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 9, 0, 0, 8]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved delete operation is modified
                expect(transformedOps.length).to.equal(1);
             */
    }
    @Test
    public void test68() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [4], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0], end: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0]); // the locally saved delete operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([1]); // the locally saved delete operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test69() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [4], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [1, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 4]); // the locally saved delete operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]); // the locally saved delete operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test70() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 1, 2, 3], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 3]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test71() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 1, 2, 3], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]); // the locally saved delete operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test72() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'delete', start: [2] }",
            "[{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test73() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'delete', start: [2], end: [4] }",
            "[{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test74() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'delete', start: [2], end: [4, 5] }",
            "[{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [4, 5], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test75() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'delete', start: [2], end: [4, 5, 6, 2] }",
            "[{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5, 6 ,2] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [4, 5, 6, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [4, 5, 6, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test76() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3, 1, 2, 3], rowcount: 5 }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "[{ name: 'splitTable', start: [3, 1, 2, 3, 5], opl: 1, osn: 1 }, { name: 'delete', start: [3, 1, 2, 4] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeTable', start: [3, 1, 2, 3], rowcount: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [3, 1, 2, 3, 5], opl: 1, osn: 1 }, { name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test77() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 1, 2, 3], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the locally saved delete operation is not modified
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test78() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 12 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]); // the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.rowcount).to.equal(8);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test79() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(5);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test80() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is inside the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test81() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is inside the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [2], rowcount: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test82() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is behind the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]); // only paragraph [2] is removed completely
                expect(oneOperation.rowcount).to.equal(1); // from the 3 characters in paragraph [3], the first 2 are removed
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has NOT the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test83() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is inside the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test84() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is inside the deleted range (that covers more than one paragraph)
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [2], rowcount: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test85() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is inside the deleted range in a completely removed paragraph
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 2 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1]); // the end position becomes a row position (2 rows are removed)
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test86() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is inside the only completely removed paragraph
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0]); // the end position becomes a text position (4 characters are removed)
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]); // the end position becomes a text position (4 characters are removed)
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test87() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is inside the first of two completely removed paragraphs
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]); // start and end position are the same
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test88() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is inside the second of three completely removed paragraphs
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [2], rowcount: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test89() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [2, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test90() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is directly behind the deleted range (that goes to the end of the paragraph)
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.rowcount).to.equal(1); // from the 5 characters in paragraph [1], all except the first are removed
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has NOT the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test91() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // the merge is inside a removed range, but no complete paragraph is removed
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [1, 1], opl: 1, osn: 1 }] }]; // without merge
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 4]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test92() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'delete', start: [2] }",
            "[{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test93() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'delete', start: [2], end: [3] }",
            "[{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test94() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // this test includes no following mergeTable operation
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 6], end: [1, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 14]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation not has the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test95() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                // this test includes no following mergeTable operation, but really removes one paragraph locally, so that merge can be ignored
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [0], rowcount: 10 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 6], end: [2, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(10);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test96() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 2, 2], rowcount: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [0], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test97() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 2, 2], rowcount: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [0], end: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test98() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 2, 2], rowcount: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test99() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 2, 2], rowcount: 5 };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test100() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 4 };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test101() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 6, 4], rowcount: 4 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 2], end: [1, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]);
                expect(oneOperation.start).to.deep.equal([1, 3, 4]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test102() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 6, 4], rowcount: 4 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 7], end: [1, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 7]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8]);
                expect(oneOperation.start).to.deep.equal([1, 6, 4]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test103() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }");
            /*
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 6, 4], rowcount: 4 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [0, 7], end: [0, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
                expect(oneOperation.start).to.deep.equal([1, 6, 4]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test104() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 17]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test105() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 12 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(11); // the length is modified
             */
    }

    @Test
    public void test106() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 12 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(oneOperation.end).to.deep.equal([1, 14]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(12);
             */
    }

    @Test
    public void test107() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the length is modified
             */
    }

    @Test
    public void test108() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]); // the position is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test109() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]); // the position is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 3]); // the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test110() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 18]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test111() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 6], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4]); // the position is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5]); // the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test112() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 8]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test113() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8, 0, 0, 3] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 8, 0, 0, 3]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test114() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 12], end: [4, 6] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 22]); // the position is modified
                expect(oneOperation.end).to.deep.equal([3, 6]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the length is not modified
             */
    }

    @Test
    public void test115() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 4], end: [2, 5] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 14]); // the position is modified
                expect(oneOperation.end).to.deep.equal([1, 15]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test116() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 8] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 12 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]); // the position is not modified
                expect(oneOperation.end).to.deep.equal([1, 8]); // the position is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the locally saved start position of the operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(7);
             */
    }

    @Test
    public void test117() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 4], end: [2, 8] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]); // the position is modified
                expect(oneOperation.end).to.deep.equal([1, 8]); // the position is modified
                expect(localActions[0].operations[0].start).to.deep.equal([0]); // the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test118() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                // the table is merged directly behind the last deleted position
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 4] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]); // no modification
                expect(oneOperation.end).to.deep.equal([1, 4]); // no modification
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(1);
             */
    }

    @Test
    public void test119() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "[]",
            "[{ name: 'splitTable', start: [1, 3] }, { name: 'delete', start: [2] }]");
            /*
                oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test120() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2], end: [3] }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "[]",
            "[{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3] }]");
            /*
                oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }, { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test121() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2, 1, 0] }",
            "{ name: 'mergeParagraph', start: [2, 1, 0, 4], paralength: 1 }",
            "[]",
            "{ name: 'delete', start: [2, 1, 0] }");
            /*
             */
    }

    @Test
    public void test122() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2, 1, 0, 4], paralength: 1 }",
            "{ name: 'delete', start: [2, 1, 0] }",
            "{ name: 'delete', start: [2, 1, 0] }",
            "[]");
            /*
             */
    }

}
