/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.OCKey;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsParaSplit {

    static final public ImmutableSet<OCKey> ignorables = new ImmutableSet.Builder<OCKey>()
        .add(OCKey.ATTRS)
        .build();

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 10] }",    // local operations
            "{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [1, 8] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 10] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 10] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 20] }",    // local operations
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 2] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 20] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 20]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 30] }",    // local operations
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 30] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 30]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 16]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 8] }",    // local operations
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 8] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 8] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 8]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 30] }",    // local operations
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [0] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 30] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 30]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 40] }",    // local operations
            "{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 40] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 40]);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 40] }",    // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",     // external operations
            "[{ name: 'setAttributes', start: [1] }, { name: 'setAttributes', start: [2] }]",     // expected local
            "{ name: 'splitParagraph', start: [1, 40] }", ignorables);   // expected external
            /*
                // Generating new operation for splitted paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 40]);
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations[1].start).to.deep.equal([2]);
                expect(localActions[0].operations[1].end).to.equal(undefined);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 40] }",    // local operations
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [0], end: [2] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 40] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 40]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 40] }",    // local operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [0], end: [3] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 40] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 40]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([3]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 1, 2, 2] }",    // local operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [0], end: [2] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 1, 2, 2] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1, 2, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 40] }",    // local operations
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2, 1, 2, 2], end: [3, 2, 1, 2, 6] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 40] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 40]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 2, 6]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 50] }",    // local operations
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 50] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 50] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 50]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",    // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [4] }",     // expected local
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",    // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 0] }",     // expected local
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, before split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",    // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 5] }",     // expected local
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, but behind split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",    // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [4] }",     // expected local
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",    // local operations
            "{ name: 'setAttributes', start: [1, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 2] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 2] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",    // local operations
            "{ name: 'setAttributes', start: [1, 4, 3], end: [1, 4, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 2, 3], end: [2, 2, 4] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 2] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3], end: [1, 4, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 4]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",    // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 2, 3, 4], end: [2, 2, 3, 6] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 2] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 3, 6]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 4] }",    // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 0, 3, 4], end: [2, 0, 3, 6] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 4] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 0, 3, 6]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5] }",    // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 5] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 3, 6]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 0] }",    // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6] }",     // expected local
            "{ name: 'splitParagraph', start: [2, 0] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 0] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 3, 6]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 2] }",    // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 4, 3, 4], end: [2, 4, 3, 6] }",     // expected local
            "{ name: 'splitParagraph', start: [0, 2] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 3, 6]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 5] }",    // local operations
            "{ name: 'setAttributes', start: [0, 1], end: [0, 12], attrs: { character: { italic: true } } }",     // external operations
            "[{ name: 'setAttributes', start: [0, 1], end: [0, 4] }, { name: 'setAttributes', start: [1, 0], end: [1, 7] }]",     // expected local
            "{ name: 'splitParagraph', start: [0, 5] }", ignorables);   // expected external
            /*
                // local change inside one paragraph and an external splitParagraph in this paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [0, 1], end: [0, 12], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([0, 4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation generated
                generatedOperation = localActions[0].operations[1];
                expect(generatedOperation.name).to.equal('setAttributes');
                expect(generatedOperation.start).to.deep.equal([1, 0]);
                expect(generatedOperation.end).to.deep.equal([1, 7]);
                expect(oneOperation.start).to.deep.equal([0, 5]); // external operation is not modified
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 3] }",    // local operations
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3], end: [4] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 3] }", ignorables);   // expected external
            /*
                // local change inside one paragraph and a following paragraph completely and an external splitParagraph in the completely the removed paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1, 3]); // external operation is not modified
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 3] }",    // local operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [1], end: [5] }",     // expected local
            "{ name: 'splitParagraph', start: [2, 3] }", ignorables);   // expected external
            /*
                // local change inside one paragraph and several following paragraphs completely and an external splitParagraph in the middle of the completely removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 3]); // external operation is not modified
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 3] }",    // local operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [1], end: [4] }",     // expected local
            "{ name: 'splitParagraph', start: [1, 3] }", ignorables);   // expected external
            /*
                // local change of several paragraphs completely and an external splitParagraph in the first of the completely removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1, 3]); // external operation is not modified
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 3] }",    // local operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [1], end: [4] }",     // expected local
            "{ name: 'splitParagraph', start: [3, 3] }", ignorables);   // expected external
            /*
                // local change of several paragraphs completely and an external splitParagraph in the last of the completely removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([3, 3]); // external operation is not modified
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 1, 4, 5, 3] }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8] }",     // expected local
            "{ name: 'splitParagraph', start: [2, 1, 4, 5, 3] }", ignorables);   // expected external
            /*
                // local change of several paragraphs and an external splitParagraph inside the table
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 4, 5, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]); // the end position of the locally saved operation is also NOT modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 1, 4, 5, 3]); // external operation is not modified
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",    // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",     // external operations
            "[{ name: 'setAttributes', start: [1] }, { name: 'setAttributes', start: [2] }]",     // expected local
            "{ name: 'splitParagraph', start: [1, 2] }", ignorables);   // expected external
            /*
                // local change of one complete paragraph and an external splitParagraph inside this paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations[1].start).to.deep.equal([2]);
                expect(localActions[0].operations[1].end).to.equal(undefined);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 2] }",    // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [1] }",     // expected local
            "{ name: 'splitParagraph', start: [2, 2] }", ignorables);   // expected external
            /*
                // local change of one complete paragraph and an external splitParagraph inside the following paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.equal(undefined); // the end position of the locally saved operation is not generated
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 2]); // the start position of the external operation is modified
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 2] }",    // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2] }",     // expected local
            "{ name: 'splitParagraph', start: [0, 2] }", ignorables);   // expected external
            /*
                // local change of one complete paragraph and an external splitParagraph inside the previous paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].end).to.equal(undefined); // the end position of the locally saved operation is not generated
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([0, 2]); // the start position of the external operation is not modified
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 10] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 10] }",     // expected local
            "{ name: 'setAttributes', start: [1, 8] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 20] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 20] }",     // expected local
            "{ name: 'setAttributes', start: [2, 2] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 20]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 30] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 30] }",     // expected local
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 12]);
                expect(oneOperation.end).to.deep.equal([1, 16]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 30]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 8] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 8] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 8] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 30] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 30] }",     // expected local
            "{ name: 'setAttributes', start: [0] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 30]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // expected local
            "{ name: 'setAttributes', start: [3] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 40]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // expected local
            "[{ name: 'setAttributes', start: [1] }, { name: 'setAttributes', start: [2] }]", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 40]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(2); // an additional external operation
                expect(transformedOps[0]).to.deep.equal(oneOperation);
                expect(transformedOps[1].name).to.equal('setAttributes');
                expect(transformedOps[1].start).to.deep.equal([2]);
                expect(transformedOps[1].end).to.equal(undefined);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // expected local
            "{ name: 'setAttributes', start: [0], end: [2] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.end).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 40]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // expected local
            "{ name: 'setAttributes', start: [0], end: [3] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.end).to.deep.equal([3]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 40]);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 1, 2, 2] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 1, 2, 2] }",     // expected local
            "{ name: 'setAttributes', start: [0], end: [2] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1, 2, 2] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.end).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 2, 2]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 40] }",     // expected local
            "{ name: 'setAttributes', start: [3, 2, 1, 2, 2], end: [3, 2, 1, 2, 6] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 2, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([3, 2, 1, 2, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 40]);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 50] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 50] }",     // expected local
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 50] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 3]);
                expect(oneOperation.end).to.deep.equal([0, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 50]);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",     // external operations
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",     // expected local
            "{ name: 'setAttributes', start: [4] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",     // external operations
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",     // expected local
            "{ name: 'setAttributes', start: [3, 1, 2, 0] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] }] }]; // changing paragraph in same cell, before split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 0]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",     // external operations
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",     // expected local
            "{ name: 'setAttributes', start: [3, 1, 2, 5] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] }] }]; // changing paragraph in same cell, but behind split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",     // external operations
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",     // expected local
            "{ name: 'setAttributes', start: [4] }", ignorables);   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1], end: [0, 12], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [0, 5] }",     // external operations
            "{ name: 'splitParagraph', start: [0, 5] }",     // expected local
            "[{ name: 'setAttributes', start: [0, 1], end: [0, 4] }, { name: 'setAttributes', start: [1, 0], end: [1, 7] }]", ignorables);   // expected external
            /*
                // external change inside one paragraph and local splitParagraph in this paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1], end: [0, 12], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [0, 5] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 5]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations.length).to.equal(1); // there is no new local operation generated
                expect(oneOperation.start).to.deep.equal([0, 1]);
                expect(oneOperation.end).to.deep.equal([0, 4]);
                expect(transformedOps.length).to.equal(2); // an additional external operation
                expect(transformedOps[0]).to.deep.equal(oneOperation);
                expect(transformedOps[1].name).to.equal('setAttributes');
                expect(transformedOps[1].start).to.deep.equal([1, 0]);
                expect(transformedOps[1].end).to.deep.equal([1, 7]);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 3] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 3] }",     // expected local
            "{ name: 'setAttributes', start: [3], end: [4] }", ignorables);   // expected external
            /*
                // external change inside one paragraph and a following paragraph completely and an intenal splitParagraph in the completely the removed paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [3], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [2, 3] }",     // external operations
            "{ name: 'splitParagraph', start: [2, 3] }",     // expected local
            "{ name: 'setAttributes', start: [1], end: [5] }", ignorables);   // expected external
            /*
                // external change inside one paragraph and several following paragraphs completely and an internal splitParagraph in the middle of the completely removed paragraphs
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [2, 3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.end).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 3] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 3] }",     // expected local
            "{ name: 'setAttributes', start: [1], end: [4] }", ignorables);   // expected external
            /*
                // external change of several paragraphs completely and an internal splitParagraph in the first of the completely removed paragraphs
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [3, 3] }",     // external operations
            "{ name: 'splitParagraph', start: [3, 3] }",     // expected local
            "{ name: 'setAttributes', start: [1], end: [4] }", ignorables);   // expected external
            /*
                // external change of several paragraphs completely and an internal splitParagraph in the last of the completely removed paragraphs
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [3, 3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [2, 1, 4, 5, 3] }",     // external operations
            "{ name: 'splitParagraph', start: [2, 1, 4, 5, 3] }",     // expected local
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8] }", ignorables);   // expected external
            /*
                // external change of several paragraphs and an internal splitParagraph inside the table
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 4, 5, 3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 4, 5, 3]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([3, 4]);
                expect(oneOperation.end).to.deep.equal([3, 8]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [1, 2] }",     // external operations
            "{ name: 'splitParagraph', start: [1, 2] }",     // expected local
            "[{ name: 'setAttributes', start: [1] },"
            +"{ name: 'setAttributes', start: [2] }]", ignorables);   // expected external
            /*
                // external change of one complete paragraph and an internal splitParagraph inside this paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(2); // an additional external operation
                expect(transformedOps[0]).to.deep.equal(oneOperation);
                expect(transformedOps[1].name).to.equal('setAttributes');
                expect(transformedOps[1].start).to.deep.equal([2]);
                expect(transformedOps[1].end).to.equal(undefined);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [2, 2] }",     // external operations
            "{ name: 'splitParagraph', start: [2, 2] }",     // expected local
            "{ name: 'setAttributes', start: [1] }", ignorables);   // expected external
            /*
                // local change of one complete paragraph and an external splitParagraph inside the following paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.end).to.equal(undefined); // not generated
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",    // local operations
            "{ name: 'splitParagraph', start: [0, 2] }",     // external operations
            "{ name: 'splitParagraph', start: [0, 2] }",     // expected local
            "{ name: 'setAttributes', start: [2] }", ignorables);   // expected external
            /*
                // local change of one complete paragraph and an external splitParagraph inside the previous paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.end).to.equal(undefined); // not generated
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } } }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } } }",
            "{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } } }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0] }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'setAttributes', start: [0] }");
            /*
                oneOperation = { name: 'setAttributes', start: [0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [0], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'setAttributes', start: [0] }",
            "{ name: 'setAttributes', start: [0] }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [0], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1, 0] }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "[{ name: 'setAttributes', start: [0, 1, 0] }, { name: 'setAttributes', start: [0, 1, 1] }]");
            /*
                oneOperation = { name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }, { name: 'setAttributes', start: [0, 1, 1], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'setAttributes', start: [0, 1, 0] }",
            "[{ name: 'setAttributes', start: [0, 1, 0] }, { name: 'setAttributes', start: [0, 1, 1] }]",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }, { name: 'setAttributes', start: [0, 1, 1], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1, 1] }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'setAttributes', start: [0, 1, 2] }");
            /*
                oneOperation = { name: 'setAttributes', start: [0, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [0, 1, 2], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }",
            "{ name: 'setAttributes', start: [0, 1, 1] }",
            "{ name: 'setAttributes', start: [0, 1, 2] }",
            "{ name: 'splitParagraph', start: [0, 1, 0, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitParagraph', start: [0, 1, 0, 2], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1, 0] }",
            "{ name: 'splitParagraph', start: [0, 1, 1, 2] }",
            "{ name: 'splitParagraph', start: [0, 1, 1, 2] }",
            "{ name: 'setAttributes', start: [0, 1, 0] }");
            /*
                oneOperation = { name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 1, 1, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [0, 1, 1, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 1, 1, 2] }",
            "{ name: 'setAttributes', start: [0, 1, 0] }",
            "{ name: 'setAttributes', start: [0, 1, 0] }",
            "{ name: 'splitParagraph', start: [0, 1, 1, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 1, 1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1, 0], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitParagraph', start: [0, 1, 1, 2], opl: 1, osn: 1 }], transformedOps);
            */
    }
}
