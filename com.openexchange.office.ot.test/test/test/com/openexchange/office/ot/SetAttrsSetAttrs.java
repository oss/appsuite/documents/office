/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsSetAttrs {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 7], end: [3, 7], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 7], end: [3, 7], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes is in front of the local setAttributes in the same paragraph (no ranges)
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 7], end: [3, 7], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 7]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 7]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }


    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 7], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 7], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes is in front of the local setAttributes in the same paragraph (with ranges)
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 7], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 7]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 9]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 6], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 2], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 2], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 6], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes is behind the local setAttributes in the same paragraph (no ranges)
                oneOperation = { name: 'setAttributes', start: [3, 6], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 2], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 6]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes is in a paragraph before the local setAttributes
                oneOperation = { name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [1, 6], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [1, 6], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes is in a paragraph behind the local setAttributes over several paragraphs
                oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 6], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([5, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([5, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes is in a paragraph behind the local setAttributes over several complete paragraphs
                oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([5, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([5, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [1, 6], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [1, 6], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes is inside the last paragraph behind the local setAttributes over several paragraphs
                oneOperation = { name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 6], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([5]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [8], end: [9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 7], end: [6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 7], end: [6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [8], end: [9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes changes several paragraphs behind the local setAttributes
                oneOperation = { name: 'setAttributes', start: [8], end: [9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7], end: [6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([8]); // not modified
                expect(oneOperation.end).to.deep.equal([9]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes changes several top level paragraphs before the local setAttributes inside a table
                oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 2, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes changes content inside a table behind the local change of paragraphs before the table
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2, 1]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 2, 6]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table in front of the local changed paragraphs in the same table cell
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 4, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 4, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 3]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table behind the local changed paragraphs in the same table cell
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 4, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 4, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 6, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 6, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 4, 1, 2], end: [4, 7, 4, 1, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [4, 7, 4, 1, 2], end: [4, 7, 4, 1, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table behind the local changed paragraphs in another cell
                oneOperation = { name: 'setAttributes', start: [4, 7, 4, 1, 2], end: [4, 7, 4, 1, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 3, 4, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 3, 4, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 4, 1, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 4, 1, 5]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table behind the local changed paragraphs in the same table cell
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 3]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 6, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 6, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "[]");
            /*
                // concurrent attributes
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // external setAttributes operation got marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // local setAttributes operation does not get marker for ignoring
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // external setAttributes operation does not get marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], end: [4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "[]");
            /*
                // concurrent attributes
                oneOperation = { name: 'setAttributes', start: [4], end: [4], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // external setAttributes operation got marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], end: [4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [4], end: [4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                oneOperation = { name: 'setAttributes', start: [4], end: [4], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // local setAttributes operation does not get marker for ignoring
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // not modified
                expect(oneOperation.start).to.deep.equal([4]); // not modified
                expect(oneOperation.end).to.deep.equal([4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // external setAttributes operation does not get marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 3, 2, 2, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 3, 2, 2, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "[]");
            /*
                // concurrent attributes
                oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 3, 2, 2, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 3, 2, 2, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // external setAttributes operation got marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 3, 2, 2, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 3, 2, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [4, 3, 2, 2, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 3, 2, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // local setAttributes operation does not get marker for ignoring
                expect(localActions[0].operations[0].start).to.deep.equal([4, 3, 2, 2, 4]); // not modified
                expect(oneOperation.start).to.deep.equal([4, 3, 2, 2, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 3, 2, 2, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // external setAttributes operation does not get marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "[]");
            /*
                // concurrent attributes
                oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 3, 2, 2]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // external setAttributes operation got marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // local setAttributes operation does not get marker for ignoring
                expect(localActions[0].operations[0].start).to.deep.equal([4, 3, 2, 2]); // not modified
                expect(oneOperation.start).to.deep.equal([4, 3, 2, 2]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // external setAttributes operation does not get marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "[]");
            /*
                // concurrent attributes
                // the external setAttributes is surrounded by the local setAttributes in the same paragraph
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external setAttributes operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the external setAttributes is surrounded by the local setAttributes in the same paragraph
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character:  { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation does not get marker for ignoring
                expect(transformedOps.length).to.equal(1); // no additional external operations
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 5], end: [3, 7], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 5], end: [3, 7], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "[{ name: 'setAttributes', start: [3, 3], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } },"
            +"{ name: 'setAttributes', start: [3, 8], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }]");
            /*
                // concurrent attributes
                // the external setAttributes surrounds the local setAttributes in the same paragraph at both sides -> a new external operation is required
                oneOperation = { name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 5], end: [3, 7], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 7]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0]).to.deep.equal(oneOperation);
                expect(transformedOps[0].start).to.deep.equal([3, 3]); // modified
                expect(transformedOps[0].end).to.deep.equal([3, 4]); // modified
                expect(transformedOps[1].name).to.equal('setAttributes');
                expect(transformedOps[1].start).to.deep.equal([3, 8]);
                expect(transformedOps[1].end).to.deep.equal([3, 9]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 5], end: [3, 7], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 5], end: [3, 7], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the external setAttributes surrounds the local setAttributes in the same paragraph
                oneOperation = { name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 5], end: [3, 7], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 7]); // not modified
                expect(oneOperation.start).to.deep.equal([3, 3]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 9]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 5], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 5], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 3], end: [3, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes surrounds the local setAttributes in the same paragraph only at the beginning -> no new external operation required
                oneOperation = { name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 5], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 9]); // not modified
                expect(oneOperation.start).to.deep.equal([3, 3]); // modified
                expect(oneOperation.end).to.deep.equal([3, 4]); // modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 7], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 7], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 8], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes surrounds the local setAttributes in the same paragraph only at the end -> no new external operation required
                oneOperation = { name: 'setAttributes', start: [3, 3], end: [3, 9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 7], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 7]); // not modified
                expect(oneOperation.start).to.deep.equal([3, 8]); // modified
                expect(oneOperation.end).to.deep.equal([3, 9]); // modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes surrounds the local setAttributes because it is an ancestor node
                // If a text attribute is set at the paragraph [3], the more specific setting of a concurrent attribute
                // from [3,2] to [3,6] will overwrite it -> no further operation required.
                oneOperation = { name: 'setAttributes', start: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // not modified
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.equal(undefined); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // local setAttributes operation does not got marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes surrounds the local setAttributes because it is an ancestor node
                oneOperation = { name: 'setAttributes', start: [3], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // not modified
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes surrounds the local setAttributes because its final paragraph is an ancestor node
                oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes surrounds the local setAttributes because its final table is an ancestor node
                oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 2, 2, 0]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 2, 2, 6]); // not modified
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes is surrounded by the local setAttributes because several paragraphs are changed
                // -> not removing the more specific external operation
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes is surrounded by the local setAttributes because several paragraphs are changed
                // -> not removing the more specific external operation
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes is surrounded by the local setAttributes because several paragraphs are changed
                // -> not removing the more specific external operation
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the external setAttributes inside a table is surrounded by the local setAttributes because several paragraphs are removed
                // -> not removing the more specific external operation
                oneOperation = { name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // setAttributes operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "[]");
            /*
                // concurrent attributes
                // the external setAttributes of paragraphs is surrounded by the local setAttributes because several paragraphs are removed
                oneOperation = { name: 'setAttributes', start: [3], end: [4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external setAttributes operation has the marker for ignoring
                expect(transformedOps.length).to.equal(0); // no operation returned
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3], end: [4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the external setAttributes of paragraphs is surrounded by the local setAttributes because several paragraphs are removed
                oneOperation = { name: 'setAttributes', start: [3], end: [4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.deep.equal([4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 5, 2, 1], end: [3, 5, 2, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 5, 2, 1], end: [3, 5, 2, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // the external setAttributes surrounds the local setAttributes because that is an ancestor table
                // -> no modification required, because local operation is more specific in position
                oneOperation = { name: 'setAttributes', start: [3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 5, 2, 1], end: [3, 5, 2, 4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 2, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5, 2, 4]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // local setAttributes operation does not get marker for ignoring
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.equal(undefined); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 6], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the two ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 6]); // modified, only one character must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 6]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 5], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the two ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 6]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 6], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the two ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 6]); // modified, only one character must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 6]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the two ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 6]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [0, 1], end: [0, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [0, 1], end: [0, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [0, 6], end: [0, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the two ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], end: [0, 5], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([0, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([0, 6]); // modified, only three characters must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([0, 8]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [0, 1], end: [0, 5], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [0, 1], end: [0, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the two ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], end: [0, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([0, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([0, 3]); // not modified
                expect(oneOperation.end).to.deep.equal([0, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], end: [9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [5], end: [9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the two ranges overlap on paragraph level (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [4], end: [9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([5]); // modified, 5 instead of 6 paragraphs must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([9]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], end: [9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [4], end: [9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the two ranges overlap on paragraph level (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [4], end: [9], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4]); // not modified
                expect(oneOperation.end).to.deep.equal([9]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 2], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the two ranges overlap each other inside the same paragraph (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 2]); // modified, only one character must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 2]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the two ranges overlap each other inside the same paragraph (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2, 1, 1, 3, 4], end: [2, 1, 1, 3, 10], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 1, 1, 3, 4], end: [2, 1, 1, 3, 10], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 3], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the two ranges overlap each other inside the same paragraph in a table cell (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 1, 3, 4], end: [2, 1, 1, 3, 10], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 3, 10]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 3, 1]); // modified, only three characters must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([2, 1, 1, 3, 3]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2, 1, 1, 3, 4], end: [2, 1, 1, 3, 10], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 1, 1, 3, 4], end: [2, 1, 1, 3, 10], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the two ranges overlap each other inside the same paragraph in a table cell (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 5], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 1, 3, 4], end: [2, 1, 1, 3, 10], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 3, 10]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 3, 1]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 1, 1, 3, 5]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 1, 1], end: [2, 1, 1, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2, 1, 1, 3], end: [2, 1, 1, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 1, 1, 3], end: [2, 1, 1, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } } }",
            "{ name: 'setAttributes', start: [2, 1, 1, 1], end: [2, 1, 1, 2], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // concurrent attributes
                // the two ranges overlap on paragraph level in a table cell (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [2, 1, 1, 1], end: [2, 1, 1, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 1, 3], end: [2, 1, 1, 8], attrs: { character: { color: { type: 'rgb', value: '00FF00' } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 1]); // modified, 2 instead of 4 paragraphs must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([2, 1, 1, 2]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 1, 1], end: [2, 1, 1, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }",    // local operations
            "{ name: 'setAttributes', start: [2, 1, 1, 3], end: [2, 1, 1, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [2, 1, 1, 3], end: [2, 1, 1, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [2, 1, 1, 1], end: [2, 1, 1, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } } }");
            /*
                // non concurrent attributes
                // the two ranges overlap on paragraph level in a table cell (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [2, 1, 1, 1], end: [2, 1, 1, 4], attrs: { character: { color: { type: 'rgb', value: 'FF0000' } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 1, 3], end: [2, 1, 1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 1]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 1, 1, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } } }",
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [2, 2], end: [2, 5], attrs: { character: { italic: false } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } } }",
        "{ name: 'setAttributes', start: [2, 2], end: [2, 5], attrs: { character: { italic: false } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [2, 2], end: [2, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [2, 2], end: [2, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: false } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } } }",
        "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: false } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 1], end: [3, 5], target: \"cmt0\", attrs: { character: { italic: false } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } } }",
        "{ name: 'setAttributes', start: [3, 1], end: [3, 5], target: \"cmt0\", attrs: { character: { italic: false } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: false } }, target: 'cmt0', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 0], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: false } }, target: 'cmt0', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } } }",
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: false } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } } }",
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true, bold: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true, bold: true } } }",
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { italic: true, bold: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 3], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } } }",
        "[]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, underline: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: false }, family2: { propA: true } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true } } }",
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { family2: { propA: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { bold: false }, family2: { propA: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { family2: { propA: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false, color: { propA: true } } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } } }",
        "[]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false, color: { propA: true } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false, color: { propA: true } }, family2: { propA: true } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } } }",
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { family2: { propA: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false, color: { propA: true } }, family2: { propA: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: false } } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { family2: { propA: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false, color: { propA: true } } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: true } } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: true } } } }",
        "{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: { propA: true } } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false, color: { propA: true } } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: true } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true, bold: true, color: { propA: true } } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { color: { propA: true } } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
        "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true, italic: true } } }",    // local operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
        "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",
        "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true, italic: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { bold: true, italic: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { bold: true, italic: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test68() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",
            "[]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
              */
    }

    @Test
    public void test69() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true, italic: false } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test70() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false }, family2: { propA: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { family2: { propA: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false }, family2: { propA: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { family2: { propA: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test71() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: false } } }",    // local operations
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: false } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test72() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test73() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test74() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false, bold: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } } },"
            +"{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } } },"
            +"{ name: 'setAttributes', start: [3, 7], end: [3, 8], attrs: { character: { italic: false, bold: true } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 7], end: [3, 8], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test75() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false } } },"
            +"{ name: 'setAttributes', start: [3, 7], end: [3, 8], attrs: { character: { italic: false } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 7], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test76() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test77() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 5], end: [3, 8], attrs: { character: { italic: false } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 5], end: [3, 8], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test78() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false }, family2: { propA: 1 } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { family2: { propA: 1 } } },"
            +"{ name: 'setAttributes', start: [3, 5], end: [3, 8], attrs: { character: { italic: false }, family2: { propA: 1 } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false }, family2: { propA: 1 } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { family2: { propA: 1 } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 5], end: [3, 8], attrs: { character: { italic: false }, family2: { propA: 1 } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test79() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false }, family2: { propA: 1 } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false }, family2: { propA: 1 } } },"
            +"{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { family2: { propA: 1 } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: false }, family2: { propA: 1 } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: false }, family2: { propA: 1 } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { family2: { propA: 1 } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test80() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { bold: true, italic: false } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { bold: true, italic: false } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3], end: [3], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { bold: true, italic: false } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test81() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { bold: true, italic: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { bold: true, italic: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { bold: true, italic: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test82() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { italic: false, bold: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } } },"
            +"{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test83() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: false, bold: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } } },"
            +"{ name: 'setAttributes', start: [3, 7], end: [3, 9], attrs: { character: { italic: false, bold: true } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 9], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { bold: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 7], end: [3, 9], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test84() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { bold: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { bold: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test85() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: false, bold: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } } },"
            +"{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { bold: true } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test85A() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true, bold: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } } }",
            "[{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: true, bold: true } } },"
            +"{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { bold: true } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test86() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test87() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false, bold: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { bold: true } } },"
            +"{ name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { italic: false, bold: true } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { bold: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test88() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false, bold: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false, bold: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false, bold: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test89() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { italic: false } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { italic: false } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test90() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false }, family2: { propA: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { family2: { propA: true } } },"
            +"{ name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { italic: false }, family2: { propA: true } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: false }, family2: { propA: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { family2: { propA: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { italic: false }, family2: { propA: true } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test91() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 2 } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 2 } } } }",
            "[{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { family2: { propA: true } } },"
            +"{ name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 2 } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 2 } } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { family2: { propA: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

    @Test
    public void test92() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } } }");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1 } }, family2: { propA: true } }, opl: 1, osn: 1 }], transformedOps);
              */
    }

    @Test
    public void test93() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1, propC: 2 } }, family2: { propA: true } } }",    // local operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } } }",     // external operations
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } } }",
            "[{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { family2: { propA: true } } },"
            +"{ name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { propA: { propB: 1, propC: 2 } }, family2: { propA: true } } }]");
              /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { propA: { propB: 1, propC: 2 } }, family2: { propA: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { propA: { propB: 1 } } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([
                    { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { family2: { propA: true } }, opl: 1, osn: 1 },
                    { name: 'setAttributes', start: [3, 5], end: [3, 6], attrs: { character: { propA: { propB: 1, propC: 2 } }, family2: { propA: true } }, opl: 1, osn: 1 }
                ], transformedOps);
              */
    }

}

