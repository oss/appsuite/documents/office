/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsSplitTable {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 10] }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 8], attrs: { row: { height: 2000 } } }",                                       // external operations
            "{ name: 'setAttributes', start: [1, 8], attrs: { row: { height: 2000 } } }",                                       // expected local
            "{ name: 'splitTable', start: [1, 10] }");                                                                          // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 10] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { row: { height: 2000 } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 10] }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 8, 2, 2, 2], attrs: { character: { italic: true } } }",                        // external operations
            "{ name: 'setAttributes', start: [1, 8, 2, 2, 2], attrs: { character: { italic: true } } }",                        // expected local
            "{ name: 'splitTable', start: [1, 10] }");                                                                          // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 10] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 2, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 2, 2, 2]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 20] }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 22], attrs: { row: { height: 2000 } } }",                                      // external operations
            "{ name: 'setAttributes', start: [2, 2], attrs: { row: { height: 2000 } } }",                                       // expected local
            "{ name: 'splitTable', start: [1, 20] }");                                                                          // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { row: { height: 2000 } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 20]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 20] }",                                                                           // local operations
            "{ name: 'setAttributes', start: [1, 22, 4, 2, 2], attrs: { character: { italic: true } } }",                       // external operations
            "{ name: 'setAttributes', start: [2, 2, 4, 2, 2], attrs: { character: { italic: true } } }",                        // expected local
            "{ name: 'splitTable', start: [1, 20] }");                                                                          // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22, 4, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 20]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 4, 2, 2]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 20] }",                                                                           // local operations
            "{ name: 'setAttributes', start: [2, 2], attrs: { character: { italic: true } } }",                                 // external operations
            "{ name: 'setAttributes', start: [3, 2], attrs: { character: { italic: true } } }",                                 // expected local
            "{ name: 'splitTable', start: [1, 20] }");                                                                          // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 20]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 20] }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0, 2], attrs: { character: { italic: true } } }",                                 // external operations
            "{ name: 'setAttributes', start: [0, 2], attrs: { character: { italic: true } } }",                                 // expected local
            "{ name: 'splitTable', start: [1, 20] }");                                                                          // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 20]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 20] }",                                                                           // local operations
            "{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }",                                    // external operations
            "{ name: 'setAttributes', start: [3], attrs: { character: { italic: true } } }",                                    // expected local
            "{ name: 'splitTable', start: [1, 20] }");                                                                          // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 20]);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }",                                                                   // local operations
            "{ name: 'setAttributes', start: [2, 0, 2, 2, 0], attrs: { character: { italic: true } } }",                        // external operations
            "{ name: 'setAttributes', start: [2, 0, 2, 3, 0], attrs: { character: { italic: true } } }",                        // expected local
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }");                                                                  // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 0, 2, 1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 0, 2, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 3, 0]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }",                                                                   // local operations
            "{ name: 'setAttributes', start: [2, 0, 2, 2, 0, 3, 2, 1], attrs: { character: { italic: true } } }",               // external operations
            "{ name: 'setAttributes', start: [2, 0, 2, 3, 0, 3, 2, 1], attrs: { character: { italic: true } } }",               // expected local
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }");                                                                  // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 0, 2, 1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 0, 2, 2, 0, 3, 2, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 3, 0, 3, 2, 1]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }",                                                                   // local operations
            "{ name: 'setAttributes', start: [2, 1, 2, 2, 0], attrs: { character: { italic: true } } }",                        // external operations
            "{ name: 'setAttributes', start: [2, 1, 2, 2, 0], attrs: { character: { italic: true } } }",                        // expected local
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }");                                                                  // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 0, 2, 1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 1, 2, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 2, 0]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 30] }",                                                                           // local operations
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",                                    // external operations
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",                                    // expected local
            "{ name: 'splitTable', start: [1, 30] }");                                                                          // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 30] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 30]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 40] }",                                                                           // local operations
            "{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }",                                    // external operations
            "{ name: 'setAttributes', start: [3], attrs: { character: { italic: true } } }",                                    // expected local
            "{ name: 'splitTable', start: [1, 40] }");                                                                          // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 40] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 40]);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                    // external operations
            "[{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } },"
            +"{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }]",                                  // expected local
            "{ name: 'splitTable', start: [1, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations[1].start).to.deep.equal([2]);
                expect(localActions[0].operations[1].end).to.equal(undefined);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1], end: [1], attrs: { character: { italic: true } } }",                          // external operations
            "[{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } },"
            +"{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }]",                                  // expected local
            "{ name: 'splitTable', start: [1, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
                expect(localActions[0].operations[1].start).to.deep.equal([2]);
                expect(localActions[0].operations[1].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 4] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",                          // external operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                          // expected local
            "{ name: 'splitTable', start: [1, 4] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 4] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                          // external operations
            "{ name: 'setAttributes', start: [0], end: [3], attrs: { character: { italic: true } } }",                          // expected local
            "{ name: 'splitTable', start: [1, 4] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([3]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1, 2, 2] }",                                                                      // local operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                          // external operations
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",                          // expected local
            "{ name: 'splitTable', start: [1, 1, 2, 2] }");                                                                     // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 1, 2, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 4] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",  // external operations
            "{ name: 'setAttributes', start: [3, 2, 1, 2, 2], end: [3, 2, 1, 2, 6], attrs: { character: { italic: true } } }",  // expected local
            "{ name: 'splitTable', start: [1, 4] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 2, 6]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",                    // external operations
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",                    // expected local
            "{ name: 'splitTable', start: [1, 5] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 1, 2, 3 , 4] }",                                                                  // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                    // external operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                    // expected local
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }");                                                                  // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",                                                                   // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } } }",                           // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } } }",                           // expected local
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }");                                                                  // expected external,
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, before split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",                                                                   // local operations
            "{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } } }",                           // external operations
            "{ name: 'setAttributes', start: [3, 1, 2, 5], attrs: { character: { italic: true } } }",                           // expected local
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }");                                                                  // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, but behind split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",                                                                   // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                    // external operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                    // expected local
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }");                                                                  // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",                                    // external operations
            "{ name: 'setAttributes', start: [5], attrs: { character: { italic: true } } }",                                    // expected local
            "{ name: 'splitTable', start: [1, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1, 4], attrs: { character: { italic: true } } }",                                 // external operations
            "{ name: 'setAttributes', start: [2, 2], attrs: { character: { italic: true } } }",                                 // expected local
            "{ name: 'splitTable', start: [1, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1, 4, 3], end: [1, 4, 4], attrs: { character: { italic: true } } }",              // external operations
            "{ name: 'setAttributes', start: [2, 2, 3], end: [2, 2, 4], attrs: { character: { italic: true } } }",              // expected local
            "{ name: 'splitTable', start: [1, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3], end: [1, 4, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 4]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",        // external operations
            "{ name: 'setAttributes', start: [2, 2, 3, 4], end: [2, 2, 3, 6], attrs: { character: { italic: true } } }",        // expected local
            "{ name: 'splitTable', start: [1, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 3, 6]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 4] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",        // external operations
            "{ name: 'setAttributes', start: [2, 0, 3, 4], end: [2, 0, 3, 6], attrs: { character: { italic: true } } }",        // expected local
            "{ name: 'splitTable', start: [1, 4] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 0, 3, 6]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",        // external operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",        // expected local
            "{ name: 'splitTable', start: [1, 5] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 3, 6]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",        // external operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",        // expected local
            "{ name: 'splitTable', start: [2, 0] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 0] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 3, 6]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } } }",        // external operations
            "{ name: 'setAttributes', start: [2, 4, 3, 4], end: [2, 4, 3, 6], attrs: { character: { italic: true } } }",        // expected local
            "{ name: 'splitTable', start: [0, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 4, 3, 4], end: [1, 4, 3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 3, 6]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [0, 1], end: [0, 7], attrs: { row: { height: 2000 } } }",                          // external operations
            "{ name: 'setAttributes', start: [0, 1], end: [1, 2], attrs: { row: { height: 2000 } } }",                          // expected local
            "{ name: 'splitTable', start: [0, 5] }");                                                                           // expected external
            /*
                // -> this is not handled correctly. But there is no setAttributes operation covering several rows
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [0, 1], end: [0, 7], attrs: { row: { height: 2000 } } }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 3] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }",                          // external operations
            "{ name: 'setAttributes', start: [3], end: [4], attrs: { character: { italic: true } } }",                          // expected local
            "{ name: 'splitTable', start: [1, 3] }");                                                                           // expected external
            /*
                // -> this is not handled correctly. But there is no setAttributes operation covering several tables/paragraphs
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 3] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",                          // external operations
            "{ name: 'setAttributes', start: [1], end: [5], attrs: { character: { italic: true } } }",                          // expected local
            "{ name: 'splitTable', start: [2, 3] }");                                                                           // expected external
            /*
                // -> this is not handled correctly. But there is no setAttributes operation covering several tables/paragraphs
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 3]); // external operation is not modified
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 3] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                          // external operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",                          // expected local
            "{ name: 'splitTable', start: [1, 3] }");                                                                           // expected external
            /*
                // -> this is not lhandled correctly. But there is no setAttributes operation covering several tables/paragraphs
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1, 3]); // external operation is not modified
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 3] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",                          // external operations
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",                          // expected local
            "{ name: 'splitTable', start: [3, 3] }");                                                                           // expected external
            /*
                // -> this is not handled correctly. But there is no setAttributes operation covering several tables/paragraphs
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([3, 3]); // external operation is not modified
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 4, 5, 3] }",                                                                   // local operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",                    // external operations
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",                    // expected local
            "{ name: 'splitTable', start: [2, 1, 4, 5, 3] }");                                                                  // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1, 4, 5, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]);
                expect(oneOperation.start).to.deep.equal([2, 1, 4, 5, 3]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                    // external operations
            "[{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } },"
            +"{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }]",                                  // expected local
            "{ name: 'splitTable', start: [1, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[1].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(2);
                expect(oneOperation.start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                    // external operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                    // expected local
            "{ name: 'splitTable', start: [2, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(oneOperation.start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 2] }",                                                                            // local operations
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                    // external operations
            "{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }",                                    // expected local
            "{ name: 'splitTable', start: [0, 2] }");                                                                           // expected external
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 2]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",                                    // local operations
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // external operations
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // expected local
            "[{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } },"
            +"{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }]");                                 // expected external
            /*
                // testing, that external operation is added correctly (making setAttributes to external operation)
                oneOperation = { name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0]).to.deep.equal(oneOperation);
                expect(transformedOps[1].name).to.equal('setAttributes');
                expect(transformedOps[1].start).to.deep.equal([2]); // generated by the splitTable operation
                expect(transformedOps[1].end).to.equal(undefined);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], end: [1], attrs: { character: { italic: true } } }",                          // local operations
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // external operations
            "{ name: 'splitTable', start: [1, 2] }",                                                                            // expected local
            "[{ name: 'setAttributes', start: [1], end: [1], attrs: { character: { italic: true } } },"
            +"{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }]");                       // expected external
            /*
                // testing, that external operation is added correctly (making setAttributes to external operation)
                oneOperation = { name: 'setAttributes', start: [1], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0]).to.deep.equal(oneOperation);
                expect(transformedOps[1].name).to.equal('setAttributes');
                expect(transformedOps[1].start).to.deep.equal([2]); // generated by the splitTable operation
                expect(transformedOps[1].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc' }",
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'updateField', start: [2, 2, 0, 1, 1], representation: 'abc' }");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' };
                localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }], localActions);
                expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], representation: 'abc' }], transformedOps);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc' }",
            "{ name: 'splitTable', start: [1, 6] }",
            "{ name: 'splitTable', start: [1, 6] }",
            "{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc' }");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' };
                localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }], localActions);
                expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }], transformedOps);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc' }",
            "{ name: 'splitTable', start: [1, 4] }",
            "{ name: 'splitTable', start: [1, 4] }",
            "{ name: 'updateField', start: [2, 0, 0, 1, 1], representation: 'abc' }");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' };
                localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }] }], localActions);
                expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [2, 0, 0, 1, 1], representation: 'abc' }], transformedOps);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc' }",
            "{ name: 'updateField', start: [2, 2, 0, 1, 1], representation: 'abc' }",
            "{ name: 'splitTable', start: [1, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], representation: 'abc' }] }], localActions);
                expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }], transformedOps);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 6] }",
            "{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc' }",
            "{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc' }",
            "{ name: 'splitTable', start: [1, 6] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 6] };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }] }], localActions);
                expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }], transformedOps);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 4] }",
            "{ name: 'updateField', start: [1, 4, 0, 1, 1], representation: 'abc' }",
            "{ name: 'updateField', start: [2, 0, 0, 1, 1], representation: 'abc' }",
            "{ name: 'splitTable', start: [1, 4] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], representation: 'abc' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [2, 0, 0, 1, 1], representation: 'abc' }] }], localActions);
                expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }], transformedOps);
             */
    }


    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'splitTable', start: [2, 2] }",
            "[{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }, { name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } } }]");
            /*
                oneOperation = { name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', start: [2, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [2, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }, { name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [3, 2] }",
            "{ name: 'splitTable', start: [3, 2] }",
            "{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', start: [3, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [3, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 1, 1, 1, 2] }",
            "{ name: 'splitTable', start: [1, 1, 1, 1, 2] }",
            "{ name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 1, 1, 2, 2] }",
            "{ name: 'splitTable', start: [1, 1, 1, 2, 2] }",
            "[{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }, { name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } } }]");
            /*
                oneOperation = { name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 1, 1, 2, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 1, 1, 2, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }, { name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 1, 1, 3, 2] }",
            "{ name: 'splitTable', start: [1, 1, 1, 3, 2] }",
            "{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 1, 1, 3, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 1, 1, 3, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 1, 1, 1, 2] }",
            "{ name: 'splitTable', start: [1, 1, 1, 1, 2] }",
            "{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 0, 1, 1, 2] }",
            "{ name: 'splitTable', start: [1, 0, 1, 1, 2] }",
            "{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 0, 1, 1, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitTable', start: [1, 0, 1, 1, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 2] }");
            /*
                oneOperation = { name: 'splitTable', start: [1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [1, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }, { name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } } }]",
            "{ name: 'splitTable', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitTable', start: [2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }, { name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [2, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 2] }",
            "{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [3, 2] }");
            /*
                oneOperation = { name: 'splitTable', start: [3, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [2], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1, 1, 1, 2] }",
            "{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 1, 1, 1, 2] }");
            /*
                oneOperation = { name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1, 1, 2, 2] }",
            "{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }",
            "[{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }, { name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } } }]",
            "{ name: 'splitTable', start: [1, 1, 1, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', start: [1, 1, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }, { name: 'setAttributes', start: [1, 1, 1, 3], end: [1, 1, 1, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [1, 1, 1, 2, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1, 1, 3, 2] }",
            "{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 1, 1, 3, 2] }");
            /*
                oneOperation = { name: 'splitTable', start: [1, 1, 1, 3, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1, 2], end: [1, 1, 1, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [1, 1, 1, 3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1, 1, 1, 2] }",
            "{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 1, 1, 1, 2] }");
            /*
                oneOperation = { name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [1, 1, 1, 1, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 0, 1, 1, 2] }",
            "{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } } }",
            "{ name: 'splitTable', start: [1, 0, 1, 1, 2] }");
            /*
                oneOperation = { name: 'splitTable', start: [1, 0, 1, 1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [1, 1, 1], end: [1, 1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitTable', start: [1, 0, 1, 1, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }
}

