/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertCharInsertChar {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2], text: 'o' }",             // local operations
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",           // expected local
            "{ name: 'insertText', start: [1, 5], text: 'o' }");           // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 2], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [1, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]); // not modified
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2], text: 'o' }",             // local operations
            "{ name: 'insertText', start: [1, 5], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [1, 6], text: 'ppp' }",           // expected local
            "{ name: 'insertText', start: [1, 2], text: 'o' }");            // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 2], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [1, 5], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]); // not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // modified
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2], text: 'o' }",             // local operations
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",           // expected local
            "{ name: 'insertText', start: [1, 2], text: 'o' }");            // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 2], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [0, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]); // not modified
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // not modified
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2], text: 'o' }",             // local operations
            "{ name: 'insertText', start: [2, 1], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [2, 1], text: 'ppp' }",           // expected local
            "{ name: 'insertText', start: [1, 2], text: 'o' }");            // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 2], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [2, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]); // not modified
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1]); // not modified
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 4], text: 'o' }",             // local operations
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [1, 5], text: 'ppp' }",           // expected local
            "{ name: 'insertText', start: [1, 4], text: 'o' }");            // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 4], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [1, 4], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]); // but the locally saved operation is modified
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [0, 1, 2, 2, 2], text: 'o' }",    // local operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",  // external operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 4], text: 'ppp' }",  // expected local
            "{ name: 'insertText', start: [0, 1, 2, 2, 2], text: 'o' }");   // expected external
            /*
                oneOperation = { name: 'insertText', start: [0, 1, 2, 2, 2], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1, 2, 2, 4]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [0, 1, 2, 2, 4], text: 'o' }",    // local operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",  // external operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",  // expected local
            "{ name: 'insertText', start: [0, 1, 2, 2, 7], text: 'o' }");   // expected external
            /*
                oneOperation = { name: 'insertText', start: [0, 1, 2, 2, 4], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1, 2, 2, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1, 2, 2, 3]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [0, 1, 2, 3, 4], text: 'o' }",    // local operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",  // external operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",  // expected local
            "{ name: 'insertText', start: [0, 1, 2, 3, 4], text: 'o' }");   // expected external
            /*
                oneOperation = { name: 'insertText', start: [0, 1, 2, 3, 4], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1, 2, 2, 3]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [0, 1, 1, 2, 4], text: 'o' }",    // local operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",  // external operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",  // expected local
            "{ name: 'insertText', start: [0, 1, 1, 2, 4], text: 'o' }");   // expected external
            /*
                oneOperation = { name: 'insertText', start: [0, 1, 1, 2, 4], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1, 1, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1, 2, 2, 3]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2], text: 'o' }",             // local operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",  // external operations
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",  // expected local
            "{ name: 'insertText', start: [1, 2], text: 'o' }");            // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 2], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]); // not modified
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1, 2, 2, 3]); // not modified
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2, 2, 2, 3], text: 'o' }",    // local operations
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",           // expected local
            "{ name: 'insertText', start: [1, 2, 2, 2, 3], text: 'o' }");   // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 2, 2, 2, 3], text: 'o' };
                localActions = [{ operations: [{ name: 'insertText', start: [0, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2, 3]); // not modified
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // not modified
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 6, 1, 1], text: 'o' }",       // local operations
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",           // expected local
            "{ name: 'insertText', start: [1, 9, 1, 1], text: 'o' }");      // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 6, 1, 1], text: 'o' }; // inside a text frame
                localActions = [{ operations: [{ name: 'insertText', start: [1, 4], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 9, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2, 1, 1], text: 'o' }",       // local operations
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",           // expected local
            "{ name: 'insertText', start: [1, 2, 1, 1], text: 'o' }");      // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 2, 1, 1], text: 'o' }; // inside a text frame
                localActions = [{ operations: [{ name: 'insertText', start: [1, 4], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 6, 1, 1], text: 'o' }",       // local operations
            "{ name: 'insertText', start: [0, 4], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [0, 4], text: 'ppp' }",           // expected local
            "{ name: 'insertText', start: [1, 6, 1, 1], text: 'o' }");      // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 6, 1, 1], text: 'o' }; // inside a text frame
                localActions = [{ operations: [{ name: 'insertText', start: [0, 4], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 4]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 6, 1, 1], text: 'o' }",       // local operations
            "{ name: 'insertText', start: [1, 4, 1, 0], text: 'ppp' }",     // external operations
            "{ name: 'insertText', start: [1, 4, 1, 0], text: 'ppp' }",     // expected local
            "{ name: 'insertText', start: [1, 6, 1, 1], text: 'o' }");      // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 6, 1, 1], text: 'o' }; // inside a text frame
                localActions = [{ operations: [{ name: 'insertText', start: [1, 4, 1, 0], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 1, 0]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 6, 1, 1], text: 'o' }",       // local operations
            "{ name: 'insertText', start: [1, 6, 1, 0], text: 'ppp' }",     // external operations
            "{ name: 'insertText', start: [1, 6, 1, 0], text: 'ppp' }",     // expected local
            "{ name: 'insertText', start: [1, 6, 1, 4], text: 'o' }");      // expected external
            /*
                oneOperation = { name: 'insertText', start: [1, 6, 1, 1], text: 'o' }; // inside a text frame
                localActions = [{ operations: [{ name: 'insertText', start: [1, 6, 1, 0], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6, 1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6, 1, 0]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertDrawing', start: [1, 6] }",                     // local operations
            "{ name: 'insertText', start: [1, 2], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [1, 2], text: 'ppp' }",           // expected local
            "{ name: 'insertDrawing', start: [1, 9] }");                    // expected external
            /*
                oneOperation = { name: 'insertDrawing', start: [1, 6] };
                localActions = [{ operations: [{ name: 'insertText', start: [1, 2], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 9]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertDrawing', start: [1, 6] }",                     // local operations
            "{ name: 'insertText', start: [1, 8], text: 'ppp' }",           // external operations
            "{ name: 'insertText', start: [1, 9], text: 'ppp' }",           // expected local
            "{ name: 'insertDrawing', start: [1, 6] }");                    // expected external
            /*
                oneOperation = { name: 'insertDrawing', start: [1, 6] };
                localActions = [{ operations: [{ name: 'insertText', start: [1, 8], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 9]);
             */
    }
}
