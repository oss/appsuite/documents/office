/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertRowsDelete {

    // it('should calculate valid transformed insertRows operation after local delete operation', function () {
    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 3 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 11] }",
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 2]);
            expect(oneOperation.referenceRow).to.equal(1);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 11]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'insertRows', start: [2, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([2, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].end).to.deep.equal([1]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 3 }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'insertRows', start: [1, 5], referenceRow: 4, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 5], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 5]);
            expect(oneOperation.referenceRow).to.equal(4);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'insertRows', start: [0, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([0, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
            expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertRows', start: [2, 1, 2, 3, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([2, 1, 2, 3, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'insertRows', start: [3, 1, 2, 2, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before insertRows
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertRows', start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind insert
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 3 }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 5], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 5]);
            expect(oneOperation.referenceRow).to.equal(4);
            expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 5], referenceRow: 4, count: 3 }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertRows', start: [3, 1, 2, 3, 5], referenceRow: 4, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 2, 3, 5], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 5]);
            expect(oneOperation.referenceRow).to.equal(4);
            expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 1, 3, 3], referenceRow: 2, count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 3, 4, 6] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 7, 6] }",
            "{ name: 'insertRows', start: [3, 1, 1, 3, 3], referenceRow: 2, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 1, 3, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 6], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 3]);
            expect(oneOperation.referenceRow).to.equal(2);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 7, 6]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 1, 3, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertRows', start: [3, 1, 1, 3, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 1, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 1, 3, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [3, 1, 2, 1] }",
            "{ name: 'delete', start: [3, 1, 2, 1] }",
            "{ name: 'insertRows', start: [3, 1, 1, 3, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1, 1, 3, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [1, 8] }",
            "{ name: 'delete', start: [0, 1], end: [1, 11] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 8] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
            expect(localActions[0].operations[0].end).to.deep.equal([1, 11]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 3 }",
            "[{ name: 'delete', start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[{ name: 'delete', start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
            expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
            expect(localActions[0].operations.length).to.equal(2);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 1], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
            expect(localActions[0].operations[0].end).to.deep.equal([2]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 3 }",
            "{ name: 'delete', start: [1, 2], end: [1, 4] }",
            "{ name: 'delete', start: [1, 5], end: [1, 7] }",
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 2]);
            expect(oneOperation.referenceRow).to.equal(1);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
            expect(localActions[0].operations[0].end).to.deep.equal([1, 7]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [1] }",
            "{ name: 'delete', start: [0, 1], end: [1] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
            expect(localActions[0].operations[0].end).to.deep.equal([1]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
            expect(localActions[0].operations[0].end).to.deep.equal([4]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1, 1, 4, 2], referenceRow: 1, count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 1, 4, 2], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1, 1, 5, 3], referenceRow: 2, count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'insertRows', start: [2, 1, 1, 2, 3], referenceRow: 2, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 1, 5, 3], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([2, 1, 1, 2, 3]);
            expect(oneOperation.referenceRow).to.equal(2);
            expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2]);
            expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1, 1, 2, 1], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1], end: [2, 1, 1, 4, 2] }",
            "{ name: 'delete', start: [2, 1, 1, 2, 4], end: [2, 1, 1, 4, 2] }",
            "{ name: 'insertRows', start: [2, 1, 1, 2, 1], referenceRow: 0, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], end: [2, 1, 1, 4, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([2, 1, 1, 2, 1]);
            expect(oneOperation.referenceRow).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 4]);
            expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4, 2]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1, 1, 2, 1], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }",
            "{ name: 'delete', start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }",
            "{ name: 'insertRows', start: [2, 1, 1, 2, 1], referenceRow: 0, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([2, 1, 1, 2, 1]);
            expect(oneOperation.referenceRow).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 2, 1]);
            expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 2, 4, 2]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 1], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [2, 3] }",
            "{ name: 'delete', start: [1, 4], end: [2, 3] }",
            "{ name: 'insertRows', start: [1, 1], referenceRow: 0, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [2, 3] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 1]);
            expect(oneOperation.referenceRow).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
            expect(localActions[0].operations[0].end).to.deep.equal([2, 3]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [3, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].end).to.deep.equal([3]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [1], end: [2, 5] }",
            "{ name: 'delete', start: [1], end: [2, 8] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 5] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].end).to.deep.equal([2, 8]);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1, 4, 5], referenceRow: 4, count: 3 }",
            "[{ name: 'delete', start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', start: [1], paralength: 4 }]",
            "[{ name: 'delete', start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', start: [1], paralength: 4 }]",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 1, 4, 5], referenceRow: 4, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
            expect(localActions[0].operations[0].end).to.deep.equal([3, 8]);
            expect(localActions[0].operations.length).to.equal(2);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 1], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].end).to.equal(undefined);
            expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].end).to.equal(undefined);
            expect(localActions[0].operations.length).to.equal(1);
            expect(oneOperation.start).to.deep.equal([1, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [0, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertRows', start: [0, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [0, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].end).to.equal(undefined);
            expect(localActions[0].operations.length).to.equal(1);
            expect(oneOperation.start).to.deep.equal([0, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 7, 2, 2], referenceRow: 1, count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'insertRows', start: [1, 5, 2, 2], referenceRow: 1, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 7, 2, 2], referenceRow: 1, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
            expect(oneOperation.referenceRow).to.equal(1);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
            expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3, 2, 2], referenceRow: 1, count: 3 }",
            "{ name: 'delete', start: [1, 5], end: [1, 7] }",
            "{ name: 'delete', start: [1, 5], end: [1, 7] }",
            "{ name: 'insertRows', start: [1, 3, 2, 2], referenceRow: 1, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 3, 2, 2], referenceRow: 1, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 5], end: [1, 7], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 3, 2, 2]);
            expect(oneOperation.referenceRow).to.equal(1);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
            expect(localActions[0].operations[0].end).to.deep.equal([1, 7]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2, 2, 2], referenceRow: 1, count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 2, 2], referenceRow: 1, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
            expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 2, 2, 2], referenceRow: 1, count: 3 }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 2, 2], referenceRow: 1, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expect(transformedOps.length).to.equal(0);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 7, 2, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'insertRows', start: [1, 7, 2, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 7, 2, 4], referenceRow: 3, count: 3 }; // in a text frame
            localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 7, 2, 4]);
            expect(oneOperation.referenceRow).to.equal(3);
            expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
            expect(localActions[0].operations[0].end).to.deep.equal([0, 2]);
             */
    }

    // it('should calculate valid transformed insertRows operation after local delete operation with special handling of the reference row', function () {
    @Test
    public void test01a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 1] }",
            "{ name: 'delete', start: [1, 1] }",
            "{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 }], transformedOps);
             */
    }

    @Test
    public void test02a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 1], count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }], transformedOps);
            expect(transformedOps[0].referenceRow).to.equal(undefined); // the reference row is deleted
             */
    }

    @Test
    public void test03a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 2, count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 1], count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 2, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }], transformedOps);
            expect(transformedOps[0].referenceRow).to.equal(undefined); // the reference row is deleted
             */
    }

    @Test
    public void test04a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 1, count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 1], count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 1, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }], transformedOps);
            expect(transformedOps[0].referenceRow).to.equal(undefined); // the reference row is deleted
             */
    }

    @Test
    public void test05a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 1], referenceRow: 0, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 0, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 }], transformedOps); // the reference row is not affected
             */
    }

    @Test
    public void test06a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3] }",
            "{ name: 'delete', start: [1, 7, 2], end: [1, 7, 3] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 7, 2], end: [1, 7, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }], transformedOps);
             */
    }

    @Test
    public void test07a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3] }",
            "{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }], transformedOps);
             */
    }

    @Test
    public void test08a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 4, 2, 0, 0], end: [1, 4, 2, 0, 5] }",
            "{ name: 'delete', start: [1, 7, 2, 0, 0], end: [1, 7, 2, 0, 5] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 2, 0, 0], end: [1, 4, 2, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 7, 2, 0, 0], end: [1, 7, 2, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }], transformedOps);
             */
    }

    @Test
    public void test09a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5] }",
            "{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }], transformedOps);
             */
    }

    @Test
    public void test10a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3] }",
            "{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3] }",
            "{ name: 'insertRows', start: [1, 4, 4], referenceRow: 3, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 4], referenceRow: 3, count: 3 }], transformedOps);
             */
    }

    @Test
    public void test11a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2] }",
            "{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2] }",
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }], transformedOps);
             */
    }

    @Test
    public void test12a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'delete', start: [1, 4, 8], end: [1, 4, 9] }",
            "{ name: 'delete', start: [1, 4, 11], end: [1, 4, 12] }",
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 4, 8], end: [1, 4, 9], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 4, 11], end: [1, 4, 12], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }], transformedOps);
             */
    }

    @Test
    public void test13a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'delete', start: [1, 2], end: [1, 3] }",
            "{ name: 'delete', start: [1, 2], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 2, 6], referenceRow: 5, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 };
            localActions = [{ operations: [{ name: 'delete', start: [1, 2], end: [1, 3], opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'delete', start: [1, 2], end: [1, 3], opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 6], referenceRow: 5, count: 3 }], transformedOps);
             */
    }

    // it('should calculate valid transformed delete operation after local insertRows operation', function () {
    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10], end: [1, 11] }",
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 2 }",
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 2 }",
            "{ name: 'delete', start: [1, 12], end: [1, 13] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [1, 11] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 12]);
            expect(oneOperation.end).to.deep.equal([1, 13]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
            expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2 }",
            "{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 2 }",
            "{ name: 'delete', start: [1, 1] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 1]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 3]);
            expect(localActions[0].operations[0].referenceRow).to.equal(2);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2 }",
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 2 }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 1]);
            expect(oneOperation.end).to.deep.equal([1, 2]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
            expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertRows', start: [2, 4], referenceRow: 3, count: 2 }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2 }",
            "{ name: 'delete', start: [1] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertRows', start: [2, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
            expect(localActions[0].operations[0].referenceRow).to.equal(3);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3, 2, 8] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2 }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2 }",
            "{ name: 'delete', start: [1, 3, 2, 8] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 2, 8] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 3, 2, 8]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
            expect(localActions[0].operations[0].referenceRow).to.equal(3);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3] }",
            "{ name: 'insertRows', start: [1, 4, 1, 1, 4], referenceRow: 3, count: 2 }",
            "{ name: 'insertRows', start: [1, 3, 1, 1, 4], referenceRow: 3, count: 2 }",
            "{ name: 'delete', start: [1, 3] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4, 1, 1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 3]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 1, 1, 4]);
            expect(localActions[0].operations[0].referenceRow).to.equal(3);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3] }",
            "{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 2 }",
            "{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 2 }",
            "{ name: 'delete', start: [1, 5] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 5]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 3]);
            expect(localActions[0].operations[0].referenceRow).to.equal(2);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 2 }",
            "{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 2 }",
            "{ name: 'delete', start: [2] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([2]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 3]);
            expect(localActions[0].operations[0].referenceRow).to.equal(2);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertRows', start: [2, 1, 1, 1, 4], referenceRow: 3, count: 2 }",
            "{ name: 'insertRows', start: [1, 1, 1, 1, 4], referenceRow: 3, count: 2 }",
            "{ name: 'delete', start: [1] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertRows', start: [2, 1, 1, 1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 1, 1, 4]);
            expect(localActions[0].operations[0].referenceRow).to.equal(3);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertRows', start: [3, 1, 2, 3], referenceRow: 2, count: 2 }",
            "{ name: 'insertRows', start: [3, 1, 2, 3], referenceRow: 2, count: 2 }",
            "{ name: 'delete', start: [4] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([4]);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
            expect(localActions[0].operations[0].referenceRow).to.equal(2);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 8] }",
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], referenceRow: 1, count: 2 }",
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], referenceRow: 1, count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 3, 10] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 3, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 10]);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 2] }",
            "{ name: 'insertRows', start: [3, 1, 2, 2, 2], referenceRow: 1, count: 2 }",
            "{ name: 'insertRows', start: [3, 1, 2, 2, 2], referenceRow: 1, count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 3, 2] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 2, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2, 2]);
            expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 1, 2] }",
            "{ name: 'insertRows', start: [3, 1, 2, 2, 2], referenceRow: 1, count: 2 }",
            "{ name: 'insertRows', start: [3, 1, 2, 2, 2], referenceRow: 1, count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 1, 2] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 1, 2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 2, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 2, 1, 2]);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2, 2]);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertRows', start: [3, 1, 2, 5, 2], referenceRow: 1, count: 2 }",
            "{ name: 'insertRows', start: [3, 1, 2, 4, 2], referenceRow: 1, count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 4] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 5, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 2, 4]);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4, 2]);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }",
            "{ name: 'insertRows', start: [3, 1, 2, 3, 4], referenceRow: 3, count: 2 }",
            "{ name: 'insertRows', start: [3, 1, 2, 3, 4], referenceRow: 3, count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 2, 3, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([3, 1, 2, 4, 8]);
            expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
            expect(localActions[0].operations[0].referenceRow).to.equal(3);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10], end: [3, 6] }",
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 2 }",
            "{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 2 }",
            "{ name: 'delete', start: [1, 12], end: [3, 6] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 12]);
            expect(oneOperation.end).to.deep.equal([3, 6]);
            expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
            expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertRows', start: [5, 3], referenceRow: 2, count: 2 }",
            "{ name: 'insertRows', start: [4, 3], referenceRow: 2, count: 2 }",
            "{ name: 'delete', start: [1] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
            localActions = [{ operations: [{ name: 'insertRows', start: [5, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1]);
            expect(localActions[0].operations[0].start).to.deep.equal([4, 3]);
            expect(localActions[0].operations[0].referenceRow).to.equal(2);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'insertRows', start: [5, 3], referenceRow: 2, count: 2 }",
            "{ name: 'insertRows', start: [2, 3], referenceRow: 2, count: 2 }",
            "{ name: 'delete', start: [1], end: [3] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
            localActions = [{ operations: [{ name: 'insertRows', start: [5, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1]);
            expect(oneOperation.end).to.deep.equal([3]);
            expect(localActions[0].operations[0].start).to.deep.equal([2, 3]);
            expect(localActions[0].operations[0].referenceRow).to.equal(2);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 2], end: [1, 6] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2 }",
            "[]",
            "{ name: 'delete', start: [1, 2], end: [1, 8] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 6] };
            localActions = [{ operations: [{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 2]);
            expect(oneOperation.end).to.deep.equal([1, 8]);
            expect(localActions[0].operations.length).to.equal(0);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertRows', start: [2, 3], referenceRow: 2, count: 2 }",
            "[]",
            "{ name: 'delete', start: [2] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
            localActions = [{ operations: [{ name: 'insertRows', start: [2, 3], referenceRow: 2, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([2]);
            expect(localActions[0].operations.length).to.equal(0);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [3, 18] }",
            "{ name: 'insertRows', start: [0, 2], referenceRow: 1, count: 2 }",
            "{ name: 'insertRows', start: [0, 2], referenceRow: 1, count: 2 }",
            "{ name: 'delete', start: [1, 14], end: [3, 18] }");
           /*
            oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [3, 18] };
            localActions = [{ operations: [{ name: 'insertRows', start: [0, 2], referenceRow: 1, count: 2, opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 14]);
            expect(oneOperation.end).to.deep.equal([3, 18]);
            expect(localActions[0].operations[0].start).to.deep.equal([0, 2]);
            expect(localActions[0].operations[0].referenceRow).to.equal(1);
             */
    }

    // it('should calculate valid transformed delete operation after local insertRows operation with special handling of the reference row', function () {
    @Test
    public void test39a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'insertRows', start: [1, 3], referenceRow: 2, count: 3 }",
            "{ name: 'delete', start: [1, 1] }");
           /*
            oneOperation = { name: 'delete', start: [1, 1], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 3], referenceRow: 2, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test40a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'insertRows', start: [1, 1], count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }");
           /*
            oneOperation = { name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }], transformedOps);
            expect(localActions[0].operations[0].referenceRow).to.equal(undefined); // the reference row is deleted
             */
    }

    @Test
    public void test41a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 2, count: 3 }",
            "{ name: 'insertRows', start: [1, 1], count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }");
           /*
            oneOperation = { name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 2, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }], transformedOps);
            expect(localActions[0].operations[0].referenceRow).to.equal(undefined); // the reference row is deleted
             */
    }

    @Test
    public void test42a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 1, count: 3 }",
            "{ name: 'insertRows', start: [1, 1], count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }");
           /*
            oneOperation = { name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 1, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }], transformedOps);
            expect(localActions[0].operations[0].referenceRow).to.equal(undefined); // the reference row is deleted
             */
    }

    @Test
    public void test43a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 0, count: 3 }",
            "{ name: 'insertRows', start: [1, 1], referenceRow: 0, count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }");
           /*
            oneOperation = { name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 0, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 1], referenceRow: 0, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }], transformedOps); // the reference row is not affected
             */
    }

    @Test
    public void test44a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 7, 2], end: [1, 7, 3] }");
           /*
            oneOperation = { name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 7, 2], end: [1, 7, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test45a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3] }");
           /*
            oneOperation = { name: 'delete', start: [1, 3, 2], end: [1, 3, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 3, 2], end: [1, 3, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test46a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4, 2, 0, 0], end: [1, 4, 2, 0, 5] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 7, 2, 0, 0], end: [1, 7, 2, 0, 5] }");
           /*
            oneOperation = { name: 'delete', start: [1, 4, 2, 0, 0], end: [1, 4, 2, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 7, 2, 0, 0], end: [1, 7, 2, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test47a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5] }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'insertRows', start: [1, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5] }");
           /*
            oneOperation = { name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 3, 2, 0, 0], end: [1, 3, 2, 0, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test48a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3] }",
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'insertRows', start: [1, 4, 4], referenceRow: 3, count: 3 }",
            "{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3] }");
           /*
            oneOperation = { name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 4], referenceRow: 3, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 4, 2], end: [1, 4, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test49a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2] }",
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2] }");
           /*
            oneOperation = { name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 4, 2, 0], end: [1, 4, 2, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test50a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4, 8], end: [1, 4, 9] }",
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'delete', start: [1, 4, 11], end: [1, 4, 12] }");
           /*
            oneOperation = { name: 'delete', start: [1, 4, 8], end: [1, 4, 9], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 4, 11], end: [1, 4, 12], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test51a() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 2], end: [1, 3] }",
            "{ name: 'insertRows', start: [1, 4, 6], referenceRow: 5, count: 3 }",
            "{ name: 'insertRows', start: [1, 2, 6], referenceRow: 5, count: 3 }",
            "{ name: 'delete', start: [1, 2], end: [1, 3] }");
           /*
            oneOperation = { name: 'delete', start: [1, 2], end: [1, 3], opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 4, 6], referenceRow: 5, count: 3 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2, 6], referenceRow: 5, count: 3 }] }], localActions);
            expectOp([{ name: 'delete', start: [1, 2], end: [1, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 2, 3] }",
            "{ name: 'delete', start: [1, 2, 6] }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 2, 3], opl: 1, osn: 1 }] }]; // deleting a cell
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 6]);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 2, 0] }",
            "{ name: 'delete', start: [1, 2, 0] }",
            "{ name: 'insertCells', start: [1, 2, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 2, 0], opl: 1, osn: 1 }] }]; // deleting a cell
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 0]);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 2, 2] }",
            "{ name: 'delete', start: [1, 2, 5] }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 5]);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 1, 0] }",
            "{ name: 'delete', start: [1, 1, 0] }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1, 0], opl: 1, osn: 1 }] }]; // deleting a cell
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 0]);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'delete', start: [0, 2, 0] }",
            "{ name: 'delete', start: [0, 2, 0] }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 2, 0], opl: 1, osn: 1 }] }]; // deleting a cell
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2, 0]);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1], count: 3 }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'insertCells', start: [2, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1], count: 3 }",
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'insertCells', start: [1, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
             */
    }

    @Test
    public void test68() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'insertCells', start: [1, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertCells operation has NOT the marker for local ignoring
             */
    }

    @Test
    public void test69() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1], count: 3 }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test70() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 1], count: 3 }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'insertCells', start: [0, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test71() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 1], count: 3 }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertCells', start: [1, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test72() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 1], count: 3 }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'insertCells', start: [1, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test73() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 4, 1], count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertCells', start: [1, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test74() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 4, 1], count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 4] }",
            "{ name: 'delete', start: [1, 1], end: [1, 4] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 4, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test75() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertCells', start: [2, 1, 2, 3, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test76() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'insertCells', start: [3, 1, 2, 2, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before insert
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]);
             */
    }

    @Test
    public void test77() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind insert
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test78() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 2, 3, 0] }",
            "{ name: 'delete', start: [3, 1, 2, 3, 0] }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 0, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 3, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 0]);
             */
    }

    @Test
    public void test79() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test80() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 2, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test81() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 3, 4, 6] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 4, 6] }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 4, 6]);
             */
    }

    @Test
    public void test82() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 3, 1, 7] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 1, 10] }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 1, 7], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 1, 10]);
             */
    }

    @Test
    public void test83() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 3, 1, 1] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 1, 1] }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 1, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 1, 1]);
             */
    }

    @Test
    public void test84() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test85() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 2, 1] }",
            "{ name: 'delete', start: [3, 1, 2, 1] }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
             */
    }

    @Test
    public void test86() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 1] }",
            "{ name: 'delete', start: [3, 1, 1, 1] }",
            "{ name: 'insertCells', start: [3, 1, 1, 2, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1]);
             */
    }

    @Test
    public void test87() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 4] }",
            "{ name: 'delete', start: [3, 1, 1, 4] }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 4]);
             */
    }

    @Test
    public void test88() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 3, 0] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 0] }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 0, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 0]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test89() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 3, 2] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 2] }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test90() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 1, 2], count: 3 }",
            "{ name: 'delete', start: [3, 1, 1, 3, 1] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 1] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1, 3, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test91() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [1, 8] }",
            "{ name: 'delete', start: [0, 1], end: [1, 8] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 8] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test92() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [1, 1, 0], end: [1, 1, 8] }",
            "{ name: 'delete', start: [1, 1, 0], end: [1, 1, 11] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1, 0], end: [1, 1, 8] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 1, 11]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test93() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [3, 3] }",
            "{ name: 'delete', start: [0, 1], end: [3, 3] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test94() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test95() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 2, 2], end: [1, 2, 4] }",
            "{ name: 'delete', start: [1, 2, 5], end: [1, 2, 7] }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2, 2], end: [1, 2, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2, 7]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertCells operation has not the marker for local ignoring
             */
    }

    @Test
    public void test96() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [1] }",
            "{ name: 'delete', start: [0, 1], end: [1] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test97() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test98() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1, 4, 1, 2], count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 4, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertCells operation has the marker for local ignoring
             */
    }

    @Test
    public void test99() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1, 5, 1, 2], count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 5, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test100() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 3], count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1], end: [2, 1, 1, 2, 2] }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1], end: [2, 1, 1, 2, 2] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 3], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], end: [2, 1, 1, 2, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2, 1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test101() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 4], count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1, 0], end: [2, 1, 1, 2, 1, 2] }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1, 0], end: [2, 1, 1, 2, 1, 2] }",
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 4], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 0], end: [2, 1, 1, 2, 1, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 1, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 2, 1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test102() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 4], count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1, 6], end: [2, 1, 1, 2, 1, 7] }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1, 9], end: [2, 1, 1, 2, 1, 10] }",
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 4], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 4], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 6], end: [2, 1, 1, 2, 1, 7] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2, 1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 1, 9]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 2, 1, 10]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test103() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 4], count: 3 }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1, 2], end: [2, 1, 1, 2, 1, 6] }",
            "{ name: 'delete', start: [2, 1, 1, 2, 1, 2], end: [2, 1, 1, 2, 1, 9] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 4], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 2], end: [2, 1, 1, 2, 1, 6] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2, 1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 2, 1, 9]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test104() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 2], count: 3 }",
            "{ name: 'delete', start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }",
            "{ name: 'delete', start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }",
            "{ name: 'insertCells', start: [2, 1, 1, 2, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1, 2, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 2, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 2, 4, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test105() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [1, 1, 2], end: [1, 1, 3] }",
            "{ name: 'delete', start: [1, 1, 5], end: [1, 1, 6] }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1, 2], end: [1, 1, 3] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 1, 6]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test106() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [3, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test107() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1], count: 3 }",
            "{ name: 'delete', start: [1], end: [2, 5] }",
            "{ name: 'delete', start: [1], end: [2, 5] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 1], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 5] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test108() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 4, 5, 2], count: 3 }",
            "{ name: 'delete', start: [1, 4], end: [3, 8] }",
            "{ name: 'delete', start: [1, 4], end: [3, 8] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 4, 5, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
                expect(localActions[0].operations.length).to.equal(2);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]);
                expect(localActions[0].operations.length).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test109() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test110() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 2], count: 3 }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test111() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [0, 1, 2], count: 3 }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertCells', start: [0, 1, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [0, 1, 2], count: 3 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test112() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 7, 2, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'insertCells', start: [1, 5, 2, 2, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 7, 2, 2, 2], count: 3 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test113() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 5], end: [1, 7] }",
            "{ name: 'delete', start: [1, 5], end: [1, 7] }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 3, 2, 2, 2], count: 3 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 5], end: [1, 7], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 7]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test114() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], count: 3 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test115() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 2, 2, 2, 2], count: 3 }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], count: 3 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test116() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 7, 2, 2, 2], count: 3 }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'insertCells', start: [1, 7, 2, 2, 2], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 7, 2, 2, 2], count: 3 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 7, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test117() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10], end: [1, 11] }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 2 }",
            "{ name: 'delete', start: [1, 10], end: [1, 11] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [1, 11] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(oneOperation.end).to.deep.equal([1, 11]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2]);
             */
    }

    @Test
    public void test118() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 2, 4, 2, 2], end: [1, 2, 4, 2, 6] }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 2 }",
            "{ name: 'delete', start: [1, 2, 6, 2, 2], end: [1, 2, 6, 2, 6] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2, 4, 2, 2], end: [1, 2, 4, 2, 6] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 6, 2, 2]);
                expect(oneOperation.end).to.deep.equal([1, 2, 6, 2, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2]);
             */
    }

    @Test
    public void test119() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1] }",
            "{ name: 'insertCells', start: [1, 4, 1], count: 2 }",
            "{ name: 'insertCells', start: [1, 3, 1], count: 2 }",
            "{ name: 'delete', start: [1, 1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 1], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 1]);
             */
    }

    @Test
    public void test120() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'insertCells', start: [1, 4, 1], count: 2 }",
            "{ name: 'insertCells', start: [1, 2, 1], count: 2 }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 1], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(oneOperation.end).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 1]);
             */
    }

    @Test
    public void test121() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4, 0], end: [1, 4, 2] }",
            "{ name: 'insertCells', start: [1, 4, 4], count: 2 }",
            "{ name: 'insertCells', start: [1, 4, 1], count: 2 }",
            "{ name: 'delete', start: [1, 4, 0], end: [1, 4, 2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4, 0], end: [1, 4, 2] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 4], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4, 0]);
                expect(oneOperation.end).to.deep.equal([1, 4, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 1]);
             */
    }

    @Test
    public void test122() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertCells', start: [2, 4, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 4, 2], count: 2 }",
            "{ name: 'delete', start: [1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertCells', start: [2, 4, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 2]);
             */
    }

    @Test
    public void test123() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3, 2, 8] }",
            "{ name: 'insertCells', start: [1, 4, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 4, 2], count: 2 }",
            "{ name: 'delete', start: [1, 3, 2, 8] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 2, 8] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 2]);
             */
    }

    @Test
    public void test124() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3] }",
            "{ name: 'insertCells', start: [1, 4, 1, 1, 1, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 3, 1, 1, 1, 2], count: 2 }",
            "{ name: 'delete', start: [1, 3] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 1, 1, 1, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 1, 1, 1, 2]);
             */
    }

    @Test
    public void test125() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3, 4] }",
            "{ name: 'insertCells', start: [1, 3, 4], count: 2 }",
            "{ name: 'insertCells', start: [1, 3, 4], count: 2 }",
            "{ name: 'delete', start: [1, 3, 6] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 4] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 3, 4], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 4]);
             */
    }

    @Test
    public void test126() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertCells', start: [1, 3, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 3, 2], count: 2 }",
            "{ name: 'delete', start: [2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 3, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test127() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertCells', start: [2, 1, 1, 1, 1, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 1, 1, 1, 1, 2], count: 2 }",
            "{ name: 'delete', start: [1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 1, 1, 1, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 1, 1, 1, 2]);
             */
    }

    @Test
    public void test128() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 2 }",
            "{ name: 'delete', start: [4] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 1, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 1, 2]);
             */
    }

    @Test
    public void test129() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 8] }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 3, 8] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 2, 2]);
             */
    }

    @Test
    public void test130() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 2, 4] }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 3, 2, 6] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2, 4] };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 2, 2]);
             */
    }

    @Test
    public void test131() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 2, 0] }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 1], count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 3, 2, 0] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2, 0] };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2, 0]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 2, 1]);
             */
    }

    @Test
    public void test132() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 2] }",
            "{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 3, 2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2] };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2, 2, 2]);
             */
    }

    @Test
    public void test133() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 1, 2] }",
            "{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 1, 2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 1, 2] };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2, 2, 2]);
             */
    }

    @Test
    public void test134() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertCells', start: [3, 1, 2, 5, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 1, 2, 4, 2, 2], count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 4] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 5, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4, 2, 2]);
             */
    }

    @Test
    public void test135() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 4, 2], count: 2 }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 4, 2], count: 2 }",
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 2, 3, 4, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4, 2]);
             */
    }

    @Test
    public void test136() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10], end: [3, 6] }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 2 }",
            "{ name: 'delete', start: [1, 10], end: [3, 6] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(oneOperation.end).to.deep.equal([3, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2]);
             */
    }

    @Test
    public void test137() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertCells', start: [5, 1, 1], count: 2 }",
            "{ name: 'insertCells', start: [4, 1, 1], count: 2 }",
            "{ name: 'delete', start: [1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertCells', start: [5, 1, 1], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 1]);
             */
    }

    @Test
    public void test138() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'insertCells', start: [5, 1, 1], count: 2 }",
            "{ name: 'insertCells', start: [2, 1, 1], count: 2 }",
            "{ name: 'delete', start: [1], end: [3] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
                localActions = [{ operations: [{ name: 'insertCells', start: [5, 1, 1], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.end).to.deep.equal([3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1]);
             */
    }

    @Test
    public void test139() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 2], end: [1, 6] }",
            "{ name: 'insertCells', start: [1, 4, 1], count: 2 }",
            "[]",
            "{ name: 'delete', start: [1, 2], end: [1, 6] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 6] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 4, 1], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(oneOperation.end).to.deep.equal([1, 6]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test140() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertCells', start: [2, 1, 1], count: 2 }",
            "[]",
            "{ name: 'delete', start: [2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 1], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test141() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [3, 18] }",
            "{ name: 'insertCells', start: [0, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [0, 2, 2], count: 2 }",
            "{ name: 'delete', start: [1, 14], end: [3, 18] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [3, 18] };
                localActions = [{ operations: [{ name: 'insertCells', start: [0, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 14]);
                expect(oneOperation.end).to.deep.equal([3, 18]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2, 2]);
             */
    }

    @Test
    public void test142() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4], end: [1, 8] }",
            "{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 5, 0, 2, 2], count: 2 }",
            "{ name: 'delete', start: [1, 4], end: [1, 8] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 8] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(oneOperation.end).to.deep.equal([1, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 0, 2, 2]);
             */
    }

    @Test
    public void test143() {
        TransformerTest.transform(
            "{ name: 'delete', start: [0, 4], end: [0, 8] }",
            "{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2 }",
            "{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2 }",
            "{ name: 'delete', start: [0, 4], end: [0, 8] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 4], end: [0, 8] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 4]);
                expect(oneOperation.end).to.deep.equal([0, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10, 0, 2, 2]);
             */
    }

    @Test
    public void test144() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 4], end: [1, 14] }",
            "{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2 }",
            "[]",
            "{ name: 'delete', start: [1, 4], end: [1, 14] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 14] };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 10, 0, 2, 2], count: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(oneOperation.end).to.deep.equal([1, 14]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10, 0, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
             */
    }
}
