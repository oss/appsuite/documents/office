/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertDeleteDrawing {

    /*
     * describe('"insertDrawing" and "deleteDrawing"', function () {
            it('should skip different sheets', function () {
                testRunner.runBidiTest(insertShape(1, 0, ATTRS), deleteDrawing(0, 1));
            });
            it('should shift positions of top-level shapes', function () {
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 0), insertShape(1, 1, ATTRS), deleteDrawing(1, 0));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 1), insertShape(1, 1, ATTRS), deleteDrawing(1, 1));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 2), insertShape(1, 2, ATTRS), deleteDrawing(1, 3));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 3), insertShape(1, 2, ATTRS), deleteDrawing(1, 4));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, 4), insertShape(1, 2, ATTRS), deleteDrawing(1, 5));
            });
            it('should shift position of external embedded shape', function () {
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, '0 4'), null, deleteDrawing(1, '0 4'));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, '1 4'), null, deleteDrawing(1, '1 4'));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, '2 4'), null, deleteDrawing(1, '3 4'));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, '3 4'), null, deleteDrawing(1, '4 4'));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), deleteDrawing(1, '4 4'), null, deleteDrawing(1, '5 4'));
            });
            it('should shift position of local embedded shape', function () {
                testRunner.runBidiTest(insertShape(1, '2 4', ATTRS), deleteDrawing(1, 0), insertShape(1, '1 4', ATTRS));
                testRunner.runBidiTest(insertShape(1, '2 4', ATTRS), deleteDrawing(1, 1), insertShape(1, '1 4', ATTRS));
                testRunner.runBidiTest(insertShape(1, '2 4', ATTRS), deleteDrawing(1, 2), []);
                testRunner.runBidiTest(insertShape(1, '2 4', ATTRS), deleteDrawing(1, 3), insertShape(1, '2 4', ATTRS));
                testRunner.runBidiTest(insertShape(1, '2 4', ATTRS), deleteDrawing(1, 4), insertShape(1, '2 4', ATTRS));
            });
            it('should shift positions of embedded siblings', function () {
                testRunner.runBidiTest(insertShape(1, '2 2', ATTRS), deleteDrawing(1, '2 0'), insertShape(1, '2 1', ATTRS), deleteDrawing(1, '2 0'));
                testRunner.runBidiTest(insertShape(1, '2 2', ATTRS), deleteDrawing(1, '2 1'), insertShape(1, '2 1', ATTRS), deleteDrawing(1, '2 1'));
                testRunner.runBidiTest(insertShape(1, '2 2', ATTRS), deleteDrawing(1, '2 2'), insertShape(1, '2 2', ATTRS), deleteDrawing(1, '2 3'));
                testRunner.runBidiTest(insertShape(1, '2 2', ATTRS), deleteDrawing(1, '2 3'), insertShape(1, '2 2', ATTRS), deleteDrawing(1, '2 4'));
                testRunner.runBidiTest(insertShape(1, '2 2', ATTRS), deleteDrawing(1, '2 4'), insertShape(1, '2 2', ATTRS), deleteDrawing(1, '2 5'));
            });
            it('should skip independent embedded shapes', function () {
                -testRunner.runBidiTest(insertShape(1, '2 2', ATTRS), opSeries2(deleteDrawing, 1, ['0 0', '0 2', '0 4', '4 0', '4 2', '4 4']));
            });
        });
    */

    @Test
    public void insertDrawingDeleteDrawing01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runBidiTest(
        //  insertShape(1, 0, ATTRS),
        //  deleteDrawing(0, 1));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 0", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("0 1").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing02() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, 0),
        //  insertShape(1, 1, ATTRS),
        //  deleteDrawing(1, 0))
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 0").toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 0").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing03() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, 1),
        //  insertShape(1, 1, ATTRS),
        //  deleteDrawing(1, 1));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 1").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing04() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, 2),
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, 3));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 3").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing05() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, 3),
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, 4));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 3").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 4").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing06() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, 4),
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, 5));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 4").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 5").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing07() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, '0 4'),
        //  null,
        //  deleteDrawing(1, '0 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 0 4").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 0 4").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing08() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, '1 4'),
        //  null,
        //  deleteDrawing(1, '1 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 1 4").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 1 4").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing09() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, '2 4'),
        //  null,
        //  deleteDrawing(1, '3 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 3 4").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing10() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, '3 4'),
        //  null,
        //  deleteDrawing(1, '4 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 3 4").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 4 4").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing11() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  deleteDrawing(1, '4 4'),
        //  null,
        //  deleteDrawing(1, '5 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 4 4").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 5 4").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing12() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 4', ATTRS),
        //  deleteDrawing(1, 0),
        //  insertShape(1, '1 4', ATTRS))
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 0").toString(),
            Helper.createInsertShapeOp("1 1 4", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 0").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing13() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 4', ATTRS),
        //  deleteDrawing(1, 1),
        //  insertShape(1, '1 4', ATTRS));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createInsertShapeOp("1 1 4", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 1").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing14() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 4', ATTRS),
        //  deleteDrawing(1, 2),
        //  []);
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            "[]",
            Helper.createDeleteDrawingOp("1 2").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing15() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 4', ATTRS),
        //  deleteDrawing(1, 3),
        //  insertShape(1, '2 4', ATTRS));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 3").toString(),
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 3").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing16() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 4', ATTRS),
        //  deleteDrawing(1, 4),
        //  insertShape(1, '2 4', ATTRS));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 4").toString(),
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 4").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing17() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 2', ATTRS),
        //  deleteDrawing(1, '2 0'),
        //  insertShape(1, '2 1', ATTRS),
        //  deleteDrawing(1, '2 0'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2 0").toString(),
            Helper.createInsertShapeOp("1 2 1", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2 0").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing18() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 2', ATTRS),
        //  deleteDrawing(1, '2 1'),
        //  insertShape(1, '2 1', ATTRS),
        //  deleteDrawing(1, '2 1'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2 1").toString(),
            Helper.createInsertShapeOp("1 2 1", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2 1").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing19() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 2', ATTRS),
        //  deleteDrawing(1, '2 2'),
        //  insertShape(1, '2 2', ATTRS),
        //  deleteDrawing(1, '2 3'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2 3").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing20() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 2', ATTRS),
        //  deleteDrawing(1, '2 3'),
        //  insertShape(1, '2 2', ATTRS),
        //  deleteDrawing(1, '2 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2 3").toString(),
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createDeleteDrawingOp("1 2 4").toString()
        );
    }

    @Test
    public void insertDrawingDeleteDrawing21() throws JSONException {
        // Result: Should skip independent embedded shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 2', ATTRS),
        //  opSeries2(deleteDrawing, 1, ['0 0', '0 2', '0 4', '4 0', '4 2', '4 4']));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createArrayFromJSON(
                Helper.createDeleteDrawingOp("1 0 0"),
                Helper.createDeleteDrawingOp("1 0 2"),
                Helper.createDeleteDrawingOp("1 0 4"),
                Helper.createDeleteDrawingOp("1 4 0"),
                Helper.createDeleteDrawingOp("1 4 2"),
                Helper.createDeleteDrawingOp("1 4 4")
            )
        );
    }
}
