/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DeleteDrawingMoveDrawing {

    /*
     * describe('"deleteDrawing" and "moveDrawing"', function () {
            it('should skip different sheets', function () {
                testRunner.runTest(deleteDrawing(1, 0), moveDrawing(0, 1, 2));
            });
            it('should shift positions for top-level shape', function () {
                testRunner.runBidiTest(deleteDrawing(1, 1), moveDrawing(1, 2, 4), deleteDrawing(1, 1), moveDrawing(1, 1, 3));
                testRunner.runBidiTest(deleteDrawing(1, 2), moveDrawing(1, 2, 4), deleteDrawing(1, 4), []);
                testRunner.runBidiTest(deleteDrawing(1, 3), moveDrawing(1, 2, 4), deleteDrawing(1, 2), moveDrawing(1, 2, 3));
                testRunner.runBidiTest(deleteDrawing(1, 4), moveDrawing(1, 2, 4), deleteDrawing(1, 3), moveDrawing(1, 2, 3));
                testRunner.runBidiTest(deleteDrawing(1, 5), moveDrawing(1, 2, 4), deleteDrawing(1, 5), moveDrawing(1, 2, 4));
                testRunner.runBidiTest(deleteDrawing(1, 1), moveDrawing(1, 4, 2), deleteDrawing(1, 1), moveDrawing(1, 3, 1));
                testRunner.runBidiTest(deleteDrawing(1, 2), moveDrawing(1, 4, 2), deleteDrawing(1, 3), moveDrawing(1, 3, 2));
                testRunner.runBidiTest(deleteDrawing(1, 3), moveDrawing(1, 4, 2), deleteDrawing(1, 4), moveDrawing(1, 3, 2));
                testRunner.runBidiTest(deleteDrawing(1, 4), moveDrawing(1, 4, 2), deleteDrawing(1, 2), []);
                testRunner.runBidiTest(deleteDrawing(1, 5), moveDrawing(1, 4, 2), deleteDrawing(1, 5), moveDrawing(1, 4, 2));
                testRunner.runBidiTest(deleteDrawing(1, 1), moveDrawing(1, 3, 3), null, []);
            });
            it('should shift positions for embedded shape', function () {
                testRunner.runBidiTest(deleteDrawing(1, '1 1'), moveDrawing(1, '1 2', '1 4'), deleteDrawing(1, '1 1'), moveDrawing(1, '1 1', '1 3'));
                testRunner.runBidiTest(deleteDrawing(1, '1 2'), moveDrawing(1, '1 2', '1 4'), deleteDrawing(1, '1 4'), []);
                testRunner.runBidiTest(deleteDrawing(1, '1 3'), moveDrawing(1, '1 2', '1 4'), deleteDrawing(1, '1 2'), moveDrawing(1, '1 2', '1 3'));
                testRunner.runBidiTest(deleteDrawing(1, '1 4'), moveDrawing(1, '1 2', '1 4'), deleteDrawing(1, '1 3'), moveDrawing(1, '1 2', '1 3'));
                testRunner.runBidiTest(deleteDrawing(1, '1 5'), moveDrawing(1, '1 2', '1 4'), deleteDrawing(1, '1 5'), moveDrawing(1, '1 2', '1 4'));
                testRunner.runBidiTest(deleteDrawing(1, '1 1'), moveDrawing(1, '1 4', '1 2'), deleteDrawing(1, '1 1'), moveDrawing(1, '1 3', '1 1'));
                testRunner.runBidiTest(deleteDrawing(1, '1 2'), moveDrawing(1, '1 4', '1 2'), deleteDrawing(1, '1 3'), moveDrawing(1, '1 3', '1 2'));
                testRunner.runBidiTest(deleteDrawing(1, '1 3'), moveDrawing(1, '1 4', '1 2'), deleteDrawing(1, '1 4'), moveDrawing(1, '1 3', '1 2'));
                testRunner.runBidiTest(deleteDrawing(1, '1 4'), moveDrawing(1, '1 4', '1 2'), deleteDrawing(1, '1 2'), []);
                testRunner.runBidiTest(deleteDrawing(1, '1 5'), moveDrawing(1, '1 4', '1 2'), deleteDrawing(1, '1 5'), moveDrawing(1, '1 4', '1 2'));
                testRunner.runBidiTest(deleteDrawing(1, '1 1'), moveDrawing(1, '1 3', '1 3'), null, []);
            });
            it('should shift positions of embedded deleted shapes', function () {
                testRunner.runBidiTest(deleteDrawing(1, '1 1'), moveDrawing(1, 2, 4), deleteDrawing(1, '1 1'));
                testRunner.runBidiTest(deleteDrawing(1, '2 1'), moveDrawing(1, 2, 4), deleteDrawing(1, '4 1'));
                testRunner.runBidiTest(deleteDrawing(1, '3 1'), moveDrawing(1, 2, 4), deleteDrawing(1, '2 1'));
                testRunner.runBidiTest(deleteDrawing(1, '4 1'), moveDrawing(1, 2, 4), deleteDrawing(1, '3 1'));
                testRunner.runBidiTest(deleteDrawing(1, '5 1'), moveDrawing(1, 2, 4), deleteDrawing(1, '5 1'));
            });
            it('should shift positions of embedded moved shapes', function () {
                testRunner.runBidiTest(deleteDrawing(1, 1), moveDrawing(1, '3 2', '3 4'), null, moveDrawing(1, '2 2', '2 4'));
                testRunner.runBidiTest(deleteDrawing(1, 2), moveDrawing(1, '3 2', '3 4'), null, moveDrawing(1, '2 2', '2 4'));
                testRunner.runBidiTest(deleteDrawing(1, 3), moveDrawing(1, '3 2', '3 4'), null, []);
                testRunner.runBidiTest(deleteDrawing(1, 4), moveDrawing(1, '3 2', '3 4'));
                testRunner.runBidiTest(deleteDrawing(1, 5), moveDrawing(1, '3 2', '3 4'));
            });
            it('should not shift positions of independent shapes', function () {
                testRunner.runBidiTest(deleteDrawing(1, '1 1'),   moveDrawing(1, '0 0',   '0 1'));
                testRunner.runBidiTest(deleteDrawing(1, '1 1'),   moveDrawing(1, '2 0',   '2 1'));
                testRunner.runBidiTest(deleteDrawing(1, '1 1 1'), moveDrawing(1, '0 1 1', '0 1 2'));
            });
        });
    */

    @Test
    public void deleteDrawingMoveDrawing01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runTest(
        //  deleteDrawing(1, 0),
        //  moveDrawing(0, 1, 2));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 0").toString(),
            Helper.createMoveDrawingOp("0 1", "0 2").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing02() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 1),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, 1),
        //  moveDrawing(1, 1, 3));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createMoveDrawingOp("1 1", "1 3").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing03() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 2),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, 4), []);
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 4").toString(),
            "[]"
        );
    }

    @Test
    public void deleteDrawingMoveDrawing04() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 3),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, 2),
        //  moveDrawing(1, 2, 3));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 3").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createMoveDrawingOp("1 2", "1 3").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing05() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 4),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, 3),
        //  moveDrawing(1, 2, 3));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 4").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 3").toString(),
            Helper.createMoveDrawingOp("1 2", "1 3").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing06() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 5),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, 5),
        //  moveDrawing(1, 2, 4));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 5").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 5").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing07() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 1),
        //  moveDrawing(1, 4, 2),
        //  deleteDrawing(1, 1),
        //  moveDrawing(1, 3, 1));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 5").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 5").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing08() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 2),
        //  moveDrawing(1, 4, 2),
        //  deleteDrawing(1, 3),
        //  moveDrawing(1, 3, 2));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createDeleteDrawingOp("1 3").toString(),
            Helper.createMoveDrawingOp("1 3", "1 2").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing09() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 3),
        //  moveDrawing(1, 4, 2),
        //  deleteDrawing(1, 4),
        //  moveDrawing(1, 3, 2));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 3").toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createDeleteDrawingOp("1 4").toString(),
            Helper.createMoveDrawingOp("1 3", "1 2").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing10() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 4),
        //  moveDrawing(1, 4, 2),
        //  deleteDrawing(1, 2),
        //  []);
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 4").toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            "[]"
        );
    }

    @Test
    public void deleteDrawingMoveDrawing11() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 5),
        //  moveDrawing(1, 4, 2),
        //  deleteDrawing(1, 5),
        //  moveDrawing(1, 4, 2));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 5").toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createDeleteDrawingOp("1 5").toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing12() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 1),
        //  moveDrawing(1, 3, 3),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createMoveDrawingOp("1 3", "1 3").toString(),
            Helper.createDeleteDrawingOp("1 1").toString(),
            "[]"
        );
    }

    @Test
    public void deleteDrawingMoveDrawing13() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  moveDrawing(1, '1 2', '1 4'),
        //  deleteDrawing(1, '1 1'),
        //  moveDrawing(1, '1 1', '1 3'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 1 1", "1 1 3").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing14() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 2'),
        //  moveDrawing(1, '1 2', '1 4'),
        //  deleteDrawing(1, '1 4'),
        //  []);
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 2").toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createDeleteDrawingOp("1 1 4").toString(),
            "[]"
        );
    }

    @Test
    public void deleteDrawingMoveDrawing15() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 3'),
        //  moveDrawing(1, '1 2', '1 4'),
        //  deleteDrawing(1, '1 2'),
        //  moveDrawing(1, '1 2', '1 3'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 3").toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createDeleteDrawingOp("1 1 2").toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 3").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing16() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 4'),
        //  moveDrawing(1, '1 2', '1 4'),
        //  deleteDrawing(1, '1 3'),
        //  moveDrawing(1, '1 2', '1 3'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 4").toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createDeleteDrawingOp("1 1 3").toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 3").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing17() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 5'),
        //  moveDrawing(1, '1 2', '1 4'),
        //  deleteDrawing(1, '1 5'),
        //  moveDrawing(1, '1 2', '1 4'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 5").toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createDeleteDrawingOp("1 1 5").toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing18() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  moveDrawing(1, '1 4', '1 2'),
        //  deleteDrawing(1, '1 1'),
        //  moveDrawing(1, '1 3', '1 1'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 1 3", "1 1 1").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing19() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 2'),
        //  moveDrawing(1, '1 4', '1 2'),
        //  deleteDrawing(1, '1 3'),
        //  moveDrawing(1, '1 3', '1 2'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 2").toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createDeleteDrawingOp("1 1 3").toString(),
            Helper.createMoveDrawingOp("1 1 3", "1 1 2").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing20() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 3'),
        //  moveDrawing(1, '1 4', '1 2'),
        //  deleteDrawing(1, '1 4'),
        //  moveDrawing(1, '1 3', '1 2'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 3").toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createDeleteDrawingOp("1 1 4").toString(),
            Helper.createMoveDrawingOp("1 1 3", "1 1 2").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing21() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 4'),
        //  moveDrawing(1, '1 4', '1 2'),
        //  deleteDrawing(1, '1 2'),
        //  []);
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 4").toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createDeleteDrawingOp("1 1 2").toString(),
            "[]"
        );
    }

    @Test
    public void deleteDrawingMoveDrawing22() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 5'),
        //  moveDrawing(1, '1 4', '1 2'),
        //  deleteDrawing(1, '1 5'),
        //  moveDrawing(1, '1 4', '1 2'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 5").toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createDeleteDrawingOp("1 1 5").toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing23() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  moveDrawing(1, '1 3', '1 3'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 1 3", "1 1 3").toString(),
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            "[]"
        );
    }

    @Test
    public void deleteDrawingMoveDrawing24() throws JSONException {
        // Result: Should shift positions of embedded deleted shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, '1 1'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing25() throws JSONException {
        // Result: Should shift positions of embedded deleted shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '2 1'),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, '4 1'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 2 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 4 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing26() throws JSONException {
        // Result: Should shift positions of embedded deleted shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '3 1'),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, '2 1'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 3 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 2 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing27() throws JSONException {
        // Result: Should shift positions of embedded deleted shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '4 1'),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, '3 1'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 4 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 3 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing28() throws JSONException {
        // Result: Should shift positions of embedded deleted shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '5 1'),
        //  moveDrawing(1, 2, 4),
        //  deleteDrawing(1, '5 1'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 5 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createDeleteDrawingOp("1 5 1").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing29() throws JSONException {
        // Result: Should shift positions of embedded moved shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 1),
        //  moveDrawing(1, '3 2', '3 4'),
        //  null,
        //  moveDrawing(1, '2 2', '2 4'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString(),
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createMoveDrawingOp("1 2 2", "1 2 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing30() throws JSONException {
        // Result: Should shift positions of embedded moved shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 2),
        //  moveDrawing(1, '3 2', '3 4'),
        //  null,
        //  moveDrawing(1, '2 2', '2 4'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createMoveDrawingOp("1 2 2", "1 2 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing31() throws JSONException {
        // Result: Should shift positions of embedded moved shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 3),
        //  moveDrawing(1, '3 2', '3 4'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 3").toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString(),
            Helper.createDeleteDrawingOp("1 3").toString(),
            "[]"
        );
    }

    @Test
    public void deleteDrawingMoveDrawing32() throws JSONException {
        // Result: Should shift positions of embedded moved shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 4),
        //  moveDrawing(1, '3 2', '3 4'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 4").toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing33() throws JSONException {
        // Result: Should shift positions of embedded moved shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 5),
        //  moveDrawing(1, '3 2', '3 4'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 5").toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing34() throws JSONException {
        // Result: Should not shift positions of independent shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  moveDrawing(1, '0 0',   '0 1'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 0 0", "1 0 1").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing35() throws JSONException {
        // Result: Should not shift positions of independent shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  moveDrawing(1, '2 0',   '2 1'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 2 0", "1 2 1").toString()
        );
    }

    @Test
    public void deleteDrawingMoveDrawing36() throws JSONException {
        // Result: Should not shift positions of independent shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1 1'),
        //  moveDrawing(1, '0 1 1', '0 1 2'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createMoveDrawingOp("1 0 1 1", "1 0 1 2").toString()
        );
    }
}
