/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class MoveMoveNote {

    /*
     * describe('"moveNotes" and "moveNotes', function () {
            it('should skip different sheets', function () {
                testRunner.runTest(moveNotes(1, 'A1 C3', 'C3 A1'), moveNotes(2, 'A1 B2', 'B2 A1'));
            });
            it('should ignore moving same note to same cell', function () {
                testRunner.runTest(
                    moveNotes(1, 'A1 D4', 'B2 E5'), moveNotes(1, 'A1 F6', 'B2 G7'),
                    moveNotes(1, 'D4',    'E5'),    moveNotes(1, 'F6',    'G7')
                );
                testRunner.runTest(moveNotes(1, 'A1', 'B2'), moveNotes(1, 'A1', 'B2'), [], []);
            });
            it('should prefer local move when moving same note to different cells', function () {
                testRunner.runTest(
                    moveNotes(1, 'A1 D4', 'B2 E5'), moveNotes(1, 'A1 F6', 'C3 G7'),
                    moveNotes(1, 'C3 D4', 'B2 E5'), moveNotes(1, 'F6', 'G7')
                );
                testRunner.runTest(
                    moveNotes(1, 'A1', 'B2'), moveNotes(1, 'A1', 'C3'),
                    moveNotes(1, 'C3', 'B2'), []
                );
            });
            it('should prefer local move when moving different notes to same cell', function () {
                testRunner.runTest(
                    moveNotes(1, 'A1 D4',    'C3 E5'),    moveNotes(1, 'B2 F6', 'C3 G7'),
                    moveNotes(1, 'A1 C3 D4', 'C3 B2 E5'), moveNotes(1, 'F6', 'G7')
                );
                testRunner.runTest(
                    moveNotes(1, 'A1',    'C3'),    moveNotes(1, 'B2', 'C3'),
                    moveNotes(1, 'A1 C3', 'C3 B2'), []
                );
            });
            it('should handle concurring cyclic moves', function () {
                testRunner.runTest(
                    moveNotes(1, 'A1 B2 D4',    'B2 A1 E5'),    moveNotes(1, 'A1 C3 F6', 'C3 A1 G7'),
                    moveNotes(1, 'C3 B2 A1 D4', 'B2 A1 C3 E5'), moveNotes(1, 'F6',       'G7')
                );
                testRunner.runTest(
                    moveNotes(1, 'A1 B2',    'B2 A1'),    moveNotes(1, 'A1 C3', 'C3 A1'),
                    moveNotes(1, 'C3 B2 A1', 'B2 A1 C3'), []
                );
            });
        });
    */

    @Test
    public void moveNoteMoveNotes01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runTest(
        //  moveNotes(1, 'A1 C3', 'C3 A1'),
        //  moveNotes(2, 'A1 B2', 'B2 A1'));
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1 C3", "C3 A1").toString(),
            SheetHelper.createMoveNotesOp(2, "A1 B2", "B2 A1").toString()
        );
    }

    @Test
    public void moveNoteMoveNotes02() throws JSONException {
        // Result: Should ignore moving same note to same cell.
        // testRunner.runTest(
        //  moveNotes(1, 'A1 D4', 'B2 E5'),
        //  moveNotes(1, 'A1 F6', 'B2 G7'),
        //  moveNotes(1, 'D4',    'E5'),
        //  moveNotes(1, 'F6',    'G7'));
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1 D4", "B2 E5").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 F6", "B2 G7").toString(),
            SheetHelper.createMoveNotesOp(1, "D4", "E5").toString(),
            SheetHelper.createMoveNotesOp(1, "F6", "G7").toString()
        );
    }

    @Test
    public void moveNoteMoveNotes03() throws JSONException {
        // Result: Should ignore moving same note to same cell.
        // testRunner.runTest(
        //  moveNotes(1, 'A1', 'B2'),
        //  moveNotes(1, 'A1', 'B2'),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1", "B2").toString(),
            SheetHelper.createMoveNotesOp(1, "A1", "B2").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void moveNoteMoveNotes04() throws JSONException {
        // Result: Should prefer local move when moving same note to different cells.
        // testRunner.runTest(
        //  moveNotes(1, 'A1 D4', 'B2 E5'),
        //  moveNotes(1, 'A1 F6', 'C3 G7'),
        //  moveNotes(1, 'C3 D4', 'B2 E5'),
        //  moveNotes(1, 'F6', 'G7'));
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1 D4", "B2 E5").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 F6", "C3 G7").toString(),
            SheetHelper.createMoveNotesOp(1, "C3 D4", "B2 E5").toString(),
            SheetHelper.createMoveNotesOp(1, "F6", "G7").toString()
        );
    }

    @Test
    public void moveNoteMoveNotes05() throws JSONException {
        // Result: Should prefer local move when moving same note to different cells.
        // testRunner.runTest(
        //  moveNotes(1, 'A1', 'B2'),
        //  moveNotes(1, 'A1', 'C3'),
        //  moveNotes(1, 'C3', 'B2'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1", "B2").toString(),
            SheetHelper.createMoveNotesOp(1, "A1", "C3").toString(),
            SheetHelper.createMoveNotesOp(1, "C3", "B2").toString(),
            "[]"
        );
    }

    @Test
    public void moveNoteMoveNotes06() throws JSONException {
        // Result: Should prefer local move when moving different notes to same cell.
        // testRunner.runTest(
        //  moveNotes(1, 'A1 D4',    'C3 E5'),
        //  moveNotes(1, 'B2 F6', 'C3 G7'),
        //  moveNotes(1, 'A1 C3 D4', 'C3 B2 E5'),
        //  moveNotes(1, 'F6', 'G7'));
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1 D4", "C3 E5").toString(),
            SheetHelper.createMoveNotesOp(1, "B2 F6", "C3 G7").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 C3 D4", "C3 B2 E5").toString(),
            SheetHelper.createMoveNotesOp(1, "F6", "G7").toString()
        );
    }

    @Test
    public void moveNoteMoveNotes07() throws JSONException {
        // Result: Should prefer local move when moving different notes to same cell.
        // testRunner.runTest(
        //  moveNotes(1, 'A1',    'C3'),
        //  moveNotes(1, 'B2', 'C3'),
        //  moveNotes(1, 'A1 C3', 'C3 B2'), []);
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1", "C3").toString(),
            SheetHelper.createMoveNotesOp(1, "B2", "C3").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 C3", "C3 B2").toString(),
            "[]"
        );
    }

    @Test
    public void moveNoteMoveNotes08() throws JSONException {
        // Result: Should handle concurring cyclic moves.
        // testRunner.runTest(
        //  moveNotes(1, 'A1 B2 D4',    'B2 A1 E5'),
        //  moveNotes(1, 'A1 C3 F6', 'C3 A1 G7'),
        //  moveNotes(1, 'C3 B2 A1 D4', 'B2 A1 C3 E5'),
        //  moveNotes(1, 'F6',       'G7'));
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1 B2 D4", "B2 A1 E5").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 C3 F6", "C3 A1 G7").toString(),
            SheetHelper.createMoveNotesOp(1, "C3 B2 A1 D4", "B2 A1 C3 E5").toString(),
            SheetHelper.createMoveNotesOp(1, "F6", "G7").toString()
        );
    }

    @Test
    public void moveNoteMoveNotes09() throws JSONException {
        // Result: Should handle concurring cyclic moves.
        // testRunner.runTest(
        //  moveNotes(1, 'A1 B2',    'B2 A1'),
        //  moveNotes(1, 'A1 C3', 'C3 A1'),
        //  moveNotes(1, 'C3 B2 A1', 'B2 A1 C3'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1 B2", "B2 A1").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 C3", "C3 A1").toString(),
            SheetHelper.createMoveNotesOp(1, "C3 B2 A1", "B2 A1 C3").toString(),
            "[]"
        );
    }

    @Test
    public void moveNoteMoveNotes10() throws JSONException {
        // Result: Should handle concurring cyclic moves.
        // testRunner.runTest(
        //  moveNotes(1, 'A1 B2 D4',    'B2 A1 E5'),
        //  moveNotes(1, 'A1 C3 F6', 'C3 A1 G7'),
        //  moveNotes(1, 'C3 B2 A1 D4', 'B2 A1 C3 E5'),
        //  moveNotes(1, 'F6',       'G7'));
        SheetHelper.runTest(
            SheetHelper.createMoveNotesOp(1, "A1 B2 D4", "B2 A1 E5").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 C3 F6", "C3 A1 G7").toString(),
            SheetHelper.createMoveNotesOp(1, "C3 B2 A1 D4", "B2 A1 C3 E5").toString(),
            SheetHelper.createMoveNotesOp(1, "F6", "G7").toString()
        );
    }
}
