/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DefinedNames {

/*
    describe('defined names and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest([insertName('g'), deleteName('g')], [
                GLOBAL_OPS, STYLESHEET_OPS,
                tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/

    @Test
    public void DefinedNames01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertNameOp("g", null, null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createTableOps("1"),
                SheetHelper.createDvRuleOps("1", null),
                SheetHelper.createCfRuleOps("1", null),
                SheetHelper.createNoteOps("1"),
                SheetHelper.createCommentOps("1"),
                // select ...
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

    @Test
    public void DefinedNames02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteNameOp("g", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createTableOps("1"),
                SheetHelper.createDvRuleOps("1", null),
                SheetHelper.createCfRuleOps("1", null),
                SheetHelper.createNoteOps("1"),
                SheetHelper.createCommentOps("1"),
                // select ...
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

    // insertName ---------------------------------------------------------

/*
    describe('"insertName" and "insertName"', function () {
        it('should skip different names', function () {
            testRunner.runTest(insertName('n1'),    insertName('n2'));
            testRunner.runTest(insertName('n1'),    insertName('n1', 0));
            testRunner.runTest(insertName('n1', 0), insertName('n1'));
            testRunner.runTest(insertName('n1', 0), insertName('n1', 1));
            testRunner.runTest(insertName('n1', 0), insertName('n2', 0));
        });
        it('should fail to insert same name twice', function () {
            testRunner.expectError(insertName('n1'),    insertName('n1'));
            testRunner.expectError(insertName('n1', 0), insertName('n1', 0));
            testRunner.expectError(insertName('n1'),    insertName('N1'));
            testRunner.expectError(insertName('n1', 0), insertName('N1', 0));
        });
    });
*/

    @Test
    public void insertNameInsertName01() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  insertName('n1'),
        //  insertName('n2'));
        SheetHelper.runTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createInsertNameOp("n2", null, null).toString()
        );
    }

    @Test
    public void insertNameInsertName02() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  insertName('n1'),
        //  insertName('n1', 0));
        SheetHelper.runTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createInsertNameOp("n1", 0, null).toString()
        );
    }

    @Test
    public void insertNameInsertName03() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  insertName('n1', 0),
        //  insertName('n1'));
        SheetHelper.runTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertNameOp("n1", null, null).toString()
        );
    }

    @Test
    public void insertNameInsertName04() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  insertName('n1', 0),
        //  insertName('n1', 1));
        SheetHelper.runTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertNameOp("n1", 1, null).toString()
        );
    }

    @Test
    public void insertNameInsertName05() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  insertName('n1', 0),
        //  insertName('n2', 0));
        SheetHelper.runTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertNameOp("n2", 0, null).toString()
        );
    }

    @Test
    public void insertNameInsertName06() throws JSONException {
        // Result: Should fail to insert same name twice.
        // testRunner.expectError(
        //  insertName('n1'),
        //  insertName('n1'));
        SheetHelper.expectError(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createInsertNameOp("n1", null, null).toString()
        );
    }

    @Test
    public void insertNameInsertName07() throws JSONException {
        // Result: Should fail to insert same name twice.
        // testRunner.expectError(
        //  insertName('n1', 0),
        //  insertName('n1', 0));
        SheetHelper.expectError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertNameOp("n1", 0, null).toString()
        );
    }

    @Test
    public void insertNameInsertName08() throws JSONException {
        // Result: Should fail to insert same name twice.
        // testRunner.expectError(
        //  insertName('n1'),
        //  insertName('N1'));
        SheetHelper.expectError(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createInsertNameOp("N1", null, null).toString()
        );
    }

    @Test
    public void insertNameInsertName09() throws JSONException {
        // Result: Should fail to insert same name twice.
        // testRunner.expectError(
        //  insertName('n1', 0),
        //  insertName('N1', 0));
        SheetHelper.expectError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertNameOp("N1", 0, null).toString()
        );
    }

/*
    describe('"insertName" and "deleteName"', function () {
        it('should skip different names', function () {
            testRunner.runBidiTest(insertName('n1'),    deleteName('n2'));
            testRunner.runBidiTest(insertName('n1'),    deleteName('n1', 0));
            testRunner.runBidiTest(insertName('n1', 0), deleteName('n1'));
            testRunner.runBidiTest(insertName('n1', 0), deleteName('n1', 1));
            testRunner.runBidiTest(insertName('n1', 0), deleteName('n2', 0));
        });
        it('should handle same name', function () {
            testRunner.runBidiTest(insertName('n1'),    deleteName('n1'),    []);
            testRunner.runBidiTest(insertName('n1', 0), deleteName('n1', 0), []);
            testRunner.runBidiTest(insertName('n1'),    deleteName('N1'),    []);
            -testRunner.runBidiTest(insertName('n1', 0), deleteName('N1', 0), []);
        });
    });
*/

    @Test
    public void insertNameDeleteName01() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  deleteName('n2'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createDeleteNameOp("n2", null).toString()
        );
    }

    @Test
    public void insertNameDeleteName02() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  deleteName('n1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createDeleteNameOp("n2", 0).toString()
        );
    }

    @Test
    public void insertNameDeleteName03() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  deleteName('n1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createDeleteNameOp("n1", null).toString()
        );
    }

    @Test
    public void insertNameDeleteName04() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  deleteName('n1', 1));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createDeleteNameOp("n1", 1).toString()
        );
    }

    @Test
    public void insertNameDeleteName05() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  deleteName('n2', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createDeleteNameOp("n2", 0).toString()
        );
    }

    @Test
    public void insertNameDeleteName06() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  deleteName('n1'),    []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            "[]",
            SheetHelper.createDeleteNameOp("n1", null).toString()
        );
    }

    @Test
    public void insertNameDeleteName07() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  deleteName('n1', 0), []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            "[]",
            SheetHelper.createDeleteNameOp("n1", 0).toString()
        );
    }

    @Test
    public void insertNameDeleteName08() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  deleteName('N1'),    []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createDeleteNameOp("N1", null).toString(),
            "[]",
            SheetHelper.createDeleteNameOp("N1", null).toString()
        );
    }

    @Test
    public void insertNameDeleteName09() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  deleteName('N1', 0), []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createDeleteNameOp("N1", 0).toString(),
            "[]",
            SheetHelper.createDeleteNameOp("N1", 0).toString()
        );
    }

/*
    describe('"insertName" and "changeName"', function () {
        it('should skip different names', function () {
            testRunner.runBidiTest(insertName('n1'),    changeName('n2'));
            testRunner.runBidiTest(insertName('n1'),    changeName('n1', 0));
            testRunner.runBidiTest(insertName('n1'),    changeName('n2', 0, { newLabel: 'n1' }));
            testRunner.runBidiTest(insertName('n1', 0), changeName('n1'));
            testRunner.runBidiTest(insertName('n1', 0), changeName('n1', 1));
            testRunner.runBidiTest(insertName('n1', 0), changeName('n2', 0));
            testRunner.runBidiTest(insertName('n1', 0), changeName('n2', { newLabel: 'n1' }));
            testRunner.runBidiTest(insertName('n1', 0), changeName('n2', 1, { newLabel: 'n1' }));
        });
        it('should fail to insert same name again', function () {
            testRunner.expectBidiError(insertName('n1'),    changeName('n1'));
            testRunner.expectBidiError(insertName('n1', 0), changeName('n1', 0));
            testRunner.expectBidiError(insertName('n1'),    changeName('N1'));
            testRunner.expectBidiError(insertName('n1', 0), changeName('N1', 0));
        });
        it('should fail to insert and change name to same label', function () {
            testRunner.expectBidiError(insertName('n1'),    changeName('n2', { newLabel: 'n1' }));
            testRunner.expectBidiError(insertName('n1', 0), changeName('n2', 0, { newLabel: 'n1' }));
            testRunner.expectBidiError(insertName('n1'),    changeName('n2', { newLabel: 'N1' }));
            testRunner.expectBidiError(insertName('n1', 0), changeName('n2', 0, { newLabel: 'N1' }));
        });
    });
*/

    @Test
    public void insertNameChangeName01() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  changeName('n2'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeNameOp("n2", null, null).toString()
        );
    }

    @Test
    public void insertNameChangeName02() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  changeName('n1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeNameOp("n2", 0, null).toString()
        );
    }

    @Test
    public void insertNameChangeName03() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  changeName('n2', 0, { newLabel: 'n1' }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeNameOp("n2", 0, "{ newLabel: 'n1' }").toString()
        );
    }

    @Test
    public void insertNameChangeName04() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  changeName('n1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeNameOp("n1", null, null).toString()
        );
    }

    @Test
    public void insertNameChangeName05() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  changeName('n1', 1));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeNameOp("n1", 1, null).toString()
        );
    }

    @Test
    public void insertNameChangeName06() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  changeName('n2', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeNameOp("n2", 0, null).toString()
        );
    }

    @Test
    public void insertNameChangeName07() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  changeName('n2', { newLabel: 'n1' }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeNameOp("n2", null, "{ newLabel: 'n1' }").toString()
        );
    }

    @Test
    public void insertNameChangeName08() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  changeName('n2', 1, { newLabel: 'n1' }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'n1' }").toString()
        );
    }

    @Test
    public void insertNameChangeName09() throws JSONException {
        // Result: Should fail to insert same name again.
        // testRunner.expectBidiError(
        //  insertName('n1'),
        //  changeName('n1'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeNameOp("n1", null, null).toString()
        );
    }

    @Test
    public void insertNameChangeName10() throws JSONException {
        // Result: Should fail to insert same name again.
        // testRunner.expectBidiError(
        //  insertName('n1', 0),
        //  changeName('n1', 0));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeNameOp("n1", 0, null).toString()
        );
    }

    @Test
    public void insertNameChangeName11() throws JSONException {
        // Result: Should fail to insert same name again.
        // testRunner.expectBidiError(
        //  insertName('n1'),
        //  changeName('N1'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeNameOp("N1", null, null).toString()
        );
    }

    @Test
    public void insertNameChangeName12() throws JSONException {
        // Result: Should fail to insert same name again.
        // testRunner.expectBidiError(
        //  insertName('n1', 0),
        //  changeName('N1', 0));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeNameOp("N1", 0, null).toString()
        );
    }

    @Test
    public void insertNameChangeName13() throws JSONException {
        // Result: Should fail to insert and change name to same label.
        // testRunner.expectBidiError(
        //  insertName('n1'),
        //  changeName('n2', { newLabel: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeNameOp("n2", null, "{ newLabel: 'n1' }").toString()
        );
    }

    @Test
    public void insertNameChangeName14() throws JSONException {
        // Result: Should fail to insert and change name to same label.
        // testRunner.expectBidiError(
        //  insertName('n1', 0),
        //  changeName('n2', 0, { newLabel: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeNameOp("n2", 0, "{ newLabel: 'n1' }").toString()
        );
    }

    @Test
    public void insertNameChangeName15() throws JSONException {
        // Result: Should fail to insert and change name to same label.
        // testRunner.expectBidiError(
        //  insertName('n1'),
        //  changeName('n2', { newLabel: 'N1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeNameOp("n2", null, "{ newLabel: 'N1' }").toString()
        );
    }

    @Test
    public void insertNameChangeName16() throws JSONException {
        // Result: Should fail to insert and change name to same label.
        // testRunner.expectBidiError(
        //  insertName('n1', 0),
        //  changeName('n2', 0, { newLabel: 'N1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeNameOp("n2", 0, "{ newLabel: 'N1' }").toString()
        );
    }

/*
    describe('"insertName" and "insertTable"', function () {
        it('should skip different names', function () {
            testRunner.runBidiTest(insertName('n1'),    insertTable(0, null, 'A1:D4'));
            testRunner.runBidiTest(insertName('n1'),    insertTable(0, 'n2', 'A1:D4'));
            testRunner.runBidiTest(insertName('n1', 0), insertTable(0, 'n2', 'A1:D4'));
            testRunner.runBidiTest(insertName('n1', 1), insertTable(0, 'n2', 'A1:D4'));
        });
        it('should fail to insert name and table with same label', function () {
            testRunner.expectBidiError(insertName('n1'),    insertTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(insertName('n1', 0), insertTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(insertName('n1', 1), insertTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(insertName('N1', 1), insertTable(0, 'n1', 'A1:D4'));
        });
    });
*/

    @Test
    public void insertNameInsertTable01() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  insertTable(0, null, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createInsertTableOp(0, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameInsertTable02() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  insertTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createInsertTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameInsertTable03() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  insertTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameInsertTable04() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  insertName('n1', 1),
        //  insertTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 1, null).toString(),
            SheetHelper.createInsertTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameInsertTable05() throws JSONException {
        // Result: Should fail to insert name and table with same label.
        // testRunner.expectBidiError(
        //  insertName('n1'),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 1, null).toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameInsertTable06() throws JSONException {
        // Result: Should fail to insert name and table with same label.
        // testRunner.expectBidiError(
        //  insertName('n1', 0),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameInsertTable07() throws JSONException {
        // Result: Should fail to insert name and table with same label.
        // testRunner.expectBidiError(
        //  insertName('n1', 0),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameInsertTable08() throws JSONException {
        // Result: Should fail to insert name and table with same label.
        // testRunner.expectBidiError(
        //  insertName('N1', 1),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("N1", 1, null).toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

/*
    describe('"insertName" and "changeTable"', function () {
        it('should skip different labels', function () {
            testRunner.runBidiTest(insertName('n1'),    changeTable(0, null, 'A1:D4'));
            testRunner.runBidiTest(insertName('n1'),    changeTable(0, 'n2', 'A1:D4'));
            testRunner.runBidiTest(insertName('n1', 0), changeTable(0, 'n2', 'A1:D4'));
            testRunner.runBidiTest(insertName('n1', 1), changeTable(0, 'n2', 'A1:D4'));
        });
        it('should fail to insert name for existing table', function () {
            testRunner.expectBidiError(insertName('n1'),    changeTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(insertName('n1', 0), changeTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(insertName('n1', 1), changeTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(insertName('N1', 1), changeTable(0, 'n1', 'A1:D4'));
        });
        it('should fail to insert name and change table to same label', function () {
            testRunner.expectBidiError(insertName('n1'),    changeTable(0, 'n2', { newName: 'n1' }));
            testRunner.expectBidiError(insertName('n1', 0), changeTable(0, 'n2', { newName: 'n1' }));
            testRunner.expectBidiError(insertName('n1', 1), changeTable(0, 'n2', { newName: 'n1' }));
            testRunner.expectBidiError(insertName('N1', 1), changeTable(0, 'n2', { newName: 'n1' }));
        });
    });
*/

    @Test
    public void insertNameChangeTable01() throws JSONException {
        // Result: Should skip different labels.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  changeTable(0, null, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeTableOp(0, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameChangeTable02() throws JSONException {
        // Result: Should skip different labels.
        // testRunner.runBidiTest(
        //  insertName('n1'),
        //  changeTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameChangeTable03() throws JSONException {
        // Result: Should skip different labels.
        // testRunner.runBidiTest(
        //  insertName('n1', 0),
        //  changeTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameChangeTable04() throws JSONException {
        // Result: Should skip different labels.
        // testRunner.runBidiTest(
        //  insertName('n1', 1),
        //  changeTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNameOp("n1", 1, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameChangeTable05() throws JSONException {
        // Result: Should fail to insert name for existing table.
        // testRunner.expectBidiError(
        //  insertName('n1'),
        //  changeTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", null, null).toString(),
            SheetHelper.createChangeTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameChangeTable06() throws JSONException {
        // Result: Should fail to insert name for existing table.
        // testRunner.expectBidiError(
        //  insertName('n1', 0),
        //  changeTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameChangeTable07() throws JSONException {
        // Result: Should fail to insert name for existing table.
        // testRunner.expectBidiError(
        //  insertName('n1', 1),
        //  changeTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 1, null).toString(),
            SheetHelper.createChangeTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameChangeTable08() throws JSONException {
        // Result: Should fail to insert name for existing table.
        // testRunner.expectBidiError(
        //  insertName('N1', 1),
        //  changeTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("N1", 1, null).toString(),
            SheetHelper.createChangeTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertNameChangeTable09() throws JSONException {
        // Result: Should fail to insert name for existing table.
        // testRunner.expectBidiError(
        //  insertName('n1'),
        //  changeTable(0, 'n2', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 1, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", null, "{ newName: 'n1' }").toString()
        );
    }

    @Test
    public void insertNameChangeTable10() throws JSONException {
        // Result: Should fail to insert name for existing table.
        // testRunner.expectBidiError(
        //  insertName('n1', 0),
        //  changeTable(0, 'n2', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", null, "{ newName: 'n1' }").toString()
        );
    }

    @Test
    public void insertNameChangeTable11() throws JSONException {
        // Result: Should fail to insert name for existing table.
        // testRunner.expectBidiError(
        //  insertName('N1', 1),
        //  changeTable(0, 'n2', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNameOp("N1", 1, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", null, "{ newName: 'n1' }").toString()
        );
    }

    // deleteName ---------------------------------------------------------

/*
    describe('"deleteName" and "deleteName"', function () {
        it('should skip different names', function () {
            testRunner.runTest(deleteName('n1'),    deleteName('n2'));
            testRunner.runTest(deleteName('n1'),    deleteName('n1', 0));
            testRunner.runTest(deleteName('n1', 0), deleteName('n1'));
            testRunner.runTest(deleteName('n1', 0), deleteName('n1', 1));
            testRunner.runTest(deleteName('n1', 0), deleteName('n2', 0));
        });
        it('should handle same name', function () {
            testRunner.runTest(deleteName('n1'),    deleteName('n1'),    [], []);
            testRunner.runTest(deleteName('n1', 0), deleteName('n1', 0), [], []);
            testRunner.runTest(deleteName('n1', 0), deleteName('N1', 0), [], []);
        });
    });
*/

    @Test
    public void deleteNameDeleteName01() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  deleteName('n1'),
        //  deleteName('n2'));
        SheetHelper.runTest(
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            SheetHelper.createDeleteNameOp("n2", null).toString()
        );
    }

    @Test
    public void deleteNameDeleteName02() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  deleteName('n1'),
        //  deleteName('n1', 0));
        SheetHelper.runTest(
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            SheetHelper.createDeleteNameOp("n1", 0).toString()
        );
    }

    @Test
    public void deleteNameDeleteName03() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  deleteName('n1', 0),
        //  deleteName('n1'));
        SheetHelper.runTest(
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            SheetHelper.createDeleteNameOp("n1", null).toString()
        );
    }

    @Test
    public void deleteNameDeleteName04() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  deleteName('n1', 0),
        //  deleteName('n1', 1));
        SheetHelper.runTest(
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            SheetHelper.createDeleteNameOp("n1", 1).toString()
        );
    }

    @Test
    public void deleteNameDeleteName05() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  deleteName('n1', 0),
        //  deleteName('n2', 0));
        SheetHelper.runTest(
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            SheetHelper.createDeleteNameOp("n2", 0).toString()
        );
    }

    @Test
    public void deleteNameDeleteName06() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runTest(
        //  deleteName('n1'),
        //  deleteName('n1'),    [], []);
        SheetHelper.runTest(
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteNameDeleteName07() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runTest(
        //  deleteName('n1', 0),
        //  deleteName('N1', 0), [], []);
        SheetHelper.runTest(
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            SheetHelper.createDeleteNameOp("N1", 0).toString(),
            "[]",
            "[]"
        );
    }

/*
    describe('"deleteName" and "changeName"', function () {
        it('should skip different names', function () {
            testRunner.runBidiTest(deleteName('n1'),    changeName('n2'));
            testRunner.runBidiTest(deleteName('n1'),    changeName('n1', 0));
            testRunner.runBidiTest(deleteName('n1', 0), changeName('n1'));
            testRunner.runBidiTest(deleteName('n1', 0), changeName('n1', 1));
            testRunner.runBidiTest(deleteName('n1', 0), changeName('n2', 0));
        });
        it('should handle same name', function () {
            testRunner.runBidiTest(deleteName('n1'),    changeName('n1'),    null, []);
            testRunner.runBidiTest(deleteName('n1', 0), changeName('n1', 0), null, []);
            testRunner.runBidiTest(deleteName('n1', 0), changeName('N1', 0), null, []);
        });
        it('should update new label in operations', function () {
            testRunner.runBidiTest(deleteName('n1'), changeName('n1', { newLabel: 'n2' }), deleteName('n2'), []);
        });
    });
*/

    @Test
    public void deleteNameChangeName01() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  deleteName('n1'),
        //  changeName('n2'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            SheetHelper.createChangeNameOp("n2", null, null).toString()
        );
    }

    @Test
    public void deleteNameChangeName02() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  deleteName('n1'),
        //  changeName('n1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            SheetHelper.createChangeNameOp("n2", 0, null).toString()
        );
    }

    @Test
    public void deleteNameChangeName03() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  deleteName('n1', 0),
        //  changeName('n1'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            SheetHelper.createChangeNameOp("n1", null, null).toString()
        );
    }

    @Test
    public void deleteNameChangeName04() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  deleteName('n1', 0),
        //  changeName('n1', 1));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            SheetHelper.createChangeNameOp("n1", 1, null).toString()
        );
    }

    @Test
    public void deleteNameChangeName05() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  deleteName('n1', 0),
        //  changeName('n2', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            SheetHelper.createChangeNameOp("n2", 0, null).toString()
        );
    }

    @Test
    public void deleteNameChangeName06() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  deleteName('n1'),
        //  changeName('n1'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            SheetHelper.createChangeNameOp("n1", null, null).toString(),
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            "[]"
        );
    }

    @Test
    public void deleteNameChangeName07() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  deleteName('n1', 0),
        //  changeName('n1', 0),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            SheetHelper.createChangeNameOp("n1", 0, null).toString(),
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            "[]"
        );
    }

    @Test
    public void deleteNameChangeName08() throws JSONException {
        // Result: Should handle same name.
        // testRunner.runBidiTest(
        //  deleteName('n1', 0),
        //  changeName('N1', 0),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            SheetHelper.createChangeNameOp("N1", 0, null).toString(),
            SheetHelper.createDeleteNameOp("n1", 0).toString(),
            "[]"
        );
    }

    @Test
    public void deleteNameChangeName09() throws JSONException {
        // Result: Should update new label in operations.
        // testRunner.runBidiTest(
        //  deleteName('n1'),
        //  changeName('n1', { newLabel: 'n2' }),
        //  deleteName('n2'),
        // []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNameOp("n1", null).toString(),
            SheetHelper.createChangeNameOp("n1", null, "{ newLabel: 'n2' }").toString(),
            SheetHelper.createDeleteNameOp("n2", null).toString(),
            "[]"
        );
    }

    // changeName ---------------------------------------------------------

/*
    describe('"changeName" and "changeName"', function () {
        it('should skip different names', function () {
            testRunner.runTest(changeName('n1', { formula: '1' }), changeName('n2', { formula: '1' }));
            testRunner.runTest(changeName('n1', 0, { formula: '1' }), changeName('n1', { formula: '1' }));
            testRunner.runTest(changeName('n1', 0, { formula: '1' }), changeName('n1', 1, { formula: '1' }));
        });
        it('should reduce formula expression', function () {
            testRunner.runTest(changeName('n1', { formula: 'A1', ref: 'A1', isExpr: true }), changeName('n1', { formula: 'A2', ref: 'A1', isExpr: true }), null, []);
            testRunner.runTest(changeName('n1', { formula: 'A1', ref: 'A1', isExpr: true }), changeName('n1', { formula: 'A1', ref: 'A1', isExpr: true }), [], []);
            testRunner.runTest(changeName('n1', { formula: 'A1', ref: 'A1', isExpr: true }), changeName('N1', { formula: 'A1', ref: 'A1', isExpr: true }), [], []);
        });
        it('should fail to change different names in same parent to the same label', function () {
            testRunner.expectError(changeName('n1', 1, { newLabel: 'n3' }), changeName('n2', 1, { newLabel: 'n3' }));
            testRunner.expectError(changeName('n1', 1, { newLabel: 'n3' }), changeName('n2', 1, { newLabel: 'N3' }));
        });
        it('should allow to change names in different parents to the same label', function () {
            testRunner.runTest(changeName('n1', { newLabel: 'n3' }), changeName('n1', 1, { newLabel: 'n3' }));
            testRunner.runTest(changeName('n1', 1, { newLabel: 'n3' }), changeName('n1', 2, { newLabel: 'n3' }));
        });
        it('should update label in operations', function () {
            testRunner.runBidiTest(
                changeName('n1', { newLabel: 'n2' }), changeName('n1', { formula: '' }),
                changeName('n1', { newLabel: 'n2' }), changeName('n2', { formula: '' })
            );
            testRunner.runTest(
                changeName('n1', { newLabel: 'n2' }), changeName('n1', { newLabel: 'n3', formula: '' }),
                changeName('n3', { newLabel: 'n2' }), changeName('n2', { formula: '' })
            );
            -testRunner.runTest(
                changeName('n1', { newLabel: 'n2' }), changeName('n1', { newLabel: 'n3' }),
                changeName('n3', { newLabel: 'n2' }), []
            );
        });
    });
*/

    @Test
    public void changeNameChangeName01() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  changeName('n1', { formula: '1' }),
        //  changeName('n2', { formula: '1' }));
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", null, "{ formula: '1' }").toString(),
            SheetHelper.createChangeNameOp("n2", null, "{ formula: '1' }").toString()
        );
    }

    @Test
    public void changeNameChangeName02() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  changeName('n1', 0, { formula: '1' }),
        //  changeName('n1', { formula: '1' }));
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", 0, "{ formula: '1' }").toString(),
            SheetHelper.createChangeNameOp("n1", null, "{ formula: '1' }").toString()
        );
    }

    @Test
    public void changeNameChangeName03() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runTest(
        //  changeName('n1', 0, { formula: '1' }),
        //  changeName('n1', 1, { formula: '1' }));
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", 0, "{ formula: '1' }").toString(),
            SheetHelper.createChangeNameOp("n1", 1, "{ formula: '1' }").toString()
        );
    }

    @Test
    public void changeNameChangeName04() throws JSONException {
        // Result: Should reduce formula expression.
        // testRunner.runTest(
        //  changeName('n1', { formula: 'A1', ref: 'A1', isExpr: true }),
        //  changeName('n1', { formula: 'A2', ref: 'A1', isExpr: true }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", null, "{ formula: 'A1', ref: 'A1', isExpr: true }").toString(),
            SheetHelper.createChangeNameOp("n1", null, "{ formula: 'A2', ref: 'A1', isExpr: true }").toString(),
            SheetHelper.createChangeNameOp("n1", null, "{ formula: 'A1', ref: 'A1', isExpr: true }").toString(),
            "[]"
        );
    }

    @Test
    public void changeNameChangeName05() throws JSONException {
        // Result: Should reduce formula expression.
        // testRunner.runTest(
        //  changeName('n1', { formula: 'A1', ref: 'A1', isExpr: true }),
        //  changeName('n1', { formula: 'A1', ref: 'A1', isExpr: true }),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", null, "{ formula: 'A1', ref: 'A1', isExpr: true }").toString(),
            SheetHelper.createChangeNameOp("n1", null, "{ formula: 'A1', ref: 'A1', isExpr: true }").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeNameChangeName06() throws JSONException {
        // Result: Should reduce formula expression.
        // testRunner.runTest(
        //  changeName('n1', { formula: 'A1', ref: 'A1', isExpr: true }),
        //  changeName('N1', { formula: 'A1', ref: 'A1', isExpr: true }),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", null, "{ formula: 'A1', ref: 'A1', isExpr: true }").toString(),
            SheetHelper.createChangeNameOp("N1", null, "{ formula: 'A1', ref: 'A1', isExpr: true }").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeNameChangeName07() throws JSONException {
        // Result: Should fail to change different names in same parent to the same label.
        // testRunner.expectError(
        //  changeName('n1', 1, { newLabel: 'n3' }),
        //  changeName('n2', 1, { newLabel: 'n3' }));
        SheetHelper.expectError(
            SheetHelper.createChangeNameOp("n1", 1, "{ newLabel: 'n3' }").toString(),
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'n3' }").toString()
        );
    }

    @Test
    public void changeNameChangeName08() throws JSONException {
        // Result: Should fail to change different names in same parent to the same label.
        // testRunner.expectError(
        //  changeName('n1', 1, { newLabel: 'n3' }),
        //  changeName('n2', 1, { newLabel: 'N3' }));
        SheetHelper.expectError(
            SheetHelper.createChangeNameOp("n1", 1, "{ newLabel: 'n3' }").toString(),
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'N3' }").toString()
        );
    }

    @Test
    public void changeNameChangeName09() throws JSONException {
        // Result: Should allow to change names in different parents to the same label.
        // testRunner.runTest(
        //  changeName('n1', { newLabel: 'n3' }),
        //  changeName('n1', 1, { newLabel: 'n3' }));
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", null, "{ newLabel: 'n3' }").toString(),
            SheetHelper.createChangeNameOp("n1", 1, "{ newLabel: 'n3' }").toString()
        );
    }

    @Test
    public void changeNameChangeName10() throws JSONException {
        // Result: Should allow to change names in different parents to the same label.
        // testRunner.runTest(
        //  changeName('n1', 1, { newLabel: 'n3' }),
        //  changeName('n1', 2, { newLabel: 'n3' }));
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", 1, "{ newLabel: 'n3' }").toString(),
            SheetHelper.createChangeNameOp("n1", 2, "{ newLabel: 'n3' }").toString()
        );
    }

    @Test
    public void changeNameChangeName11() throws JSONException {
        // Result: Should update label in operations.
        // testRunner.runBidiTest(
        //  changeName('n1', { newLabel: 'n2' }),
        //  changeName('n1', { formula: '' }),
        //  changeName('n1', { newLabel: 'n2' }),
        //  changeName('n2', { formula: '' }));
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", null, "{ newLabel: 'n2' }").toString(),
            SheetHelper.createChangeNameOp("n1", null, "{ formula: '' }").toString(),
            SheetHelper.createChangeNameOp("n1", null, "{ newLabel: 'n2' }").toString(),
            SheetHelper.createChangeNameOp("n2", null, "{ formula: '' }").toString()
        );
    }

    @Test
    public void changeNameChangeName12() throws JSONException {
        // Result: Should update label in operations.
        // testRunner.runTest(
        //  changeName('n1', { newLabel: 'n2' }),
        //  changeName('n1', { newLabel: 'n3', formula: '' }),
        //  changeName('n3', { newLabel: 'n2' }),
        //  changeName('n2', { formula: '' }));
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", null, "{ newLabel: 'n2' }").toString(),
            SheetHelper.createChangeNameOp("n1", null, "{ newLabel: 'n3', formula: '' }").toString(),
            SheetHelper.createChangeNameOp("n3", null, "{ newLabel: 'n2' }").toString(),
            SheetHelper.createChangeNameOp("n2", null, "{ formula: '' }").toString()
        );
    }

    @Test
    public void changeNameChangeName13() throws JSONException {
        // Result: Should update label in operations.
        // testRunner.runTest(
        //  changeName('n1', { newLabel: 'n2' }),
        //  changeName('n1', { newLabel: 'n3' }),
        //  changeName('n3', { newLabel: 'n2' }),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", null, "{ newLabel: 'n2' }").toString(),
            SheetHelper.createChangeNameOp("n1", null, "{ newLabel: 'n3' }").toString(),
            SheetHelper.createChangeNameOp("n3", null, "{ newLabel: 'n2' }").toString(),
            "[]"
        );
    }

/*
    describe('"changeName" and "insertTable"', function () {
        it('should skip different names', function () {
            testRunner.runBidiTest(changeName('n1'),    insertTable(0, null, 'A1:D4'));
            testRunner.runBidiTest(changeName('n1'),    insertTable(0, 'n2', 'A1:D4'));
            testRunner.runBidiTest(changeName('n1', 0), insertTable(0, 'n2', 'A1:D4'));
            testRunner.runBidiTest(changeName('n1', 1), insertTable(0, 'n2', 'A1:D4'));
        });
        it('should fail to insert table for existing name', function () {
            testRunner.expectBidiError(changeName('n1'),    insertTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(changeName('n1', 0), insertTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(changeName('n1', 1), insertTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(changeName('N1', 1), insertTable(0, 'n1', 'A1:D4'));
        });
        it('should fail to insert table and give a name the same label', function () {
            testRunner.expectBidiError(changeName('n2', { newLabel: 'n1' }),    insertTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(changeName('n2', 0, { newLabel: 'n1' }), insertTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(changeName('n2', 1, { newLabel: 'n1' }), insertTable(0, 'n1', 'A1:D4'));
            -testRunner.expectBidiError(changeName('n2', 1, { newLabel: 'N1' }), insertTable(0, 'n1', 'A1:D4'));
        });
    });
*/
    @Test
    public void changeNameInsertTable01() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  changeName('n1'),
        //  insertTable(0, null, 'A1:D4'));
        SheetHelper.runTest(
            SheetHelper.createChangeNameOp("n1", null, null).toString(),
            SheetHelper.createInsertTableOp(0, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable02() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  changeName('n1'),
        //  insertTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeNameOp("n1", null, null).toString(),
            SheetHelper.createInsertTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable03() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  changeName('n1', 0),
        //  insertTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable04() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  changeName('n1', 1),
        //  insertTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeNameOp("n1", 1, null).toString(),
            SheetHelper.createInsertTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable05() throws JSONException {
        // Result: Should fail to insert table for existing name.
        // testRunner.expectBidiError(
        //  changeName('n1'),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n1", null, null).toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable06() throws JSONException {
        // Result: Should fail to insert table for existing name.
        // testRunner.expectBidiError(
        //  changeName('n1', 0),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n1", 0, null).toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable07() throws JSONException {
        // Result: Should fail to insert table for existing name.
        // testRunner.expectBidiError(
        //  changeName('n1', 1),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n1", 1, null).toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable08() throws JSONException {
        // Result: Should fail to insert table for existing name.
        // testRunner.expectBidiError(
        //  changeName('N1', 1),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("N1", 1, null).toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable09() throws JSONException {
        // Result: Should fail to insert table and give a name the same label.
        // testRunner.expectBidiError(
        //  changeName('n2', { newLabel: 'n1' }),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", null, "{ newLabel: 'n1' }").toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable10() throws JSONException {
        // Result: Should fail to insert table and give a name the same label.
        // testRunner.expectBidiError(
        //  changeName('n2', 0, { newLabel: 'n1' }),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 0, "{ newLabel: 'n1' }").toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable11() throws JSONException {
        // Result: Should fail to insert table and give a name the same label.
        // testRunner.expectBidiError(
        //  changeName('n2', 1, { newLabel: 'n1' }),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'n1' }").toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameInsertTable12() throws JSONException {
        // Result: Should fail to insert table and give a name the same label.
        // testRunner.expectBidiError(
        //  changeName('n2', 1, { newLabel: 'N1' }),
        //  insertTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'N1' }").toString(),
            SheetHelper.createInsertTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

/*
    describe('"changeName" and "changeTable"', function () {
        it('should skip different names', function () {
            testRunner.runBidiTest(changeName('n1'),    changeTable(0, null, 'A1:D4'));
            testRunner.runBidiTest(changeName('n1'),    changeTable(0, 'n2', 'A1:D4'));
            testRunner.runBidiTest(changeName('n1', 0), changeTable(0, 'n2', 'A1:D4'));
            testRunner.runBidiTest(changeName('n1', 1), changeTable(0, 'n2', 'A1:D4'));
        });
        it('should fail to change table name for existing name', function () {
            testRunner.expectBidiError(changeName('n1'),    changeTable(0, 'n2', { newName: 'n1' }));
            testRunner.expectBidiError(changeName('n1', 0), changeTable(0, 'n2', { newName: 'n1' }));
            testRunner.expectBidiError(changeName('n1', 1), changeTable(0, 'n2', { newName: 'n1' }));
            testRunner.expectBidiError(changeName('N1', 1), changeTable(0, 'n2', { newName: 'n1' }));
        });
        it('should fail to change name label for existing table', function () {
            testRunner.expectBidiError(changeName('n2', { newLabel: 'n1' }),    changeTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(changeName('n2', 0, { newLabel: 'n1' }), changeTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(changeName('n2', 1, { newLabel: 'n1' }), changeTable(0, 'n1', 'A1:D4'));
            testRunner.expectBidiError(changeName('n2', 1, { newLabel: 'N1' }), changeTable(0, 'n1', 'A1:D4'));
        });
        it('should fail to give table and name the same label', function () {
            testRunner.expectBidiError(changeName('n2', { newLabel: 'n1' }),    changeTable(0, 'n3', { newName: 'n1' }));
            testRunner.expectBidiError(changeName('n2', 0, { newLabel: 'n1' }), changeTable(0, 'n3', { newName: 'n1' }));
            testRunner.expectBidiError(changeName('n2', 1, { newLabel: 'n1' }), changeTable(0, 'n3', { newName: 'n1' }));
            -testRunner.expectBidiError(changeName('n2', 1, { newLabel: 'N1' }), changeTable(0, 'n3', { newName: 'n1' }));
        });
    });
*/

    @Test
    public void changeNameChangeTable01() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  changeName('n1'),
        //  changeTable(0, null, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeNameOp("n1", null, null).toString(),
            SheetHelper.createChangeTableOp(0, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameChangeTable02() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  changeName('n1'),
        //  changeTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeNameOp("n1", null, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameChangeTable03() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  changeName('n1', 0),
        //  changeTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeNameOp("n1", null, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameChangeTable04() throws JSONException {
        // Result: Should skip different names.
        // testRunner.runBidiTest(
        //  changeName('n1', 1),
        //  changeTable(0, 'n2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeNameOp("n1", 1, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameChangeTable05() throws JSONException {
        // Result: Should fail to change table name for existing name.
        // testRunner.expectBidiError(
        //  changeName('n1'),
        //  changeTable(0, 'n2', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n1", 1, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", "A1:D4", "{ newName: 'n1' }").toString()
        );
    }

    @Test
    public void changeNameChangeTable06() throws JSONException {
        // Result: Should fail to change table name for existing name.
        // testRunner.expectBidiError(
        //  changeName('n1', 0),
        //  changeTable(0, 'n2', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n1", 0, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", null, "{ newName: 'n1' }").toString()
        );
    }

    @Test
    public void changeNameChangeTable07() throws JSONException {
        // Result: Should fail to change table name for existing name.
        // testRunner.expectBidiError(
        //  changeName('n1', 1),
        //  changeTable(0, 'n2', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n1", 1, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", null, "{ newName: 'n1' }").toString()
        );
    }

    @Test
    public void changeNameChangeTable08() throws JSONException {
        // Result: Should fail to change table name for existing name.
        // testRunner.expectBidiError(
        //  changeName('N1', 1),
        //  changeTable(0, 'n2', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("N1", 1, null).toString(),
            SheetHelper.createChangeTableOp(0, "n2", null, "{ newName: 'n1' }").toString()
        );
    }

    @Test
    public void changeNameChangeTable09() throws JSONException {
        // Result: Should fail to change name label for existing table.
        // testRunner.expectBidiError(
        //  changeName('n2', { newLabel: 'n1' }),
        //  changeTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", null, "{ newLabel: 'n1' }").toString(),
            SheetHelper.createChangeTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameChangeTable10() throws JSONException {
        // Result: Should fail to change name label for existing table.
        // testRunner.expectBidiError(
        //  changeName('n2', 0, { newLabel: 'n1' }),
        //  changeTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 0, "{ newLabel: 'n1' }").toString(),
            SheetHelper.createChangeTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameChangeTable11() throws JSONException {
        // Result: Should fail to change name label for existing table.
        // testRunner.expectBidiError(
        //  changeName('n2', 1, { newLabel: 'n1' }),
        //  changeTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'n1' }").toString(),
            SheetHelper.createChangeTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameChangeTable12() throws JSONException {
        // Result: Should fail to change name label for existing table.
        // testRunner.expectBidiError(
        //  changeName('n2', 1, { newLabel: 'N1' }),
        //  changeTable(0, 'n1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'N1' }").toString(),
            SheetHelper.createChangeTableOp(0, "n1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeNameChangeTable13() throws JSONException {
        // Result: Should fail to give table and name the same label.
        // testRunner.expectBidiError(
        //  changeName('n2', { newLabel: 'n1' }),
        //  changeTable(0, 'n3', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'n1' }").toString(),
            SheetHelper.createChangeTableOp(0, "n3", null, "{ newName: 'n1' }").toString()
        );
    }

    @Test
    public void changeNameChangeTable14() throws JSONException {
        // Result: Should fail to give table and name the same label.
        // testRunner.expectBidiError(
        //  changeName('n2', 0, { newLabel: 'n1' }),
        //  changeTable(0, 'n3', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 0, "{ newLabel: 'n1' }").toString(),
            SheetHelper.createChangeTableOp(0, "n3", null, "{ newName: 'n1' }").toString()
        );
    }

    @Test
    public void changeNameChangeTable15() throws JSONException {
        // Result: Should fail to give table and name the same label.
        // testRunner.expectBidiError(
        //  changeName('n2', 1, { newLabel: 'n1' }),
        //  changeTable(0, 'n3', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'n1' }").toString(),
            SheetHelper.createChangeTableOp(0, "n3", null, "{ newName: 'n1' }").toString()
        );
    }

    @Test
    public void changeNameChangeTable16() throws JSONException {
        // Result: Should fail to give table and name the same label.
        // testRunner.expectBidiError(
        //  changeName('n2', 1, { newLabel: 'N1' }),
        //  changeTable(0, 'n3', { newName: 'n1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeNameOp("n2", 1, "{ newLabel: 'N1' }").toString(),
            SheetHelper.createChangeTableOp(0, "n3", null, "{ newName: 'n1' }").toString()
        );
    }

}
