/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class InsertDeleteComment {

    /*
     * describe('"insertComment" and "deleteComment"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(2, 'A1', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(2, 'B2', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(2, 'A1', 2));
                testRunner.runBidiTest(insertComment(1, 'A1', 2), deleteComment(2, 'A1', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 2), deleteComment(2, 'A1', 2));
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(2, 'B2', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(2, 'B2', 2));
                testRunner.runBidiTest(insertComment(1, 'A1', 2), deleteComment(2, 'B2', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 2), deleteComment(2, 'B2', 2));
            });
            it('should shift comment index', function () {
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(1, 'A1', 1), insertComment(1, 'A1', 0), deleteComment(1, 'A1', 2));
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(1, 'A1', 2), insertComment(1, 'A1', 0), deleteComment(1, 'A1', 3));
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(1, 'A1', 3), insertComment(1, 'A1', 0), deleteComment(1, 'A1', 4));
                testRunner.runBidiTest(insertComment(1, 'A1', 1), deleteComment(1, 'A1', 1), insertComment(1, 'A1', 1), deleteComment(1, 'A1', 2));
                testRunner.runBidiTest(insertComment(1, 'A1', 1), deleteComment(1, 'A1', 2), insertComment(1, 'A1', 1), deleteComment(1, 'A1', 3));
                testRunner.runBidiTest(insertComment(1, 'A1', 1), deleteComment(1, 'A1', 3), insertComment(1, 'A1', 1), deleteComment(1, 'A1', 4));
                testRunner.runBidiTest(insertComment(1, 'A1', 2), deleteComment(1, 'A1', 1), insertComment(1, 'A1', 1), deleteComment(1, 'A1', 1));
                testRunner.runBidiTest(insertComment(1, 'A1', 2), deleteComment(1, 'A1', 2), insertComment(1, 'A1', 2), deleteComment(1, 'A1', 3));
                testRunner.runBidiTest(insertComment(1, 'A1', 2), deleteComment(1, 'A1', 3), insertComment(1, 'A1', 2), deleteComment(1, 'A1', 4));
            });
            it('should handle deleted threads', function () {
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(1, 'A1', 0), insertComment(1, 'A1', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 1), deleteComment(1, 'A1', 0), insertComment(1, 'A1', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 2), deleteComment(1, 'A1', 0), insertComment(1, 'A1', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 3), deleteComment(1, 'A1', 0), insertComment(1, 'A1', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 4), deleteComment(1, 'A1', 0), insertComment(1, 'A1', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 0), deleteComment(1, 'A1', 0), []);
                testRunner.runBidiTest(insertComment(1, 'A1', 1), deleteComment(1, 'A1', 0), []);
                -testRunner.runBidiTest(insertComment(1, 'A1', 2), deleteComment(1, 'A1', 0), []);
            });
        });
    */

    @Test
    public void insertCommentDeleteComment01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(2, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(2, "A1", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(2, 'B2', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(2, "B2", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment03() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 0),
        //  deleteComment(1, 'A1', 2));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 2).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment04() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 0),
        //  deleteComment(1, 'A1', 3));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 2).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 3).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment05() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(1, 'A1', 3),
        //  insertComment(1, 'A1', 0),
        //  deleteComment(1, 'A1', 4));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 3).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 4).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment06() throws JSONException {
        // Result: Should handle deleted threads.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(1, 'A1', 0),
        //  insertComment(1, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            "[]",
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment07() throws JSONException {
        // Result: Should handle deleted threads.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 1),
        //  deleteComment(1, 'A1', 0),
        //  insertComment(1, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            "[]",
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment08() throws JSONException {
        // Result: Should handle deleted threads.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 2),
        //  deleteComment(1, 'A1', 0),
        //  insertComment(1, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            "[]",
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment09() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(2, 'A1', 2));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(2, "A1", 2).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment10() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 2),
        //  deleteComment(2, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createDeleteCommentOp(2, "A1", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment11() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 2),
        //  deleteComment(2, 'A1', 2));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createDeleteCommentOp(2, "A1", 2).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment12() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(2, 'B2', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(2, "B2", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment13() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(2, 'B2', 2));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(2, "B2", 2).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment14() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 2),
        //  deleteComment(2, 'B2', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createDeleteCommentOp(2, "B2", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment15() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 2),
        //  deleteComment(2, 'B2', 2));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createDeleteCommentOp(2, "B2", 2).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment16() throws JSONException {
        // Result: Should handle deleted threads.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  deleteComment(1, 'A1', 0),
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            "[]",
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment17() throws JSONException {
        // Result: Should handle deleted threads.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 1),
        //  deleteComment(1, 'A1', 0),
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            "[]",
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString()
        );
    }

    @Test
    public void insertCommentDeleteComment18() throws JSONException {
        // Result: Should handle deleted threads.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 2),
        //  deleteComment(1, 'A1', 0),
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            "[]",
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString()
        );
    }
}
