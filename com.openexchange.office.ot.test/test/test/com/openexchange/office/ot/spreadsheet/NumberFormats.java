/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class NumberFormats {

    /*
        describe('number formats and independent operations', function () {
            it('should skip the transformations', function () {
                testRunner.runBidiTest([insertNumFmt(184, '0'), deleteNumFmt(184)], [
                    GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, SHEETCOLL_OPS, colOps(1), rowOps(1),
                    cellOps(1), hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                    noteOps(1), commentOps(1), selectOps(1),
                    drawingOps(1), chartOps(1), drawingTextOps(1)
                ]);
            });
        });
     */

    @Test
    public void test01() {
        SheetHelper.runBidiTest(
            Helper.createArrayFromJSON(
                SheetHelper.createInsertNumFmtOp(184, "0"),
                SheetHelper.createDeleteNumFmtOp(184)
            ),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.AUTOSTYLE_OPS,
                SheetHelper.SHEETCOLL_OPS,
                SheetHelper.createColOps("1", null),
                SheetHelper.createRowOps("1", null),
                SheetHelper.createCellOps("1"),
                SheetHelper.createHlinkOps("1"),
                SheetHelper.createNameOps("1"),
                SheetHelper.createTableOps("1"),
                SheetHelper.createDvRuleOps("1", null),
                SheetHelper.createCfRuleOps("1", null),
                SheetHelper.createNoteOps("1"),
                SheetHelper.createCommentOps("1"),
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }
}
