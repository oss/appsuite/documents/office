/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class InsertTable {

/*
    describe('"insertTable" and "insertTable"', function () {
        it('should fail to insert table name twice', function () {
            testRunner.expectError(insertTable(1, null, 'A1:D4'), insertTable(1, null, 'E5:H8'));
            testRunner.expectError(insertTable(1, 'T1', 'A1:D4'), insertTable(1, 'T1', 'E5:H8'));
            testRunner.expectError(insertTable(1, 'T1', 'A1:D4'), insertTable(2, 'T1', 'E5:H8'));
        });
        it('should fail to insert overlapping tables', function () {
            testRunner.expectError(insertTable(1, null, 'A1:D4'), insertTable(1, 'T2', 'D4:G7'));
            testRunner.expectError(insertTable(1, 'T1', 'A1:D4'), insertTable(1, null, 'D4:G7'));
            testRunner.expectError(insertTable(1, 'T1', 'A1:D4'), insertTable(1, 'T2', 'D4:G7'));
        });
        it('should ignore overlapping table ranges on different sheets', function () {
            testRunner.runTest(insertTable(1, null, 'A1:D4'), insertTable(2, null, 'A1:D4'));
            testRunner.runTest(insertTable(1, null, 'A1:D4'), insertTable(2, 'T2', 'A1:D4'));
            -testRunner.runTest(insertTable(1, 'T1', 'A1:D4'), insertTable(2, null, 'A1:D4'));
            testRunner.runTest(insertTable(1, 'T1', 'A1:D4'), insertTable(2, 'T2', 'A1:D4'));
        });
    });
*/

    @Test
    public void insertTableInsertTable01() throws JSONException {
        // Result: Should fail to insert table name twice.
        // testRunner.expectError(
        //  insertTable(1, null, 'A1:D4'),
        //  insertTable(1, null, 'E5:H8'));
        SheetHelper.expectError(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createInsertTableOp(1, null, "E5:H8", null).toString()
        );
    }

    @Test
    public void insertTableInsertTable02() throws JSONException {
        // Result: Should fail to insert table name twice.
        // testRunner.expectError(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  insertTable(1, 'T1', 'E5:H8'));
        SheetHelper.expectError(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createInsertTableOp(1, "T1", "E5:H8", null).toString()
        );
    }

    @Test
    public void insertTableInsertTable03() throws JSONException {
        // Result: Should fail to insert table name twice.
        // testRunner.expectError(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  insertTable(2, 'T1', 'E5:H8'));
        SheetHelper.expectError(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createInsertTableOp(2, "T1", "E5:H8", null).toString()
        );
    }

    @Test
    public void insertTableInsertTable04() throws JSONException {
        // Result: Should fail to insert overlapping tables.
        // testRunner.expectError(
        //  insertTable(1, null, 'A1:D4'),
        //  insertTable(1, 'T2', 'D4:G7'));
        SheetHelper.expectError(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createInsertTableOp(1, "T2", "D4:G7", null).toString()
        );
    }

    @Test
    public void insertTableInsertTable05() throws JSONException {
        // Result: Should fail to insert overlapping tables.
        // testRunner.expectError(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  insertTable(1, null, 'D4:G7'));
        SheetHelper.expectError(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createInsertTableOp(1, null, "D4:G7", null).toString()
        );
    }

    @Test
    public void insertTableInsertTable06() throws JSONException {
        // Result: Should ignore overlapping table ranges on different sheets.
        // testRunner.runTest(
        //  insertTable(1, null, 'A1:D4'),
        //  insertTable(2, null, 'A1:D4'));
        SheetHelper.runTest(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createInsertTableOp(2, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableInsertTable07() throws JSONException {
        // Result: Should ignore overlapping table ranges on different sheets.
        // testRunner.runTest(
        //  insertTable(1, null, 'A1:D4'),
        //  insertTable(2, 'T2', 'A1:D4'));
        SheetHelper.runTest(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createInsertTableOp(2, "T2", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableInsertTable08() throws JSONException {
        // Result: Should ignore overlapping table ranges on different sheets.
        // testRunner.runTest(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  insertTable(2, null, 'A1:D4'));
        SheetHelper.runTest(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createInsertTableOp(2, null, "A1:D4", null).toString()
        );
    }

/*
    describe('"insertTable" and "deleteTable"', function () {
        it('should skip different tables', function () {
            testRunner.runBidiTest(insertTable(1, null, 'A1:D4'), deleteTable(1, 'T2'));
            testRunner.runBidiTest(insertTable(1, 'T1', 'A1:D4'), deleteTable(1, null));
            testRunner.runBidiTest(insertTable(1, 'T1', 'A1:D4'), deleteTable(1, 'T2'));
            testRunner.runBidiTest(insertTable(1, null, 'A1:D4'), deleteTable(2, null));
        });
        it('should fail for insert existing table', function () {
            testRunner.expectBidiError(insertTable(1, 'T1', 'A1:D4'), deleteTable(2, 'T1'));
            testRunner.expectBidiError(insertTable(1, 'T1', 'A1:D4'), deleteTable(1, 'T1'));
            testRunner.expectBidiError(insertTable(1, 'T1', 'A1:D4'), deleteTable(1, 't1'));
        });
    });
*/

    @Test
    public void insertTableDeleteTable01() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, null, 'A1:D4'),
        //  deleteTable(1, 'T2'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(1, "T2").toString()
        );
    }

    @Test
    public void insertTableDeleteTable02() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  deleteTable(1, null));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(1, null).toString()
        );
    }

    @Test
    public void insertTableDeleteTable03() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  deleteTable(1, 'T2'));
        SheetHelper.runTest(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(1, "T2").toString()
        );
    }

    @Test
    public void insertTableDeleteTable04() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, null, 'A1:D4'),
        //  deleteTable(2, null));
        SheetHelper.runTest(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(2, null).toString()
        );
    }

    @Test
    public void insertTableDeleteTable05() throws JSONException {
        // Result: Should fail for insert existing table.
        // testRunner.expectBidiError(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  deleteTable(2, 'T1'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(2, "T1").toString()
        );
    }

    @Test
    public void insertTableDeleteTable06() throws JSONException {
        // Result: Should fail for insert existing table.
        // testRunner.expectBidiError(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  deleteTable(1, 'T1'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(1, "T1").toString()
        );
    }

    @Test
    public void insertTableDeleteTable07() throws JSONException {
        // Result: Should fail for insert existing table.
        // testRunner.expectBidiError(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  deleteTable(1, 't1'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(1, "t1").toString()
        );
    }

/*
    describe('"insertTable" and "changeTable"', function () {
        it('should skip different tables', function () {
            testRunner.runBidiTest(insertTable(1, null, 'A1:D4'), changeTable(1, 'T2', 'A1:D4'));
            testRunner.runBidiTest(insertTable(1, 'T1', 'A1:D4'), changeTable(1, null, 'A1:D4'));
            testRunner.runBidiTest(insertTable(1, 'T1', 'A1:D4'), changeTable(1, 'T2', 'A1:D4'));
            testRunner.runBidiTest(insertTable(1, null, 'A1:D4'), changeTable(2, null, 'A1:D4'));
        });
        it('should fail to insert table with used name', function () {
            testRunner.expectBidiError(insertTable(1, null, 'A1:D4'), changeTable(1, null, 'A1:D4'));
            testRunner.expectBidiError(insertTable(1, 'T1', 'A1:D4'), changeTable(1, 'T1', 'A1:D4'));
            testRunner.expectBidiError(insertTable(2, 'T1', 'A1:D4'), changeTable(1, 'T1', 'A1:D4'));
            testRunner.expectBidiError(insertTable(2, 't1', 'A1:D4'), changeTable(1, 'T1', 'A1:D4'));
        });
        it('should fail to insert table and change table to same name', function () {
            testRunner.expectBidiError(insertTable(1, 'T1', 'A1:D4'), changeTable(1, 'T2', { newName: 'T1' }));
            testRunner.expectBidiError(insertTable(2, 'T1', 'A1:D4'), changeTable(1, 'T2', { newName: 'T1' }));
            testRunner.expectBidiError(insertTable(2, 't1', 'A1:D4'), changeTable(1, 'T2', { newName: 'T1' }));
        });
    });
*/

    @Test
    public void insertTableChangeTable01() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, null, 'A1:D4'),
        //  changeTable(1, 'T2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T2", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableChangeTable02() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  changeTable(1, null, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableChangeTable03() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  changeTable(1, 'T2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T2", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableChangeTable04() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, null, 'A1:D4'),
        //  changeTable(2, null, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(2, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableChangeTable05() throws JSONException {
        // Result: Should fail to insert table with used name.
        // testRunner.expectBidiError(
        //  insertTable(1, null, 'A1:D4'),
        //  changeTable(1, null, 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableChangeTable06() throws JSONException {
        // Result: Should fail to insert table with used name.
        // testRunner.expectBidiError(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  changeTable(1, 'T1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableChangeTable07() throws JSONException {
        // Result: Should fail to insert table with used name.
        // testRunner.expectBidiError(
        //  insertTable(2, 'T1', 'A1:D4'),
        //  changeTable(1, 'T1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(2, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableChangeTable08() throws JSONException {
        // Result: Should fail to insert table with used name.
        // testRunner.expectBidiError(
        //  insertTable(2, 't1', 'A1:D4'),
        //  changeTable(1, 'T1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(2, "t1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertTableChangeTable09() throws JSONException {
        // Result: Should fail to insert table and change table to same name.
        // testRunner.expectBidiError(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  changeTable(1, 'T2', { newName: 'T1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T2", null, "{ newName: 'T1' }").toString()
        );
    }

    @Test
    public void insertTableChangeTable10() throws JSONException {
        // Result: Should fail to insert table and change table to same name.
        // testRunner.expectBidiError(
        //  insertTable(2, 'T1', 'A1:D4'),
        //  changeTable(1, 'T2', { newName: 'T1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(2, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T2", null, "{ newName: 'T1' }").toString()
        );
    }

    @Test
    public void insertTableChangeTable11() throws JSONException {
        // Result: Should fail to insert table and change table to same name.
        // testRunner.expectBidiError(
        //  insertTable(2, 't1', 'A1:D4'),
        //  changeTable(1, 'T2', { newName: 'T1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(2, "t1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T2", null, "{ newName: 'T1' }").toString()
        );
    }

/*
    describe('"insertTable" and "changeTableColumn"', function () {
        it('should skip different tables', function () {
            testRunner.runBidiTest(insertTable(1, null, 'A1:D4'), changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
            testRunner.runBidiTest(insertTable(1, 'T1', 'A1:D4'), changeTableCol(1, null, 0, { attrs: ATTRS }));
            testRunner.runBidiTest(insertTable(1, 'T1', 'A1:D4'), changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
            testRunner.runBidiTest(insertTable(1, null, 'A1:D4'), changeTableCol(2, null, 0, { attrs: ATTRS }));
        });
        it('should fail to insert table with used name', function () {
            testRunner.expectBidiError(insertTable(1, null, 'A1:D4'), changeTableCol(1, null, 0, { attrs: ATTRS }));
            testRunner.expectBidiError(insertTable(1, 'T1', 'A1:D4'), changeTableCol(1, 'T1', 0, { attrs: ATTRS }));
            testRunner.expectBidiError(insertTable(2, 'T1', 'A1:D4'), changeTableCol(1, 'T1', 0, { attrs: ATTRS }));
            -testRunner.expectBidiError(insertTable(2, 't1', 'A1:D4'), changeTableCol(1, 'T1', 0, { attrs: ATTRS }));
        });
    });
*/
    private static String ATTRS = "{ attrs: " + SheetHelper.ATTRS + " }";

    @Test
    public void insertTableChangeTableColumn01() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, null, 'A1:D4'),
        //  changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, "T2", 0, ATTRS).toString()
        );
    }

    @Test
    public void insertTableChangeTableColumn02() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  changeTableCol(1, null, 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, null, 0, ATTRS).toString()
        );
    }

    @Test
    public void insertTableChangeTableColumn03() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, "T2", 0, ATTRS).toString()
        );
    }

    @Test
    public void insertTableChangeTableColumn04() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  insertTable(1, null, 'A1:D4'),
        //  changeTableCol(2, null, 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(2, null, 0, ATTRS).toString()
        );
    }

    @Test
    public void insertTableChangeTableColumn05() throws JSONException {
        // Result: Should fail to insert table with used name.
        // testRunner.expectBidiError(
        //  insertTable(1, null, 'A1:D4'),
        //  changeTableCol(1, null, 0, { attrs: ATTRS }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, null, 0, ATTRS).toString()
        );
    }

    @Test
    public void insertTableChangeTableColumn06() throws JSONException {
        // Result: Should fail to insert table with used name.
        // testRunner.expectBidiError(
        //  insertTable(1, 'T1', 'A1:D4'),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, ATTRS).toString()
        );
    }

    @Test
    public void insertTableChangeTableColumn07() throws JSONException {
        // Result: Should fail to insert table with used name.
        // testRunner.expectBidiError(
        //  insertTable(2, 'T1', 'A1:D4'),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(2, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, ATTRS).toString()
        );
    }

    @Test
    public void insertTableChangeTableColumn08() throws JSONException {
        // Result: Should fail to insert table with used name.
        // testRunner.expectBidiError(
        //  insertTable(2, 't1', 'A1:D4'),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertTableOp(2, "t1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, ATTRS).toString()
        );
    }
}

