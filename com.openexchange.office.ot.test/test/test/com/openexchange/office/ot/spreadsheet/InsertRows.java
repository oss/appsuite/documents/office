/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertRows {

/*
    describe('"insertRows" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(insertRows(1, '3'), [
                GLOBAL_OPS, STYLESHEET_OPS, colOps(1), nameOps(1),
                deleteTable(1, 'T'), changeTableCol(1, 'T', 0, { attrs: ATTRS }),
                deleteDVRule(1, 0), deleteCFRule(1, 'R1'),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/

    @Test
    public void InsertRowsIndependentOperations01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertRowsOp(1, "3").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createColOps("1", null),
                SheetHelper.createNameOps("1"),
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createChangeTableColOp(1, "T", 0, "{ attrs: " + SheetHelper.ATTRS.toString() + "}"),
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                // select ...
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

/*
    describe('"insertRows" and "insertRows"', function () {
        it('should transform single intervals', function () {
            testRunner.runTest(insertRows(1, '2:4'), insertRows(1, '1:3'), insertRows(1, '5:7'), insertRows(1, '1:3'));
            testRunner.runTest(insertRows(1, '2:4'), insertRows(1, '2:4'), insertRows(1, '2:4'), insertRows(1, '5:7'));
            testRunner.runTest(insertRows(1, '2:4'), insertRows(1, '3:5'), insertRows(1, '2:4'), insertRows(1, '6:8'));
        });
        it('should transform interval lists #1', function () {
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '1 2'), insertRows(1, '3 6'), insertRows(1, '1 3'));
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '2 3'), insertRows(1, '2 6'), insertRows(1, '3 4'));
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '3 4'), insertRows(1, '2 5'), insertRows(1, '4 6'));
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '4 5'), insertRows(1, '2 4'), insertRows(1, '6 7'));
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '5 6'), insertRows(1, '2 4'), insertRows(1, '7 8'));
        });
        it('should transform interval lists #2', function () {
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '1 3'), insertRows(1, '3 6'), insertRows(1, '1 4'));
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '2 4'), insertRows(1, '2 5'), insertRows(1, '3 6'));
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '3 5'), insertRows(1, '2 5'), insertRows(1, '4 7'));
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '4 6'), insertRows(1, '2 4'), insertRows(1, '6 8'));
            testRunner.runTest(insertRows(1, '2 4'), insertRows(1, '5 7'), insertRows(1, '2 4'), insertRows(1, '7 9'));
        });
        it('should shift intervals outside sheet', function () {
            testRunner.runBidiTest(insertRows(1, '1048574'), insertRows(1, '1048575:1048576'), null, insertRows(1, '1048576'));
            testRunner.runBidiTest(insertRows(1, '1048574'), insertRows(1, '1048576'),         null, []);
        });
        it('should not transform intervals in different sheets', function () {
            -testRunner.runTest(insertRows(1, '3'), insertRows(2, '3'));
        });
    });
*/

    @Test
    public void insertRowsInsertRows01() throws JSONException {
        // Result: Should transform single intervals.
        // testRunner.runTest(
        //  insertRows(1, '2:4'),
        //  insertRows(1, '1:3'),
        //  insertRows(1, '5:7'),
        //  insertRows(1, '1:3'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2:4").toString(),
            SheetHelper.createInsertRowsOp(1, "1:3").toString(),
            SheetHelper.createInsertRowsOp(1, "5:7").toString(),
            SheetHelper.createInsertRowsOp(1, "1:3").toString()
        );
    }

    @Test
    public void insertRowsInsertRows02() throws JSONException {
        // Result: Should transform single intervals.
        // testRunner.runTest(
        //  insertRows(1, '2:4'),
        //  insertRows(1, '2:4'),
        //  insertRows(1, '2:4'),
        //  insertRows(1, '5:7'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2:4").toString(),
            SheetHelper.createInsertRowsOp(1, "2:4").toString(),
            SheetHelper.createInsertRowsOp(1, "2:4").toString(),
            SheetHelper.createInsertRowsOp(1, "5:7").toString()
        );
    }

    @Test
    public void insertRowsInsertRows03() throws JSONException {
        // Result: Should transform single intervals.
        // testRunner.runTest(
        //  insertRows(1, '2:4'),
        //  insertRows(1, '3:5'),
        //  insertRows(1, '2:4'),
        //  insertRows(1, '6:8'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2:4").toString(),
            SheetHelper.createInsertRowsOp(1, "3:5").toString(),
            SheetHelper.createInsertRowsOp(1, "2:4").toString(),
            SheetHelper.createInsertRowsOp(1, "6:8").toString()
        );
    }

    @Test
    public void insertRowsInsertRows04() throws JSONException {
        // Result: Should transform interval lists #1.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '1 2'),
        //  insertRows(1, '3 6'),
        //  insertRows(1, '1 3'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "1 2").toString(),
            SheetHelper.createInsertRowsOp(1, "3 6").toString(),
            SheetHelper.createInsertRowsOp(1, "1 3").toString()
        );
    }

    @Test
    public void insertRowsInsertRows05() throws JSONException {
        // Result: Should transform interval lists #1.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '2 3'),
        //  insertRows(1, '2 6'),
        //  insertRows(1, '3 4'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "2 3").toString(),
            SheetHelper.createInsertRowsOp(1, "2 6").toString(),
            SheetHelper.createInsertRowsOp(1, "3 4").toString()
        );
    }

    @Test
    public void insertRowsInsertRows06() throws JSONException {
        // Result: Should transform interval lists #1.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '3 4'),
        //  insertRows(1, '2 5'),
        //  insertRows(1, '4 6'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "3 4").toString(),
            SheetHelper.createInsertRowsOp(1, "2 5").toString(),
            SheetHelper.createInsertRowsOp(1, "4 6").toString()
        );
    }

    @Test
    public void insertRowsInsertRows07() throws JSONException {
        // Result: Should transform interval lists #1.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '4 5'),
        //  insertRows(1, '2 4'),
        //  insertRows(1, '6 7'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "4 5").toString(),
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "6 7").toString()
        );
    }

    @Test
    public void insertRowsInsertRows08() throws JSONException {
        // Result: Should transform interval lists #1.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '5 6'),
        //  insertRows(1, '2 4'),
        //  insertRows(1, '7 8'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "5 6").toString(),
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "7 8").toString()
        );
    }

    @Test
    public void insertRowsInsertRows09() throws JSONException {
        // Result: Should transform interval lists #2.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '1 3'),
        //  insertRows(1, '3 6'),
        //  insertRows(1, '1 4'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "1 3").toString(),
            SheetHelper.createInsertRowsOp(1, "3 6").toString(),
            SheetHelper.createInsertRowsOp(1, "1 4").toString()
        );
    }

    @Test
    public void insertRowsInsertRows10() throws JSONException {
        // Result: Should transform interval lists #2.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '2 4'),
        //  insertRows(1, '2 5'),
        //  insertRows(1, '3 6'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "2 5").toString(),
            SheetHelper.createInsertRowsOp(1, "3 6").toString()
        );
    }

    @Test
    public void insertRowsInsertRows11() throws JSONException {
        // Result: Should transform interval lists #2.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '3 5'),
        //  insertRows(1, '2 5'),
        //  insertRows(1, '4 7'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "3 5").toString(),
            SheetHelper.createInsertRowsOp(1, "2 5").toString(),
            SheetHelper.createInsertRowsOp(1, "4 7").toString()
        );
    }

    @Test
    public void insertRowsInsertRows12() throws JSONException {
        // Result: Should transform interval lists #2.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '4 6'),
        //  insertRows(1, '2 4'),
        //  insertRows(1, '6 8'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "4 6").toString(),
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "6 8").toString()
        );
    }

    @Test
    public void insertRowsInsertRows13() throws JSONException {
        // Result: Should transform interval lists #2.
        // testRunner.runTest(
        //  insertRows(1, '2 4'),
        //  insertRows(1, '5 7'),
        //  insertRows(1, '2 4'),
        //  insertRows(1, '7 9'));
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "5 7").toString(),
            SheetHelper.createInsertRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "7 9").toString()
        );
    }

    @Test
    public void insertRowsInsertRows14() throws JSONException {
        // Result: Should shift intervals outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '1048574'),
        //  insertRows(1, '1048575:1048576'),
        //  null,
        //  insertRows(1, '1048576'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            SheetHelper.createInsertRowsOp(1, "1048575:1048576").toString(),
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            SheetHelper.createInsertRowsOp(1, "1048576").toString()
        );
    }

    @Test
    public void insertRowsInsertRows15() throws JSONException {
        // Result: Should shift intervals outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '1048574'),
        //  insertRows(1, '1048576'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            SheetHelper.createInsertRowsOp(1, "1048576").toString(),
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            "[]"
        );
    }

    @Test
    public void insertRowsInsertRows16() throws JSONException {
        // Result: Should not transform intervals in different sheets.
        // testRunner.runTest(insertRows(1, '3'), insertRows(2, '3'))
        SheetHelper.runTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createInsertRowsOp(2, "3").toString()
        );
    }

/*
    describe('"insertRows" and "deleteRows"', function () {
        it('should transform single intervals', function () {
            testRunner.runBidiTest(insertRows(1, '1:2'), deleteRows(1, '2:3'), insertRows(1, '1:2'), deleteRows(1, '4:5'));
            testRunner.runBidiTest(insertRows(1, '2:3'), deleteRows(1, '2:3'), insertRows(1, '2:3'), deleteRows(1, '4:5'));
            testRunner.runBidiTest(insertRows(1, '3:4'), deleteRows(1, '2:3'), insertRows(1, '2:3'), deleteRows(1, '2 5'));
            testRunner.runBidiTest(insertRows(1, '4:5'), deleteRows(1, '2:3'), insertRows(1, '2:3'), deleteRows(1, '2:3'));
            testRunner.runBidiTest(insertRows(1, '5:6'), deleteRows(1, '2:3'), insertRows(1, '3:4'), deleteRows(1, '2:3'));
        });
        it('should transform interval lists', function () {
            testRunner.runBidiTest(insertRows(1, '4 7'), deleteRows(1, '1 3'),  insertRows(1, '2 5'), deleteRows(1, '1 3'));
            testRunner.runBidiTest(insertRows(1, '4 7'), deleteRows(1, '2 4'),  insertRows(1, '3 5'), deleteRows(1, '2 5'));
            testRunner.runBidiTest(insertRows(1, '4 7'), deleteRows(1, '3 5'),  insertRows(1, '3 5'), deleteRows(1, '3 6'));
            testRunner.runBidiTest(insertRows(1, '4 7'), deleteRows(1, '4 6'),  insertRows(1, '4 5'), deleteRows(1, '5 7'));
            testRunner.runBidiTest(insertRows(1, '4 7'), deleteRows(1, '5 7'),  insertRows(1, '4 6'), deleteRows(1, '6 9'));
            testRunner.runBidiTest(insertRows(1, '4 7'), deleteRows(1, '6 8'),  insertRows(1, '4 6'), deleteRows(1, '7 10'));
            testRunner.runBidiTest(insertRows(1, '4 7'), deleteRows(1, '7 9'),  insertRows(1, '4 7'), deleteRows(1, '9 11'));
            testRunner.runBidiTest(insertRows(1, '4 7'), deleteRows(1, '8 10'), insertRows(1, '4 7'), deleteRows(1, '10 12'));
        });
        it('should shift intervals outside sheet', function () {
            testRunner.runBidiTest(insertRows(1, '1048574'), deleteRows(1, '1048575:1048576'), null, deleteRows(1, '1048576'));
            testRunner.runBidiTest(insertRows(1, '1048574'), deleteRows(1, '1048576'),     null, []);
        });
        it('should not transform intervals in different sheets', function () {
            -testRunner.runBidiTest(insertRows(1, '3'), deleteRows(2, '3'));
        });
    });
*/

    @Test
    public void insertRowsDeleteRows01() throws JSONException {
        // Result: Should transform single intervals.
        // testRunner.runBidiTest(
        //  insertRows(1, '1:2'),
        //  deleteRows(1, '2:3'),
        //  insertRows(1, '1:2'),
        //  deleteRows(1, '4:5'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:2").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString(),
            SheetHelper.createInsertRowsOp(1, "1:2").toString(),
            SheetHelper.createDeleteRowsOp(1, "4:5").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows02() throws JSONException {
        // Result: Should transform single intervals.
        // testRunner.runBidiTest(
        //  insertRows(1, '2:3'),
        //  deleteRows(1, '2:3'),
        //  insertRows(1, '2:3'),
        //  deleteRows(1, '4:5'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "2:3").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString(),
            SheetHelper.createInsertRowsOp(1, "2:3").toString(),
            SheetHelper.createDeleteRowsOp(1, "4:5").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows03() throws JSONException {
        // Result: Should transform single intervals.
        // testRunner.runBidiTest(
        //  insertRows(1, '3:4'),
        //  deleteRows(1, '2:3'),
        //  insertRows(1, '2:3'),
        //  deleteRows(1, '2 5'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString(),
            SheetHelper.createInsertRowsOp(1, "2:3").toString(),
            SheetHelper.createDeleteRowsOp(1, "2 5").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows04() throws JSONException {
        // Result: Should transform single intervals.
        // testRunner.runBidiTest(
        //  insertRows(1, '4:5'),
        //  deleteRows(1, '2:3'),
        //  insertRows(1, '2:3'),
        //  deleteRows(1, '2:3'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4:5").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString(),
            SheetHelper.createInsertRowsOp(1, "2:3").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows05() throws JSONException {
        // Result: Should transform single intervals.
        // testRunner.runBidiTest(
        //  insertRows(1, '5:6'),
        //  deleteRows(1, '2:3'),
        //  insertRows(1, '3:4'),
        //  deleteRows(1, '2:3'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "5:6").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString(),
            SheetHelper.createInsertRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows06() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runBidiTest(
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '1 3'),
        //  insertRows(1, '2 5'),
        //  deleteRows(1, '1 3'));;
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "1 3").toString(),
            SheetHelper.createInsertRowsOp(1, "2 5").toString(),
            SheetHelper.createDeleteRowsOp(1, "1 3").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows07() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runBidiTest(
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '2 4'),
        //  insertRows(1, '3 5'),
        //  deleteRows(1, '2 5'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "2 4").toString(),
            SheetHelper.createInsertRowsOp(1, "3 5").toString(),
            SheetHelper.createDeleteRowsOp(1, "2 5").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows08() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runBidiTest(
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '3 5'),
        //  insertRows(1, '3 5'),
        //  deleteRows(1, '3 6'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "3 5").toString(),
            SheetHelper.createInsertRowsOp(1, "3 5").toString(),
            SheetHelper.createDeleteRowsOp(1, "3 6").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows09() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runBidiTest(
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '4 6'),
        //  insertRows(1, '4 5'),
        //  deleteRows(1, '5 7'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "4 6").toString(),
            SheetHelper.createInsertRowsOp(1, "4 5").toString(),
            SheetHelper.createDeleteRowsOp(1, "5 7").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows10() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runBidiTest(
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '5 7'),
        //  insertRows(1, '4 6'),
        //  deleteRows(1, '6 9'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "5 7").toString(),
            SheetHelper.createInsertRowsOp(1, "4 6").toString(),
            SheetHelper.createDeleteRowsOp(1, "6 9").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows11() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runBidiTest(
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '6 8'),
        //  insertRows(1, '4 6'),
        //  deleteRows(1, '7 10'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "6 8").toString(),
            SheetHelper.createInsertRowsOp(1, "4 6").toString(),
            SheetHelper.createDeleteRowsOp(1, "7 10").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows12() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runBidiTest(
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '7 9'),
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '9 11'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "7 9").toString(),
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "9 11").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows13() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runBidiTest(
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '8 10'),
        //  insertRows(1, '4 7'),
        //  deleteRows(1, '10 12'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "8 10").toString(),
            SheetHelper.createInsertRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "10 12").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows14() throws JSONException {
        // Result: Should shift intervals outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '1048574'),
        //  deleteRows(1, '1048575:1048576'),
        //  null,
        //  deleteRows(1, '1048576'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            SheetHelper.createDeleteRowsOp(1, "1048575:1048576").toString(),
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            SheetHelper.createDeleteRowsOp(1, "1048576").toString()
        );
    }

    @Test
    public void insertRowsDeleteRows15() throws JSONException {
        // Result: Should shift intervals outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '1048574'),
        //  deleteRows(1, '1048576'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            SheetHelper.createDeleteRowsOp(1, "1048576").toString(),
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            "[]"
        );
    }

    @Test
    public void insertRowsDeleteRows16() throws JSONException {
        // Result: Should not transform intervals in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  deleteRows(2, '3'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createDeleteRowsOp(2, "3").toString()
        );
    }

/*
    describe('"insertRows" and "changeRows"', function () {
        it('should transform intervals', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), opSeries2(changeRows, 1, ['1 2 3 4', '1:2', '1:3',   '1:4',     '2:4',   '3:4', '1 1048574:1048575 1048575:1048576 1048576 1', '1048576']),
                insertRows(1, '3'), opSeries2(changeRows, 1, ['1 2 4 5', '1:2', '1:2 4', '1:2 4:5', '2 4:5', '4:5', '1 1048575:1048576 1048576 1'])
            );
            testRunner.runBidiTest(
                insertRows(1, '3:4 4:5'), opSeries2(changeRows, 1, ['1 2 3 4', '1:2', '1:3',   '1:4',     '2:5',     '3:5',   '4:5']),
                insertRows(1, '3:4 4:5'), opSeries2(changeRows, 1, ['1 2 5 8', '1:2', '1:2 5', '1:2 5 8', '2 5 8:9', '5 8:9', '8:9'])
            );
        });
        it('should not transform intervals in different sheets', function () {
            testRunner.runBidiTest(insertRows(1, '3'), changeRows(2, '1:4', 'a1'));
        });
    });
*/

    @Test
    public void insertRowsChangeRows01() throws JSONException {
        // Result: Should transform intervals.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  opSeries2(
        //      changeRows, 1, ['1 2 3 4', '1:2', '1:3',   '1:4',     '2:4',   '3:4', '1 1048574:1048575 1048575:1048576 1048576 1', '1048576']),
        //  insertRows(1, '3'),
        //  opSeries2(
        //      changeRows, 1, ['1 2 4 5', '1:2', '1:2 4', '1:2 4:5', '2 4:5', '4:5', '1 1048575:1048576 1048576 1']));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeRowsOp(1, "1 2 3 4"),
                SheetHelper.createChangeRowsOp(1, "1:2"),
                SheetHelper.createChangeRowsOp(1, "1:3"),
                SheetHelper.createChangeRowsOp(1, "1:4"),
                SheetHelper.createChangeRowsOp(1, "2:4"),
                SheetHelper.createChangeRowsOp(1, "3:4"),
                SheetHelper.createChangeRowsOp(1, "1 1048574:1048575 1048575:1048576 1048576 1"),
                SheetHelper.createChangeRowsOp(1, "1048576")
            ),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeRowsOp(1, "1 2 4 5"),
                SheetHelper.createChangeRowsOp(1, "1:2"),
                SheetHelper.createChangeRowsOp(1, "1:2 4"),
                SheetHelper.createChangeRowsOp(1, "1:2 4:5"),
                SheetHelper.createChangeRowsOp(1, "2 4:5"),
                SheetHelper.createChangeRowsOp(1, "4:5"),
                SheetHelper.createChangeRowsOp(1, "1 1048575:1048576 1048576 1")
            )
        );
    }

    @Test
    public void insertRowsChangeRows02() throws JSONException {
        // Result: Should transform intervals.
        // testRunner.runBidiTest(
        //  insertRows(1, '3:4 4:5'),
        //  opSeries2(
        //      changeRows, 1, ['1 2 3 4', '1:2', '1:3',   '1:4',     '2:5',     '3:5',   '4:5']),
        //  insertRows(1, '3:4 4:5'),
        //  opSeries2(
        //      changeRows, 1, ['1 2 5 8', '1:2', '1:2 5', '1:2 5 8', '2 5 8:9', '5 8:9', '8:9']));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3:4 4:5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeRowsOp(1, "1 2 3 4"),
                SheetHelper.createChangeRowsOp(1, "1:2"),
                SheetHelper.createChangeRowsOp(1, "1:3"),
                SheetHelper.createChangeRowsOp(1, "1:4"),
                SheetHelper.createChangeRowsOp(1, "2:5"),
                SheetHelper.createChangeRowsOp(1, "3:5"),
                SheetHelper.createChangeRowsOp(1, "4:5")
            ),
            SheetHelper.createInsertRowsOp(1, "3:4 4:5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeRowsOp(1, "1 2 5 8"),
                SheetHelper.createChangeRowsOp(1, "1:2"),
                SheetHelper.createChangeRowsOp(1, "1:2 5"),
                SheetHelper.createChangeRowsOp(1, "1:2 5 8"),
                SheetHelper.createChangeRowsOp(1, "2 5 8:9"),
                SheetHelper.createChangeRowsOp(1, "5 8:9"),
                SheetHelper.createChangeRowsOp(1, "8:9")
            )
        );
    }

    @Test
    public void insertRowsChangeRows03() throws JSONException {
        // Result: Should not transform intervals in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeRows(2, '1:4', 'a1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeRowsOp(2, "1:4", "a1", null).toString()
        );
    }

/*
    describe('"insertRows" and "changeCells"', function () {
        it('should transform ranges', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), changeCellsOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4',       'A1 A1048574:B1048575 A1048575:B1048576 A1048576:B1048576 B1', 'A1048576'),
                insertRows(1, '3'), changeCellsOps(1, 'A1 B2 C4 D5', 'A1:B2 B2:C2 B4:C4 C4:D5', 'A1 A1048575:B1048576 A1048576:B1048576 B1')
            );
            testRunner.runBidiTest(
                insertRows(1, '3:4 4:5'), changeCellsOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4 D4:E5'),
                insertRows(1, '3:4 4:5'), changeCellsOps(1, 'A1 B2 C5 D8', 'A1:B2 B2:C2 B5:C5 C5:D5 C8:D8 D8:E9')
            );
        });
        it('should transform matrix formula range', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), changeCells(1, { A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' } }),
                null,               changeCells(1, { A1: { mr: 'A1:B2' }, C4: { mr: 'C4:D5' } })
            );
        });
        it('should fail to split a matrix formula range', function () {
            testRunner.expectBidiError(insertRows(1, '3'), changeCells(1, { A1: { mr: 'A1:D4' } }));
        });
        it('should fail to change a shared formula', function () {
            testRunner.runBidiTest(insertRows(1, '3'), changeCells(1, { A1: { si: null } }));
            testRunner.runBidiTest(insertRows(1, '3'), changeCells(1, { A1: { sr: null } }));
            testRunner.expectBidiError(insertRows(1, '3'), changeCells(1, { A1: { si: 1 } }));
            testRunner.expectBidiError(insertRows(1, '3'), changeCells(1, { A1: { sr: 'A1:D4' } }));
        });
        it('should not transform ranges in different sheets', function () {
            -testRunner.runBidiTest(insertRows(1, '3'), changeCellsOps(2, 'A1:D4'));
        });
    });
*/

    @Test
    public void insertRowsChangeCells01() throws JSONException {
        // Result: Should transform ranges.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeCellsOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4',       'A1 A1048574:B1048575 A1048575:B1048576 A1048576:B1048576 B1', 'A1048576'),
        //  insertRows(1, '3'),
        //  changeCellsOps(1, 'A1 B2 C4 D5', 'A1:B2 B2:C2 B4:C4 C4:D5', 'A1 A1048575:B1048576 A1048576:B1048576 B1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4", "A1 A1048574:B1048575 A1048575:B1048576 A1048576:B1048576 B1", "A1048576").toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOps(1, "A1 B2 C4 D5", "A1:B2 B2:C2 B4:C4 C4:D5", "A1 A1048575:B1048576 A1048576:B1048576 B1").toString()
        );
    }

    @Test
    public void insertRowsChangeCells02() throws JSONException {
        // Result: Should transform ranges.
        // testRunner.runBidiTest(
        //  insertRows(1, '3:4 4:5'),
        //  changeCellsOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4 D4:E5'),
        //  insertRows(1, '3:4 4:5'),
        //  changeCellsOps(1, 'A1 B2 C5 D8', 'A1:B2 B2:C2 B5:C5 C5:D5 C8:D8 D8:E9'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3:4 4:5").toString(),
            SheetHelper.createChangeCellsOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4 D4:E5").toString(),
            SheetHelper.createInsertRowsOp(1, "3:4 4:5").toString(),
            SheetHelper.createChangeCellsOps(1, "A1 B2 C5 D8", "A1:B2 B2:C2 B5:C5 C5:D5 C8:D8 D8:E9").toString()
        );
    }

    @Test
    public void insertRowsChangeCells03() throws JSONException {
        // Result: Should transform matrix formula range.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeCells(1, { A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' } }),
        //  null,
        //  changeCells(1, { A1: { mr: 'A1:B2' }, C4: { mr: 'C4:D5' } }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' } }")).toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { mr: 'A1:B2' }, C4: { mr: 'C4:D5' } }")).toString()
        );
    }

    @Test
    public void insertRowsChangeCells04() throws JSONException {
        // Result: Should fail to split a matrix formula range.
        // testRunner.expectBidiError(
        //  insertRows(1, '3'),
        //  changeCells(1, { A1: { mr: 'A1:D4' } }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { mr: 'A1:D4' } }")).toString()
        );
    }

    @Test
    public void insertRowsChangeCells05() throws JSONException {
        // Result: Should fail to change a shared formula.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeCells(1, { A1: { si: null } }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { si: null } }")).toString()
        );
    }

    @Test
    public void insertRowsChangeCells06() throws JSONException {
        // Result: Should fail to change a shared formula.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeCells(1, { A1: { sr: null } }));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { sr: null } }")).toString()
        );
    }

    @Test
    public void insertRowsChangeCells07() throws JSONException {
        // Result: Should fail to change a shared formula.
        // testRunner.expectBidiError(
        //  insertRows(1, '3'),
        //  changeCells(1, { A1: { si: 1 } }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { si: 1 } }")).toString()
        );
    }

    @Test
    public void insertRowsChangeCells08() throws JSONException {
        // Result: Should fail to change a shared formula.
        // testRunner.expectBidiError(
        //  insertRows(1, '3'),
        //  changeCells(1, { A1: { sr: 'A1:D4' } }));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { sr: 'A1:D4' } }")).toString()
        );
    }

    @Test
    public void insertRowsChangeCells09() throws JSONException {
        // Result: Should not transform ranges in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeCellsOps(2, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOps(2, "A1:D4").toString()
        );
    }

/*
    describe('"insertRows" and "mergeCells"', function () {
        it('should resize ranges (MERGE, VERTICAL, UNMERGE mode)', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), opSeries2(mergeCells, 1, 'B1:C2 D2:E3 F3:G4', [MM_MERGE, MM_VERTICAL, MM_UNMERGE]),
                insertRows(1, '3'), opSeries2(mergeCells, 1, 'B1:C2 D2:E4 F4:G5', [MM_MERGE, MM_VERTICAL, MM_UNMERGE])
            );
            testRunner.runBidiTest(
                insertRows(1, '1048574'), opSeries2(mergeCells, 1, 'A1048573:B1048575 C1048573:D1048576', [MM_MERGE, MM_VERTICAL, MM_UNMERGE]),
                insertRows(1, '1048574'), opSeries2(mergeCells, 1, 'A1048573:B1048576 C1048573:D1048576', [MM_MERGE, MM_VERTICAL, MM_UNMERGE])
            );
        });
        it('should split ranges (HORIZONTAL mode)', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), mergeCells(1, 'B1:C2 D2:E3 F3:G4',       MM_HORIZONTAL),
                insertRows(1, '3'), mergeCells(1, 'B1:C2 D2:E2 D4:E4 F4:G5', MM_HORIZONTAL)
            );
            testRunner.runBidiTest(
                insertRows(1, '1048574'), mergeCells(1, 'A1048573:B1048575 C1048573:D1048576',                                     MM_HORIZONTAL),
                insertRows(1, '1048574'), mergeCells(1, 'A1048573:B1048573 A1048575:B1048576 C1048573:D1048573 C1048575:D1048576', MM_HORIZONTAL)
            );
        });
        it('should remove no-op operations', function () {
            testRunner.runBidiTest(insertRows(1, '3:4'), opSeries2(mergeCells, 1, 'A1048575:B1048576', MM_ALL), null, []);
        });
        it('should not modify entire column ranges', function () {
            testRunner.runBidiTest(insertRows(1, '3:4'), opSeries2(mergeCells, 1, 'A1:B1048576', MM_ALL));
        });
        it('should not transform ranges in different sheets', function () {
            -testRunner.runBidiTest(insertRows(1, '3'), mergeCells(2, 'A1:D4'));
        });
    });
*/

    @Test
    public void insertRowsMergeCells01() throws JSONException {
        // Result: Should resize ranges (MERGE, VERTICAL, UNMERGE mode).
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //      opSeries2(mergeCells, 1, 'B1:C2 D2:E3 F3:G4', [MM_MERGE, MM_VERTICAL, MM_UNMERGE]),
        //  insertRows(1, '3'),
        //      opSeries2(mergeCells, 1, 'B1:C2 D2:E4 F4:G5', [MM_MERGE, MM_VERTICAL, MM_UNMERGE]));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E3 F3:G4", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E3 F3:G4", SheetHelper.MM_VERTICAL),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E3 F3:G4", SheetHelper.MM_UNMERGE)
            ),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E4 F4:G5", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E4 F4:G5", SheetHelper.MM_VERTICAL),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E4 F4:G5", SheetHelper.MM_UNMERGE)
            )
        );
    }

    @Test
    public void insertRowsMergeCells02() throws JSONException {
        // Result: Should resize ranges (MERGE, VERTICAL, UNMERGE mode).
        // testRunner.runBidiTest(
        //  insertRows(1, '1048574'),
        //  opSeries2(
        //      mergeCells, 1, 'A1048573:B1048575 C1048573:D1048576', [MM_MERGE, MM_VERTICAL, MM_UNMERGE]),
        //  insertRows(1, '1048574'),
        //  opSeries2(
        //  mergeCells, 1, 'A1048573:B1048576 C1048573:D1048576', [MM_MERGE, MM_VERTICAL, MM_UNMERGE]));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A1048573:B1048575 C1048573:D1048576", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "A1048573:B1048575 C1048573:D1048576", SheetHelper.MM_VERTICAL),
                SheetHelper.createMergeCellsOp(1, "A1048573:B1048575 C1048573:D1048576", SheetHelper.MM_UNMERGE)
            ),
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A1048573:B1048576 C1048573:D1048576", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "A1048573:B1048576 C1048573:D1048576", SheetHelper.MM_VERTICAL),
                SheetHelper.createMergeCellsOp(1, "A1048573:B1048576 C1048573:D1048576", SheetHelper.MM_UNMERGE)
            )
        );
    }

    @Test
    public void insertRowsMergeCells03() throws JSONException {
        // Result: Should split ranges (HORIZONTAL mode).
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  mergeCells(1, 'B1:C2 D2:E3 F3:G4',       MM_HORIZONTAL),
        //  insertRows(1, '3'),
        //  mergeCells(1, 'B1:C2 D2:E2 D4:E4 F4:G5', MM_HORIZONTAL));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E3 F3:G4", SheetHelper.MM_HORIZONTAL).toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E2 D4:E4 F4:G5", SheetHelper.MM_HORIZONTAL).toString()
        );
    }

    @Test
    public void insertRowsMergeCells04() throws JSONException {
        // Result: Should split ranges (HORIZONTAL mode).
        // testRunner.runBidiTest(
        //  insertRows(1, '1048574'),
        //  mergeCells(1, 'A1048573:B1048575 C1048573:D1048576',                                     MM_HORIZONTAL),
        //  insertRows(1, '1048574'),
        //  mergeCells(1, 'A1048573:B1048573 A1048575:B1048576 C1048573:D1048573 C1048575:D1048576', MM_HORIZONTAL));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            SheetHelper.createMergeCellsOp(1, "A1048573:B1048575 C1048573:D1048576", SheetHelper.MM_HORIZONTAL).toString(),
            SheetHelper.createInsertRowsOp(1, "1048574").toString(),
            SheetHelper.createMergeCellsOp(1, "A1048573:B1048573 A1048575:B1048576 C1048573:D1048573 C1048575:D1048576", SheetHelper.MM_HORIZONTAL).toString()
        );
    }

    @Test
    public void insertRowsMergeCells05() throws JSONException {
        // Result: Should remove no-op operations.
        // testRunner.runBidiTest(
        //  insertRows(1, '3:4'),
        //  opSeries2(mergeCells, 1, 'A1048575:B1048576', MM_ALL), null, []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3:4").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A1048575:B1048576", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "A1048575:B1048576", SheetHelper.MM_HORIZONTAL),
                SheetHelper.createMergeCellsOp(1, "A1048575:B1048576", SheetHelper.MM_VERTICAL),
                SheetHelper.createMergeCellsOp(1, "A1048575:B1048576", SheetHelper.MM_UNMERGE)
            ),
            SheetHelper.createInsertRowsOp(1, "3:4").toString(),
            "[]"
        );
    }

    @Test
    public void insertRowsMergeCells06() throws JSONException {
        // Result: Should not modify entire column ranges.
        // testRunner.runBidiTest(
        //  insertRows(1, '3:4'), opSeries2(mergeCells, 1, 'A1:B1048576', MM_ALL));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3:4").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A1:B1048576", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "A1:B1048576", SheetHelper.MM_HORIZONTAL),
                SheetHelper.createMergeCellsOp(1, "A1:B1048576", SheetHelper.MM_VERTICAL),
                SheetHelper.createMergeCellsOp(1, "A1:B1048576", SheetHelper.MM_UNMERGE)
            )
        );
    }

    @Test
    public void insertRowsMergeCells07() throws JSONException {
        // Result: Should not transform ranges in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  mergeCells(2, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(2, "A1:D4", null).toString()
        );
    }

/*
    describe('"insertRows" and hyperlinks (RESIZE mode)', function () {
        it('should transform ranges', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), hlinkOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4', 'A1 A1048574:B1048575 A1048575:B1048576 A1048576:B1048576 B1', 'A1048576'),
                insertRows(1, '3'), hlinkOps(1, 'A1 B2 C4 D5', 'A1:B2 B2:C4 C4:D5', 'A1 A1048575:B1048576 A1048576:B1048576 B1')
            );
            testRunner.runBidiTest(
                insertRows(1, '3:4 4:5'), hlinkOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4 D4:E5'),
                insertRows(1, '3:4 4:5'), hlinkOps(1, 'A1 B2 C5 D8', 'A1:B2 B2:C5 C5:D8 D8:E9')
            );
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(insertRows(1, '3'), hlinkOps(2, 'A1:D4'));
        });
    });
*/

    @Test
    public void insertRowsHyperlinks01() throws JSONException {
        // Result: Should transform ranges.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'), hlinkOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4', 'A1 A1048574:B1048575 A1048575:B1048576 A1048576:B1048576 B1', 'A1048576'),
        //  insertRows(1, '3'), hlinkOps(1, 'A1 B2 C4 D5', 'A1:B2 B2:C4 C4:D5', 'A1 A1048575:B1048576 A1048576:B1048576 B1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createHlinkOps("1", "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4", "A1 A1048574:B1048575 A1048575:B1048576 A1048576:B1048576 B1", "A1048576").toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createHlinkOps("1", "A1 B2 C4 D5", "A1:B2 B2:C4 C4:D5", "A1 A1048575:B1048576 A1048576:B1048576 B1").toString()
        );
    }

    @Test
    public void insertRowsHyperlinks02() throws JSONException {
        // Result: Should transform ranges.
        // testRunner.runBidiTest(
        //  insertRows(1, '3:4 4:5'), hlinkOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4 D4:E5'),
        //  insertRows(1, '3:4 4:5'), hlinkOps(1, 'A1 B2 C5 D8', 'A1:B2 B2:C5 C5:D8 D8:E9'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3:4 4:5").toString(),
            SheetHelper.createHlinkOps("1", "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4 D4:E5").toString(),
            SheetHelper.createInsertRowsOp(1, "3:4 4:5").toString(),
            SheetHelper.createHlinkOps("1", "A1 B2 C5 D8", "A1:B2 B2:C5 C5:D8 D8:E9").toString()
        );
    }

    @Test
    public void insertRowsHyperlinks03() throws JSONException {
        // Result: Should not transform ranges in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  hlinkOps(2, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createHlinkOps("2", "A1:D4").toString()
        );
    }

/*
    describe('"insertRows" and "insertTable"', function () {
        it('should shift and expand table range', function () {
            testRunner.runBidiTest(
                insertRows(1, '5'), opSeries2(insertTable, 1, 'T', ['A1:D4', 'A3:D6', 'A5:D8']),
                insertRows(1, '5'), opSeries2(insertTable, 1, 'T', ['A1:D4', 'A3:D7', 'A6:D9'])
            );
        });
        it('should delete table range shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertRows(1, '1:9'), insertTable(1, 'T', 'A1048573:D1048576'),
                [deleteTable(1, 'T'), insertRows(1, '1:9')], []
            );
            testRunner.runBidiTest(
                insertRows(1, '4:5'), insertTable(1, 'T', 'A1048573:D1048576'),
                [deleteTable(1, 'T'), insertRows(1, '4:5')], []
            );
        });
        it('should not transform tables in different sheets', function () {
            -testRunner.runBidiTest(insertRows(1, '3'), insertTable(2, 'T', 'A1:D4'));
        });
    });
*/

    @Test
    public void insertRowsInsertTable01() throws JSONException {
        // Result: Should shift and expand table range.
        // testRunner.runBidiTest(
        //  insertRows(1, '5'),
        //  opSeries2(insertTable, 1, 'T', ['A1:D4', 'A3:D6', 'A5:D8']),
        //  insertRows(1, '5'),
        //  opSeries2(insertTable, 1, 'T', ['A1:D4', 'A3:D7', 'A6:D9']));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1:D4", null),
                SheetHelper.createInsertTableOp(1, "T", "A3:D6", null),
                SheetHelper.createInsertTableOp(1, "T", "A5:D8", null)
            ),
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1:D4", null),
                SheetHelper.createInsertTableOp(1, "T", "A3:D7", null),
                SheetHelper.createInsertTableOp(1, "T", "A6:D9", null)
            )
        );
    }

    @Test
    public void insertRowsInsertTable02() throws JSONException {
        // Result: Should shift and expand table range.
        // testRunner.runBidiTest(
        //  insertRows(1, '1:9'),
        //  insertTable(1, 'T', 'A1048573:D1048576'),
        //  [deleteTable(1, 'T'),insertRows(1, '1:9')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:9").toString(),
            SheetHelper.createInsertTableOp(1, "T", "A1048573:D1048576", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createInsertRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsInsertTable03() throws JSONException {
        // Result: Should shift and expand table range.
        // testRunner.runBidiTest(
        //  insertRows(1, '4:5'),
        //  insertTable(1, 'T', 'A1048573:D1048576'),
        //  [deleteTable(1, 'T'), insertRows(1, '4:5')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4:5").toString(),
            SheetHelper.createInsertTableOp(1, "T", "A1048573:D1048576", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createInsertRowsOp(1, "4:5")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsInsertTable04() throws JSONException {
        // Result: Should not transform tables in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  insertTable(2, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createInsertTableOp(2, "T", "A1:D4", null).toString()
        );
    }

/*
    describe('"insertRows" and "changeTable"', function () {
        it('should shift and expand table range', function () {
            testRunner.runBidiTest(
                insertRows(1, '5'), opSeries2(changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'A3:D6', 'A5:D8']),
                insertRows(1, '5'), opSeries2(changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'A3:D7', 'A6:D9'])
            );
        });
        it('should delete table range shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertRows(1, '1:9'), changeTable(1, 'T', 'A1048573:D1048576'),
                [deleteTable(1, 'T'), insertRows(1, '1:9')], []
            );
            testRunner.runBidiTest(
                insertRows(1, '4:5'), changeTable(1, 'T', 'A1048573:D1048576'),
                [deleteTable(1, 'T'), insertRows(1, '4:5')], []
            );
        });
        it('should not transform tables in different sheets', function () {
            testRunner.runBidiTest(insertRows(1, '3'), changeTable(2, 'T', 'A1:D4'));
        });
    });
*/

    @Test
    public void insertRowsChangeTable01() throws JSONException {
        // Result: Should shift and expand table range.
        // testRunner.runBidiTest(
        //  insertRows(1, '5'),
        //  opSeries2(
        //      changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'A3:D6', 'A5:D8']),
        //  insertRows(1, '5'),
        //  opSeries2(
        //  changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'A3:D7', 'A6:D9']));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeTableOp(1, "T", null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeTableOp(1, "T", "A1:D4", null),
                SheetHelper.createChangeTableOp(1, "T", "A3:D6", null),
                SheetHelper.createChangeTableOp(1, "T", "A5:D8", null)
            ),
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeTableOp(1, "T", null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeTableOp(1, "T", "A1:D4", null),
                SheetHelper.createChangeTableOp(1, "T", "A3:D7", null),
                SheetHelper.createChangeTableOp(1, "T", "A6:D9", null)
            )
        );
    }

    @Test
    public void insertRowsChangeTable02() throws JSONException {
        // Result: Should delete table range shifted outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '1:9'),
        //  changeTable(1, 'T', 'A1048573:D1048576'),
        //  [deleteTable(1, 'T'), insertRows(1, '1:9')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:9").toString(),
            SheetHelper.createChangeTableOp(1, "T", "A1048573:D1048576", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createInsertRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsChangeTable03() throws JSONException {
        // Result: Should delete table range shifted outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '4:5'),
        //  changeTable(1, 'T', 'A1048573:D1048576'),
        //  [deleteTable(1, 'T'), insertRows(1, '4:5')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "4:5").toString(),
            SheetHelper.createChangeTableOp(1, "T", "A1048573:D1048576", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createInsertRowsOp(1, "4:5")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsChangeTable04() throws JSONException {
        // Result: Should not transform tables in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeTable(2, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeTableOp(2, "T", "A1:D4", null).toString()
        );
    }

/*
    describe('"insertRows" and tables', function () {
        it('should handle insert/change sequence', function () {
            testRunner.runBidiTest(
                insertRows(1, '1:9'), [insertTable(1, 'T', 'A1048573:D1048576'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS })],
                [deleteTable(1, 'T'), insertRows(1, '1:9')], []
            );
        });
        it('should handle insert/change/delete sequence', function () {
            -testRunner.runBidiTest(
                insertRows(1, '1:9'), [insertTable(1, 'T', 'A1048573:D1048576'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS }), deleteTable(1, 'T')],
                null, []
            );
        });
    });
*/

    @Test
    public void insertRowsInsertTables01() throws JSONException {
        // Result: Should handle insert/change sequence.
        // testRunner.runBidiTest(
        //  insertRows(1, '1:9'),
        //  [
        //      insertTable(1, 'T', 'A1048573:D1048576'),
        //      changeTable(1, 'T', { attrs: ATTRS }),
        //      changeTableCol(1, 'T', 0, { attrs: ATTRS })],
        //  [
        //      deleteTable(1, 'T'),
        //      insertRows(1, '1:9')
        //  ],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:9").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1048573:D1048576", null),
                SheetHelper.createChangeTableOp(1, "T", null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeTableColOp(1, "T", 0, "{ attrs: " + SheetHelper.ATTRS + " }")
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createInsertRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsInsertTables02() throws JSONException {
        // Result: Should handle insert/change/delete sequence.
        // testRunner.runBidiTest(
        //  insertRows(1, '1:9'),
        //  [
        //      insertTable(1, 'T', 'A1048573:D1048576'),
        //      changeTable(1, 'T', { attrs: ATTRS }),
        //      changeTableCol(1, 'T', 0, { attrs: ATTRS }),
        //      deleteTable(1, 'T')],
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:9").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1048573:D1048576", null),
                SheetHelper.createChangeTableOp(1, "T", null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeTableColOp(1, "T", 0, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createDeleteTableOp(1, "T")
            ),
            SheetHelper.createInsertRowsOp(1, "1:9").toString(),
            "[]"
        );
    }

/*
    describe('"insertRows" and "insertDVRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                insertRows(1, '5'), opSeries2(insertDVRule, 1, 0, ['A1:D4', 'A3:D6', 'A5:D8']),
                insertRows(1, '5'), opSeries2(insertDVRule, 1, 0, ['A1:D5', 'A3:D7', 'A6:D9'])
            );
        });
        it('should delete rule shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertRows(1, '1:9'), insertDVRule(1, 0, 'A1048573:D1048576'),
                [deleteDVRule(1, 0), insertRows(1, '1:9')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(insertRows(1, '3'), insertDVRule(2, 0, 'A1:D4'));
        });
    });
*/

    @Test
    public void insertRowsInsertDVRule01() throws JSONException {
        // Result: Should shift rule.
        // testRunner.runBidiTest(
        //  insertRows(1, '5'),
        //  opSeries2(
        //      insertDVRule, 1, 0, ['A1:D4', 'A3:D6', 'A5:D8']
        //  ),
        //  insertRows(1, '5'),
        //  opSeries2(
        //      insertDVRule, 1, 0, ['A1:D5', 'A3:D7', 'A6:D9'])
        //  );
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "A3:D6", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "A5:D8", null)
            ),
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertDVRuleOp(1, 0, "A1:D5", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "A3:D7", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "A6:D9", null)
            )
        );
    }

    @Test
    public void insertRowsInsertDVRule02() throws JSONException {
        // Result: Should delete rule shifted outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '1:9'),
        //  insertDVRule(1, 0, 'A1048573:D1048576'),
        //  [
        //      deleteDVRule(1, 0),
        //      insertRows(1, '1:9')
        //  ],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:9").toString(),
            SheetHelper.createInsertDVRuleOp(1, 0, "A1048573:D1048576", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createInsertRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsInsertDVRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  insertDVRule(2, 0, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createInsertDVRuleOp(2, 0, "A1:D4", null).toString()
        );
    }

/*
    describe('"insertRows" and "changeDVRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                insertRows(1, '5'), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'A3:D6', 'A5:D8']),
                insertRows(1, '5'), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D5', 'A3:D7', 'A6:D9'])
            );
        });
        it('should delete rule shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertRows(1, '1:9'), changeDVRule(1, 0, 'A1048573:D1048576'),
                [deleteDVRule(1, 0), insertRows(1, '1:9')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            -testRunner.runBidiTest(insertRows(1, '3'), changeDVRule(2, 0, 'A1:D4'));
        });
    });
*/

    @Test
    public void insertRowsChangeDVRule01() throws JSONException {
        // Result: Should shift rule.
        // testRunner.runBidiTest(
        //  insertRows(1, '5'),
        //  opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'A3:D6', 'A5:D8']),
        //  insertRows(1, '5'),
        //  opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D5', 'A3:D7', 'A6:D9']));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeDVRuleOp(1, 0, null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "A3:D6", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "A5:D8", null)
            ),
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeDVRuleOp(1, 0, null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeDVRuleOp(1, 0, "A1:D5", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "A3:D7", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "A6:D9", null)
            )
        );
    }

    @Test
    public void insertRowsChangeDVRule02() throws JSONException {
        // Result: Should delete rule shifted outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '1:9'),
        //  changeDVRule(1, 0, 'A1048573:D1048576'),
        //  [deleteDVRule(1, 0), insertRows(1, '1:9')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:9").toString(),
            SheetHelper.createChangeDVRuleOp(1, 0, "A1048573:D1048576", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createInsertRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsChangeDVRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeDVRule(2, 0, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeDVRuleOp(2, 0, "A1:D4", null).toString()
        );
    }

/*
    describe('"insertRows" and "insertCFRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                insertRows(1, '5'), opSeries2(insertCFRule, 1, 'R1', ['A1:D4', 'A3:D6', 'A5:D8']),
                insertRows(1, '5'), opSeries2(insertCFRule, 1, 'R1', ['A1:D5', 'A3:D7', 'A6:D9'])
            );
        });
        it('should delete rule shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertRows(1, '1:9'), insertCFRule(1, 'R1', 'A1048573:D1048576'),
                [deleteCFRule(1, 'R1'), insertRows(1, '1:9')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            -testRunner.runBidiTest(insertRows(1, '3'), insertCFRule(2, 'R1', 'A1:D4'));
        });
    });
*/

    @Test
    public void insertRowsInsertCFRule01() throws JSONException {
        // Result: Should shift rule.
        // testRunner.runBidiTest(
        //  insertRows(1, '5'), opSeries2(insertCFRule, 1, 'R1', ['A1:D4', 'A3:D6', 'A5:D8']),
        //  insertRows(1, '5'), opSeries2(insertCFRule, 1, 'R1', ['A1:D5', 'A3:D7', 'A6:D9']));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "A3:D6", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "A5:D8", null)
            ),
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D5", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "A3:D7", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "A6:D9", null)
            )
        );
    }

    @Test
    public void insertRowsInsertCFRule02() throws JSONException {
        // Result: Should delete rule shifted outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '1:9'),
        //  insertCFRule(1, 'R1', 'A1048573:D1048576'),
        //  [deleteCFRule(1, 'R1'), insertRows(1, '1:9')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:9").toString(),
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1048573:D1048576", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                SheetHelper.createInsertRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsInsertCFRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  insertCFRule(2, 'R1', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:3").toString(),
            SheetHelper.createInsertCFRuleOp(2, "R1", "A1:D4", null).toString()
        );
    }

/*
    describe('"insertRows" and "changeCFRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                insertRows(1, '5'), opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D4', 'A3:D6', 'A5:D8']),
                insertRows(1, '5'), opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D5', 'A3:D7', 'A6:D9'])
            );
        });
        it('should delete rule shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertRows(1, '1:9'), changeCFRule(1, 'R1', 'A1048573:D1048576'),
                [deleteCFRule(1, 'R1'), insertRows(1, '1:9')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            -testRunner.runBidiTest(insertRows(1, '3'), changeCFRule(2, 'R1', 'A1:D4'));
        });
    });
*/

    @Test
    public void insertRowsChangeCFRule01() throws JSONException {
        // Result: Should shift rule.
        // testRunner.runBidiTest(
        //  insertRows(1, '5'),
        //  opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D4', 'A3:D6', 'A5:D8']),
        //  insertRows(1, '5'),
        //  opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D5', 'A3:D7', 'A6:D9']));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeCFRuleOp(1, "R1", null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A3:D6", null),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A5:D8", null)
            ),
            SheetHelper.createInsertRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeCFRuleOp(1, "R1", null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D5", null),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A3:D7", null),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A6:D9", null)
            )
        );
    }

    @Test
    public void insertRowsChangeCFRule02() throws JSONException {
        // Result: Should delete rule shifted outside sheet.
        // testRunner.runBidiTest(
        //  insertRows(1, '1:9'),
        //  changeCFRule(1, 'R1', 'A1048573:D1048576'),
        //  [deleteCFRule(1, 'R1'), insertRows(1, '1:9')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "1:9").toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1048573:D1048576", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                SheetHelper.createInsertRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsChangeCFRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeCFRule(2, 'R1', 'A1:D4'))
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCFRuleOp(2, "R1", "A1:D4", null).toString()
        );
    }

/*
    describe('"insertRows" and cell anchor', function () {
        it('should transform cell anchor', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), cellAnchorOps(1, 'A1', 'B2', 'C3', 'D4', 'E1048575'),
                insertRows(1, '3'), cellAnchorOps(1, 'A1', 'B2', 'C4', 'D5', 'E1048576')
            );
            testRunner.runBidiTest(
                insertRows(1, '3:4 4:5'), cellAnchorOps(1, 'A1', 'B2', 'C3', 'D4', 'E5', 'F1048572'),
                insertRows(1, '3:4 4:5'), cellAnchorOps(1, 'A1', 'B2', 'C5', 'D8', 'E9', 'F1048576')
            );
        });
        it('should handle deleted cell note', function () {
            testRunner.runBidiTest(insertRows(1, '3'), insertNote(1, 'A1048576', 'abc'), [deleteNote(1, 'A1048576'), insertRows(1, '3')], []);
            testRunner.runBidiTest(insertRows(1, '3'), deleteNote(1, 'A1048576'),        null,                                            []);
            testRunner.runBidiTest(insertRows(1, '3'), changeNote(1, 'A1048576', 'abc'), [deleteNote(1, 'A1048576'), insertRows(1, '3')], []);
        });
        it('should handle deleted cell comment', function () {
            testRunner.runBidiTest(insertRows(1, '3'), insertComment(1, 'A1048576', 0, { text: 'abc' }), [deleteComment(1, 'A1048576', 0), insertRows(1, '3')], []);
            testRunner.runBidiTest(insertRows(1, '3'), insertComment(1, 'A1048576', 1, { text: 'abc' }), null,                                                  []);
            testRunner.runBidiTest(insertRows(1, '3'), deleteComment(1, 'A1048576', 0),                  null,                                                  []);
            testRunner.runBidiTest(insertRows(1, '3'), deleteComment(1, 'A1048576', 1),                  null,                                                  []);
            testRunner.runBidiTest(insertRows(1, '3'), changeComment(1, 'A1048576', 0, { text: 'abc' }), [deleteComment(1, 'A1048576', 0), insertRows(1, '3')], []);
            testRunner.runBidiTest(insertRows(1, '3'), changeComment(1, 'A1048576', 1, { text: 'abc' }), null,                                                  []);
        });
        it('should not transform cell anchor in different sheets', function () {
            -testRunner.runBidiTest(insertRows(1, '3'), cellAnchorOps(2, 'D4'));
        });
    });
*/

    @Test
    public void insertRowsCellAnchor01() throws JSONException {
        // Result: Should transform cell anchor.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  cellAnchorOps(1, 'A1', 'B2', 'C3', 'D4', 'E1048575'),
        //  insertRows(1, '3'),
        //  cellAnchorOps(1, 'A1', 'B2', 'C4', 'D5', 'E1048576'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createCellAnchorOps(1, "A1", "B2", "C3", "D4", "E1048575").toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString().toString(),
            SheetHelper.createCellAnchorOps(1, "A1", "B2", "C4", "D5", "E1048576").toString()
        );
    }

    @Test
    public void insertRowsCellAnchor02() throws JSONException {
        // Result: Should transform cell anchor.
        // testRunner.runBidiTest(
        //  insertRows(1, '3:4 4:5'),
        //  cellAnchorOps(1, 'A1', 'B2', 'C3', 'D4', 'E5', 'F1048572'),
        //  insertRows(1, '3:4 4:5'),
        //  cellAnchorOps(1, 'A1', 'B2', 'C5', 'D8', 'E9', 'F1048576'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3:4 4:5").toString(),
            SheetHelper.createCellAnchorOps(1, "A1", "B2", "C3", "D4", "E5", "F1048572").toString(),
            SheetHelper.createInsertRowsOp(1, "3:4 4:5").toString().toString(),
            SheetHelper.createCellAnchorOps(1, "A1", "B2", "C5", "D8", "E9", "F1048576").toString()
        );
    }

    @Test
    public void insertRowsCellAnchor03() throws JSONException {
        // Result: Should handle deleted cell note.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  insertNote(1, 'A1048576', 'abc'),
        //  [deleteNote(1, 'A1048576'), insertRows(1, '3')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createInsertNoteOp(1, "A1048576", "abc", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "A1048576"),
                SheetHelper.createInsertRowsOp(1, "3")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsCellAnchor04() throws JSONException {
        // Result: Should handle deleted cell note.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  deleteNote(1, 'A1048576'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createDeleteNoteOp(1, "A1048576").toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void insertRowsCellAnchor05() throws JSONException {
        // Result: Should handle deleted cell note.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeNote(1, 'A1048576', 'abc'),
        //  [deleteNote(1, 'A1048576'),
        //  insertRows(1, '3')], []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeNoteOp(1, "A1048576", "abc", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "A1048576"),
                SheetHelper.createInsertRowsOp(1, "3")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsCellAnchor06() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  insertComment(1, 'A1048576', 0, { text: 'abc' }),
        //  [deleteComment(1, 'A1048576', 0), insertRows(1, '3')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createInsertCommentOp(1, "A1048576", 0, "{ text: 'abc' }").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "A1048576", 0),
                SheetHelper.createInsertRowsOp(1, "3")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsCellAnchor07() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  insertComment(1, 'A1048576', 1, { text: 'abc' }),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createInsertCommentOp(1, "A1048576", 1, "{ text: 'abc' }").toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void insertRowsCellAnchor08() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  deleteComment(1, 'A1048576', 0),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createDeleteCommentOp(1, "A1048576", 0).toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void insertRowsCellAnchor09() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  deleteComment(1, 'A1048576', 1),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createDeleteCommentOp(1, "A1048576", 1).toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void insertRowsCellAnchor10() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeComment(1, 'A1048576', 0, { text: 'abc' }),
        //  [deleteComment(1, 'A1048576', 0), insertRows(1, '3')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCommentOp(1, "A1048576", 0, "{ text: 'abc' }").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "A1048576", 0),
                SheetHelper.createInsertRowsOp(1, "3")
            ),
            "[]"
        );
    }

    @Test
    public void insertRowsCellAnchor11() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  changeComment(1, 'A1048576', 1, { text: 'abc' }),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createChangeCommentOp(1, "A1048576", 1, "{ text: 'abc' }").toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void insertRowsCellAnchor12() throws JSONException {
        // Result: Should not transform cell anchor in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  cellAnchorOps(2, 'D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createCellAnchorOps(2, "D4").toString()
        );
    }

/*
    describe('"insertRows" and "moveNotes"', function () {
        it('should transform cell anchors', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), moveNotes(1, 'A1 B2 C3 D4', 'A4 B3 C2 D1'),
                insertRows(1, '3'), moveNotes(1, 'A1 B2 C4 D5', 'A5 B4 C2 D1')
            );
        });
        it('should handle deleted source anchor', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), moveNotes(1, 'A1 B1048576 C1048576 D1', 'A5 B5 A1048576 D5'),
                [deleteNote(1, 'B5'), deleteNote(1, 'A1048576'), insertRows(1, '3')], moveNotes(1, 'A1 D1', 'A6 D6')
            );
        });
        it('should handle deleted target anchor', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), moveNotes(1, 'A5 B5 C5', 'A2 B1048576 C2'),
                [deleteNote(1, 'B1048576'), insertRows(1, '3')], [deleteNote(1, 'B6'), moveNotes(1, 'A6 C6', 'A2 C2')]
            );
        });
        it('should not transform cell anchor in different sheets', function () {
            -testRunner.runBidiTest(insertRows(1, '3'), moveNotes(2, 'D4', 'E5'));
        });
    });
*/

    @Test
    public void insertRowsMoveNotes01() throws JSONException {
        // Result: Should not transform cell anchor in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  moveNotes(1, 'A1 B2 C3 D4', 'A4 B3 C2 D1'),
        //  insertRows(1, '3'),
        //  moveNotes(1, 'A1 B2 C4 D5', 'A5 B4 C2 D1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 B2 C3 D4", "A4 B3 C2 D1").toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 B2 C4 D5", "A5 B4 C2 D1").toString()
        );
    }

    @Test
    public void insertRowsMoveNotes02() throws JSONException {
        // Result: Should handle deleted source anchor.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  moveNotes(1, 'A1 B1048576 C1048576 D1', 'A5 B5 A1048576 D5'),
        //  [deleteNote(1, 'B5'), deleteNote(1, 'A1048576'), insertRows(1, '3')],
        //  moveNotes(1, 'A1 D1', 'A6 D6'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 B1048576 C1048576 D1", "A5 B5 A1048576 D5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "B5"),
                SheetHelper.createDeleteNoteOp(1, "A1048576"),
                SheetHelper.createInsertRowsOp(1, "3")
            ),
            SheetHelper.createMoveNotesOp(1, "A1 D1", "A6 D6").toString()
        );
    }

    @Test
    public void insertRowsMoveNotes03() throws JSONException {
        // Result: Should handle deleted target anchor.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  moveNotes(1, 'A5 B5 C5', 'A2 B1048576 C2'),
        //  [deleteNote(1, 'B1048576'), insertRows(1, '3')],
        //  [deleteNote(1, 'B6'), moveNotes(1, 'A6 C6', 'A2 C2')]);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(1, "A5 B5 C5", "A2 B1048576 C2").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "B1048576"),
                SheetHelper.createInsertRowsOp(1, "3")
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "B6"),
                SheetHelper.createMoveNotesOp(1, "A6 C6", "A2 C2")
            )
        );
    }

    @Test
    public void insertRowsMoveNotes04() throws JSONException {
        // Result: Should not transform cell anchor in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  moveNotes(2, 'D4', 'E5'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(2, "D4", "E5").toString()
        );
    }

/*
    describe('"insertRows" and "moveComments"', function () {
        it('should transform cell anchors', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), moveComments(1, 'A1 B2 C3 D4', 'A4 B3 C2 D1'),
                insertRows(1, '3'), moveComments(1, 'A1 B2 C4 D5', 'A5 B4 C2 D1')
            );
        });
        it('should handle deleted source anchor', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), moveComments(1, 'A1 B1048576 C1048576 D1', 'A5 B5 A1048576 D5'),
                [deleteComment(1, 'B5', 0), deleteComment(1, 'A1048576', 0), insertRows(1, '3')], moveComments(1, 'A1 D1', 'A6 D6')
            );
        });
        it('should handle deleted target anchor', function () {
            testRunner.runBidiTest(
                insertRows(1, '3'), moveComments(1, 'A5 B5 C5', 'A2 B1048576 C2'),
                [deleteComment(1, 'B1048576', 0), insertRows(1, '3')], [deleteComment(1, 'B6', 0), moveComments(1, 'A6 C6', 'A2 C2')]
            );
        });
        it('should not transform cell anchor in different sheets', function () {
            testRunner.runBidiTest(insertRows(1, '3'), moveComments(2, 'D4', 'E5'));
        });
    });
*/

    @Test
    public void insertRowsMoveComments01() throws JSONException {
        // Result: Should transform cell anchors.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  moveComments(1, 'A1 B2 C3 D4', 'A4 B3 C2 D1'),
        //  insertRows(1, '3'),
        //  moveComments(1, 'A1 B2 C4 D5', 'A5 B4 C2 D1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(1, "A1 B2 C3 D4", "A4 B3 C2 D1").toString(),
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(1, "A1 B2 C4 D5", "A5 B4 C2 D1").toString()
        );
    }

    @Test
    public void insertRowsMoveComments02() throws JSONException {
        // Result: Should handle deleted source anchor.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  moveComments(1, 'A1 B1048576 C1048576 D1', 'A5 B5 A1048576 D5'),
        //  [deleteComment(1, 'B5', 0), deleteComment(1, 'A1048576', 0), insertRows(1, '3')],
        //  moveComments(1, 'A1 D1', 'A6 D6'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(1, "A1 B1048576 C1048576 D1", "A5 B5 A1048576 D5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "B5", 0),
                SheetHelper.createDeleteCommentOp(1, "A1048576", 0),
                SheetHelper.createInsertRowsOp(1, "3")
            ),
            SheetHelper.createMoveCommentsOp(1, "A1 D1", "A6 D6").toString()
        );
    }

    @Test
    public void insertRowsMoveComments03() throws JSONException {
        // Result: Should handle deleted target anchor.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  moveComments(1, 'A5 B5 C5', 'A2 B1048576 C2'),
        //  [deleteComment(1, 'B1048576', 0), insertRows(1, '3')],
        //  [deleteComment(1, 'B6', 0), moveComments(1, 'A6 C6', 'A2 C2')]);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(1, "A5 B5 C5", "A2 B1048576 C2").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "B1048576", 0),
                SheetHelper.createInsertRowsOp(1, "3")
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "B6", 0),
                SheetHelper.createMoveCommentsOp(1, "A6 C6", "A2 C2")
            )
        );
    }

    @Test
    public void insertRowsMoveComments04() throws JSONException {
        // Result: Should not transform cell anchor in different sheets.
        // testRunner.runBidiTest(
        //  insertRows(1, '3'),
        //  moveComments(2, 'D4', 'E5'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(2, "D4", "E5").toString()
        );
    }

/*
 * NOT IMPLMENTED -> nothing is transformed and we do not have warnings in the backend transformation
 *
    describe('"insertRows" and drawing anchor', function () {
        it('should transform drawing anchor', function () {
            -testRunner.runBidiTest(
                insertRows(1, '3'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 A2 10 10 B3 20 0', '2 A2 10 10 B3 20 20', '2 A3 10 10 B4 20 20', '2 A7 10 10 B1048576 20 20'),
                insertRows(1, '3'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 A2 10 10 B3 20 0', '2 A2 10 10 B4 20 20', '2 A4 10 10 B5 20 20', '2 A7 10 10 B1048576 20 20'),
                { warnings: true } // missing transformation of absolute drawing position
            );
            testRunner.runBidiTest(
                insertRows(1, '3:4 4:5'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 A2 10 10 B3 20 20', '2 A3 10 10 B4 20 20', '2 A4 10 10 B5 20 20', '2 A7 10 10 B1048575 20 20'),
                insertRows(1, '3:4 4:5'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 A2 10 10 B5 20 20', '2 A5 10 10 B8 20 20', '2 A8 10 10 B9 20 20', '2 A8 10 10 B1048576 20 20'),
                { warnings: true } // missing transformation of absolute drawing position
            );
        });
        it('should log warnings for absolute anchor', function () {
            var anchorOps = drawingAnchorOps(1, '2 A1 0 0 C3 0 0');
            testRunner.expectBidiWarnings(insertRows(1, '3'), anchorOps, anchorOps.length);
        });
        it('should not log warnings without anchor attribute', function () {
            testRunner.expectBidiWarnings(insertRows(1, '3'), drawingNoAnchorOps(1), 0);
        });
        it('should not transform drawing anchor in different sheets', function () {
            testRunner.runBidiTest(insertRows(1, '3'), drawingAnchorOps(2, '2 A1 10 10 D4 20 20'));
        });
    });
*/

/*
 * NOT IMPLMENTED -> NO SHEET SELECTION ON THE SERVER
 *
    describe('"insertRows" and "sheetSelection"', function () {
        it('should transform selected ranges', function () {
            testRunner.runBidiTest(
                insertRows(1, '3:4 4:5'), sheetSelection(1, 'A1 B2 C3 D4 A1:B2 B2:C3 C3:D4 D4:E5 A1048569:D1048572 A1048570:D1048573 A1048571:D1048574 A1048572:D1048575', 0, 'A1', [0, 1]),
                insertRows(1, '3:4 4:5'), sheetSelection(1, 'A1 B2 C5 D8 A1:B2 B2:C5 C5:D8 D8:E9 A1048573:D1048576 A1048574:D1048576 A1048575:D1048576 A1048576:D1048576', 0, 'A1', [0, 1])
            );
        });
        it('should transform active address', function () {
            testRunner.runBidiTest(
                insertRows(1, '3:4 4:5'), opSeries2(sheetSelection, 1, 'A1:D1048576', 0, ['A1', 'B2', 'C3', 'D4', 'D1048572', 'E1048573']),
                insertRows(1, '3:4 4:5'), opSeries2(sheetSelection, 1, 'A1:D1048576', 0, ['A1', 'B2', 'C5', 'D8', 'D1048576', 'E1048576'])
            );
        });
        it('should transform origin address', function () {
            testRunner.runBidiTest(
                insertRows(1, '3:4 4:5'), opSeries2(sheetSelection, 1, 'A1:D1048576', 0, 'A1', ['A1', 'B2', 'C3', 'D4', 'A1048572', 'B1048573']),
                insertRows(1, '3:4 4:5'), opSeries2(sheetSelection, 1, 'A1:D1048576', 0, 'A1', ['A1', 'B2', 'C5', 'D8', 'A1048576', 'B1048576'])
            );
        });
        it('should transform deleted active range', function () {
            testRunner.runBidiTest(insertRows(1, '3'), sheetSelection(1, 'D4 C3 A1048576:D1048576 B2 A1', 0, 'D4',       'D4'), null, sheetSelection(1, 'D5 C4 B2 A1', 0, 'D5', 'D5'));
            testRunner.runBidiTest(insertRows(1, '3'), sheetSelection(1, 'D4 C3 A1048576:D1048576 B2 A1', 1, 'C3',       'C3'), null, sheetSelection(1, 'D5 C4 B2 A1', 1, 'C4', 'C4'));
            testRunner.runBidiTest(insertRows(1, '3'), sheetSelection(1, 'D4 C3 A1048576:D1048576 B2 A1', 2, 'A1048576', 'A1'), null, sheetSelection(1, 'D5 C4 B2 A1', 0, 'D5'));
            testRunner.runBidiTest(insertRows(1, '3'), sheetSelection(1, 'D4 C3 A1048576:D1048576 B2 A1', 3, 'B2',       'B2'), null, sheetSelection(1, 'D5 C4 B2 A1', 2, 'B2', 'B2'));
            testRunner.runBidiTest(insertRows(1, '3'), sheetSelection(1, 'D4 C3 A1048576:D1048576 B2 A1', 4, 'A1',       'A1'), null, sheetSelection(1, 'D5 C4 B2 A1', 3, 'A1', 'A1'));
        });
        it('should keep active cell when deleting the selection', function () {
            testRunner.runBidiTest(insertRows(1, '3:4 4:5'), sheetSelection(1, 'F1048575:I1048576 A1048573:D1048576', 0, 'G1048575'), null, sheetSelection(1, 'G1048576', 0, 'G1048576'));
            testRunner.runBidiTest(insertRows(1, '3:4 4:5'), sheetSelection(1, 'F1048575:I1048576 A1048573:D1048576', 1, 'B1048574'), null, sheetSelection(1, 'B1048576', 0, 'B1048576'));
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(insertRows(1, '3'), sheetSelection(2, 'A1:D4', 0, 'A1'));
        });
    });
*/
}
