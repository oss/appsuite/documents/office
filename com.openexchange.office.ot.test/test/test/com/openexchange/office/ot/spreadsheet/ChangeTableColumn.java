/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeTableColumn {

/*
    describe('"changeTableCol" and "changeTableCol"', function () {
        it('should skip different tables or columns', function () {
            testRunner.runTest(changeTableCol(1, null, 0, { attrs: ATTRS }), changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
            testRunner.runTest(changeTableCol(1, 'T1', 0, { attrs: ATTRS }), changeTableCol(1, null, 0, { attrs: ATTRS }));
            testRunner.runTest(changeTableCol(1, 'T1', 0, { attrs: ATTRS }), changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
            testRunner.runTest(changeTableCol(1, 'T1', 0, { attrs: ATTRS }), changeTableCol(1, 'T1', 1, { attrs: ATTRS }));
            testRunner.runTest(changeTableCol(1, null, 0, { attrs: ATTRS }), changeTableCol(2, null, 0, { attrs: ATTRS }));
        });
        it('should fail for same table in different sheets', function () {
            testRunner.expectError(changeTableCol(1, 'T1', 0, { attrs: ATTRS }), changeTableCol(2, 'T1', 0, { attrs: ATTRS }));
        });
        it('should not process independent attributes', function () {
            testRunner.runTest(changeTableCol(1, 'T1', 0, { attrs: ATTRS_I1 }), changeTableCol(1, 'T1', 0, { attrs: ATTRS_I2 }));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeTableCol(1, 'T1', 0, { attrs: ATTRS_O1 }), changeTableCol(1, 'T1', 0, { attrs: ATTRS_O2 }),
                changeTableCol(1, 'T1', 0, { attrs: ATTRS_R1 }), changeTableCol(1, 'T1', 0, { attrs: ATTRS_R2 })
            );
        });
        it('should fail to transform multi-column sort', function () {
            testRunner.expectBidiError(changeTableCol(1, 'T1', 0, { attrs: ATTRS_I1 }), changeTableCol(1, 'T1', 0, { attrs: ATTRS_I2, sort: true }));
        });
        it('should remove entire operation if nothing will change', function () {
            testRunner.runTest(changeTableCol(1, 'T1', 0, { attrs: ATTRS }), changeTableCol(1, 'T1', 0, { attrs: ATTRS2 }), null, []);
            -testRunner.runTest(changeTableCol(1, 'T1', 0, { attrs: ATTRS }), changeTableCol(1, 'T1', 0, { attrs: ATTRS }), [], []);
        });
    });
*/

    @Test
    public void changeTableColumnChangeTableColumn01() throws JSONException {
        // Result: Should skip different tables or columns.
        // testRunner.runTest(
        //  changeTableCol(1, null, 0, { attrs: ATTRS }),
        //  changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableColOp(0, null, 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(0, "T2", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn02() throws JSONException {
        // Result: Should skip different tables or columns.
        // testRunner.runTest(
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }),
        //  changeTableCol(1, null, 0, { attrs: ATTRS }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(0, null, 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn03() throws JSONException {
        // Result: Should skip different tables or columns.
        // testRunner.runTest(
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }),
        //  changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T2", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn04() throws JSONException {
        // Result: Should skip different tables or columns.
        //  testRunner.runTest(
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }),
        //  changeTableCol(1, 'T1', 1, { attrs: ATTRS }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 1, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn05() throws JSONException {
        // Result: Should skip different tables or columns.
        // testRunner.runTest(
        //  changeTableCol(1, null, 0, { attrs: ATTRS }),
        //  changeTableCol(2, null, 0, { attrs: ATTRS }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableColOp(1, null, 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(2, null, 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn06() throws JSONException {
        // Result: Should fail for same table in different sheets.
        // testRunner.expectError(
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }),
        //  changeTableCol(2, 'T1', 0, { attrs: ATTRS }));
        SheetHelper.expectError(
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(2, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn07() throws JSONException {
        // Result: Should not process independent attributes.
        // testRunner.runTest(
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS_I1 }),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS_I2 }));
        SheetHelper.expectError(
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(2, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I2.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn08() throws JSONException {
        // Result: Should reduce attribute sets.
        // testRunner.runTest(
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS_O1 }),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS_O2 }),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS_R1 }),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS_R2 }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS_O1.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS_O2.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS_R1.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS_R2.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn09() throws JSONException {
        // Result: Should fail to transform multi-column sort.
        // testRunner.expectBidiError(
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS_I1 }),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS_I2, sort: true }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I2.toString() + ", sort: true }").toString()).toString()
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn10() throws JSONException {
        // Result: Should remove entire operation if nothing will change.
        // testRunner.runTest(
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS2 }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS2.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeTableColumnChangeTableColumn11() throws JSONException {
        // Result: Should remove entire operation if nothing will change.
        // testRunner.runTest(
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            "[]",
            "[]"
        );
    }
}
