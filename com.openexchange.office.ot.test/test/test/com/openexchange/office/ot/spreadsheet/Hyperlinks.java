/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class Hyperlinks {

/*
    describe('hyperlinks and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(hlinkOps(1), [
                GLOBAL_OPS, STYLESHEET_OPS,
                nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/

    @Test
    public void Hyperlinks01() {

        SheetHelper.runBidiTest(SheetHelper.createHlinkOps("1").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createNameOps("1"),
                SheetHelper.createTableOps("1"),
                SheetHelper.createDvRuleOps("1", null),
                SheetHelper.createCfRuleOps("1", null),
                SheetHelper.createNoteOps("1"),
                SheetHelper.createCommentOps("1"),
                // select ...
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

    // insertHyperlink ----------------------------------------------------

/*
    describe('"insertHyperlink" and "insertHyperlink"', function () {
        var URL2 = 'https://example.org';
        it('should skip different sheets', function () {
            testRunner.runTest(insertHlink(1, 'A1:B2', URL), insertHlink(0, 'A1:B2', URL2));
        });
        it('should reduce external hyperlink ranges', function () {
            testRunner.runTest(
                insertHlink(1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', URL), insertHlink(1, 'C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6',                   URL2),
                insertHlink(1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', URL), insertHlink(1, 'C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6', URL2)
            );
        });
        it('should set no-op to "removed" state', function () {
            testRunner.runTest(insertHlink(1, 'A1:D4', URL), insertHlink(1, 'B2:C3', URL2), null, []);
        });
    });
*/


    final static String URL2 = "https://example.org";

    // it('should skip different sheets', function () {
    @Test
    public void insertHyperlinkInsertHyperlink01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertHlinkOp(1, "A1:B2", SheetHelper.URL).toString(), SheetHelper.createInsertHlinkOp(0, "A1:B2", URL2).toString());
    }

    // it('should reduce external hyperlink ranges', function () {
    @Test
    public void insertHyperlinkInsertHyperlink02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertHlinkOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", SheetHelper.URL).toString(), SheetHelper.createInsertHlinkOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", URL2).toString(),
                            SheetHelper.createInsertHlinkOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", SheetHelper.URL).toString(), SheetHelper.createInsertHlinkOp(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6", URL2).toString());

    }

    // it('should set no-op to "removed" state', function () {
    @Test
    public void insertHyperlinkInsertHyperlink03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertHlinkOp(1, "A1:D4", SheetHelper.URL).toString(), SheetHelper.createInsertHlinkOp(1, "B2:C3", URL2).toString(), SheetHelper.createInsertHlinkOp(1, "A1:D4", SheetHelper.URL).toString(), "[]");
    }

/*
    describe('"insertHyperlink" and "deleteHyperlink"', function () {
        it('should skip different sheets', function () {
            testRunner.runTest(insertHlink(1, 'A1:B2', URL), deleteHlink(0, 'A1:B2'));
        });
        it('should reduce external hyperlink ranges', function () {
            testRunner.runTest(
                insertHlink(1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', URL), deleteHlink(1, 'C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6'),
                insertHlink(1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', URL), deleteHlink(1, 'C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6')
            );
        });
        it('should set no-op to "removed" state', function () {
            testRunner.runTest(insertHlink(1, 'A1:D4', URL), deleteHlink(1, 'B2:C3'), null, []);
        });
    });
*/

    // it('should skip different sheets', function () {
    @Test
    public void insertHyperlinkDeleteHyperlink01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertHlinkOp(1, "A1:B2", SheetHelper.URL).toString(), SheetHelper.createDeleteHlinkOp(0, "A1:B2").toString());
    }

    // it('should reduce external hyperlink ranges', function () {
    @Test
    public void insertHyperlinkDeleteHyperlink02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertHlinkOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", SheetHelper.URL).toString(), SheetHelper.createDeleteHlinkOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6").toString(),
                            SheetHelper.createInsertHlinkOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", SheetHelper.URL).toString(), SheetHelper.createDeleteHlinkOp(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6").toString());
    }

    // it('should set no-op to "removed" state', function () {
    @Test
    public void insertHyperlinkDeleteHyperlink03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertHlinkOp(1, "A1:D4", SheetHelper.URL).toString(), SheetHelper.createDeleteHlinkOp(1, "B2:C3").toString(), SheetHelper.createInsertHlinkOp(1, "A1:D4", SheetHelper.URL).toString(), "[]");
    }

    // deleteHyperlink ----------------------------------------------------

/*
    describe('"deleteHyperlink" and "insertHyperlink"', function () {
        it('should skip different sheets', function () {
            testRunner.runTest(deleteHlink(1, 'A1:B2'), insertHlink(0, 'A1:B2', URL));
        });
        it('should reduce external hyperlink ranges', function () {
            testRunner.runTest(
                deleteHlink(1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3'), insertHlink(1, 'C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6',                   URL),
                deleteHlink(1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3'), insertHlink(1, 'C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6', URL)
            );
        });
        it('should set no-op to "removed" state', function () {
            testRunner.runTest(deleteHlink(1, 'A1:D4'), insertHlink(1, 'B2:C3', URL), null, []);
        });
    });
*/

    //it('should skip different sheets', function () {
    @Test
    public void deleteHyperlinkInsertHyperlink01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteHlinkOp(1, "A1:B2").toString(), SheetHelper.createInsertHlinkOp(0, "A1:B2", SheetHelper.URL).toString());
    }

    // it('should reduce external hyperlink ranges', function () {
    @Test
    public void deleteHyperlinkInsertHyperlink02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteHlinkOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3").toString(), SheetHelper.createInsertHlinkOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", SheetHelper.URL).toString(),
                            SheetHelper.createDeleteHlinkOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3").toString(), SheetHelper.createInsertHlinkOp(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6", SheetHelper.URL).toString());
    }

    // it('should set no-op to "removed" state', function () {
    @Test
    public void deleteHyperlinkInsertHyperlink03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteHlinkOp(1,  "A1:D4").toString(), SheetHelper.createInsertHlinkOp(1, "B2:C3", SheetHelper.URL).toString(), SheetHelper.createDeleteHlinkOp(1,  "A1:D4").toString(), "[]");
    }

/*
    describe('"deleteHyperlink" and "deleteHyperlink"', function () {
        it('should skip different sheets', function () {
            testRunner.runTest(deleteHlink(1, 'A1:B2'), deleteHlink(0, 'A1:B2'));
        });
        it('should reduce external hyperlink ranges', function () {
            testRunner.runTest(
                deleteHlink(1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3'), deleteHlink(1, 'C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6'),
                deleteHlink(1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3'), deleteHlink(1, 'C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6')
            );
        });
        it('should set no-op to "removed" state', function () {
            testRunner.runTest(deleteHlink(1, 'A1:D4'), deleteHlink(1, 'B2:C3'), null, []);
        });
    });
*/

    // it('should skip different sheets', function () {
    @Test
    public void deleteHyperlinkDeleteHyperlink01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteHlinkOp(1, "A1:B2").toString(), SheetHelper.createDeleteHlinkOp(0, "A1:B2").toString());
    }

    // it('should reduce external hyperlink ranges', function () {
    @Test
    public void deleteHyperlinkDeleteHyperlink02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteHlinkOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3").toString(), SheetHelper.createDeleteHlinkOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6").toString(),
                            SheetHelper.createDeleteHlinkOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3").toString(), SheetHelper.createDeleteHlinkOp(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6").toString());
    }

    // it('should set no-op to "removed" state', function () {
    @Test
    public void deleteHyperlinkDeleteHyperlink03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteHlinkOp(1, "A1:D4").toString(), SheetHelper.createDeleteHlinkOp(1, "B2:C3").toString(), SheetHelper.createDeleteHlinkOp(1, "A1:D4").toString(), "[]");
    }
}
