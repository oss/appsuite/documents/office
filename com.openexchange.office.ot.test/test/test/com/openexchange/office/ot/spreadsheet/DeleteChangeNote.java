/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class DeleteChangeNote {

    /*
     * describe('"deleteNote" and "changeNote', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runBidiTest(deleteNote(2, 'A1'), changeNote(1, 'A1', 'abc'));
                testRunner.runBidiTest(deleteNote(1, 'B2'), changeNote(1, 'A1', 'abc'));
            });
            it('should set change operation to "removed" state', function () {
                testRunner.runBidiTest(deleteNote(1, 'A1'), changeNote(1, 'A1', 'abc'), null, []);
            });
        });
     */

    @Test
    public void deleteNoteChangeNote01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  deleteNote(2, 'A1'),
        //  changeNote(1, 'A1', 'abc'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNoteOp(2, "A1").toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "abc", null).toString()
        );
    }

    @Test
    public void deleteNoteChangeNote02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  deleteNote(1, 'B2'),
        //  changeNote(1, 'A1', 'abc'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNoteOp(1, "B2").toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "abc", null).toString()
        );
    }

    @Test
    public void deleteNoteChangeNote03() throws JSONException {
        // Result: Should set change operation to "removed" state.
        // testRunner.runBidiTest(
        //  deleteNote(1, 'A1'),
        //  changeNote(1, 'A1', 'abc'), null, []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNoteOp(1, "A1").toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createDeleteNoteOp(1, "A1").toString(),
            "[]"
        );
    }
}
