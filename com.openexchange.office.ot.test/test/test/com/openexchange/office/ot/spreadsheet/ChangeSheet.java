/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeSheet {

/*
    describe('"changeSheet" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(changeSheet(1, 'S1'), [
                GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, colOps(1), rowOps(1),
                cellOps(1), hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/
    @Test
    public void changeSheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createChangeSheetOp(1, "S1", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.AUTOSTYLE_OPS,
                SheetHelper.createColOps("1", null),
                SheetHelper.createRowOps("1", null),
                SheetHelper.createCellOps("1"),
                SheetHelper.createHlinkOps("1"),
                SheetHelper.createNameOps("1"),
                SheetHelper.createTableOps("1"),
                SheetHelper.createDvRuleOps("1", null),
                SheetHelper.createCfRuleOps("1", null),
                SheetHelper.createNoteOps("1"),
                SheetHelper.createCommentOps("1"),
//              SheetHelper.createSelectOps("1"),
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

/*
    describe('"changeSheet" and "changeSheet"', function () {
        it('should not process different sheets', function () {
            testRunner.runTest(changeSheet(1, 'S1', ATTRS), changeSheet(2, 'S2', ATTRS));
        });
        it('should not process independent attributes', function () {
            testRunner.runTest(changeSheet(1, ATTRS_I1), changeSheet(1, ATTRS_I2));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeSheet(1, ATTRS_O1), changeSheet(1, ATTRS_O2),
                changeSheet(1, ATTRS_R1), changeSheet(1, ATTRS_R2)
            );
        });
        it('should reduce sheet name', function () {
            testRunner.runTest(
                changeSheet(1, 'S1', ATTRS_I1), changeSheet(1, 'S1', ATTRS_I2),
                changeSheet(1,       ATTRS_I1), changeSheet(1,       ATTRS_I2)
            );
            testRunner.runTest(
                changeSheet(1, 'S1', ATTRS_I1), changeSheet(1, 'S2', ATTRS_I2),
                changeSheet(1, 'S1', ATTRS_I1), changeSheet(1,       ATTRS_I2)
            );
            testRunner.runBidiTest(changeSheet(1, 'S1', ATTRS_I1), changeSheet(1, ATTRS_I2));
        });
        it('should remove entire operation if nothing will change', function () {
            testRunner.runTest(changeSheet(1, 'S1', ATTRS), changeSheet(1, 'S2', ATTRS2), null,                []);
            testRunner.runTest(changeSheet(1, 'S1', ATTRS), changeSheet(1, 'S2', ATTRS), changeSheet(1, 'S1'), []);
            testRunner.runTest(changeSheet(1, 'S1', ATTRS), changeSheet(1, 'S1', ATTRS), [],                   []);
        });
        it('should fail to give two sheets the same name', function () {
            testRunner.expectError(changeSheet(1, 'S1'), changeSheet(2, 'S1'));
        });
    });
*/
    @Test
    public void changeSheetChangeSheet01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS).toString(), SheetHelper.createChangeSheetOp(2, "S2", SheetHelper.ATTRS).toString());
    }

    @Test
    public void changeSheetChangeSheet02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_I1).toString(), SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_I2).toString());
    }

    @Test
    public void changeSheetChangeSheet03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_O1).toString(), SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_O2).toString(),
                            SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_R1).toString(), SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_R2).toString(), null);
    }

    @Test
    public void changeSheetChangeSheet04() throws JSONException {
        SheetHelper.runTest(SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS_I1).toString(), SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS_I2).toString(),
                            SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_I1).toString(), SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_I2).toString(), null);
    }

    @Test
    public void changeSheetChangeSheet05() throws JSONException {
        SheetHelper.runTest(SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS_I1).toString(), SheetHelper.createChangeSheetOp(1, "S2", SheetHelper.ATTRS_I2).toString(),
                            SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS_I1).toString(), SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_I2).toString(), null);
    }

    @Test
    public void changeSheetChangeSheet06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS_I1).toString(), SheetHelper.createChangeSheetOp(1, null, SheetHelper.ATTRS_I2).toString());
    }

    @Test
    public void changeSheetChangeSheet07() throws JSONException {
        SheetHelper.runTest(SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS).toString(), SheetHelper.createChangeSheetOp(1, "S2", SheetHelper.ATTRS2).toString(), SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS).toString(), "[]", null);
    }

    @Test
    public void changeSheetChangeSheet08() throws JSONException {
        SheetHelper.runTest(SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS).toString(), SheetHelper.createChangeSheetOp(1, "S2", SheetHelper.ATTRS).toString(), SheetHelper.createChangeSheetOp(1, "S1", null).toString(), "[]", null);
    }

    @Test
    public void changeSheetChangeSheet09() throws JSONException {
        SheetHelper.runTest(SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS).toString(), SheetHelper.createChangeSheetOp(1, "S1", SheetHelper.ATTRS).toString(), "[]", "[]", null);
    }

    @Test
    public void changeSheetChangeSheet10() throws JSONException {
        SheetHelper.expectError(SheetHelper.createChangeSheetOp(1, "S1", null).toString(), SheetHelper.createChangeSheetOp(2, "S1", null).toString());
    }

/*
 *  INFO: Warning are client specific. We don't support it on the server side.
 *
    describe('"changeSheet" and drawing anchor', function () {
        var SHEET_ATTRS = COL_ATTRS.concat(ROW_ATTRS);
        it('should log warnings for absolute anchor', function () {
            var count = 4 * SHEET_ATTRS.length;
            testRunner.expectBidiWarnings(opSeries2(changeSheet, 1, SHEET_ATTRS), drawingAnchorOps(1, '2 A1 0 0 C3 0 0'), count);
        });
        it('should not log warnings without changed column/row size', function () {
            testRunner.expectBidiWarnings(changeSheet(1, ATTRS), drawingAnchorOps(1, '2 A1 0 0 C3 0 0'), 0);
        });
        it('should not log warnings without anchor attribute', function () {
            testRunner.expectBidiWarnings(opSeries2(changeSheet, 1, SHEET_ATTRS), drawingNoAnchorOps(1), 0);
        });
    });
*/
}
