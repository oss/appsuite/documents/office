/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DeleteChangeComment {

    /*
     * describe('"deleteComment" and "changeComment"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runBidiTest(deleteComment(1, 'A1', 0), changeComment(2, 'A1', 0));
                testRunner.runBidiTest(deleteComment(1, 'A1', 0), changeComment(2, 'B2', 0));
            });
            it('should shift comment index', function () {
                testRunner.runBidiTest(deleteComment(1, 'A1', 1), opSeries2(changeComment, 1, 'A1', [0, 5, 1, 6, 2, 7, 3]), null, opSeries2(changeComment, 1, 'A1', [0, 4, 5, 1, 6, 2]));
                testRunner.runBidiTest(deleteComment(1, 'A1', 2), opSeries2(changeComment, 1, 'A1', [0, 5, 1, 6, 2, 7, 3]), null, opSeries2(changeComment, 1, 'A1', [0, 4, 1, 5, 6, 2]));
                testRunner.runBidiTest(deleteComment(1, 'A1', 3), opSeries2(changeComment, 1, 'A1', [0, 5, 1, 6, 2, 7, 3]), null, opSeries2(changeComment, 1, 'A1', [0, 4, 1, 5, 2, 6]));
            });
            it('should handle deleted threads', function () {
                testRunner.runBidiTest(deleteComment(1, 'A1', 0), opSeries2(changeComment, 1, 'A1', [0, 5, 1, 6, 2, 7, 3]), null, []);
            });
        });
    */

    @Test
    public void deleteCommentChangeComment01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  deleteComment(1, 'A1', 0),
        //  changeComment(2, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            SheetHelper.createChangeCommentOp(2, "A1", 0, null).toString()
        );
    }

    @Test
    public void deleteCommentChangeComment02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  deleteComment(1, 'A1', 0),
        //  changeComment(2, 'B2', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            SheetHelper.createChangeCommentOp(2, "B2", 0, null).toString()
        );
    }

    @Test
    public void deleteCommentChangeComment03() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runBidiTest(
        //  deleteComment(1, 'A1', 1),
        //  opSeries2(changeComment, 1, 'A1', [0, 5, 1, 6, 2, 7, 3]),
        //  null,
        //  opSeries2(changeComment, 1, 'A1', [0, 4, 5, 1, 6, 2]));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 5, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 6, null),
                Helper.createChangeCommentOp(1, "A1", 2, null),
                Helper.createChangeCommentOp(1, "A1", 7, null),
                Helper.createChangeCommentOp(1, "A1", 3, null)
            ),
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 4, null),
                Helper.createChangeCommentOp(1, "A1", 5, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 6, null),
                Helper.createChangeCommentOp(1, "A1", 2, null)
            )
        );
    }

    @Test
    public void deleteCommentChangeComment04() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runBidiTest(
        //  deleteComment(1, 'A1', 2),
        //  opSeries2(changeComment, 1, 'A1', [0, 5, 1, 6, 2, 7, 3]),
        //  null,
        //  opSeries2(changeComment, 1, 'A1', [0, 4, 1, 5, 6, 2]));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 2).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 5, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 6, null),
                Helper.createChangeCommentOp(1, "A1", 2, null),
                Helper.createChangeCommentOp(1, "A1", 7, null),
                Helper.createChangeCommentOp(1, "A1", 3, null)
            ),
            SheetHelper.createDeleteCommentOp(1, "A1", 2).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 4, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 5, null),
                Helper.createChangeCommentOp(1, "A1", 6, null),
                Helper.createChangeCommentOp(1, "A1", 2, null)
            )
        );
    }

    @Test
    public void deleteCommentChangeComment05() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runBidiTest(
        //  deleteComment(1, 'A1', 3),
        //  opSeries2(changeComment, 1, 'A1', [0, 5, 1, 6, 2, 7, 3]),
        //  null,
        //  opSeries2(changeComment, 1, 'A1', [0, 4, 1, 5, 2, 6]));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 3).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 5, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 6, null),
                Helper.createChangeCommentOp(1, "A1", 2, null),
                Helper.createChangeCommentOp(1, "A1", 7, null),
                Helper.createChangeCommentOp(1, "A1", 3, null)
            ),
            SheetHelper.createDeleteCommentOp(1, "A1", 3).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 4, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 5, null),
                Helper.createChangeCommentOp(1, "A1", 2, null),
                Helper.createChangeCommentOp(1, "A1", 6, null)
            )
        );
    }

    @Test
    public void deleteCommentChangeComment06() throws JSONException {
        // Result: Should handle deleted threads.
        // testRunner.runBidiTest(
        //  deleteComment(1, 'A1', 0),
        //  opSeries2(changeComment, 1, 'A1', [0, 5, 1, 6, 2, 7, 3]),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 5, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 6, null),
                Helper.createChangeCommentOp(1, "A1", 2, null),
                Helper.createChangeCommentOp(1, "A1", 7, null),
                Helper.createChangeCommentOp(1, "A1", 3, null)
            ),
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            "[]"
        );
    }
}
