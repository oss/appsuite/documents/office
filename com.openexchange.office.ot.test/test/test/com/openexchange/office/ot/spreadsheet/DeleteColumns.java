/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DeleteColumns {


/*
    describe('"deleteColumns" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), [
                GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, rowOps(1), nameOps(1),
                deleteTable(1, 'T'), deleteDVRule(1, 0), deleteCFRule(1, 'R1'),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/
    @Test
    public void insertColumns01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.AUTOSTYLE_OPS,
                SheetHelper.createRowOps("1", null),
                SheetHelper.createNameOps("1"),
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

/*
    describe('"deleteColumns" and "deleteColumns"', function () {
        it('should transform single intervals #1', function () {
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'A'), deleteCols(1, 'B:C'), deleteCols(1, 'A'));
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'B'), deleteCols(1, 'B:C'), deleteCols(1, 'B'));
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'C'), deleteCols(1, 'C'),   []);
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'D'), deleteCols(1, 'C'),   []);
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'E'), deleteCols(1, 'C:D'), deleteCols(1, 'C'));
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'F'), deleteCols(1, 'C:D'), deleteCols(1, 'D'));
        });
        it('should transform single intervals #2', function () {
            testRunner.runTest(deleteCols(1, 'D'), deleteCols(1, 'A:B'), deleteCols(1, 'B'), deleteCols(1, 'A:B'));
            testRunner.runTest(deleteCols(1, 'D'), deleteCols(1, 'B:C'), deleteCols(1, 'B'), deleteCols(1, 'B:C'));
            testRunner.runTest(deleteCols(1, 'D'), deleteCols(1, 'C:D'), [],                 deleteCols(1, 'C'));
            testRunner.runTest(deleteCols(1, 'D'), deleteCols(1, 'D:E'), [],                 deleteCols(1, 'D'));
            testRunner.runTest(deleteCols(1, 'D'), deleteCols(1, 'E:F'), deleteCols(1, 'D'), deleteCols(1, 'D:E'));
            testRunner.runTest(deleteCols(1, 'D'), deleteCols(1, 'F:G'), deleteCols(1, 'D'), deleteCols(1, 'E:F'));
        });
        it('should transform single intervals #3', function () {
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'A:B'), deleteCols(1, 'A:B'), deleteCols(1, 'A:B'));
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'B:C'), deleteCols(1, 'B'),   deleteCols(1, 'B'));
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'C:D'), [],                   []);
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'D:E'), deleteCols(1, 'C'),   deleteCols(1, 'C'));
            testRunner.runTest(deleteCols(1, 'C:D'), deleteCols(1, 'E:F'), deleteCols(1, 'C:D'), deleteCols(1, 'C:D'));
        });
        it('should transform interval lists', function () {
            testRunner.runTest(deleteCols(1, 'D G'), deleteCols(1, 'A C'), deleteCols(1, 'B E'), deleteCols(1, 'A C'));
            testRunner.runTest(deleteCols(1, 'D G'), deleteCols(1, 'B D'), deleteCols(1, 'E'),   deleteCols(1, 'B'));
            testRunner.runTest(deleteCols(1, 'D G'), deleteCols(1, 'C E'), deleteCols(1, 'C E'), deleteCols(1, 'C:D'));
            testRunner.runTest(deleteCols(1, 'D G'), deleteCols(1, 'D F'), deleteCols(1, 'E'),   deleteCols(1, 'E'));
            testRunner.runTest(deleteCols(1, 'D G'), deleteCols(1, 'E G'), deleteCols(1, 'D'),   deleteCols(1, 'D'));
            testRunner.runTest(deleteCols(1, 'D G'), deleteCols(1, 'F H'), deleteCols(1, 'D F'), deleteCols(1, 'E:F'));
            testRunner.runTest(deleteCols(1, 'D G'), deleteCols(1, 'G I'), deleteCols(1, 'D'),   deleteCols(1, 'G'));
            testRunner.runTest(deleteCols(1, 'D G'), deleteCols(1, 'H J'), deleteCols(1, 'D G'), deleteCols(1, 'F H'));
        });
        it('should not transform intervals in different sheets', function () {
            testRunner.runTest(deleteCols(1, 'C'), deleteCols(2, 'D'));
        });
    });
*/

    // it('should transform single intervals #1', function () {
    @Test
    public void insertDeleteColumnsDeleteColumns01a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "A").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString(), SheetHelper.createDeleteColsOp(1, "A").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns02a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "B").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString(), SheetHelper.createDeleteColsOp(1, "B").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns03a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDeleteColsOp(1, "C").toString(), "[]");
    }

    @Test
    public void insertDeleteColumnsDeleteColumns04a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "C").toString(), "[]");
    }

    @Test
    public void insertDeleteColumnsDeleteColumns05a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "C").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns06a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "F").toString(), SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "D").toString());
    }

    // it('should transform single intervals #2', function () {
    @Test
    public void insertDeleteColumnsDeleteColumns01b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "A:B").toString(), SheetHelper.createDeleteColsOp(1, "B").toString(), SheetHelper.createDeleteColsOp(1, "A:B").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns02b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString(), SheetHelper.createDeleteColsOp(1, "B").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns03b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "C:D").toString(), "[]", SheetHelper.createDeleteColsOp(1, "C").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns04b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "D:E").toString(), "[]", SheetHelper.createDeleteColsOp(1, "D").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns05b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "E:F").toString(), SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "D:E").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns06b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "F:G").toString(), SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "E:F").toString());
    }

    // it('should transform single intervals #3', function () {
    @Test
    public void insertDeleteColumnsDeleteColumns01c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "A:B").toString(), SheetHelper.createDeleteColsOp(1, "A:B").toString(), SheetHelper.createDeleteColsOp(1, "A:B").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns02c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString(), SheetHelper.createDeleteColsOp(1, "B").toString(), SheetHelper.createDeleteColsOp(1, "B").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns03c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "C:D").toString(), "[]", "[]");
    }

    @Test
    public void insertDeleteColumnsDeleteColumns04c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "D:E").toString(), SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDeleteColsOp(1, "C").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns05c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "E:F").toString(), SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "C:D").toString());
    }

    // it('should transform interval lists', function () {
    @Test
    public void insertDeleteColumnsDeleteColumns01d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "A C").toString(), SheetHelper.createDeleteColsOp(1, "B E").toString(), SheetHelper.createDeleteColsOp(1, "A C").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns02d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "B D").toString(), SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createDeleteColsOp(1, "B").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns03d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "C E").toString(), SheetHelper.createDeleteColsOp(1, "C E").toString(), SheetHelper.createDeleteColsOp(1, "C:D").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns04d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "D F").toString(), SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createDeleteColsOp(1, "E").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns05d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "E G").toString(), SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "D").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns06d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "F H").toString(), SheetHelper.createDeleteColsOp(1, "D F").toString(), SheetHelper.createDeleteColsOp(1, "E:F").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns07d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "G I").toString(), SheetHelper.createDeleteColsOp(1, "D").toString(), SheetHelper.createDeleteColsOp(1, "G").toString());
    }

    @Test
    public void insertDeleteColumnsDeleteColumns08d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "H J").toString(), SheetHelper.createDeleteColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "F H").toString());
    }

    // it('should not transform intervals in different sheets', function () {
    @Test
    public void insertDeleteColumnsDeleteColumns01e() throws JSONException {
        SheetHelper.runTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDeleteColsOp(2, "D").toString());
    }

/*
    describe('"deleteColumns" and "changeColumns"', function () {
        it('should transform intervals', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), opSeries2(changeCols, 1, ['A B C D E', 'A:B B:C C:D D:E', 'C']),
                deleteCols(1, 'C'), opSeries2(changeCols, 1, ['A B C D',   'A:B B C C:D'])
            );
            testRunner.runBidiTest(
                deleteCols(1, 'C:D F:G'), changeCols(1, 'A:C D:F G:I'),
                deleteCols(1, 'C:D F:G'), changeCols(1, 'A:B C D:E')
            );
        });
        it('should transform matrix formula range', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C:D'), changeCells(1, { A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' }, E5: { mr: 'E5:F6' } }),
                null,                 changeCells(1, { A1: { mr: 'A1:B2' }, C5: { mr: 'C5:D6' } })
            );
        });
        it('should fail to shrink a matrix formula range', function () {
            testRunner.expectBidiError(deleteCols(1, 'C'), changeCells(1, { A1: { mr: 'A1:D4' } }));
        });
        it('should fail to change a shared formula', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), changeCells(1, { A1: { si: null } }));
            testRunner.runBidiTest(deleteCols(1, 'C'), changeCells(1, { A1: { sr: null } }));
            testRunner.expectBidiError(deleteCols(1, 'C'), changeCells(1, { A1: { si: 1 } }));
            testRunner.expectBidiError(deleteCols(1, 'C'), changeCells(1, { A1: { sr: 'A1:D4' } }));
        });
        it('should not transform intervals in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), changeCols(2, 'A:D', 'a1'));
        });
    });
*/

    // it('should transform intervals', function () {
    @Test
    public void deleteColumnsChangeColumns01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "A B C D E"),
                SheetHelper.createChangeColsOp(1, "A:B B:C C:D D:E"),
                SheetHelper.createChangeColsOp(1, "C")),
            SheetHelper.createDeleteColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "A B C D"),
                SheetHelper.createChangeColsOp(1, "A:B B C C:D")
            )
        );
    }

    @Test
    public void deleteColumnsChangeColumns02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C:D F:G").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "A:B D:F G:I")),
            SheetHelper.createDeleteColsOp(1, "C:D F:G").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "A:B C D:E")
            )
        );
   }

    // it('should transform matrix formula range', function () {
    @Test
    public void deleteColumnsChangeCols01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' }, E5: { mr: 'E5:F6' } }")).toString(),
                                SheetHelper.createDeleteColsOp(1, "C:D").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { mr: 'A1:B2' }, C5: { mr: 'C5:D6' } }")).toString());
    }

    // it('should fail to shrink a matrix formula range', function () {
    @Test
    public void deleteColumnsChangeCells01() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { mr: 'A1:D4' } }")).toString());
    }

    // it('should fail to change a shared formula', function () {
    @Test
    public void deleteColumnsChangeCells02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { si: null } }")).toString());
    }

    @Test
    public void deleteColumnsChangeCells03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { sr: null } }")).toString());
    }

    @Test
    public void deleteColumnsChangeCells04() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { si: 1 } }")).toString());
    }

    @Test
    public void deleteColumnsChangeCells05() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { sr: 'A1:D4' } }")).toString());
    }


    // it('should not transform intervals in different sheets', function () {
    @Test
    public void deleteColumnsChangeCols02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeColsOp(2, "A:D", "a1", null).toString());
    }

/*
    describe('"deleteColumns" and "changeCells"', function () {
        it('should transform ranges', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), changeCellsOps(1, 'A1 B2 C3 D4 E5', 'A1:B2 B2:C3 C3:D4 D4:E5', 'C3'),
                deleteCols(1, 'C'), changeCellsOps(1, 'A1 B2 C4 D5',    'A1:B2 B2:B3 C3:C4 C4:D5')
            );
            testRunner.runBidiTest(
                deleteCols(1, 'C:D F:G'), changeCellsOps(1, 'A1:C3 D4:F6 G7:I9'),
                deleteCols(1, 'C:D F:G'), changeCellsOps(1, 'A1:B3 C4:C6 D7:E9')
            );
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), changeCellsOps(2, 'A1:D4'));
        });
    });
*/
    // it('should transform ranges', function () {
    @Test
    public void deleteColumnsChangeCells01a() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCellsOps(1, "A1 B2 C3 D4 E5", "A1:B2 B2:C3 C3:D4 D4:E5", "C3"),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCellsOps(1, "A1 B2 C4 D5", "A1:B2 B2:B3 C3:C4 C4:D5"));
    }

    @Test
    public void deleteColumnsChangeCells02a() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C:D F:G").toString(), SheetHelper.createChangeCellsOps(1, "A1:C3 D4:F6 G7:I9"),
                                SheetHelper.createDeleteColsOp(1, "C:D F:G").toString(), SheetHelper.createChangeCellsOps(1, "A1:B3 C4:C6 D7:E9"));
    }
    // it('should not transform ranges in different sheets', function () {
    @Test
    public void deleteColumnsChangeCells03a() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCellsOps(2, "A1:D4"));
    }

/*
    describe('"deleteColumns" and "mergeCells"', function () {
        it('should resize ranges', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), opSeries2(mergeCells, 1, 'A2:B3 B4:D5 D6:E7', MM_ALL),
                deleteCols(1, 'C'), opSeries2(mergeCells, 1, 'A2:B3 B4:C5 C6:D7', MM_ALL)
            );
        });
        it('should delete single ranges', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), mergeCells(1, 'A1:C2 A3:C3 B4:C5 B6:C6', MM_MERGE),
                deleteCols(1, 'C'), mergeCells(1, 'A1:B2 A3:B3 B4:B5',       MM_MERGE) // deletes single cell
            );
            testRunner.runBidiTest(
                deleteCols(1, 'C'), mergeCells(1, 'A1:C2 A3:C3 B4:C5 B6:C6', MM_VERTICAL),
                deleteCols(1, 'C'), mergeCells(1, 'A1:B2 B4:B5',             MM_VERTICAL) // deletes row vector
            );
            testRunner.runBidiTest(
                deleteCols(1, 'C'), mergeCells(1, 'A1:C2 A3:C3 B4:C5 B6:C6', MM_HORIZONTAL),
                deleteCols(1, 'C'), mergeCells(1, 'A1:B2 A3:B3',             MM_HORIZONTAL) // deletes column vector
            );
            testRunner.runBidiTest(
                deleteCols(1, 'C'), mergeCells(1, 'A1:C2 A3:C3 B4:C5 B6:C6', MM_UNMERGE),
                deleteCols(1, 'C'), mergeCells(1, 'A1:B2 A3:B3 B4:B5 B6',    MM_UNMERGE) // keeps all
            );
        });
        it('should remove no-op operations', function () {
            testRunner.runBidiTest(deleteCols(1, 'C:D'), opSeries2(mergeCells, 1, 'C3:D4', MM_ALL), null, []);
        });
        it('should not modify entire row ranges', function () {
            testRunner.runBidiTest(deleteCols(1, 'C:D'), opSeries2(mergeCells, 1, 'A1:XFD2', MM_ALL));
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), mergeCells(2, 'A1:D4'));
        });
    });
*/
    // it('should resize ranges', function () {
    @Test
    public void deleteColumnsMergeCells01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:D5 D6:E7", "merge"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:D5 D6:E7", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:D5 D6:E7", "vertical"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:D5 D6:E7", "unmerge")),
            SheetHelper.createDeleteColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:C5 C6:D7", "merge"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:C5 C6:D7", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:C5 C6:D7", "vertical"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:C5 C6:D7", "unmerge")
            )
        );
    }

    // it('should delete single ranges', function () {
    @Test
    public void deleteColumnsMergeCells02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A1:C2 A3:C3 B4:C5 B6:C6", "merge").toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A1:B2 A3:B3 B4:B5", "merge").toString());
    }

    @Test
    public void deleteColumnsMergeCells03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A1:C2 A3:C3 B4:C5 B6:C6", "vertical").toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A1:B2 B4:B5", "vertical").toString());
    }

    @Test
    public void deleteColumnsMergeCells04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A1:C2 A3:C3 B4:C5 B6:C6", "horizontal").toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A1:B2 A3:B3", "horizontal").toString());
    }

    @Test
    public void deleteColumnsMergeCells05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A1:C2 A3:C3 B4:C5 B6:C6", "unmerge").toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A1:B2 A3:B3 B4:B5 B6", "unmerge").toString());
    }

    // it('should remove no-op operations', function () {
    @Test
    public void deleteColumnsMergeCells06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "C3:D4", "merge"),
                SheetHelper.createMergeCellsOp(1, "C3:D4", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "C3:D4", "vertical"),
                SheetHelper.createMergeCellsOp(1, "C3:D4", "unmerge")),
                SheetHelper.createDeleteColsOp(1, "C:D").toString(), "[]"
        );
    }

    // it('should not modify entire row ranges', function () {
    @Test
    public void deleteColumnsMergeCells07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C:D").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A1:XFD2", "merge"),
                SheetHelper.createMergeCellsOp(1, "A1:XFD2", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "A1:XFD2", "vertical"),
                SheetHelper.createMergeCellsOp(1, "A1:XFD2", "unmerge")));
    }

    // it('should not transform ranges in different sheets', function () {
    @Test
    public void deleteColumnsMergeCells08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(2, "A1:D4", "merge"),
                SheetHelper.createMergeCellsOp(2, "A1:D4", "horizontal"),
                SheetHelper.createMergeCellsOp(2, "A1:D4", "vertical"),
                SheetHelper.createMergeCellsOp(2, "A1:D4", "unmerge")));
    }

/*
    describe('"deleteColumns" and hyperlinks (RESIZE mode)', function () {
        it('should transform ranges', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), hlinkOps(1, 'A1 B2 C3 D4 E5', 'A1:B2 B2:C3 C3:D4 D4:E5', 'C3'),
                deleteCols(1, 'C'), hlinkOps(1, 'A1 B2 C4 D5',    'A1:B2 B2:B3 C3:C4 C4:D5')
            );
            testRunner.runBidiTest(
                deleteCols(1, 'C:D F:G'), hlinkOps(1, 'A1:C3 D4:F6 G7:I9'),
                deleteCols(1, 'C:D F:G'), hlinkOps(1, 'A1:B3 C4:C6 D7:E9')
            );
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), hlinkOps(2, 'A1:D4'));
        });
    });
*/

    // it('should transform ranges', function () {
    @Test
    public void deleteColumnsHlinkOps01() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "C").toString(),
            SheetHelper.createHlinkOps("1", "A1 B2 C3 D4 E5", "A1:B2 B2:C3 C3:D4 D4:E5", "C3").toString(),
            SheetHelper.createDeleteColsOp(1, "C").toString(),
            SheetHelper.createHlinkOps("1", "A1 B2 C4 D5", "A1:B2 B2:B3 C3:C4 C4:D5").toString());
    }

    @Test
    public void deleteColumnsHlinkOps02() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "C:D F:G").toString(),
            SheetHelper.createHlinkOps("1", "A1:C3 D4:F6 G7:I9").toString(),
            SheetHelper.createDeleteColsOp(1, "C:D F:G").toString(),
            SheetHelper.createHlinkOps("1", "A1:B3 C4:C6 D7:E9").toString());
    }

    // it('should not transform ranges in different sheets', function () {
    @Test
    public void deleteColumnsHlinkOps03() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "C").toString(),
            SheetHelper.createHlinkOps("2", "A1:D4").toString());
    }

/*
    describe('"deleteColumns" and "insertTable"', function () {
        it('should shift table range', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'E'), opSeries2(insertTable, 1, 'T', ['A1:D4', 'G1:J4']),
                deleteCols(1, 'E'), opSeries2(insertTable, 1, 'T', ['A1:D4', 'F1:I4'])
            );
        });
        it('should fail to shrink table range', function () {
            testRunner.expectBidiError(deleteCols(1, 'C'), insertTable(1, 'T', 'A1:D4'));
        });
        it('should delete table range explicitly', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'B:G'), insertTable(1, 'T', 'C1:F4'),
                [deleteTable(1, 'T'), deleteCols(1, 'B:G')], []
            );
        });
        it('should not transform tables in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), insertTable(2, 'T', 'A1:D4'));
        });
    });
*/

    // it('should shift table range', function () {
    @Test
    public void deleteColumnsInsertTable01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1:D4", null),
                SheetHelper.createInsertTableOp(1, "T", "G1:J4", null)),
            SheetHelper.createDeleteColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1:D4", null),
                SheetHelper.createInsertTableOp(1, "T", "F1:I4", null))
        );
    }

    // it('should fail to shrink table range', function () {
    @Test
    public void deleteColumnsInsertTable02() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createInsertTableOp(1, "T", "A1:D4", null).toString());
    }

    // it('should delete table range explicitly', function () {
    @Test
    public void deleteColumnsInsertTable03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "B:G").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "C1:F4", null)),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createDeleteColsOp(1, "B:G")),
            "[]"
        );
    }

    // it('should not transform tables in different sheets', function () {
    @Test
    public void deleteColumnsInsertTable04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createInsertTableOp(2, "T", "A1:D4", null).toString());
    }

/*
    describe('"deleteColumns" and "changeTable"', function () {
        it('should shift and shrink table range', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'E'), opSeries2(changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'G1:J4']),
                deleteCols(1, 'E'), opSeries2(changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'F1:I4'])
            );
        });
        it('should fail to shrink table range', function () {
            testRunner.expectBidiError(deleteCols(1, 'C'), changeTable(1, 'T', 'A1:D4'));
        });
        it('should delete table range explicitly', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'B:G'), changeTable(1, 'T', 'C1:F4'),
                [deleteTable(1, 'T'), deleteCols(1, 'B:G')], []
            );
        });
        it('should not transform tables in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), changeTable(2, 'T', 'A1:D4'));
        });
    });
*/

    // it('should shift and shrink table range', function () {
    @Test
    public void deleteColumnsChangeTable01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeTableOp(1, "T", "A1:D4", "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeTableOp(1, "T", "G1:J4", "{ attrs: " + SheetHelper.ATTRS + " }")),
            SheetHelper.createDeleteColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeTableOp(1, "T", "A1:D4", "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeTableOp(1, "T", "F1:I4", "{ attrs: " + SheetHelper.ATTRS + " }"))
        );
    }

    // it('should fail to shrink table range', function () {
    @Test
    public void deleteColumnsChangeTable02() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeTableOp(1, "T", "A1:D4", null).toString());
    }

    // it('should delete table range explicitly', function () {
    @Test
    public void deleteColumnsChangeTable03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "B:G").toString(),
            SheetHelper.createChangeTableOp(1, "T", "C1:F4", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createDeleteColsOp(1, "B:G")), "[]"
        );
    }

    // it('should not transform tables in different sheets', function () {
    @Test
    public void deleteColumnsChangeTable04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeTableOp(2, "T", "A1:D4", null).toString());
    }

/*
    describe('"deleteColumns" and "changeTableColumn"', function () {
        it('should fail in same sheet', function () {
            testRunner.expectBidiError(deleteCols(1, 'C'), changeTableCol(1, 'T', 0, { attrs: ATTRS }));
        });
        it('should skip different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), changeTableCol(2, 'T', 0, { attrs: ATTRS }));
        });
    });
*/
    // it('should fail in same sheet', function () {
    @Test
    public void deleteColumnsChangeTableColumn01() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeTableColOp(1, "T", 0, "{ attrs: " + SheetHelper.ATTRS + " }").toString());
    }

    // it('should skip different sheets', function () {
    @Test
    public void deleteColumnsChangeTableColumn02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeTableColOp(2, "T", 0, "{ attrs: " + SheetHelper.ATTRS + " }").toString());
    }


/*
    describe('"deleteColumns" and tables', function () {
        it('should handle insert/change sequence', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'B:G'), [insertTable(1, 'T', 'C1:F4'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS })],
                [deleteTable(1, 'T'), deleteCols(1, 'B:G')], []
            );
        });
        it('should handle insert/change/delete sequence', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'B:G'), [insertTable(1, 'T', 'C1:F4'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS }), deleteTable(1, 'T')],
                null, []
            );
        });
        it('should handle insert/change sequences on both sides', function () {
            testRunner.runTest(
                [insertTable(1, 'T1', 'A1:D4'), deleteCols(2, 'A:D'), changeTable(1, 'T1', { attrs: ATTRS })],
                [insertTable(2, 'T2', 'A1:D4'), deleteCols(1, 'A:D'), changeTable(2, 'T2', { attrs: ATTRS })],
                [deleteTable(2, 'T2'), deleteCols(2, 'A:D')],
                [deleteTable(1, 'T1'), deleteCols(1, 'A:D')]
            );
            testRunner.runTest(
                [insertTable(1, 'T1', 'E1:H4'), deleteCols(1, 'A:D'), changeTable(1, 'T1', { attrs: ATTRS })],
                [insertTable(1, 'T2', 'A1:D4'), deleteCols(1, 'E:H'), changeTable(1, 'T2', { attrs: ATTRS })],
                [deleteTable(1, 'T2'), deleteCols(1, 'A:D')],
                [deleteTable(1, 'T1'), deleteCols(1, 'A:D')]
            );
        });
        it('should handle insert/change/delete sequences on both sides', function () {
            testRunner.runTest(
                [insertTable(1, 'T1', 'A1:D4'), deleteCols(2, 'A:D'), changeTable(1, 'T1', { attrs: ATTRS }), deleteTable(1, 'T1')],
                [insertTable(2, 'T2', 'A1:D4'), deleteCols(1, 'A:D'), changeTable(2, 'T2', { attrs: ATTRS }), deleteTable(2, 'T2')],
                deleteCols(2, 'A:D'),
                deleteCols(1, 'A:D')
            );
            testRunner.runTest(
                [insertTable(1, 'T1', 'E1:H4'), deleteCols(1, 'A:D'), changeTable(1, 'T1', { attrs: ATTRS }), deleteTable(1, 'T1')],
                [insertTable(1, 'T2', 'A1:D4'), deleteCols(1, 'E:H'), changeTable(1, 'T2', { attrs: ATTRS }), deleteTable(1, 'T2')],
                deleteCols(1, 'A:D'),
                deleteCols(1, 'A:D')
            );
        });
    });
*/
    // it('should handle insert/change sequence', function () {
    @Test
    public void deleteColumnsChangeTable01a() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "B:G").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "C1:F4", null),
                SheetHelper.createChangeTableOp(1, "T", null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeTableColOp(1, "T", 0, "{ attrs: " + SheetHelper.ATTRS + " }")
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createDeleteColsOp(1, "B:G")
            ), "[]"
        );
    }

    // it('should handle insert/change/delete sequence', function () {
    @Test
    public void deleteColumnsChangeTable02a() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "B:G").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "C1:F4", null),
                SheetHelper.createChangeTableOp(1, "T", null, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createChangeTableColOp(1, "T", 0, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createDeleteTableOp(1, "T")
            ),
            SheetHelper.createDeleteColsOp(1, "B:G").toString(), "[]"
        );
    }

/*
    describe('"deleteColumns" and "insertDVRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'E'), opSeries2(insertDVRule, 1, 0, ['A1:D4', 'C1:F4', 'F1:I4']),
                deleteCols(1, 'E'), opSeries2(insertDVRule, 1, 0, ['A1:D4', 'C1:E4', 'E1:H4'])
            );
        });
        it('should delete rule explicitly', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'A:I'), insertDVRule(1, 0, 'C1:F4'),
                [deleteDVRule(1, 0), deleteCols(1, 'A:I')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), insertDVRule(2, 0, 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteColumnInsertDVRule01() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runBidiTest(
        //  deleteCols(1, 'E'),
        //  opSeries2(insertDVRule, 1, 0, ['A1:D4', 'C1:F4', 'F1:I4']),
        //  deleteCols(1, 'E'),
        //  opSeries2(insertDVRule, 1, 0, ['A1:D4', 'C1:E4', 'E1:H4']));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "C1:F4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "F1:I4", null)
            ),
            SheetHelper.createDeleteColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "C1:E4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "E1:H4", null)
            )
        );
    }

    @Test
    public void deleteColumnInsertDVRule02() throws JSONException {
        // Result: Should delete rule explicitly.
        // testRunner.runBidiTest(
        //  deleteCols(1, 'A:I'),
        //  insertDVRule(1, 0, 'C1:F4'),
        //  [deleteDVRule(1, 0), deleteCols(1, 'A:I')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "A:I").toString(),
            SheetHelper.createInsertDVRuleOp(1, 0, "C1:F4", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createDeleteColsOp(1, "A:I")
            ),
            "[]"
        );
    }

    @Test
    public void deleteColumnInsertDVRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  deleteCols(1, 'C'),
        //  insertDVRule(2, 0, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "C").toString(),
            SheetHelper.createInsertDVRuleOp(2, 0, "A1:D4", null).toString()
        );
    }

/*
    describe('"deleteColumns" and "changeDVRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'E'), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'C1:F4', 'F1:I4']),
                deleteCols(1, 'E'), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'C1:E4', 'E1:H4'])
            );
        });
        it('should delete rule explicitly', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'A:I'), changeDVRule(1, 0, 'C1:F4'),
                [deleteDVRule(1, 0), deleteCols(1, 'A:I')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            -testRunner.runBidiTest(deleteCols(1, 'C'), changeDVRule(2, 0, 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteColumnChangeDVRule01() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runBidiTest(
        //  deleteCols(1, 'E'),
        //  opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'C1:F4', 'F1:I4']),
        //  deleteCols(1, 'E'),
        //  opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'C1:E4', 'E1:H4']));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeDVRuleOp(1, 0, null, "{ attr: " + SheetHelper.ATTRS.toString() + " }"),
                SheetHelper.createChangeDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "C1:F4", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "F1:I4", null)
            ),
            SheetHelper.createDeleteColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeDVRuleOp(1, 0, null, "{ attr: " + SheetHelper.ATTRS.toString() + " }"),
                SheetHelper.createChangeDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "C1:E4", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "E1:H4", null)
            )
        );
    }

    @Test
    public void deleteColumnChangeDVRule02() throws JSONException {
        // Result: Should delete rule explicitly.
        // testRunner.runBidiTest(
        //  deleteCols(1, 'A:I'),
        //  changeDVRule(1, 0, 'C1:F4'),
        //  [deleteDVRule(1, 0), deleteCols(1, 'A:I')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "A:I").toString(),
            SheetHelper.createChangeDVRuleOp(1, 0, "C1:F4", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createDeleteColsOp(1, "A:I")
            ),
            "[]"
        );
    }

    @Test
    public void deleteColumnChangeDVRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  deleteCols(1, 'C'),
        //  changeDVRule(2, 0, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "C").toString(),
            SheetHelper.createChangeDVRuleOp(2, 0, "A1:D4", null).toString()
        );
    }

/*
    describe('"deleteColumns" and "insertCFRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'E'), opSeries2(insertCFRule, 1, 'R1', ['A1:D4', 'C1:F4', 'F1:I4']),
                deleteCols(1, 'E'), opSeries2(insertCFRule, 1, 'R1', ['A1:D4', 'C1:E4', 'E1:H4'])
            );
        });
        it('should delete rule explicitly', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'A:I'), insertCFRule(1, 'R1', 'C1:F4'),
                [deleteCFRule(1, 'R1'), deleteCols(1, 'A:I')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), insertCFRule(2, 'R1', 'A1:D4'));
        });
    });
*/

    // it('should shift rule', function () {
    @Test
    public void deleteColumnsInsertCFRule01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(), SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString());
    };

    @Test
    public void deleteColumnsInsertCFRule02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createInsertCFRuleOp(1, "R1", "C1:F4", null).toString(), SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createInsertCFRuleOp(1, "R1", "C1:E4", null).toString());
    };

    @Test
    public void deleteColumnsInsertCFRule03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createInsertCFRuleOp(1, "R1", "F1:I4", null).toString(), SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createInsertCFRuleOp(1, "R1", "E1:H4", null).toString());
    };

    // it('should delete rule explicitly', function () {
    public void deleteColumnsInsertCFRule04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "A:I").toString(), SheetHelper.createInsertCFRuleOp(1, "R1", "C1:F4", null).toString(),
        Helper.createArrayFromJSON(SheetHelper.createDeleteCFRuleOp(1, "R1"), SheetHelper.createDeleteColsOp(1, "A:I")), "[]");
    };

    // it('should not transform rules in different sheets', function () {
    @Test
    public void deleteColumnsInsertCFRule05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createInsertCFRuleOp(2, "R1", "A1:D4", null).toString());
    };


/*
    describe('"deleteColumns" and "changeCFRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'E'), opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D4', 'C1:F4', 'F1:I4']),
                deleteCols(1, 'E'), opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D4', 'C1:E4', 'E1:H4'])
            );
        });
        it('should delete rule explicitly', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'A:I'), changeCFRule(1, 'R1', 'C1:F4'),
                [deleteCFRule(1, 'R1'), deleteCols(1, 'A:I')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), changeCFRule(2, 'R1', 'A1:D4'));
        });
    });
*/

    // it('should shift rule', function () {
    @Test
    public void deleteColumnsChangeCFRule01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", "{ attrs: " + SheetHelper.ATTRS + " }").toString(), SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", "{ attrs: " + SheetHelper.ATTRS + " }").toString());
    };

    @Test
    public void deleteColumnsChangeCFRule02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createChangeCFRuleOp(1, "R1", "C1:F4", "{ attrs: " + SheetHelper.ATTRS + " }").toString(), SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createChangeCFRuleOp(1, "R1", "C1:E4", "{ attrs: " + SheetHelper.ATTRS + " }").toString());
    };

    @Test
    public void deleteColumnsChangeCFRule03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createChangeCFRuleOp(1, "R1", "F1:I4", "{ attrs: " + SheetHelper.ATTRS + " }").toString(), SheetHelper.createDeleteColsOp(1, "E").toString(), SheetHelper.createChangeCFRuleOp(1, "R1", "E1:H4", "{ attrs: " + SheetHelper.ATTRS + " }").toString());
    };

    // it('should delete rule explicitly', function () {
    public void deleteColumnsChangeCFRule04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "A:I").toString(), SheetHelper.createChangeCFRuleOp(1, "R1", "C1:F4", null).toString(),
        Helper.createArrayFromJSON(SheetHelper.createDeleteCFRuleOp(1, "R1"), SheetHelper.createDeleteColsOp(1, "A:I")), "[]");
    };

    // it('should not transform rules in different sheets', function () {
    @Test
    public void deleteColumnsChangeCFRule05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCFRuleOp(2, "R1", "A1:D4", null).toString());
    };

/*
    describe('"deleteColumns" and cell anchor', function () {
        it('should transform cell anchor', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), cellAnchorOps(1, 'A1', 'B2', 'D4', 'E5'),
                deleteCols(1, 'C'), cellAnchorOps(1, 'A1', 'B2', 'C4', 'D5')
            );
            testRunner.runBidiTest(
                deleteCols(1, 'C:D F:G'), cellAnchorOps(1, 'A1', 'B2', 'E5', 'H8', 'I9'),
                deleteCols(1, 'C:D F:G'), cellAnchorOps(1, 'A1', 'B2', 'C5', 'D8', 'E9')
            );
        });
        it('should handle deleted cell note', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), insertNote(1, 'C3', 'abc'), [deleteNote(1, 'C3'), deleteCols(1, 'C')], []);
            testRunner.runBidiTest(deleteCols(1, 'C'), deleteNote(1, 'C3'),        null,                                      []);
            testRunner.runBidiTest(deleteCols(1, 'C'), changeNote(1, 'C3', 'abc'), [deleteNote(1, 'C3'), deleteCols(1, 'C')], []);
        });
        it('should handle deleted cell comment', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), insertComment(1, 'C3', 0, { text: 'abc' }), [deleteComment(1, 'C3', 0), deleteCols(1, 'C')], []);
            testRunner.runBidiTest(deleteCols(1, 'C'), insertComment(1, 'C3', 1, { text: 'abc' }), null,                                            []);
            testRunner.runBidiTest(deleteCols(1, 'C'), deleteComment(1, 'C3', 0),                  null,                                            []);
            testRunner.runBidiTest(deleteCols(1, 'C'), deleteComment(1, 'C3', 1),                  null,                                            []);
            testRunner.runBidiTest(deleteCols(1, 'C'), changeComment(1, 'C3', 0, { text: 'abc' }), [deleteComment(1, 'C3', 0), deleteCols(1, 'C')], []);
            testRunner.runBidiTest(deleteCols(1, 'C'), changeComment(1, 'C3', 1, { text: 'abc' }), null,                                            []);
        });
        it('should not transform cell anchor in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), cellAnchorOps(2, 'D4'));
        });
    });
*/

    // it('should transform cell anchor', function () {
    @Test
    public void deleteColumnsCellAnchor01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createCellAnchorOps(1, "A1", "B2", "D4", "E5").toString(), SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createCellAnchorOps(1, "A1", "B2", "C4", "D5").toString());
    };

    @Test
    public void deleteColumnsCellAnchor02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C:D F:G").toString(), SheetHelper.createCellAnchorOps(1, "A1", "B2", "E5", "H8", "I9").toString(), SheetHelper.createDeleteColsOp(1, "C:D F:G").toString(), SheetHelper.createCellAnchorOps(1, "A1", "B2", "C5", "D8", "E9").toString());
    };

    // it('should handle deleted cell note', function () {
    @Test
    public void deleteColumnsCellAnchor03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createInsertNoteOp(1, "C3", "abc", null).toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteNoteOp(1, "C3"), SheetHelper.createDeleteColsOp(1, "C")), "[]");
    };

    @Test
    public void deleteColumnsCellAnchor04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDeleteNoteOp(1, "C3").toString(),
            SheetHelper.createDeleteColsOp(1, "C").toString(), "[]");
    };

    @Test
    public void deleteColumnsCellAnchor05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeNoteOp(1, "C3", "abc", null).toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteNoteOp(1, "C3"), SheetHelper.createDeleteColsOp(1, "C")), "[]");
    };

    // it('should handle deleted cell comment', function () {
    @Test
    public void deleteColumnsCellAnchor06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createInsertCommentOp(1, "C3", 0, "{ text: 'abc' }").toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteCommentOp(1, "C3", 0), SheetHelper.createDeleteColsOp(1, "C")), "[]");
    };

    @Test
    public void deleteColumnsCellAnchor07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createInsertCommentOp(1, "C3", 1, "{ text: 'abc' }").toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), "[]");
    };

    @Test
    public void deleteColumnsCellAnchor08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDeleteCommentOp(1, "C3", 0).toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), "[]");
    };

    @Test
    public void deleteColumnsCellAnchor09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDeleteCommentOp(1, "C3", 1).toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), "[]");
    };

    @Test
    public void deleteColumnsCellAnchor10() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCommentOp(1, "C3", 0, "{ text: 'abc' }").toString(),
                    Helper.createArrayFromJSON(SheetHelper.createDeleteCommentOp(1, "C3", 0), SheetHelper.createDeleteColsOp(1, "C")), "[]");
    };

    @Test
    public void deleteColumnsCellAnchor11() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createChangeCommentOp(1, "C3", 1, "{ text: 'abc' }").toString(),
            SheetHelper.createDeleteColsOp(1, "C").toString(), "[]");
    };

    // it('should not transform cell anchor in different sheets', function () {
    @Test
    public void deleteColumnsCellAnchor12() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createCellAnchorOps(2, "D4").toString());
    };

/*
    describe('"deleteColumns" and "moveNotes"', function () {
        it('should transform cell anchors', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), moveNotes(1, 'A1 B2 D3 E4', 'E1 D2 B3 A4'),
                deleteCols(1, 'C'), moveNotes(1, 'A1 B2 C3 D4', 'D1 C2 B3 A4')
            );
        });
        it('should handle deleted source anchor', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), moveNotes(1, 'B1 C2 C3 D4', 'E1 E2 C1 E4'),
                [deleteNote(1, 'E2'), deleteNote(1, 'C1'), deleteCols(1, 'C')], moveNotes(1, 'B1 C4', 'D1 D4')
            );
        });
        it('should handle deleted target anchor', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), moveNotes(1, 'E1 E2 E3', 'B1 C2 D3'),
                [deleteNote(1, 'C2'), deleteCols(1, 'C')], [deleteNote(1, 'D2'), moveNotes(1, 'D1 D3', 'B1 C3')]
            );
        });
        it('should not transform cell anchor in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), moveNotes(2, 'D4', 'E5'));
        });
    });
*/

    // it('should transform cell anchors', function () {
    @Test
    public void deleteColumnsMoveNotes01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMoveNotesOp(1, "A1 B2 D3 E4", "E1 D2 B3 A4").toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMoveNotesOp(1, "A1 B2 C3 D4", "D1 C2 B3 A4").toString());
    };

    // it('should handle deleted source anchor', function () {
    @Test
    public void deleteColumnsMoveNotes02() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "C").toString(),
            SheetHelper.createMoveNotesOp(1, "B1 C2 C3 D4", "E1 E2 C1 E4").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "E2"),
                SheetHelper.createDeleteNoteOp(1, "C1"),
                SheetHelper.createDeleteColsOp(1, "C")),
            SheetHelper.createMoveNotesOp(1, "B1 C4", "D1 D4").toString());
    };

    // it('should handle deleted target anchor', function () {
    @Test
    public void deleteColumnsMoveNotes03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMoveNotesOp(1, "E1 E2 E3", "B1 C2 D3").toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteNoteOp(1, "C2"), SheetHelper.createDeleteColsOp(1, "C")),
            Helper.createArrayFromJSON(SheetHelper.createDeleteNoteOp(1, "D2"), SheetHelper.createMoveNotesOp(1, "D1 D3", "B1 C3")));
    };

    // it('should not transform cell anchor in different sheets', function () {
    @Test
    public void deleteColumnsMoveNotes04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMoveNotesOp(2, "D4", "E5").toString());
    };

/*
    describe('"deleteColumns" and "moveComments"', function () {
        it('should transform cell anchors', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), moveComments(1, 'A1 B2 D3 E4', 'E1 D2 B3 A4'),
                deleteCols(1, 'C'), moveComments(1, 'A1 B2 C3 D4', 'D1 C2 B3 A4')
            );
        });
        it('should handle deleted source anchor', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), moveComments(1, 'B1 C2 C3 D4', 'E1 E2 C1 E4'),
                [deleteComment(1, 'E2', 0), deleteComment(1, 'C1', 0), deleteCols(1, 'C')], moveComments(1, 'B1 C4', 'D1 D4')
            );
        });
        it('should handle deleted target anchor', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), moveComments(1, 'E1 E2 E3', 'B1 C2 D3'),
                [deleteComment(1, 'C2', 0), deleteCols(1, 'C')], [deleteComment(1, 'D2', 0), moveComments(1, 'D1 D3', 'B1 C3')]
            );
        });
        it('should not transform cell anchor in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), moveComments(2, 'D4', 'E5'));
        });
    });
*/

    // it('should transform cell anchors', function () {
    @Test
    public void deleteColumnsMoveComments01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMoveCommentsOp(1, "A1 B2 D3 E4", "E1 D2 B3 A4").toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMoveCommentsOp(1, "A1 B2 C3 D4", "D1 C2 B3 A4").toString());
    };

    // it('should handle deleted source anchor', function () {
    @Test
    public void deleteColumnsMoveComments02() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "C").toString(),
            SheetHelper.createMoveCommentsOp(1, "B1 C2 C3 D4", "E1 E2 C1 E4").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "E2", 0),
                SheetHelper.createDeleteCommentOp(1, "C1", 0),
                SheetHelper.createDeleteColsOp(1, "C")),
            SheetHelper.createMoveCommentsOp(1, "B1 C4", "D1 D4").toString());
    };

    // it('should handle deleted target anchor', function () {
    @Test
    public void deleteColumnsMoveComments03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMoveCommentsOp(1, "E1 E2 E3", "B1 C2 D3").toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteCommentOp(1, "C2", 0), SheetHelper.createDeleteColsOp(1, "C")),
            Helper.createArrayFromJSON(SheetHelper.createDeleteCommentOp(1, "D2", 0), SheetHelper.createMoveCommentsOp(1, "D1 D3", "B1 C3")));
    };

    // it('should not transform cell anchor in different sheets', function () {
    @Test
    public void deleteColumnsMoveComments04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createMoveCommentsOp(2, "D4", "E5").toString());
    };

/*
    describe('"deleteColumns" and drawing anchor', function () {
        it('should transform drawing anchor', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 B1 10 10 C2 20 20', '2 C1 10 10 D2 20 20', '2 D1 10 10 E2 20 20'),
                deleteCols(1, 'C'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 B1 10 10 C2 0 20',  '2 C1 0 10 C2 20 20',  '2 C1 10 10 D2 20 20'),
                { warnings: true } // missing transformation of absolute drawing position
            );
            testRunner.runBidiTest(
                deleteCols(1, 'C:D F:G'), drawingAnchorOps(1, '2 A1 10 10 D2 20 20', '2 B1 10 10 E2 20 20', '2 C1 10 10 F2 20 20', '2 E1 10 10 H2 20 20'),
                deleteCols(1, 'C:D F:G'), drawingAnchorOps(1, '2 A1 10 10 C2 0 20',  '2 B1 10 10 C2 20 20', '2 C1 0 10 D2 0 20',   '2 C1 10 10 D2 20 20'),
                { warnings: true } // missing transformation of absolute drawing position
            );
        });
        var DEL_ANCHOR_O = { drawing: { anchor: '2 C3 10 10 D4 20 20' } };  // original
        var DEL_ANCHOR_T = { drawing: { anchor: '2 B3 0 10 B4 0 20' } };    // transformed
        it('should handle deleted drawing anchor of drawing objects', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'B:E'), [insertShape(1, 0, DEL_ANCHOR_O), changeDrawing(1, 2, DEL_ANCHOR_O)],
                [deleteDrawing(1, 0), deleteDrawing(1, 1), deleteCols(1, 'B:E')], []
            );
        });
        it('should handle deleted drawing anchor of notes and comments', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'B:E'), [insertNote(1, 'A1', 'abc', DEL_ANCHOR_O), changeNote(1, 'A1', DEL_ANCHOR_O), insertComment(1, 'A1', 0, { attrs: DEL_ANCHOR_O }), changeComment(1, 'A1', 0, { attrs: DEL_ANCHOR_O })],
                deleteCols(1, 'B:E'), [insertNote(1, 'A1', 'abc', DEL_ANCHOR_T), changeNote(1, 'A1', DEL_ANCHOR_T), insertComment(1, 'A1', 0, { attrs: DEL_ANCHOR_T }), changeComment(1, 'A1', 0, { attrs: DEL_ANCHOR_T })],
                { warnings: true }
            );
        });
        it('should log warnings for absolute anchor', function () {
            var anchorOps = drawingAnchorOps(1, '2 A1 0 0 C3 0 0');
            testRunner.expectBidiWarnings(deleteCols(1, 'C'), anchorOps, anchorOps.length);
        });
        it('should not log warnings without anchor attribute', function () {
            testRunner.expectBidiWarnings(deleteCols(1, 'C'), drawingNoAnchorOps(1), 0);
        });
        it('should not transform drawing anchor in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), drawingAnchorOps(2, '2 A1 10 10 D4 20 20'));
        });
    });
*/
    // it('should transform drawing anchor', function () {
    public void deleteColumnsDrawingAnchor01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDrawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 20 20", "2 C1 10 10 D2 20 20", "2 D1 10 10 E2 20 20").toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDrawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 0 20",  "2 C1 0 10 C2 20 20",  "2 C1 10 10 D2 20 20").toString());
    };

    public void deleteColumnsDrawingAnchor02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDrawingAnchorOps(1, "2 A1 10 10 D2 20 20", "2 B1 10 10 E2 20 20", "2 C1 10 10 F2 20 20", "2 E1 10 10 H2 20 20").toString(),
                                SheetHelper.createDeleteColsOp(1, "C").toString(), SheetHelper.createDrawingAnchorOps(1, "2 A1 10 10 C2 0 20",  "2 B1 10 10 C2 20 20", "2 C1 0 10 D2 0 20",   "2 C1 10 10 D2 20 20").toString());
    };

    final static String DEL_ANCHOR_O = "{ drawing: { anchor: '2 C3 10 10 D4 20 20' } }";  // original
    final static String DEL_ANCHOR_T = "{ drawing: { anchor: '2 B3 0 10 B4 0 20' } }";    // transformed

    // it('should handle deleted drawing anchor of drawing objects', function () {
    public void deleteColumnsDrawingAnchor03() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteColsOp(1, "B:E").toString(),
            Helper.createArrayFromJSON(Helper.createInsertShapeOp("1 0", new JSONObject(DEL_ANCHOR_O)), Helper.createChangeDrawingOp("1 2", new JSONObject(DEL_ANCHOR_O))),
            Helper.createArrayFromJSON(Helper.createDeleteDrawingOp("1 0"), Helper.createDeleteDrawingOp("1 1"), SheetHelper.createDeleteColsOp(1, "B:E")),
            "[]");
    };

    // it('should handle deleted drawing anchor of notes and comments', function () {
    public void deleteColumnsDrawingAnchor04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteColsOp(1, "B:E").toString(),
            Helper.createArrayFromJSON(SheetHelper.createInsertNoteOp(1, "A1", "abc", new JSONObject(DEL_ANCHOR_O)), SheetHelper.createChangeNoteOp(1, "A1", null, new JSONObject(DEL_ANCHOR_O)), SheetHelper.createInsertCommentOp(1, "A1", 0, DEL_ANCHOR_O), SheetHelper.createChangeCommentOp(1, "A1", 0, DEL_ANCHOR_O)),
            SheetHelper.createDeleteColsOp(1, "B:E").toString(),
            Helper.createArrayFromJSON(SheetHelper.createInsertNoteOp(1, "A1", "abc", new JSONObject(DEL_ANCHOR_T)), SheetHelper.createChangeNoteOp(1, "A1", null, new JSONObject(DEL_ANCHOR_T)), SheetHelper.createInsertCommentOp(1, "A1", 0, DEL_ANCHOR_T), SheetHelper.createChangeCommentOp(1, "A1", 0, DEL_ANCHOR_T)));
    }


/* Not working on server.
    describe('"deleteColumns" and "sheetSelection"', function () {
        it('should transform selected ranges', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C:D F:G'), sheetSelection(1, 'A1:C3 D4:F6 G7:I9', 0, 'A1', [0, 1]),
                deleteCols(1, 'C:D F:G'), sheetSelection(1, 'A1:B3 C4:C6 D7:E9', 0, 'A1', [0, 1])
            );
        });
        it('should transform active address', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C:D F:G'), opSeries2(sheetSelection, 1, 'A1:XFD4', 0, ['A1', 'B2', 'C3', 'D4', 'E5', 'F6', 'G7', 'H8', 'I9']),
                deleteCols(1, 'C:D F:G'), opSeries2(sheetSelection, 1, 'A1:XFD4', 0, ['A1', 'B2', 'C3', 'C4', 'C5', 'D6', 'D7', 'D8', 'E9'])
            );
        });
        it('should transform origin address', function () {
            testRunner.runBidiTest(
                deleteCols(1, 'C:D F:G'), opSeries2(sheetSelection, 1, 'A1:XFD4', 0, 'A1', ['A1', 'B2', 'C3', 'D4', 'E5', 'F6', 'G7', 'H8', 'I9']),
                deleteCols(1, 'C:D F:G'), opSeries2(sheetSelection, 1, 'A1:XFD4', 0, 'A1', ['A1', 'B2', 'C3', 'C4', 'C5', 'D6', 'D7', 'D8', 'E9'])
            );
        });
        it('should transform deleted active range', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), sheetSelection(1, 'E5 D4 C3 B2 A1', 0, 'E5', 'E5'), null, sheetSelection(1, 'D5 C4 B2 A1', 0, 'D5', 'D5'));
            testRunner.runBidiTest(deleteCols(1, 'C'), sheetSelection(1, 'E5 D4 C3 B2 A1', 1, 'D4', 'D4'), null, sheetSelection(1, 'D5 C4 B2 A1', 1, 'C4', 'C4'));
            testRunner.runBidiTest(deleteCols(1, 'C'), sheetSelection(1, 'E5 D4 C3 B2 A1', 2, 'C3', 'C3'), null, sheetSelection(1, 'D5 C4 B2 A1', 0, 'D5'));
            testRunner.runBidiTest(deleteCols(1, 'C'), sheetSelection(1, 'E5 D4 C3 B2 A1', 3, 'B2', 'B2'), null, sheetSelection(1, 'D5 C4 B2 A1', 2, 'B2', 'B2'));
            testRunner.runBidiTest(deleteCols(1, 'C'), sheetSelection(1, 'E5 D4 C3 B2 A1', 4, 'A1', 'A1'), null, sheetSelection(1, 'D5 C4 B2 A1', 3, 'A1', 'A1'));
        });
        it('should keep active cell when deleting the selection', function () {
            testRunner.runBidiTest(deleteCols(1, 'C:D'), sheetSelection(1, 'C6:D9 C1:D4', 0, 'C7'), null, sheetSelection(1, 'C7', 0, 'C7'));
            testRunner.runBidiTest(deleteCols(1, 'C:D'), sheetSelection(1, 'C6:D9 C1:D4', 1, 'D2'), null, sheetSelection(1, 'C2', 0, 'C2'));
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(deleteCols(1, 'C'), sheetSelection(2, 'A1:D4', 0, 'A1'));
        });
    });
*/

}
