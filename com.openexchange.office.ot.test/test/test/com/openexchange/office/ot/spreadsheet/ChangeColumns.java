/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeColumns {

/*
    describe('"changeColumns" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(changeCols(1, 'C', 'a1'), [
                GLOBAL_OPS, STYLESHEET_OPS, rowOps(1), cellOps(1),
                hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/

    @Test
    public void changeColumnsIndependentOperations01() throws JSONException {
        // Result: Should skip transformations.
        // testRunner.runBidiTest(changeCols(1, 'C', 'a1'), [
        //  GLOBAL_OPS, STYLESHEET_OPS, rowOps(1), cellOps(1),
        //  hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
        //  noteOps(1), commentOps(1), selectOps(1),
        //  drawingOps(1), chartOps(1), drawingTextOps(1)
        //  ]);
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "C", "a1", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createRowOps("1", null),
                SheetHelper.createCellOps("1"),
                SheetHelper.createHlinkOps("1", new String[] {}),
                SheetHelper.createNameOps("1"),
                SheetHelper.createTableOps("1"),
                SheetHelper.createDvRuleOps("1", null),
                SheetHelper.createCfRuleOps("1", null),
                SheetHelper.createNoteOps("1"),
                SheetHelper.createCommentOps("1"),
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }
/*
    describe('"changeColumns" and "changeColumns"', function () {

        var LCL_ATTRS = { f1: { p1: 10, p2: 20 } };     // local attributes
        var EXT_ATTRS_1 = { f2: { p1: 10 } };           // independent formatting attributes (local and external remain unchanged)
        var EXT_ATTRS_2 = { f1: { p1: 10, p3: 30 } };   // reduced formatting (both operations remain active)
        var EXT_ATTRS_3 = { f1: { p1: 10 } };           // reduced formatting (external operation becomes no-op)
        var EXT_ATTRS_4 = LCL_ATTRS;                    // equal formatting (both operations become no-op)
        var LCL_ATTRS_REDUCED = { f1: { p2: 20 } };     // expected reduced local attribute set (for steps 2 and 3)
        var EXT_ATTRS_REDUCED = { f1: { p3: 30 } };     // expected reduced external attribute set (for step 2)

        it('should not transform anything in distinct intervals', function () {
            testRunner.runTest(changeCols(1, 'A:C', LCL_ATTRS), changeCols(1, 'D:F', EXT_ATTRS_1));
            testRunner.runTest(changeCols(1, 'A:C', LCL_ATTRS), changeCols(1, 'D:F', EXT_ATTRS_2));
            testRunner.runTest(changeCols(1, 'A:C', LCL_ATTRS), changeCols(1, 'D:F', EXT_ATTRS_3));
            testRunner.runTest(changeCols(1, 'A:C', LCL_ATTRS), changeCols(1, 'D:F', EXT_ATTRS_4));
            testRunner.runTest(changeCols(1, 'A:C', 'a1'),      changeCols(1, 'D:F', 'a1'));
        });
        it('should transform with partially overlapping intervals', function () {
            testRunner.runTest(changeCols(1, 'A:D', LCL_ATTRS), changeCols(1, 'C:F', EXT_ATTRS_1));
            testRunner.runTest(changeCols(1, 'A:D', LCL_ATTRS), changeCols(1, 'C:F', EXT_ATTRS_2), changeCols(1, 'A:D', LCL_ATTRS), opSeries1(changeCols, [1, 'E:F', EXT_ATTRS_2], [1, 'C:D', EXT_ATTRS_REDUCED]));
            testRunner.runTest(changeCols(1, 'A:D', LCL_ATTRS), changeCols(1, 'C:F', EXT_ATTRS_3), changeCols(1, 'A:D', LCL_ATTRS), changeCols(1, 'E:F', EXT_ATTRS_3));
            testRunner.runTest(changeCols(1, 'A:D', LCL_ATTRS), changeCols(1, 'C:F', EXT_ATTRS_4), changeCols(1, 'A:B', LCL_ATTRS), changeCols(1, 'E:F', EXT_ATTRS_4));
            testRunner.runTest(changeCols(1, 'A:D', 'a1'),      changeCols(1, 'C:F', 'a2'),        changeCols(1, 'A:D', 'a1'),      changeCols(1, 'E:F', 'a2'));
            testRunner.runTest(changeCols(1, 'A:D', 'a1'),      changeCols(1, 'C:F', 'a1'),        changeCols(1, 'A:B', 'a1'),      changeCols(1, 'E:F', 'a1'));
        });
        it('should transform with dominant local intervals', function () {
            testRunner.runTest(changeCols(1, 'A:F', LCL_ATTRS), changeCols(1, 'C:D', EXT_ATTRS_1));
            testRunner.runTest(changeCols(1, 'A:F', LCL_ATTRS), changeCols(1, 'C:D', EXT_ATTRS_2), changeCols(1, 'A:F',     LCL_ATTRS), changeCols(1, 'C:D', EXT_ATTRS_REDUCED));
            testRunner.runTest(changeCols(1, 'A:F', LCL_ATTRS), changeCols(1, 'C:D', EXT_ATTRS_3), changeCols(1, 'A:F',     LCL_ATTRS), []);
            testRunner.runTest(changeCols(1, 'A:F', LCL_ATTRS), changeCols(1, 'C:D', EXT_ATTRS_4), changeCols(1, 'A:B E:F', LCL_ATTRS), []);
            testRunner.runTest(changeCols(1, 'A:F', 'a1'),      changeCols(1, 'C:D', 'a2'),        changeCols(1, 'A:F',     'a1'),      []);
            testRunner.runTest(changeCols(1, 'A:F', 'a1'),      changeCols(1, 'C:D', 'a1'),        changeCols(1, 'A:B E:F', 'a1'),      []);
        });
        it('should transform with dominant external intervals', function () {
            testRunner.runTest(changeCols(1, 'C:D', LCL_ATTRS), changeCols(1, 'A:F', EXT_ATTRS_1));
            testRunner.runTest(changeCols(1, 'C:D', LCL_ATTRS), changeCols(1, 'A:F', EXT_ATTRS_2), changeCols(1, 'C:D', LCL_ATTRS_REDUCED), opSeries1(changeCols, [1, 'A:B E:F', EXT_ATTRS_2], [1, 'C:D', EXT_ATTRS_REDUCED]));
            testRunner.runTest(changeCols(1, 'C:D', LCL_ATTRS), changeCols(1, 'A:F', EXT_ATTRS_3), changeCols(1, 'C:D', LCL_ATTRS_REDUCED), changeCols(1, 'A:B E:F', EXT_ATTRS_3));
            testRunner.runTest(changeCols(1, 'C:D', LCL_ATTRS), changeCols(1, 'A:F', EXT_ATTRS_4), [],                                      changeCols(1, 'A:B E:F', EXT_ATTRS_4));
            testRunner.runTest(changeCols(1, 'C:D', 'a1'),      changeCols(1, 'A:F', 'a2'),        changeCols(1, 'C:D', 'a1'),              changeCols(1, 'A:B E:F', 'a2'));
            testRunner.runTest(changeCols(1, 'C:D', 'a1'),      changeCols(1, 'A:F', 'a1'),        [],                                      changeCols(1, 'A:B E:F', 'a1'));
        });
        it('should transform with equal intervals', function () {
            testRunner.runTest(changeCols(1, 'A:F', LCL_ATTRS), changeCols(1, 'A:F', EXT_ATTRS_1));
            testRunner.runTest(changeCols(1, 'A:F', LCL_ATTRS), changeCols(1, 'A:F', EXT_ATTRS_2), changeCols(1, 'A:F', LCL_ATTRS_REDUCED), changeCols(1, 'A:F', EXT_ATTRS_REDUCED));
            testRunner.runTest(changeCols(1, 'A:F', LCL_ATTRS), changeCols(1, 'A:F', EXT_ATTRS_3), changeCols(1, 'A:F', LCL_ATTRS_REDUCED), []);
            testRunner.runTest(changeCols(1, 'A:F', LCL_ATTRS), changeCols(1, 'A:F', EXT_ATTRS_4), [],                                      []);
        });
        it('should not transform anything in different sheets', function () {
            -testRunner.runTest(changeCols(1, 'A:D', 'a1'), changeCols(2, 'C:F', 'a1'));
        });
    });
*/

    @Test
    public void changeColumnsChangeColumns01() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:C', LCL_ATTRS),
        //  changeCols(1, 'D:F', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:C", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "D:F", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns02() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:C', LCL_ATTRS),
        //  changeCols(1, 'D:F', EXT_ATTRS_2));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:C", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "D:F", null, SheetHelper.EXT_ATTRS_2).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns03() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:C', LCL_ATTRS),
        //  changeCols(1, 'D:F', EXT_ATTRS_3));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:C", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "D:F", null, SheetHelper.EXT_ATTRS_3).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns04() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:C', LCL_ATTRS),
        //  changeCols(1, 'D:F', EXT_ATTRS_4));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:C", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "D:F", null, SheetHelper.EXT_ATTRS_4).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns05() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:C', 'a1'),
        //  changeCols(1, 'D:F', 'a1'));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:C", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "D:F", "a1", null).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns06() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:D', LCL_ATTRS),
        //  changeCols(1, 'C:F', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:D", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "D:F", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns07() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:D', LCL_ATTRS),
        //  changeCols(1, 'C:F', EXT_ATTRS_2),
        //  changeCols(1, 'A:D', LCL_ATTRS),
        //  opSeries1(changeCols, [1, 'E:F', EXT_ATTRS_2], [1, 'C:D', EXT_ATTRS_REDUCED]));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:D", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "C:F", null, SheetHelper.EXT_ATTRS_2).toString(),
            SheetHelper.createChangeColsOp(1, "A:D", null, SheetHelper.LCL_ATTRS).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "E:F", null, SheetHelper.EXT_ATTRS_2),
                SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.EXT_ATTRS_REDUCED)
            )
        );
    }

    @Test
    public void changeColumnsChangeColumns08() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:D', LCL_ATTRS),
        //  changeCols(1, 'C:F', EXT_ATTRS_3),
        //  changeCols(1, 'A:D', LCL_ATTRS),
        //  changeCols(1, 'E:F', EXT_ATTRS_3));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:D", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "C:F", null, SheetHelper.EXT_ATTRS_3).toString(),
            SheetHelper.createChangeColsOp(1, "A:D", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "E:F", null, SheetHelper.EXT_ATTRS_3).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns09() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:D', LCL_ATTRS),
        //  changeCols(1, 'C:F', EXT_ATTRS_4),
        //  changeCols(1, 'A:B', LCL_ATTRS),
        //  changeCols(1, 'E:F', EXT_ATTRS_4));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:D", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "C:F", null, SheetHelper.EXT_ATTRS_4).toString(),
            SheetHelper.createChangeColsOp(1, "A:B", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "E:F", null, SheetHelper.EXT_ATTRS_4).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns10() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:D', 'a1'),
        //  changeCols(1, 'C:F', 'a2'),
        //  changeCols(1, 'A:D', 'a1'),
        //  changeCols(1, 'E:F', 'a2'));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:D", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "C:F", "a2", null).toString(),
            SheetHelper.createChangeColsOp(1, "A:D", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "E:F", "a2", null).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns11() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:D', 'a1'),
        //  changeCols(1, 'C:F', 'a1'),
        //  changeCols(1, 'A:B', 'a1'),
        //  changeCols(1, 'E:F', 'a1'));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:D", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "C:F", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "A:B", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "E:F", "a1", null).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns12() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', LCL_ATTRS),
        //  changeCols(1, 'C:D', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns13() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', LCL_ATTRS),
        //  changeCols(1, 'C:D', EXT_ATTRS_2),
        //  changeCols(1, 'A:F',     LCL_ATTRS),
        //  changeCols(1, 'C:D', EXT_ATTRS_REDUCED));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.EXT_ATTRS_2).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.EXT_ATTRS_REDUCED).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns14() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', LCL_ATTRS),
        //  changeCols(1, 'C:D', EXT_ATTRS_3),
        //  changeCols(1, 'A:F',     LCL_ATTRS),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.EXT_ATTRS_3).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            "[]"
        );
    }

    @Test
    public void changeColumnsChangeColumns15() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', LCL_ATTRS),
        //  changeCols(1, 'C:D', EXT_ATTRS_4),
        //  changeCols(1, 'A:B E:F', LCL_ATTRS),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.EXT_ATTRS_4).toString(),
            SheetHelper.createChangeColsOp(1, "A:B E:F", null, SheetHelper.LCL_ATTRS).toString(),
            "[]"
        );
    }

    @Test
    public void changeColumnsChangeColumns16() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', 'a1'),
        //  changeCols(1, 'C:D', 'a2'),
        //  changeCols(1, 'A:F',     'a1'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", "a2", null).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", "a1", null).toString(),
            "[]"
        );
    }

    @Test
    public void changeColumnsChangeColumns17() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', 'a1'),
        //  changeCols(1, 'C:D', 'a1'),
        //  changeCols(1, 'A:B E:F', 'a1'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "A:B E:F", "a1", null).toString(),
            "[]"
        );
    }

    @Test
    public void changeColumnsChangeColumns18() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeCols(1, 'C:D', LCL_ATTRS),
        //  changeCols(1, 'A:F', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns19() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeCols(1, 'C:D', LCL_ATTRS),
        //  changeCols(1, 'A:F', EXT_ATTRS_2),
        //  changeCols(1, 'C:D', LCL_ATTRS_REDUCED),
        //  opSeries1(changeCols, [1, 'A:B E:F', EXT_ATTRS_2], [1, 'C:D', EXT_ATTRS_REDUCED]));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.EXT_ATTRS_2).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.LCL_ATTRS_REDUCED).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "A:B E:F", null, SheetHelper.EXT_ATTRS_2),
                SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.EXT_ATTRS_REDUCED)
            )
        );
    }

    @Test
    public void changeColumnsChangeColumns20() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeCols(1, 'C:D', LCL_ATTRS),
        //  changeCols(1, 'A:F', EXT_ATTRS_3),
        //  changeCols(1, 'C:D', LCL_ATTRS_REDUCED),
        //  changeCols(1, 'A:B E:F', EXT_ATTRS_3));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.EXT_ATTRS_3).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.LCL_ATTRS_REDUCED).toString(),
            SheetHelper.createChangeColsOp(1, "A:B E:F", null, SheetHelper.EXT_ATTRS_3).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns21() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeCols(1, 'C:D', LCL_ATTRS),
        //  changeCols(1, 'A:F', EXT_ATTRS_4),
        //  [],
        //  changeCols(1, 'A:B E:F', EXT_ATTRS_4));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "C:D", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.EXT_ATTRS_4).toString(),
            "[]",
            SheetHelper.createChangeColsOp(1, "A:B E:F", null, SheetHelper.EXT_ATTRS_4).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns22() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeCols(1, 'C:D', 'a1'),
        //  changeCols(1, 'A:F', 'a2'),
        //  changeCols(1, 'C:D', 'a1'),
        //  changeCols(1, 'A:B E:F', 'a2'));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "C:D", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", "a2", null).toString(),
            SheetHelper.createChangeColsOp(1, "C:D", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "A:B E:F", "a2", null).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns23() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeCols(1, 'C:D', 'a1'),
        //  changeCols(1, 'A:F', 'a1'),
        //  [],
        //  changeCols(1, 'A:B E:F', 'a1'));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "C:D", "a1", null).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", "a1", null).toString(),
            "[]",
            SheetHelper.createChangeColsOp(1, "A:B E:F", "a1", null).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns24() throws JSONException {
        // Result: Should transform with equal intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', LCL_ATTRS),
        //  changeCols(1, 'A:F', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns25() throws JSONException {
        // Result: Should transform with equal intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', LCL_ATTRS),
        //  changeCols(1, 'A:F', EXT_ATTRS_2),
        //  changeCols(1, 'A:F', LCL_ATTRS_REDUCED),
        //  changeCols(1, 'A:F', EXT_ATTRS_REDUCED));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.EXT_ATTRS_2).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS_REDUCED).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.EXT_ATTRS_REDUCED).toString()
        );
    }

    @Test
    public void changeColumnsChangeColumns26() throws JSONException {
        // Result: Should transform with equal intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', LCL_ATTRS),
        //  changeCols(1, 'A:F', EXT_ATTRS_3),
        //  changeCols(1, 'A:F', LCL_ATTRS_REDUCED),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.EXT_ATTRS_3).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS_REDUCED).toString(),
            "[]"
        );
    }

    @Test
    public void changeColumnsChangeColumns27() throws JSONException {
        // Result: Should transform with equal intervals.
        // testRunner.runTest(
        //  changeCols(1, 'A:F', LCL_ATTRS),
        //  changeCols(1, 'A:F', EXT_ATTRS_4),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeColsOp(1, "A:F", null, SheetHelper.EXT_ATTRS_4).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeColumnsChangeColumns28() throws JSONException {
        // Result: Should not transform anything in different sheets.
        // testRunner.runTest(
        //  changeCols(1, 'A:D', 'a1'),
        //  changeCols(2, 'C:F', 'a1'));
        SheetHelper.runTest(
            SheetHelper.createChangeColsOp(1, "A:D", "a1", null).toString(),
            SheetHelper.createChangeColsOp(2, "C:F", "a1", null).toString()
        );
    }

/*
    Not required for backend.

    describe('"changeColumns" and drawing anchor', function () {
        it('should log warnings for absolute anchor', function () {
            var count = 4 * COL_ATTRS.length;
            testRunner.expectBidiWarnings(opSeries2(changeCols, 1, 'C', COL_ATTRS), drawingAnchorOps(1, '2 A1 0 0 C3 0 0'), count);
        });
        it('should not log warnings without changed column size', function () {
            testRunner.expectBidiWarnings(changeCols(1, 'C', ATTRS), drawingAnchorOps(1, '2 A1 0 0 C3 0 0'), 0);
        });
        it('should not log warnings without anchor attribute', function () {
            testRunner.expectBidiWarnings(opSeries2(changeCols, 1, 'C', COL_ATTRS), drawingNoAnchorOps(1), 0);
        });
    });
*/

}
