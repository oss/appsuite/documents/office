/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class InsertCFRule {

/*
    describe('"insertCFRule" and "insertCFRule"', function () {
        it('should skip different rules', function () {
            testRunner.runTest(insertCFRule(1, 'R1', 'A1:D4'), insertCFRule(2, 'R1', 'A1:D4'));
            testRunner.runTest(insertCFRule(1, 'R1', 'A1:D4'), insertCFRule(1, 'R2', 'E5:H8'));
        });
        it('should update rules when inserting the same rule', function () {
            testRunner.runTest(
                insertCFRule(1, 'R1', 'A1:D4'), insertCFRule(1, 'R1', 'B2:E5', { stop: true }),
                changeCFRule(1, 'R1', 'A1:D4'), changeCFRule(1, 'R1', { stop: true })
            );
            testRunner.runTest(
                insertCFRule(1, 'R1', 'A1:D4'), insertCFRule(1, 'R1', 'B2:E5'),
                changeCFRule(1, 'R1', 'A1:D4'), []
            );
        });
        it('should succeed to create overlapping rules', function () {
                testRunner.runTest(insertCFRule(1, 'R1', 'A1:D4'), insertCFRule(1, 'R2', 'B2:E5'));
        });
    });
*/

    @Test
    public void insertCFRuleInsertCFRule01() throws JSONException {
        // Result: Skip different rules.
        // testRunner.runTest(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  insertCFRule(2, 'R1', 'A1:D4'));
        SheetHelper.runTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createInsertCFRuleOp(2, "R1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertCFRuleInsertCFRule02() throws JSONException {
        // Result: Skip different rules.
        // testRunner.runTest(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  insertCFRule(1, 'R2', 'E5:H8'));
        SheetHelper.runTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createInsertCFRuleOp(1, "R2", "E5:H8", null).toString()
        );
    }

    @Test
    public void insertCFRuleInsertCFRule03() throws JSONException {
        // Result: Should update rules when inserting the same rule.
        // testRunner.runTest(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  insertCFRule(1, 'R1', 'B2:E5', { stop: true }),
        //  changeCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(1, 'R1', { stop: true }));
        SheetHelper.runTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createInsertCFRuleOp(1, "R1", "B2:E5", "{ stop: true }").toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, "{ stop: true }").toString()
        );
    }

    @Test
    public void insertCFRuleInsertCFRule04() throws JSONException {
        // Result: Should update rules when inserting the same rule.
        // testRunner.runTest(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  insertCFRule(1, 'R1', 'B2:E5'),
        //  changeCFRule(1, 'R1', 'A1:D4'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createInsertCFRuleOp(1, "R1", "B2:E5", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null).toString(),
            "[]"
        );
    }

    @Test
    public void insertCFRuleInsertCFRule05() throws JSONException {
        // Result: Should succeed to create overlapping rules.
        // testRunner.expectError(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  insertCFRule(1, 'R2', 'B2:E5'));
        SheetHelper.runTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createInsertCFRuleOp(1, "R2", "B2:E5", null).toString()
        );
    }

/*
    describe('"insertCFRule" and "deleteCFRule"', function () {
        it('should skip different rules', function () {
            testRunner.runBidiTest(insertCFRule(1, 'R1', 'A1:D4'), deleteCFRule(2, 'R1'));
            testRunner.runBidiTest(insertCFRule(1, 'R1', 'A1:D4'), deleteCFRule(1, 'R2'));
        });
        it('should fail to insert an existing rule', function () {
            testRunner.expectBidiError(insertCFRule(1, 'R1', 'A1:D4'), deleteCFRule(1, 'R1'));
        });
    });
*/

    @Test
    public void insertCFRuleDeleteCFRule01() throws JSONException {
        // Result: Should skip different rules.
        // testRunner.runBidiTest(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  deleteCFRule(2, 'R1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createDeleteCFRuleOp(2, "R1").toString()
        );
    }

    @Test
    public void insertCFRuleDeleteCFRule02() throws JSONException {
        // Result: Should skip different rules.
        // testRunner.runBidiTest(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  deleteCFRule(1, 'R2'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createDeleteCFRuleOp(1, "R2").toString()
        );
    }

    @Test
    public void insertCFRuleDeleteCFRule03() throws JSONException {
        // Result: Should fail to insert an existing rule.
        // testRunner.expectBidiError(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  deleteCFRule(1, 'R1'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createDeleteCFRuleOp(1, "R1").toString()
        );
    }

/*
    describe('"insertCFRule" and "changeCFRule"', function () {
        it('should skip different rules', function () {
            testRunner.runBidiTest(insertCFRule(1, 'R1', 'A1:D4'), changeCFRule(2, 'R1', 'A1:D4'));
            testRunner.runBidiTest(insertCFRule(1, 'R1', 'A1:D4'), changeCFRule(1, 'R2', 'E5:H8'));
        });
        it('should fail to insert an existing rule', function () {
            testRunner.expectBidiError(insertCFRule(1, 'R1', 'A1:D4'), changeCFRule(1, 'R1', 'E5:H8'));
        });
        it('should succeed to create overlapping rules', function () {
            testRunner.runBidiTest(insertCFRule(1, 'R1', 'A1:D4'), changeCFRule(1, 'R2', 'B2:E5'));
        });
    });
*/
    @Test
    public void insertCFRuleChangeCFRule01() throws JSONException {
        // Result: Should skip different rules.
        // testRunner.runBidiTest(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(2, 'R1', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(2, "R1", "A1:D4", null).toString()
        );
    }

    @Test
    public void insertCFRuleChangeCFRule02() throws JSONException {
        // Result: Should skip different rules.
        // testRunner.runBidiTest(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(1, 'R2', 'E5:H8'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R2", "E5:H8", null).toString()
        );
    }

    @Test
    public void insertCFRuleChangeCFRule03() throws JSONException {
        // Result: Should fail to insert an existing rule.
        // testRunner.expectBidiError(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(1, 'R1', 'E5:H8'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", "E5:H8", null).toString()
        );
    }

    @Test
    public void insertCFRuleChangeCFRule04() throws JSONException {
        // Result: Should succeed to create overlapping rules.
        // testRunner.runBidiTest(
        //  insertCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(1, 'R2', 'B2:E5'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R2", "B2:E5", null).toString()
        );
    }
}
