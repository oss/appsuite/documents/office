/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class InsertChangeNote {

    /*
     * describe('"insertNote" and "changeNote"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runBidiTest(insertNote(1, 'A1', 'abc'), changeNote(2, 'A1', 'abc'));
                testRunner.runBidiTest(insertNote(1, 'A1', 'abc'), changeNote(1, 'B2', 'abc'));
            });
            it('should fail to insert an existing note', function () {
                testRunner.expectBidiError(insertNote(1, 'A1', 'abc'), changeNote(1, 'A1', 'abc'));
            });
        });
      */

    @Test
    public void insertNoteChangeNote01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  changeNote(2, 'A1', 'abc'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createChangeNoteOp(2, "A1", "abc", null).toString()
        );
    }

    @Test
    public void insertNoteChangeNote02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  changeNote(1, 'B2', 'abc'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createChangeNoteOp(1, "B2", "abc", null).toString()
        );
    }

    @Test
    public void insertNoteChangeNote03() throws JSONException {
        // Result: Should fail to insert an existing note.
        // testRunner.expectBidiError(
        //  insertNote(1, 'A1', 'abc'),
        //  changeNote(1, 'A1', 'abc'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "abc", null).toString()
        );
    }
}
