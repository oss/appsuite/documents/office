/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertMoveNote {

    /*
     * describe('"insertNote" and "moveNotes"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runBidiTest(insertNote(1, 'A1', 'abc'), moveNotes(2, 'A1 B2', 'B2 A1'));
                testRunner.runBidiTest(insertNote(1, 'A1', 'abc'), moveNotes(1, 'B2 C3', 'C3 B2'));
            });
            it('should fail to insert an existing note', function () {
                testRunner.expectBidiError(insertNote(1, 'A1', 'abc'), moveNotes(1, 'A1 B2', 'B2 A1'));
            });
            it('should delete moved note before inserting a note at the address', function () {
                testRunner.runBidiTest(
                    insertNote(1, 'A1', 'abc'), moveNotes(1, 'B2 C3', 'A1 B2'),
                    [deleteNote(1, 'A1'), insertNote(1, 'A1', 'abc')], moveNotes(1, 'C3', 'B2')
                );
            });
        });
      */

    @Test
    public void insertNoteMoveNote01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        //  testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  moveNotes(2, 'A1 B2', 'B2 A1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createMoveNotesOp(2, "A1 B2", "B2 A1").toString()
        );
    }

    @Test
    public void insertNoteMoveNote02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        //  testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  moveNotes(1, 'B2 C3', 'C3 B2'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createMoveNotesOp(2, "B2 C3", "C3 B2").toString()
        );
    }

    @Test
    public void insertNoteMoveNote03() throws JSONException {
        // Result: Should fail to insert an existing note.
        // testRunner.expectBidiError(
        //  insertNote(1, 'A1', 'abc'),
        //  moveNotes(1, 'A1 B2', 'B2 A1'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createMoveNotesOp(1, "A1 B2", "B2 A1").toString()
        );
    }

    @Test
    public void insertNoteMoveNote04() throws JSONException {
        // Result: Should delete moved note before inserting a note at the address.
        // testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  moveNotes(1, 'B2 C3', 'A1 B2'),
        //  [deleteNote(1, 'A1'), insertNote(1, 'A1', 'abc')],
        //  moveNotes(1, 'C3', 'B2'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createMoveNotesOp(1, "B2 C3", "A1 B2").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "A1"),
                SheetHelper.createInsertNoteOp(1, "A1", "abc", null)
            ),
            SheetHelper.createMoveNotesOp(1, "C3", "B2").toString()
        );
    }
}
