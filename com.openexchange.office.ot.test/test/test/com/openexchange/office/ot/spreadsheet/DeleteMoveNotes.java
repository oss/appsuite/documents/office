/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class DeleteMoveNotes {

    /*
     * describe('"deleteNote" and "moveNotes', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runBidiTest(deleteNote(1, 'A1'), moveNotes(2, 'A1 B2', 'B2 A1'));
                testRunner.runBidiTest(deleteNote(1, 'A1'), moveNotes(1, 'B2 C3', 'C3 B2'));
            });
            it('should delete an existing note', function () {
                testRunner.runBidiTest(
                    deleteNote(1, 'A1'), moveNotes(1, 'A1 B2', 'B2 A1'),
                    deleteNote(1, 'B2'), moveNotes(1, 'B2',    'A1')
                );
                testRunner.runBidiTest(
                    deleteNote(1, 'A1'), moveNotes(1, 'A1', 'B2'),
                    deleteNote(1, 'B2'), []
                );
            });
            it('should fail to move to an existing note', function () {
                testRunner.expectBidiError(deleteNote(1, 'A1'), moveNotes(1, 'B2', 'A1'));
            });
        });
     */

    @Test
    public void deleteNoteMoveComment01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  deleteNote(1, 'A1'),
        //  moveNotes(2, 'A1 B2', 'B2 A1'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNoteOp(1, "A1").toString(),
            SheetHelper.createMoveNotesOp(2, "A1 B2", "B2 A1").toString()
        );
    }

    @Test
    public void deleteNoteMoveComment02() throws JSONException {
        // Result: Should delete an existing note.
        // testRunner.runBidiTest(
        //  deleteNote(1, 'A1'),
        //  moveNotes(1, 'A1 B2', 'B2 A1'),
        //  deleteNote(1, 'B2'),
        //  moveNotes(1, 'B2',    'A1')
        //  );
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteNoteOp(1, "A1").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 B2", "B2 A1").toString(),
            SheetHelper.createDeleteNoteOp(1, "B2").toString(),
            SheetHelper.createMoveNotesOp(1, "B2", "A1").toString()
        );
    }

    @Test
    public void deleteCommentMoveComment03() throws JSONException {
        // Result: Should fail to move to an existing note.
        // testRunner.expectBidiError(
        //  deleteNote(1, 'A1'),
        //  moveNotes(1, 'B2', 'A1'));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteNoteOp(1, "A1").toString(),
            SheetHelper.createMoveNotesOp(1, "B2", "A1").toString()
        );
    }
}
