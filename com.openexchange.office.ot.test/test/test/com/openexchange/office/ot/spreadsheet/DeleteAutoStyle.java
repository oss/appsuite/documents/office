/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DeleteAutoStyle {

/*
    describe('"deleteAutoStyle" and "insertColumns"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertCols(1, 'A', 'a0'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertCols(1, 'A', 'a1'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertCols(1, 'A', 'a3'), deleteAutoStyle('a2', OTIS), insertCols(1, 'A', 'a2'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertCols(1, 'A', 'a4'), deleteAutoStyle('a2', OTIS), insertCols(1, 'A', 'a3'));
        });
        it('should delete the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertCols(1, 'A', 'a2'),     deleteAutoStyle('a2', OTIS), insertCols(1, 'A'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertCols(1, 'A', 'a2', {}), deleteAutoStyle('a2', OTIS), insertCols(1, 'A', {}));
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(deleteAutoStyle('a2'), insertCols(1, 'A', 'b2'));
            testRunner.expectBidiError(deleteAutoStyle('b2'), insertCols(1, 'A', 'a2'));
            testRunner.expectBidiError(deleteAutoStyle('b2'), insertCols(1, 'A', 'b2'));
        });
    });
*/
    @Test
    public void insertAutoStyleInsertColumns01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertColsOp(1, "A", "a0", null).toString());
    }

    @Test
    public void insertAutoStyleInsertColumns02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertColsOp(1, "A", "a1", null).toString());
    }

    @Test
    public void insertAutoStyleInsertColumns03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertColsOp(1, "A", "a3", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createInsertColsOp(1, "A", "a2", null).toString(), null);
    }

    @Test
    public void insertAutoStyleInsertColumns04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertColsOp(1, "A", "a4", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createInsertColsOp(1, "A", "a3", null).toString(), null);
    }

    @Test
    public void insertAutoStyleInsertColumns05() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertColsOp(1, "A", "a2", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createInsertColsOp(1, "A", null, null).toString(), null);
    }

    @Test
    public void insertAutoStyleInsertColumns06() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertColsOp(1, "A", "a2", new JSONObject()).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createInsertColsOp(1, "A", null, new JSONObject()).toString(), null);
    }

/*
    describe('"deleteAutoStyle" and "changeColumns"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCols(1, 'A', 'a0'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCols(1, 'A', 'a1'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCols(1, 'A', 'a3'), deleteAutoStyle('a2', OTIS), changeCols(1, 'A', 'a2'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCols(1, 'A', 'a4'), deleteAutoStyle('a2', OTIS), changeCols(1, 'A', 'a3'));
        });
        it('should delete the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCols(1, 'A', 'a2'),     deleteAutoStyle('a2', OTIS), []);
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCols(1, 'A', 'a2', {}), deleteAutoStyle('a2', OTIS), changeCols(1, 'A', {}));
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(insertAutoStyle('a2', {}), changeCols(1, 'A', 'b2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeCols(1, 'A', 'a2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeCols(1, 'A', 'b2'));
        });
    });
*/
    @Test
    public void insertAutoStyleChangeColumns01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeColsOp(1, "A", "a0", null).toString());
    }

    @Test
    public void insertAutoStyleChangeColumns02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeColsOp(1, "A", "a1", null).toString());
    }

    @Test
    public void insertAutoStyleChangeColumns03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeColsOp(1, "A", "a3", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeColsOp(1, "A", "a2", null).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeColumns04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeColsOp(1, "A", "a4", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeColsOp(1, "A", "a3", null).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeColumns05() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeColsOp(1, "A", "a2", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), "[]", null);
    }

    @Test
    public void insertAutoStyleChangeColumns06() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeColsOp(1, "A", "a2", new JSONObject()).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeColsOp(1, "A", null, new JSONObject()).toString(), null);
    }

/*
    describe('"deleteAutoStyle" and "insertRows"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertRows(1, '1', 'a0'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertRows(1, '1', 'a1'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertRows(1, '1', 'a3'), deleteAutoStyle('a2', OTIS), insertRows(1, '1', 'a2'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertRows(1, '1', 'a4'), deleteAutoStyle('a2', OTIS), insertRows(1, '1', 'a3'));
        });
        it('should delete the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertRows(1, '1', 'a2'),     deleteAutoStyle('a2', OTIS), insertRows(1, '1'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), insertRows(1, '1', 'a2', {}), deleteAutoStyle('a2', OTIS), insertRows(1, '1', {}));
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(deleteAutoStyle('a2'), insertRows(1, '1', 'b2'));
            testRunner.expectBidiError(deleteAutoStyle('b2'), insertRows(1, '1', 'a2'));
            testRunner.expectBidiError(deleteAutoStyle('b2'), insertRows(1, '1', 'b2'));
        });
    });
*/
    @Test
    public void insertAutoStyleInsertRows01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertRowsOp(1, "1", "a0", null).toString());
    }

    @Test
    public void insertAutoStyleInsertRows02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertRowsOp(1, "1", "a1", null).toString());
    }

    @Test
    public void insertAutoStyleInsertRows03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertRowsOp(1, "1", "a3", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createInsertRowsOp(1, "1", "a2", null).toString(), null);
    }

    @Test
    public void insertAutoStyleInsertRows04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertRowsOp(1, "1", "a4", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createInsertRowsOp(1, "1", "a3", null).toString(), null);
    }

    @Test
    public void insertAutoStyleInsertRows05() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertRowsOp(1, "1", "a2", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createInsertRowsOp(1, "1", null, null).toString(), null);
    }

    @Test
    public void insertAutoStyleInsertRows06() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createInsertRowsOp(1, "1", "a2", new JSONObject()).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createInsertRowsOp(1, "1", null, new JSONObject()).toString(), null);
    }

/*
    describe('"deleteAutoStyle" and "changeRows"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeRows(1, '1', 'a0'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeRows(1, '1', 'a1'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeRows(1, '1', 'a3'), deleteAutoStyle('a2', OTIS), changeRows(1, '1', 'a2'));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeRows(1, '1', 'a4'), deleteAutoStyle('a2', OTIS), changeRows(1, '1', 'a3'));
        });
        it('should delete the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeRows(1, '1', 'a2'),     deleteAutoStyle('a2', OTIS), []);
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeRows(1, '1', 'a2', {}), deleteAutoStyle('a2', OTIS), changeRows(1, '1', {}));
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(insertAutoStyle('a2', {}), changeRows(1, '1', 'b2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeRows(1, '1', 'a2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeRows(1, '1', 'b2'));
        });
    });
*/
    @Test
    public void insertAutoStyleChangeRows01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeRowsOp(1, "1", "a0", null).toString());
    }

    @Test
    public void insertAutoStyleChangeRows02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeRowsOp(1, "1", "a1", null).toString());
    }

    @Test
    public void insertAutoStyleChangeRows03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeRowsOp(1, "1", "a3", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeRowsOp(1, "1", "a2", null).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeRows04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeRowsOp(1, "1", "a4", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeRowsOp(1, "1", "a3", null).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeRows05() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeRowsOp(1, "1", "a2", null).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), "[]", null);
    }

    @Test
    public void insertAutoStyleChangeRows06() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeRowsOp(1, "1", "a2", new JSONObject()).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeRowsOp(1, "1", null, new JSONObject()).toString(), null);
    }

/*
    describe('"deleteAutoStyle" and "changeCells"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCells(1, { A1: { s: 'a0' } }));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCells(1, { A1: { s: 'a1' } }));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCells(1, { A1: { s: 'a3' } }), deleteAutoStyle('a2', OTIS), changeCells(1, { A1: { s: 'a2' } }));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCells(1, { A1: { s: 'a4' } }), deleteAutoStyle('a2', OTIS), changeCells(1, { A1: { s: 'a3' } }));
        });
        it('should delete the auto-style identifier', function () {
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCells(1, { A1: { s: 'a2', v: 42 } }),  deleteAutoStyle('a2', OTIS), changeCells(1, { A1: { v: 42 } }));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCells(1, { A1: { s: 'a2' }, A2: 42 }), deleteAutoStyle('a2', OTIS), changeCells(1, { A2: 42 }));
            testRunner.runBidiTest(deleteAutoStyle('a2'), changeCells(1, { A1: { s: 'a2' } }),         deleteAutoStyle('a2', OTIS), []);
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(deleteAutoStyle('a2', {}), changeCells(1, { A1: { s: 'b2' } }));
            testRunner.expectBidiError(deleteAutoStyle('b2', {}), changeCells(1, { A1: { s: 'a2' } }));
            testRunner.expectBidiError(deleteAutoStyle('b2', {}), changeCells(1, { A1: { s: 'b2' } }));
        });
    });
*/
    @Test
    public void insertAutoStyleChangeCells01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a0' } }")).toString());
    }

    @Test
    public void insertAutoStyleChangeCells02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a1' } }")).toString());
    }

    @Test
    public void insertAutoStyleChangeCells03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a3' } }")).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a2' } }")).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeCells04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a4' } }")).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a3' } }")).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeCells05() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a2', v: 42 } }")).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { v: 42 } }")).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeCells06() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a2' }, A2: 42 }")).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A2: 42 }")).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeCells07() throws JSONException {
        SheetHelper.runBidiTest(Helper.createDeleteAutoStyleOp("a2").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a2' } }")).toString(), Helper.createDeleteAutoStyleOp("a2", "{ otIndexShift: true }").toString(), "[]", null);
    }
}
