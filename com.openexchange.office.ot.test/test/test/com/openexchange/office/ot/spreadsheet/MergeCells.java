/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class MergeCells {

/*
    describe('"mergeCells" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(mergeCells(1, 'A1:B2'), [
                GLOBAL_OPS, STYLESHEET_OPS, hlinkOps(1), nameOps(1),
                deleteTable(1, 'T'), changeTableCol(1, 'T', 1, { attrs: ATTRS }),
                dvRuleOps(1), cfRuleOps(1), noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/
    @Test
    public void MergeCells01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:B2", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createHlinkOps("1"),
                SheetHelper.createNameOps("1"),
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createChangeTableColOp(1, "T", 1, "{ attrs: " + SheetHelper.ATTRS + "}"),
                SheetHelper.createDvRuleOps("1", null),
                SheetHelper.createCfRuleOps("1", null),
                SheetHelper.createNoteOps("1"),
                SheetHelper.createCommentOps("1"),
//              SheetHelper.createSelectOps("1"),
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

/*
    describe('"mergeCells" and "mergeCells"', function () {
        it('should skip different sheets', function () {
            testRunner.runTest(mergeCells(1, 'A1:B2', MM_MERGE), mergeCells(2, 'B2:C3', MM_MERGE));
        });
        it('should remove external MERGE ranges covered by local ranges', function () {
            testRunner.runTest(
                opSeries2(mergeCells, 1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', MM_ALL), mergeCells(1, 'C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6', MM_MERGE),
                opSeries2(mergeCells, 1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', MM_ALL), mergeCells(1, 'C4:D6 A4:B6',                   MM_MERGE)
            );
        });
        it('should reduce external HORIZONTAL ranges', function () {
            testRunner.runTest(
                opSeries2(mergeCells, 1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', MM_ALL), mergeCells(1, 'C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6', MM_HORIZONTAL),
                opSeries2(mergeCells, 1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', MM_ALL), mergeCells(1, 'C4:D6 H5:K6 T1:Y2 T5:Y6 A4:B6', MM_HORIZONTAL)
            );
            testRunner.runTest(
                opSeries2(mergeCells, 1, 'B2 D4 F6', MM_ALL), mergeCells(1, 'A1:G7', MM_HORIZONTAL),
                opSeries2(mergeCells, 1, 'B2 D4 F6', MM_ALL), mergeCells(1, 'A1:G1 A3:G3 A5:G5 A7:G7', MM_HORIZONTAL)
            );
        });
        it('should reduce external VERTICAL ranges', function () {
            testRunner.runTest(
                opSeries2(mergeCells, 1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', MM_ALL), mergeCells(1, 'C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6', MM_VERTICAL),
                opSeries2(mergeCells, 1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', MM_ALL), mergeCells(1, 'C4:D6 J3:K6 T1:U6 X1:Y6 A4:B6', MM_VERTICAL)
            );
            testRunner.runTest(
                opSeries2(mergeCells, 1, 'B2 D4 F6', MM_ALL), mergeCells(1, 'A1:G7', MM_VERTICAL),
                opSeries2(mergeCells, 1, 'B2 D4 F6', MM_ALL), mergeCells(1, 'A1:A7 C1:C7 E1:E7 G1:G7', MM_VERTICAL)
            );
        });
        it('should reduce external UNMERGE ranges', function () {
            testRunner.runTest(
                opSeries2(mergeCells, 1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', MM_ALL), mergeCells(1, 'C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6',                   MM_UNMERGE),
                opSeries2(mergeCells, 1, 'A1:B3 F1:I4 M1:R6 V3:W4 C1:D3', MM_ALL), mergeCells(1, 'C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6', MM_UNMERGE)
            );
        });
        it('should set no-op to "removed" state', function () {
            testRunner.runTest(mergeCells(1, 'B4:C5', MM_MERGE), mergeCells(1, 'C4:D5', MM_MERGE), null, []);
        });
    });
*/

    // it('should skip different sheets', function () {
    @Test
    public void mergeCellsMergeCells02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B2", "merge").toString(), SheetHelper.createMergeCellsOp(2, "B2:C3", "merge").toString());
    }

    // it('should remove external MERGE ranges covered by local ranges', function () {
    @Test
    public void mergeCellsMergeCells03a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "merge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "merge").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "merge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 A4:B6", "merge").toString());
    }

    @Test
    public void mergeCellsMergeCells03b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "merge").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 A4:B6", "merge").toString());
    }

    @Test
    public void mergeCellsMergeCells03c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "merge").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 A4:B6", "merge").toString());
    }

    @Test
    public void mergeCellsMergeCells03d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "merge").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 A4:B6", "merge").toString());
    }

    // it('should reduce external HORIZONTAL ranges', function () {
    @Test
    public void mergeCellsMergeCells04a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "merge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "horizontal").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "merge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H5:K6 T1:Y2 T5:Y6 A4:B6", "horizontal").toString());
    }

    @Test
    public void mergeCellsMergeCells04b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "horizontal").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H5:K6 T1:Y2 T5:Y6 A4:B6", "horizontal").toString());
    }

    @Test
    public void mergeCellsMergeCells04c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "horizontal").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H5:K6 T1:Y2 T5:Y6 A4:B6", "horizontal").toString());
    }

    @Test
    public void mergeCellsMergeCells04d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "horizontal").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H5:K6 T1:Y2 T5:Y6 A4:B6", "horizontal").toString());
    }

    @Test
    public void mergeCellsMergeCells05a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "merge").toString(), SheetHelper.createMergeCellsOp(1, "A1:G7", "horizontal").toString(),
                            SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "merge").toString(), SheetHelper.createMergeCellsOp(1, "A1:G1 A3:G3 A5:G5 A7:G7", "horizontal").toString());
    }

    @Test
    public void mergeCellsMergeCells05b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "A1:G7", "horizontal").toString(),
                            SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "A1:G1 A3:G3 A5:G5 A7:G7", "horizontal").toString());
    }

    @Test
    public void mergeCellsMergeCells05c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "A1:G7", "horizontal").toString(),
                            SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "A1:G1 A3:G3 A5:G5 A7:G7", "horizontal").toString());
    }

    @Test
    public void mergeCellsMergeCells05d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "A1:G7", "horizontal").toString(),
                            SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "A1:G1 A3:G3 A5:G5 A7:G7", "horizontal").toString());
    }

    // it('should reduce external VERTICAL ranges', function () {
    @Test
    public void mergeCellsMergeCells06a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "merge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "vertical").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "merge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 J3:K6 T1:U6 X1:Y6 A4:B6", "vertical").toString());
    }

    public void mergeCellsMergeCells06b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "vertical").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 J3:K6 T1:U6 X1:Y6 A4:B6", "vertical").toString());
    }

    public void mergeCellsMergeCells06c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "vertical").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 J3:K6 T1:U6 X1:Y6 A4:B6", "vertical").toString());
    }

    public void mergeCellsMergeCells06d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "vertical").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 J3:K6 T1:U6 X1:Y6 A4:B6", "vertical").toString());
    }

    @Test
    public void mergeCellsMergeCells07a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "merge").toString(), SheetHelper.createMergeCellsOp(1, "A1:G7", "vertical").toString(),
                            SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "merge").toString(), SheetHelper.createMergeCellsOp(1, "A1:A7 C1:C7 E1:E7 G1:G7", "vertical").toString());
    }

    @Test
    public void mergeCellsMergeCells07b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "A1:G7", "vertical").toString(),
                            SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "A1:A7 C1:C7 E1:E7 G1:G7", "vertical").toString());
    }

    @Test
    public void mergeCellsMergeCells07c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "A1:G7", "vertical").toString(),
                            SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "A1:A7 C1:C7 E1:E7 G1:G7", "vertical").toString());
    }

    @Test
    public void mergeCellsMergeCells07d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "A1:G7", "vertical").toString(),
                            SheetHelper.createMergeCellsOp(1, "B2 D4 F6", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "A1:A7 C1:C7 E1:E7 G1:G7", "vertical").toString());
    }

    // it('should reduce external UNMERGE ranges', function () {
    @Test
    public void mergeCellsMergeCells08a() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "merge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "unmerge").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "merge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6", "unmerge").toString());
    }

    @Test
    public void mergeCellsMergeCells08b() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "unmerge").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "horizontal").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6", "unmerge").toString());
    }

    @Test
    public void mergeCellsMergeCells08c() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "unmerge").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "vertical").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6", "unmerge").toString());
    }

    @Test
    public void mergeCellsMergeCells08d() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 H3:K6 O3:P4 T1:Y6 A4:B6", "unmerge").toString(),
                            SheetHelper.createMergeCellsOp(1, "A1:B3 F1:I4 M1:R6 V3:W4 C1:D3", "unmerge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D6 J3:K4 H5:K6 T1:Y2 T3:U4 X3:Y4 T5:Y6 A4:B6", "unmerge").toString());
    }

    // it('should set no-op to "removed" state', function () {
    @Test
    public void mergeCellsMergeCells09() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMergeCellsOp(1, "B4:C5", "merge").toString(), SheetHelper.createMergeCellsOp(1, "C4:D5", "merge").toString(), SheetHelper.createMergeCellsOp(1, "B4:C5", "merge").toString(), "[]");
    }

/*
    describe('"mergeCells" and "insertTable"', function () {
        it('should merge outside the table range', function () {
            testRunner.runBidiTest(opSeries2(mergeCells, 1, 'A1:D4', MM_ALL), insertTable(1, 'T', 'E5:H8'));
        });
        it('should unmerge before inserting overlapping table', function () {
            testRunner.runBidiTest(
                mergeCells(1, 'A1:D4', MM_MERGE), insertTable(1, 'T', 'D4:G7'),
                [], [mergeCells(1, 'D4:G7', MM_UNMERGE), insertTable(1, 'T', 'D4:G7')]
            );
            testRunner.runBidiTest(
                mergeCells(1, 'A1:D4', MM_HORIZONTAL), insertTable(1, 'T', 'D4:G7'),
                mergeCells(1, 'A1:D3', MM_HORIZONTAL), [mergeCells(1, 'D4:G7', MM_UNMERGE), insertTable(1, 'T', 'D4:G7')]
            );
            testRunner.runBidiTest(
                mergeCells(1, 'A1:D4', MM_VERTICAL), insertTable(1, 'T', 'D4:G7'),
                mergeCells(1, 'A1:C4', MM_VERTICAL), [mergeCells(1, 'D4:G7', MM_UNMERGE), insertTable(1, 'T', 'D4:G7')]
            );
        });
        it('should allow to unmerge over a table range', function () {
            testRunner.runBidiTest(mergeCells(1, 'A1:D4', MM_UNMERGE), opSeries2(insertTable, 1, 'T', ['A1:D4', 'D4:G7']));
        });
        it('should skip different sheets', function () {
            testRunner.runBidiTest(mergeCells(1, 'A1:D4', MM_MERGE), insertTable(2, 'T', 'A1:D4'));
        });
    });
*/
    // it('should merge outside the table range', function () {
    @Test
    public void mergeCellsInsertTable1a() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "merge").toString(), SheetHelper.createInsertTableOp(1, "T", "E5:H8", null).toString());
    }

    @Test
    public void mergeCellsInsertTable1b() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "horizontal").toString(), SheetHelper.createInsertTableOp(1, "T", "E5:H8", null).toString());
    }

    @Test
    public void mergeCellsInsertTable1c() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "vertical").toString(), SheetHelper.createInsertTableOp(1, "T", "E5:H8", null).toString());
    }

    @Test
    public void mergeCellsInsertTable1d() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "unmerge").toString(), SheetHelper.createInsertTableOp(1, "T", "E5:H8", null).toString());
    }

    // it('should unmerge before inserting overlapping table', function () {
    @Test
    public void mergeCellsInsertTable02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "merge").toString(), SheetHelper.createInsertTableOp(1, "T", "D4:G7", null).toString(), "[]",
                                Helper.createArrayFromJSON(SheetHelper.createMergeCellsOp(1, "D4:G7", "unmerge"), SheetHelper.createInsertTableOp(1, "T", "D4:G7", null)));
    }

    @Test
    public void mergeCellsInsertTable03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "horizontal").toString(), SheetHelper.createInsertTableOp(1, "T", "D4:G7", null).toString(),
                                SheetHelper.createMergeCellsOp(1, "A1:D3", "horizontal").toString(),
                                Helper.createArrayFromJSON(SheetHelper.createMergeCellsOp(1, "D4:G7", "unmerge"), SheetHelper.createInsertTableOp(1, "T", "D4:G7", null)));
    }

    @Test
    public void mergeCellsInsertTable04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "vertical").toString(), SheetHelper.createInsertTableOp(1, "T", "D4:G7", null).toString(),
                                SheetHelper.createMergeCellsOp(1, "A1:C4", "vertical").toString(),
                                Helper.createArrayFromJSON(SheetHelper.createMergeCellsOp(1, "D4:G7", "unmerge"), SheetHelper.createInsertTableOp(1, "T", "D4:G7", null)));
    }

    // it('should allow to unmerge over a table range', function () {
    @Test
    public void mergeCellsInsertTable5a() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "unmerge").toString(), SheetHelper.createInsertTableOp(1, "T", "A1:D4", null).toString());
    }

    @Test
    public void mergeCellsInsertTable5b() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "unmerge").toString(), SheetHelper.createInsertTableOp(1, "T", "D4:G7", null).toString());
    }

    // it('should skip different sheets', function () {
    @Test
    public void mergeCellsInsertTable6() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "merge").toString(), SheetHelper.createInsertTableOp(2, "T", "A1:D4", null).toString());
    }

/*
    describe('"mergeCells" and "changeTable"', function () {
        it('should merge outside the table range', function () {
            testRunner.runBidiTest(opSeries2(mergeCells, 1, 'A1:D4', MM_ALL), changeTable(1, 'T', 'E5:H8'));
            testRunner.runBidiTest(opSeries2(mergeCells, 1, 'A1:D4', MM_ALL), changeTable(1, 'T', { newName: 'T2' }));
        });
        it('should unmerge before moving table', function () {
            testRunner.runBidiTest(
                mergeCells(1, 'A1:D4', MM_MERGE), changeTable(1, 'T', 'D4:G7'),
                [], [mergeCells(1, 'D4:G7', MM_UNMERGE), changeTable(1, 'T', 'D4:G7')]
            );
            testRunner.runBidiTest(
                mergeCells(1, 'A1:D4', MM_HORIZONTAL), changeTable(1, 'T', 'D4:G7'),
                mergeCells(1, 'A1:D3', MM_HORIZONTAL), [mergeCells(1, 'D4:G7', MM_UNMERGE), changeTable(1, 'T', 'D4:G7')]
            );
            testRunner.runBidiTest(
                mergeCells(1, 'A1:D4', MM_VERTICAL), changeTable(1, 'T', 'D4:G7'),
                mergeCells(1, 'A1:C4', MM_VERTICAL), [mergeCells(1, 'D4:G7', MM_UNMERGE), changeTable(1, 'T', 'D4:G7')]
            );
        });
        it('should allow to unmerge over a table range', function () {
            testRunner.runBidiTest(mergeCells(1, 'A1:D4', MM_UNMERGE), changeTable(1, 'T', 'D4:G7'));
        });
        it('should skip different sheets', function () {
            testRunner.runBidiTest(mergeCells(1, 'A1:D4', MM_MERGE), changeTable(2, 'T', 'A1:D4'));
        });
    });
*/

    @Test
    public void mergeCellsChangeTable1a() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "merge").toString(), SheetHelper.createChangeTableOp(1, "T", "E5:H8", null).toString());
    }

    @Test
    public void mergeCellsChangeTable1b() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "horizontal").toString(), SheetHelper.createChangeTableOp(1, "T", "E5:H8", null).toString());
    }

    @Test
    public void mergeCellsChangeTable1c() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "vertical").toString(), SheetHelper.createChangeTableOp(1, "T", "E5:H8", null).toString());
    }

    @Test
    public void mergeCellsChangeTable1d() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "unmerge").toString(), SheetHelper.createChangeTableOp(1, "T", "E5:H8", null).toString());
    }

    @Test
    public void mergeCellsChangeTable2a() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "merge").toString(), SheetHelper.createChangeTableOp(1, "T", null, "{ newName: 'T2' }").toString());
    }

    @Test
    public void mergeCellsChangeTable2b() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "horizontal").toString(), SheetHelper.createChangeTableOp(1, "T", null, "{ newName: 'T2' }").toString());
    }

    @Test
    public void mergeCellsChangeTable2c() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "vertical").toString(), SheetHelper.createChangeTableOp(1, "T", null, "{ newName: 'T2' }").toString());
    }

    @Test
    public void mergeCellsChangeTable2d() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "unmerge").toString(), SheetHelper.createChangeTableOp(1, "T", null, "{ newName: 'T2' }").toString());
    }

    // it('should unmerge before moving table', function () {
    @Test
    public void mergeCellsChangeTable03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "merge").toString(), SheetHelper.createChangeTableOp(1, "T", "D4:G7", null).toString(), "[]",
                                Helper.createArrayFromJSON(SheetHelper.createMergeCellsOp(1, "D4:G7", "unmerge"), SheetHelper.createChangeTableOp(1, "T", "D4:G7", null)));
    }

    @Test
    public void mergeCellsChangeTable04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "horizontal").toString(), SheetHelper.createChangeTableOp(1, "T", "D4:G7", null).toString(),
                                SheetHelper.createMergeCellsOp(1, "A1:D3", "horizontal").toString(),
                                Helper.createArrayFromJSON(SheetHelper.createMergeCellsOp(1, "D4:G7", "unmerge"), SheetHelper.createChangeTableOp(1, "T", "D4:G7", null)));
    }

    @Test
    public void mergeCellsChangeTable05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "vertical").toString(), SheetHelper.createChangeTableOp(1, "T", "D4:G7", null).toString(),
                                SheetHelper.createMergeCellsOp(1, "A1:C4", "vertical").toString(),
                                Helper.createArrayFromJSON(SheetHelper.createMergeCellsOp(1, "D4:G7", "unmerge"), SheetHelper.createChangeTableOp(1, "T", "D4:G7", null)));
    }

    // it('should allow to unmerge over a table range', function () {
    @Test
    public void mergeCellsChangeTable6() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "unmerge").toString(), SheetHelper.createChangeTableOp(1, "T", "D4:G7", null).toString());
    }

    // it('should skip different sheets', function () {
    @Test
    public void mergeCellsChangeTable7() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMergeCellsOp(1, "A1:D4", "merge").toString(), SheetHelper.createChangeTableOp(2, "T", "A1:D4", null).toString());
    }
}
