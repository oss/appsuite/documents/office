/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class InsertDVRule {

/*
    describe('"insertDVRule" and "insertDVRule"', function () {
        it('should skip different sheets', function () {
            testRunner.runTest(insertDVRule(1, 1, 'A1:D4'), insertDVRule(2, 1, 'A1:D4'));
        });
        it('should shift rule index', function () {
            testRunner.runTest(insertDVRule(1, 2, 'A1:D4'), insertDVRule(1, 0, 'E5:H8'), insertDVRule(1, 3, 'A1:D4'), insertDVRule(1, 0, 'E5:H8'));
            testRunner.runTest(insertDVRule(1, 2, 'A1:D4'), insertDVRule(1, 1, 'E5:H8'), insertDVRule(1, 3, 'A1:D4'), insertDVRule(1, 1, 'E5:H8'));
            testRunner.runTest(insertDVRule(1, 2, 'A1:D4'), insertDVRule(1, 2, 'E5:H8'), insertDVRule(1, 3, 'A1:D4'), insertDVRule(1, 2, 'E5:H8'));
            testRunner.runTest(insertDVRule(1, 2, 'A1:D4'), insertDVRule(1, 3, 'E5:H8'), insertDVRule(1, 2, 'A1:D4'), insertDVRule(1, 4, 'E5:H8'));
            testRunner.runTest(insertDVRule(1, 2, 'A1:D4'), insertDVRule(1, 4, 'E5:H8'), insertDVRule(1, 2, 'A1:D4'), insertDVRule(1, 5, 'E5:H8'));
        });
        it('should fail to create overlapping rules', function () {
            testRunner.expectError(insertDVRule(1, 0, 'A1:D4'), insertDVRule(1, 2, 'B2:E5'));
        });
    });
*/

    // it('should skip different sheets', function () {
    @Test
    public void InsertDVRuleInsertDVRule01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 1, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(2, 1, "A1:D4", null).toString());
    }

    // it('should shift rule index', function () {
    @Test
    public void InsertDVRuleInsertDVRule02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 0, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 3, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 0, "E5:H8", null).toString());
    }

    @Test
    public void InsertDVRuleInsertDVRule03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 1, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 3, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 1, "E5:H8", null).toString());
    }

    @Test
    public void InsertDVRuleInsertDVRule04() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 2, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 3, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 2, "E5:H8", null).toString());
    }

    @Test
    public void InsertDVRuleInsertDVRule05() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 3, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 4, "E5:H8", null).toString());
    }

    @Test
    public void InsertDVRuleInsertDVRule06() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 4, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createInsertDVRuleOp(1, 5, "E5:H8", null).toString());
    }

/*
    describe('"insertDVRule" and "deleteDVRule"', function () {
        it('should skip different sheets', function () {
            testRunner.runBidiTest(insertDVRule(1, 1, 'A1:D4'), deleteDVRule(2, 1));
        });
        it('should shift rule index', function () {
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), deleteDVRule(1, 0), insertDVRule(1, 1, 'A1:D4'), deleteDVRule(1, 0));
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), deleteDVRule(1, 1), insertDVRule(1, 1, 'A1:D4'), deleteDVRule(1, 1));
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), deleteDVRule(1, 2), insertDVRule(1, 2, 'A1:D4'), deleteDVRule(1, 3));
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), deleteDVRule(1, 3), insertDVRule(1, 2, 'A1:D4'), deleteDVRule(1, 4));
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), deleteDVRule(1, 4), insertDVRule(1, 2, 'A1:D4'), deleteDVRule(1, 5));
        });
    });
*/

    // it('should skip different sheets', function () {
    @Test
    public void InsertDVRuleDeleteDVRule01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertDVRuleOp(1, 1, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(2, 1).toString());
    }

    // it('should shift rule index', function () {
    public void InsertDVRuleDeleteDVRule02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 0).toString(),
                                SheetHelper.createInsertDVRuleOp(1, 1, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 0).toString());
    }

    public void InsertDVRuleDeleteDVRule03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 1).toString(),
                                SheetHelper.createInsertDVRuleOp(1, 1, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 1).toString());
    }

    public void InsertDVRuleDeleteDVRule04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
                                SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 3).toString());
    }

    public void InsertDVRuleDeleteDVRule05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 3).toString(),
                                SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 4).toString());
    }

    public void InsertDVRuleDeleteDVRule06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 4).toString(),
                                SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createDeleteDVRuleOp(1, 5).toString());
    }

/*
    describe('"insertDVRule" and "changeDVRule"', function () {
        it('should skip different sheets', function () {
            testRunner.runBidiTest(insertDVRule(1, 1, 'A1:D4'), changeDVRule(2, 1, 'A1:D4'));
        });
        it('should shift rule index', function () {
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 0, 'E5:H8'), insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 0, 'E5:H8'));
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 1, 'E5:H8'), insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 1, 'E5:H8'));
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 2, 'E5:H8'), insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 3, 'E5:H8'));
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 3, 'E5:H8'), insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 4, 'E5:H8'));
            testRunner.runBidiTest(insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 4, 'E5:H8'), insertDVRule(1, 2, 'A1:D4'), changeDVRule(1, 5, 'E5:H8'));
        });
        it('should fail to create overlapping rules', function () {
            testRunner.expectBidiError(insertDVRule(1, 0, 'A1:D4'), changeDVRule(1, 2, 'B2:E5'));
        });
    });
*/

    // it('should skip different sheets', function () {
    @Test
    public void InsertDVRuleChangeDVRule01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertDVRuleOp(1, 1, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(2, 1, "A1:D4", null).toString());
    }

    // it('should shift rule index', function () {
    @Test
    public void InsertDVRuleChangeDVRule02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 0, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 0, "E5:H8", null).toString());
    }

    @Test
    public void InsertDVRuleChangeDVRule03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 1, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 1, "E5:H8", null).toString());
    }

    @Test
    public void InsertDVRuleChangeDVRule04() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 2, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 3, "E5:H8", null).toString());
    }

    @Test
    public void InsertDVRuleChangeDVRule05() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 3, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 4, "E5:H8", null).toString());
    }

    @Test
    public void InsertDVRuleChangeDVRule06() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 4, "E5:H8", null).toString(),
                            SheetHelper.createInsertDVRuleOp(1, 2, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 5, "E5:H8", null).toString());
    }

    // it('should fail to create overlapping rules', function () {
    @Test
    public void InsertDVRuleChangeDVRule07() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createInsertDVRuleOp(1, 0, "A1:D4", null).toString(), SheetHelper.createChangeDVRuleOp(1, 2, "B2:E5", null).toString());
    }
}
