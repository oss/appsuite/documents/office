/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class Charts {

    /*
        describe('chart operations and independent operations', function () {
            it('should skip transformations', function () {
                testRunner.runBidiTest(chartOps(1), [GLOBAL_OPS, STYLESHEET_OPS, positionOp(1, 0), selectOps(1)]);
            });
        });
     */
    @Test
    public void test01() {
        SheetHelper.runBidiTest(SheetHelper.createChartOps("1").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS
            )
        );
    }

    /*
        describe('chart operations and drawing text operations', function () {
            it('should skip independent sheets', function () {
                testRunner.runBidiTest(chartOps(1), drawingTextOps(0));
            });
            it('should skip independent shapes', function () {
                testRunner.runBidiTest(insertChSeries(1, 0, 0, ATTRS), insertText(1, '1 6 7', 'abc'));
                testRunner.runBidiTest(changeChAxis(1, 0, 0, ATTRS), deleteOp(1, '1 6 7'));
            });
            it('should fail for embedded shapes', function () {
                testRunner.expectBidiError(insertChSeries(1, 0, 0, ATTRS), insertText(1, '0 6 7', 'abc'));
                testRunner.expectBidiError(changeChAxis(1, 0, 0, ATTRS), deleteOp(1, '0 6 7'));
            });
        });
     */
    @Test
    public void test02() {
        SheetHelper.runBidiTest(SheetHelper.createChartOps("1").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDrawingTextOps("0")
            )
        );
    }

    @Test
    public void test03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertChSeriesOp("1 0", 0, SheetHelper.ATTRS).toString(),
            Helper.createInsertTextOp("1 1 6 7", "abc").toString()
        );
    }

    @Test
    public void test04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createChangeChAxisOp("1 0", 0, SheetHelper.ATTRS).toString(),
            Helper.createDeleteOp("1 1 6 7", null).toString()
        );
    }

    @Test
    public void test05() throws JSONException {
        SheetHelper.expectBidiError(Helper.createInsertChSeriesOp("1 0", 0, SheetHelper.ATTRS).toString(), Helper.createInsertTextOp("1 0 6 7",  "abc").toString());
    }

    @Test
    public void test06() throws JSONException {
        SheetHelper.expectBidiError(Helper.createChangeChAxisOp("1 0", 0, SheetHelper.ATTRS).toString(), Helper.createDeleteOp("1 0 6 7", null).toString());
    }
}
