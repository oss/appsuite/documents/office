/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class MoveSheets {

/*
    describe('"moveSheets" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(moveSheets(MOVE_SHEETS), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
        });
    });
*/
    @Test
    public void moveSheets01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.AUTOSTYLE_OPS
            )
        );
    }

/*
    describe('"moveSheets" and "moveSheets"', function () {
        var MOVE_SHEETS_2 = [3, 2, 6, 0, 4, 5, 1];
        it('should handle move operations transforming each other', function () {
            testRunner.runTest(moveSheets(MOVE_SHEETS), moveSheets(MOVE_SHEETS_2), moveSheets([3, 2, 0, 4, 1, 6, 5]), []);
            testRunner.runTest(moveSheets(MOVE_SHEETS_2), moveSheets(MOVE_SHEETS), moveSheets([2, 4, 1, 0, 3, 6, 5]), []);
        });
        it('should detect local no-op', function () {
            testRunner.runTest(moveSheets(MOVE_SHEETS), moveSheets(MOVE_SHEETS), [], []);
        });
    });
*/
    // standard sort vector for "moveSheets" operations
    final static public String MOVE_SHEETS_2 = "3 2 6 0 4 5 1";

    @Test
    public void moveSheetsMoveSheets01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createMoveSheetsOp(MOVE_SHEETS_2).toString(), SheetHelper.createMoveSheetsOp("3 2 0 4 1 6 5").toString(), "[]", null);
    }

    @Test
    public void moveSheetsMoveSheets02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMoveSheetsOp(MOVE_SHEETS_2).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createMoveSheetsOp("2 4 1 0 3 6 5").toString(), "[]", null);
    }

    @Test
    public void moveSheetsMoveSheets03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", "[]", null);
    }

/*
    describe('"moveSheets" and sheet index operations', function () {
        it('should shift sheet index', function () {
            testRunner.runBidiTest(
                moveSheets(MOVE_SHEETS), allSheetIndexOps(0, 1, 2, 3, 4, 5, 6),
                moveSheets(MOVE_SHEETS), allSheetIndexOps(0, 5, 4, 2, 3, 6, 1)
            );
        });
    });
*/
    @Test
    public void moveSheetsAllSheetIndexOps01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createAllSheetIndexOps("0 1 2 3 4 5 6", true),
                                SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createAllSheetIndexOps("0 5 4 2 3 6 1", true), null);
    }
}
