/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DeleteDrawingDrawingOperations {

    /*
     * describe('"deleteDrawing" and other drawing operations', function () {
            it('should skip different sheets', function () {
                testRunner.runBidiTest(deleteDrawing(1, 0), changeDrawingOps(0, 1));
            });
            it('should shift positions for top-level shape', function () {
                testRunner.runBidiTest(
                    deleteDrawing(1, 1), changeDrawingOps(1, '0', '1', '4', '2', '0 4', '1 5', '2 6'),
                    deleteDrawing(1, 1), changeDrawingOps(1, '0',      '3', '1', '0 4',        '1 6')
                );
            });
            it('should shift positions for embedded shape', function () {
                testRunner.runBidiTest(
                    deleteDrawing(1, '1 1'), changeDrawingOps(1, '1 0', '1 1', '1 4', '1 3', '1 0 4', '1 1 5', '1 2 6'),
                    deleteDrawing(1, '1 1'), changeDrawingOps(1, '1 0',        '1 3', '1 2', '1 0 4',          '1 1 6')
                );
            });
            it('should not shift positions of parent shapes', function () {
                testRunner.runBidiTest(deleteDrawing(1, '1 1'), changeDrawingOps(1, 0, 2));
                testRunner.runBidiTest(deleteDrawing(1, '1 1'), changeDrawing(1, 1, ATTRS));
            });
            it('should not shift positions of sibling shapes', function () {
                -testRunner.runBidiTest(deleteDrawing(1, '1 1'), changeDrawingOps(1, '0 0', '0 1', '0 2', '2 0', '2 1', '2 2'));
            });
        });
    */

    @Test
    public void deleteDrawingDrawingOperations01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 0),
        //  changeDrawingOps(0, 1));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 0").toString(),
            SheetHelper.changeDrawingOps(0, "1")
        );
    }

    @Test
    public void deleteDrawingDrawingOperations02() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, 1),
        //  changeDrawingOps(1, '0', '1', '4', '2', '0 4', '1 5', '2 6'),
        //  deleteDrawing(1, 1),
        //  changeDrawingOps(1, '0',      '3', '1', '0 4',        '1 6'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1").toString(),
            SheetHelper.changeDrawingOps(1, "0", "1", "4", "2", "0 4", "1 5", "2 6"),
            Helper.createDeleteDrawingOp("1 1").toString(),
            SheetHelper.changeDrawingOps(1, "0", "3", "1", "0 4", "1 6")
        );
    }

    @Test
    public void deleteDrawingDrawingOperations03() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  changeDrawingOps(1, '1 0', '1 1', '1 4', '1 3', '1 0 4', '1 1 5', '1 2 6'),
        //  deleteDrawing(1, '1 1'),
        //  changeDrawingOps(1, '1 0',        '1 3', '1 2', '1 0 4',          '1 1 6'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            SheetHelper.changeDrawingOps(1, "1 0", "1 1", "1 4", "1 3", "1 0 4", "1 1 5", "1 2 6"),
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            SheetHelper.changeDrawingOps(1, "1 0", "1 3", "1 2", "1 0 4", "1 1 6")
        );
    }

    @Test
    public void deleteDrawingDrawingOperations04() throws JSONException {
        // Result: Should not shift positions of parent shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  changeDrawingOps(1, 0, 2));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            SheetHelper.changeDrawingOps(1, "0", "2")
        );
    }

    @Test
    public void deleteDrawingDrawingOperations05() throws JSONException {
        // Result: Should not shift positions of parent shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  changeDrawing(1, 1, ATTRS));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void deleteDrawingDrawingOperations06() throws JSONException {
        // Result: Should not shift positions of sibling shapes.
        // testRunner.runBidiTest(
        //  deleteDrawing(1, '1 1'),
        //  changeDrawingOps(1, '0 0', '0 1', '0 2', '2 0', '2 1', '2 2'));
        SheetHelper.runBidiTest(
            Helper.createDeleteDrawingOp("1 1 1").toString(),
            SheetHelper.changeDrawingOps(1, "0 0", "0 1", "0 2", "2 0", "2 1", "2 2")
        );
    }
}
