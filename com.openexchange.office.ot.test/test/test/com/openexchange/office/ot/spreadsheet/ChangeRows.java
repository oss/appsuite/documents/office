/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeRows {

/*
    describe('"changeRows" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(changeRows(1, '3', 'a1'), [
                GLOBAL_OPS, STYLESHEET_OPS, colOps(1), cellOps(1),
                hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/
    @Test
    public void changeRowsIndependentOperations01() throws JSONException {
        // Result: Should skip transformations.
        // testRunner.runBidiTest(changeRows(1, '3', 'a1'), [
        //  GLOBAL_OPS, STYLESHEET_OPS, colOps(1), cellOps(1),
        //  hlinkOps(1), nameOps(1), tableOps(1), dvRuleOps(1), cfRuleOps(1),
        //  noteOps(1), commentOps(1), selectOps(1),
        //  drawingOps(1), chartOps(1), drawingTextOps(1)
        //  ]);
        SheetHelper.runBidiTest(
            SheetHelper.createChangeRowsOp(1, "3", "a1", null ).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createColOps("1", null),
                SheetHelper.createCellOps("1"),
                SheetHelper.createHlinkOps("1", new String[] {}),
                SheetHelper.createNameOps("1"),
                SheetHelper.createTableOps("1"),
                SheetHelper.createDvRuleOps("1", null),
                SheetHelper.createCfRuleOps("1", null),
                SheetHelper.createNoteOps("1"),
                SheetHelper.createCommentOps("1"),
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

/*
    describe('"changeRows" and "changeRows"', function () {

        var LCL_ATTRS = { f1: { p1: 10, p2: 20 } };     // local attributes
        var EXT_ATTRS_1 = { f2: { p1: 10 } };           // independent formatting attributes (local and external remain unchanged)
        var EXT_ATTRS_2 = { f1: { p1: 10, p3: 30 } };   // reduced formatting (both operations remain active)
        var EXT_ATTRS_3 = { f1: { p1: 10 } };           // reduced formatting (external operation becomes no-op)
        var EXT_ATTRS_4 = LCL_ATTRS;                    // equal formatting (both operations become no-op)
        var LCL_ATTRS_REDUCED = { f1: { p2: 20 } };     // expected reduced local attribute set (for steps 2 and 3)
        var EXT_ATTRS_REDUCED = { f1: { p3: 30 } };     // expected reduced external attribute set (for step 2)

        it('should not transform anything in distinct intervals', function () {
            testRunner.runTest(changeRows(1, '1:3', LCL_ATTRS), changeRows(1, '4:6', EXT_ATTRS_1));
            testRunner.runTest(changeRows(1, '1:3', LCL_ATTRS), changeRows(1, '4:6', EXT_ATTRS_2));
            testRunner.runTest(changeRows(1, '1:3', LCL_ATTRS), changeRows(1, '4:6', EXT_ATTRS_3));
            testRunner.runTest(changeRows(1, '1:3', LCL_ATTRS), changeRows(1, '4:6', EXT_ATTRS_4));
            testRunner.runTest(changeRows(1, '1:3', 'a1'),      changeRows(1, '4:6', 'a1'));
        });
        it('should transform with partially overlapping intervals', function () {
            testRunner.runTest(changeRows(1, '1:4', LCL_ATTRS), changeRows(1, '3:6', EXT_ATTRS_1));
            testRunner.runTest(changeRows(1, '1:4', LCL_ATTRS), changeRows(1, '3:6', EXT_ATTRS_2), changeRows(1, '1:4', LCL_ATTRS), opSeries1(changeRows, [1, '5:6', EXT_ATTRS_2], [1, '3:4', EXT_ATTRS_REDUCED]));
            testRunner.runTest(changeRows(1, '1:4', LCL_ATTRS), changeRows(1, '3:6', EXT_ATTRS_3), changeRows(1, '1:4', LCL_ATTRS), changeRows(1, '5:6', EXT_ATTRS_3));
            testRunner.runTest(changeRows(1, '1:4', LCL_ATTRS), changeRows(1, '3:6', EXT_ATTRS_4), changeRows(1, '1:2', LCL_ATTRS), changeRows(1, '5:6', EXT_ATTRS_4));
            testRunner.runTest(changeRows(1, '1:4', 'a1'),      changeRows(1, '3:6', 'a2'),        changeRows(1, '1:4', 'a1'),      changeRows(1, '5:6', 'a2'));
            testRunner.runTest(changeRows(1, '1:4', 'a1'),      changeRows(1, '3:6', 'a1'),        changeRows(1, '1:2', 'a1'),      changeRows(1, '5:6', 'a1'));
        });
        it('should transform with dominant local intervals', function () {
            testRunner.runTest(changeRows(1, '1:6', LCL_ATTRS), changeRows(1, '3:4', EXT_ATTRS_1));
            testRunner.runTest(changeRows(1, '1:6', LCL_ATTRS), changeRows(1, '3:4', EXT_ATTRS_2), changeRows(1, '1:6',     LCL_ATTRS), changeRows(1, '3:4', EXT_ATTRS_REDUCED));
            testRunner.runTest(changeRows(1, '1:6', LCL_ATTRS), changeRows(1, '3:4', EXT_ATTRS_3), changeRows(1, '1:6',     LCL_ATTRS), []);
            testRunner.runTest(changeRows(1, '1:6', LCL_ATTRS), changeRows(1, '3:4', EXT_ATTRS_4), changeRows(1, '1:2 5:6', LCL_ATTRS), []);
            testRunner.runTest(changeRows(1, '1:6', 'a1'),      changeRows(1, '3:4', 'a2'),        changeRows(1, '1:6',     'a1'),      []);
            testRunner.runTest(changeRows(1, '1:6', 'a1'),      changeRows(1, '3:4', 'a1'),        changeRows(1, '1:2 5:6', 'a1'),      []);
        });
        it('should transform with dominant external intervals', function () {
            testRunner.runTest(changeRows(1, '3:4', LCL_ATTRS), changeRows(1, '1:6', EXT_ATTRS_1));
            testRunner.runTest(changeRows(1, '3:4', LCL_ATTRS), changeRows(1, '1:6', EXT_ATTRS_2), changeRows(1, '3:4', LCL_ATTRS_REDUCED), opSeries1(changeRows, [1, '1:2 5:6', EXT_ATTRS_2], [1, '3:4', EXT_ATTRS_REDUCED]));
            testRunner.runTest(changeRows(1, '3:4', LCL_ATTRS), changeRows(1, '1:6', EXT_ATTRS_3), changeRows(1, '3:4', LCL_ATTRS_REDUCED), changeRows(1, '1:2 5:6', EXT_ATTRS_3));
            testRunner.runTest(changeRows(1, '3:4', LCL_ATTRS), changeRows(1, '1:6', EXT_ATTRS_4), [],                                      changeRows(1, '1:2 5:6', EXT_ATTRS_4));
            testRunner.runTest(changeRows(1, '3:4', 'a1'),      changeRows(1, '1:6', 'a2'),        changeRows(1, '3:4', 'a1'),              changeRows(1, '1:2 5:6', 'a2'));
            testRunner.runTest(changeRows(1, '3:4', 'a1'),      changeRows(1, '1:6', 'a1'),        [],                                      changeRows(1, '1:2 5:6', 'a1'));
        });
        it('should transform with equal intervals', function () {
            testRunner.runTest(changeRows(1, '1:6', LCL_ATTRS), changeRows(1, '1:6', EXT_ATTRS_1));
            testRunner.runTest(changeRows(1, '1:6', LCL_ATTRS), changeRows(1, '1:6', EXT_ATTRS_2), changeRows(1, '1:6', LCL_ATTRS_REDUCED), changeRows(1, '1:6', EXT_ATTRS_REDUCED));
            testRunner.runTest(changeRows(1, '1:6', LCL_ATTRS), changeRows(1, '1:6', EXT_ATTRS_3), changeRows(1, '1:6', LCL_ATTRS_REDUCED), []);
            testRunner.runTest(changeRows(1, '1:6', LCL_ATTRS), changeRows(1, '1:6', EXT_ATTRS_4), [],                                      []);
        });
        it('should not transform anything in different sheets', function () {
            testRunner.runTest(changeRows(1, '1:4', 'a1'), changeRows(2, '3:6', 'a1'));
        });
    });
*/

    @Test
    public void changeRowsChangeRows01() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:3', LCL_ATTRS),
        //  changeRows(1, '4:6', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:3", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "4:6", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeRowsChangeRows02() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:3', LCL_ATTRS),
        //  changeRows(1, '4:6', EXT_ATTRS_2));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:3", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "4:6", null, SheetHelper.EXT_ATTRS_2).toString()
        );
    }

    @Test
    public void changeRowsChangeRows03() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:3', LCL_ATTRS),
        //  changeRows(1, '4:6', EXT_ATTRS_3));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:3", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "4:6", null, SheetHelper.EXT_ATTRS_3).toString()
        );
    }

    @Test
    public void changeRowsChangeRows04() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:3', LCL_ATTRS),
        //  changeRows(1, '4:6', EXT_ATTRS_4));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:3", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "4:6", null, SheetHelper.EXT_ATTRS_4).toString()
        );
    }

    @Test
    public void changeRowsChangeRows05() throws JSONException {
        // Result: Should not transform anything in distinct intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:3', 'a1'),
        //  changeRows(1, '4:6', 'a1'));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:3", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "4:6", "a1", null).toString()
        );
    }

    @Test
    public void changeRowsChangeRows06() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:4', LCL_ATTRS),
        //  changeRows(1, '3:6', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:4", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "3:6", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeRowsChangeRows07() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:4', LCL_ATTRS),
        //  changeRows(1, '3:6', EXT_ATTRS_2),
        //  changeRows(1, '1:4', LCL_ATTRS),
        //  opSeries1(changeRows, [1, '5:6', EXT_ATTRS_2], [1, '3:4', EXT_ATTRS_REDUCED]));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:4", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "3:6", null, SheetHelper.EXT_ATTRS_2).toString(),
            SheetHelper.createChangeRowsOp(1, "1:4", null, SheetHelper.LCL_ATTRS).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeRowsOp(1, "5:6", null, SheetHelper.EXT_ATTRS_2),
                SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.EXT_ATTRS_REDUCED)
            )
        );
    }

    @Test
    public void changeRowsChangeRows08() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:4', LCL_ATTRS),
        //  changeRows(1, '3:6', EXT_ATTRS_3),
        //  changeRows(1, '1:4', LCL_ATTRS),
        //  changeRows(1, '5:6', EXT_ATTRS_3));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:4", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "3:6", null, SheetHelper.EXT_ATTRS_3).toString(),
            SheetHelper.createChangeRowsOp(1, "1:4", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "5:6", null, SheetHelper.EXT_ATTRS_3).toString()
        );
    }

    @Test
    public void changeRowsChangeRows09() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:4', LCL_ATTRS),
        //  changeRows(1, '3:6', EXT_ATTRS_4),
        //  changeRows(1, '1:2', LCL_ATTRS),
        //  changeRows(1, '5:6', EXT_ATTRS_4));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:4", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "3:6", null, SheetHelper.EXT_ATTRS_4).toString(),
            SheetHelper.createChangeRowsOp(1, "1:2", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "5:6", null, SheetHelper.EXT_ATTRS_4).toString()
        );
    }

    @Test
    public void changeRowsChangeRows10() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:4', 'a1'),
        //  changeRows(1, '3:6', 'a2'),
        //  changeRows(1, '1:4', 'a1'),
        //  changeRows(1, '5:6', 'a2'));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:4", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "3:6", "a2", null).toString(),
            SheetHelper.createChangeRowsOp(1, "1:4", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "5:6", "a2", null).toString()
        );
    }

    @Test
    public void changeRowsChangeRows11() throws JSONException {
        // Result: Should transform with partially overlapping intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:4', 'a1'),
        //  changeRows(1, '3:6', 'a1'),
        //  changeRows(1, '1:2', 'a1'),
        //  changeRows(1, '5:6', 'a1'));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:4", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "3:6", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "1:2", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "5:6", "a1", null).toString()
        );
    }

    @Test
    public void changeRowsChangeRows12() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', LCL_ATTRS),
        //  changeRows(1, '3:4', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeRowsChangeRows13() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', LCL_ATTRS),
        //  changeRows(1, '3:4', EXT_ATTRS_2),
        //  changeRows(1, '1:6',     LCL_ATTRS),
        //  changeRows(1, '3:4', EXT_ATTRS_REDUCED));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.EXT_ATTRS_2).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.EXT_ATTRS_REDUCED).toString()
        );
    }

    @Test
    public void changeRowsChangeRows14() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', LCL_ATTRS),
        //  changeRows(1, '3:4', EXT_ATTRS_3),
        //  changeRows(1, '1:6',     LCL_ATTRS),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.EXT_ATTRS_3).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            "[]"
        );
    }

    @Test
    public void changeRowsChangeRows15() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', LCL_ATTRS),
        //  changeRows(1, '3:4', EXT_ATTRS_4),
        //  changeRows(1, '1:2 5:6', LCL_ATTRS),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.EXT_ATTRS_4).toString(),
            SheetHelper.createChangeRowsOp(1, "1:2 5:6", null, SheetHelper.LCL_ATTRS).toString(),
            "[]"
        );
    }

    @Test
    public void changeRowsChangeRows16() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', 'a1'),
        //  changeRows(1, '3:4', 'a2'),
        //  changeRows(1, '1:6',     'a1'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", "a2", null).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", "a1", null).toString(),
            "[]"
        );
    }

    @Test
    public void changeRowsChangeRows17() throws JSONException {
        // Result: Should transform with dominant local intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', 'a1'),
        //  changeRows(1, '3:4', 'a1'),
        //  changeRows(1, '1:2 5:6', 'a1'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "1:2 5:6", "a1", null).toString(),
            "[]"
        );
    }

    @Test
    public void changeRowsChangeRows18() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeRows(1, '3:4', LCL_ATTRS),
        //  changeRows(1, '1:6', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeRowsChangeRows19() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeRows(1, '3:4', LCL_ATTRS),
        //  changeRows(1, '1:6', EXT_ATTRS_2),
        //  changeRows(1, '3:4', LCL_ATTRS_REDUCED),
        //  opSeries1(changeRows, [1, '1:2 5:6', EXT_ATTRS_2], [1, '3:4', EXT_ATTRS_REDUCED]));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.EXT_ATTRS_2).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.LCL_ATTRS_REDUCED).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeRowsOp(1, "1:2 5:6", null, SheetHelper.EXT_ATTRS_2),
                SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.EXT_ATTRS_REDUCED)
            )
        );
    }

    @Test
    public void changeRowsChangeRows20() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeRows(1, '3:4', LCL_ATTRS),
        //  changeRows(1, '1:6', EXT_ATTRS_3),
        //  changeRows(1, '3:4', LCL_ATTRS_REDUCED),
        //  changeRows(1, '1:2 5:6', EXT_ATTRS_3));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.EXT_ATTRS_3).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.LCL_ATTRS_REDUCED).toString(),
            SheetHelper.createChangeRowsOp(1, "1:2 5:6", null, SheetHelper.EXT_ATTRS_3).toString()
        );
    }

    @Test
    public void changeRowsChangeRows21() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeRows(1, '3:4', LCL_ATTRS),
        //  changeRows(1, '1:6', EXT_ATTRS_4),
        //  [],
        //  changeRows(1, '1:2 5:6', EXT_ATTRS_4));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "3:4", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.EXT_ATTRS_4).toString(),
            "[]",
            SheetHelper.createChangeRowsOp(1, "1:2 5:6", null, SheetHelper.EXT_ATTRS_4).toString()
        );
    }

    @Test
    public void changeRowsChangeRows22() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeRows(1, '3:4', 'a1'),
        //  changeRows(1, '1:6', 'a2'),
        //  changeRows(1, '3:4', 'a1'),
        //  changeRows(1, '1:2 5:6', 'a2'));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "3:4", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", "a2", null).toString(),
            SheetHelper.createChangeRowsOp(1, "3:4", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "1:2 5:6", "a2", null).toString()
        );
    }

    @Test
    public void changeRowsChangeRows23() throws JSONException {
        // Result: Should transform with dominant external intervals.
        // testRunner.runTest(
        //  changeRows(1, '3:4', 'a1'),
        //  changeRows(1, '1:6', 'a1'),
        //  [],
        //  changeRows(1, '1:2 5:6', 'a1'));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "3:4", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", "a1", null).toString(),
            "[]",
            SheetHelper.createChangeRowsOp(1, "1:2 5:6", "a1", null).toString()
        );
    }

    @Test
    public void changeRowsChangeRows24() throws JSONException {
        // Result: Should transform with equal intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', LCL_ATTRS),
        //  changeRows(1, '1:6', EXT_ATTRS_1));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.EXT_ATTRS_1).toString()
        );
    }

    @Test
    public void changeRowsChangeRows25() throws JSONException {
        // Result: Should transform with equal intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', LCL_ATTRS),
        //  changeRows(1, '1:6', EXT_ATTRS_2),
        //  changeRows(1, '1:6', LCL_ATTRS_REDUCED),
        //  changeRows(1, '1:6', EXT_ATTRS_REDUCED));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.EXT_ATTRS_2).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS_REDUCED).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.EXT_ATTRS_REDUCED).toString()
        );
    }

    @Test
    public void changeRowsChangeRows26() throws JSONException {
        // Result: Should transform with equal intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', LCL_ATTRS),
        //  changeRows(1, '1:6', EXT_ATTRS_3),
        //   changeRows(1, '1:6', LCL_ATTRS_REDUCED),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.EXT_ATTRS_3).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS_REDUCED).toString(),
            "[]"
        );
    }

    @Test
    public void changeRowsChangeRows27() throws JSONException {
        // Result: Should transform with equal intervals.
        // testRunner.runTest(
        //  changeRows(1, '1:6', LCL_ATTRS),
        //  changeRows(1, '1:6', EXT_ATTRS_4),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.LCL_ATTRS).toString(),
            SheetHelper.createChangeRowsOp(1, "1:6", null, SheetHelper.EXT_ATTRS_4).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeRowsChangeRows28() throws JSONException {
        // Result: Should not transform anything in different sheets.
        // testRunner.runTest(
        //  changeRows(1, '1:4', 'a1'),
        //  changeRows(2, '3:6', 'a1'));
        SheetHelper.runTest(
            SheetHelper.createChangeRowsOp(1, "1:4", "a1", null).toString(),
            SheetHelper.createChangeRowsOp(2, "3:6", "a1", null).toString()
        );
    }

/*
    Not required for backend.

    describe('"changeRows" and drawing anchor', function () {
        it('should log warnings for absolute anchor', function () {
            var count = 4 * ROW_ATTRS.length;
            testRunner.expectBidiWarnings(opSeries2(changeRows, 1, '3', ROW_ATTRS), drawingAnchorOps(1, '2 A1 0 0 C3 0 0'), count);
        });
        it('should not log warnings without changed row size', function () {
            testRunner.expectBidiWarnings(changeRows(1, '3', ATTRS), drawingAnchorOps(1, '2 A1 0 0 C3 0 0'), 0);
        });
        it('should not log warnings without anchor attribute', function () {
            testRunner.expectBidiWarnings(opSeries2(changeRows, 1, '3', ROW_ATTRS), drawingNoAnchorOps(1), 0);
        });
    });
*/

}
