/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.ot.spreadsheet.Direction;
import com.openexchange.office.ot.tools.OTUtils;
import test.com.openexchange.office.ot.tools.Helper;
import test.com.openexchange.office.ot.tools.I2PositionOp;
import test.com.openexchange.office.ot.tools.IPositionOp;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SheetHelper {

    /*
     * @param {OperationSource} lclOps
     *  The local JSON document operations to be transformed. Creates a deep
     *  clone internally before transformations.
     *
     * @param {OperationSource} extOps
     *  The external JSON document operations to be transformed. Creates a deep
     *  clone internally before transformations.
     *
     * @param {OperationSource|null} [expLclOps]
     *  The expected local operations after all transformations have been
     *  executed (without removed operations). If set to `null` or omitted, the
     *  test will expect that the passed local operations will not be modified
     *  at all. Pass an empty array to expect that all operations have been
     *  removed.
     *
     * @param {OperationSource|null} [expExtOps]
     *  The expected external operations after all transformations have been
     *  executed (without removed operations). If set to `null` or omitted, the
     *  test will expect that the passed external operations will not be
     *  modified at all. Pass an empty array to expect that all operations have
     *  been removed.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.reverse=false]
     *    Whether to run the transformations in reverse mode by passing the
     *    option `reverse` to the `OTEngine` instance.
     *  - {number|boolean} [options.warnings=0]
     *    The expected number of warnings to be issued by all transformations.
     *    If set to `false`, the number of warnings will not be checked. If set
     *    to `true`, expects one warning per executed transformation.
     */
    public static void runBidiTest(String lclOps, String extOps) {
        final String expLclOps = extOps;
        final String expExtOps = lclOps;
        runBidiTest(lclOps, extOps, expExtOps, expLclOps);
    }

    public static void runBidiTest(String lclOps, String extOps, String expLclOps, String expExtOps) {
        runBidiTest(lclOps, extOps, expLclOps, expExtOps, null);
    }

    public static void runBidiTest(String lclOps, String extOps, String expLclOps, String expExtOps, Map<String, Object> options) {
        final String bidiLclOp = extOps;
        final String bidiExtOp = lclOps;
        TransformerTest.transformSpreadsheet(lclOps, extOps, expExtOps, expLclOps);
        // testing the bidi version
        TransformerTest.transformSpreadsheet(bidiLclOp, bidiExtOp, expLclOps, expExtOps);
    }

    public static void runTest(String lclOps, String extOps) {
        final String expLclOps = extOps;
        final String expExtOps = lclOps;
        runTest(lclOps, extOps, expExtOps, expLclOps);
    }

    public static void runTest(String lclOps, String extOps, String expLclOps, String expExtOps) {
        runTest(lclOps, extOps, expLclOps, expExtOps, null);
    }

    public static void runTest(String lclOps, String extOps, String expLclOps, String expExtOps, Map<String, Object> options) {
        TransformerTest.transformSpreadsheet(lclOps, extOps, expExtOps, expLclOps);
    }

    public static void expectBidiError(String lclOps, String extOps) {
        runBidiTest(lclOps, extOps, "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }");
    }

    public static void expectError(String lclOps, String extOps) {
        runTest(lclOps, extOps, "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }");
    }

    public static JSONArray createSeries(String sheets, ISheetIndexOp op) throws JSONException {
        final int[] ints = Helper.createIntArray(sheets);
        final JSONArray a = new JSONArray(ints.length);
        for(int sheet:ints) {
            a.put(op.createOp(sheet));
        }
        return a;
    }

    public static JSONArray createSeries(String sheets, String[] ranges, ISheetIndexRangeOp op) throws JSONException {
        final int[] ints = Helper.createIntArray(sheets);
        final JSONArray a = new JSONArray(ints.length * ranges.length);
        for(int sheet:ints) {
            for(String range:ranges) {
                a.put(op.createOp(sheet, range));
            }
        }
        return a;
    }

    public static JSONArray createSeries(String sheets, String pos, IPositionOp op) throws JSONException {
        final int[] ints = Helper.createIntArray(sheets);
        final JSONArray a = new JSONArray(ints.length);
        for(int sheet:ints) {
            a.put(op.createOp(Integer.toString(sheet) + " " + pos));
        }
        return a;
    }

    public static JSONArray createSeries(String sheets, String pos1, String pos2, I2PositionOp op) throws JSONException {
        final int[] ints = Helper.createIntArray(sheets);
        final JSONArray a = new JSONArray(ints.length);
        for(int sheet:ints) {
            a.put(op.createOp(Integer.toString(sheet) + " " + pos1, Integer.toString(sheet) + " " + pos2));
        }
        return a;
    }

    final static public String URL = "http://example.org";

    // generic attribute sets
    final static public JSONObject ATTRS = Helper.createJSONObject("{ f1: { a1: 10 } }");
    final static public JSONObject ATTRS2 = Helper.createJSONObject("{ f1: { a1: 20 } }");

    // independent attribute sets (will not be reduced)
    final static public JSONObject ATTRS_I1 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a1: 10 } }");
    final static public JSONObject ATTRS_I2 = Helper.createJSONObject("{ f2: { a2: 10 }, f3: { a1: 10 } }");

    // overlapping attribute sets (will be reduced)
    final static public JSONObject ATTRS_O1 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } }");
    final static public JSONObject ATTRS_O2 = Helper.createJSONObject("{ f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 40 } }");

    // the reduced result of ATTRS_Ox
    final static public JSONObject ATTRS_R1 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a2: 20, a3: 30 } }");
    final static public JSONObject ATTRS_R2 = Helper.createJSONObject("{ f3: { a1: 10 }, f2: { a4: 40 } }");

    // attribute sets changing effective column width
    final static public JSONArray COL_ATTRS = Helper.createJSONArray("[{ column: { visible: false } }, { column: { width: 8 } }]");

    // attribute sets changing effective row height
    final static public JSONArray ROW_ATTRS = Helper.createJSONArray("[{ row: { visible: false } }, { row: { height: 10 } }, { row: { filtered: true } }]");

    // globally independent operations
    final static public JSONArray GLOBAL_OPS = Helper.createGlobalOps();

    // generic style sheet operations
    final static public JSONArray STYLESHEET_OPS = Helper.createStyleOps("cell",  "s1");

    // generic auto-style operations
    final static public JSONArray AUTOSTYLE_OPS = Helper.createAutoStyleOps();

    // generic spreadsheet operations
    final static public JSONArray SHEETCOLL_OPS = createInsertSheetOps();

    // standard sort vector for "moveSheets" operations
    final static public String MOVE_SHEETS = "0 6 3 4 2 1 5";

    final static public String MM_MERGE = "merge";

    final static public String MM_HORIZONTAL = "horizontal";

    final static public String MM_VERTICAL = "vertical";

    final static public String MM_UNMERGE = "unmerge";

    final static public JSONObject LCL_ATTRS = Helper.createJSONObject("{ f1: { p1: 10, p2: 20 } }");

    final static public JSONObject EXT_ATTRS_1 = Helper.createJSONObject("{ f2: { p1: 10 } }");

    final static public JSONObject EXT_ATTRS_2 = Helper.createJSONObject("{ f1: { p1: 10, p3: 30 } }");

    final static public JSONObject EXT_ATTRS_3 = Helper.createJSONObject("{ f1: { p1: 10 } }");

    final static public JSONObject EXT_ATTRS_4 = LCL_ATTRS;

    final static public JSONObject LCL_ATTRS_REDUCED = Helper.createJSONObject("{ f1: { p2: 20 } }");

    final static public JSONObject EXT_ATTRS_REDUCED = Helper.createJSONObject("{ f1: { p3: 30 } }");

    final static public JSONArray createInsertSheetOps() {
        try {
            return new JSONArray()
                .put(createInsertSheetOp(1, null, null))
                .put(createDeleteSheetOp(1))
                .put(createMoveSheetOp(1, 2))
                .put(createCopySheetOp(1, 2, null))
                .put(createMoveSheetsOp("1 2 0"))
                .put(createChangeSheetOp(1, null, ATTRS));
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates column operations for multiple sheets and/or intervals
    final static public JSONArray createColOps(String sheets, String _intervals) {
        try {
            final String intervals = _intervals == null ? "B:C E:F" : _intervals;
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, sheet -> createInsertColsOp(sheet, intervals, null, ATTRS)),
                createSeries(sheets, sheet -> createDeleteColsOp(sheet, intervals)),
                createSeries(sheets, sheet -> createChangeColsOp(sheet, intervals, null, ATTRS))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates row operations for multiple sheets and/or intervals
    final static public JSONArray createRowOps(String sheets, String _intervals) {
        try {
            final String intervals = _intervals == null ? "2:3 5:6" : _intervals;
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, sheet -> createInsertRowsOp(sheet, intervals, null, ATTRS)),
                createSeries(sheets, sheet -> createDeleteRowsOp(sheet, intervals)),
                createSeries(sheets, sheet -> createChangeRowsOp(sheet, intervals, null, ATTRS))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    final static public JSONArray createCellOps(String sheets) {
        try {
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, sheet -> createChangeCellsOp(sheet, new JSONObject("{ A1: 42 }"))),
                createSeries(sheets, sheet -> createMoveCellsOp(sheet, "A1:B2", Direction.DOWN)),
                createSeries(sheets, sheet -> createMergeCellsOp(sheet, "A1:B2", null))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates hyperlink operations for multiple sheets and/or ranges
    final static public JSONArray createHlinkOps(String sheets, String..._ranges) {
        try {
            final String[] ranges = _ranges.length == 0 ? new String[]{"A1:B2"} : _ranges;

            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, ranges, (sheet, range) -> createInsertHlinkOp(sheet, range, URL)),
                createSeries(sheets, ranges, (sheet, range) -> createDeleteHlinkOp(sheet, range))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates defined name operations for multiple sheets
    final static public JSONArray createNameOps(String sheets) {
        try {
            return Helper.createJSONArrayFromJSON(
                createInsertNameOp("g", null, null),
                createSeries(sheets, sheet -> createInsertNameOp("n", sheet, null)),
                createDeleteNameOp("g", null),
                createSeries(sheets, sheet -> createDeleteNameOp("n", sheet)),
                createChangeNameOp("g", null, null),
                createSeries(sheets, sheet -> createChangeNameOp("n", sheet, null))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates table range operations for multiple sheets
    final static public JSONArray createTableOps(String sheets) {
        try {
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, sheet -> createInsertTableOp(sheet, "T", "A1:B2", null)),
                createSeries(sheets, sheet -> createDeleteTableOp(sheet, "T")),
                createSeries(sheets, sheet -> createChangeTableOp(sheet, "T", "A1:B2", null)),
                createSeries(sheets, sheet -> createChangeTableColOp(sheet, "T", 1, Helper.createPropsFromAttrs(ATTRS)))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates data validation operations for multiple sheets and/or ranges
    final static public JSONArray createDvRuleOps(String sheets, String _ranges) {
        try {
            final String ranges = _ranges == null ? "A1:B2" : _ranges;
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, sheet -> createInsertDVRuleOp(sheet, 0, ranges, Helper.createPropsFromAttrs(ATTRS))),
                createSeries(sheets, sheet -> createDeleteDVRuleOp(sheet, 0)),
                createSeries(sheets, sheet -> createChangeDVRuleOp(sheet, 0, ranges, Helper.createPropsFromAttrs(ATTRS)))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates conditional formatting operations for multiple sheets
    final static public JSONArray createCfRuleOps(String sheets, String _ranges) {
        try {
            final String ranges = _ranges == null ? "A1:B2" : _ranges;
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, sheet -> createInsertCFRuleOp(sheet, "R1", ranges, Helper.createPropsFromAttrs(ATTRS))),
                createSeries(sheets, sheet -> createDeleteCFRuleOp(sheet, "R1")),
                createSeries(sheets, sheet -> createChangeCFRuleOp(sheet, "R1", ranges, Helper.createPropsFromAttrs(ATTRS)))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates cell note operations for multiple sheets
    final static public JSONArray createNoteOps(String sheets) {
        try {
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, sheet -> createInsertNoteOp(sheet, "A1", "abc", null)),
                createSeries(sheets, sheet -> createDeleteNoteOp(sheet, "A1")),
                createSeries(sheets, sheet -> createChangeNoteOp(sheet, "A1", "abc", null)),
                createSeries(sheets, sheet -> createMoveNotesOp(sheet, "A1", "B2"))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates cell comment operations for multiple sheets
    final static public JSONArray createCommentOps(String sheets) {
        try {
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, sheet -> createInsertCommentOp(sheet, "C3", 0, null)),
                createSeries(sheets, sheet -> createDeleteCommentOp(sheet, "C3", 0)),
                createSeries(sheets, sheet -> createChangeCommentOp(sheet, "C3", 0, null)),
                createSeries(sheets, sheet -> createMoveCommentsOp(sheet, "C3", "D4"))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates drawing operations for multiple sheets
    final static public JSONArray createDrawingOps(String sheets) {
        try {
            final String pos = "0";
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, pos, position -> Helper.createInsertShapeOp(position, ATTRS)),
                createSeries(sheets, pos, position -> Helper.createDeleteDrawingOp(position)),
                createSeries(sheets, pos, position -> Helper.createMoveDrawingFromSheetOp(position, "2"))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates chart operations for multiple sheets
    final static public JSONArray createChartOps(String sheets) {
        try {
            final String pos = "0";
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, pos, position -> Helper.createInsertChSeriesOp(position, 0, ATTRS)),
                createSeries(sheets, pos, position -> Helper.createDeleteChSeriesOp(position, 0)),
                createSeries(sheets, pos, position -> Helper.createChangeChSeriesOp(position, 0, ATTRS)),
                createSeries(sheets, pos, position -> Helper.createDeleteChAxisOp(position, 0)),
                createSeries(sheets, pos, position -> Helper.createChangeChAxisOp(position, 0, ATTRS)),
                createSeries(sheets, pos, position -> Helper.createChangeChGridOp(position, 0, ATTRS)),
                createSeries(sheets, pos, position -> Helper.createChangeChTitleOp(position, 0, ATTRS)),
                createSeries(sheets, pos, position -> Helper.createChangeChTitleOp(position, null, ATTRS)),
                createSeries(sheets, pos, position -> Helper.createChangeLegendOp(position, ATTRS))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    // generates drawing text operations for multiple sheets
    final static public JSONArray createDrawingTextOps(String sheets) {
        try {
            return Helper.createJSONArrayFromJSON(
                createSeries(sheets, "0 6 7", position -> Helper.createInsertTextOp(position, "abc")),
                createSeries(sheets, "0 6 7", position -> Helper.createInsertTabOp(position)),
                createSeries(sheets, "0 6 7", position -> Helper.createInsertHardBreakOp(position)),
                createSeries(sheets, "0 6 7", "0 6 8", (pos1, pos2) -> Helper.createDeleteOp(pos1, pos2)),
                createSeries(sheets, "0 6 7", "0 6 7", (pos1, pos2) -> Helper.createSetAttrsOp(pos1, pos2, ATTRS)),
                createSeries(sheets, "0 6", position -> Helper.createInsertParaOp(position)),
                createSeries(sheets, "0 6 7", position -> Helper.createSplitParaOp(position)),
                createSeries(sheets, "0 6", position -> Helper.createMergeParaOp(position))
            );
        }
        catch(JSONException e) {
            return null;
        }
    }

    final static public String createAllSheetIndexOps(String sheets, boolean skipTables) throws JSONException {
        return Helper.createArrayFromJSON(
            createSeries(sheets, sheet -> createChangeSheetOp(sheet, null, ATTRS)),
            createColOps(sheets, null),
            createRowOps(sheets, null),
            createCellOps(sheets),
            createHlinkOps(sheets),
            createNameOps(sheets),
            createDvRuleOps(sheets, null),
            createCfRuleOps(sheets, null),
            createNoteOps(sheets),
            createCommentOps(sheets),
            createDrawingOps(sheets),
            createChartOps(sheets),
            createDrawingTextOps(sheets),
//          selectOps(sheetArgs)
            skipTables ? null : createTableOps(sheets)
        );
    }

    // generates multiple "changeCells" operations with a value (focus on range addresses)
    final static public String createChangeCellsOps(int sheet, String...rangesArray) throws JSONException {
        final JSONArray changeCellOps = new JSONArray();
        for(String e:rangesArray) {
            final String[] ranges = e.split(" ", -1);
            final JSONObject contents = new JSONObject();
            for(String range:ranges) {
                contents.put(range, 42);
            }
            changeCellOps.put(SheetHelper.createChangeCellsOp(sheet, contents));
        }
        return changeCellOps.toString();
    }

    // generates different note/comment operations (with cell "anchor" property)
    final static public JSONArray createCellAnchorOps(int sheet, String...anchors) throws JSONException {
        final JSONArray ops = new JSONArray();
        for(String anchor:anchors) {
            ops.put(createInsertNoteOp(sheet, anchor, "abc", null));
        }
        for(String anchor:anchors) {
            ops.put(createDeleteNoteOp(sheet, anchor));
        }
        for(String anchor:anchors) {
            ops.put(createChangeNoteOp(sheet, anchor, "abc", null));
        }
        for(String anchor:anchors) {
            ops.put(createInsertCommentOp(sheet, anchor, 0, null));
        }
        for(String anchor:anchors) {
            ops.put(createDeleteCommentOp(sheet, anchor, 0));
        }
        for(String anchor:anchors) {
            ops.put(createChangeCommentOp(sheet, anchor, 0, null));
        }
        return ops;

    }

    // generates different drawing operations with "drawing.anchor" attribute
    final static public JSONArray createDrawingAnchorOps(int sheet, String...anchors) throws JSONException {
        final List<JSONObject> attrsArgs = new ArrayList<JSONObject>();
        for(String anchor:anchors) {
            final JSONObject attrs = new JSONObject();
            final JSONObject drawing = new JSONObject();
            drawing.put(OCKey.ANCHOR.value(), anchor);
            attrs.put(OCKey.DRAWING.value(), drawing);
            attrsArgs.add(attrs);
        }
        final List<JSONObject> propsArgs = new ArrayList<JSONObject>();
        for(JSONObject attrs:attrsArgs) {
            final JSONObject props = new JSONObject();
            props.put(OCKey.ATTRS.value(), OTUtils.cloneJSONObject(attrs));
            propsArgs.add(props);
        }
        final JSONArray ops = new JSONArray();
        for(JSONObject attrs:attrsArgs) {
            ops.put(createInsertNoteOp(sheet, "A1", "abc", OTUtils.cloneJSONObject(attrs)));
        }
        for(JSONObject attrs:attrsArgs) {
            ops.put(createChangeNoteOp(sheet, "A1", "abc", OTUtils.cloneJSONObject(attrs)));
        }
        for(JSONObject props:propsArgs) {
            ops.put(createInsertCommentOp(sheet, "A1", 0, OTUtils.cloneJSONObject(props).toString()));
        }
        for(JSONObject props:propsArgs) {
            ops.put(createChangeCommentOp(sheet, "A1", 0, OTUtils.cloneJSONObject(props).toString()));
        }
        for(JSONObject attrs:attrsArgs) {
            ops.put(Helper.createInsertShapeOp(Integer.toString(sheet) + " 0", OTUtils.cloneJSONObject(attrs)));
        }
        for(JSONObject attrs:attrsArgs) {
            ops.put(Helper.createChangeDrawingOp(Integer.toString(sheet) + " 0", OTUtils.cloneJSONObject(attrs)));
        }
        return ops;
    }

/*
        var anchorArgs = _.rest(arguments);
        var attrsArgs = anchorArgs.map(function (anchor) { return { drawing: { anchor: anchor } }; });
        var propsArgs = attrsArgs.map(function (attrs) { return { attrs: attrs }; });
        return _.flatten([
            opSeries2([insertNote, changeNote], sheet, 'A1', 'abc', attrsArgs),
            opSeries2([insertComment, changeComment], sheet, 'A1', 0, propsArgs),
            opSeries2([insertShape, changeDrawing], sheet, 0, attrsArgs)
        ], true);
    }
/*
 *      var anchorArgs = _.rest(arguments);
        var attrsArgs = anchorArgs.map(function (anchor) { return { drawing: { anchor: anchor } }; });
        var propsArgs = attrsArgs.map(function (attrs) { return { attrs: attrs }; });
        return _.flatten([
            opSeries2([insertNote, changeNote], sheet, 'A1', 'abc', attrsArgs),
            opSeries2([insertComment, changeComment], sheet, 'A1', 0, propsArgs),
            opSeries2([insertShape, changeDrawing], sheet, 0, attrsArgs)
        ], true);

 */


/*
    // generates different drawing operations without "drawing.anchor" attribute
    function drawingNoAnchorOps(sheet) {
        return [
            opSeries2([insertNote, changeNote], sheet, 'A1', 'abc', ATTRS),
            opSeries2([insertShape, changeDrawing], sheet, 0, ATTRS)
        ];
    }
*/

    final static public String changeDrawingOps(int sheet, String...positions) throws JSONException {

        JSONArray ops = new JSONArray();
        for (String position:positions) {
            ops.put(Helper.createChangeDrawingOp(String.valueOf(sheet) + " " + position, ATTRS));
        }

        for (String position:positions) {
            ops.put(Helper.createInsertChSeriesOp(String.valueOf(sheet) + " " + position, 0, ATTRS));
        }

        for (String position:positions) {
            ops.put(Helper.createDeleteChSeriesOp(String.valueOf(sheet) + " " + position, 0));
        }

        for (String position:positions) {
            ops.put(Helper.createChangeChSeriesOp(String.valueOf(sheet) + " " + position, 0, ATTRS));
        }

        for (String position:positions) {
            ops.put(Helper.createDeleteChAxisOp(String.valueOf(sheet) + " " + position, 0));
        }

        for (String position:positions) {
            ops.put(Helper.createChangeChAxisOp(String.valueOf(sheet) + " " + position, 0, ATTRS));
        }

        for (String position:positions) {
            ops.put(Helper.createChangeChGridOp(String.valueOf(sheet) + " " + position, 0, ATTRS));
        }

        for (String position:positions) {
            ops.put(Helper.createChangeChTitleOp(String.valueOf(sheet) + " " + position, 0, ATTRS));
        }

        for (String position:positions) {
            ops.put(Helper.createChangeLegendOp(String.valueOf(sheet) + " " + position, ATTRS));
        }

        for (String position:positions) {
            ops.put(Helper.createInsertTextOp(String.valueOf(sheet) + " " + position, "abc"));
        }

        for (String position:positions) {
            ops.put(Helper.createInsertTabOp(String.valueOf(sheet) + " " + position));
        }

        for (String position:positions) {
            ops.put(Helper.createInsertHardBreakOp(String.valueOf(sheet) + " " + position));
        }

        for (String position:positions) {
            ops.put(Helper.createDeleteOp(String.valueOf(sheet) + " " + position, String.valueOf(sheet) + " " + position));
        }

        for (String position:positions) {
            ops.put(Helper.createSetAttrsOp(String.valueOf(sheet) + " " + position, ATTRS));
        }

        for (String position:positions) {
            ops.put(Helper.createInsertParaOp(String.valueOf(sheet) + " " + position));
        }

        for (String position:positions) {
            ops.put(Helper.createSplitParaOp(String.valueOf(sheet) + " " + position));
        }

        for (String position:positions) {
            ops.put(Helper.createMergeParaOp(String.valueOf(sheet) + " " + position));
        }

        return Helper.createArrayFromJSON(ops);
    }
    /**
     * Creates an "insertNumberFormat" operation.
     */
    final static public JSONObject createInsertNumFmtOp(int id, String code) {
        try {
            return new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_NUMBER_FORMAT.value()).put(OCKey.ID.value(), id).put(OCKey.CODE.value(), code);
        }
        catch(JSONException e) {
            return null;
        }
    };

    /**
     * Creates a "deleteNumberFormat" operation.
     */
    final static public JSONObject createDeleteNumFmtOp(int id) {
        try {
            return new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_NUMBER_FORMAT.value()).put(OCKey.ID.value(), id);
        }
        catch(JSONException e) {
            return null;
        }
    };

    /**
     * Creates an "insertSheet" operation.
     */
    final static public JSONObject createInsertSheetOp(int sheet, String name, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_SHEET.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.SHEET_NAME.value(), name!=null ? name : "Sheet");
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    };

    /**
     * Creates a "deleteSheet" operation.
     */
    final static public JSONObject createDeleteSheetOp(int sheet) throws JSONException {
        return new JSONObject(2).put(OCKey.NAME.value(), OCValue.DELETE_SHEET.value()).put(OCKey.SHEET.value(), sheet);
    };

    /**
     * Creates a "moveSheet" operation.
     */
    final static public JSONObject createMoveSheetOp(int from, int to) throws JSONException {
        return new JSONObject(3).put(OCKey.NAME.value(), OCValue.MOVE_SHEET.value()).put(OCKey.SHEET.value(), from).put(OCKey.TO.value(), to);
    };

    /**
     * Creates a "copySheet" operation.
     */
    final static public JSONObject createCopySheetOp(int from, int to, String name) throws JSONException {
        return new JSONObject(3).put(OCKey.NAME.value(), OCValue.COPY_SHEET.value()).put(OCKey.SHEET.value(), from).put(OCKey.TO.value(), to).put(OCKey.SHEET_NAME.value(), name!=null ? name : "sheet");
    };

    /**
     * Creates a "moveSheets" operation.
     */
    final static public JSONObject createMoveSheetsOp(String sheets) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.MOVE_SHEETS.value());
        Helper.putPosition(op, OCKey.SHEETS, sheets);
        return op;
    }

    /**
     * Creates a "changeSheet" operation.
     */
    final static public JSONObject createChangeSheetOp(int sheet, String name, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_SHEET.value()).put(OCKey.SHEET.value(), sheet);
        if(name!=null) {
            op.put(OCKey.SHEET_NAME.value(), name);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates an "insertColumns" operation.
     */
    final static public JSONObject createInsertColsOp(int sheet, String intervals) throws JSONException {
        return createInsertColsOp(sheet, intervals, null, null);
    }

    final static public JSONObject createInsertColsOp(int sheet, String intervals, String style, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_COLUMNS.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INTERVALS.value(), intervals);
        if(style!=null) {
            op.put(OCKey.S.value(), style);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "deleteColumns" operation.
     */
    final static public JSONObject createDeleteColsOp(int sheet, String intervals) throws JSONException {
        return new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_COLUMNS.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INTERVALS.value(), intervals);
    }

    /**
     * Creates a "changeColumns" operation.
     */
    final static public JSONObject createChangeColsOp(int sheet, String intervals) throws JSONException {
        return createChangeColsOp(sheet, intervals, null, null);
    }

    final static public JSONObject createChangeColsOp(int sheet, String intervals, String style, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_COLUMNS.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INTERVALS.value(), intervals);
        if(style!=null) {
            op.put(OCKey.S.value(), style);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates an "insertRows" operation.
     */
    final static public JSONObject createInsertRowsOp(int sheet, String intervals) throws JSONException {
        return createInsertRowsOp(sheet, intervals, null, null);
    }

    final static public JSONObject createInsertRowsOp(int sheet, String intervals, String style, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_ROWS.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INTERVALS.value(), intervals);
        if(style!=null) {
            op.put(OCKey.S.value(), style);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "deleteRows" operation.
     */
    final static public JSONObject createDeleteRowsOp(int sheet, String intervals) throws JSONException {
        return new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_ROWS.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INTERVALS.value(), intervals);
    }

    /**
     * Creates a "changeRows" operation.
     */
    final static public JSONObject createChangeRowsOp(int sheet, String intervals) throws JSONException {
        return createChangeRowsOp(sheet, intervals, null, null);
    }

    final static public JSONObject createChangeRowsOp(int sheet, String intervals, String style, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_ROWS.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INTERVALS.value(), intervals);
        if(style!=null) {
            op.put(OCKey.S.value(), style);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "changeCells" operation.
     */
    final static public JSONObject createChangeCellsOp(int sheet, JSONObject contents) throws JSONException {
        return new JSONObject(3).put(OCKey.NAME.value(), OCValue.CHANGE_CELLS.value()).put(OCKey.SHEET.value(),  sheet).put(OCKey.CONTENTS.value(), contents);
    }

    /**
     * Creates a "moveCells" operation.
     */
    final static public JSONObject createMoveCellsOp(int sheet, String range, Direction dir) throws JSONException {
        return new JSONObject(4).put(OCKey.NAME.value(), OCValue.MOVE_CELLS.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.RANGE.value(),  range).put(OCKey.DIR.value(), dir.value());
    }

    /**
     * Creates a "mergeCells" operation.
     */
    final static public JSONObject createMergeCellsOp(int sheet, String ranges, String type) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.MERGE_CELLS.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.RANGES.value(),  ranges);
        if(type!=null) {
            op.put(OCKey.TYPE.value(), type);
        }
        return op;
    }

    /**
     * Creates an "insertHyperlink" operation.
     */
    final static public JSONObject createInsertHlinkOp(int sheet, String ranges, String url) throws JSONException {
        return new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_HYPERLINK.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.RANGES.value(),  ranges).put(OCKey.URL.value(), url!=null ? url : "example.org");
    }

    /**
     * Creates a "deleteHyperlink" operation.
     */
    final static public JSONObject createDeleteHlinkOp(int sheet, String ranges) throws JSONException {
        return new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_HYPERLINK.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.RANGES.value(), ranges);
    }

    /**
     * Creates an "insertName" operation.
     */
    final static public JSONObject createInsertNameOp(String label, Integer sheet, String props) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_NAME.value()).put(OCKey.LABEL.value(), label);
        if(sheet!=null) {
            op.put(OCKey.SHEET.value(), sheet);
        }
        if(props!=null) {
            Helper.mergeProps(op, props);
        }
        return op;
    }

    /**
     * Creates a "deleteName" operation.
     */
    final static public JSONObject createDeleteNameOp(String label, Integer sheet) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.DELETE_NAME.value()).put(OCKey.LABEL.value(), label);
        if(sheet!=null) {
            op.put(OCKey.SHEET.value(), sheet);
        }
        return op;
    }

    /**
     * Creates a "changeName" operation.
     */
    final static public JSONObject createChangeNameOp(String label, Integer sheet, String props) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_NAME.value()).put(OCKey.LABEL.value(), label);
        if(sheet!=null) {
            op.put(OCKey.SHEET.value(), sheet);
        }
        if(props!=null) {
            Helper.mergeProps(op, props);
        }
        return op;
    }

    /**
     * Creates an "insertTable" operation.
     */
    final static public JSONObject createInsertTableOp(int sheet, String table, String range, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.INSERT_TABLE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.TABLE.value(), table);
        if(range!=null) {
            op.put(OCKey.RANGE.value(), range);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "deleteTable" operation.
     */
    final static public JSONObject createDeleteTableOp(int sheet, String table) throws JSONException {
        return new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_TABLE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.TABLE.value(), table);
    }

    /**
     * Creates a "changeTable" operation.
     */
    final static public JSONObject createChangeTableOp(int sheet, String table, String range, String props) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.CHANGE_TABLE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.TABLE.value(), table);
        if(range!=null) {
            op.put(OCKey.RANGE.value(), range);
        }
        if(props!=null) {
            Helper.mergeProps(op, props);
        }
        return op;
    };

    /**
     * Creates a "changeTableColumn" operation.
     */
    final static public JSONObject createChangeTableColOp(int sheet, String table, int col, String props) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.CHANGE_TABLE_COLUMN.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.TABLE.value(), table).put(OCKey.COL.value(), col);
        if(props!=null) {
            Helper.mergeProps(op, props);
        }
        return op;
    };

    /**
     * Creates an "insertDVRule" operation.
     */
    final static public JSONObject createInsertDVRuleOp(int sheet, int index, String ranges, String props) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.INSERT_DV_RULE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INDEX.value(), index);
        if(ranges!=null) {
            op.put(OCKey.RANGES.value(), ranges);
        }
        if(props!=null) {
            Helper.mergeProps(op, props);
        }
        return op;
    };

    /**
     * Creates a "deleteDVRule" operation.
     */
    final static public JSONObject createDeleteDVRuleOp(int sheet, int index) throws JSONException {
        return new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_DV_RULE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INDEX.value(), index);
    }

    /**
     * Creates a "changeDVRule" operation.
     */
    final static public JSONObject createChangeDVRuleOp(int sheet, int index, String ranges, String props) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.CHANGE_DV_RULE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INDEX.value(), index);
        if(ranges!=null) {
            op.put(OCKey.RANGES.value(), ranges);
        }
        if(props!=null) {
            Helper.mergeProps(op, props);
        }
        return op;
    };

    /**
     * Creates a "changeDVRule" operation.
     */
    final static public JSONObject createChangeDVRuleOp(int sheet, String index, String ranges, String props) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.CHANGE_DV_RULE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.INDEX.value(), index);
        if(ranges!=null) {
            op.put(OCKey.RANGES.value(), ranges);
        }
        if(props!=null) {
            Helper.mergeProps(op, props);
        }
        return op;
    };

    /**
     * Creates an "insertCFRule" operation.
     */
    final static public JSONObject createInsertCFRuleOp(int sheet, String id, String ranges, String props) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.INSERT_CF_RULE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ID.value(), id);
        if(ranges!=null) {
            op.put(OCKey.RANGES.value(), ranges);
        }
        if(props!=null) {
            Helper.mergeProps(op, props);
        }
        return op;
    }

    /**
     * Creates a "deleteCFRule" operation.
     */
    final static public JSONObject createDeleteCFRuleOp(int sheet, String id) throws JSONException {
        return new JSONObject(5).put(OCKey.NAME.value(), OCValue.DELETE_CF_RULE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ID.value(), id);
    }

    /**
     * Creates a "changeCFRule" operation.
     */
    final static public JSONObject createChangeCFRuleOp(int sheet, String id, String ranges, String props) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.CHANGE_CF_RULE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ID.value(), id);
        if(ranges!=null) {
            op.put(OCKey.RANGES.value(), ranges);
        }
        if(props!=null) {
            Helper.mergeProps(op, props);
        }
        return op;
    }

    /**
     * Creates an "insertNote" operation.
     */
    final static public JSONObject createInsertNoteOp(int sheet, String anchor, String text, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.INSERT_NOTE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ANCHOR.value(), anchor);
        if(text!=null) {
            op.put(OCKey.TEXT.value(), text);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "deleteNote" operation.
     */
    final static public JSONObject createDeleteNoteOp(int sheet, String anchor) throws JSONException {
        return new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_NOTE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ANCHOR.value(), anchor);
    }

    /**
     * Creates a "changeNote" operation.
     */
    final static public JSONObject createChangeNoteOp(int sheet, String anchor, String text, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(5).put(OCKey.NAME.value(), OCValue.CHANGE_NOTE.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ANCHOR.value(), anchor);
        if(text!=null) {
            op.put(OCKey.TEXT.value(), text);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "moveNotes" operation.
     */
    final static public JSONObject createMoveNotesOp(int sheet, String from, String to) throws JSONException {
        return new JSONObject(4).put(OCKey.NAME.value(), OCValue.MOVE_NOTES.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.FROM.value(), from).put(OCKey.TO.value(), to);
    }

    /**
     * Creates an "insertComment" operation.
     */
    final static public JSONObject createInsertCommentOp(int sheet, String anchor, int index, String props) throws JSONException {
        final JSONObject op =  new JSONObject(5).put(OCKey.NAME.value(), OCValue.INSERT_COMMENT.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ANCHOR.value(), anchor).put(OCKey.INDEX.value(), index);
        if(props!=null) {
            Helper.mergeProps(op,  props);
        }
        return op;
    }

    /**
     * Creates a "deleteComment" operation.
     */
    final static public JSONObject createDeleteCommentOp(int sheet, String anchor, int index) throws JSONException {
        return new JSONObject(4).put(OCKey.NAME.value(), OCValue.DELETE_COMMENT.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ANCHOR.value(), anchor).put(OCKey.INDEX.value(), index);
    }

    /**
     * Creates an "changeComment" operation.
     */
    final static public JSONObject createChangeCommentOp(int sheet, String anchor, int index, String props) throws JSONException {
        final JSONObject op =  new JSONObject(5).put(OCKey.NAME.value(), OCValue.CHANGE_COMMENT.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ANCHOR.value(), anchor).put(OCKey.INDEX.value(), index);
        if(props!=null) {
            Helper.mergeProps(op,  props);
        }
        return op;
    }

    /**
     * Creates a "moveComments" operation.
     */
    final static public JSONObject createMoveCommentsOp(int sheet, String from, String to) throws JSONException {
        return new JSONObject(4).put(OCKey.NAME.value(), OCValue.MOVE_COMMENTS.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.FROM.value(), from).put(OCKey.TO.value(), to);
    }
}
