/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.junit.jupiter.api.Test;

public class InsertNumberFormat {

/*
        describe('"insertNumberFormat" and "insertNumberFormat"', function () {
            it('should set equal identifier and format code to "removed"', function () {
                testRunner.runTest(insertNumFmt(184, '0'), insertNumFmt(184, '0'), [], []);
            });
            it('should fail for different identifiers', function () {
                testRunner.expectError(insertNumFmt(184, '0'), insertNumFmt(185, '0'));
            });
            it('should fail for different format codes', function () {
                testRunner.expectError(insertNumFmt(184, '0'), insertNumFmt(184, '0.00'));
            });
        });
*/

    @Test
    public void insertNumberFormatInsertNumberFormat01() {
        SheetHelper.runTest(SheetHelper.createInsertNumFmtOp(184, "0").toString(), SheetHelper.createInsertNumFmtOp(184, "0").toString(), "[]", "[]", null);
    }

    @Test
    public void insertNumberFormatInsertNumberFormat02() {
        SheetHelper.runTest(SheetHelper.createInsertNumFmtOp(184, "0").toString(), SheetHelper.createInsertNumFmtOp(185, "0").toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

    @Test
    public void insertNumberFormatInsertNumberFormat03() {
        SheetHelper.runTest(SheetHelper.createInsertNumFmtOp(184, "0").toString(), SheetHelper.createInsertNumFmtOp(184, "0.00").toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

/*
        describe('"insertNumberFormat" and "deleteNumberFormat"', function () {
            it('should set "deleteNumberFormat" to "removed"', function () {
                testRunner.runBidiTest(insertNumFmt(184, '0'), deleteNumFmt(184), null, []);
            });
            it('should skip different identifiers', function () {
                testRunner.runBidiTest(deleteNumFmt(184, '0'), deleteNumFmt(185));
            });
        });
*/
    @Test
    public void insertNumberFormatDeleteNumberFormat01() {
        SheetHelper.runBidiTest(SheetHelper.createInsertNumFmtOp(184, "0").toString(), SheetHelper.createDeleteNumFmtOp(184).toString(), SheetHelper.createInsertNumFmtOp(184, "0").toString(), "[]", null);
    }

    @Test
    public void deleteNumberFormatDeleteNumberFormat01() {
        SheetHelper.runBidiTest(SheetHelper.createInsertNumFmtOp(184, "0").toString(), SheetHelper.createDeleteNumFmtOp(185).toString());
    }
}
