/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

public class ChangeTable {

/*
    describe('"changeTable" and "changeTable"', function () {
        it('should skip different tables', function () {
            testRunner.runTest(changeTable(1, null, 'A1:D4'), changeTable(1, 'T2', 'E5:H8'));
            testRunner.runTest(changeTable(1, 'T1', 'A1:D4'), changeTable(1, null, 'E5:H8'));
            testRunner.runTest(changeTable(1, 'T1', 'A1:D4'), changeTable(1, 'T2', 'E5:H8'));
            testRunner.runTest(changeTable(1, null, 'A1:D4'), changeTable(2, null, 'E5:H8'));
        });
        it('should fail for same table in different sheets', function () {
            testRunner.expectError(changeTable(1, 'T1', 'A1:D4'), changeTable(2, 'T1', 'A1:D4'));
        });
        it('should not process independent attributes', function () {
            testRunner.runTest(changeTable(1, 'T1', { attrs: ATTRS_I1 }), changeTable(1, 'T1', { attrs: ATTRS_I2 }));
        });
        it('should reduce attribute sets', function () {
            testRunner.runTest(
                changeTable(1, 'T1', { attrs: ATTRS_O1 }), changeTable(1, 'T1', { attrs: ATTRS_O2 }),
                changeTable(1, 'T1', { attrs: ATTRS_R1 }), changeTable(1, 'T1', { attrs: ATTRS_R2 })
            );
        });
        it('should reduce table range', function () {
            testRunner.runTest(
                changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I1 }), changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I2 }),
                changeTable(1, 'T1',          { attrs: ATTRS_I1 }), changeTable(1, 'T1',          { attrs: ATTRS_I2 })
            );
            testRunner.runTest(
                changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I1 }), changeTable(1, 'T1', 'B2:E5', { attrs: ATTRS_I2 }),
                changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I1 }), changeTable(1, 'T1',          { attrs: ATTRS_I2 })
            );
            testRunner.runBidiTest(changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I1 }), changeTable(1, 'T1', { attrs: ATTRS_I2 }));
        });
        it('should fail to change different tables to the same name', function () {
            testRunner.runTest(changeTable(1, 'T1', { newName: 'T3' }), changeTable(1, 'T1', { newName: 'T3' }), [], []);
            testRunner.runTest(changeTable(1, 'T1', { newName: 'T3' }), changeTable(2, 'T2', { newName: 'T4' }));
            testRunner.expectError(changeTable(1, 'T1', { newName: 'T3' }), changeTable(2, 'T2', { newName: 'T3' }));
            testRunner.expectError(changeTable(1, 'T1', { newName: 'T3' }), changeTable(2, 'T2', { newName: 't3' }));
        });
        it('should update table name in operations', function () {
            testRunner.runBidiTest(
                changeTable(1, 'T1', { newName: 'T2' }), changeTable(1, 'T1', 'A1:D4'),
                changeTable(1, 'T1', { newName: 'T2' }), changeTable(1, 'T2', 'A1:D4')
            );
            testRunner.runTest(
                changeTable(1, 'T1', { newName: 'T2' }), changeTable(1, 'T1', 'A1:D4', { newName: 'T3' }),
                changeTable(1, 'T3', { newName: 'T2' }), changeTable(1, 'T2', 'A1:D4')
            );
            testRunner.runTest(
                changeTable(1, 'T1', { newName: 'T2' }), changeTable(1, 'T1', { newName: 'T3' }),
                changeTable(1, 'T3', { newName: 'T2' }), []
            );
        });
        it('should remove entire operation if nothing will change', function () {
            testRunner.runTest(changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS }), changeTable(1, 'T1', 'B2:E5', { attrs: ATTRS2 }), null,                         []);
            testRunner.runTest(changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS }), changeTable(1, 'T1', 'B2:E5', { attrs: ATTRS }), changeTable(1, 'T1', 'A1:D4'), []);
            testRunner.runTest(changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS }), changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS }), [],                            []);
        });
    });
*/

    @Test
    public void changeTableChangeTable01() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runTest(
        //  changeTable(1, null, 'A1:D4'),
        //  changeTable(1, 'T2', 'E5:H8'));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T2", "E5:H8", null).toString()
        );
    }

    @Test
    public void changeTableChangeTable02() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runTest(
        //  changeTable(1, 'T1', 'A1:D4'),
        //  changeTable(1, null, 'E5:H8'));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, null, "E5:H8", null).toString()
        );
    }

    @Test
    public void changeTableChangeTable03() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runTest(
        //  changeTable(1, 'T1', 'A1:D4'),
        //  changeTable(1, 'T2', 'E5:H8'));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T2", "E5:H8", null).toString()
        );
    }

    @Test
    public void changeTableChangeTable04() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runTest(
        //  changeTable(1, null, 'A1:D4'),
        //  changeTable(2, null, 'E5:H8'));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(2, null, "E5:H8", null).toString()
        );
    }

    @Test
    public void changeTableChangeTable05() throws JSONException {
        // Result: Should fail for same table in different sheets.
        // testRunner.expectError(
        //  changeTable(1, 'T1', 'A1:D4'),
        //  changeTable(2, 'T1', 'A1:D4'));
        SheetHelper.expectError(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(2, "T1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeTableChangeTable06() throws JSONException {
        // Result: Should not process independent attributes.
        // testRunner.runTest(
        //  changeTable(1, 'T1', { attrs: ATTRS_I1 }),
        //  changeTable(1, 'T1', { attrs: ATTRS_I2 }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I2.toString() + "}").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTable07() throws JSONException {
        // Result: Should reduce attribute sets.
        // testRunner.runTest(
        //  changeTable(1, 'T1', { attrs: ATTRS_O1 }),
        //  changeTable(1, 'T1', { attrs: ATTRS_O2 }),
        //  changeTable(1, 'T1', { attrs: ATTRS_R1 }),
        //  changeTable(1, 'T1', { attrs: ATTRS_R2 }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_O1.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_O2.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_R1.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_R2.toString() + "}").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTable08() throws JSONException {
        // Result: Should reduce table range.
        // testRunner.runTest(
        //  changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I1 }),
        //  changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I2 }),
        //  changeTable(1, 'T1',          { attrs: ATTRS_I1 }),
        //  changeTable(1, 'T1',          { attrs: ATTRS_I2 }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS_I2.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I2.toString() + "}").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTable09() throws JSONException {
        // Result: Should reduce table range.
        // testRunner.runTest(
        //  changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I1 }),
        //  changeTable(1, 'T1', 'B2:E5', { attrs: ATTRS_I2 }),
        //  changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I1 }),
        //  changeTable(1, 'T1',          { attrs: ATTRS_I2 }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "B2:E5", new JSONObject("{ attrs: " + SheetHelper.ATTRS_I2.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I2.toString() + "}").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTable10() throws JSONException {
        // Result: Should reduce table range.
        // testRunner.runBidiTest(
        //  changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS_I1 }),
        //  changeTable(1, 'T1', { attrs: ATTRS_I2 }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + "}").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ attrs: " + SheetHelper.ATTRS_I2.toString() + "}").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTable11() throws JSONException {
        // Result: Should fail to change different tables to the same name.
        // testRunner.runTest(
        //  changeTable(1, 'T1', { newName: 'T3' }),
        //  changeTable(2, 'T2', { newName: 'T4' }));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T3' }").toString()).toString(),
            SheetHelper.createChangeTableOp(2, "T2", null, new JSONObject("{ newName: 'T4' }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTable12() throws JSONException {
        // Result: Should fail to change different tables to the same name.
        // testRunner.expectError(
        //  changeTable(1, 'T1', { newName: 'T3' }),
        //  changeTable(2, 'T2', { newName: 'T3' }));
        SheetHelper.expectError(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T3' }").toString()).toString(),
            SheetHelper.createChangeTableOp(2, "T2", null, new JSONObject("{ newName: 'T3' }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTable13() throws JSONException {
        // Result: Should fail to change different tables to the same name.
        // testRunner.expectError(
        //  changeTable(1, 'T1', { newName: 'T3' }),
        //  changeTable(2, 'T2', { newName: 't3' }));
        SheetHelper.expectError(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T3' }").toString()).toString(),
            SheetHelper.createChangeTableOp(2, "T2", null, new JSONObject("{ newName: 't3' }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTable14() throws JSONException {
        // Result: Should update table name in operations.
        // testRunner.runBidiTest(
        //  changeTable(1, 'T1', { newName: 'T2' }),
        //  changeTable(1, 'T1', 'A1:D4'),
        //  changeTable(1, 'T1', { newName: 'T2' }),
        //  changeTable(1, 'T2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T2' }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T2' }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T2", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeTableChangeTable15() throws JSONException {
        // Result: Should update table name in operations.
        // testRunner.runTest(
        //  changeTable(1, 'T1', { newName: 'T2' }),
        //  changeTable(1, 'T1', 'A1:D4', { newName: 'T3' }),
        //  changeTable(1, 'T3', { newName: 'T2' }),
        //  changeTable(1, 'T2', 'A1:D4'));
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T2' }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ newName: 'T3' }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T3", null, new JSONObject("{ newName: 'T2' }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T2", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeTableChangeTable16() throws JSONException {
        // Result: Should update table name in operations.
        // testRunner.runTest(
        //  changeTable(1, 'T1', { newName: 'T2' }),
        //  changeTable(1, 'T1', { newName: 'T3' }),
        //  changeTable(1, 'T3', { newName: 'T2' }),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T2' }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T3' }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T3", null, new JSONObject("{ newName: 'T2' }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeTableChangeTable17() throws JSONException {
        // Result: Should remove entire operation if nothing will change.
        // testRunner.runTest(
        //  changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS }),
        //  changeTable(1, 'T1', 'B2:E5', { attrs: ATTRS2 }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "B2:E5", new JSONObject("{ attrs: " + SheetHelper.ATTRS2.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeTableChangeTable18() throws JSONException {
        // Result: Should remove entire operation if nothing will change.
        // testRunner.runTest(
        //  changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS }),
        //  changeTable(1, 'T1', 'B2:E5', { attrs: ATTRS }),
        //  changeTable(1, 'T1', 'A1:D4'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "B2:E5", new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString(),
            "[]"
        );
    }

    @Test
    public void changeTableChangeTable19() throws JSONException {
        // Result: Should remove entire operation if nothing will change.
        // testRunner.runTest(
        //  changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS }),
        //  changeTable(1, 'T1', 'A1:D4', { attrs: ATTRS }),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            "[]",
            "[]"
        );
    }

/*
    describe('"changeTable" and "changeTableColumn"', function () {
        it('should skip different tables', function () {
            testRunner.runBidiTest(changeTable(1, null, 'A1:D4'), changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
            testRunner.runBidiTest(changeTable(1, 'T1', 'A1:D4'), changeTableCol(1, null, 0, { attrs: ATTRS }));
            testRunner.runBidiTest(changeTable(1, 'T1', 'A1:D4'), changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
            testRunner.runBidiTest(changeTable(1, null, 'A1:D4'), changeTableCol(2, null, 0, { attrs: ATTRS }));
        });
        it('should fail to change table range', function () {
            testRunner.expectBidiError(changeTable(1, null, 'A1:D4'), changeTableCol(1, null, 0, { attrs: ATTRS }));
            testRunner.expectBidiError(changeTable(1, 'T1', 'A1:D4'), changeTableCol(1, 'T1', 0, { attrs: ATTRS }));
        });
        it('should fail to change table name to used name', function () {
            testRunner.expectBidiError(changeTable(1, 'T2', { newName: 'T1' }), changeTableCol(0, 'T1', 0, { attrs: ATTRS }));
            testRunner.expectBidiError(changeTable(1, 'T2', { newName: 't1' }), changeTableCol(0, 'T1', 0, { attrs: ATTRS }));
        });
        it('should update table name in operations', function () {
            -testRunner.runBidiTest(changeTable(1, 'T1', { newName: 'T3' }), changeTableCol(1, 'T1', 0, { attrs: ATTRS }), null, changeTableCol(1, 'T3', 0, { attrs: ATTRS }));
        });
    });
*/

    @Test
    public void changeTableChangeTableColumn01() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  changeTable(1, null, 'A1:D4'),
        //  changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTableColumn02() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  changeTable(1, 'T1', 'A1:D4'),
        //  changeTableCol(1, null, 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, null, 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTableColumn03() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  changeTable(1, 'T1', 'A1:D4'),
        //  changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, "T2", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTableColumn04() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  changeTable(1, null, 'A1:D4'),
        //  changeTableCol(2, null, 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(2, null, 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTableColumn05() throws JSONException {
        // Result: Should fail to change table range.
        // testRunner.expectBidiError(
        //  changeTable(1, 'T1', 'A1:D4'),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTableColumn06() throws JSONException {
        // Result: Should fail to change table name to used name.
        // testRunner.expectBidiError(
        //  changeTable(1, 'T2', { newName: 'T1' }),
        //  changeTableCol(0, 'T1', 0, { attrs: ATTRS }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T1' }").toString()).toString(),
            SheetHelper.createChangeTableColOp(0, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTableColumn07() throws JSONException {
        // Result: Should fail to change table name to used name.
        // testRunner.expectBidiError(
        //  changeTable(1, 'T2', { newName: 't1' }),
        //  changeTableCol(0, 'T1', 0, { attrs: ATTRS }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 't1' }").toString()).toString(),
            SheetHelper.createChangeTableColOp(0, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

    @Test
    public void changeTableChangeTableColumn08() throws JSONException {
        // Result: Should update table name in operations.
        // testRunner.runBidiTest(
        //  changeTable(1, 'T1', { newName: 'T3' }),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }),
        //  null,
        //  changeTableCol(1, 'T3', 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T3' }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString(),
            SheetHelper.createChangeTableOp(1, "T1", null, new JSONObject("{ newName: 'T3' }").toString()).toString(),
            SheetHelper.createChangeTableColOp(1, "T3", 0, new JSONObject("{ attrs: " + SheetHelper.ATTRS.toString() + " }").toString()).toString()
        );
    }

}
