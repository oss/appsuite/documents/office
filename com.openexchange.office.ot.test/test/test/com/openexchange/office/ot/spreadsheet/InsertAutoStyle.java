/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertAutoStyle {

/*
    describe('"insertAutoStyle" and "insertColumns"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(insertAutoStyle('a2', {}), insertCols(1, 'A', 'a0'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), insertCols(1, 'A', 'a1'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), insertCols(1, 'A', 'a2'), insertAutoStyle('a2', {}, OTIS), insertCols(1, 'A', 'a3'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), insertCols(1, 'A', 'a3'), insertAutoStyle('a2', {}, OTIS), insertCols(1, 'A', 'a4'));
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(insertAutoStyle('a2', {}), insertCols(1, 'A', 'b2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), insertCols(1, 'A', 'a2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), insertCols(1, 'A', 'b2'));
        });
    });
*/
    @Test
    public void insertAutoStyleInsertColumns01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createInsertColsOp(1, "A", "a0", null).toString());
    }

    @Test
    public void insertAutoStyleInsertColumns02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createInsertColsOp(1, "A", "a1", null).toString());
    }

    @Test
    public void insertAutoStyleInsertColumns03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createInsertColsOp(1, "A", "a2", null).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createInsertColsOp(1, "A", "a3", null).toString(), null);
    }

    @Test
    public void insertAutoStyleInsertColumns04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createInsertColsOp(1, "A", "a3", null).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createInsertColsOp(1, "A", "a4", null).toString(), null);
    }

/*
    describe('"insertAutoStyle" and "changeColumns"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeCols(1, 'A', 'a0'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeCols(1, 'A', 'a1'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeCols(1, 'A', 'a2'), insertAutoStyle('a2', {}, OTIS), changeCols(1, 'A', 'a3'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeCols(1, 'A', 'a3'), insertAutoStyle('a2', {}, OTIS), changeCols(1, 'A', 'a4'));
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(insertAutoStyle('a2', {}), changeCols(1, 'A', 'b2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeCols(1, 'A', 'a2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeCols(1, 'A', 'b2'));
        });
    });
*/
    @Test
    public void insertAutoStyleChangeColumns01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeColsOp(1, "A", "a0", null).toString());
    }

    @Test
    public void insertAutoStyleChangeColumns02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeColsOp(1, "A", "a1", null).toString());
    }

    @Test
    public void insertAutoStyleChangeColumns03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeColsOp(1, "A", "a2", null).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createChangeColsOp(1, "A", "a3", null).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeColumns04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeColsOp(1, "A", "a3", null).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createChangeColsOp(1, "A", "a4", null).toString(), null);
    }

/*
    describe('"insertAutoStyle" and "insertRows"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(insertAutoStyle('a2', {}), insertRows(1, '1', 'a0'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), insertRows(1, '1', 'a1'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), insertRows(1, '1', 'a2'), insertAutoStyle('a2', {}, OTIS), insertRows(1, '1', 'a3'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), insertRows(1, '1', 'a3'), insertAutoStyle('a2', {}, OTIS), insertRows(1, '1', 'a4'));
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(insertAutoStyle('a2', {}), insertRows(1, '1', 'b2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), insertRows(1, '1', 'a2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), insertRows(1, '1', 'b2'));
        });
    });
*/
    @Test
    public void insertAutoStyleInsertRows01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createInsertRowsOp(1, "1", "a0", null).toString());
    }

    @Test
    public void insertAutoStyleInsertRows02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createInsertRowsOp(1, "1", "a1", null).toString());
    }

    @Test
    public void insertAutoStyleInsertRows03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createInsertRowsOp(1, "1", "a2", null).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createInsertRowsOp(1, "1", "a3", null).toString(), null);
    }

    @Test
    public void insertAutoStyleInsertRows04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createInsertRowsOp(1, "1", "a3", null).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createInsertRowsOp(1, "1", "a4", null).toString(), null);
    }

/*
    describe('"insertAutoStyle" and "changeRows"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeRows(1, '1', 'a0'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeRows(1, '1', 'a1'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeRows(1, '1', 'a2'), insertAutoStyle('a2', {}, OTIS), changeRows(1, '1', 'a3'));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeRows(1, '1', 'a3'), insertAutoStyle('a2', {}, OTIS), changeRows(1, '1', 'a4'));
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(insertAutoStyle('a2', {}), changeRows(1, '1', 'b2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeRows(1, '1', 'a2'));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeRows(1, '1', 'b2'));
        });
    });
*/
    @Test
    public void insertAutoStyleChangeRows01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeRowsOp(1, "1", "a0", null).toString());
    }

    @Test
    public void insertAutoStyleChangeRows02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeRowsOp(1, "1", "a1", null).toString());
    }

    @Test
    public void insertAutoStyleChangeRows03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeRowsOp(1, "1", "a2", null).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createChangeRowsOp(1, "1", "a3", null).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeRows04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeRowsOp(1, "1", "a3", null).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createChangeRowsOp(1, "1", "a4", null).toString(), null);
    }

/*
    describe('"insertAutoStyle" and "changeCells"', function () {
        var OTIS = { otIndexShift: true };
        it('should transform the auto-style identifier', function () {
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeCells(1, { A1: { s: 'a0' } }));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeCells(1, { A1: { s: 'a1' } }));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeCells(1, { A1: { s: 'a2' } }), insertAutoStyle('a2', {}, OTIS), changeCells(1, { A1: { s: 'a3' } }));
            testRunner.runBidiTest(insertAutoStyle('a2', {}), changeCells(1, { A1: { s: 'a3' } }), insertAutoStyle('a2', {}, OTIS), changeCells(1, { A1: { s: 'a4' } }));
        });
        it('should fail for invalid auto-style identifier', function () {
            testRunner.expectBidiError(insertAutoStyle('a2', {}), changeCells(1, { A1: { s: 'b2' } }));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeCells(1, { A1: { s: 'a2' } }));
            testRunner.expectBidiError(insertAutoStyle('b2', {}), changeCells(1, { A1: { s: 'b2' } }));
        });
    });
*/
    @Test
    public void insertAutoStyleChangeCells01() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a0' } }")).toString());
    }

    @Test
    public void insertAutoStyleChangeCells02() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a1' } }")).toString());
    }

    @Test
    public void insertAutoStyleChangeCells03() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a2' } }")).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a3' } }")).toString(), null);
    }

    @Test
    public void insertAutoStyleChangeCells04() throws JSONException {
        SheetHelper.runBidiTest(Helper.createInsertAutoStyleOp("a2", null, null).toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a3' } }")).toString(), Helper.createInsertAutoStyleOp("a2", null, "{ otIndexShift: true }").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { s: 'a4' } }")).toString(), null);
    }
}
