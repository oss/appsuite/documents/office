/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertInsertDrawing {

    /*
     * describe('"insertDrawing" and "insertDrawing"', function () {
            it('should skip different sheets', function () {
                testRunner.runTest(insertShape(1, 0, ATTRS), insertShape(0, 1, ATTRS));
            });
            it('should shift positions of top-level shapes', function () {
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, 0, ATTRS), insertShape(1, 3, ATTRS), insertShape(1, 0, ATTRS));
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, 1, ATTRS), insertShape(1, 3, ATTRS), insertShape(1, 1, ATTRS));
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, 2, ATTRS), insertShape(1, 3, ATTRS), insertShape(1, 2, ATTRS));
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, 3, ATTRS), insertShape(1, 2, ATTRS), insertShape(1, 4, ATTRS));
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, 4, ATTRS), insertShape(1, 2, ATTRS), insertShape(1, 5, ATTRS));
            });
            it('should shift position of external embedded shape', function () {
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, '0 4', ATTRS), null, insertShape(1, '0 4', ATTRS));
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, '1 4', ATTRS), null, insertShape(1, '1 4', ATTRS));
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, '2 4', ATTRS), null, insertShape(1, '3 4', ATTRS));
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, '3 4', ATTRS), null, insertShape(1, '4 4', ATTRS));
                testRunner.runTest(insertShape(1, 2, ATTRS), insertShape(1, '4 4', ATTRS), null, insertShape(1, '5 4', ATTRS));
            });
            it('should shift position of local embedded shape', function () {
                testRunner.runTest(insertShape(1, '2 4', ATTRS), insertShape(1, 0, ATTRS), insertShape(1, '3 4', ATTRS));
                testRunner.runTest(insertShape(1, '2 4', ATTRS), insertShape(1, 1, ATTRS), insertShape(1, '3 4', ATTRS));
                testRunner.runTest(insertShape(1, '2 4', ATTRS), insertShape(1, 2, ATTRS), insertShape(1, '3 4', ATTRS));
                testRunner.runTest(insertShape(1, '2 4', ATTRS), insertShape(1, 3, ATTRS), insertShape(1, '2 4', ATTRS));
                testRunner.runTest(insertShape(1, '2 4', ATTRS), insertShape(1, 4, ATTRS), insertShape(1, '2 4', ATTRS));
            });
            it('should shift positions of embedded siblings', function () {
                testRunner.runTest(insertShape(1, '2 2', ATTRS), insertShape(1, '2 0', ATTRS), insertShape(1, '2 3', ATTRS), insertShape(1, '2 0', ATTRS));
                testRunner.runTest(insertShape(1, '2 2', ATTRS), insertShape(1, '2 1', ATTRS), insertShape(1, '2 3', ATTRS), insertShape(1, '2 1', ATTRS));
                testRunner.runTest(insertShape(1, '2 2', ATTRS), insertShape(1, '2 2', ATTRS), insertShape(1, '2 3', ATTRS), insertShape(1, '2 2', ATTRS));
                testRunner.runTest(insertShape(1, '2 2', ATTRS), insertShape(1, '2 3', ATTRS), insertShape(1, '2 2', ATTRS), insertShape(1, '2 4', ATTRS));
                testRunner.runTest(insertShape(1, '2 2', ATTRS), insertShape(1, '2 4', ATTRS), insertShape(1, '2 2', ATTRS), insertShape(1, '2 5', ATTRS));
            });
            it('should skip independent embedded shapes', function () {
                testRunner.runTest(insertShape(1, '2 2', ATTRS), opSeries2(insertShape, 1, ['0 0', '0 2', '0 4', '4 0', '4 2', '4 4'], ATTRS));
            });
        });
        */

    @Test
    public void insertDrawingInsertDrawing01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runTest(
        //  insertShape(1, 0, ATTRS),
        //  insertShape(0, 1, ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 0", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("0 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing02() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, 0, ATTRS),
        //  insertShape(1, 3, ATTRS),
        //  insertShape(1, 0, ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 0", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 0", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing03() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, 1, ATTRS),
        //  insertShape(1, 3, ATTRS),
        //  insertShape(1, 1, ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing04() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, 3, ATTRS),
        //  insertShape(1, 2, ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing05() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, 3, ATTRS),
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, 4, ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 4", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing06() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, 4, ATTRS),
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, 5, ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 5", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing07() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, '0 4', ATTRS),
        //  null,
        //  insertShape(1, '0 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 0 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 0 4", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing08() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, '1 4', ATTRS),
        //  null,
        //  insertShape(1, '1 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 1 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 1 4", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing09() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, '2 4', ATTRS),
        //  null,
        //  insertShape(1, '3 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3 4", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing10() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, '3 4', ATTRS),
        //  null,
        //  insertShape(1, '4 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 4 4", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing11() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, '4 4', ATTRS),
        //  null,
        //  insertShape(1, '5 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 4 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 5 4", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing12() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  insertShape(1, '2 4', ATTRS),
        //  insertShape(1, 0, ATTRS),
        //  insertShape(1, '3 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 0", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 0", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing13() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  insertShape(1, '2 4', ATTRS),
        //  insertShape(1, 1, ATTRS),
        //  insertShape(1, '3 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing14() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  insertShape(1, '2 4', ATTRS),
        //  insertShape(1, 2, ATTRS),
        //  insertShape(1, '3 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing15() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  insertShape(1, '2 4', ATTRS),
        //  insertShape(1, 3, ATTRS),
        //  insertShape(1, '2 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing16() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  insertShape(1, '2 4', ATTRS),
        //  insertShape(1, 4, ATTRS),
        //  insertShape(1, '2 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 4", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing17() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runTest(
        //  insertShape(1, '2 2', ATTRS),
        //  insertShape(1, '2 0', ATTRS),
        //  insertShape(1, '2 3', ATTRS),
        //  insertShape(1, '2 0', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 0", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 3", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 0", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing18() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runTest(
        //  insertShape(1, '2 2', ATTRS),
        //  insertShape(1, '2 1', ATTRS),
        //  insertShape(1, '2 3', ATTRS),
        //  insertShape(1, '2 1', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 1", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 3", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing19() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runTest(
        //  insertShape(1, '2 2', ATTRS),
        //  insertShape(1, '2 2', ATTRS),
        //  insertShape(1, '2 3', ATTRS),
        //  insertShape(1, '2 2', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 3", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing20() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runTest(
        //  insertShape(1, '2 2', ATTRS),
        //  insertShape(1, '2 3', ATTRS),
        //  insertShape(1, '2 2', ATTRS),
        //  insertShape(1, '2 4', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 3", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing21() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runTest(
        //  insertShape(1, '2 2', ATTRS),
        //  insertShape(1, '2 4', ATTRS),
        //  insertShape(1, '2 2', ATTRS),
        //  insertShape(1, '2 5', ATTRS));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 4", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createInsertShapeOp("1 2 5", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertDrawingInsertDrawing22() throws JSONException {
        // Result: Should skip independent embedded shapes.
        // testRunner.runTest(
        //  insertShape(1, '2 2', ATTRS),
        //  opSeries2(insertShape, 1, ['0 0', '0 2', '0 4', '4 0', '4 2', '4 4'], ATTRS));;
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 2 2", SheetHelper.ATTRS).toString(),
            Helper.createArrayFromJSON(
                Helper.createInsertShapeOp("1 0 0", SheetHelper.ATTRS),
                Helper.createInsertShapeOp("1 0 2", SheetHelper.ATTRS),
                Helper.createInsertShapeOp("1 0 4", SheetHelper.ATTRS),
                Helper.createInsertShapeOp("1 4 0", SheetHelper.ATTRS),
                Helper.createInsertShapeOp("1 4 2", SheetHelper.ATTRS),
                Helper.createInsertShapeOp("1 4 4", SheetHelper.ATTRS)
            )
        );
    }
}
