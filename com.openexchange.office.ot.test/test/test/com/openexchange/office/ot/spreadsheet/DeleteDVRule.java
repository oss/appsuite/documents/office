/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DeleteDVRule {

/*
    describe('"deleteDVRule" and "deleteDVRule"', function () {
        it('should skip different sheets', function () {
            testRunner.runTest(deleteDVRule(1, 1), deleteDVRule(2, 1));
        });
*/

    @Test
    public void deleteDVRuleDeleteDVRule01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runTest(
        //  deleteDVRule(1, 1),
        //  deleteDVRule(2, 1));
        SheetHelper.runTest(
            SheetHelper.createDeleteDVRuleOp(1, 1).toString(),
            SheetHelper.createDeleteDVRuleOp(2, 1).toString()
        );
    }

/*
        it('should shift rule index', function () {
            testRunner.runTest(deleteDVRule(1, 2), deleteDVRule(1, 0), deleteDVRule(1, 1), deleteDVRule(1, 0));
            testRunner.runTest(deleteDVRule(1, 2), deleteDVRule(1, 1), deleteDVRule(1, 1), deleteDVRule(1, 1));
            testRunner.runTest(deleteDVRule(1, 2), deleteDVRule(1, 2), [],                 []);
            testRunner.runTest(deleteDVRule(1, 2), deleteDVRule(1, 3), deleteDVRule(1, 2), deleteDVRule(1, 2));
            -testRunner.runTest(deleteDVRule(1, 2), deleteDVRule(1, 4), deleteDVRule(1, 2), deleteDVRule(1, 3));
        });

     });
*/
    @Test
    public void deleteDVRuleDeleteDVRule02() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runTest(
        //  deleteDVRule(1, 2),
        //  deleteDVRule(1, 0),
        //  deleteDVRule(1, 1),
        //  deleteDVRule(1, 0));
        SheetHelper.runTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 0).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 1).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 0).toString()
        );
    }

    @Test
    public void deleteDVRuleDeleteDVRule03() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runTest(
        //  deleteDVRule(1, 2),
        //  deleteDVRule(1, 1),
        //  deleteDVRule(1, 1),
        //  deleteDVRule(1, 1));
        SheetHelper.runTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 1).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 1).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 1).toString()
        );
    }

    @Test
    public void deleteDVRuleDeleteDVRule04() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runTest(
        //  deleteDVRule(1, 2),
        //  deleteDVRule(1, 2),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteDVRuleDeleteDVRule05() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runTest(
        //  deleteDVRule(1, 2), deleteDVRule(1, 3), deleteDVRule(1, 2), deleteDVRule(1, 2));
        SheetHelper.runTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteDVRuleDeleteDVRule06() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runTest(
        //  deleteDVRule(1, 2),
        //  deleteDVRule(1, 4),
        //  deleteDVRule(1, 2),
        //  deleteDVRule(1, 3));
        SheetHelper.runTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 4).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 3).toString()
        );
    }

/*
    describe('"deleteDVRule" and "changeDVRule"', function () {
        it('should skip different sheets', function () {
            testRunner.runBidiTest(deleteDVRule(1, 1), changeDVRule(2, 1, 'A1:D4'));
        });
        it('should shift rule index', function () {
            testRunner.runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 0, 'A1:D4'), null, changeDVRule(1, 0, 'A1:D4'));
            testRunner.runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 1, 'A1:D4'), null, changeDVRule(1, 1, 'A1:D4'));
            testRunner.runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 2, 'A1:D4'), null, []);
            testRunner.runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 3, 'A1:D4'), null, changeDVRule(1, 2, 'A1:D4'));
            testRunner.runBidiTest(deleteDVRule(1, 2), changeDVRule(1, 4, 'A1:D4'), null, changeDVRule(1, 3, 'A1:D4'));
        });
    });
*/
    @Test
    public void deleteDVRuleChangeDVRule01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runBidiTest(
        //  deleteDVRule(1, 1),
        //  changeDVRule(2, 1, 'A1:D4'));
        SheetHelper.runTest(
            SheetHelper.createDeleteDVRuleOp(1, 1).toString(),
            SheetHelper.createChangeDVRuleOp(2, 1, "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteDVRuleChangeDVRule02() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runBidiTest(
        //  deleteDVRule(1, 2),
        //  changeDVRule(1, 0, 'A1:D4'),
        //  null,
        //  changeDVRule(1, 0, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createChangeDVRuleOp(1, 0, "A1:D4", null).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createChangeDVRuleOp(1, 0, "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteDVRuleChangeDVRule03() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runBidiTest(
        //  deleteDVRule(1, 2),
        //  changeDVRule(1, 1, 'A1:D4'),
        //  null,
        //  changeDVRule(1, 1, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, "A1:D4", null).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteDVRuleChangeDVRule04() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runBidiTest(
        //  deleteDVRule(1, 2),
        //  changeDVRule(1, 2, 'A1:D4'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createChangeDVRuleOp(1, 2, "A1:D4", null).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            "[]"
        );
    }

    @Test
    public void deleteDVRuleChangeDVRule05() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runBidiTest(
        //  deleteDVRule(1, 2),
        //  changeDVRule(1, 3, 'A1:D4'),
        //  null,
        //  changeDVRule(1, 2, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createChangeDVRuleOp(1, 3, "A1:D4", null).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createChangeDVRuleOp(1, 2, "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteDVRuleChangeDVRule06() throws JSONException {
        // Result: Should shift rule index.
        // testRunner.runBidiTest(
        //  deleteDVRule(1, 2),
        //  changeDVRule(1, 4, 'A1:D4'),
        //  null,
        //  changeDVRule(1, 3, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createChangeDVRuleOp(1, 4, "A1:D4", null).toString(),
            SheetHelper.createDeleteDVRuleOp(1, 2).toString(),
            SheetHelper.createChangeDVRuleOp(1, 3, "A1:D4", null).toString()
        );
    }

}
