/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.OCKey;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeChangeComment {

    // overlapping attribute sets (will be reduced)
    private final JSONObject    attrs01                 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } }");
    private final JSONObject    attrs02                 = Helper.createJSONObject("{ f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 40 } }");
    // the reduced result of ATTRS_Ox
    private final JSONObject    attrsR1                 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a2: 20, a3: 30 } }");
    private final JSONObject    attrsR2                 = Helper.createJSONObject("{ f3: { a1: 10 }, f2: { a4: 40 } }");
    // independent attribute sets (will not be reduced) ATTRS_I
    private final JSONObject    attrsI1                 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a1: 10 } }");
    private final JSONObject    attrsI2                 = Helper.createJSONObject("{ f2: { a2: 10 }, f3: { a1: 10 } }");

    /*
     * describe('"changeComment" and "changeComment"', function () {
            it('should skip different sheets, anchors, and indexes', function () {
                testRunner.runTest(changeComment(1, 'A1', 1, { text: 'c1' }), changeComment(2, 'A1', 1, { text: 'c1' }));
                testRunner.runTest(changeComment(1, 'A1', 1, { text: 'c1' }), changeComment(1, 'B2', 1, { text: 'c1' }));
                testRunner.runTest(changeComment(1, 'A1', 1, { text: 'c1' }), changeComment(1, 'A1', 2, { text: 'c1' }));
            });
            it('should not process independent attributes', function () {
                testRunner.runTest(changeComment(1, 'A1', 1, { attrs: ATTRS_I1 }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2 }));
            });
            it('should reduce attribute sets', function () {
                testRunner.runTest(
                    changeComment(1, 'A1', 1, { attrs: ATTRS_O1 }), changeComment(1, 'A1', 1, { attrs: ATTRS_O2 }),
                    changeComment(1, 'A1', 1, { attrs: ATTRS_R1 }), changeComment(1, 'A1', 1, { attrs: ATTRS_R2 })
                );
            });
            it('should reduce comment text', function () {
                testRunner.runTest(
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, text: 'c1' }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2, text: 'c1' }),
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1 }),             changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
                );
                testRunner.runTest(
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, text: 'c1' }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2, text: 'c2' }),
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, text: 'c1' }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
                );
                testRunner.runBidiTest(changeComment(1, 'A1', 1, { attrs: ATTRS_I1, text: 'c1' }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2 }));
            });
            it('should reduce mentions', function () {
                testRunner.runTest(
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, mentions: [1] }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2, mentions: [1] }),
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1 }),                changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
                );
                testRunner.runTest(
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, mentions: [1] }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2, mentions: [2] }),
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, mentions: [1] }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
                );
                testRunner.runBidiTest(changeComment(1, 'A1', 1, { attrs: ATTRS_I1, mentions: [1] }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2 }));
            });
            it('should reduce "done" flag"', function () {
                testRunner.runTest(
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: true }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2, done: true }),
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1 }),             changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
                );
                testRunner.runTest(
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: true }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2, done: false }),
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: true }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
                );
                testRunner.runTest(
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: false }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2, done: true }),
                    changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: false }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
                );
                testRunner.runBidiTest(changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: true }), changeComment(1, 'A1', 1, { attrs: ATTRS_I2 }));
            });
            it('should remove entire operation if nothing will change', function () {
                testRunner.runTest(changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c1' }), changeComment(1, 'A1', 1, { attrs: ATTRS2, text: 'c2' }), null,                                      []);
                testRunner.runTest(changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c1' }), changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c2' }),  changeComment(1, 'A1', 1, { text: 'c1' }), []);
                -testRunner.runTest(changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c1' }), changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c1' }),  [],                                        []);
            });
        });
    */

    @Test
    public void changeChangeComment01() throws JSONException {
        // Result: Should skip different sheets, anchors, and indexes.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { text: 'c1' }),
        //  changeComment(2, 'A1', 1, { text: 'c1' }));
        JSONObject attrs = Helper.createJSONObject("{ text: 'c1' }");
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrs).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrs).toString()
        );
    }

    @Test
    public void changeChangeComment02() throws JSONException {
        // Result: Should skip different sheets, anchors, and indexes.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { text: 'c1' }),
        //  changeComment(1, 'B2', 1, { text: 'c1' }));
        JSONObject attrs = Helper.createJSONObject("{ text: 'c1' }");
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrs).toString(),
            Helper.createChangeCommentOp(1, "B1", 1, attrs).toString()
        );
    }

    @Test
    public void changeChangeComment03() throws JSONException {
        // Result: Should skip different sheets, anchors, and indexes.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { text: 'c1' }),
        //  changeComment(1, 'A1', 2, { text: 'c1' }));
        JSONObject attrs = Helper.createJSONObject("{ text: 'c1' }");
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrs).toString(),
            Helper.createChangeCommentOp(1, "A1", 2, attrs).toString()
        );
    }

    @Test
    public void changeChangeComment04() throws JSONException {
        // Result: Should not process independent attributes.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1 }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 }));
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment05() throws JSONException {
        // Result: Should reduce attribute sets.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_O1 }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_O2 }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_R1 }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_R2 })
        //  );
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrs01).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrs02).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsR1).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsR2).toString()
        );
    }

    @Test
    public void changeChangeComment06() throws JSONException {
        // Result: Should reduce comment text.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, text: 'c1' }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2, text: 'c1' }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1 }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
        //  );
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1, "C1").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2, "C1").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment07() throws JSONException {
        // Result: Should reduce comment text.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, text: 'c1' }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2, text: 'c2' }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, text: 'c1' }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
        // );
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1, "C1").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2, "C2").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1, "C1").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment08() throws JSONException {
        // Result: Should reduce comment text.
        // testRunner.runBidiTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, text: 'c1' }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 }));
        SheetHelper.runBidiTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1, "C1").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment09() throws JSONException {
        // Result: Should reduce mentions.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, mentions: [1] }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2, mentions: [1] }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1 }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
        //  );
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.MENTIONS.value(), new JSONArray().put(1)).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).put(OCKey.MENTIONS.value(), new JSONArray().put(1)).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment10() throws JSONException {
        // Result: Should reduce mentions.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, mentions: [1] }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2, mentions: [2] }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, mentions: [1] }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
        //  );
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.MENTIONS.value(), new JSONArray().put(1)).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).put(OCKey.MENTIONS.value(), new JSONArray().put(2)).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.MENTIONS.value(), new JSONArray().put(1)).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment11() throws JSONException {
        // Result: Should reduce mentions.
        // testRunner.runBidiTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, mentions: [1] }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 }));
        SheetHelper.runBidiTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.MENTIONS.value(), new JSONArray().put(1)).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment12() throws JSONException {
        // Result: Should reduce "done" flag.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: true }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2, done: true }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1 }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
        //  );
        SheetHelper.runBidiTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.DONE.value(), true).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).put(OCKey.DONE.value(), true).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment13() throws JSONException {
        // Result: Should reduce "done" flag.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: true }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2, done: false }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: true }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
        //  );
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.DONE.value(), true).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).put(OCKey.DONE.value(), false).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.DONE.value(), true).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment14() throws JSONException {
        // Result: Should reduce "done" flag.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: false }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2, done: true }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: false }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 })
        //  );
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.DONE.value(), false).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).put(OCKey.DONE.value(), true).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.DONE.value(), false).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment15() throws JSONException {
        // Result: Should reduce "done" flag.
        // testRunner.runBidiTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I1, done: true }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS_I2 }));
        SheetHelper.runBidiTest(
            Helper.createChangeCommentOp(1, "A1", 1, attrsI1).put(OCKey.DONE.value(), true).toString(),
            Helper.createChangeCommentOp(1, "A1", 1, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeComment16() throws JSONException {
        // Result: Should remove entire operation if nothing will change.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c1' }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS2, text: 'c2' }),
        //  null,
        //  []);
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, SheetHelper.ATTRS, "c1").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, SheetHelper.ATTRS2, "c2").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, SheetHelper.ATTRS, "c1").toString(),
            "[]"
        );
    }

    @Test
    public void changeChangeComment17() throws JSONException {
        // Result: Should remove entire operation if nothing will change.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c1' }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c2' }),
        //  changeComment(1, 'A1', 1, { text: 'c1' }),
        //  []);
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, SheetHelper.ATTRS, "c1").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, SheetHelper.ATTRS, "c2").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, null, "c1").toString(),
            "[]"
        );
    }

    @Test
    public void changeChangeComment18() throws JSONException {
        // Result: Should remove entire operation if nothing will change.
        // testRunner.runTest(
        //  changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c1' }),
        //  changeComment(1, 'A1', 1, { attrs: ATTRS, text: 'c1' }),
        //  [],
        //  []);
        SheetHelper.runTest(
            Helper.createChangeCommentOp(1, "A1", 1, SheetHelper.ATTRS, "c1").toString(),
            Helper.createChangeCommentOp(1, "A1", 1, SheetHelper.ATTRS, "c1").toString(),
            "[]",
            "[]"
        );
    }
}