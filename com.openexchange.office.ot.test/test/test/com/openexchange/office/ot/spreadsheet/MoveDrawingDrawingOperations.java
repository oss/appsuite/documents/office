/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class MoveDrawingDrawingOperations {

    /*
     * describe('"moveDrawing" and other drawing operations', function () {
            it('should not process shapes in different sheets', function () {
                testRunner.runTest(moveDrawing(0, 0, 4), changeDrawingOps(1, 0, 1, 2));
            });
            it('should shift positions for top-level shape', function () {
                testRunner.runBidiTest(
                    moveDrawing(1, 2, 4), changeDrawingOps(1, 1, 2, 3, 4, 5),
                    moveDrawing(1, 2, 4), changeDrawingOps(1, 1, 4, 2, 3, 5)
                );
                testRunner.runBidiTest(
                    moveDrawing(1, 4, 2), changeDrawingOps(1, 1, 2, 3, 4, 5),
                    moveDrawing(1, 4, 2), changeDrawingOps(1, 1, 3, 4, 2, 5)
                );
                testRunner.runBidiTest(
                    moveDrawing(1, 3, 3), changeDrawingOps(1, 1, 2, 3, 4, 5)
                );
            });
            it('should shift position of external embedded shape', function () {
                testRunner.runBidiTest(
                    moveDrawing(1, 2, 4), changeDrawingOps(1, '1 5', '2 5', '3 5', '4 5', '5 5'),
                    moveDrawing(1, 2, 4), changeDrawingOps(1, '1 5', '4 5', '2 5', '3 5', '5 5')
                );
                testRunner.runBidiTest(
                    moveDrawing(1, 4, 2), changeDrawingOps(1, '1 5', '2 5', '3 5', '4 5', '5 5'),
                    moveDrawing(1, 4, 2), changeDrawingOps(1, '1 5', '3 5', '4 5', '2 5', '5 5')
                );
                testRunner.runBidiTest(
                    moveDrawing(1, 3, 3), changeDrawingOps(1, '1 5', '2 5', '3 5', '4 5', '5 5')
                );
            });
            it('should not shift position of parent shapes', function () {
                testRunner.runBidiTest(
                    moveDrawing(1, '1 2', '1 4'), [changeDrawingOps(1, 0, 2), changeDrawing(1, 1)]
                );
            });
            it('should shift positions for embedded siblings', function () {
                testRunner.runBidiTest(
                    moveDrawing(1, '1 2', '1 4'), changeDrawingOps(1, '1 1', '1 2', '1 3', '1 4', '1 5'),
                    moveDrawing(1, '1 2', '1 4'), changeDrawingOps(1, '1 1', '1 4', '1 2', '1 3', '1 5')
                );
                testRunner.runBidiTest(
                    moveDrawing(1, '1 4', '1 2'), changeDrawingOps(1, '1 1', '1 2', '1 3', '1 4', '1 5'),
                    moveDrawing(1, '1 4', '1 2'), changeDrawingOps(1, '1 1', '1 3', '1 4', '1 2', '1 5')
                );
                testRunner.runBidiTest(
                    moveDrawing(1, '1 3', '1 3'), changeDrawingOps(1, '1 1', '1 2', '1 3', '1 4', '1 5')
                );
            });
        });
    */

    @Test
    public void moveDrawingDrawingOperations01() throws JSONException {
        // Result: Should not process shapes in different sheets.
        // testRunner.runTest(
        //  moveDrawing(0, 0, 4),
        //  changeDrawingOps(1, 0, 1, 2));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("0 0", "0 4").toString(),
            SheetHelper.changeDrawingOps(1, "0", "1", "2").toString()
        );
    }

    @Test
    public void moveDrawingDrawingOperations02() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  moveDrawing(1, 2, 4),
        //  changeDrawingOps(1, 1, 2, 3, 4, 5),
        //  moveDrawing(1, 2, 4),
        //  changeDrawingOps(1, 1, 4, 2, 3, 5));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            SheetHelper.changeDrawingOps(1, "1", "2", "3", "4", "5").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            SheetHelper.changeDrawingOps(1, "1", "4", "2", "3", "5").toString()
        );
    }

    @Test
    public void moveDrawingDrawingOperations03() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  moveDrawing(1, 4, 2),
        //  changeDrawingOps(1, 1, 2, 3, 4, 5),
        //  moveDrawing(1, 4, 2),
        //  changeDrawingOps(1, 1, 3, 4, 2, 5));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            SheetHelper.changeDrawingOps(1, "1", "2", "3", "4", "5").toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            SheetHelper.changeDrawingOps(1, "1", "3", "4", "2", "5").toString()
        );
    }

    @Test
    public void moveDrawingDrawingOperations04() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  moveDrawing(1, 3, 3),
        //  changeDrawingOps(1, 1, 2, 3, 4, 5));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 3", "1 3").toString(),
            SheetHelper.changeDrawingOps(1, "1", "2", "3", "4", "5").toString()
        );
    }

    @Test
    public void moveDrawingDrawingOperations05() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runBidiTest(
        //  moveDrawing(1, 2, 4),
        //  changeDrawingOps(1, '1 5', '2 5', '3 5', '4 5', '5 5'),
        //  moveDrawing(1, 2, 4),
        //  changeDrawingOps(1, '1 5', '4 5', '2 5', '3 5', '5 5'));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            SheetHelper.changeDrawingOps(1, "1 5", "2 5", "3 5", "4 5", "5 5").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            SheetHelper.changeDrawingOps(1, "1 5", "4 5", "2 5", "3 5", "5 5").toString()
        );
    }

    @Test
    public void moveDrawingDrawingOperations06() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runBidiTest(
        //  moveDrawing(1, 4, 2),
        //  changeDrawingOps(1, '1 5', '2 5', '3 5', '4 5', '5 5'),
        //  moveDrawing(1, 4, 2),
        //  changeDrawingOps(1, '1 5', '3 5', '4 5', '2 5', '5 5'));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            SheetHelper.changeDrawingOps(1, "1 5", "2 5", "3 5", "4 5", "5 5").toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            SheetHelper.changeDrawingOps(1, "1 5", "3 5", "4 5", "2 5", "5 5").toString()
        );
    }

    @Test
    public void moveDrawingDrawingOperations07() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runBidiTest(
        //  moveDrawing(1, 3, 3),
        //  changeDrawingOps(1, '1 5', '2 5', '3 5', '4 5', '5 5'));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 3", "1 3").toString(),
            SheetHelper.changeDrawingOps(1, "1 5", "2 5", "3 5", "4 5", "5 5").toString()
        );
    }

    @Test
    public void moveDrawingDrawingOperations08() throws JSONException {
        // Result: Should not shift position of parent shapes.
        // testRunner.runBidiTest(
        //  moveDrawing(1, '1 2', '1 4'),
        //  [changeDrawingOps(1, 0, 2), changeDrawing(1, 1)]);
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createArrayFromJSON(
                Helper.createJSONObject(SheetHelper.changeDrawingOps(1, "0", "2")),
                Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS)
            )
        );
    }

    @Test
    public void moveDrawingDrawingOperations09() throws JSONException {
        // Result: Should shift positions for embedded siblings.
        // testRunner.runBidiTest(
        //  moveDrawing(1, '1 2', '1 4'),
        //  changeDrawingOps(1, '1 1', '1 2', '1 3', '1 4', '1 5'),
        //  moveDrawing(1, '1 2', '1 4'),
        //  changeDrawingOps(1, '1 1', '1 4', '1 2', '1 3', '1 5'));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            SheetHelper.changeDrawingOps(1, "1 1", "1 2", "1 3", "1 4", "1 5"),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            SheetHelper.changeDrawingOps(1, "1 1", "1 4", "1 2", "1 3", "1 5")
        );
    }

    @Test
    public void moveDrawingDrawingOperations10() throws JSONException {
        // Result: Should shift positions for embedded siblings.
        // testRunner.runBidiTest(
        //  moveDrawing(1, '1 4', '1 2'),
        //  changeDrawingOps(1, '1 1', '1 2', '1 3', '1 4', '1 5'),
        //  moveDrawing(1, '1 4', '1 2'),
        //  changeDrawingOps(1, '1 1', '1 3', '1 4', '1 2', '1 5'));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            SheetHelper.changeDrawingOps(1, "1 1", "1 2", "1 3", "1 4", "1 5"),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            SheetHelper.changeDrawingOps(1, "1 1", "1 3", "1 4", "1 2", "1 5")
        );
    }

    @Test
    public void moveDrawingDrawingOperations11() throws JSONException {
        // Result: Should shift positions for embedded siblings.
        // testRunner.runBidiTest(
        //  moveDrawing(1, '1 3', '1 3'),
        //  changeDrawingOps(1, '1 1', '1 2', '1 3', '1 4', '1 5'));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 3", "1 3").toString(),
            SheetHelper.changeDrawingOps(1, "1 1", "1 2", "1 3", "1 4", "1 5")
        );
    }
}
