/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DeleteRows {

/*
    describe('"deleteRows" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), [
                GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS, colOps(1), nameOps(1),
                deleteTable(1, 'T'), changeTableCol(1, 'T', 0, { attrs: ATTRS }),
                deleteDVRule(1, 0), deleteCFRule(1, 'R1'),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/

    @Test
    public void deleteRowsIndependentOperations01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteRowsOp(1, "3").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.AUTOSTYLE_OPS,
                SheetHelper.createColOps("1", null),
                SheetHelper.createNameOps("1"),
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createChangeTableColOp(1, "T", 0, "{ attrs: " + SheetHelper.ATTRS + " }"),
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

/*
    describe('"deleteRows" and "deleteRows"', function () {
        it('should transform single intervals #1', function () {
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '1'), deleteRows(1, '2:3'), deleteRows(1, '1'));
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '2'), deleteRows(1, '2:3'), deleteRows(1, '2'));
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '3'), deleteRows(1, '3'),   []);
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '4'), deleteRows(1, '3'),   []);
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '5'), deleteRows(1, '3:4'), deleteRows(1, '3'));
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '6'), deleteRows(1, '3:4'), deleteRows(1, '4'));
        });
        it('should transform single intervals #2', function () {
            testRunner.runTest(deleteRows(1, '4'), deleteRows(1, '1:2'), deleteRows(1, '2'), deleteRows(1, '1:2'));
            testRunner.runTest(deleteRows(1, '4'), deleteRows(1, '2:3'), deleteRows(1, '2'), deleteRows(1, '2:3'));
            testRunner.runTest(deleteRows(1, '4'), deleteRows(1, '3:4'), [],                 deleteRows(1, '3'));
            testRunner.runTest(deleteRows(1, '4'), deleteRows(1, '4:5'), [],                 deleteRows(1, '4'));
            testRunner.runTest(deleteRows(1, '4'), deleteRows(1, '5:6'), deleteRows(1, '4'), deleteRows(1, '4:5'));
            testRunner.runTest(deleteRows(1, '4'), deleteRows(1, '6:7'), deleteRows(1, '4'), deleteRows(1, '5:6'));
        });
        it('should transform single intervals #3', function () {
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '1:2'), deleteRows(1, '1:2'), deleteRows(1, '1:2'));
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '2:3'), deleteRows(1, '2'),   deleteRows(1, '2'));
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '3:4'), [],                   []);
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '4:5'), deleteRows(1, '3'),   deleteRows(1, '3'));
            testRunner.runTest(deleteRows(1, '3:4'), deleteRows(1, '5:6'), deleteRows(1, '3:4'), deleteRows(1, '3:4'));
        });
        it('should transform interval lists', function () {
            testRunner.runTest(deleteRows(1, '4 7'), deleteRows(1, '1 3'),  deleteRows(1, '2 5'), deleteRows(1, '1 3'));
            testRunner.runTest(deleteRows(1, '4 7'), deleteRows(1, '2 4'),  deleteRows(1, '5'),   deleteRows(1, '2'));
            testRunner.runTest(deleteRows(1, '4 7'), deleteRows(1, '3 5'),  deleteRows(1, '3 5'), deleteRows(1, '3:4'));
            testRunner.runTest(deleteRows(1, '4 7'), deleteRows(1, '4 6'),  deleteRows(1, '5'),   deleteRows(1, '5'));
            testRunner.runTest(deleteRows(1, '4 7'), deleteRows(1, '5 7'),  deleteRows(1, '4'),   deleteRows(1, '4'));
            testRunner.runTest(deleteRows(1, '4 7'), deleteRows(1, '6 8'),  deleteRows(1, '4 6'), deleteRows(1, '5:6'));
            testRunner.runTest(deleteRows(1, '4 7'), deleteRows(1, '7 9'),  deleteRows(1, '4'),   deleteRows(1, '7'));
            testRunner.runTest(deleteRows(1, '4 7'), deleteRows(1, '8 10'), deleteRows(1, '4 7'), deleteRows(1, '6 8'));
        });
        it('should not transform intervals in different sheets', function () {
            -testRunner.runTest(deleteRows(1, '3'), deleteRows(2, '4'));
        });
    });
*/

    @Test
    public void deleteRowsdeleteRows01() throws JSONException {
        // Result: Should transform single intervals #1.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '1'),
        //  deleteRows(1, '2:3'),
        //  deleteRows(1, '1'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "1").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString(),
            SheetHelper.createDeleteRowsOp(1, "1").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows02() throws JSONException {
        // Result: Should transform single intervals #1.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '2'),
        //  deleteRows(1, '2:3'),
        //  deleteRows(1, '2'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "2").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString(),
            SheetHelper.createDeleteRowsOp(1, "2").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows03() throws JSONException {
        // Result: Should transform single intervals #1.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '3'),
        //  deleteRows(1, '3'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void deleteRowsdeleteRows04() throws JSONException {
        // Result: Should transform single intervals #1.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '4'),
        //  deleteRows(1, '3'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void deleteRowsdeleteRows05() throws JSONException {
        // Result: Should transform single intervals #1.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '5'),
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '3'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows06() throws JSONException {
        // Result: Should transform single intervals #1.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '6'),
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '4'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "6").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "4").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows07() throws JSONException {
        // Result: Should transform single intervals #2.
        // testRunner.runTest(
        //  deleteRows(1, '4'),
        //  deleteRows(1, '1:2'),
        //  deleteRows(1, '2'),
        //  deleteRows(1, '1:2'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "1:2").toString(),
            SheetHelper.createDeleteRowsOp(1, "2").toString(),
            SheetHelper.createDeleteRowsOp(1, "1:2").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows08() throws JSONException {
        // Result: Should transform single intervals #2.
        // testRunner.runTest(
        //  deleteRows(1, '4'),
        //  deleteRows(1, '2:3'),
        //  deleteRows(1, '2'),
        //  deleteRows(1, '2:3'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString(),
            SheetHelper.createDeleteRowsOp(1, "2").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows09() throws JSONException {
        // Result: Should transform single intervals #2.
        // testRunner.runTest(
        //  deleteRows(1, '4'),
        //  deleteRows(1, '3:4'),
        //  [],
        //  deleteRows(1, '3'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            "[]",
            SheetHelper.createDeleteRowsOp(1, "3").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows10() throws JSONException {
        // Result: Should transform single intervals #2.
        // testRunner.runTest(
        //  deleteRows(1, '4'),
        //  deleteRows(1, '4:5'),
        //  [],
        //  deleteRows(1, '4'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "4:5").toString(),
            "[]",
            SheetHelper.createDeleteRowsOp(1, "4").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows11() throws JSONException {
        // Result: Should transform single intervals #2.
        // testRunner.runTest(
        //  deleteRows(1, '4'),
        //  deleteRows(1, '5:6'),
        //  deleteRows(1, '4'),
        //  deleteRows(1, '4:5'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "5:6").toString(),
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "4:5").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows12() throws JSONException {
        // Result: Should transform single intervals #2.
        // testRunner.runTest(
        //  deleteRows(1, '4'),
        //  deleteRows(1, '6:7'),
        //  deleteRows(1, '4'),
        //  deleteRows(1, '5:6'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "6:7").toString(),
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "5:6").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows13() throws JSONException {
        // Result: Should transform single intervals #3.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '1:2'),
        //  deleteRows(1, '1:2'),
        //  deleteRows(1, '1:2'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "1:2").toString(),
            SheetHelper.createDeleteRowsOp(1, "1:2").toString(),
            SheetHelper.createDeleteRowsOp(1, "1:2").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows14() throws JSONException {
        // Result: Should transform single intervals #3.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '2:3'),
        //  deleteRows(1, '2'),
        //  deleteRows(1, '2'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "2:3").toString(),
            SheetHelper.createDeleteRowsOp(1, "2").toString(),
            SheetHelper.createDeleteRowsOp(1, "2").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows15() throws JSONException {
        // Result: Should transform single intervals #3.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '3:4'),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteRowsdeleteRows16() throws JSONException {
        // Result: Should transform single intervals #3.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '4:5'),
        //  deleteRows(1, '3'),
        //  deleteRows(1, '3'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "4:5").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows17() throws JSONException {
        // Result: Should transform single intervals #3.
        // testRunner.runTest(
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '5:6'),
        //  deleteRows(1, '3:4'),
        //  deleteRows(1, '3:4'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "5:6").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows18() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runTest(
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '1 3'),
        //  deleteRows(1, '2 5'),
        //  deleteRows(1, '1 3'))
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "1 3").toString(),
            SheetHelper.createDeleteRowsOp(1, "2 5").toString(),
            SheetHelper.createDeleteRowsOp(1, "1 3").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows19() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runTest(
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '2 4'),
        //  deleteRows(1, '5'),
        //  deleteRows(1, '2'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "2 4").toString(),
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            SheetHelper.createDeleteRowsOp(1, "2").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows20() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runTest(
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '3 5'),
        //  deleteRows(1, '3 5'),
        //  deleteRows(1, '3:4'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "3 5").toString(),
            SheetHelper.createDeleteRowsOp(1, "3 5").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows21() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runTest(
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '4 6'),
        //  deleteRows(1, '5'),
        //  deleteRows(1, '5'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "4 6").toString(),
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            SheetHelper.createDeleteRowsOp(1, "5").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows22() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runTest(
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '5 7'),
        //  deleteRows(1, '4'),
        //  deleteRows(1, '4'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "5 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "4").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows23() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runTest(
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '5 7'),
        //  deleteRows(1, '4'),
        //  deleteRows(1, '4'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "5 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "4").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows24() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runTest(
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '6 8'),
        //  deleteRows(1, '4 6'),
        //  deleteRows(1, '5:6'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "6 8").toString(),
            SheetHelper.createDeleteRowsOp(1, "4 6").toString(),
            SheetHelper.createDeleteRowsOp(1, "5:6").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows25() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runTest(
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '7 9'),
        //  deleteRows(1, '4'),
        //  deleteRows(1, '7'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "7 9").toString(),
            SheetHelper.createDeleteRowsOp(1, "4").toString(),
            SheetHelper.createDeleteRowsOp(1, "7").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows26() throws JSONException {
        // Result: Should transform interval lists.
        // testRunner.runTest(
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '8 10'),
        //  deleteRows(1, '4 7'),
        //  deleteRows(1, '6 8'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "8 10").toString(),
            SheetHelper.createDeleteRowsOp(1, "4 7").toString(),
            SheetHelper.createDeleteRowsOp(1, "6 8").toString()
        );
    }

    @Test
    public void deleteRowsdeleteRows27() throws JSONException {
        // Result: Should not transform intervals in different sheets.
        // testRunner.runTest(
        //  deleteRows(1, '3'),
        //  deleteRows(2, '4'));
        SheetHelper.runTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createDeleteRowsOp(2, "4").toString()
        );
    }

/*
    describe('"deleteRows" and "changeRows"', function () {
        it('should transform intervals', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), opSeries2(changeRows, 1, ['1 2 3 4 5', '1:2 2:3 3:4 4:5', '3']),
                deleteRows(1, '3'), opSeries2(changeRows, 1, ['1 2 3 4',   '1:2 2 3 3:4'])
            );
            testRunner.runBidiTest(
                deleteRows(1, '3:4 6:7'), changeRows(1, '1:3 4:6 7:9'),
                deleteRows(1, '3:4 6:7'), changeRows(1, '1:2 3 4:5')
            );
        });
        it('should not transform intervals in different sheets', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), changeRows(2, '1:4', 'a1'));
        });
    });
*/

    @Test
    public void deleteRowsChangeRows01() throws JSONException {
        // Result: Should transform intervals.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3:4 6:7'),
        //  changeRows(1, '1:3 4:6 7:9'),
        //  deleteRows(1, '3:4 6:7'),
        //  changeRows(1, '1:2 3 4:5'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createChangeRowsOp(1, "1:3 4:6 7:9").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createChangeRowsOp(1, "1:2 3 4:5").toString()
        );
    }

    @Test
    public void deleteRowsChangeRows02() throws JSONException {
        // Result: Should transform intervals.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3:4 6:7'),
        //  changeRows(1, '1:3 4:6 7:9'),
        //  deleteRows(1, '3:4 6:7'),
        //  changeRows(1, '1:2 3 4:5'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createChangeRowsOp(1, "1:3 4:6 7:9").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createChangeRowsOp(1, "1:2 3 4:5").toString()
        );
    }

    @Test
    public void deleteRowsChangeRows03() throws JSONException {
        // Result: Should not transform intervals in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeRows(2, '1:4', 'a1'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeRowsOp(2, "1:4", "a1", null).toString()
        );
    }

/*
    describe('"deleteRows" and "changeCells"', function () {
        it('should transform ranges', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), changeCellsOps(1, 'A1 B2 C3 D4 E5', 'A1:B2 B2:C3 C3:D4 D4:E5', 'C3'),
                deleteRows(1, '3'), changeCellsOps(1, 'A1 B2 D3 E4',    'A1:B2 B2:C2 C3:D3 D3:E4')
            );
            -testRunner.runBidiTest(
                deleteRows(1, '3:4 6:7'), changeCellsOps(1, 'A1:C3 D4:F6 G7:I9'),
                deleteRows(1, '3:4 6:7'), changeCellsOps(1, 'A1:C2 D3:F3 G4:I5')
            );
        });
        it('should transform matrix formula range', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3:4'), changeCells(1, { A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' }, E5: { mr: 'E5:F6' } }),
                null,                 changeCells(1, { A1: { mr: 'A1:B2' }, E3: { mr: 'E3:F4' } })
            );
        });
        it('should fail to shrink a matrix formula range', function () {
            testRunner.expectBidiError(deleteRows(1, '3'), changeCells(1, { A1: { mr: 'A1:D4' } }));
        });
        it('should fail to change a shared formula', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), changeCells(1, { A1: { si: null } }));
            testRunner.runBidiTest(deleteRows(1, '3'), changeCells(1, { A1: { sr: null } }));
            testRunner.expectBidiError(deleteRows(1, '3'), changeCells(1, { A1: { si: 1 } }));
            testRunner.expectBidiError(deleteRows(1, '3'), changeCells(1, { A1: { sr: 'A1:D4' } }));
        });
        it('should not transform ranges in different sheets', function () {
            -testRunner.runBidiTest(deleteRows(1, '3'), changeCellsOps(2, 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteRowsChangeCells01() throws JSONException {
        // Result: Should transform ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeCellsOps(1, 'A1 B2 C3 D4 E5', 'A1:B2 B2:C3 C3:D4 D4:E5', 'C3'),
        //  deleteRows(1, '3'),
        //  changeCellsOps(1, 'A1 B2 D3 E4',    'A1:B2 B2:C2 C3:D3 D3:E4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOps(1, "A1 B2 C3 D4 E5", "A1:B2 B2:C3 C3:D4 D4:E5", "C3").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOps(1, "A1 B2 D3 E4", "A1:B2 B2:C2 C3:D3 D3:E4").toString()
        );
    }

    @Test
    public void deleteRowsChangeCells02() throws JSONException {
        // Result: Should transform ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3:4 6:7'),
        //  changeCellsOps(1, 'A1:C3 D4:F6 G7:I9'),
        //  deleteRows(1, '3:4 6:7'),
        //  changeCellsOps(1, 'A1:C2 D3:F3 G4:I5'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createChangeCellsOps(1, "A1:C3 D4:F6 G7:I9").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createChangeCellsOps(1, "A1:C2 D3:F3 G4:I5").toString()
        );
    }

    @Test
    public void deleteRowsChangeCells03() throws JSONException {
        // Result: Should transform matrix formula range.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3:4'),
        //  changeCells(1, { A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' }, E5: { mr: 'E5:F6' } }),
        //  null,
        //  changeCells(1, { A1: { mr: 'A1:B2' }, E3: { mr: 'E3:F4' } }));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' }, E5: { mr: 'E5:F6' } }")).toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { mr: 'A1:B2' }, E3: { mr: 'E3:F4' } }")).toString()
        );
    }

    @Test
    public void deleteRowsChangeCells04() throws JSONException {
        // Result: Should fail to shrink a matrix formula range.
        // testRunner.expectBidiError(
        //  deleteRows(1, '3'),
        //  changeCells(1, { A1: { mr: 'A1:D4' } }));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { mr: 'A1:D4' } }")).toString()
        );
    }

    @Test
    public void deleteRowsChangeCells05() throws JSONException {
        // Result: Should fail to change a shared formula.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeCells(1, { A1: { si: null } }));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { si: null } }")).toString()
        );
    }

    @Test
    public void deleteRowsChangeCells06() throws JSONException {
        // Result: Should fail to change a shared formula.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeCells(1, { A1: { sr: null } }));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { sr: null } }")).toString()
        );
    }

    @Test
    public void deleteRowsChangeCells07() throws JSONException {
        // Result: Should fail to change a shared formula.
        // testRunner.expectBidiError(
        //  deleteRows(1, '3'),
        //  changeCells(1, { A1: { si: 1 } }));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { si: 1 } }")).toString()
        );
    }

    @Test
    public void deleteRowsChangeCells08() throws JSONException {
        // Result: Should fail to change a shared formula.
        // testRunner.expectBidiError(
        //  deleteRows(1, '3'),
        //  changeCells(1, { A1: { sr: 'A1:D4' } }));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOp(1, Helper.createJSONObject("{ A1: { sr: 'A1:D4' } }")).toString()
        );
    }

    @Test
    public void deleteRowsChangeCells09() throws JSONException {
        // Result: Should not transform ranges in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeCellsOps(2, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCellsOps(2, "A1:D4").toString()
        );
    }

/*
    describe('"deleteRows" and "mergeCells"', function () {
        it('should resize ranges', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), opSeries2(mergeCells, 1, 'B1:C2 D2:E4 F4:G5', MM_ALL),
                deleteRows(1, '3'), opSeries2(mergeCells, 1, 'B1:C2 D2:E3 F3:G4', MM_ALL)
            );
        });
        it('should delete single ranges', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), mergeCells(1, 'A1:B3 C1:C3 D2:E3 F2:F3', MM_MERGE),
                deleteRows(1, '3'), mergeCells(1, 'A1:B2 C1:C2 D2:E2',       MM_MERGE) // deletes single cell
            );
            testRunner.runBidiTest(
                deleteRows(1, '3'), mergeCells(1, 'A1:B3 C1:C3 D2:E3 F2:F3', MM_VERTICAL),
                deleteRows(1, '3'), mergeCells(1, 'A1:B2 C1:C2',             MM_VERTICAL) // deletes row vector
            );
            testRunner.runBidiTest(
                deleteRows(1, '3'), mergeCells(1, 'A1:B3 C1:C3 D2:E3 F2:F3', MM_HORIZONTAL),
                deleteRows(1, '3'), mergeCells(1, 'A1:B2 D2:E2',             MM_HORIZONTAL) // deletes column vector
            );
            testRunner.runBidiTest(
                deleteRows(1, '3'), mergeCells(1, 'A1:B3 C1:C3 D2:E3 F2:F3', MM_UNMERGE),
                deleteRows(1, '3'), mergeCells(1, 'A1:B2 C1:C2 D2:E2 F2',    MM_UNMERGE) // keeps all
            );
        });
        it('should remove no-op operations', function () {
            testRunner.runBidiTest(deleteRows(1, '3:4'), opSeries2(mergeCells, 1, 'C3:D4', MM_ALL), null, []);
        });
        it('should not modify entire column ranges', function () {
            testRunner.runBidiTest(deleteRows(1, '3:4'), opSeries2(mergeCells, 1, 'A1:B1048576', MM_ALL));
        });
        it('should not transform ranges in different sheets', function () {
            -testRunner.runBidiTest(deleteRows(1, 'C'), mergeCells(2, 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteRowsMergeCells01() throws JSONException {
        // Result: Should resize ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  opSeries2(mergeCells, 1, 'B1:C2 D2:E4 F4:G5', MM_ALL),
        //  deleteRows(1, '3'),
        //  opSeries2(mergeCells, 1, 'B1:C2 D2:E3 F3:G4', MM_ALL));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E4 F4:G5", SheetHelper.MM_HORIZONTAL),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E4 F4:G5", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E4 F4:G5", SheetHelper.MM_UNMERGE),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E4 F4:G5", SheetHelper.MM_VERTICAL)
            ),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E3 F3:G4", SheetHelper.MM_HORIZONTAL),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E3 F3:G4", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E3 F3:G4", SheetHelper.MM_UNMERGE),
                SheetHelper.createMergeCellsOp(1, "B1:C2 D2:E3 F3:G4", SheetHelper.MM_VERTICAL)
            )
        );
    }

    @Test
    public void deleteRowsMergeCells02() throws JSONException {
        // Result: Should delete single ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  mergeCells(1, 'A1:B3 C1:C3 D2:E3 F2:F3', MM_MERGE),
        //  deleteRows(1, '3'),
        //  mergeCells(1, 'A1:B2 C1:C2 D2:E2',       MM_MERGE) // deletes single cell);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "A1:B3 C1:C3 D2:E3 F2:F3", SheetHelper.MM_MERGE).toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "A1:B2 C1:C2 D2:E2", SheetHelper.MM_MERGE).toString()
        );
    }

    @Test
    public void deleteRowsMergeCells03() throws JSONException {
        // Result: Should delete single ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  mergeCells(1, 'A1:B3 C1:C3 D2:E3 F2:F3', MM_VERTICAL),
        //  deleteRows(1, '3'),
        //  mergeCells(1, 'A1:B2 C1:C2',             MM_VERTICAL) // deletes row vector);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "A1:B3 C1:C3 D2:E3 F2:F3", SheetHelper.MM_VERTICAL).toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "A1:B2 C1:C2", SheetHelper.MM_VERTICAL).toString()
        );
    }

    @Test
    public void deleteRowsMergeCells04() throws JSONException {
        // Result: Should delete single ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  mergeCells(1, 'A1:B3 C1:C3 D2:E3 F2:F3', MM_HORIZONTAL),
        //  deleteRows(1, '3'),
        //  mergeCells(1, 'A1:B2 D2:E2',             MM_HORIZONTAL) // deletes column vector);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "A1:B3 C1:C3 D2:E3 F2:F3", SheetHelper.MM_HORIZONTAL).toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "A1:B2 D2:E2", SheetHelper.MM_HORIZONTAL).toString()
        );
    }

    @Test
    public void deleteRowsMergeCells05() throws JSONException {
        // Result: Should delete single ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  mergeCells(1, 'A1:B3 C1:C3 D2:E3 F2:F3', MM_UNMERGE),
        //  deleteRows(1, '3'),
        //  mergeCells(1, 'A1:B2 C1:C2 D2:E2 F2',    MM_UNMERGE) // keeps all);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "A1:B3 C1:C3 D2:E3 F2:F3", SheetHelper.MM_UNMERGE).toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMergeCellsOp(1, "A1:B2 C1:C2 D2:E2 F2", SheetHelper.MM_UNMERGE).toString()
        );
    }

    @Test
    public void deleteRowsMergeCells06() throws JSONException {
        // Result: Should remove no-op operations.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3:4'),
        //  opSeries2(mergeCells, 1, 'C3:D4', MM_ALL), null, []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "C3:D4", SheetHelper.MM_HORIZONTAL),
                SheetHelper.createMergeCellsOp(1, "C3:D4", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "C3:D4", SheetHelper.MM_UNMERGE),
                SheetHelper.createMergeCellsOp(1, "C3:D4", SheetHelper.MM_VERTICAL)
            ),
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            "[]"
        );
    }

    @Test
    public void deleteRowsMergeCells07() throws JSONException {
        // Result: Should not modify entire column ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3:4'),
        //  opSeries2(mergeCells, 1, 'A1:B1048576', MM_ALL));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3:4").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A1:B1048576", SheetHelper.MM_HORIZONTAL),
                SheetHelper.createMergeCellsOp(1, "A1:B1048576", SheetHelper.MM_MERGE),
                SheetHelper.createMergeCellsOp(1, "A1:B1048576", SheetHelper.MM_UNMERGE),
                SheetHelper.createMergeCellsOp(1, "A1:B1048576", SheetHelper.MM_VERTICAL)
            )
        );
    }

    @Test
    public void deleteRowsMergeCells08() throws JSONException {
        // Result: Should not transform ranges in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, 'C'),
        //  mergeCells(2, 'A1:D4'))
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "C").toString(),
            SheetHelper.createMergeCellsOp(2, "A1:D4", null).toString()
        );
    }

/*
    describe('"deleteRows" and hyperlinks (RESIZE mode)', function () {
        it('should transform ranges', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), hlinkOps(1, 'A1 B2 C3 D4 E5', 'A1:B2 B2:C3 C3:D4 D4:E5', 'C3'),
                deleteRows(1, '3'), hlinkOps(1, 'A1 B2 D3 E4',    'A1:B2 B2:C2 C3:D3 D3:E4')
            );
            testRunner.runBidiTest(
                deleteRows(1, '3:4 6:7'), hlinkOps(1, 'A1:C3 D4:F6 G7:I9'),
                deleteRows(1, '3:4 6:7'), hlinkOps(1, 'A1:C2 D3:F3 G4:I5')
            );
        });
        it('should not transform ranges in different sheets', function () {
            -testRunner.runBidiTest(deleteRows(1, '3'), hlinkOps(2, 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteRowsHyperlinks01() throws JSONException {
        // Result: Should transform ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  hlinkOps(1, 'A1 B2 C3 D4 E5', 'A1:B2 B2:C3 C3:D4 D4:E5', 'C3'),
        //  deleteRows(1, '3'),
        //  hlinkOps(1, 'A1 B2 D3 E4',    'A1:B2 B2:C2 C3:D3 D3:E4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createHlinkOps("1", "A1 B2 C3 D4 E5", "A1:B2 B2:C3 C3:D4 D4:E5", "C3").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createHlinkOps("1", "A1 B2 D3 E4", "A1:B2 B2:C2 C3:D3 D3:E4").toString()
        );
    }

    @Test
    public void deleteRowsHyperlinks02() throws JSONException {
        // Result: Should transform ranges.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3:4 6:7'),
        //  hlinkOps(1, 'A1:C3 D4:F6 G7:I9'),
        //  deleteRows(1, '3:4 6:7'),
        //  hlinkOps(1, 'A1:C2 D3:F3 G4:I5'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createHlinkOps("1", "A1:C3 D4:F6 G7:I9").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createHlinkOps("1", "A1:C2 D3:F3 G4:I5").toString()
        );
    }

    @Test
    public void deleteRowsHyperlinks03() throws JSONException {
        // Result: Should not transform ranges in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  hlinkOps(2, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createHlinkOps("2", "A1:D4").toString()
        );
    }

/*
    describe('"deleteRows" and "insertTable"', function () {
        it('should shift and shrink table range', function () {
            testRunner.runBidiTest(
                deleteRows(1, '5'), opSeries2(insertTable, 1, 'T', ['A1:D4', 'A3:D6', 'A6:D9']),
                deleteRows(1, '5'), opSeries2(insertTable, 1, 'T', ['A1:D4', 'A3:D5', 'A5:D8'])
            );
        });
        it('should delete table range explicitly', function () {
            testRunner.runBidiTest(
                deleteRows(1, '2:7'), insertTable(1, 'T', 'A3:D6'),
                [deleteTable(1, 'T'), deleteRows(1, '2:7')], []
            );
        });
        it('should fail to delete header or footer row', function () {
            testRunner.expectBidiError(deleteRows(1, '4:6'), insertTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(deleteRows(1, '4:6'), insertTable(1, 'T', 'A2:D5'));
            testRunner.expectBidiError(deleteRows(1, '4:6'), insertTable(1, 'T', 'A5:D8'));
            testRunner.expectBidiError(deleteRows(1, '4:6'), insertTable(1, 'T', 'A6:D9'));
        });
        it('should not transform tables in different sheets', function () {
            -testRunner.runBidiTest(deleteRows(1, '3'), insertTable(2, 'T', 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteRowsInsertTable01() throws JSONException {
        // Result: Should shift and shrink table range.
        // testRunner.runBidiTest(
        //  deleteRows(1, '5'),
        //  opSeries2(insertTable, 1, 'T', ['A1:D4', 'A3:D6', 'A6:D9']),
        //  deleteRows(1, '5'),
        //  opSeries2(insertTable, 1, 'T', ['A1:D4', 'A3:D5', 'A5:D8']));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1:D4", null),
                SheetHelper.createInsertTableOp(1, "T", "A3:D6", null),
                SheetHelper.createInsertTableOp(1, "T", "A6:D9", null)
            ),
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1:D4", null),
                SheetHelper.createInsertTableOp(1, "T", "A3:D5", null),
                SheetHelper.createInsertTableOp(1, "T", "A5:D8", null)
            )
        );
    }

    @Test
    public void deleteRowsInsertTable02() throws JSONException {
        // Result: Should delete table range explicitly.
        // testRunner.runBidiTest(
        //  deleteRows(1, '2:7'),
        //  insertTable(1, 'T', 'A3:D6'),
        //  [deleteTable(1, 'T'), deleteRows(1, '2:7')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "2:7").toString(),
            SheetHelper.createInsertTableOp(1, "T", "A3:D6", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createDeleteRowsOp(1, "2:7")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsInsertTable03() throws JSONException {
        // Result: Should fail to delete header or footer row.
        // testRunner.expectBidiError(
        //  deleteRows(1, '4:6'),
        //  insertTable(1, 'T', 'A1:D4'))
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "4:6").toString(),
            SheetHelper.createInsertTableOp(1, "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteRowsInsertTable04() throws JSONException {
        // Result: Should fail to delete header or footer row.
        // testRunner.expectBidiError(
        //  deleteRows(1, '4:6'),
        //  insertTable(1, 'T', 'A2:D5'));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "4:6").toString(),
            SheetHelper.createInsertTableOp(1, "T", "A2:D5", null).toString()
        );
    }

    @Test
    public void deleteRowsInsertTable05() throws JSONException {
        // Result: Should fail to delete header or footer row.
        // testRunner.expectBidiError(
        //  deleteRows(1, '4:6'),
        //  insertTable(1, 'T', 'A5:D8'));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "4:6").toString(),
            SheetHelper.createInsertTableOp(1, "T", "A5:D8", null).toString()
        );
    }

    @Test
    public void deleteRowsInsertTable06() throws JSONException {
        // Result: Should fail to delete header or footer row.
        // testRunner.expectBidiError(
        //  deleteRows(1, '4:6'),
        //  insertTable(1, 'T', 'A6:D9'));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "4:6").toString(),
            SheetHelper.createInsertTableOp(1, "T", "A6:D9", null).toString()
        );
    }

    @Test
    public void deleteRowsInsertTable07() throws JSONException {
        // Result: Should not transform tables in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  insertTable(2, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createInsertTableOp(2, "T", "A1:D4", null).toString()
        );
    }

/*
    describe('"deleteRows" and "changeTable"', function () {
        it('should shift and shrink table range', function () {
            -testRunner.runBidiTest(
                deleteRows(1, '5'), opSeries2(changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'A3:D6', 'A6:D9']),
                deleteRows(1, '5'), opSeries2(changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'A3:D5', 'A5:D8'])
            );
        });
        it('should delete table range explicitly', function () {
            testRunner.runBidiTest(
                deleteRows(1, '2:7'), changeTable(1, 'T', 'A3:D6'),
                [deleteTable(1, 'T'), deleteRows(1, '2:7')], []
            );
        });
        it('should fail to delete header or footer row', function () {
            testRunner.expectBidiError(deleteRows(1, '4:6'), changeTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(deleteRows(1, '4:6'), changeTable(1, 'T', 'A2:D5'));
            testRunner.expectBidiError(deleteRows(1, '4:6'), changeTable(1, 'T', 'A5:D8'));
            testRunner.expectBidiError(deleteRows(1, '4:6'), changeTable(1, 'T', 'A6:D9'));
        });
        it('should not transform tables in different sheets', function () {
            -testRunner.runBidiTest(deleteRows(1, '3'), changeTable(2, 'T', 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteRowsChangeTable01() throws JSONException {
        // Result: Should shift and shrink table range.
        // testRunner.runBidiTest(
        //  deleteRows(1, '5'),
        //  opSeries2(changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'A3:D6', 'A6:D9']),
        //  deleteRows(1, '5'),
        //  opSeries2(changeTable, 1, 'T', [{ attrs: ATTRS }, 'A1:D4', 'A3:D5', 'A5:D8']));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeTableOp(1, "T", null, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS + " }").toString()),
                SheetHelper.createChangeTableOp(1, "T", "A1:D4", null),
                SheetHelper.createChangeTableOp(1, "T", "A3:D6", null),
                SheetHelper.createChangeTableOp(1, "T", "A6:D9", null)
            ),
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeTableOp(1, "T", null, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS + " }").toString()),
                SheetHelper.createChangeTableOp(1, "T", "A1:D4", null),
                SheetHelper.createChangeTableOp(1, "T", "A3:D5", null),
                SheetHelper.createChangeTableOp(1, "T", "A5:D8", null)
            )
        );
    }

    @Test
    public void deleteRowsChangeTable02() throws JSONException {
        // Result: Should delete table range explicitly.
        // testRunner.runBidiTest(
        //  deleteRows(1, '2:7'),
        //  changeTable(1, 'T', 'A3:D6'),
        //  [deleteTable(1, 'T'), deleteRows(1, '2:7')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "2:7").toString(),
            SheetHelper.createChangeTableOp(1, "T", "A3:D6", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createDeleteRowsOp(1, "2:7")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsChangeTable03() throws JSONException {
        // Result: Should fail to delete header or footer row.
        // testRunner.expectBidiError(
        //  deleteRows(1, '4:6'),
        //  changeTable(1, 'T', 'A2:D5'));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "4:6").toString(),
            SheetHelper.createChangeTableOp(1, "T", "A2:D5", null).toString()
        );
    }

    @Test
    public void deleteRowsChangeTable04() throws JSONException {
        // Result: Should fail to delete header or footer row.
        // testRunner.expectBidiError(
        //  deleteRows(1, '4:6'),
        //  changeTable(1, 'T', 'A5:D8'));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "4:6").toString(),
            SheetHelper.createChangeTableOp(1, "T", "A5:D8", null).toString()
        );
    }

    @Test
    public void deleteRowsChangeTable05() throws JSONException {
        // Result: Should fail to delete header or footer row.
        // testRunner.expectBidiError(
        //  deleteRows(1, '4:6'),
        //  changeTable(1, 'T', 'A6:D9'));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteRowsOp(1, "4:6").toString(),
            SheetHelper.createChangeTableOp(1, "T", "A6:D9", null).toString()
        );
    }

    @Test
    public void deleteRowsChangeTable06() throws JSONException {
        // Result: Should not transform tables in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeTable(2, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeTableOp(2, "T", "A1:D4", null).toString()
        );
    }

/*
    describe('"deleteRows" and tables', function () {
        it('should handle insert/change sequence', function () {
            testRunner.runBidiTest(
                deleteRows(1, '2:7'), [insertTable(1, 'T', 'A3:D6'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS })],
                [deleteTable(1, 'T'), deleteRows(1, '2:7')], []
            );
        });
        it('should handle insert/change/delete sequence', function () {
            -testRunner.runBidiTest(
                deleteRows(1, '2:7'), [insertTable(1, 'T', 'A3:D6'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS }), deleteTable(1, 'T')],
                null, []
            );
        });
    });
*/

    @Test
    public void deleteRowsTable01() throws JSONException {
        // Result: Should handle insert/change/delete sequence.
        // testRunner.runBidiTest(
        //  deleteRows(1, '2:7'),
        //  [insertTable(1, 'T', 'A3:D6'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS })],
        //  [deleteTable(1, 'T'), deleteRows(1, '2:7')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "2:7").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A3:D6", null),
                SheetHelper.createChangeTableOp(1, "T", null, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS + " }").toString()),
                SheetHelper.createChangeTableColOp(1, "T", 0, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS + " }").toString())
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createDeleteRowsOp(1, "2:7")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsTable02() throws JSONException {
        // Result: Should handle insert/change sequence.
        // testRunner.runBidiTest(
        //  deleteRows(1, '2:7'),
        //  [insertTable(1, 'T', 'A3:D6'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS }), deleteTable(1, 'T')],
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "2:7").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A3:D6", null),
                SheetHelper.createChangeTableOp(1, "T", null, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS + " }").toString()),
                SheetHelper.createChangeTableColOp(1, "T", 0, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS + " }").toString()),
                SheetHelper.createDeleteTableOp(1, "T")
            ),
            SheetHelper.createDeleteRowsOp(1, "2:7").toString(),
            "[]"
        );
    }

/*
    describe('"deleteRows" and "insertDVRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                deleteRows(1, '5'), opSeries2(insertDVRule, 1, 0, ['A1:D4', 'A3:D6', 'A6:D9']),
                deleteRows(1, '5'), opSeries2(insertDVRule, 1, 0, ['A1:D4', 'A3:D5', 'A5:D8'])
            );
        });
        it('should delete rule explicitly', function () {
            testRunner.runBidiTest(
                deleteRows(1, '1:9'), insertDVRule(1, 0, 'A3:D6'),
                [deleteDVRule(1, 0), deleteRows(1, '1:9')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            -testRunner.runBidiTest(deleteRows(1, '3'), insertDVRule(2, 0, 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteRowsInsertDVRule01() throws JSONException {
        // Result: Should shift rule.
        // testRunner.runBidiTest(
        //  deleteRows(1, '5'),
        //  opSeries2(insertDVRule, 1, 0, ['A1:D4', 'A3:D6', 'A6:D9']),
        //  deleteRows(1, '5'),
        //  opSeries2(insertDVRule, 1, 0, ['A1:D4', 'A3:D5', 'A5:D8']));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "A3:D6", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "A6:D9", null)
            ),
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "A3:D5", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "A5:D8", null)
            )
        );
    }

    @Test
    public void deleteRowsInsertDVRule02() throws JSONException {
        // Result: Should delete rule explicitly.
        // testRunner.runBidiTest(
        //  deleteRows(1, '1:9'),
        //  insertDVRule(1, 0, 'A3:D6'),
        //  [deleteDVRule(1, 0), deleteRows(1, '1:9')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "1:9").toString(),
            SheetHelper.createInsertDVRuleOp(1, 0, "A3:D6", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createDeleteRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsInsertDVRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  insertDVRule(2, 0, 'A1:D4'))
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createInsertDVRuleOp(2, 0, "A1:D4", null).toString()
        );
    }

/*
    describe('"deleteRows" and "changeDVRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                deleteRows(1, '5'), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'A3:D6', 'A6:D9']),
                deleteRows(1, '5'), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'A3:D5', 'A5:D8'])
            );
        });
        it('should delete rule explicitly', function () {
            -testRunner.runBidiTest(
                deleteRows(1, '1:9'), changeDVRule(1, 0, 'A3:D6'),
                [deleteDVRule(1, 0), deleteRows(1, '1:9')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), changeDVRule(2, 0, 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteRowsChangeDVRule01() throws JSONException {
        // Result: Should shift rule.
        // testRunner.runBidiTest(
        //  deleteRows(1, '5'),
        //  opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'A3:D6', 'A6:D9']),
        //  deleteRows(1, '5'),
        //  opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'A3:D5', 'A5:D8']));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "A3:D6", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "A6:D9", null)
            ),
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "A3:D5", null),
                SheetHelper.createChangeDVRuleOp(1, 0, "A5:D8", null)
            )
        );
    }

    @Test
    public void deleteRowsChangeDVRule02() throws JSONException {
        // Result: Should delete rule explicitly.
        // testRunner.runBidiTest(
        //  deleteRows(1, '1:9'),
        //  changeDVRule(1, 0, 'A3:D6'),
        //  [deleteDVRule(1, 0), deleteRows(1, '1:9')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "1:9").toString(),
            SheetHelper.createChangeDVRuleOp(1, 0, "A3:D6", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createDeleteRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsChangeDVRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeDVRule(2, 0, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeDVRuleOp(2, 0, "A1:D4", null).toString()
        );
    }

/*
    describe('"deleteRows" and "insertCFRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                deleteRows(1, '5'), opSeries2(insertCFRule, 1, 'R1', ['A1:D4', 'A3:D6', 'A6:D9']),
                deleteRows(1, '5'), opSeries2(insertCFRule, 1, 'R1', ['A1:D4', 'A3:D5', 'A5:D8'])
            );
        });
        it('should delete rule explicitly', function () {
            -testRunner.runBidiTest(
                deleteRows(1, '1:9'), insertCFRule(1, 'R1', 'A3:D6'),
                [deleteCFRule(1, 'R1'), deleteRows(1, '1:9')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), insertCFRule(2, 'R1', 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteRowsInsertCFRule01() throws JSONException {
        // Result: Should shift rule.
        // testRunner.runBidiTest(
        //  deleteRows(1, '5'),
        //  opSeries2(insertCFRule, 1, 'R1', ['A1:D4', 'A3:D6', 'A6:D9']),
        //  deleteRows(1, '5'),
        //  opSeries2(insertCFRule, 1, 'R1', ['A1:D4', 'A3:D5', 'A5:D8']));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "A3:D6", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "A6:D9", null)
            ),
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "A3:D5", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "A5:D8", null)
            )
        );
    }

    @Test
    public void deleteRowsInsertCFRule02() throws JSONException {
        // Result: Should delete rule explicitly.
        // testRunner.runBidiTest(
        //  deleteRows(1, '1:9'),
        //  insertCFRule(1, 'R1', 'A3:D6'),
        //  [deleteCFRule(1, 'R1'), deleteRows(1, '1:9')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "1:9").toString(),
            SheetHelper.createInsertCFRuleOp(1, "R1", "A3:D6", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                SheetHelper.createDeleteRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsInsertCFRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  insertCFRule(2, 'R1', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createInsertCFRuleOp(2, "R1", "A1:D4", null).toString()
        );
    }

/*
    describe('"deleteRows" and "changeCFRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                deleteRows(1, '5'), opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D4', 'A3:D6', 'A6:D9']),
                deleteRows(1, '5'), opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D4', 'A3:D5', 'A5:D8'])
            );
        });
        it('should delete rule explicitly', function () {
            testRunner.runBidiTest(
                deleteRows(1, '1:9'), changeCFRule(1, 'R1', 'A3:D6'),
                [deleteCFRule(1, 'R1'), deleteRows(1, '1:9')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), changeCFRule(2, 'R1', 'A1:D4'));
        });
    });
*/

    @Test
    public void deleteRowsChangeCFRule01() throws JSONException {
        // Result: Should shift rule.
        // testRunner.runBidiTest(
        //  deleteRows(1, '5'),
        //  opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D4', 'A3:D6', 'A6:D9']),
        //  deleteRows(1, '5'),
        //  opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D4', 'A3:D5', 'A5:D8']));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A3:D6", null),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A6:D9", null)
            ),
            SheetHelper.createDeleteRowsOp(1, "5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A3:D5", null),
                SheetHelper.createChangeCFRuleOp(1, "R1", "A5:D8", null)
            )
        );
    }

    @Test
    public void deleteRowsChangeCFRule02() throws JSONException {
        // Result: Should delete rule explicitly.
        // testRunner.runBidiTest(
        //  deleteRows(1, '1:9'),
        //  changeCFRule(1, 'R1', 'A3:D6'),
        //  [deleteCFRule(1, 'R1'), deleteRows(1, '1:9')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "1:9").toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", "A3:D6", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                SheetHelper.createDeleteRowsOp(1, "1:9")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsChangeCFRule03() throws JSONException {
        // Result: Should not transform rules in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeCFRule(2, 'R1', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCFRuleOp(2, "R1", "A1:D4", null).toString()
        );
    }

/*
    describe('"deleteRows" and cell anchor', function () {
        it('should transform cell anchor', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), cellAnchorOps(1, 'A1', 'B2', 'D4', 'E5'),
                deleteRows(1, '3'), cellAnchorOps(1, 'A1', 'B2', 'D3', 'E4')
            );
            testRunner.runBidiTest(
                deleteRows(1, '3:4 6:7'), cellAnchorOps(1, 'A1', 'B2', 'E5', 'H8', 'I9'),
                deleteRows(1, '3:4 6:7'), cellAnchorOps(1, 'A1', 'B2', 'E3', 'H4', 'I5')
            );
        });
        it('should handle deleted cell note', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), insertNote(1, 'C3', 'abc'), [deleteNote(1, 'C3'), deleteRows(1, '3')], []);
            testRunner.runBidiTest(deleteRows(1, '3'), deleteNote(1, 'C3'),        null,                                      []);
            testRunner.runBidiTest(deleteRows(1, '3'), changeNote(1, 'C3', 'abc'), [deleteNote(1, 'C3'), deleteRows(1, '3')], []);
        });
        it('should handle deleted cell comment', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), insertComment(1, 'C3', 0, { text: 'abc' }), [deleteComment(1, 'C3', 0), deleteRows(1, '3')], []);
            testRunner.runBidiTest(deleteRows(1, '3'), insertComment(1, 'C3', 1, { text: 'abc' }), null,                                            []);
            testRunner.runBidiTest(deleteRows(1, '3'), deleteComment(1, 'C3', 0),                  null,                                            []);
            testRunner.runBidiTest(deleteRows(1, '3'), deleteComment(1, 'C3', 1),                  null,                                            []);
            testRunner.runBidiTest(deleteRows(1, '3'), changeComment(1, 'C3', 0, { text: 'abc' }), [deleteComment(1, 'C3', 0), deleteRows(1, '3')], []);
            testRunner.runBidiTest(deleteRows(1, '3'), changeComment(1, 'C3', 1, { text: 'abc' }), null,                                            []);
        });
        it('should not transform cell anchor in different sheets', function () {
            -testRunner.runBidiTest(deleteRows(1, '3'), cellAnchorOps(2, 'D4'));
        });
    });
*/

    @Test
    public void deleteRowsCellAnchor01() throws JSONException {
        // Result: Should transform cell anchor.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  cellAnchorOps(1, 'A1', 'B2', 'D4', 'E5'),
        //  deleteRows(1, '3'),
        //  cellAnchorOps(1, 'A1', 'B2', 'D3', 'E4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createCellAnchorOps(1, "A1", "B2", "D4", "E5").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createCellAnchorOps(1, "A1", "B2", "D3", "E4").toString()
        );
    }

    @Test
    public void deleteRowsCellAnchor02() throws JSONException {
        // Result: Should transform cell anchor.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3:4 6:7'),
        //  cellAnchorOps(1, 'A1', 'B2', 'E5', 'H8', 'I9'),
        //  deleteRows(1, '3:4 6:7'),
        //  cellAnchorOps(1, 'A1', 'B2', 'E3', 'H4', 'I5'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createCellAnchorOps(1, "A1", "B2", "E5", "H8", "I9").toString(),
            SheetHelper.createDeleteRowsOp(1, "3:4 6:7").toString(),
            SheetHelper.createCellAnchorOps(1, "A1", "B2", "E3", "H4", "I5").toString()
        );
    }

    @Test
    public void deleteRowsCellAnchor03() throws JSONException {
        // Result: Should handle deleted cell note.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  insertNote(1, 'C3', 'abc'),
        //  [deleteNote(1, 'C3'), deleteRows(1, '3')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createInsertNoteOp(1, "C3", "abc", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "C3"),
                SheetHelper.createDeleteRowsOp(1, "3")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsCellAnchor04() throws JSONException {
        // Result: Should handle deleted cell note.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  deleteNote(1, 'C3'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createDeleteNoteOp(1, "C3").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void deleteRowsCellAnchor05() throws JSONException {
        // Result: Should handle deleted cell note.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeNote(1, 'C3', 'abc'),
        //  [deleteNote(1, 'C3'), deleteRows(1, '3')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeNoteOp(1, "C3", "abc", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "C3"),
                SheetHelper.createDeleteRowsOp(1, "3")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsCellAnchor06() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  insertComment(1, 'C3', 0, { text: 'abc' }),
        //  [deleteComment(1, 'C3', 0), deleteRows(1, '3')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createInsertCommentOp(1, "C3", 0, "{ text: 'abc' }").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "C3", 0),
                SheetHelper.createDeleteRowsOp(1, "3")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsCellAnchor07() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  insertComment(1, 'C3', 1, { text: 'abc' }),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createInsertCommentOp(1, "C3", 1, "{ text: 'abc' }").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void deleteRowsCellAnchor08() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  deleteComment(1, 'C3', 0),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createDeleteCommentOp(1, "C3", 0).toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void deleteRowsCellAnchor09() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  deleteComment(1, 'C3', 1),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createDeleteCommentOp(1, "C3", 1).toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void deleteRowsCellAnchor10() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeComment(1, 'C3', 0, { text: 'abc' }),
        //  [deleteComment(1, 'C3', 0), deleteRows(1, '3')],
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCommentOp(1, "C3", 0, "{ text: 'abc' }").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "C3", 0),
                SheetHelper.createDeleteRowsOp(1, "3")
            ),
            "[]"
        );
    }

    @Test
    public void deleteRowsCellAnchor11() throws JSONException {
        // Result: Should handle deleted cell comment.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  changeComment(1, 'C3', 1, { text: 'abc' }),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createChangeCommentOp(1, "C3", 1, "{ text: 'abc' }").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            "[]"
        );
    }

    @Test
    public void deleteRowsCellAnchor12() throws JSONException {
        // Result: Should not transform cell anchor in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  cellAnchorOps(2, 'D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createCellAnchorOps(2, "D4").toString()
        );
    }

/*
    describe('"deleteRows" and "moveNotes"', function () {
        it('should transform cell anchors', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), moveNotes(1, 'A1 B2 C4 D5', 'D1 C2 B4 A5'),
                deleteRows(1, '3'), moveNotes(1, 'A1 B2 C3 D4', 'D1 C2 B3 A4')
            );
        });
        it('should handle deleted source anchor', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), moveNotes(1, 'A2 B3 C3 D4', 'A5 B5 A3 D5'),
                [deleteNote(1, 'B5'), deleteNote(1, 'A3'), deleteRows(1, '3')], moveNotes(1, 'A2 D3', 'A4 D4')
            );
        });
        it('should handle deleted target anchor', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), moveNotes(1, 'A5 B5 C5', 'A2 B3 C4'),
                [deleteNote(1, 'B3'), deleteRows(1, '3')], [deleteNote(1, 'B4'), moveNotes(1, 'A4 C4', 'A2 C3')]
            );
        });
        it('should not transform cell anchor in different sheets', function () {
            -testRunner.runBidiTest(deleteRows(1, '3'), moveNotes(2, 'D4', 'E5'));
        });
    });
*/

    @Test
    public void deleteRowsMoveNotes01() throws JSONException {
        // Result: Should transform cell anchors.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  moveNotes(1, 'A1 B2 C4 D5', 'D1 C2 B4 A5'),
        //  deleteRows(1, '3'),
        //  moveNotes(1, 'A1 B2 C3 D4', 'D1 C2 B3 A4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 B2 C4 D5", "D1 C2 B4 A5").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(1, "A1 B2 C3 D4", "D1 C2 B3 A4").toString()
        );
    }

    @Test
    public void deleteRowsMoveNotes02() throws JSONException {
        // Result: Should handle deleted source anchor.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  moveNotes(1, 'A2 B3 C3 D4', 'A5 B5 A3 D5'),
        //  [deleteNote(1, 'B5'), deleteNote(1, 'A3'), deleteRows(1, '3')],
        //  moveNotes(1, 'A2 D3', 'A4 D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(1, "A2 B3 C3 D4", "A5 B5 A3 D5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "B5"),
                SheetHelper.createDeleteNoteOp(1, "A3"),
                SheetHelper.createDeleteRowsOp(1, "3")
            ),
            SheetHelper.createMoveNotesOp(1, "A2 D3", "A4 D4").toString()
        );
    }

    @Test
    public void deleteRowsMoveNotes03() throws JSONException {
        // Result: Should handle deleted target anchor.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  moveNotes(1, 'A5 B5 C5', 'A2 B3 C4'),
        //  [deleteNote(1, 'B3'), deleteRows(1, '3')],
        //  [deleteNote(1, 'B4'), moveNotes(1, 'A4 C4', 'A2 C3')]);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(1, "A5 B5 C5", "A2 B3 C4").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "B3"),
                SheetHelper.createDeleteRowsOp(1, "3")
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "B4"),
                SheetHelper.createMoveNotesOp(1, "A4 C4", "A2 C3")
            )
        );
    }

    @Test
    public void deleteRowsMoveNotes04() throws JSONException {
        // Result: Should not transform cell anchor in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  moveNotes(2, 'D4', 'E5'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveNotesOp(2, "D4", "E5").toString()
        );
    }

/*
    describe('"deleteRows" and "moveComments"', function () {
        it('should transform cell anchors', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), moveComments(1, 'A1 B2 C4 D5', 'D1 C2 B4 A5'),
                deleteRows(1, '3'), moveComments(1, 'A1 B2 C3 D4', 'D1 C2 B3 A4')
            );
        });
        it('should handle deleted source anchor', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), moveComments(1, 'A2 B3 C3 D4', 'A5 B5 A3 D5'),
                [deleteComment(1, 'B5', 0), deleteComment(1, 'A3', 0), deleteRows(1, '3')], moveComments(1, 'A2 D3', 'A4 D4')
            );
        });
        it('should handle deleted target anchor', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), moveComments(1, 'A5 B5 C5', 'A2 B3 C4'),
                [deleteComment(1, 'B3', 0), deleteRows(1, '3')], [deleteComment(1, 'B4', 0), moveComments(1, 'A4 C4', 'A2 C3')]
            );
        });
        it('should not transform cell anchor in different sheets', function () {
            -testRunner.runBidiTest(deleteRows(1, '3'), moveComments(2, 'D4', 'E5'));
        });
    });
*/

    @Test
    public void deleteRowsMoveComments01() throws JSONException {
        // Result: Should transform cell anchors.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  moveComments(1, 'A1 B2 C4 D5', 'D1 C2 B4 A5'),
        //  deleteRows(1, '3'),
        //  moveComments(1, 'A1 B2 C3 D4', 'D1 C2 B3 A4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(1, "A1 B2 C4 D5", "D1 C2 B4 A5").toString(),
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(1, "A1 B2 C3 D4", "D1 C2 B3 A4").toString()
        );
    }

    @Test
    public void deleteRowsMoveComments02() throws JSONException {
        // Result: Should handle deleted source anchor.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  moveComments(1, 'A2 B3 C3 D4', 'A5 B5 A3 D5'),
        //  [deleteComment(1, 'B5', 0), deleteComment(1, 'A3', 0), deleteRows(1, '3')],
        //  moveComments(1, 'A2 D3', 'A4 D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(1, "A2 B3 C3 D4", "A5 B5 A3 D5").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "B5", 0),
                SheetHelper.createDeleteCommentOp(1, "A3", 0),
                SheetHelper.createDeleteRowsOp(1, "3")
            ),
            SheetHelper.createMoveCommentsOp(1, "A2 D3", "A4 D4").toString()
        );
    }

    @Test
    public void deleteRowsMoveComments03() throws JSONException {
        // Result: Should handle deleted target anchor.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  moveComments(1, 'A5 B5 C5', 'A2 B3 C4'),
        //  [deleteComment(1, 'B3', 0), deleteRows(1, '3')],
        //  [deleteComment(1, 'B4', 0), moveComments(1, 'A4 C4', 'A2 C3')]);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(1, "A5 B5 C5", "A2 B3 C4").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "B3", 0),
                SheetHelper.createDeleteRowsOp(1, "3")
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCommentOp(1, "B4", 0),
                SheetHelper.createMoveCommentsOp(1, "A4 C4", "A2 C3")
            )
        );
    }

    @Test
    public void deleteRowsMoveComments04() throws JSONException {
        // Result: Should not transform cell anchor in different sheets.
        // testRunner.runBidiTest(
        //  deleteRows(1, '3'),
        //  moveComments(2, 'D4', 'E5'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteRowsOp(1, "3").toString(),
            SheetHelper.createMoveCommentsOp(2, "D4", "E5").toString()
        );
    }

/*
 * NOT IMPLMENTED -> nothing is transformed and we do not have warnings in the backend transformation
 *
    describe('"deleteRows" and drawing anchor', function () {
        it('should transform drawing anchor', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 A2 10 10 B3 20 20', '2 A3 10 10 B4 20 20', '2 A4 10 10 B5 20 20'),
                deleteRows(1, '3'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 A2 10 10 B3 20 0',  '2 A3 10 0 B3 20 20',  '2 A3 10 10 B4 20 20'),
                { warnings: true } // missing transformation of absolute drawing position
            );
            testRunner.runBidiTest(
                deleteRows(1, '3:4 6:7'), drawingAnchorOps(1, '2 A1 10 10 B4 20 20', '2 A2 10 10 B5 20 20', '2 A3 10 10 B6 20 20', '2 A5 10 10 B8 20 20'),
                deleteRows(1, '3:4 6:7'), drawingAnchorOps(1, '2 A1 10 10 B3 20 0',  '2 A2 10 10 B3 20 20', '2 A3 10 0 B4 20 0',   '2 A3 10 10 B4 20 20'),
                { warnings: true } // missing transformation of absolute drawing position
            );
        });
        var DEL_ANCHOR_O = { drawing: { anchor: '2 C3 10 10 D4 20 20' } };  // original
        var DEL_ANCHOR_T = { drawing: { anchor: '2 C2 10 0 D2 20 0' } };    // transformed
        it('should handle deleted drawing anchor of drawing objects', function () {
            testRunner.runBidiTest(
                deleteRows(1, '2:5'), [insertShape(1, 0, DEL_ANCHOR_O), changeDrawing(1, 1, DEL_ANCHOR_O)],
                [deleteDrawing(1, 0), deleteDrawing(1, 0), deleteRows(1, '2:5')], []
            );
        });
        it('should handle deleted drawing anchor of notes and comments', function () {
            testRunner.runBidiTest(
                deleteRows(1, '2:5'), [insertNote(1, 'A1', 'abc', DEL_ANCHOR_O), changeNote(1, 'A1', DEL_ANCHOR_O), insertComment(1, 'A1', 0, { attrs: DEL_ANCHOR_O }), changeComment(1, 'A1', 0, { attrs: DEL_ANCHOR_O })],
                deleteRows(1, '2:5'), [insertNote(1, 'A1', 'abc', DEL_ANCHOR_T), changeNote(1, 'A1', DEL_ANCHOR_T), insertComment(1, 'A1', 0, { attrs: DEL_ANCHOR_T }), changeComment(1, 'A1', 0, { attrs: DEL_ANCHOR_T })],
                { warnings: true });
        });
        it('should log warnings for absolute anchor', function () {
            var anchorOps = drawingAnchorOps(1, '2 A1 0 0 C3 0 0');
            testRunner.expectBidiWarnings(deleteRows(1, '3'), anchorOps, anchorOps.length);
        });
        it('should not log warnings without anchor attribute', function () {
            testRunner.expectBidiWarnings(deleteRows(1, '3'), drawingNoAnchorOps(1), 0);
        });
        it('should not transform drawing anchor in different sheets', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), drawingAnchorOps(2, '2 A1 10 10 D4 20 20'));
        });
    });
*/

/*
 * NOT IMPLMENTED -> NO SHEET SELECTION ON THE SERVER
 *
    describe('"deleteRows" and "sheetSelection"', function () {
        it('should transform selected ranges', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3:4 6:7'), sheetSelection(1, 'A1:C3 D4:F6 G7:I9', 0, 'A1', [0, 1]),
                deleteRows(1, '3:4 6:7'), sheetSelection(1, 'A1:C2 D3:F3 G4:I5', 0, 'A1', [0, 1])
            );
        });
        it('should transform active address', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3:4 6:7'), opSeries2(sheetSelection, 1, 'A1:D1048576', 0, ['A1', 'B2', 'C3', 'D4', 'E5', 'F6', 'G7', 'H8', 'I9']),
                deleteRows(1, '3:4 6:7'), opSeries2(sheetSelection, 1, 'A1:D1048576', 0, ['A1', 'B2', 'C3', 'D3', 'E3', 'F4', 'G4', 'H4', 'I5'])
            );
        });
        it('should transform origin address', function () {
            testRunner.runBidiTest(
                deleteRows(1, '3:4 6:7'), opSeries2(sheetSelection, 1, 'A1:D1048576', 0, 'A1', ['A1', 'B2', 'C3', 'D4', 'E5', 'F6', 'G7', 'H8', 'I9']),
                deleteRows(1, '3:4 6:7'), opSeries2(sheetSelection, 1, 'A1:D1048576', 0, 'A1', ['A1', 'B2', 'C3', 'D3', 'E3', 'F4', 'G4', 'H4', 'I5'])
            );
        });
        it('should transform deleted active range', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), sheetSelection(1, 'E5 D4 C3 B2 A1', 0, 'E5', 'E5'), null, sheetSelection(1, 'E4 D3 B2 A1', 0, 'E4', 'E4'));
            testRunner.runBidiTest(deleteRows(1, '3'), sheetSelection(1, 'E5 D4 C3 B2 A1', 1, 'D4', 'D4'), null, sheetSelection(1, 'E4 D3 B2 A1', 1, 'D3', 'D3'));
            testRunner.runBidiTest(deleteRows(1, '3'), sheetSelection(1, 'E5 D4 C3 B2 A1', 2, 'C3', 'C3'), null, sheetSelection(1, 'E4 D3 B2 A1', 0, 'E4'));
            testRunner.runBidiTest(deleteRows(1, '3'), sheetSelection(1, 'E5 D4 C3 B2 A1', 3, 'B2', 'B2'), null, sheetSelection(1, 'E4 D3 B2 A1', 2, 'B2', 'B2'));
            testRunner.runBidiTest(deleteRows(1, '3'), sheetSelection(1, 'E5 D4 C3 B2 A1', 4, 'A1', 'A1'), null, sheetSelection(1, 'E4 D3 B2 A1', 3, 'A1', 'A1'));
        });
        it('should keep active cell when deleting the selection', function () {
            testRunner.runBidiTest(deleteRows(1, '3:4'), sheetSelection(1, 'F3:I4 A3:D4', 0, 'G3'), null, sheetSelection(1, 'G3', 0, 'G3'));
            testRunner.runBidiTest(deleteRows(1, '3:4'), sheetSelection(1, 'F3:I4 A3:D4', 1, 'B4'), null, sheetSelection(1, 'B3', 0, 'B3'));
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(deleteRows(1, '3'), sheetSelection(2, 'A1:D4', 0, 'A1'));
        });
    });
*/
}
