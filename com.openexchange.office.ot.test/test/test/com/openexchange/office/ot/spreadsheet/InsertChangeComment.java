/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertChangeComment {

    /*
     *  describe('"insertComment" and "changeComment"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runBidiTest(insertComment(1, 'A1', 0), changeComment(2, 'A1', 0));
                testRunner.runBidiTest(insertComment(1, 'A1', 0), changeComment(2, 'B2', 0));
            });
            it('should shift comment index', function () {
                testRunner.runBidiTest(insertComment(1, 'A1', 0), opSeries2(changeComment, 1, 'A1', [0, 1, 2]), null, opSeries2(changeComment, 1, 'A1', [1, 2, 3]));
                testRunner.runBidiTest(insertComment(1, 'A1', 1), opSeries2(changeComment, 1, 'A1', [0, 1, 2]), null, opSeries2(changeComment, 1, 'A1', [0, 2, 3]));
                testRunner.runBidiTest(insertComment(1, 'A1', 2), opSeries2(changeComment, 1, 'A1', [0, 1, 2]), null, opSeries2(changeComment, 1, 'A1', [0, 1, 3]));
            });
        });
     */

    @Test
    public void insertCommentInsertComment01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  changeComment(2, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createChangeCommentOp(2, "A1", 0, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  changeComment(2, 'B2', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createChangeCommentOp(2, "B2", 0, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment03() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 0),
        //  opSeries2(changeComment, 1, 'A1', [0, 1, 2]),
        //  null,
        //  opSeries2(changeComment, 1, 'A1', [1, 2, 3]));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 2, null)
            ),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 2, null),
                Helper.createChangeCommentOp(1, "A1", 3, null)
            )
        );
    }

    @Test
    public void insertCommentInsertComment04() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 1),
        //  opSeries2(changeComment, 1, 'A1', [0, 1, 2]),
        //  null,
        //  opSeries2(changeComment, 1, 'A1', [0, 2, 3]));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 2, null)
            ),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 2, null),
                Helper.createChangeCommentOp(1, "A1", 3, null)
            )
        );
    }

    @Test
    public void insertCommentInsertComment05() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runBidiTest(
        //  insertComment(1, 'A1', 2),
        //  opSeries2(changeComment, 1, 'A1', [0, 1, 2]),
        //  null,
        //  opSeries2(changeComment, 1, 'A1', [0, 1, 3]));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 2, null)
            ),
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            Helper.createArrayFromJSON(
                Helper.createChangeCommentOp(1, "A1", 0, null),
                Helper.createChangeCommentOp(1, "A1", 1, null),
                Helper.createChangeCommentOp(1, "A1", 3, null)
            )
        );
    }
}
