/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeCFRule {

/*
        describe('"changeCFRule" and "changeCFRule"', function () {
            it('should skip different rules', function () {
                testRunner.runTest(changeCFRule(1, 'R1', 'A1:D4'), changeCFRule(2, 'R1', 'A1:D4'));
                testRunner.runTest(changeCFRule(1, 'R1', 'A1:D4'), changeCFRule(1, 'R2', 'E5:H8'));
            });
            it('should reduce various properties', function () {
                testRunner.runTest(changeCFRule(1, 'R1', 'A1:D4'),                changeCFRule(1, 'R1', 'B2:E5'),                 null, []);
                testRunner.runTest(changeCFRule(1, 'R1', 'A1:D4'),                changeCFRule(1, 'R1', 'A1:D4'),                 [],   []);
                testRunner.runTest(changeCFRule(1, 'R1', { type: 'formula' }),    changeCFRule(1, 'R1', { type: 'formula' }),     [],   []);
                testRunner.runTest(changeCFRule(1, 'R1', { priority: 1 }),        changeCFRule(1, 'R1', { priority: 2 }),         null, []);
                testRunner.runTest(changeCFRule(1, 'R1', { priority: 1 }),        changeCFRule(1, 'R1', { priority: 1 }),         [],   []);
                testRunner.runTest(changeCFRule(1, 'R1', { stop: true }),         changeCFRule(1, 'R1', { stop: false }),         null, []);
                testRunner.runTest(changeCFRule(1, 'R1', { stop: true }),         changeCFRule(1, 'R1', { stop: true }),          [],   []);
                testRunner.runTest(changeCFRule(1, 'R1', { attrs: ATTRS_I1 }),    changeCFRule(1, 'R1', { attrs: ATTRS_I2 }),     null, []);
                testRunner.runTest(changeCFRule(1, 'R1', { attrs: ATTRS_I1 }),    changeCFRule(1, 'R1', { attrs: ATTRS_I1 }),     [],   []);
            });
            it('should succeed to create overlapping rules', function () {
                testRunner.runTest(changeCFRule(1, 'R1', 'A1:D4'), changeCFRule(1, 'R2', 'B2:E5'));
            });
            it('should fail to reduce type/value properties', function () {
                testRunner.expectError(changeDVRule(1, 1, { type: 'all' }), changeDVRule(1, 1, { type: 'list' }));
                testRunner.expectBidiError(changeDVRule(1, 1, { type: 'all' }), changeDVRule(1, 1, { compare: 'between' }));
                testRunner.expectBidiError(changeDVRule(1, 1, { type: 'all' }), changeDVRule(1, 1, { value1: 'A1' }));
                testRunner.expectBidiError(changeDVRule(1, 1, { type: 'all' }), changeDVRule(1, 1, { value2: 'A1' }));
            });
        });
*/

    @Test
    public void changeCFRuleChangeCFRule01() throws JSONException {
        // Result: Should skip different rules.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(2, 'R1', 'A1:D4'));
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(2, "R1", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCFRuleChangeCFRule02() throws JSONException {
        // Result: Should skip different rules.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(1, 'R2', 'E5:H8'));
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R2", "E5:H8", null).toString()
        );
    }

    @Test
    public void changeCFRuleChangeCFRule03() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(1, 'R1', 'B2:E5'),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", "B2:E5", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null).toString(),
            "[]"
        );
    }

    @Test
    public void changeCFRuleChangeCFRule04() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(1, 'R1', 'A1:D4'),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeCFRuleChangeCFRule05() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', { type: 'formula' }),
        //  changeCFRule(1, 'R1', { type: 'formula' }),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ type: 'formula' }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ type: 'formula' }").toString()).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeCFRuleChangeCFRule06() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', { priority: 1 }),
        //  changeCFRule(1, 'R1', { priority: 2 }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ priority: 1 }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ priority: 2 }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ priority: 1 }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeCFRuleChangeCFRule07() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', { priority: 1 }),
        //  changeCFRule(1, 'R1', { priority: 1 }),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ priority: 1 }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ priority: 1 }").toString()).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeCFRuleChangeCFRule08() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', { stop: true }),
        //  changeCFRule(1, 'R1', { stop: false }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ stop: true }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ stop: false }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ stop: true }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeCFRuleChangeCFRule09() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', { stop: true }),
        //  changeCFRule(1, 'R1', { stop: true }),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ stop: true }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ stop: true }").toString()).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeCFRuleChangeCFRule10() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', { attrs: ATTRS_I1 }),
        //  changeCFRule(1, 'R1', { attrs: ATTRS_I2 }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + " }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS_I2.toString() + " }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + " }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeCFRuleChangeCFRule11() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', { attrs: ATTRS_I1 }),
        //  changeCFRule(1, 'R1', { attrs: ATTRS_I1 }),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + " }").toString()).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", null, Helper.createJSONObject("{ attrs: " + SheetHelper.ATTRS_I1.toString() + " }").toString()).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeCFRuleChangeCFRule12() throws JSONException {
        // Result: Should succeed to create overlapping rules.
        // testRunner.runTest(
        //  changeCFRule(1, 'R1', 'A1:D4'),
        //  changeCFRule(1, 'R2', 'B2:E5'));
        SheetHelper.runTest(
            SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", null).toString(),
            SheetHelper.createChangeCFRuleOp(1, "R2", "B2:E5", null).toString()
        );
    }

    @Test
    public void changeCFRuleChangeCFRule13() throws JSONException {
        // Result: Should fail to reduce type/value properties.
        // testRunner.expectError(
        //  changeDVRule(1, 1, { type: 'all' }),
        //  changeDVRule(1, 1, { type: 'list' }));
        SheetHelper.expectError(
            SheetHelper.createChangeDVRuleOp(1, "1", null, Helper.createJSONObject("{ type: 'all' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, "1", null, Helper.createJSONObject("{ type: 'list' }").toString()).toString()
        );
    }

    @Test
    public void changeCFRuleChangeCFRule14() throws JSONException {
        // Result: Should fail to reduce type/value properties.
        // testRunner.expectBidiError(
        //  changeDVRule(1, 1, { type: 'all' }),
        //  changeDVRule(1, 1, { compare: 'between' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeDVRuleOp(1, "1", null, Helper.createJSONObject("{ type: 'all' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, "1", null, Helper.createJSONObject("{ value1: 'between' }").toString()).toString()
        );
    }

    @Test
    public void changeCFRuleChangeCFRule15() throws JSONException {
        // Result: Should fail to reduce type/value properties.
        // testRunner.expectBidiError(
        //  changeDVRule(1, 1, { type: 'all' }),
        //  changeDVRule(1, 1, { value1: 'A1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeDVRuleOp(1, "1", null, Helper.createJSONObject("{ type: 'all' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, "1", null, Helper.createJSONObject("{ value1: 'A1' }").toString()).toString()
        );
    }

    @Test
    public void changeCFRuleChangeCFRule16() throws JSONException {
        // Result: Should fail to reduce type/value properties.
        // testRunner.expectBidiError(testRunner.expectBidiError(
        //  changeDVRule(1, 1, { type: 'all' }),
        //  changeDVRule(1, 1, { value2: 'A1' }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeDVRuleOp(1, "1", null, Helper.createJSONObject("{ type: 'all' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, "1", null, Helper.createJSONObject("{ value2: 'A1' }").toString()).toString()
        );
    }
}
