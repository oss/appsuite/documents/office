/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import com.openexchange.office.ot.spreadsheet.*;
import test.com.openexchange.office.ot.tools.Helper;

public class MoveCells {

/*
    describe('"moveCells" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(moveCells(1, 'A1:B2', 'down'), [
                GLOBAL_OPS, STYLESHEET_OPS,
                deleteName('a', 1), deleteTable(1, 'T'), changeTableCol(1, 'T', 1, { attrs: ATTRS })
            ]);
        });
    });
*/

    @Test
    public void MergeCells01() throws JSONException {

        SheetHelper.runBidiTest(SheetHelper.createMoveCellsOp(1, "A1:B2", Direction.DOWN).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createDeleteNameOp("a", 1),
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createChangeTableColOp(1, "T", 1, "{ attrs: " + SheetHelper.ATTRS + "}")));
    }
}
