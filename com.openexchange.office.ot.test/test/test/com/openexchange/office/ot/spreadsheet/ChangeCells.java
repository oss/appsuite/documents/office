/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeCells {

/*
    describe('"changeCells" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(changeCells(1, { A1: 42 }), [
                GLOBAL_OPS, STYLESHEET_OPS, mergeCells(1, 'A1:B2'), hlinkOps(1), nameOps(1),
                deleteTable(1, 'T'), changeTableCol(1, 'T', 1, { attrs: ATTRS }), dvRuleOps(1), cfRuleOps(1),
                noteOps(1), commentOps(1), selectOps(1),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/

    @Test
    public void changeCellsIndependentOperations01() throws JSONException {
        // Result: Should skip transformations.
        // testRunner.runBidiTest(changeCells(1, { A1: 42 }),
        //  [
        //  GLOBAL_OPS, STYLESHEET_OPS, mergeCells(1, 'A1:B2'), hlinkOps(1), nameOps(1),
        //  deleteTable(1, 'T'), changeTableCol(1, 'T', 1, { attrs: ATTRS }), dvRuleOps(1), cfRuleOps(1),
        //  noteOps(1), commentOps(1), selectOps(1),
        //  drawingOps(1), chartOps(1), drawingTextOps(1)
        // ]);
        SheetHelper.runTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 42 }" )).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createMergeCellsOp(1, "A1:B2", null),
                SheetHelper.createHlinkOps("1", new String[] {}),
                SheetHelper.createNameOps("1"),
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createChangeTableColOp(1, "T", 1, new JSONObject("{ attrs: " + SheetHelper.ATTRS + " }").toString()),
                SheetHelper.createDvRuleOps("1", null),
                SheetHelper.createCfRuleOps("1", null),
                SheetHelper.createNoteOps("1"),
                SheetHelper.createCommentOps("1"),
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

/*
    describe('"changeCells" and "changeCells"', function () {
        it('should not modify independent values', function () {
            testRunner.runTest(
                changeCells(1, { A1: 42,    A2: { e: '#NAME?' } }),
                changeCells(1, { B1: 'abc', B2: { e: '#NULL!' } })
            );
        });
        it('should prefer values from local operation', function () {
            testRunner.runTest(
                changeCells(1, { A1: 42,    A2: { e: '#NAME?' }, A3: 42,              A4: { e: '#NAME?' } }),
                changeCells(1, { A1: 'abc', A2: 'abc',           A3: { e: '#NULL!' }, A4: { e: '#NULL!' } }),
                changeCells(1, { A1: 42,    A2: { e: '#NAME?' }, A3: 42,              A4: { e: '#NAME?' } }),
                []
            );
        });
        it('should delete equal values from local operation', function () {
            testRunner.runTest(
                changeCells(1, { A1: 42, A2: { e: '#NAME?' }, A3: 42 }),
                changeCells(1, { A1: 42, A2: { e: '#NAME?' }, A4: 42 }),
                changeCells(1, { A3: 42 }),
                changeCells(1, { A4: 42 })
            );
        });
        it('should handle the "f" property', function () {
            testRunner.runTest(
                changeCells(1, { A1: { f: '1' }, A2: { f: '2' }, A3: { f: '3' } }),
                changeCells(1, { A1: { f: '1' }, A2: { f: '3' }, A4: { f: '4' } }),
                changeCells(1, { A2: { f: '2' }, A3: { f: '3' } }),
                changeCells(1, { A4: { f: '4' } })
            );
        });
        it('should handle the "s" property', function () {
            testRunner.runTest(
                changeCells(1, { A1: { s: 'a1' }, A2: { s: 'a2' }, A3: { s: 'a3' } }),
                changeCells(1, { A1: { s: 'a1' }, A2: { s: 'a3' }, A4: { s: 'a4' } }),
                changeCells(1, { A2: { s: 'a2' }, A3: { s: 'a3' } }),
                changeCells(1, { A4: { s: 'a4' } })
            );
        });
        it('should decompose overlapping ranges', function () {
            testRunner.runTest(
                changeCells(1, { 'A1:B2': 42, C3: 42 }),
                changeCells(1, { 'B2:C3': 'abc' }),
                changeCells(1, { 'A1:B1': 42, A2: 42, B2: 42, C3: 42 }),
                changeCells(1, { C2: 'abc', B3: 'abc' })
            );
        });
        it('should handle the "u" flag', function () {
            testRunner.runTest(
                changeCells(1, { A1: { u: true }, A2: { u: true }, A3: { u: true, v: 42 }, A4: 42,                       A5: 42 }),
                changeCells(1, { A1: { u: true }, A2: 42,          A3: 42,                 A4: { u: true, e: '#NAME?' }, A5: { u: true, v: 42 } }),
                changeCells(1, {                  A2: { u: true }, A3: { u: true, v: 42 }, A4: 42 }),
                changeCells(1, {                                                           A4: { u: true, v: 42 },       A5: { u: true, v: 42 } })
            );
        });
        it('should fail to transform the "sr" property', function () {
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }), changeCells(1, { A1: { f: '1' } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }), changeCells(1, { A1: { f: null } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }), changeCells(1, { A1: { si: 2 } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }), changeCells(1, { A1: { si: null } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }), changeCells(1, { A1: { sr: 'A1:C3' } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }), changeCells(1, { A1: { sr: null } }));
        });
        it('should fail to transform the "mr" property', function () {
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }), changeCells(1, { B2: { f: '1' } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }), changeCells(1, { B2: { f: null } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }), changeCells(1, { B2: { si: 1 } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }), changeCells(1, { B2: { si: null } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }), changeCells(1, { B2: { sr: 'B2' } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }), changeCells(1, { B2: { sr: null } }));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }), changeCells(1, { B2: { mr: 'B2:C3' } }));
            -testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }), changeCells(1, { B2: { mr: null } }));
        });
    });
*/

    @Test
    public void changeCellsChangeCells01() throws JSONException {
        // Result: Should not modify independent values.
        // testRunner.runTest(
        //  changeCells(1, { A1: 42,    A2: { e: '#NAME?' } }),
        //  changeCells(1, { B1: 'abc', B2: { e: '#NULL!' } }));
        SheetHelper.runTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 42,    A2: { e: '#NAME?' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B1: 'abc', B2: { e: '#NULL!' } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells02() throws JSONException {
        // Result: Should prefer values from local operation.
        // testRunner.runTest(
        //  changeCells(1, { A1: 42,    A2: { e: '#NAME?' }, A3: 42,              A4: { e: '#NAME?' } }),
        //  changeCells(1, { A1: 'abc', A2: 'abc',           A3: { e: '#NULL!' }, A4: { e: '#NULL!' } }),
        //  changeCells(1, { A1: 42,    A2: { e: '#NAME?' }, A3: 42,              A4: { e: '#NAME?' } }),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 42,    A2: { e: '#NAME?' }, A3: 42,              A4: { e: '#NAME?' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 'abc', A2: 'abc',           A3: { e: '#NULL!' }, A4: { e: '#NULL!' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 42,    A2: { e: '#NAME?' }, A3: 42,              A4: { e: '#NAME?' } }" )).toString(),
            "[]"
        );
    }

    @Test
    public void changeCellsChangeCells03() throws JSONException {
        // Result: Should delete equal values from local operation.
        // testRunner.runTest(
        //  changeCells(1, { A1: 42, A2: { e: '#NAME?' }, A3: 42 }),
        //  changeCells(1, { A1: 42, A2: { e: '#NAME?' }, A4: 42 }),
        //  changeCells(1, { A3: 42 }),
        //  changeCells(1, { A4: 42 }));
        SheetHelper.runTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 42, A2: { e: '#NAME?' }, A3: 42 }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 42, A2: { e: '#NAME?' }, A4: 42 }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A3: 42 }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A4: 42 }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells04() throws JSONException {
        // Result: Should handle the "f" property.
        // testRunner.runTest(
        //  changeCells(1, { A1: { f: '1' }, A2: { f: '2' }, A3: { f: '3' } }),
        //  changeCells(1, { A1: { f: '1' }, A2: { f: '3' }, A4: { f: '4' } }),
        //  changeCells(1, { A2: { f: '2' }, A3: { f: '3' } }),
        //  changeCells(1, { A4: { f: '4' } }));
        SheetHelper.runTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1' }, A2: { f: '2' }, A3: { f: '3' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1' }, A2: { f: '3' }, A4: { f: '4' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A2: { f: '2' }, A3: { f: '3' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A4: { f: '4' } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells05() throws JSONException {
        // Result: Should handle the "s" property.
        // testRunner.runTest(
        //  changeCells(1, { A1: { s: 'a1' }, A2: { s: 'a2' }, A3: { s: 'a3' } }),
        //  changeCells(1, { A1: { s: 'a1' }, A2: { s: 'a3' }, A4: { s: 'a4' } }),
        //  changeCells(1, { A2: { s: 'a2' }, A3: { s: 'a3' } }),
        //  changeCells(1, { A4: { s: 'a4' } }));
        SheetHelper.runTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { s: 'a1' }, A2: { s: 'a2' }, A3: { s: 'a3' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { s: 'a1' }, A2: { s: 'a3' }, A4: { s: 'a4' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A2: { s: 'a2' }, A3: { s: 'a3' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A4: { s: 'a4' } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells06() throws JSONException {
        // Result: Should decompose overlapping ranges.
        // testRunner.runTest(
        //  changeCells(1, { 'A1:B2': 42, C3: 42 }),
        //  changeCells(1, { 'B2:C3': 'abc' }),
        //  changeCells(1, { 'A1:B1': 42, A2: 42, B2: 42, C3: 42 }),
        //  changeCells(1, { C2: 'abc', B3: 'abc' }));
        SheetHelper.runTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ 'A1:B2': 42, C3: 42 }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ 'B2:C3': 'abc' }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ 'A1:B1': 42, A2: 42, B2: 42, C3: 42 }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ C2: 'abc', B3: 'abc' }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells07() throws JSONException {
        // Result: Should handle the "u" flag.
        // testRunner.runTest(
        //  changeCells(1, { A1: { u: true }, A2: { u: true }, A3: { u: true, v: 42 }, A4: 42,                       A5: 42 }),
        //  changeCells(1, { A1: { u: true }, A2: 42,          A3: 42,                 A4: { u: true, e: '#NAME?' }, A5: { u: true, v: 42 } }),
        //  changeCells(1, {                  A2: { u: true }, A3: { u: true, v: 42 }, A4: 42 }),
        //  changeCells(1, {                                                           A4: { u: true, v: 42 },       A5: { u: true, v: 42 } }));
        SheetHelper.runTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { u: true }, A2: { u: true }, A3: { u: true, v: 42 }, A4: 42,                       A5: 42 }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { u: true }, A2: 42,          A3: 42,                 A4: { u: true, e: '#NAME?' }, A5: { u: true, v: 42 } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{                  A2: { u: true }, A3: { u: true, v: 42 }, A4: 42 }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{                                                           A4: { u: true, v: 42 },       A5: { u: true, v: 42 } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells08() throws JSONException {
        // Result: Should fail to transform the "sr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }),
        //  changeCells(1, { A1: { f: '1' } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', si: 1, sr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1' } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells09() throws JSONException {
        // Result: Should fail to transform the "sr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }),
        //  changeCells(1, { A1: { f: null } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', si: 1, sr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: null } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells10() throws JSONException {
        // Result: Should fail to transform the "sr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }),
        //  changeCells(1, { A1: { si: 2 } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', si: 1, sr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { si: 2 } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells11() throws JSONException {
        // Result: Should fail to transform the "sr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }),
        //  changeCells(1, { A1: { si: null } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', si: 1, sr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { si: null } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells12() throws JSONException {
        // Result: Should fail to transform the "sr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }),
        //  changeCells(1, { A1: { sr: 'A1:C3' } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', si: 1, sr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { sr: 'A1:C3' } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells13() throws JSONException {
        // Result: Should fail to transform the "sr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', si: 1, sr: 'A1:B2' } }),
        //  changeCells(1, { A1: { sr: null } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', si: 1, sr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { sr: null } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells14() throws JSONException {
        // Result: Should fail to transform the "mr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }),
        //  changeCells(1, { B2: { f: '1' } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { f: '1' } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells15() throws JSONException {
        // Result: Should fail to transform the "mr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }),
        //  changeCells(1, { B2: { f: null } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { f: null } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells16() throws JSONException {
        // Result: Should fail to transform the "mr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }),
        //  changeCells(1, { B2: { si: 1 } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { si: 1 } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells17() throws JSONException {
        // Result: Should fail to transform the "mr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }),
        //  changeCells(1, { B2: { si: null } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { si: null } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells18() throws JSONException {
        // Result: Should fail to transform the "mr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }),
        //  changeCells(1, { B2: { sr: 'B2' } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { sr: 'B2' } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells19() throws JSONException {
        // Result: Should fail to transform the "mr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }),
        //  changeCells(1, { B2: { sr: null } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { sr: null } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells20() throws JSONException {
        // Result: Should fail to transform the "mr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }),
        //  changeCells(1, { B2: { mr: 'B2:C3' } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { mr: 'B2:C3' } }" )).toString()
        );
    }

    @Test
    public void changeCellsChangeCells21() throws JSONException {
        // Result: Should fail to transform the "mr" property.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }),
        //  changeCells(1, { B2: { mr: null } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { mr: null } }" )).toString()
        );
    }

/*
    describe('"changeCells" and "mergeCells"', function () {
        it('should skip different sheets', function () {
            testRunner.runBidiTest(changeCells(1, { B2: { f: '1', mr: 'B2:C3' } }), mergeCells(0, 'A1:B2', MM_MERGE));
        });
        it('should fail to merge over matrix formula', function () {
            testRunner.expectBidiError(changeCells(1, { B2: { f: '1', mr: 'B2:C3' } }), mergeCells(1, 'A1:B2', MM_MERGE));
            testRunner.expectBidiError(changeCells(1, { B2: { f: '1', mr: 'B2:C3' } }), mergeCells(1, 'A1:B2', MM_HORIZONTAL));
            testRunner.expectBidiError(changeCells(1, { B2: { f: '1', mr: 'B2:C3' } }), mergeCells(1, 'A1:B2', MM_VERTICAL));
        });
        it('should allow to unmerge over matrix formula', function () {
            testRunner.runBidiTest(changeCells(1, { B2: { f: '1', mr: 'B2:C3' } }), mergeCells(1, 'A1:B2', MM_UNMERGE));
        });
    });
*/

    @Test
    public void changeCellsMergeCells01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:B2' } }),
        //  changeCells(1, { B2: { mr: null } }));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:B2' } }" )).toString(),
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { mr: null } }" )).toString()
        );
    }

    @Test
    public void changeCellsMergeCells02() throws JSONException {
        // Result: Should fail to merge over matrix formula.
        // testRunner.expectBidiError(
        //  changeCells(1, { B2: { f: '1', mr: 'B2:C3' } }),
        //  mergeCells(1, 'A1:B2', MM_MERGE));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { f: '1', mr: 'B2:C3' } }" )).toString(),
            SheetHelper.createMergeCellsOp(1,  "A1:B2", SheetHelper.MM_MERGE).toString()
        );
    }

    @Test
    public void changeCellsMergeCells03() throws JSONException {
        // Result: Should fail to merge over matrix formula.
        // testRunner.expectBidiError(
        //  changeCells(1, { B2: { f: '1', mr: 'B2:C3' } }),
        //  mergeCells(1, 'A1:B2', MM_HORIZONTAL));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { f: '1', mr: 'B2:C3' } }" )).toString(),
            SheetHelper.createMergeCellsOp(1,  "A1:B2", SheetHelper.MM_HORIZONTAL).toString()
        );
    }

    @Test
    public void changeCellsMergeCells04() throws JSONException {
        // Result: Should fail to merge over matrix formula.
        // testRunner.expectBidiError(
        //  changeCells(1, { B2: { f: '1', mr: 'B2:C3' } }),
        //  mergeCells(1, 'A1:B2', MM_VERTICAL));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { f: '1', mr: 'B2:C3' } }" )).toString(),
            SheetHelper.createMergeCellsOp(1,  "A1:B2", SheetHelper.MM_VERTICAL).toString()
        );
    }

    @Test
    public void changeCellsMergeCells05() throws JSONException {
        // Result: Should allow to unmerge over matrix formula.
        // testRunner.runBidiTest(
        //  changeCells(1, { B2: { f: '1', mr: 'B2:C3' } }),
        //  mergeCells(1, 'A1:B2', MM_UNMERGE));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B2: { f: '1', mr: 'B2:C3' } }" )).toString(),
            SheetHelper.createMergeCellsOp(1,  "A1:B2", SheetHelper.MM_UNMERGE).toString()
        );
    }

/*
    describe('"changeCells" and "insertTable"', function () {
        it('should fail to change values of a table header', function () {
            testRunner.runBidiTest(changeCells(1, { A2: 'a' }), insertTable(1, 'T', 'A1:D4'));
            testRunner.runBidiTest(changeCells(1, { A1: { s: 'a1' } }), insertTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(changeCells(1, { A1: 'a' }), insertTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(changeCells(1, { B1: { v: 42 } }), insertTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(changeCells(1, { C1: { f: '1' } }), insertTable(1, 'T', 'A1:D4'));
        });
        it('should fail to insert matrix formula over a table range', function () {
            testRunner.runBidiTest(changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }), insertTable(1, 'T', 'E5:H8'));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }), insertTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }), insertTable(1, 'T', 'D4:G7'));
        });
        it('should skip different sheets', function () {
            testRunner.runBidiTest(changeCells(1, { A1: 'a' }), insertTable(2, 'T', 'A1:D4'));
            testRunner.runBidiTest(changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }), insertTable(2, 'T', 'A1:D4'));
        });
    });
*/

    @Test
    public void changeCellsInsertTable01() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.runBidiTest(
        //  changeCells(1, { A2: 'a' }),
        //  insertTable(1, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A2: 'a' }" )).toString(),
            SheetHelper.createInsertTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsInsertTable02() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.runBidiTest(
        //  changeCells(1, { A1: { s: 'a1' } }),
        //  insertTable(1, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { s: 'a1' } }" )).toString(),
            SheetHelper.createInsertTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsInsertTable03() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: 'a' }),
        //  insertTable(1, 'T', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 'a' }" )).toString(),
            SheetHelper.createInsertTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsInsertTable04() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.expectBidiError(
        //  changeCells(1, { B1: { v: 42 } }),
        //  insertTable(1, 'T', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B1: { v: 42 } }" )).toString(),
            SheetHelper.createInsertTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsInsertTable05() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.expectBidiError(
        //  changeCells(1, { C1: { f: '1' } }),
        //  insertTable(1, 'T', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ C1: { f: '1' } }" )).toString(),
            SheetHelper.createInsertTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsInsertTable06() throws JSONException {
        // Result: Should fail to insert matrix formula over a table range.
        // testRunner.runBidiTest(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }),
        //  insertTable(1, 'T', 'E5:H8'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:D4' } }" )).toString(),
            SheetHelper.createInsertTableOp(1,  "T", "E5:H8", null).toString()
        );
    }

    @Test
    public void changeCellsInsertTable07() throws JSONException {
        // Result: Should fail to insert matrix formula over a table range.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }),
        //  insertTable(1, 'T', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:D4' } }" )).toString(),
            SheetHelper.createInsertTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsInsertTable08() throws JSONException {
        // Result: Should fail to insert matrix formula over a table range.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }),
        //  insertTable(1, 'T', 'D4:G7'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:D4' } }" )).toString(),
            SheetHelper.createInsertTableOp(1,  "T", "D4:G7", null).toString()
        );
    }

    @Test
    public void changeCellsInsertTable09() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runBidiTest(
        //  changeCells(1, { A1: 'a' }),
        //  insertTable(2, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 'a' }" )).toString(),
            SheetHelper.createInsertTableOp(2,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsInsertTable10() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runBidiTest(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }),
        //  insertTable(2, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:D4' } }" )).toString(),
            SheetHelper.createInsertTableOp(2,  "T", "A1:D4", null).toString()
        );
    }

/*
    describe('"changeCells" and "changeTable"', function () {
        it('should fail to change values of a table header', function () {
            testRunner.runBidiTest(changeCells(1, { A2: 'a' }), changeTable(1, 'T', 'A1:D4'));
            testRunner.runBidiTest(changeCells(1, { A1: 'a' }), changeTable(1, 'T', { newName: 'T2' }));
            testRunner.runBidiTest(changeCells(1, { A1: { s: 'a1' } }), changeTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(changeCells(1, { A1: 'a' }), changeTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(changeCells(1, { B1: { v: 42 } }), changeTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(changeCells(1, { C1: { f: '1' } }), changeTable(1, 'T', 'A1:D4'));
        });
        it('should fail to insert matrix formula over a table range', function () {
            testRunner.runBidiTest(changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }), changeTable(1, 'T', 'E5:H8'));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }), changeTable(1, 'T', 'A1:D4'));
            testRunner.expectBidiError(changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }), changeTable(1, 'T', 'D4:G7'));
        });
        it('should skip different sheets', function () {
            testRunner.runBidiTest(changeCells(1, { A1: 'a' }), changeTable(2, 'T', 'A1:D4'));
            -testRunner.runBidiTest(changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }), changeTable(2, 'T', 'A1:D4'));
        });
    });
*/

    @Test
    public void changeCellsChangeTable01() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.runBidiTest(
        //  changeCells(1, { A2: 'a' }),
        //  changeTable(1, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A2: 'a' }" )).toString(),
            SheetHelper.createChangeTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsChangeTable02() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.runBidiTest(
        //  changeCells(1, { A1: 'a' }),
        //  changeTable(1, 'T', { newName: 'T2' }));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 'a' }" )).toString(),
            SheetHelper.createChangeTableOp(1,  "T", null, "{ newName: 'T2' }").toString()
        );
    }

    @Test
    public void changeCellsChangeTable03() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.runBidiTest(
        //  changeCells(1, { A1: { s: 'a1' } }),
        //  changeTable(1, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { s: 'a1' } }" )).toString(),
            SheetHelper.createChangeTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsChangeTable04() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: 'a' }),
        //  changeTable(1, 'T', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 'a' }" )).toString(),
            SheetHelper.createChangeTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsChangeTable05() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.expectBidiError(
        //  changeCells(1, { B1: { v: 42 } }),
        //  changeTable(1, 'T', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ B1: { v: 42 } }" )).toString(),
            SheetHelper.createChangeTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsChangeTable06() throws JSONException {
        // Result: Should fail to change values of a table header.
        // testRunner.expectBidiError(
        //  changeCells(1, { C1: { f: '1' } }),
        //  changeTable(1, 'T', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ C1: { f: '1' } }" )).toString(),
            SheetHelper.createChangeTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsChangeTable07() throws JSONException {
        // Result: Should fail to insert matrix formula over a table range.
        // testRunner.runBidiTest(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }),
        //  changeTable(1, 'T', 'E5:H8'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:D4' } }" )).toString(),
            SheetHelper.createChangeTableOp(1,  "T", "E5:H8", null).toString()
        );
    }

    @Test
    public void changeCellsChangeTable08() throws JSONException {
        // Result: Should fail to insert matrix formula over a table range.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }),
        //  changeTable(1, 'T', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:D4' } }" )).toString(),
            SheetHelper.createChangeTableOp(1,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsChangeTable09() throws JSONException {
        // Result: Should fail to insert matrix formula over a table range.
        // testRunner.expectBidiError(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }),
        //  changeTable(1, 'T', 'D4:G7'));
        SheetHelper.expectBidiError(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:D4' } }" )).toString(),
            SheetHelper.createChangeTableOp(1,  "T", "D4:G7", null).toString()
        );
    }

    @Test
    public void changeCellsChangeTable10() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runBidiTest(
        //  changeCells(1, { A1: 'a' }),
        //  changeTable(2, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: 'a' }" )).toString(),
            SheetHelper.createChangeTableOp(2,  "T", "A1:D4", null).toString()
        );
    }

    @Test
    public void changeCellsChangeTable11() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runBidiTest(
        //  changeCells(1, { A1: { f: '1', mr: 'A1:D4' } }),
        //  changeTable(2, 'T', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createChangeCellsOp(1,  new JSONObject( "{ A1: { f: '1', mr: 'A1:D4' } }" )).toString(),
            SheetHelper.createChangeTableOp(2,  "T", "A1:D4", null).toString()
        );
    }
}
