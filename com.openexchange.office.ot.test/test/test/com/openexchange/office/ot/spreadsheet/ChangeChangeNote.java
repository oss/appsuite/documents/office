/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeChangeNote {

    private final JSONObject    attrs                 = Helper.createJSONObject("{ f1: { a1: 10 } }");
    private final JSONObject    attrs2                 = Helper.createJSONObject("{ f1: { a1: 20 } }");
    // overlapping attribute sets (will be reduced)
    private final JSONObject    attrs01                 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } }");
    private final JSONObject    attrs02                 = Helper.createJSONObject("{ f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 40 } }");
    // the reduced result of ATTRS_Ox
    private final JSONObject    attrsR1                 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a2: 20, a3: 30 } }");
    private final JSONObject    attrsR2                 = Helper.createJSONObject("{ f3: { a1: 10 }, f2: { a4: 40 } }");
    // independent attribute sets (will not be reduced) ATTRS_I
    private final JSONObject    attrsI1                 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a1: 10 } }");
    private final JSONObject    attrsI2                 = Helper.createJSONObject("{ f2: { a2: 10 }, f3: { a1: 10 } }");

    /*
     * describe('"changeNote" and "changeNote"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runTest(changeNote(1, 'A1', 'abc'), changeNote(2, 'A1', 'abc'));
                testRunner.runTest(changeNote(1, 'A1', 'abc'), changeNote(1, 'B2', 'abc'));
            });
            it('should not process independent attributes', function () {
                testRunner.runTest(changeNote(1, 'A1', ATTRS_I1), changeNote(1, 'A1', ATTRS_I2));
            });
            it('should reduce attribute sets', function () {
                testRunner.runTest(
                    changeNote(1, 'A1', ATTRS_O1), changeNote(1, 'A1', ATTRS_O2),
                    changeNote(1, 'A1', ATTRS_R1), changeNote(1, 'A1', ATTRS_R2)
                );
            });
            it('should reduce note text', function () {
                testRunner.runTest(
                    changeNote(1, 'A1', 't1', ATTRS_I1), changeNote(1, 'A1', 't1', ATTRS_I2),
                    changeNote(1, 'A1',       ATTRS_I1), changeNote(1, 'A1',       ATTRS_I2)
                );
                testRunner.runTest(
                    changeNote(1, 'A1', 't1', ATTRS_I1), changeNote(1, 'A1', 't2', ATTRS_I2),
                    changeNote(1, 'A1', 't1', ATTRS_I1), changeNote(1, 'A1',       ATTRS_I2)
                );
                testRunner.runBidiTest(changeNote(1, 'A1', 't1', ATTRS_I1), changeNote(1, 'A1', ATTRS_I2));
            });
            it('should remove entire operation if nothing will change', function () {
                testRunner.runTest(changeNote(1, 'A1', 't1', ATTRS), changeNote(1, 'A1', 't2', ATTRS2), null,                     []);
                testRunner.runTest(changeNote(1, 'A1', 't1', ATTRS), changeNote(1, 'A1', 't2', ATTRS), changeNote(1, 'A1', 't1'), []);
                testRunner.runTest(changeNote(1, 'A1', 't1', ATTRS), changeNote(1, 'A1', 't1', ATTRS), [],                        []);
            });
        });
     */

    @Test
    public void changeChangeNote01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runTest(
        //  changeNote(1, 'A1', 'abc'),
        //  changeNote(2, 'A1', 'abc'));
        SheetHelper.runTest(
            SheetHelper.createChangeNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createChangeNoteOp(2, "A1", "abc", null).toString()
        );
    }

    @Test
    public void changeChangeNote02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runTest(
        //  changeNote(1, 'A1', 'abc'),
        //  changeNote(1, 'B2', 'abc'));
        SheetHelper.runTest(
            SheetHelper.createChangeNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createChangeNoteOp(1, "B2", "abc", null).toString()
        );
    }

    @Test
    public void changeChangeNote03() throws JSONException {
        // Result: Should reduce attribute sets.
        // testRunner.runTest(
        //  changeNote(1, 'A1', ATTRS_O1),
        //  changeNote(1, 'A1', ATTRS_O2),
        //  changeNote(1, 'A1', ATTRS_R1),
        //  changeNote(1, 'A1', ATTRS_R2));
        SheetHelper.runTest(
            SheetHelper.createChangeNoteOp(1, "A1", null, attrs01).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", null, attrs02).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", null, attrsR1).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", null, attrsR2).toString()
        );
    }

    @Test
    public void changeChangeNote04() throws JSONException {
        // Result: Should reduce note text.
        // testRunner.runTest(
        //  changeNote(1, 'A1', 't1', ATTRS_I1),
        //  changeNote(1, 'A1', 't1', ATTRS_I2),
        //  changeNote(1, 'A1',       ATTRS_I1),
        //  changeNote(1, 'A1',       ATTRS_I2));
        SheetHelper.runTest(
            SheetHelper.createChangeNoteOp(1, "A1", "t1", attrsI1).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "t1", attrsI2).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", null, attrsI1).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", null, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeNote05() throws JSONException {
        // Result: Should reduce note text.
        // testRunner.runTest(
        //  changeNote(1, 'A1', 't1', ATTRS_I1),
        //  changeNote(1, 'A1', 't2', ATTRS_I2),
        //  changeNote(1, 'A1', 't1', ATTRS_I1),
        //  changeNote(1, 'A1',       ATTRS_I2));
        SheetHelper.runTest(
            SheetHelper.createChangeNoteOp(1, "A1", "t1", attrsI1).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "t2", attrsI2).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "t1", attrsI1).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", null, attrsI2).toString()
        );
    }

    @Test
    public void changeChangeNote06() throws JSONException {
        // Result: Should remove entire operation if nothing will change.
        // testRunner.runTest(
        //  changeNote(1, 'A1', 't1', ATTRS),
        //  changeNote(1, 'A1', 't2', ATTRS2),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeNoteOp(1, "A1", "t1", attrs).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "t2", attrs2).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "t1", attrs).toString(),
            "[]"
        );
    }
}
