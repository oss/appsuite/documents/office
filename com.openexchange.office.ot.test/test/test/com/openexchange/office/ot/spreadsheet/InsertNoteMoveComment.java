/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertNoteMoveComment {

    /*
     * describe('"insertNote" and "moveComments"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runBidiTest(insertNote(1, 'A1', 'abc'), moveComments(2, 'A1 B2', 'B2 A1'));
                testRunner.runBidiTest(insertNote(1, 'A1', 'abc'), moveComments(1, 'B2 C3', 'C3 B2'));
            });
            it('should fail to insert note for existing comment', function () {
                testRunner.expectBidiError(insertNote(1, 'A1', 'abc'), moveComments(1, 'A1 B2', 'B2 A1'));
            });
            it('should delete note before moving a comment to the address', function () {
                testRunner.runBidiTest(
                    insertNote(1, 'A1', 'abc'), moveComments(1, 'B2 C3', 'A1 B2'),
                    [], [deleteNote(1, 'A1'), moveComments(1, 'B2 C3', 'A1 B2')]
                );
            });
        });
     */

    @Test
    public void insertNoteMoveComments01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  moveComments(2, 'A1 B2', 'B2 A1'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createMoveCommentsOp(2, "A1 B2", "B2 A1").toString()
        );
    }

    @Test
    public void insertNoteMoveComments02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  moveComments(1, 'B2 C3', 'C3 B2'));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createMoveCommentsOp(1, "B2 C3", "C3 B2").toString()
        );
    }

    @Test
    public void insertNoteMoveComments03() throws JSONException {
        // Result: Should fail to insert note for existing comment.
        // testRunner.expectBidiError(
        //  insertNote(1, 'A1', 'abc'),
        //  moveComments(1, 'A1 B2', 'B2 A1'));
        SheetHelper.expectBidiError(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createMoveCommentsOp(1, "A1 B2", "B2 A1").toString()
        );
    }

    @Test
    public void insertNoteMoveComments04() throws JSONException {
        // Result: Should delete note before moving a comment to the address.
        // testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  moveComments(1, 'B2 C3', 'A1 B2'),
        //  [],
        //  [deleteNote(1, 'A1'),
        //  moveComments(1, 'B2 C3', 'A1 B2')]);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createMoveCommentsOp(1, "B2 C3", "A1 B2").toString(),
            "[]",
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "A1"),
                SheetHelper.createMoveCommentsOp(1, "B2 C3", "A1 B2")
            )
        );
    }
}
