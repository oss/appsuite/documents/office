/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertNoteInsertComment {

    /*
     * describe('"insertNote" and "insertComment"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runBidiTest(insertNote(1, 'A1', 'abc'), insertComment(2, 'A1', 0));
                testRunner.runBidiTest(insertNote(1, 'A1', 'abc'), insertComment(1, 'B2', 0));
            });
            it('should delete note before inserting a comment at the same address', function () {
                testRunner.runBidiTest(
                    insertNote(1, 'A1', 'abc'), opSeries2(insertComment, 1, 'A1', [0, 1, 2]),
                    [], [deleteNote(1, 'A1'), opSeries2(insertComment, 1, 'A1', [0, 1, 2])]
                );
            });
        });
    */

    @Test
    public void insertNoteInsertComment01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        //  testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  insertComment(2, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createInsertCommentOp(2, "A1", 0, null).toString()
        );
    }

    @Test
    public void insertNoteInsertComment02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  insertComment(1, 'B2', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createInsertCommentOp(1, "B2", 0, null).toString()
        );
    }

    @Test
    public void insertNoteInsertComment03() throws JSONException {
        // Result: Should delete note before inserting a comment at the same address.
        // testRunner.runBidiTest(
        //  insertNote(1, 'A1', 'abc'),
        //  opSeries2(insertComment, 1, 'A1', [0, 1, 2]),
        //  [],
        //  [deleteNote(1, 'A1'), opSeries2(insertComment, 1, 'A1', [0, 1, 2])]);
        SheetHelper.runBidiTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertCommentOp(1, "A1", 0, null),
                SheetHelper.createInsertCommentOp(1, "A1", 1, null),
                SheetHelper.createInsertCommentOp(1, "A1", 2, null)
            ),
            "[]",
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "A1"),
                Helper.createJSONArrayFromJSON(
                    SheetHelper.createInsertCommentOp(1, "A1", 0, null),
                    SheetHelper.createInsertCommentOp(1, "A1", 1, null),
                    SheetHelper.createInsertCommentOp(1, "A1", 2, null)
                )
            )
        );
    }
}
