/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class DeleteTable {

/*
    describe('"deleteTable" and "deleteTable"', function () {
        it('should skip different tables', function () {
            testRunner.runTest(deleteTable(1, null), deleteTable(1, 'T2'));
            testRunner.runTest(deleteTable(1, 'T1'), deleteTable(1, null));
            testRunner.runTest(deleteTable(1, 'T1'), deleteTable(1, 'T2'));
            testRunner.runTest(deleteTable(1, null), deleteTable(2, null));
        });
        it('should fail for same table in different sheets', function () {
            testRunner.expectError(deleteTable(1, 'T1'), deleteTable(2, 'T1'));
            testRunner.expectError(deleteTable(1, 'T1'), deleteTable(2, 't1'));
        });
        it('should handle same table', function () {
            testRunner.runTest(deleteTable(1, null), deleteTable(1, null), [], []);
            testRunner.runTest(deleteTable(1, 'T1'), deleteTable(1, 'T1'), [], []);
            testRunner.runTest(deleteTable(1, 'T1'), deleteTable(1, 't1'), [], []);
        });
    });
*/

    @Test
    public void deleteTabledeleteTable01() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runTest(
        //  deleteTable(1, null),
        //  deleteTable(1, 'T2'));
        SheetHelper.runTest(
            SheetHelper.createDeleteTableOp(1, null).toString(),
            SheetHelper.createDeleteTableOp(1, "T2").toString()
        );
    }

    @Test
    public void deleteTabledeleteTable02() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runTest(
        //  deleteTable(1, 'T1'),
        //  deleteTable(1, null));
        SheetHelper.runTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createDeleteTableOp(1, null).toString()
        );
    }

    @Test
    public void deleteTabledeleteTable03() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runTest(
        //  deleteTable(1, 'T1'),
        //  deleteTable(1, 'T2'));
        SheetHelper.runTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createDeleteTableOp(1, "T2").toString()
        );
    }

    @Test
    public void deleteTabledeleteTable04() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runTest(
        //  deleteTable(1, null),
        //  deleteTable(2, null));
        SheetHelper.runTest(
            SheetHelper.createDeleteTableOp(1, null).toString(),
            SheetHelper.createDeleteTableOp(2, null).toString()
        );
    }

    @Test
    public void deleteTabledeleteTable05() throws JSONException {
        // Result: Should fail for same table in different sheets.
        // testRunner.expectError(
        //  deleteTable(1, 'T1'),
        //  deleteTable(2, 'T1'))
        SheetHelper.expectError(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createDeleteTableOp(2, "T1").toString()
        );
    }

    @Test
    public void deleteTabledeleteTable06() throws JSONException {
        // Result: Should fail for same table in different sheets.
        // testRunner.expectError(
        //  deleteTable(1, 'T1'),
        //  deleteTable(2, 't1'));
        SheetHelper.expectError(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createDeleteTableOp(2, "t1").toString()
        );
    }

    @Test
    public void deleteTabledeleteTable07() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runTest(
        //  deleteTable(1, null),
        //  deleteTable(1, null),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createDeleteTableOp(1, null).toString(),
            SheetHelper.createDeleteTableOp(1, null).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteTabledeleteTable08() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runTest(
        //  deleteTable(1, 'T1'),
        //  deleteTable(1, 'T1'),
        //  [],
        //  []);;
        SheetHelper.runTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteTabledeleteTable09() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runTest(
        //  deleteTable(1, 'T1'),
        //  deleteTable(1, 't1'),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createDeleteTableOp(1, "t1").toString(),
            "[]",
            "[]"
        );
    }

/*
    describe('"deleteTable" and "changeTable"', function () {
        it('should skip different tables', function () {
            testRunner.runBidiTest(deleteTable(1, null), changeTable(1, 'T2', 'A1:D4'));
            testRunner.runBidiTest(deleteTable(1, 'T1'), changeTable(1, null, 'A1:D4'));
            testRunner.runBidiTest(deleteTable(1, 'T1'), changeTable(1, 'T2', 'A1:D4'));
            testRunner.runBidiTest(deleteTable(1, null), changeTable(2, null, 'A1:D4'));
        });
        it('should fail for same table in different sheets', function () {
            testRunner.expectBidiError(deleteTable(1, 'T1'), changeTable(2, 'T1', 'A1:D4'));
        });
        it('should handle same table', function () {
            testRunner.runBidiTest(deleteTable(1, null), changeTable(1, null, 'A1:D4'), null, []);
            testRunner.runBidiTest(deleteTable(1, 'T1'), changeTable(1, 'T1', 'A1:D4'), null, []);
            testRunner.runBidiTest(deleteTable(1, 'T1'), changeTable(1, 't1', 'A1:D4'), null, []);
        });
        it('should update table name in operations', function () {
            testRunner.runBidiTest(deleteTable(1, 'T1'), changeTable(1, 'T1', { newName: 'T3' }), deleteTable(1, 'T3'), []);
        });
    });
*/

    @Test
    public void deleteTableChangeTable01() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  deleteTable(1, null),
        //  changeTable(1, 'T2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, null).toString(),
            SheetHelper.createChangeTableOp(1, "T2", "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteTableChangeTable02() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  deleteTable(1, 'T1'),
        //  changeTable(1, null, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableOp(1, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteTableChangeTable03() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  deleteTable(1, 'T1'),
        //  changeTable(1, 'T2', 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableOp(1, "T2", "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteTableChangeTable04() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  deleteTable(1, null),
        //  changeTable(2, null, 'A1:D4'));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, null).toString(),
            SheetHelper.createChangeTableOp(2, null, "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteTableChangeTable05() throws JSONException {
        // Result: Should fail for same table in different sheets.
        // testRunner.expectBidiError(
        //  deleteTable(1, 'T1'),
        //  changeTable(2, 'T1', 'A1:D4'));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableOp(2, "T1", "A1:D4", null).toString()
        );
    }

    @Test
    public void deleteTableChangeTable06() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runBidiTest(
        //  deleteTable(1, null),
        //  changeTable(1, null, 'A1:D4'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, null).toString(),
            SheetHelper.createChangeTableOp(1, null, "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(1, null).toString(),
            "[]"
        );
    }

    @Test
    public void deleteTableChangeTable07() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runBidiTest(
        //  deleteTable(1, 'T1'),
        //  changeTable(1, 'T1', 'A1:D4'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableOp(1, "T1", "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            "[]"
        );
    }

    @Test
    public void deleteTableChangeTable08() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runBidiTest(
        //  deleteTable(1, 'T1'),
        //  changeTable(1, 't1', 'A1:D4'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableOp(1, "t1", "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            "[]"
        );
    }

    @Test
    public void deleteTableChangeTable09() throws JSONException {
        // Result: Should update table name in operations.
        // testRunner.runBidiTest(
        //  deleteTable(1, 'T1'),
        //  changeTable(1, 'T1', { newName: 'T3' }),
        //  deleteTable(1, 'T3'),
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableOp(1, "t1", "A1:D4", null).toString(),
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            "[]"
        );
    }

/*
    describe('"deleteTable" and "changeTableColumn"', function () {
        it('should skip different tables', function () {
            testRunner.runBidiTest(deleteTable(1, null), changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
            testRunner.runBidiTest(deleteTable(1, 'T1'), changeTableCol(1, null, 0, { attrs: ATTRS }));
            testRunner.runBidiTest(deleteTable(1, 'T1'), changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
            testRunner.runBidiTest(deleteTable(1, null), changeTableCol(2, null, 0, { attrs: ATTRS }));
        });
        it('should fail for same table in different sheets', function () {
            testRunner.expectBidiError(deleteTable(1, 'T1'), changeTableCol(2, 'T1', 0, { attrs: ATTRS }));
        });
        it('should handle same table', function () {
            testRunner.runBidiTest(deleteTable(1, null), changeTableCol(1, null, 0, { attrs: ATTRS }), null, []);
            testRunner.runBidiTest(deleteTable(1, 'T1'), changeTableCol(1, 'T1', 0, { attrs: ATTRS }), null, []);
            -testRunner.runBidiTest(deleteTable(1, 'T1'), changeTableCol(1, 't1', 0, { attrs: ATTRS }), null, []);
        });
    });
*/
    private static String ATTRS = "{ attrs: " + SheetHelper.ATTRS + " }";

    @Test
    public void deleteTableChangeTableColumn01() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  deleteTable(1, null),
        //  changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, null).toString(),
            SheetHelper.createChangeTableColOp(1, "T2", 0, ATTRS).toString()
        );
    }

    @Test
    public void deleteTableChangeTableColumn02() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  deleteTable(1, 'T1'),
        //  changeTableCol(1, null, 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableColOp(1, null, 0, ATTRS).toString()
        );
    }

    @Test
    public void deleteTableChangeTableColumn03() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  deleteTable(1, 'T1'),
        //  changeTableCol(1, 'T2', 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableColOp(1, "T2", 0, ATTRS).toString()
        );
    }

    @Test
    public void deleteTableChangeTableColumn04() throws JSONException {
        // Result: Should skip different tables.
        // testRunner.runBidiTest(
        //  deleteTable(1, null),
        //  changeTableCol(2, null, 0, { attrs: ATTRS }));
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, null).toString(),
            SheetHelper.createChangeTableColOp(2, null, 0, ATTRS).toString()
        );
    }

    @Test
    public void deleteTableChangeTableColumn05() throws JSONException {
        // Result: Should fail for same table in different sheets.
        // testRunner.expectBidiError(
        //  deleteTable(1, 'T1'),
        //  changeTableCol(2, 'T1', 0, { attrs: ATTRS }));
        SheetHelper.expectBidiError(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableColOp(2, "T1", 0, ATTRS).toString()
        );
    }

    @Test
    public void deleteTableChangeTableColumn06() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runBidiTest(
        //  deleteTable(1, null),
        //  changeTableCol(1, null, 0, { attrs: ATTRS }),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, null).toString(),
            SheetHelper.createChangeTableColOp(1, null, 0, ATTRS).toString(),
            SheetHelper.createDeleteTableOp(1, null).toString(),
            "[]"
        );
    }

    @Test
    public void deleteTableChangeTableColumn07() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runBidiTest(
        //  deleteTable(1, 'T1'),
        //  changeTableCol(1, 'T1', 0, { attrs: ATTRS }),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableColOp(1, "T1", 0, ATTRS).toString(),
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            "[]"
        );
    }

    @Test
    public void deleteTableChangeTableColumn08() throws JSONException {
        // Result: Should handle same table.
        // testRunner.runBidiTest(
        //  deleteTable(1, 'T1'),
        //  changeTableCol(1, 't1', 0, { attrs: ATTRS }),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            SheetHelper.createChangeTableColOp(1, "t1", 0, ATTRS).toString(),
            SheetHelper.createDeleteTableOp(1, "T1").toString(),
            "[]"
        );
    }
}
