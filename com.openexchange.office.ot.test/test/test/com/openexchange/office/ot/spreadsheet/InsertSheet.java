/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertSheet {

/*
        describe('"insertSheet" and independent operations', function () {
            it('should skip transformations', function () {
                testRunner.runBidiTest(insertSheet(1), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
            });
        });
*/

    @Test
    public void insertSheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, null, null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.AUTOSTYLE_OPS
            )
        );
    }

/*
        describe('"insertSheet" and "insertSheet"', function () {
            it('should shift sheet indexes', function () {
                testRunner.runTest(insertSheet(2, 'S1'), insertSheet(0, 'S2'), insertSheet(3, 'S1'), insertSheet(0, 'S2'));
                testRunner.runTest(insertSheet(2, 'S1'), insertSheet(1, 'S2'), insertSheet(3, 'S1'), insertSheet(1, 'S2'));
                testRunner.runTest(insertSheet(2, 'S1'), insertSheet(2, 'S2'), insertSheet(3, 'S1'), insertSheet(2, 'S2'));
                testRunner.runTest(insertSheet(2, 'S1'), insertSheet(3, 'S2'), insertSheet(2, 'S1'), insertSheet(4, 'S2'));
                testRunner.runTest(insertSheet(2, 'S1'), insertSheet(4, 'S2'), insertSheet(2, 'S1'), insertSheet(5, 'S2'));
            });
            it('should fail to give two sheets the same name', function () {
                testRunner.expectError(insertSheet(1, 'S1'), insertSheet(2, 'S1'));
            });
        });
*/
    @Test
    public void insertSheetInsertSheet01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createInsertSheetOp(0, "S2", null).toString(), SheetHelper.createInsertSheetOp(3, "S1", null).toString(), SheetHelper.createInsertSheetOp(0, "S2", null).toString(), null);
    }

    @Test
    public void insertSheetInsertSheet02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createInsertSheetOp(1, "S2", null).toString(), SheetHelper.createInsertSheetOp(3, "S1", null).toString(), SheetHelper.createInsertSheetOp(1, "S2", null).toString(), null);
    }

    @Test
    public void insertSheetInsertSheet03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createInsertSheetOp(2, "S2", null).toString(), SheetHelper.createInsertSheetOp(3, "S1", null).toString(), SheetHelper.createInsertSheetOp(2, "S2", null).toString(), null);
    }

    @Test
    public void insertSheetInsertSheet04() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createInsertSheetOp(3, "S2", null).toString(), SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createInsertSheetOp(4, "S2", null).toString(), null);
    }

    @Test
    public void insertSheetInsertSheet05() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createInsertSheetOp(4, "S2", null).toString(), SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createInsertSheetOp(5, "S2", null).toString(), null);
    }

    @Test
    public void insertSheetInsertSheet06() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createInsertSheetOp(2, "S1", null).toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

/*
        describe('"insertSheet" and "deleteSheet"', function () {
            it('should handle inserted/deleted sheets without collision', function () {
                testRunner.runBidiTest(insertSheet(0), deleteSheet(2), insertSheet(0), deleteSheet(3));
                testRunner.runBidiTest(insertSheet(1), deleteSheet(2), insertSheet(1), deleteSheet(3));
                testRunner.runBidiTest(insertSheet(2), deleteSheet(2), insertSheet(2), deleteSheet(3));
                testRunner.runBidiTest(insertSheet(3), deleteSheet(2), insertSheet(2), deleteSheet(2));
                testRunner.runBidiTest(insertSheet(4), deleteSheet(2), insertSheet(3), deleteSheet(2));
            });
            it('should handle multiple inserted/deleted sheets without collision', function () {
                testRunner.runBidiTest(
                    opSeries1(insertSheet, 2, 0, 1, 2), deleteSheet(1),
                    opSeries1(insertSheet, 1, 0, 1, 2), deleteSheet(4)
                );
            });
        });
*/
    @Test
    public void insertSheetDeleteSheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(0, null, null).toString(), SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createInsertSheetOp(0, null, null).toString(), SheetHelper.createDeleteSheetOp(3).toString(), null);
    }

    @Test
    public void insertSheetDeleteSheet02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createDeleteSheetOp(3).toString(), null);
    }

    @Test
    public void insertSheetDeleteSheet03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createDeleteSheetOp(3).toString(), null);
    }

    @Test
    public void insertSheetDeleteSheet04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(3, null, null).toString(), SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createDeleteSheetOp(2).toString(), null);
    }

    @Test
    public void insertSheetDeleteSheet05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(4, null, null).toString(), SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createInsertSheetOp(3, null, null).toString(), SheetHelper.createDeleteSheetOp(2).toString(), null);
    }

    @Test
    public void insertSheetDeleteSheet06() throws JSONException {
        SheetHelper.runBidiTest(
            Helper.createArrayFromJSON(
                SheetHelper.createInsertSheetOp(2, null, null),
                SheetHelper.createInsertSheetOp(0, null, null),
                SheetHelper.createInsertSheetOp(1, null, null),
                SheetHelper.createInsertSheetOp(2, null, null)
            ),
            SheetHelper.createDeleteSheetOp(1).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertSheetOp(1, null, null),
                SheetHelper.createInsertSheetOp(0, null, null),
                SheetHelper.createInsertSheetOp(1, null, null),
                SheetHelper.createInsertSheetOp(2, null, null)
            ),
            SheetHelper.createDeleteSheetOp(4).toString(), null
        );
    }

/*
        describe('"insertSheet" and "moveSheet"', function () {
            it('should handle sheet moved to end', function () {
                testRunner.runBidiTest(insertSheet(1), moveSheet(2, 4), insertSheet(1), moveSheet(3, 5));
                testRunner.runBidiTest(insertSheet(2), moveSheet(2, 4), insertSheet(2), moveSheet(3, 5));
                testRunner.runBidiTest(insertSheet(3), moveSheet(2, 4), insertSheet(2), moveSheet(2, 5));
                testRunner.runBidiTest(insertSheet(4), moveSheet(2, 4), insertSheet(3), moveSheet(2, 5));
                testRunner.runBidiTest(insertSheet(5), moveSheet(2, 4), insertSheet(5), moveSheet(2, 4));
                testRunner.runBidiTest(insertSheet(6), moveSheet(2, 4), insertSheet(6), moveSheet(2, 4));
            });
            it('should handle sheet moved to front', function () {
                testRunner.runBidiTest(insertSheet(1), moveSheet(4, 2), insertSheet(1), moveSheet(5, 3));
                testRunner.runBidiTest(insertSheet(2), moveSheet(4, 2), insertSheet(2), moveSheet(5, 3));
                testRunner.runBidiTest(insertSheet(3), moveSheet(4, 2), insertSheet(4), moveSheet(5, 2));
                testRunner.runBidiTest(insertSheet(4), moveSheet(4, 2), insertSheet(5), moveSheet(5, 2));
                testRunner.runBidiTest(insertSheet(5), moveSheet(4, 2), insertSheet(5), moveSheet(4, 2));
                testRunner.runBidiTest(insertSheet(6), moveSheet(4, 2), insertSheet(6), moveSheet(4, 2));
            });
            it('should handle no-op move', function () {
                testRunner.runBidiTest(insertSheet(1), moveSheet(3, 3), null, []);
            });
            it('should handle multiple inserted sheets without collision', function () {
                testRunner.runBidiTest(
                    opSeries1(insertSheet, 1, 3, 5, 7), opSeries1(moveSheet, [1, 2], [6, 3]),
                    opSeries1(insertSheet, 1, 2, 5, 8), opSeries1(moveSheet, [2, 4], [10, 6])
                );
            });
        });
*/
    @Test
    public void insertSheetMoveSheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createMoveSheetOp(3, 5).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createMoveSheetOp(3, 5).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(3, null, null).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createMoveSheetOp(2, 5).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(4, null, null).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createInsertSheetOp(3, null, null).toString(), SheetHelper.createMoveSheetOp(2, 5).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(5, null, null).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createInsertSheetOp(5, null, null).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(6, null, null).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createInsertSheetOp(6, null, null).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createMoveSheetOp(5, 3).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createMoveSheetOp(5, 3).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(3, null, null).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createInsertSheetOp(4, null, null).toString(), SheetHelper.createMoveSheetOp(5, 2).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet10() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(4, null, null).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createInsertSheetOp(5, null, null).toString(), SheetHelper.createMoveSheetOp(5, 2).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet11() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(5, null, null).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createInsertSheetOp(5, null, null).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet12() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(6, null, null).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createInsertSheetOp(6, null, null).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), null);
    }

    @Test
    public void insertSheetMoveSheet13() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createMoveSheetOp(3, 3).toString(), SheetHelper.createInsertSheetOp(1, null, null).toString(), "[]", null);
    }

    @Test
    public void insertSheetMoveSheet14() throws JSONException {
        SheetHelper.runBidiTest(
            Helper.createArrayFromJSON(
                SheetHelper.createInsertSheetOp(1, null, null),
                SheetHelper.createInsertSheetOp(3, null, null),
                SheetHelper.createInsertSheetOp(5, null, null),
                SheetHelper.createInsertSheetOp(7, null, null)
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createMoveSheetOp(1, 2),
                SheetHelper.createMoveSheetOp(6, 3)
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertSheetOp(1, null, null),
                SheetHelper.createInsertSheetOp(2, null, null),
                SheetHelper.createInsertSheetOp(5, null, null),
                SheetHelper.createInsertSheetOp(8, null, null)
            ),
            Helper.createArrayFromJSON(
                SheetHelper.createMoveSheetOp(2, 4),
                SheetHelper.createMoveSheetOp(10, 6)
            ), null
        );
    }

/*
        describe('"insertSheet" and "copySheet"', function () {
            it('should handle sheet copied to end', function () {
                testRunner.runBidiTest(insertSheet(1, 'S1'), copySheet(2, 4, 'S2'), insertSheet(1, 'S1'), copySheet(3, 5, 'S2'));
                testRunner.runBidiTest(insertSheet(2, 'S1'), copySheet(2, 4, 'S2'), insertSheet(2, 'S1'), copySheet(3, 5, 'S2'));
                testRunner.runBidiTest(insertSheet(3, 'S1'), copySheet(2, 4, 'S2'), insertSheet(3, 'S1'), copySheet(2, 5, 'S2'));
                testRunner.runBidiTest(insertSheet(4, 'S1'), copySheet(2, 4, 'S2'), insertSheet(4, 'S1'), copySheet(2, 5, 'S2'));
                testRunner.runBidiTest(insertSheet(5, 'S1'), copySheet(2, 4, 'S2'), insertSheet(6, 'S1'), copySheet(2, 4, 'S2'));
            });
            it('should handle sheet copied to front', function () {
                testRunner.runBidiTest(insertSheet(1, 'S1'), copySheet(4, 2, 'S2'), insertSheet(1, 'S1'), copySheet(5, 3, 'S2'));
                testRunner.runBidiTest(insertSheet(2, 'S1'), copySheet(4, 2, 'S2'), insertSheet(2, 'S1'), copySheet(5, 3, 'S2'));
                testRunner.runBidiTest(insertSheet(3, 'S1'), copySheet(4, 2, 'S2'), insertSheet(4, 'S1'), copySheet(5, 2, 'S2'));
                testRunner.runBidiTest(insertSheet(4, 'S1'), copySheet(4, 2, 'S2'), insertSheet(5, 'S1'), copySheet(5, 2, 'S2'));
                testRunner.runBidiTest(insertSheet(5, 'S1'), copySheet(4, 2, 'S2'), insertSheet(6, 'S1'), copySheet(4, 2, 'S2'));
            });
            it('should handle sheet copied to itself', function () {
                testRunner.runBidiTest(insertSheet(1, 'S1'), copySheet(3, 3, 'S2'), insertSheet(1, 'S1'), copySheet(4, 4, 'S2'));
                testRunner.runBidiTest(insertSheet(2, 'S1'), copySheet(3, 3, 'S2'), insertSheet(2, 'S1'), copySheet(4, 4, 'S2'));
                testRunner.runBidiTest(insertSheet(3, 'S1'), copySheet(3, 3, 'S2'), insertSheet(3, 'S1'), copySheet(4, 4, 'S2'));
                testRunner.runBidiTest(insertSheet(4, 'S1'), copySheet(3, 3, 'S2'), insertSheet(5, 'S1'), copySheet(3, 3, 'S2'));
                testRunner.runBidiTest(insertSheet(5, 'S1'), copySheet(3, 3, 'S2'), insertSheet(6, 'S1'), copySheet(3, 3, 'S2'));
            });
            it('should fail to give two sheets the same name', function () {
                testRunner.expectBidiError(insertSheet(1, 'S1'), copySheet(2, 3, 'S1'));
            });
        });
*/
    @Test
    public void insertSheetCopySheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, "S1", null).toString(), SheetHelper.createCopySheetOp(2, 4, "S2").toString(), SheetHelper.createInsertSheetOp(1, "S1", null).toString(), SheetHelper.createCopySheetOp(3, 5, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createCopySheetOp(2, 4, "S2").toString(), SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createCopySheetOp(3, 5, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(3, "S1", null).toString(), SheetHelper.createCopySheetOp(2, 4, "S2").toString(), SheetHelper.createInsertSheetOp(3, "S1", null).toString(), SheetHelper.createCopySheetOp(2, 5, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(4, "S1", null).toString(), SheetHelper.createCopySheetOp(2, 4, "S2").toString(), SheetHelper.createInsertSheetOp(4, "S1", null).toString(), SheetHelper.createCopySheetOp(2, 5, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(5, "S1", null).toString(), SheetHelper.createCopySheetOp(2, 4, "S2").toString(), SheetHelper.createInsertSheetOp(6, "S1", null).toString(), SheetHelper.createCopySheetOp(2, 4, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, "S1", null).toString(), SheetHelper.createCopySheetOp(4, 2, "S2").toString(), SheetHelper.createInsertSheetOp(1, "S1", null).toString(), SheetHelper.createCopySheetOp(5, 3, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createCopySheetOp(4, 2, "S2").toString(), SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createCopySheetOp(5, 3, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(3, "S1", null).toString(), SheetHelper.createCopySheetOp(4, 2, "S2").toString(), SheetHelper.createInsertSheetOp(4, "S1", null).toString(), SheetHelper.createCopySheetOp(5, 2, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(4, "S1", null).toString(), SheetHelper.createCopySheetOp(4, 2, "S2").toString(), SheetHelper.createInsertSheetOp(5, "S1", null).toString(), SheetHelper.createCopySheetOp(5, 2, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet10() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(5, "S1", null).toString(), SheetHelper.createCopySheetOp(4, 2, "S2").toString(), SheetHelper.createInsertSheetOp(6, "S1", null).toString(), SheetHelper.createCopySheetOp(4, 2, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet11() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, "S1", null).toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), SheetHelper.createInsertSheetOp(1, "S1", null).toString(), SheetHelper.createCopySheetOp(4, 4, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet12() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), SheetHelper.createInsertSheetOp(2, "S1", null).toString(), SheetHelper.createCopySheetOp(4, 4, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet13() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(3, "S1", null).toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), SheetHelper.createInsertSheetOp(3, "S1", null).toString(), SheetHelper.createCopySheetOp(4, 4, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet14() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(4, "S1", null).toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), SheetHelper.createInsertSheetOp(5, "S1", null).toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet15() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(5, "S1", null).toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), SheetHelper.createInsertSheetOp(6, "S1", null).toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), null);
    }

    @Test
    public void insertSheetCopySheet16() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, "S1", null).toString(), SheetHelper.createCopySheetOp(2, 3, "S1").toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

/*
        describe('"insertSheet" and "moveSheets"', function () {
            it('should handle inserted sheet', function () {
                testRunner.runBidiTest(insertSheet(0), moveSheets(MOVE_SHEETS), insertSheet(0), moveSheets([0, 1, 7, 4, 5, 3, 2, 6]));
                testRunner.runBidiTest(insertSheet(1), moveSheets(MOVE_SHEETS), insertSheet(1), moveSheets([0, 1, 7, 4, 5, 3, 2, 6]));
                testRunner.runBidiTest(insertSheet(2), moveSheets(MOVE_SHEETS), insertSheet(2), moveSheets([0, 7, 2, 4, 5, 3, 1, 6]));
                testRunner.runBidiTest(insertSheet(3), moveSheets(MOVE_SHEETS), insertSheet(3), moveSheets([0, 7, 4, 3, 5, 2, 1, 6]));
                testRunner.runBidiTest(insertSheet(4), moveSheets(MOVE_SHEETS), insertSheet(4), moveSheets([0, 7, 3, 5, 4, 2, 1, 6]));
                testRunner.runBidiTest(insertSheet(5), moveSheets(MOVE_SHEETS), insertSheet(5), moveSheets([0, 7, 3, 4, 2, 5, 1, 6]));
                testRunner.runBidiTest(insertSheet(6), moveSheets(MOVE_SHEETS), insertSheet(6), moveSheets([0, 7, 3, 4, 2, 1, 6, 5]));
                testRunner.runBidiTest(insertSheet(7), moveSheets(MOVE_SHEETS), insertSheet(7), moveSheets([0, 6, 3, 4, 2, 1, 5, 7]));
            });
            it('should detect no-op "moveSheets" operation', function () {
                testRunner.runBidiTest(insertSheet(1), moveSheets([0, 1, 2, 3, 4]), insertSheet(1), []);
            });
        });
*/
    @Test
    public void insertSheetMoveSheets01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(0, null, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createInsertSheetOp(0, null, null).toString(), SheetHelper.createMoveSheetsOp("0 1 7 4 5 3 2 6").toString(), null);
    }

    @Test
    public void insertSheetMoveSheets02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createMoveSheetsOp("0 1 7 4 5 3 2 6").toString(), null);
    }

    @Test
    public void insertSheetMoveSheets03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createInsertSheetOp(2, null, null).toString(), SheetHelper.createMoveSheetsOp("0 7 2 4 5 3 1 6").toString(), null);
    }

    @Test
    public void insertSheetMoveSheets04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(3, null, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createInsertSheetOp(3, null, null).toString(), SheetHelper.createMoveSheetsOp("0 7 4 3 5 2 1 6").toString(), null);
    }

    @Test
    public void insertSheetMoveSheets05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(4, null, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createInsertSheetOp(4, null, null).toString(), SheetHelper.createMoveSheetsOp("0 7 3 5 4 2 1 6").toString(), null);
    }

    @Test
    public void insertSheetMoveSheets06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(5, null, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createInsertSheetOp(5, null, null).toString(), SheetHelper.createMoveSheetsOp("0 7 3 4 2 5 1 6").toString(), null);
    }

    @Test
    public void insertSheetMoveSheets07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(6, null, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createInsertSheetOp(6, null, null).toString(), SheetHelper.createMoveSheetsOp("0 7 3 4 2 1 6 5").toString(), null);
    }

    @Test
    public void insertSheetMoveSheets08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(7, null, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createInsertSheetOp(7, null, null).toString(), SheetHelper.createMoveSheetsOp("0 6 3 4 2 1 5 7").toString(), null);
    }

    @Test
    public void insertSheetMoveSheets09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createMoveSheetsOp("0 1 2 3 4").toString(), SheetHelper.createInsertSheetOp(1, null, null).toString(), "[]", null);
    }

/*
    describe('"insertSheet" and "changeSheet"', function () {
        it('should fail to give two sheets the same name', function () {
            testRunner.expectBidiError(insertSheet(1, 'S1'), changeSheet(2, 'S1'));
        });
    });
*/
    @Test
    public void insertSheetChangeSheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, "S1", null).toString(), SheetHelper.createChangeSheetOp(2, "S1", null).toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

/*
    describe('"insertSheet" and sheet index operations', function () {
        it('should shift sheet index', function () {
            testRunner.runBidiTest(
                insertSheet(1), allSheetIndexOps(0, 1, 2),
                insertSheet(1), allSheetIndexOps(0, 2, 3)
            );
        });
    });
*/
    @Test
    public void insertSheetSheetIndexOperations01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createAllSheetIndexOps("0 1 2", false), SheetHelper.createInsertSheetOp(1, null, null).toString(), SheetHelper.createAllSheetIndexOps("0 2 3", false), null);
    }
}
