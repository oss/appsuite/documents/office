/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class InsertInsertNote {

    /*
     * describe.only('"insertNote" and "insertNote"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runTest(insertNote(1, 'A1', 'abc'), insertNote(2, 'A1', 'abc'));
                testRunner.runTest(insertNote(1, 'A1', 'abc'), insertNote(1, 'B2', 'abc'));
            });
            it('should update notes when inserting at the same anchor', function () {
                testRunner.runTest(
                    insertNote(1, 'A1', 'abc1'), insertNote(1, 'A1', 'abc2', ATTRS),
                    changeNote(1, 'A1', 'abc1'), changeNote(1, 'A1', ATTRS)
                );
                testRunner.runTest(
                    insertNote(1, 'A1', 'abc1'), insertNote(1, 'A1', 'abc2'),
                    changeNote(1, 'A1', 'abc1'), []
                );
            });
        });
    */

    @Test
    public void insertNoteInsertNote01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runTest(
        //  insertNote(1, 'A1', 'abc'),
        //  insertNote(2, 'A1', 'abc'));
        SheetHelper.runTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createInsertNoteOp(2, "A1", "abc", null).toString()
        );
    }

    @Test
    public void insertNoteInsertNote02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runTest(
        //  insertNote(1, 'A1', 'abc'),
        //  insertNote(1, 'B2', 'abc'));
        SheetHelper.runTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc", null).toString(),
            SheetHelper.createInsertNoteOp(2, "B2", "abc", null).toString()
        );
    }

    @Test
    public void insertNoteInsertNote03() throws JSONException {
        // Result: Should update notes when inserting at the same anchor.
        // testRunner.runTest(
        //  insertNote(1, 'A1', 'abc1'),
        //  insertNote(1, 'A1', 'abc2', ATTRS),
        //  changeNote(1, 'A1', 'abc1'),
        //  changeNote(1, 'A1', ATTRS));
        SheetHelper.runTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc1", null).toString(),
            SheetHelper.createInsertNoteOp(1, "A1", "abc2", SheetHelper.ATTRS).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "abc1", null).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", null, SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void insertNoteInsertNote04() throws JSONException {
        // Result: Should update notes when inserting at the same anchor.
        // testRunner.runTest(
        //  insertNote(1, 'A1', 'abc1'),
        //  insertNote(1, 'A1', 'abc2'),
        //  changeNote(1, 'A1', 'abc1'),
        //  []);
        SheetHelper.runTest(
            SheetHelper.createInsertNoteOp(1, "A1", "abc1", null).toString(),
            SheetHelper.createInsertNoteOp(1, "A1", "abc2", null).toString(),
            SheetHelper.createChangeNoteOp(1, "A1", "abc1", null).toString(),
            "[]"
        );
    }
}
