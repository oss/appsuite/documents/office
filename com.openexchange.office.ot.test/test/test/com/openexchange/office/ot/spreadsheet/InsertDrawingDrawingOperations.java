/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertDrawingDrawingOperations {

    /*
     * describe('"insertDrawing" and other drawing operations', function () {
            it('should skip different sheets', function () {
                testRunner.runBidiTest(insertShape(1, 0, ATTRS), changeDrawingOps(0, 1));
            });
            it('should shift positions for top-level shape', function () {
                testRunner.runBidiTest(
                    insertShape(1, 1, ATTRS), changeDrawingOps(1, '0', '1', '2', '0 4', '1 4', '2 4'),
                    insertShape(1, 1, ATTRS), changeDrawingOps(1, '0', '2', '3', '0 4', '2 4', '3 4')
                );
            });
            it('should shift positions for embedded shape', function () {
                testRunner.runBidiTest(
                    insertShape(1, '1 1', ATTRS), changeDrawingOps(1, '1 0', '1 1', '1 2', '1 0 4', '1 1 4', '1 2 4'),
                    insertShape(1, '1 1', ATTRS), changeDrawingOps(1, '1 0', '1 2', '1 3', '1 0 4', '1 2 4', '1 3 4')
                );
            });
            it('should not shift positions of parent shapes', function () {
                testRunner.runBidiTest(insertShape(1, '1 1', ATTRS), changeDrawingOps(1, 0, 2));
                testRunner.runBidiTest(insertShape(1, '1 1', ATTRS), changeDrawing(1, 1, ATTRS));
            });
            it('should not shift positions of sibling shapes', function () {
                testRunner.runBidiTest(insertShape(1, '1 1', ATTRS), changeDrawingOps(1, '0 0', '0 1', '0 2', '2 0', '2 1', '2 2'));
            });
        });
    */

    @Test
    public void insertDrawingDrawingOperations01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runTest(
        //  insertShape(1, 0, ATTRS),
        //  moveDrawing(0, 1, 2));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 0", SheetHelper.ATTRS).toString(),
            SheetHelper.changeDrawingOps(0, "1")
        );
    }

    @Test
    public void insertDrawingDrawingOperations02() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 1, ATTRS), changeDrawingOps(1, '0', '1', '2', '0 4', '1 4', '2 4'),
        //  insertShape(1, 1, ATTRS), changeDrawingOps(1, '0', '2', '3', '0 4', '2 4', '3 4')
        // );
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            SheetHelper.changeDrawingOps(1, "0", "1", "2", "0 4", "1 4", "2 4"),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            SheetHelper.changeDrawingOps(1, "0", "2", "3", "0 4", "2 4", "3 4")
        );
    }

    @Test
    public void insertDrawingDrawingOperations03() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1', ATTRS), changeDrawingOps(1, '1 0', '1 1', '1 2', '1 0 4', '1 1 4', '1 2 4'),
        //  insertShape(1, '1 1', ATTRS), changeDrawingOps(1, '1 0', '1 2', '1 3', '1 0 4', '1 2 4', '1 3 4')
        //  );
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            SheetHelper.changeDrawingOps(1, "1 0", "1 1", "1 2", "1 0 4", "1 1 4", "1 2 4"),
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            SheetHelper.changeDrawingOps(1, "1 0", "1 2", "1 3", "1 0 4", "1 2 4", "1 3 4")
        );
    }

    @Test
    public void insertDrawingDrawingOperations04() throws JSONException {
        // Result: Should not shift positions of parent shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1', ATTRS),
        //  changeDrawingOps(1, 0, 2));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            SheetHelper.changeDrawingOps(1, "0", "2")
        );
    }

    @Test
    public void insertDrawingDrawingOperations05() throws JSONException {
        // Result: Should not shift positions of parent shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1', ATTRS),
        //  changeDrawing(1, 1, ATTRS));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            SheetHelper.changeDrawingOps(1, "1")
        );
    }

    @Test
    public void insertDrawingDrawingOperations06() throws JSONException {
        // Result: Should not shift positions of sibling shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1', ATTRS),
        //  changeDrawingOps(1, '0 0', '0 1', '0 2', '2 0', '2 1', '2 2'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            SheetHelper.changeDrawingOps(1, "0 0", "0 1", "0 2", "2 0", "2 1", "2 2")
        );
    }
}
