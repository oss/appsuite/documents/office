/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class CopySheet {

/*
    describe('"copySheet" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(copySheet(2, 4), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
        });
    });
*/
    @Test
    public void copySheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 4, null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.AUTOSTYLE_OPS
            )
        );
    }

/*
    describe('"copySheet" and "copySheet"', function () {
        it('should handle copy operations transforming each other', function () {
            testRunner.runTest(copySheet(2, 4, 'S1'), copySheet(1, 3, 'S2'), copySheet(2, 5, 'S1'), copySheet(1, 3, 'S2'));
            testRunner.runTest(copySheet(2, 4, 'S1'), copySheet(2, 3, 'S2'), copySheet(2, 5, 'S1'), copySheet(2, 3, 'S2'));
            testRunner.runTest(copySheet(2, 4, 'S1'), copySheet(3, 3, 'S2'), copySheet(2, 5, 'S1'), copySheet(3, 3, 'S2'));
            testRunner.runTest(copySheet(2, 4, 'S1'), copySheet(3, 2, 'S2'), copySheet(3, 5, 'S1'), copySheet(3, 2, 'S2'));
            testRunner.runTest(copySheet(2, 4, 'S1'), copySheet(3, 1, 'S2'), copySheet(3, 5, 'S1'), copySheet(3, 1, 'S2'));
            testRunner.runTest(copySheet(4, 2, 'S1'), copySheet(1, 3, 'S2'), copySheet(5, 2, 'S1'), copySheet(1, 4, 'S2'));
            testRunner.runTest(copySheet(4, 2, 'S1'), copySheet(2, 3, 'S2'), copySheet(5, 2, 'S1'), copySheet(3, 4, 'S2'));
            testRunner.runTest(copySheet(4, 2, 'S1'), copySheet(3, 3, 'S2'), copySheet(5, 2, 'S1'), copySheet(4, 4, 'S2'));
            testRunner.runTest(copySheet(4, 2, 'S1'), copySheet(3, 2, 'S2'), copySheet(5, 3, 'S1'), copySheet(4, 2, 'S2'));
            testRunner.runTest(copySheet(4, 2, 'S1'), copySheet(3, 1, 'S2'), copySheet(5, 3, 'S1'), copySheet(4, 1, 'S2'));
        });
        it('should fail to give two sheets the same name', function () {
            testRunner.expectError(copySheet(1, 2, 'S1'), copySheet(3, 4, 'S1'));
        });
    });
*/
    @Test
    public void copySheetCopySheet01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(2, 4, "S1").toString(), SheetHelper.createCopySheetOp(1, 3, "S2").toString(), SheetHelper.createCopySheetOp(2, 5, "S1").toString(), SheetHelper.createCopySheetOp(1, 3, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(2, 4, "S1").toString(), SheetHelper.createCopySheetOp(2, 3, "S2").toString(), SheetHelper.createCopySheetOp(2, 5, "S1").toString(), SheetHelper.createCopySheetOp(2, 3, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(2, 4, "S1").toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), SheetHelper.createCopySheetOp(2, 5, "S1").toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet04() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(2, 4, "S1").toString(), SheetHelper.createCopySheetOp(3, 2, "S2").toString(), SheetHelper.createCopySheetOp(3, 5, "S1").toString(), SheetHelper.createCopySheetOp(3, 2, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet05() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(2, 4, "S1").toString(), SheetHelper.createCopySheetOp(3, 1, "S2").toString(), SheetHelper.createCopySheetOp(3, 5, "S1").toString(), SheetHelper.createCopySheetOp(3, 1, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet06() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(4, 2, "S1").toString(), SheetHelper.createCopySheetOp(1, 3, "S2").toString(), SheetHelper.createCopySheetOp(5, 2, "S1").toString(), SheetHelper.createCopySheetOp(1, 4, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet07() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(4, 2, "S1").toString(), SheetHelper.createCopySheetOp(2, 3, "S2").toString(), SheetHelper.createCopySheetOp(5, 2, "S1").toString(), SheetHelper.createCopySheetOp(3, 4, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet08() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(4, 2, "S1").toString(), SheetHelper.createCopySheetOp(3, 3, "S2").toString(), SheetHelper.createCopySheetOp(5, 2, "S1").toString(), SheetHelper.createCopySheetOp(4, 4, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet09() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(4, 2, "S1").toString(), SheetHelper.createCopySheetOp(3, 2, "S2").toString(), SheetHelper.createCopySheetOp(5, 3, "S1").toString(), SheetHelper.createCopySheetOp(4, 2, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet10() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(4, 2, "S1").toString(), SheetHelper.createCopySheetOp(3, 1, "S2").toString(), SheetHelper.createCopySheetOp(5, 3, "S1").toString(), SheetHelper.createCopySheetOp(4, 1, "S2").toString(), null);
    }

    @Test
    public void copySheetCopySheet11() throws JSONException {
        SheetHelper.runTest(SheetHelper.createCopySheetOp(1, 2, "S1").toString(), SheetHelper.createCopySheetOp(3, 4, "S1").toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

/*
    describe('"copySheet" and "moveSheets"', function () {
        it('should transform the vector, and the copy operation', function () {
            testRunner.runBidiTest(copySheet(2, 1), moveSheets(MOVE_SHEETS), copySheet(4, 1), moveSheets([0, 1, 7, 4, 5, 3, 2, 6]));
            testRunner.runBidiTest(copySheet(2, 2), moveSheets(MOVE_SHEETS), copySheet(4, 2), moveSheets([0, 7, 2, 4, 5, 3, 1, 6]));
            testRunner.runBidiTest(copySheet(2, 3), moveSheets(MOVE_SHEETS), copySheet(4, 3), moveSheets([0, 7, 4, 3, 5, 2, 1, 6]));
            testRunner.runBidiTest(copySheet(2, 4), moveSheets(MOVE_SHEETS), copySheet(4, 4), moveSheets([0, 7, 3, 5, 4, 2, 1, 6]));
            testRunner.runBidiTest(copySheet(2, 5), moveSheets(MOVE_SHEETS), copySheet(4, 5), moveSheets([0, 7, 3, 4, 2, 5, 1, 6]));
            testRunner.runBidiTest(copySheet(3, 1), moveSheets(MOVE_SHEETS), copySheet(2, 1), moveSheets([0, 1, 7, 4, 5, 3, 2, 6]));
            testRunner.runBidiTest(copySheet(3, 2), moveSheets(MOVE_SHEETS), copySheet(2, 2), moveSheets([0, 7, 2, 4, 5, 3, 1, 6]));
            testRunner.runBidiTest(copySheet(3, 3), moveSheets(MOVE_SHEETS), copySheet(2, 3), moveSheets([0, 7, 4, 3, 5, 2, 1, 6]));
            testRunner.runBidiTest(copySheet(3, 4), moveSheets(MOVE_SHEETS), copySheet(2, 4), moveSheets([0, 7, 3, 5, 4, 2, 1, 6]));
            testRunner.runBidiTest(copySheet(3, 5), moveSheets(MOVE_SHEETS), copySheet(2, 5), moveSheets([0, 7, 3, 4, 2, 5, 1, 6]));
            testRunner.runBidiTest(copySheet(4, 1), moveSheets(MOVE_SHEETS), copySheet(3, 1), moveSheets([0, 1, 7, 4, 5, 3, 2, 6]));
            testRunner.runBidiTest(copySheet(4, 2), moveSheets(MOVE_SHEETS), copySheet(3, 2), moveSheets([0, 7, 2, 4, 5, 3, 1, 6]));
            testRunner.runBidiTest(copySheet(4, 3), moveSheets(MOVE_SHEETS), copySheet(3, 3), moveSheets([0, 7, 4, 3, 5, 2, 1, 6]));
            testRunner.runBidiTest(copySheet(4, 4), moveSheets(MOVE_SHEETS), copySheet(3, 4), moveSheets([0, 7, 3, 5, 4, 2, 1, 6]));
            testRunner.runBidiTest(copySheet(4, 5), moveSheets(MOVE_SHEETS), copySheet(3, 5), moveSheets([0, 7, 3, 4, 2, 5, 1, 6]));
        });
    });
*/
    @Test
    public void copySheetMoveSheets01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 1, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(4, 1, null).toString(), SheetHelper.createMoveSheetsOp("0 1 7 4 5 3 2 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 2, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(4, 2, null).toString(), SheetHelper.createMoveSheetsOp("0 7 2 4 5 3 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 3, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(4, 3, null).toString(), SheetHelper.createMoveSheetsOp("0 7 4 3 5 2 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(4, 4, null).toString(), SheetHelper.createMoveSheetsOp("0 7 3 5 4 2 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 5, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(4, 5, null).toString(), SheetHelper.createMoveSheetsOp("0 7 3 4 2 5 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(3, 1, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(2, 1, null).toString(), SheetHelper.createMoveSheetsOp("0 1 7 4 5 3 2 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(3, 2, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(2, 2, null).toString(), SheetHelper.createMoveSheetsOp("0 7 2 4 5 3 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(3, 3, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), SheetHelper.createMoveSheetsOp("0 7 4 3 5 2 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(3, 4, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createMoveSheetsOp("0 7 3 5 4 2 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets10() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(3, 5, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(2, 5, null).toString(), SheetHelper.createMoveSheetsOp("0 7 3 4 2 5 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets11() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(4, 1, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(3, 1, null).toString(), SheetHelper.createMoveSheetsOp("0 1 7 4 5 3 2 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets12() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(4, 2, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), SheetHelper.createMoveSheetsOp("0 7 2 4 5 3 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets13() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(4, 3, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(3, 3, null).toString(), SheetHelper.createMoveSheetsOp("0 7 4 3 5 2 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets14() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(4, 4, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(3, 4, null).toString(), SheetHelper.createMoveSheetsOp("0 7 3 5 4 2 1 6").toString(), null);
    }

    @Test
    public void copySheetMoveSheets15() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(4, 5, null).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createCopySheetOp(3, 5, null).toString(), SheetHelper.createMoveSheetsOp("0 7 3 4 2 5 1 6").toString(), null);
    }

/*
    describe('"copySheet" and "changeSheet"', function () {
        it('should clone sheet attributes but not sheet name', function () {
            testRunner.runBidiTest(
                copySheet(2, 4), opSeries1(changeSheet, [2, ATTRS],             [2, '2'], [2, '2', ATTRS]),
                copySheet(2, 4), opSeries1(changeSheet, [2, ATTRS], [4, ATTRS], [2, '2'], [2, '2', ATTRS], [4, ATTRS])
            );
        });
        it('should fail to give two sheets the same name', function () {
            testRunner.expectBidiError(copySheet(1, 2, 'S1'), changeSheet(3, 'S1'));
        });
    });
*/
    @Test
    public void copySheetChangeSheets01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 4, null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeSheetOp(2, null, SheetHelper.ATTRS),
                SheetHelper.createChangeSheetOp(2, "2", null),
                SheetHelper.createChangeSheetOp(2, "2", SheetHelper.ATTRS)),
            SheetHelper.createCopySheetOp(2, 4, null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeSheetOp(2, null, SheetHelper.ATTRS),
                SheetHelper.createChangeSheetOp(4, null, SheetHelper.ATTRS),
                SheetHelper.createChangeSheetOp(2, "2", null),
                SheetHelper.createChangeSheetOp(2, "2", SheetHelper.ATTRS),
                SheetHelper.createChangeSheetOp(4, null, SheetHelper.ATTRS)),
             null);
    }

    @Test
    public void copySheetChangeSheet02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(1, 2, "S1").toString(), SheetHelper.createChangeSheetOp(3, "S1", null).toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

/*
    describe('"copySheet" and table operations', function () {
        it('should always fail', function () {
            testRunner.expectBidiError(copySheet(2, 4), insertTable(2, 'T', 'A1:D4'));
            testRunner.expectBidiError(copySheet(2, 4), changeTable(2, 'T', 'A1:D4'));
            testRunner.expectBidiError(copySheet(2, 4), deleteTable(2, 'T'));
            testRunner.expectBidiError(copySheet(2, 4), changeTableCol(2, 'T', 0, { attrs: ATTRS }));
        });
    });
*/

    @Test
    public void copySheetAndTable01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createInsertTableOp(2, "T", "A1:D4", null).toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

    @Test
    public void copySheetAndTable02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createChangeTableOp(2, "T", "A1:D4", null).toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

    @Test
    public void copySheetAndTable03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createDeleteTableOp(2, "T").toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

    @Test
    public void copySheetAndTable04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createChangeTableColOp(2, "T", 0, SheetHelper.ATTRS.toString()).toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }", null);
    }

/*
    describe('"copySheet" and sheet index operations', function () {
        it('should clone and shift sheet index', function () {
            testRunner.runBidiTest(
                copySheet(2, 4), allSheetIndexOps(1, 2,    3, 4, 5, { skipTables: true }),
                copySheet(2, 4), allSheetIndexOps(1, 2, 4, 3, 5, 6, { skipTables: true })
            );
            testRunner.runBidiTest(
                copySheet(4, 2), allSheetIndexOps(1, 2, 3, 4,    5, { skipTables: true }),
                copySheet(4, 2), allSheetIndexOps(1, 3, 4, 5, 2, 6, { skipTables: true })
            );
            testRunner.runBidiTest(
                copySheet(3, 3), allSheetIndexOps(1, 2, 3,    4, 5, { skipTables: true }),
                copySheet(3, 3), allSheetIndexOps(1, 2, 4, 3, 5, 6, { skipTables: true })
            );
        });
        it('should handle cascaded copy operations', function () {
            testRunner.runBidiTest(
                opSeries1(copySheet, [2, 4], [4, 1]), allSheetIndexOps(1, 2,       5, { skipTables: true }),
                opSeries1(copySheet, [2, 4], [4, 1]), allSheetIndexOps(2, 3, 5, 1, 7, { skipTables: true })
            );
        });
    });
*/
    @Test
    public void copySheetSheetIndex01() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createCopySheetOp(2, 4, null).toString(),
            SheetHelper.createAllSheetIndexOps("1 2 3 4 5", true),
            SheetHelper.createCopySheetOp(2, 4, null).toString(),
            SheetHelper.createAllSheetIndexOps("1 2 4 3 5 6", true),
            null);
    }

    @Test
    public void copySheetSheetIndex02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(4, 2, null).toString(), SheetHelper.createAllSheetIndexOps("1 2 3 4 5", true), SheetHelper.createCopySheetOp(4, 2, null).toString(), SheetHelper.createAllSheetIndexOps("1 3 4 5 2 6", true), null);
    }

    @Test
    public void copySheetSheetIndex03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createCopySheetOp(3, 3, null).toString(), SheetHelper.createAllSheetIndexOps("1 2 3 4 5", true), SheetHelper.createCopySheetOp(3, 3, null).toString(), SheetHelper.createAllSheetIndexOps("1 2 4 3 5 6", true), null);
    }

    @Test
    public void copySheetSheetIndex04() throws JSONException {
          SheetHelper.runTest(
              Helper.createArrayFromJSON(
                  SheetHelper.createCopySheetOp(2, 4, null),
                  SheetHelper.createCopySheetOp(4, 1, null)),
              SheetHelper.createAllSheetIndexOps("1 2 5", true),
              Helper.createArrayFromJSON(
                  SheetHelper.createCopySheetOp(2, 4, null),
                  SheetHelper.createCopySheetOp(4, 1, null)),
              SheetHelper.createAllSheetIndexOps("2 3 5 1 7", true), null);
    }
}