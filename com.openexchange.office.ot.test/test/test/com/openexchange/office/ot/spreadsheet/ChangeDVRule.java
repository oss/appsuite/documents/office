/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeDVRule {

/*
    describe('"changeDVRule" and "changeDVRule"', function () {
        it('should skip different sheets and rules', function () {
            testRunner.runTest(changeDVRule(1, 1, 'A1:D4'), changeDVRule(2, 1, 'A1:D4'));
            testRunner.runTest(changeDVRule(1, 1, 'A1:D4'), changeDVRule(1, 2, 'E5:H8'));
        });
        it('should reduce various properties', function () {
            testRunner.runTest(changeDVRule(1, 1, 'A1:D4'),                changeDVRule(1, 1, 'B2:E5'),                 null, []);
            testRunner.runTest(changeDVRule(1, 1, 'A1:D4'),                changeDVRule(1, 1, 'A1:D4'),                 [],   []);
            testRunner.runTest(changeDVRule(1, 1, { showInfo: true }),     changeDVRule(1, 1, { showInfo: false }),     null, []);
            testRunner.runTest(changeDVRule(1, 1, { showInfo: true }),     changeDVRule(1, 1, { showInfo: true }),      [],   []);
            testRunner.runTest(changeDVRule(1, 1, { infoTitle: 'a' }),     changeDVRule(1, 1, { infoTitle: 'b' }),      null, []);
            testRunner.runTest(changeDVRule(1, 1, { infoText: 'a' }),      changeDVRule(1, 1, { infoText: 'b' }),       null, []);
            testRunner.runTest(changeDVRule(1, 1, { showError: true }),    changeDVRule(1, 1, { showError: false }),    null, []);
            testRunner.runTest(changeDVRule(1, 1, { errorTitle: 'a' }),    changeDVRule(1, 1, { errorTitle: 'b' }),     null, []);
            testRunner.runTest(changeDVRule(1, 1, { errorText: 'a' }),     changeDVRule(1, 1, { errorText: 'b' }),      null, []);
            testRunner.runTest(changeDVRule(1, 1, { errorType: 'a' }),     changeDVRule(1, 1, { errorType: 'b' }),      null, []);
            testRunner.runTest(changeDVRule(1, 1, { showDropDown: true }), changeDVRule(1, 1, { showDropDown: false }), null, []);
            testRunner.runTest(changeDVRule(1, 1, { ignoreEmpty: true }),  changeDVRule(1, 1, { ignoreEmpty: false }),  null, []);
        });
        it('should fail to create overlapping rules', function () {
            testRunner.expectError(changeDVRule(1, 0, 'A1:D4'), changeDVRule(1, 2, 'B2:E5'));
        });
        it('should fail to reduce type/value properties', function () {
            testRunner.expectError(changeDVRule(1, 1, { type: 'all' }), changeDVRule(1, 1, { type: 'list' }));
            testRunner.expectBidiError(changeDVRule(1, 1, { type: 'all' }), changeDVRule(1, 1, { compare: 'between' }));
            testRunner.expectBidiError(changeDVRule(1, 1, { type: 'all' }), changeDVRule(1, 1, { value1: 'A1' }));
            -testRunner.expectBidiError(changeDVRule(1, 1, { type: 'all' }), changeDVRule(1, 1, { value2: 'A1' }));
        });
    });
*/

    @Test
    public void changeDVRuleChangeDVRule01() throws JSONException {
        // Result: Should skip different sheets and rules.
        // testRunner.runTest(
        //  changeDVRule(1, 1, 'A1:D4'),
        //  changeDVRule(2, 1, 'A1:D4'));
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, "A1:D4", null).toString(),
            SheetHelper.createChangeDVRuleOp(2, 1, "A1:D4", null).toString()
        );
    }

    @Test
    public void changeDVRuleChangeDVRule02() throws JSONException {
        // Result: Should skip different sheets and rules.
        // testRunner.runTest(
        //  changeDVRule(1, 1, 'A1:D4'),
        //  changeDVRule(1, 2, 'E5:H8'));
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, "A1:D4", null).toString(),
            SheetHelper.createChangeDVRuleOp(1, 2, "E5:H8", null).toString()
        );
    }

    @Test
    public void changeDVRuleChangeDVRule03() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, 'A1:D4'),
        //  changeDVRule(1, 1, 'B2:E5'),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, "A1:D4", null).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, "B2:E5", null).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, "A1:D4", null).toString(),
            ""
        );
    }

    @Test
    public void changeDVRuleChangeDVRule04() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, 'A1:D4'),
        //  changeDVRule(1, 1, 'A1:D4'),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, "A1:D4", null).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, "A1:D4", null).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule05() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { showInfo: true }),
        //  changeDVRule(1, 1, { showInfo: false }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showInfo: true }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showInfo: false }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showInfo: true }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule06() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { showInfo: true }),
        //  changeDVRule(1, 1, { showInfo: false }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showInfo: true }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showInfo: false }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showInfo: true }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule07() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { showInfo: true }),
        //  changeDVRule(1, 1, { showInfo: true }),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showInfo: true }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showInfo: true }").toString()).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule08() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { infoTitle: 'a' }),
        //  changeDVRule(1, 1, { infoTitle: 'b' }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ infoTitle: 'a' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ infoTitle: 'b' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ infoTitle: 'a' }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule09() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { infoText: 'a' }),
        //  changeDVRule(1, 1, { infoText: 'b' }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ infoText: 'a' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ infoText: 'b' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ infoText: 'a' }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule10() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { showError: true }),
        //  changeDVRule(1, 1, { showError: false }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showError: true }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showError: false }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showError: true }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule11() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { errorTitle: 'a' }),
        //  changeDVRule(1, 1, { errorTitle: 'b' }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ errorTitle: 'a' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ errorTitle: 'b' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ errorTitle: 'a' }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule12() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { errorText: 'a' }),
        //  changeDVRule(1, 1, { errorText: 'b' }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ errorText: 'a' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ errorText: 'b' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ errorText: 'a' }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule13() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { errorType: 'a' }),
        //  changeDVRule(1, 1, { errorType: 'b' }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ errorType: 'a' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ errorType: 'b' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ errorType: 'a' }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule14() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { showDropDown: true }),
        //  changeDVRule(1, 1, { showDropDown: false }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showDropDown: true }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showDropDown: false }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ showDropDown: true }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule15() throws JSONException {
        // Result: Should reduce various properties.
        // testRunner.runTest(
        //  changeDVRule(1, 1, { ignoreEmpty: true }),
        //  changeDVRule(1, 1, { ignoreEmpty: false }),
        //  null,
        //  []);
        SheetHelper.runTest(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ ignoreEmpty: true }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ ignoreEmpty: false }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ ignoreEmpty: true }").toString()).toString(),
            "[]"
        );
    }

    @Test
    public void changeDVRuleChangeDVRule16() throws JSONException {
        // Result: Should fail to create overlapping rules.
        // testRunner.expectError(
        //  changeDVRule(1, 0, 'A1:D4'),
        //  changeDVRule(1, 2, 'B2:E5'));
        SheetHelper.expectError(
            SheetHelper.createChangeDVRuleOp(1, 0, "A1:D4", null).toString(),
            SheetHelper.createChangeDVRuleOp(1, 2, "B2:E5", null).toString()
        );
    }

    @Test
    public void changeDVRuleChangeDVRule17() throws JSONException {
        // Result: Should fail to reduce type/value properties.
        // testRunner.expectError(
        //  changeDVRule(1, 1, { type: 'all' }),
        //  changeDVRule(1, 1, { type: 'list' }));
        SheetHelper.expectError(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ type: 'all' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ type: 'list' }").toString()).toString()
        );
    }

    @Test
    public void changeDVRuleChangeDVRule18() throws JSONException {
        // Result: Should fail to reduce type/value properties.
        // testRunner.expectBidiError(
        //  changeDVRule(1, 1, { type: 'all' }),
        //  changeDVRule(1, 1, { compare: 'between' }));
        SheetHelper.expectError(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ type: 'all' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ compare: 'between' }").toString()).toString()
        );
    }

    @Test
    public void changeDVRuleChangeDVRule19() throws JSONException {
        // Result: Should fail to reduce type/value properties.
        // testRunner.expectBidiError(
        //  changeDVRule(1, 1, { type: 'all' }),
        //  changeDVRule(1, 1, { value1: 'A1' }));
        SheetHelper.expectError(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ type: 'all' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ value1: 'A1' }").toString()).toString()
        );
    }

    @Test
    public void changeDVRuleChangeDVRule20() throws JSONException {
        // Result: Should fail to reduce type/value properties.
        // testRunner.expectBidiError(
        //  changeDVRule(1, 1, { type: 'all' }),
        //  changeDVRule(1, 1, { value2: 'A1' }));
        SheetHelper.expectError(
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ type: 'all' }").toString()).toString(),
            SheetHelper.createChangeDVRuleOp(1, 1, null, Helper.createJSONObject("{ value2: 'A1' }").toString()).toString()
        );
    }
}
