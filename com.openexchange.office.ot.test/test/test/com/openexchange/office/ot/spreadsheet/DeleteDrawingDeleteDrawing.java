/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DeleteDrawingDeleteDrawing {

    /*
     * describe('"deleteDrawing" and "deleteDrawing"', function () {
            it('should skip different sheets', function () {
                testRunner.runTest(deleteDrawing(1, 0), deleteDrawing(0, 1));
            });
            it('should shift positions of top-level shapes', function () {
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, 0), deleteDrawing(1, 1), deleteDrawing(1, 0));
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, 1), deleteDrawing(1, 1), deleteDrawing(1, 1));
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, 2), [],                   []);
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, 3), deleteDrawing(1, 2), deleteDrawing(1, 2));
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, 4), deleteDrawing(1, 2), deleteDrawing(1, 3));
            });
            it('should shift position of external embedded shape', function () {
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, '0 4'), null, deleteDrawing(1, '0 4'));
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, '1 4'), null, deleteDrawing(1, '1 4'));
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, '2 4'), null, []);
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, '3 4'), null, deleteDrawing(1, '2 4'));
                testRunner.runTest(deleteDrawing(1, 2), deleteDrawing(1, '4 4'), null, deleteDrawing(1, '3 4'));
            });
            it('should shift position of local embedded shape', function () {
                testRunner.runTest(deleteDrawing(1, '2 4'), deleteDrawing(1, 0), deleteDrawing(1, '1 4'));
                testRunner.runTest(deleteDrawing(1, '2 4'), deleteDrawing(1, 1), deleteDrawing(1, '1 4'));
                testRunner.runTest(deleteDrawing(1, '2 4'), deleteDrawing(1, 2), []);
                testRunner.runTest(deleteDrawing(1, '2 4'), deleteDrawing(1, 3), deleteDrawing(1, '2 4'));
                testRunner.runTest(deleteDrawing(1, '2 4'), deleteDrawing(1, 4), deleteDrawing(1, '2 4'));
            });
            it('should shift positions of embedded siblings', function () {
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '2 0'), deleteDrawing(1, '2 1'), deleteDrawing(1, '2 0'));
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '2 1'), deleteDrawing(1, '2 1'), deleteDrawing(1, '2 1'));
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '2 2'), [],                     []);
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '2 3'), deleteDrawing(1, '2 2'), deleteDrawing(1, '2 2'));
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '2 4'), deleteDrawing(1, '2 2'), deleteDrawing(1, '2 3'));
            });
            it('should skip independent embedded shapes', function () {
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '0 0'));
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '0 2'));
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '0 4'));
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '4 0'));
                testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '4 2'));
                -testRunner.runTest(deleteDrawing(1, '2 2'), deleteDrawing(1, '4 4'));
            });
        });
    */

    @Test
    public void deleteDrawingDeleteDrawing01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runTest(
        //  deleteDrawing(1, 0),
        //  deleteDrawing(0, 1));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 0").toString(),
            Helper.createDeleteDrawingOp("0 1").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing02() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, 0),
        //  deleteDrawing(1, 1),
        //  deleteDrawing(1, 0));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 0").toString(),
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createDeleteDrawingOp("1 0").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing03() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, 1),
        //  deleteDrawing(1, 1),
        //  deleteDrawing(1, 1));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createDeleteDrawingOp("1 1").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing04() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, 2),
        //  [],
        //  []);
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing06() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, 3),
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, 2));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 3").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 2").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing07() throws JSONException {
        // Result: Should shift positions of top-level shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, 4),
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, 3));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 4").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 3").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing08() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, '0 4'),
        //  null,
        //  deleteDrawing(1, '0 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 0 4").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 0 4").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing09() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, '1 4'),
        //  null,
        //  deleteDrawing(1, '1 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 1 4").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 1 4").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing10() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, '2 4'),
        //  null,
        //  []);
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            "[]"
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing11() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, '3 4'),
        //  null,
        //  deleteDrawing(1, '2 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 3 4").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 2 4").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing12() throws JSONException {
        // Result: Should shift position of external embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, 2),
        //  deleteDrawing(1, '4 4'),
        //  null,
        //  deleteDrawing(1, '3 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 4 4").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            Helper.createDeleteDrawingOp("1 3 4").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing13() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 4'),
        //  deleteDrawing(1, 0),
        //  deleteDrawing(1, '1 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createDeleteDrawingOp("1 0").toString(),
            Helper.createDeleteDrawingOp("1 1 4").toString(),
            Helper.createDeleteDrawingOp("1 0").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing14() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 4'),
        //  deleteDrawing(1, 1),
        //  deleteDrawing(1, '1 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createDeleteDrawingOp("1 1").toString(),
            Helper.createDeleteDrawingOp("1 1 4").toString(),
            Helper.createDeleteDrawingOp("1 1").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing15() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 4'),
        //  deleteDrawing(1, 2),
        //  []);
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createDeleteDrawingOp("1 2").toString(),
            "[]",
            Helper.createDeleteDrawingOp("1 2").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing16() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 4'),
        //  deleteDrawing(1, 3),
        //  deleteDrawing(1, '2 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createDeleteDrawingOp("1 3").toString(),
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createDeleteDrawingOp("1 3").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing17() throws JSONException {
        // Result: Should shift position of local embedded shape.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 4'),
        //  deleteDrawing(1, 4),
        //  deleteDrawing(1, '2 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createDeleteDrawingOp("1 4").toString(),
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createDeleteDrawingOp("1 4").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing18() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '2 0'),
        //  deleteDrawing(1, '2 1'),
        //  deleteDrawing(1, '2 0'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 2 0").toString(),
            Helper.createDeleteDrawingOp("1 2 1").toString(),
            Helper.createDeleteDrawingOp("1 2 0").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing19() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '2 1'),
        //  deleteDrawing(1, '2 1'),
        //  deleteDrawing(1, '2 1'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 2 1").toString(),
            Helper.createDeleteDrawingOp("1 2 1").toString(),
            Helper.createDeleteDrawingOp("1 2 1").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing20() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '2 2'),
        //  [],
        //  []);
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing21() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        //  testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '2 3'),
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '2 2'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 2 3").toString(),
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 2 2").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing22() throws JSONException {
        // Result: Should shift positions of embedded siblings.
        //  testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '2 4'),
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '2 3'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 2 4").toString(),
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 2 3").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing23() throws JSONException {
        // Result: Should skip independent embedded shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '0 0'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 0 0").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing24() throws JSONException {
        // Result: Should skip independent embedded shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '0 2'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 0 2").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing25() throws JSONException {
        // Result: Should skip independent embedded shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '0 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 0 4").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing26() throws JSONException {
        // Result: Should skip independent embedded shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '4 0'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 4 0").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing27() throws JSONException {
        // Result: Should skip independent embedded shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '4 2'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 4 2").toString()
        );
    }

    @Test
    public void deleteDrawingDeleteDrawing28() throws JSONException {
        // Result: Should skip independent embedded shapes.
        // testRunner.runTest(
        //  deleteDrawing(1, '2 2'),
        //  deleteDrawing(1, '4 4'));
        SheetHelper.runTest(
            Helper.createDeleteDrawingOp("1 2 2").toString(),
            Helper.createDeleteDrawingOp("1 4 4").toString()
        );
    }
}
