/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class InsertInsertComment {

    /*
     * describe('"insertComment" and "insertComment"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runTest(insertComment(1, 'A1', 0), insertComment(2, 'A1', 0));
                testRunner.runTest(insertComment(1, 'A1', 0), insertComment(2, 'B2', 0));
            });
            it('should shift comment index', function () {
                testRunner.runTest(insertComment(1, 'A1', 0), insertComment(1, 'A1', 0), insertComment(1, 'A1', 1), insertComment(1, 'A1', 0));
                testRunner.runTest(insertComment(1, 'A1', 0), insertComment(1, 'A1', 1), insertComment(1, 'A1', 0), insertComment(1, 'A1', 2));
                testRunner.runTest(insertComment(1, 'A1', 0), insertComment(1, 'A1', 2), insertComment(1, 'A1', 0), insertComment(1, 'A1', 3));
                testRunner.runTest(insertComment(1, 'A1', 1), insertComment(1, 'A1', 0), insertComment(1, 'A1', 2), insertComment(1, 'A1', 0));
                testRunner.runTest(insertComment(1, 'A1', 1), insertComment(1, 'A1', 1), insertComment(1, 'A1', 2), insertComment(1, 'A1', 1));
                testRunner.runTest(insertComment(1, 'A1', 1), insertComment(1, 'A1', 2), insertComment(1, 'A1', 1), insertComment(1, 'A1', 3));
                testRunner.runTest(insertComment(1, 'A1', 2), insertComment(1, 'A1', 0), insertComment(1, 'A1', 3), insertComment(1, 'A1', 0));
                testRunner.runTest(insertComment(1, 'A1', 2), insertComment(1, 'A1', 1), insertComment(1, 'A1', 3), insertComment(1, 'A1', 1));
                -testRunner.runTest(insertComment(1, 'A1', 2), insertComment(1, 'A1', 2), insertComment(1, 'A1', 3), insertComment(1, 'A1', 2));
            });
        });
     */

    @Test
    public void insertCommentInsertComment01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 0),
        //  insertComment(2, 'A1', 0));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createInsertCommentOp(2, "A1", 0, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 0),
        //  insertComment(2, 'B2', 0));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createInsertCommentOp(2, "B2", 0, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment03() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 3));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 3, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment04() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 1));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment05() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 3),
        //  insertComment(1, 'A1', 1));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 3, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment07() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 0),
        //  insertComment(1, 'A1', 0),
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 0));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment08() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 0),
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 0),
        //  insertComment(1, 'A1', 2));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment09() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 1));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment10() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 3));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 3, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment11() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 0),
        //  insertComment(1, 'A1', 3),
        //  insertComment(1, 'A1', 0));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 3, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment12() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 1),
        //  insertComment(1, 'A1', 3),
        //  insertComment(1, 'A1', 1));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 3, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 1, null).toString()
        );
    }

    @Test
    public void insertCommentInsertComment13() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  insertComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 2),
        //  insertComment(1, 'A1', 3),
        //  insertComment(1, 'A1', 2));
        SheetHelper.runTest(
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 3, null).toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 2, null).toString()
        );
    }
}
