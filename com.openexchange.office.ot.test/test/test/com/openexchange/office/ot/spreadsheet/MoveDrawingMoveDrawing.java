/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class MoveDrawingMoveDrawing {

    /*
     * describe('"moveDrawing" and "moveDrawing"', function () {
            it('should not process shapes in different sheets', function () {
                testRunner.runTest(moveDrawing(1, 0, 4), moveDrawing(0, 3, 2));
            });
            it('should process top-level shapes in same sheet', function () {
                testRunner.runTest(moveDrawing(1, 2, 4), moveDrawing(1, 3, 1), moveDrawing(1, 3, 4), moveDrawing(1, 2, 1));
                testRunner.runTest(moveDrawing(1, 1, 3), moveDrawing(1, 4, 2), moveDrawing(1, 1, 4), moveDrawing(1, 4, 1));
                testRunner.runTest(moveDrawing(1, 1, 3), moveDrawing(1, 2, 3), moveDrawing(1, 1, 2), moveDrawing(1, 1, 3));
                testRunner.runTest(moveDrawing(1, 1, 2), moveDrawing(1, 2, 4), [],                   moveDrawing(1, 1, 4));
                testRunner.runTest(moveDrawing(1, 2, 4), moveDrawing(1, 1, 2), moveDrawing(1, 1, 4), []);
                testRunner.runTest(moveDrawing(1, 1, 3), moveDrawing(1, 1, 4), moveDrawing(1, 4, 3), []);
                testRunner.runTest(moveDrawing(1, 1, 3), moveDrawing(1, 1, 3), [],                   []);
            });
            it('should process embedded shapes in same parent', function () {
                testRunner.runTest(moveDrawing(1, '5 2', '5 4'), moveDrawing(1, '5 3', '5 1'), moveDrawing(1, '5 3', '5 4'), moveDrawing(1, '5 2', '5 1'));
                testRunner.runTest(moveDrawing(1, '5 1', '5 3'), moveDrawing(1, '5 4', '5 2'), moveDrawing(1, '5 1', '5 4'), moveDrawing(1, '5 4', '5 1'));
                testRunner.runTest(moveDrawing(1, '5 1', '5 3'), moveDrawing(1, '5 2', '5 3'), moveDrawing(1, '5 1', '5 2'), moveDrawing(1, '5 1', '5 3'));
                testRunner.runTest(moveDrawing(1, '5 1', '5 2'), moveDrawing(1, '5 2', '5 4'), [],                           moveDrawing(1, '5 1', '5 4'));
                testRunner.runTest(moveDrawing(1, '5 2', '5 4'), moveDrawing(1, '5 1', '5 2'), moveDrawing(1, '5 1', '5 4'), []);
                testRunner.runTest(moveDrawing(1, '5 1', '5 3'), moveDrawing(1, '5 1', '5 4'), moveDrawing(1, '5 4', '5 3'), []);
                testRunner.runTest(moveDrawing(1, '5 1', '5 3'), moveDrawing(1, '5 1', '5 3'), [],                           []);
            });
            it('should not process embedded shapes in different parent', function () {
                testRunner.runTest(moveDrawing(1, '5 2', '5 4'), moveDrawing(1, '4 3', '4 1'));
                testRunner.runTest(moveDrawing(1, '5 1', '5 3'), moveDrawing(1, '4 4', '4 2'));
                testRunner.runTest(moveDrawing(1, '5 1', '5 3'), moveDrawing(1, '4 1', '4 4'));
                testRunner.runTest(moveDrawing(1, '5 1', '5 3'), moveDrawing(1, '4 1', '4 3'));
            });
            it('should shift embedded shape only', function () {
                testRunner.runBidiTest(moveDrawing(1, '2 2', '2 4'), moveDrawing(1, 1, 4), moveDrawing(1, '1 2', '1 4'));
                -testRunner.runBidiTest(moveDrawing(1, '2 2', '2 4'), moveDrawing(1, 2, 4), moveDrawing(1, '4 2', '4 4'));
            });
        });
    */

    @Test
    public void moveDrawingmoveDrawing01() throws JSONException {
        // Result: Should not process shapes in different sheets.
        // testRunner.runTest(
        //  moveDrawing(1, 0, 4),
        //  moveDrawing(0, 3, 2));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 0", "1 4").toString(),
            Helper.createMoveDrawingOp("0 3", "0 2").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing02() throws JSONException {
        // Result: Should process top-level shapes in same sheet.
        // testRunner.runTest(
        //  moveDrawing(1, 2, 4),
        //  moveDrawing(1, 3, 1),
        //  moveDrawing(1, 3, 4),
        //  moveDrawing(1, 2, 1));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createMoveDrawingOp("1 3", "1 1").toString(),
            Helper.createMoveDrawingOp("1 3", "1 4").toString(),
            Helper.createMoveDrawingOp("1 2", "1 1").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing03() throws JSONException {
        // Result: Should process top-level shapes in same sheet.
        // testRunner.runTest(
        //  moveDrawing(1, 1, 3),
        //  moveDrawing(1, 4, 2),
        //  moveDrawing(1, 1, 4),
        //  moveDrawing(1, 4, 1));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 1", "1 3").toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createMoveDrawingOp("1 1", "1 4").toString(),
            Helper.createMoveDrawingOp("1 4", "1 1").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing04() throws JSONException {
        // Result: Should process top-level shapes in same sheet.
        // testRunner.runTest(
        //  moveDrawing(1, 1, 3),
        //  moveDrawing(1, 2, 3),
        //  moveDrawing(1, 1, 2),
        //  moveDrawing(1, 1, 3));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 1", "1 3").toString(),
            Helper.createMoveDrawingOp("1 2", "1 3").toString(),
            Helper.createMoveDrawingOp("1 1", "1 2").toString(),
            Helper.createMoveDrawingOp("1 1", "1 3").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing05() throws JSONException {
        // Result: Should process top-level shapes in same sheet.
        // testRunner.runTest(
        //  moveDrawing(1, 1, 2),
        //  moveDrawing(1, 2, 4), [],
        //  moveDrawing(1, 1, 4));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 1", "1 2").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            "[]",
            Helper.createMoveDrawingOp("1 1", "1 4").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing06() throws JSONException {
        // Result: Should process top-level shapes in same sheet.
        // testRunner.runTest(
        //  moveDrawing(1, 2, 4),
        //  moveDrawing(1, 1, 2),
        //  moveDrawing(1, 1, 4),
        //  []);
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createMoveDrawingOp("1 1", "1 2").toString(),
            Helper.createMoveDrawingOp("1 1", "1 4").toString(),
            "[]"
        );
    }

    @Test
    public void moveDrawingmoveDrawing07() throws JSONException {
        // Result: Should process top-level shapes in same sheet.
        // testRunner.runTest(
        //  moveDrawing(1, 1, 3),
        //  moveDrawing(1, 1, 4),
        //  moveDrawing(1, 4, 3),
        //  []);
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 1", "1 3").toString(),
            Helper.createMoveDrawingOp("1 1", "1 4").toString(),
            Helper.createMoveDrawingOp("1 4", "1 3").toString(),
            "[]"
        );
    }

    @Test
    public void moveDrawingmoveDrawing08() throws JSONException {
        // Result: Should process top-level shapes in same sheet.
        // testRunner.runTest(
        //  moveDrawing(1, 1, 3),
        //  moveDrawing(1, 1, 3),
        //  [],
        //  []);
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 1", "1 3").toString(),
            Helper.createMoveDrawingOp("1 1", "1 3").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void moveDrawingmoveDrawing09() throws JSONException {
        // Result: Should process embedded shapes in same parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 2', '5 4'),
        //  moveDrawing(1, '5 3', '5 1'),
        //  moveDrawing(1, '5 3', '5 4'),
        //  moveDrawing(1, '5 2', '5 1'));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 2", "1 5 4").toString(),
            Helper.createMoveDrawingOp("1 5 3", "1 5 1").toString(),
            Helper.createMoveDrawingOp("1 5 3", "1 5 4").toString(),
            Helper.createMoveDrawingOp("1 5 2", "1 5 1").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing10() throws JSONException {
        // Result: Should process embedded shapes in same parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 1', '5 3'),
        //  moveDrawing(1, '5 4', '5 2'),
        //  moveDrawing(1, '5 1', '5 4'),
        //  moveDrawing(1, '5 4', '5 1'));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 5 4", "1 5 2").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 4").toString(),
            Helper.createMoveDrawingOp("1 5 4", "1 5 1").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing11() throws JSONException {
        // Result: Should process embedded shapes in same parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 1', '5 3'),
        //  moveDrawing(1, '5 2', '5 3'),
        //  moveDrawing(1, '5 1', '5 2'),
        //  moveDrawing(1, '5 1', '5 3'));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 5 2", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 2").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing12() throws JSONException {
        // Result: Should process embedded shapes in same parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 1', '5 2'),
        //  moveDrawing(1, '5 2', '5 4'),
        //  [],
        //  moveDrawing(1, '5 1', '5 4'));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 1", "1 5 2").toString(),
            Helper.createMoveDrawingOp("1 5 2", "1 5 4").toString(),
            "[]",
            Helper.createMoveDrawingOp("1 5 1", "1 5 4").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing13() throws JSONException {
        // Result: Should process embedded shapes in same parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 2', '5 4'),
        //  moveDrawing(1, '5 1', '5 2'),
        //  moveDrawing(1, '5 1', '5 4'),
        //  []);
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 2", "1 5 4").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 2").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 4").toString(),
            "[]"
        );
    }

    @Test
    public void moveDrawingmoveDrawing14() throws JSONException {
        // Result: Should process embedded shapes in same parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 1', '5 3'),
        //  moveDrawing(1, '5 1', '5 4'),
        //  moveDrawing(1, '5 4', '5 3'),
        //  []);
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 4").toString(),
            Helper.createMoveDrawingOp("1 5 4", "1 5 3").toString(),
            "[]"
        );
    }

    @Test
    public void moveDrawingmoveDrawing15() throws JSONException {
        // Result: Should process embedded shapes in same parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 1', '5 3'),
        //  moveDrawing(1, '5 1', '5 3'),
        //  [],
        //  []);
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void moveDrawingmoveDrawing16() throws JSONException {
        // Result: Should not process embedded shapes in different parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 2', '5 4'),
        //  moveDrawing(1, '4 3', '4 1'));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 2", "1 5 4").toString(),
            Helper.createMoveDrawingOp("1 4 3", "1 4 1").toString(),
            Helper.createMoveDrawingOp("1 5 2", "1 5 4").toString(),
            Helper.createMoveDrawingOp("1 4 3", "1 4 1").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing17() throws JSONException {
        // Result: Should not process embedded shapes in different parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 1', '5 3'),
        //  moveDrawing(1, '4 4', '4 2'));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 4 4", "1 4 2").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 4 4", "1 4 2").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing18() throws JSONException {
        // Result: Should not process embedded shapes in different parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 1', '5 3'),
        //  moveDrawing(1, '4 1', '4 4'));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 4 1", "1 4 4").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 4 1", "1 4 4").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing19() throws JSONException {
        // Result: Should not process embedded shapes in different parent.
        // testRunner.runTest(
        //  moveDrawing(1, '5 1', '5 3'),
        //  moveDrawing(1, '4 1', '4 3'));
        SheetHelper.runTest(
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 4 1", "1 4 3").toString(),
            Helper.createMoveDrawingOp("1 5 1", "1 5 3").toString(),
            Helper.createMoveDrawingOp("1 4 1", "1 4 3").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing20() throws JSONException {
        // Result: Should shift embedded shape only.
        // testRunner.runBidiTest(
        //  moveDrawing(1, '2 2', '2 4'),
        //  moveDrawing(1, 1, 4),
        //  moveDrawing(1, '1 2', '1 4'));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 2 2", "1 2 4").toString(),
            Helper.createMoveDrawingOp("1 1", "1 4").toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createMoveDrawingOp("1 1", "1 4").toString()
        );
    }

    @Test
    public void moveDrawingmoveDrawing21() throws JSONException {
        // Result: Should shift embedded shape only.
        // testRunner.runBidiTest(
        //  moveDrawing(1, '2 2', '2 4'),
        //  moveDrawing(1, 2, 4),
        //  moveDrawing(1, '4 2', '4 4'));
        SheetHelper.runBidiTest(
            Helper.createMoveDrawingOp("1 2 2", "1 2 4").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createMoveDrawingOp("1 4 2", "1 4 4").toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }
}
