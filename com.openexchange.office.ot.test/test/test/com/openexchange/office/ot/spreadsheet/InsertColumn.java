/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertColumn {

/*
    describe('"insertColumns" and independent operations', function () {
        it('should skip transformations', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), [
                GLOBAL_OPS, STYLESHEET_OPS, rowOps(1), nameOps(1),
                deleteTable(1, 'T'), deleteDVRule(1, 0), deleteCFRule(1, 'R1'),
                drawingOps(1), chartOps(1), drawingTextOps(1)
            ]);
        });
    });
*/
    @Test
    public void insertColumns01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.createRowOps("1", null),
                SheetHelper.createNameOps("1"),
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                SheetHelper.createDrawingOps("1"),
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
            )
        );
    }

/*
    describe('"insertColumns" and "insertColumns"', function () {
        it('should transform single intervals', function () {
            testRunner.runTest(insertCols(1, 'B:D'), insertCols(1, 'A:C'), insertCols(1, 'E:G'), insertCols(1, 'A:C'));
            testRunner.runTest(insertCols(1, 'B:D'), insertCols(1, 'B:D'), insertCols(1, 'B:D'), insertCols(1, 'E:G'));
            testRunner.runTest(insertCols(1, 'B:D'), insertCols(1, 'C:E'), insertCols(1, 'B:D'), insertCols(1, 'F:H'));
        });
        it('should transform interval lists #1', function () {
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'A B'), insertCols(1, 'C F'), insertCols(1, 'A C'));
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'B C'), insertCols(1, 'B F'), insertCols(1, 'C D'));
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'C D'), insertCols(1, 'B E'), insertCols(1, 'D F'));
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'D E'), insertCols(1, 'B D'), insertCols(1, 'F G'));
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'E F'), insertCols(1, 'B D'), insertCols(1, 'G H'));
        });
        it('should transform interval lists #2', function () {
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'A C'), insertCols(1, 'C F'), insertCols(1, 'A D'));
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'B D'), insertCols(1, 'B E'), insertCols(1, 'C F'));
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'C E'), insertCols(1, 'B E'), insertCols(1, 'D G'));
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'D F'), insertCols(1, 'B D'), insertCols(1, 'F H'));
            testRunner.runTest(insertCols(1, 'B D'), insertCols(1, 'E G'), insertCols(1, 'B D'), insertCols(1, 'G I'));
        });
        it('should shift intervals outside sheet', function () {
            testRunner.runBidiTest(insertCols(1, 'XFB'), insertCols(1, 'XFC:XFD'), null, insertCols(1, 'XFD'));
            testRunner.runBidiTest(insertCols(1, 'XFB'), insertCols(1, 'XFD'),     null, []);
        });
        it('should not transform intervals in different sheets', function () {
            testRunner.runTest(insertCols(1, 'C'), insertCols(2, 'C'));
        });
    });
*/
    // it('should transform single intervals', function () {
    @Test
    public void insertColumnsInsertColumns01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B:D").toString(), SheetHelper.createInsertColsOp(1, "A:C").toString(), SheetHelper.createInsertColsOp(1, "E:G").toString(), SheetHelper.createInsertColsOp(1, "A:C").toString());
    }

    @Test
    public void insertColumnsInsertColumns02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B:D").toString(), SheetHelper.createInsertColsOp(1, "B:D").toString(), SheetHelper.createInsertColsOp(1, "B:D").toString(), SheetHelper.createInsertColsOp(1, "E:G").toString());
    }

    @Test
    public void insertColumnsInsertColumns03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B:D").toString(), SheetHelper.createInsertColsOp(1, "C:E").toString(), SheetHelper.createInsertColsOp(1, "B:D").toString(), SheetHelper.createInsertColsOp(1, "F:H").toString());
    }

    // it('should transform interval lists #1', function () {
    @Test
    public void insertColumnsInsertColumns04() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "A B").toString(), SheetHelper.createInsertColsOp(1, "C F").toString(), SheetHelper.createInsertColsOp(1, "A C").toString());
    }

    @Test
    public void insertColumnsInsertColumns05() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "B C").toString(), SheetHelper.createInsertColsOp(1, "B F").toString(), SheetHelper.createInsertColsOp(1, "C D").toString());
    }

    @Test
    public void insertColumnsInsertColumns06() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "C D").toString(), SheetHelper.createInsertColsOp(1, "B E").toString(), SheetHelper.createInsertColsOp(1, "D F").toString());
    }

    @Test
    public void insertColumnsInsertColumns07() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "D E").toString(), SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "F G").toString());
    }

    @Test
    public void insertColumnsInsertColumns08() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "E F").toString(), SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "G H").toString());
    }

    // it('should transform interval lists #2', function () {
    @Test
    public void insertColumnsInsertColumns09() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "A C").toString(), SheetHelper.createInsertColsOp(1, "C F").toString(), SheetHelper.createInsertColsOp(1, "A D").toString());
    }

    @Test
    public void insertColumnsInsertColumns10() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "B E").toString(), SheetHelper.createInsertColsOp(1, "C F").toString());
    }

    @Test
    public void insertColumnsInsertColumns11() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "C E").toString(), SheetHelper.createInsertColsOp(1, "B E").toString(), SheetHelper.createInsertColsOp(1, "D G").toString());
    }

    @Test
    public void insertColumnsInsertColumns12() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "D F").toString(), SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "F H").toString());
    }

    @Test
    public void insertColumnsInsertColumns13() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "E G").toString(), SheetHelper.createInsertColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "G I").toString());
    }

    // it('should shift intervals outside sheet', function () {
    @Test
    public void insertColumnsInsertColumns14() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "XFB").toString(), SheetHelper.createInsertColsOp(1, "XFC:XFD").toString(), SheetHelper.createInsertColsOp(1, "XFB").toString(), SheetHelper.createInsertColsOp(1, "XFD").toString());
    }

    @Test
    public void insertColumnsInsertColumns15() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "XFB").toString(), SheetHelper.createInsertColsOp(1, "XFD").toString(), SheetHelper.createInsertColsOp(1, "XFB").toString(), "[]");
    }

    // it('should not transform intervals in different sheets', function () {
    @Test
    public void insertColumnsInsertColumns16() throws JSONException {
        SheetHelper.runTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createInsertColsOp(2, "C").toString());
    }

/*
    describe('"insertColumns" and "deleteColumns"', function () {
        it('should transform single intervals', function () {
            testRunner.runBidiTest(insertCols(1, 'A:B'), deleteCols(1, 'B:C'), insertCols(1, 'A:B'), deleteCols(1, 'D:E'));
            testRunner.runBidiTest(insertCols(1, 'B:C'), deleteCols(1, 'B:C'), insertCols(1, 'B:C'), deleteCols(1, 'D:E'));
            testRunner.runBidiTest(insertCols(1, 'C:D'), deleteCols(1, 'B:C'), insertCols(1, 'B:C'), deleteCols(1, 'B E'));
            testRunner.runBidiTest(insertCols(1, 'D:E'), deleteCols(1, 'B:C'), insertCols(1, 'B:C'), deleteCols(1, 'B:C'));
            testRunner.runBidiTest(insertCols(1, 'E:F'), deleteCols(1, 'B:C'), insertCols(1, 'C:D'), deleteCols(1, 'B:C'));
        });
        it('should transform interval lists', function () {
            testRunner.runBidiTest(insertCols(1, 'D G'), deleteCols(1, 'A C'), insertCols(1, 'B E'), deleteCols(1, 'A C'));
            testRunner.runBidiTest(insertCols(1, 'D G'), deleteCols(1, 'B D'), insertCols(1, 'C E'), deleteCols(1, 'B E'));
            testRunner.runBidiTest(insertCols(1, 'D G'), deleteCols(1, 'C E'), insertCols(1, 'C E'), deleteCols(1, 'C F'));
            testRunner.runBidiTest(insertCols(1, 'D G'), deleteCols(1, 'D F'), insertCols(1, 'D E'), deleteCols(1, 'E G'));
            testRunner.runBidiTest(insertCols(1, 'D G'), deleteCols(1, 'E G'), insertCols(1, 'D F'), deleteCols(1, 'F I'));
            testRunner.runBidiTest(insertCols(1, 'D G'), deleteCols(1, 'F H'), insertCols(1, 'D F'), deleteCols(1, 'G J'));
            testRunner.runBidiTest(insertCols(1, 'D G'), deleteCols(1, 'G I'), insertCols(1, 'D G'), deleteCols(1, 'I K'));
            testRunner.runBidiTest(insertCols(1, 'D G'), deleteCols(1, 'H J'), insertCols(1, 'D G'), deleteCols(1, 'J L'));
        });
        it('should shift intervals outside sheet', function () {
            testRunner.runBidiTest(insertCols(1, 'XFB'), deleteCols(1, 'XFC:XFD'), null, deleteCols(1, 'XFD'));
            testRunner.runBidiTest(insertCols(1, 'XFB'), deleteCols(1, 'XFD'),     null, []);
        });
        it('should not transform intervals in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), deleteCols(2, 'C'));
        });
    });
*/

    // it('should transform single intervals', function () {
    @Test
    public void insertColumnsDeleteColumns01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "A:B").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString(), SheetHelper.createInsertColsOp(1, "A:B").toString(), SheetHelper.createDeleteColsOp(1, "D:E").toString());
    }

    @Test
    public void insertColumnsDeleteColumns02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "B:C").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString(), SheetHelper.createInsertColsOp(1, "B:C").toString(), SheetHelper.createDeleteColsOp(1, "D:E").toString());
    }

    @Test
    public void insertColumnsDeleteColumns03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString(), SheetHelper.createInsertColsOp(1, "B:C").toString(), SheetHelper.createDeleteColsOp(1, "B E").toString());
    }

    @Test
    public void insertColumnsDeleteColumns04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "D:E").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString(), SheetHelper.createInsertColsOp(1, "B:C").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString());
    }

    @Test
    public void insertColumnsDeleteColumns05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "E:F").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString(), SheetHelper.createInsertColsOp(1, "C:D").toString(), SheetHelper.createDeleteColsOp(1, "B:C").toString());
    }

    // it('should transform interval lists', function () {
    @Test
    public void insertColumnsDeleteColumns06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "A C").toString(), SheetHelper.createInsertColsOp(1, "B E").toString(), SheetHelper.createDeleteColsOp(1, "A C").toString());
    }

    @Test
    public void insertColumnsDeleteColumns07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "B D").toString(), SheetHelper.createInsertColsOp(1, "C E").toString(), SheetHelper.createDeleteColsOp(1, "B E").toString());
    }

    @Test
    public void insertColumnsDeleteColumns08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "C E").toString(), SheetHelper.createInsertColsOp(1, "C E").toString(), SheetHelper.createDeleteColsOp(1, "C F").toString());
    }

    @Test
    public void insertColumnsDeleteColumns09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "D F").toString(), SheetHelper.createInsertColsOp(1, "D E").toString(), SheetHelper.createDeleteColsOp(1, "E G").toString());
    }

    @Test
    public void insertColumnsDeleteColumns10() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "E G").toString(), SheetHelper.createInsertColsOp(1, "D F").toString(), SheetHelper.createDeleteColsOp(1, "F I").toString());
    }

    @Test
    public void insertColumnsDeleteColumns11() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "F H").toString(), SheetHelper.createInsertColsOp(1, "D F").toString(), SheetHelper.createDeleteColsOp(1, "G J").toString());
    }

    @Test
    public void insertColumnsDeleteColumns12() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "G I").toString(), SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "I K").toString());
    }

    @Test
    public void insertColumnsDeleteColumns13() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "H J").toString(), SheetHelper.createInsertColsOp(1, "D G").toString(), SheetHelper.createDeleteColsOp(1, "J L").toString());
    }

    // it('should shift intervals outside sheet', function () {
    @Test
    public void insertColumnsDeleteColumns14() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "XFB").toString(), SheetHelper.createDeleteColsOp(1, "XFC:XFD").toString(), SheetHelper.createInsertColsOp(1, "XFB").toString(), SheetHelper.createDeleteColsOp(1, "XFD").toString());
    }

    @Test
    public void insertColumnsDeleteColumns15() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "XFB").toString(), SheetHelper.createDeleteColsOp(1, "XFD").toString(), SheetHelper.createInsertColsOp(1, "XFB").toString(), "[]");
    }

    // it('should not transform intervals in different sheets', function () {
    @Test
    public void insertColumnsDeleteColumns16() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createDeleteColsOp(2, "C").toString());
    }


/*
    describe('"insertColumns" and "changeColumns"', function () {
        it('should transform intervals', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), opSeries2(changeCols, 1, ['A B C D', 'A:B', 'A:C',   'A:D',     'B:D',   'C:D', 'A XFB:XFC XFC:XFD XFD:XFD A', 'XFD']),
                insertCols(1, 'C'), opSeries2(changeCols, 1, ['A B D E', 'A:B', 'A:B D', 'A:B D:E', 'B D:E', 'D:E', 'A XFC:XFD XFD A'])
            );
            testRunner.runBidiTest(
                insertCols(1, 'C:D D:E'), opSeries2(changeCols, 1, ['A B C D', 'A:B', 'A:C',   'A:D',     'B:E',     'C:E',   'D:E']),
                insertCols(1, 'C:D D:E'), opSeries2(changeCols, 1, ['A B E H', 'A:B', 'A:B E', 'A:B E H', 'B E H:I', 'E H:I', 'H:I'])
            );
        });
        it('should not transform intervals in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), changeCols(2, 'A:D', 'a1'));
        });
    });
*/
    // it('should transform intervals', function () {
    @Test
    public void insertColumnsChangeColumns01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "A B C D"),
                SheetHelper.createChangeColsOp(1, "A:B"),
                SheetHelper.createChangeColsOp(1, "A:C"),
                SheetHelper.createChangeColsOp(1, "A:D"),
                SheetHelper.createChangeColsOp(1, "B:D"),
                SheetHelper.createChangeColsOp(1, "C:D"),
                SheetHelper.createChangeColsOp(1, "A XFB:XFC XFC:XFD XFD:XFD A"),
                SheetHelper.createChangeColsOp(1, "XFD")),
            SheetHelper.createInsertColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "A B D E"),
                SheetHelper.createChangeColsOp(1, "A:B"),
                SheetHelper.createChangeColsOp(1, "A:B D"),
                SheetHelper.createChangeColsOp(1, "A:B D:E"),
                SheetHelper.createChangeColsOp(1, "B D:E"),
                SheetHelper.createChangeColsOp(1, "D:E"),
                SheetHelper.createChangeColsOp(1, "A XFC:XFD XFD A"))
            );
    }

    @Test
    public void insertColumnsChangeColumns02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C:D D:E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "A B C D"),
                SheetHelper.createChangeColsOp(1, "A:B"),
                SheetHelper.createChangeColsOp(1, "A:C"),
                SheetHelper.createChangeColsOp(1, "A:D"),
                SheetHelper.createChangeColsOp(1, "B:E"),
                SheetHelper.createChangeColsOp(1, "C:E"),
                SheetHelper.createChangeColsOp(1, "D:E")),
            SheetHelper.createInsertColsOp(1, "C:D D:E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeColsOp(1, "A B E H"),
                SheetHelper.createChangeColsOp(1, "A:B"),
                SheetHelper.createChangeColsOp(1, "A:B E"),
                SheetHelper.createChangeColsOp(1, "A:B E H"),
                SheetHelper.createChangeColsOp(1, "B E H:I"),
                SheetHelper.createChangeColsOp(1, "E H:I"),
                SheetHelper.createChangeColsOp(1, "H:I"))
            );
    }

/*
    describe('"insertColumns" and "changeCells"', function () {
        it('should transform ranges', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), changeCellsOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4',       'A1 XFB1:XFC2 XFC1:XFD2 XFD1:XFD2 A2', 'XFD1'),
                insertCols(1, 'C'), changeCellsOps(1, 'A1 B2 D3 E4', 'A1:B2 B2:B3 D2:D3 D3:E4', 'A1 XFC1:XFD2 XFD1:XFD2 A2')
            );
            testRunner.runBidiTest(
                insertCols(1, 'C:D D:E'), changeCellsOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4 D4:E5'),
                insertCols(1, 'C:D D:E'), changeCellsOps(1, 'A1 B2 E3 H4', 'A1:B2 B2:B3 E2:E3 E3:E4 H3:H4 H4:I5')
            );
        });
        it('should transform matrix formula range', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), changeCells(1, { A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' } }),
                null,               changeCells(1, { A1: { mr: 'A1:B2' }, D3: { mr: 'D3:E4' } })
            );
        });
        it('should fail to split a matrix formula range', function () {
            testRunner.expectBidiError(insertCols(1, 'C'), changeCells(1, { A1: { mr: 'A1:D4' } }));
        });
        it('should fail to change a shared formula', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), changeCells(1, { A1: { si: null } }));
            testRunner.runBidiTest(insertCols(1, 'C'), changeCells(1, { A1: { sr: null } }));
            testRunner.expectBidiError(insertCols(1, 'C'), changeCells(1, { A1: { si: 1 } }));
            testRunner.expectBidiError(insertCols(1, 'C'), changeCells(1, { A1: { sr: 'A1:D4' } }));
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), changeCellsOps(2, 'A1:D4'));
        });
    });
*/
    // it('should transform ranges', function () {
    @Test
    public void insertColumnsChangeCells01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4",       "A1 XFB1:XFC2 XFC1:XFD2 XFD1:XFD2 A2", "XFD1"),
                                SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOps(1, "A1 B2 D3 E4", "A1:B2 B2:B3 D2:D3 D3:E4", "A1 XFC1:XFD2 XFD1:XFD2 A2"));
    }

    @Test
    public void insertColumnsChangeCells02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C:D D:E").toString(), SheetHelper.createChangeCellsOps(1, "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4 D4:E5"),
                                SheetHelper.createInsertColsOp(1, "C:D D:E").toString(), SheetHelper.createChangeCellsOps(1, "A1 B2 E3 H4", "A1:B2 B2:B3 E2:E3 E3:E4 H3:H4 H4:I5"));
    }

    // it('should transform matrix formula range', function () {
    @Test
    public void insertColumnsChangeCells03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { mr: 'A1:B2' }, C3: { mr: 'C3:D4' } }")).toString(),
                                SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { mr: 'A1:B2' }, D3: { mr: 'D3:E4' } }")).toString());
    }

    // it('should fail to split a matrix formula range', function () {
    @Test
    public void insertColumnsChangeCells04() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { mr: 'A1:D4' } }")).toString());
    }

    // it('should fail to change a shared formula', function () {
    @Test
    public void insertColumnsChangeCells05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { si: null } }")).toString());
    }

    @Test
    public void insertColumnsChangeCells06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { sr: null } }")).toString());
    }

    @Test
    public void insertColumnsChangeCells07() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { si: 1 } }")).toString());
    }

    @Test
    public void insertColumnsChangeCells08() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOp(1, new JSONObject("{ A1: { sr: 'A1:D4' } }")).toString());
    }

    // it('should not transform ranges in different sheets', function () {
    @Test
    public void insertColumnsChangeCells09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCellsOps(2, "A1:D4"));
    }

/*
    describe('"insertColumns" and "mergeCells"', function () {
        it('should resize ranges (MERGE, HORIZONTAL, UNMERGE mode)', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), opSeries2(mergeCells, 1, 'A2:B3 B4:C5 C6:D7', [MM_MERGE, MM_HORIZONTAL, MM_UNMERGE]),
                insertCols(1, 'C'), opSeries2(mergeCells, 1, 'A2:B3 B4:D5 D6:E7', [MM_MERGE, MM_HORIZONTAL, MM_UNMERGE])
            );
            testRunner.runBidiTest(
                insertCols(1, 'XFB'), opSeries2(mergeCells, 1, 'XFA1:XFC2 XFA3:XFD4', [MM_MERGE, MM_HORIZONTAL, MM_UNMERGE]),
                insertCols(1, 'XFB'), opSeries2(mergeCells, 1, 'XFA1:XFD2 XFA3:XFD4', [MM_MERGE, MM_HORIZONTAL, MM_UNMERGE])
            );
        });
        it('should split ranges (VERTICAL mode)', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), mergeCells(1, 'A2:B3 B4:C5 C6:D7',       MM_VERTICAL),
                insertCols(1, 'C'), mergeCells(1, 'A2:B3 B4:B5 D4:D5 D6:E7', MM_VERTICAL)
            );
            testRunner.runBidiTest(
                insertCols(1, 'XFB'), mergeCells(1, 'XFA1:XFC2 XFA3:XFD4',                     MM_VERTICAL),
                insertCols(1, 'XFB'), mergeCells(1, 'XFA1:XFA2 XFC1:XFD2 XFA3:XFA4 XFC3:XFD4', MM_VERTICAL)
            );
        });
        it('should remove no-op operations', function () {
            testRunner.runBidiTest(insertCols(1, 'C:D'), opSeries2(mergeCells, 1, 'XFC1:XFD2', MM_ALL), null, []);
        });
        it('should not modify entire row ranges', function () {
            testRunner.runBidiTest(insertCols(1, 'C:D'), opSeries2(mergeCells, 1, 'A1:XFD2', MM_ALL));
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), mergeCells(2, 'A1:D4'));
        });
    });
*/
    @Test
    public void insertColumnsMergeCells01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:C5 C6:D7", "merge"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:C5 C6:D7", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:C5 C6:D7", "unmerge")),
            SheetHelper.createInsertColsOp(1, "C").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:D5 D6:E7", "merge"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:D5 D6:E7", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "A2:B3 B4:D5 D6:E7", "unmerge")));
    }

    @Test
    public void insertColumnsMergeCells02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "XFB").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "XFA1:XFC2 XFA3:XFD4", "merge"),
                SheetHelper.createMergeCellsOp(1, "XFA1:XFC2 XFA3:XFD4", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "XFA1:XFC2 XFA3:XFD4", "unmerge")),
            SheetHelper.createInsertColsOp(1, "XFB").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "XFA1:XFD2 XFA3:XFD4", "merge"),
                SheetHelper.createMergeCellsOp(1, "XFA1:XFD2 XFA3:XFD4", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "XFA1:XFD2 XFA3:XFD4", "unmerge")));
    }

    // it('should split ranges (VERTICAL mode)', function () {
    @Test
    public void insertColumnsMergeCells03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A2:B3 B4:C5 C6:D7", "vertical").toString(),
                                SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMergeCellsOp(1, "A2:B3 B4:B5 D4:D5 D6:E7", "vertical").toString());
    }

    @Test
    public void insertColumnsMergeCells04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "XFB").toString(), SheetHelper.createMergeCellsOp(1, "XFA1:XFC2 XFA3:XFD4", "vertical").toString(),
                                SheetHelper.createInsertColsOp(1, "XFB").toString(), SheetHelper.createMergeCellsOp(1, "XFA1:XFA2 XFC1:XFD2 XFA3:XFA4 XFC3:XFD4", "vertical").toString());
    }

    // it('should remove no-op operations', function () {
    @Test
    public void insertColumnsMergeCells05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C:D").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "XFC1:XFD2", "merge"),
                SheetHelper.createMergeCellsOp(1, "XFC1:XFD2", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "XFC1:XFD2", "vertical"),
                SheetHelper.createMergeCellsOp(1, "XFC1:XFD2", "unmerge")),
            SheetHelper.createInsertColsOp(1, "C:D").toString(),
            "[]");
    }

    // it('should not modify entire row ranges', function () {
    @Test
    public void insertColumnsMergeCells06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C:D").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createMergeCellsOp(1, "A1:XFD2", "merge"),
                SheetHelper.createMergeCellsOp(1, "A1:XFD2", "horizontal"),
                SheetHelper.createMergeCellsOp(1, "A1:XFD2", "vertical"),
                SheetHelper.createMergeCellsOp(1, "A1:XFD2", "unmerge")));
    }

    // it('should not transform ranges in different sheets', function () {
    @Test
    public void insertColumnsMergeCells07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(),
            SheetHelper.createMergeCellsOp(2, "A1:D4", "merge").toString());
    }

/*
    describe('"insertColumns" and hyperlinks (RESIZE mode)', function () {
        it('should transform ranges', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), hlinkOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4', 'A1 XFB1:XFC2 XFC1:XFD2 XFD1:XFD2 A2', 'XFD1'),
                insertCols(1, 'C'), hlinkOps(1, 'A1 B2 D3 E4', 'A1:B2 B2:D3 D3:E4', 'A1 XFC1:XFD2 XFD1:XFD2 A2')
            );
            testRunner.runBidiTest(
                insertCols(1, 'C:D D:E'), hlinkOps(1, 'A1 B2 C3 D4', 'A1:B2 B2:C3 C3:D4 D4:E5'),
                insertCols(1, 'C:D D:E'), hlinkOps(1, 'A1 B2 E3 H4', 'A1:B2 B2:E3 E3:H4 H4:I5')
            );
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), hlinkOps(2, 'A1:D4'));
        });
    });
*/
    @Test
    public void insertColumnsHlinkOps01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createHlinkOps("1", "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4", "A1 XFB1:XFC2 XFC1:XFD2 XFD1:XFD2 A2", "XFD1").toString(),
                                SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createHlinkOps("1", "A1 B2 D3 E4", "A1:B2 B2:D3 D3:E4", "A1 XFC1:XFD2 XFD1:XFD2 A2").toString());
    }

    @Test
    public void insertColumnsHlinkOps02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C:D D:E").toString(), SheetHelper.createHlinkOps("1", "A1 B2 C3 D4", "A1:B2 B2:C3 C3:D4 D4:E5", "XFD1").toString(),
                                SheetHelper.createInsertColsOp(1, "C:D D:E").toString(), SheetHelper.createHlinkOps("1", "A1 B2 E3 H4", "A1:B2 B2:E3 E3:H4 H4:I5").toString());
    }

    @Test
    public void insertColumnsHlinkOps03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createHlinkOps("2", "A1:D4").toString());
    }

/*
    describe('"insertColumns" and "insertTable"', function () {
        it('should shift table range', function () {
            testRunner.runBidiTest(
                insertCols(1, 'E'), opSeries2(insertTable, 1, 'T', ['A1:D4', 'E1:H4']),
                insertCols(1, 'E'), opSeries2(insertTable, 1, 'T', ['A1:D4', 'F1:I4'])
            );
        });
        it('should fail to expand table range', function () {
            testRunner.expectBidiError(insertCols(1, 'C'), insertTable(1, 'T', 'A1:D4'));
        });
        it('should delete table range shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertCols(1, 'A:I'), insertTable(1, 'T', 'XFA1:XFD4'),
                [deleteTable(1, 'T'), insertCols(1, 'A:I')], []
            );
            testRunner.runBidiTest(
                insertCols(1, 'D:E'), insertTable(1, 'T', 'XFA1:XFD4'),
                [deleteTable(1, 'T'), insertCols(1, 'D:E')], []
            );
        });
        it('should not transform tables in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), insertTable(2, 'T', 'A1:D4'));
        });
    });
*/

    // it('should shift table range', function () {
    @Test
    public void insertColumnsInsertTable01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1:D4", null),
                SheetHelper.createInsertTableOp(1, "T", "E1:H4", null)),
            SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "A1:D4", null),
                SheetHelper.createInsertTableOp(1, "T", "F1:I4", null)));
    }

    // it('should fail to expand table range', function () {
    @Test
    public void insertColumnsInsertTable02() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createInsertTableOp(1, "T", "A1:D4", null).toString());
    }

    // it('should delete table range shifted outside sheet', function () {
    @Test
    public void insertColumnsInsertTable03() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "A:I").toString(), SheetHelper.createInsertTableOp(1, "T", "XFA1:XFD4", null).toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteTableOp(1, "T"), SheetHelper.createInsertColsOp(1, "A:I")), "[]");
    }

    @Test
    public void insertColumnsInsertTable04() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "D:E").toString(), SheetHelper.createInsertTableOp(1, "T", "XFA1:XFD4", null).toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteTableOp(1, "T"), SheetHelper.createInsertColsOp(1, "D:E")), "[]");
    }

    // it('should not transform tables in different sheets', function () {
    @Test
    public void insertColumnsInsertTable05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createInsertTableOp(2, "T", "A1:D4", null).toString());
    }

/*
    describe('"insertColumns" and "changeTable"', function () {
        it('should shift table range', function () {
            testRunner.runBidiTest(
                insertCols(1, 'E'), opSeries2(changeTable, 1, 'T', [{ attr: ATTRS }, 'A1:D4', 'E1:H4']),
                insertCols(1, 'E'), opSeries2(changeTable, 1, 'T', [{ attr: ATTRS }, 'A1:D4', 'F1:I4'])
            );
        });
        it('should fail to expand table range', function () {
            testRunner.expectBidiError(insertCols(1, 'C'), changeTable(1, 'T', 'A1:D4'));
        });
        it('should delete table range shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertCols(1, 'A:I'), changeTable(1, 'T', 'XFA1:XFD4'),
                [deleteTable(1, 'T'), insertCols(1, 'A:I')], []
            );
            testRunner.runBidiTest(
                insertCols(1, 'D:E'), changeTable(1, 'T', 'XFA1:XFD4'),
                [deleteTable(1, 'T'), insertCols(1, 'D:E')], []
            );
        });
        it('should not transform tables in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), changeTable(2, 'T', 'A1:D4'));
        });
    });
*/
    @Test
    public void insertColumnsChangeTable01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeTableOp(1, "T", "A1:D4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeTableOp(1, "T", "E1:H4", SheetHelper.ATTRS.toString())),
            SheetHelper.createInsertColsOp(1,  "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeTableOp(1, "T", "A1:D4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeTableOp(1, "T", "F1:I4", SheetHelper.ATTRS.toString())));
    }

    // it('should fail to expand table range', function () {
    @Test
    public void insertColumnsChangeTable02() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeTableOp(1, "T", "A1:D4", null).toString());
    }

    // it('should delete table range shifted outside sheet', function () {
    @Test
    public void insertColumnsChangeTable03() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "A:I").toString(), SheetHelper.createChangeTableOp(1, "T", "XFA1:XFD4", null).toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteTableOp(1, "T"), SheetHelper.createInsertColsOp(1, "A:I")), "[]");
    }

    @Test
    public void insertColumnsChangeTable04() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "D:E").toString(), SheetHelper.createChangeTableOp(1, "T", "XFA1:XFD4", null).toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteTableOp(1, "T"), SheetHelper.createInsertColsOp(1, "D:E")), "[]");
    }

    // it('should not transform tables in different sheets', function () {
    @Test
    public void insertColumnsChangeTable05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeTableOp(2, "T", "A1:D4", null).toString());
    }


/*
    describe('"insertColumns" and "changeTableColumn"', function () {
        it('should fail in same sheet', function () {
            testRunner.expectBidiError(insertCols(1, 'C'), changeTableCol(1, 'T', 0, { attrs: ATTRS }));
        });
        it('should skip different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), changeTableCol(2, 'T', 0, { attrs: ATTRS }));
        });
    });
*/
    // it('should fail in same sheet', function () {
    @Test
    public void insertColumnsChangeTableColumn01() throws JSONException {
        SheetHelper.expectBidiError(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeTableColOp(1, "T", 0, SheetHelper.ATTRS.toString()).toString());
    }

    // it('should skip different sheets', function () {
    @Test
    public void insertColumnsChangeTableColumn02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeTableColOp(2, "T", 0, SheetHelper.ATTRS.toString()).toString());
    }

/*
    describe('"insertColumns" and tables', function () {
        it('should handle insert/change sequence', function () {
            testRunner.runBidiTest(
                insertCols(1, 'A:I'), [insertTable(1, 'T', 'XFA1:XFD4'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS })],
                [deleteTable(1, 'T'), insertCols(1, 'A:I')], []
            );
        });
        it('should handle insert/change/delete sequence', function () {
            testRunner.runBidiTest(
                insertCols(1, 'A:I'), [insertTable(1, 'T', 'XFA1:XFD4'), changeTable(1, 'T', { attrs: ATTRS }), changeTableCol(1, 'T', 0, { attrs: ATTRS }), deleteTable(1, 'T')],
                null, []
            );
        });
    });
*/

    //it('should handle insert/change sequence', function () {
    @Test
    public void insertColumnsTableOps01() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "A:I").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "XFA1:XFD4", null),
                SheetHelper.createChangeTableOp(1, "T", null, SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeTableColOp(1, "T", 0, SheetHelper.ATTRS.toString())),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteTableOp(1, "T"),
                SheetHelper.createInsertColsOp(1, "A:I")),
            "[]");
    }

    // it('should handle insert/change/delete sequence', function () {
    // deleteTable_deleteTable needs to be implemented
    @Test
    public void insertColumnsTableOps02() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "A:I").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertTableOp(1, "T", "XFA1:XFD4", null),
                SheetHelper.createChangeTableOp(1, "T", null, SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeTableColOp(1, "T", 0, SheetHelper.ATTRS.toString()),
                SheetHelper.createDeleteTableOp(1, "T")),
        SheetHelper.createInsertColsOp(1, "A:I").toString(),
        "[]");
    }

/*
    describe('"insertColumns" and "insertDVRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                insertCols(1, 'E'), opSeries2(insertDVRule, 1, 0, ['A1:D4', 'C1:F4', 'E1:H4']),
                insertCols(1, 'E'), opSeries2(insertDVRule, 1, 0, ['A1:E4', 'C1:G4', 'F1:I4'])
            );
        });
        it('should delete rule shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertCols(1, 'A:I'), insertDVRule(1, 0, 'XFA1:XFD4'),
                [deleteDVRule(1, 0), insertCols(1, 'A:I')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), insertDVRule(2, 0, 'A1:D4'));
        });
    });
*/

    // it('should shift rule', function () {
    @Test
    public void insertColumnsInsertDVRule01() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertDVRuleOp(1, 0, "A1:D4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "C1:F4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "E1:H4", null)),
            SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertDVRuleOp(1, 0, "A1:E4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "C1:G4", null),
                SheetHelper.createInsertDVRuleOp(1, 0, "F1:I4", null))
        );
    }

    // it('should delete rule shifted outside sheet', function () {
    @Test
    public void insertColumnsInsertDVRule02() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "A:I").toString(),
            SheetHelper.createInsertDVRuleOp(1, 0, "XFA1:XFD4", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createInsertColsOp(1,  "A:I")),
            "[]"
        );
    }

    // it('should not transform rules in different sheets', function () {
    @Test
    public void insertColumnsInsertDVRule03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createInsertDVRuleOp(2, 0, "A1:D4", null).toString());
    }

/*
    describe('"insertColumns" and "changeDVRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                insertCols(1, 'E'), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:D4', 'C1:F4', 'E1:H4']),
                insertCols(1, 'E'), opSeries2(changeDVRule, 1, 0, [{ attr: ATTRS }, 'A1:E4', 'C1:G4', 'F1:I4'])
            );
        });
        it('should delete rule shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertCols(1, 'A:I'), changeDVRule(1, 0, 'XFA1:XFD4'),
                [deleteDVRule(1, 0), insertCols(1, 'A:I')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), changeDVRule(2, 0, 'A1:D4'));
        });
    });
*/

    // it('should shift rule', function () {
    @Test
    public void insertColumnsChangeDVRule01() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeDVRuleOp(1, 0, "A1:D4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeDVRuleOp(1, 0, "C1:F4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeDVRuleOp(1, 0, "E1:H4", SheetHelper.ATTRS.toString())),
            SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeDVRuleOp(1, 0, "A1:E4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeDVRuleOp(1, 0, "C1:G4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeDVRuleOp(1, 0, "F1:I4", SheetHelper.ATTRS.toString()))
        );
    }

    // it('should delete rule shifted outside sheet', function () {
    @Test
    public void insertColumnsChangeDVRule02() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "A:I").toString(),
            SheetHelper.createChangeDVRuleOp(1, 0, "XFA1:XFD4", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteDVRuleOp(1, 0),
                SheetHelper.createInsertColsOp(1,  "A:I")),
            "[]"
        );
    }

    // it('should not transform rules in different sheets', function () {
    @Test
    public void insertColumnsChangeDVRule03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeDVRuleOp(2, 0, "A1:D4", null).toString());
    }

/*
    describe('"insertColumns" and "insertCFRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                insertCols(1, 'E'), opSeries2(insertCFRule, 1, 'R1', ['A1:D4', 'C1:F4', 'E1:H4']),
                insertCols(1, 'E'), opSeries2(insertCFRule, 1, 'R1', ['A1:E4', 'C1:G4', 'F1:I4'])
            );
        });
        it('should delete rule shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertCols(1, 'A:I'), insertCFRule(1, 'R1', 'XFA1:XFD4'),
                [deleteCFRule(1, 'R1'), insertCols(1, 'A:I')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), insertCFRule(2, 'R1', 'A1:D4'));
        });
    });
*/

    // it('should shift rule', function () {
    @Test
    public void insertColumnsInsertCFRule01() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertCFRuleOp(1, "R1", "A1:D4", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "C1:F4", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "E1:H4", null)),
            SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createInsertCFRuleOp(1, "R1", "A1:E4", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "C1:G4", null),
                SheetHelper.createInsertCFRuleOp(1, "R1", "F1:I4", null))
        );
    }

    // it('should delete rule shifted outside sheet', function () {
    @Test
    public void insertColumnsInsertCFRule02() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "A:I").toString(),
            SheetHelper.createInsertCFRuleOp(1, "R1", "XFA1:XFD4", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                SheetHelper.createInsertColsOp(1,  "A:I")),
            "[]"
        );
    }

    // it('should not transform rules in different sheets', function () {
    @Test
    public void insertColumnsInsertCFRule03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createInsertCFRuleOp(2, "R1", "A1:D4", null).toString());
    }

/*
    describe('"insertColumns" and "changeCFRule"', function () {
        it('should shift rule', function () {
            testRunner.runBidiTest(
                insertCols(1, 'E'), opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:D4', 'C1:F4', 'E1:H4']),
                insertCols(1, 'E'), opSeries2(changeCFRule, 1, 'R1', [{ attr: ATTRS }, 'A1:E4', 'C1:G4', 'F1:I4'])
            );
        });
        it('should delete rule shifted outside sheet', function () {
            testRunner.runBidiTest(
                insertCols(1, 'A:I'), changeCFRule(1, 'R1', 'XFA1:XFD4'),
                [deleteCFRule(1, 'R1'), insertCols(1, 'A:I')], []
            );
        });
        it('should not transform rules in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), changeCFRule(2, 'R1', 'A1:D4'));
        });
    });
*/

    // it('should shift rule', function () {
    @Test
    public void insertColumnsChangeCFRule01() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeCFRuleOp(1, "R1", "A1:D4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeCFRuleOp(1, "R1", "C1:F4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeCFRuleOp(1, "R1", "E1:H4", SheetHelper.ATTRS.toString())),
            SheetHelper.createInsertColsOp(1, "E").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createChangeCFRuleOp(1, "R1", "A1:E4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeCFRuleOp(1, "R1", "C1:G4", SheetHelper.ATTRS.toString()),
                SheetHelper.createChangeCFRuleOp(1, "R1", "F1:I4", SheetHelper.ATTRS.toString()))
        );
    }

    // it('should delete rule shifted outside sheet', function () {
    @Test
    public void insertColumnsChangeCFRule02() throws JSONException {
        SheetHelper.runBidiTest(
            SheetHelper.createInsertColsOp(1, "A:I").toString(),
            SheetHelper.createChangeCFRuleOp(1, "R1", "XFA1:XFD4", null).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteCFRuleOp(1, "R1"),
                SheetHelper.createInsertColsOp(1,  "A:I")),
            "[]"
        );
    }

    // it('should not transform rules in different sheets', function () {
    @Test
    public void insertColumnsChangeCFRule03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCFRuleOp(2, "R1", "A1:D4", null).toString());
    }

/*
    describe('"insertColumns" and cell anchor', function () {
        it('should transform cell anchor', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), cellAnchorOps(1, 'A1', 'B2', 'C3', 'D4', 'XFC5'),
                insertCols(1, 'C'), cellAnchorOps(1, 'A1', 'B2', 'D3', 'E4', 'XFD5')
            );
            testRunner.runBidiTest(
                insertCols(1, 'C:D D:E'), cellAnchorOps(1, 'A1', 'B2', 'C3', 'D4', 'E5', 'XEZ6'),
                insertCols(1, 'C:D D:E'), cellAnchorOps(1, 'A1', 'B2', 'E3', 'H4', 'I5', 'XFD6')
            );
        });
        it('should handle deleted cell note', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), insertNote(1, 'XFD1', 'abc'), [deleteNote(1, 'XFD1'), insertCols(1, 'C')], []);
            testRunner.runBidiTest(insertCols(1, 'C'), deleteNote(1, 'XFD1'),        null,                                        []);
            testRunner.runBidiTest(insertCols(1, 'C'), changeNote(1, 'XFD1', 'abc'), [deleteNote(1, 'XFD1'), insertCols(1, 'C')], []);
        });
        it('should handle deleted cell comment', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), insertComment(1, 'XFD1', 0, { text: 'abc' }), [deleteComment(1, 'XFD1', 0), insertCols(1, 'C')], []);
            testRunner.runBidiTest(insertCols(1, 'C'), insertComment(1, 'XFD1', 1, { text: 'abc' }), null,                                              []);
            testRunner.runBidiTest(insertCols(1, 'C'), deleteComment(1, 'XFD1', 0),                  null,                                              []);
            testRunner.runBidiTest(insertCols(1, 'C'), deleteComment(1, 'XFD1', 1),                  null,                                              []);
            testRunner.runBidiTest(insertCols(1, 'C'), changeComment(1, 'XFD1', 0, { text: 'abc' }), [deleteComment(1, 'XFD1', 0), insertCols(1, 'C')], []);
            testRunner.runBidiTest(insertCols(1, 'C'), changeComment(1, 'XFD1', 1, { text: 'abc' }), null,                                              []);
        });
        it('should not transform cell anchor in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), cellAnchorOps(2, 'D4'));
        });
    });
*/
    // it('should transform cell anchor', function () {
    @Test
    public void insertColumnsCellAnchor01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createCellAnchorOps(1, "A1", "B2", "C3", "D4", "XFC5").toString(), SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createCellAnchorOps(1, "A1", "B2", "D3", "E4", "XFD5").toString());
    }

    @Test
    public void insertColumnsCellAnchor02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C:D D:E").toString(), SheetHelper.createCellAnchorOps(1, "A1", "B2", "C3", "D4", "E5", "XEZ6").toString(), SheetHelper.createInsertColsOp(1, "C:D D:E").toString(), SheetHelper.createCellAnchorOps(1, "A1", "B2", "E3", "H4", "I5", "XFD6").toString());
    }

    // it('should handle deleted cell note', function () {
    @Test
    public void insertColumnsNote01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createInsertNoteOp(1, "XFD1", "abc", null).toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteNoteOp(1, "XFD1"), SheetHelper.createInsertColsOp(1, "C")), "[]");
    }

    @Test
    public void insertColumnsNote02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createDeleteNoteOp(1, "XFD1").toString(), SheetHelper.createInsertColsOp(1, "C").toString(), "[]");
    }

    @Test
    public void insertColumnsNote03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeNoteOp(1, "XFD1", "abc", null).toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteNoteOp(1, "XFD1"), SheetHelper.createInsertColsOp(1, "C")), "[]");
    }

    // it('should handle deleted cell comment', function () {
    @Test
    public void insertColumnsComment01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createInsertCommentOp(1, "XFD1", 0, "{ text: 'abc' }").toString(), Helper.createArrayFromJSON(SheetHelper.createDeleteCommentOp(1, "XFD1", 0), SheetHelper.createInsertColsOp(1, "C")), "[]");
    }

    @Test
    public void insertColumnsComment02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createInsertCommentOp(1, "XFD1", 1, "{ text: 'abc' }").toString(), SheetHelper.createInsertColsOp(1, "C").toString(), "[]");
    }

    @Test
    public void insertColumnsComment03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createDeleteCommentOp(1, "XFD1", 0).toString(), SheetHelper.createInsertColsOp(1, "C").toString(), "[]");
    }

    @Test
    public void insertColumnsComment04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createDeleteCommentOp(1, "XFD1", 1).toString(), SheetHelper.createInsertColsOp(1, "C").toString(), "[]");
    }

    @Test
    public void insertColumnsComment05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCommentOp(1, "XFD1", 0, "{ text: 'abc' }").toString(), Helper.createArrayFromJSON(SheetHelper.createDeleteCommentOp(1, "XFD1", 0), SheetHelper.createInsertColsOp(1, "C")), "[]");
    }

    @Test
    public void insertColumnsComment06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createChangeCommentOp(1, "XFD1", 1, "{ text: 'abc' }").toString(), SheetHelper.createInsertColsOp(1, "C").toString(), "[]");
    }

/*
    describe('"insertColumns" and "moveNotes"', function () {
        it('should transform cell anchors', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), moveNotes(1, 'A1 B2 C3 D4', 'D1 C2 B3 A4'),
                insertCols(1, 'C'), moveNotes(1, 'A1 B2 D3 E4', 'E1 D2 B3 A4')
            );
        });
        it('should handle deleted source anchor', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), moveNotes(1, 'A1 XFD2 XFD3 A4', 'E1 E2 XFD1 E4'),
                [deleteNote(1, 'E2'), deleteNote(1, 'XFD1'), insertCols(1, 'C')], moveNotes(1, 'A1 A4', 'F1 F4')
            );
        });
        it('should handle deleted target anchor', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), moveNotes(1, 'E1 E2 E3', 'A1 XFD2 A3'),
                [deleteNote(1, 'XFD2'), insertCols(1, 'C')], [deleteNote(1, 'F2'), moveNotes(1, 'F1 F3', 'A1 A3')]
            );
        });
        it('should not transform cell anchor in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), moveNotes(2, 'D4', 'E5'));
        });
    });
*/

    // it('should transform cell anchors', function () {
    @Test
    public void insertColumnsMoveNotes01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveNotesOp(1, "A1 B2 C3 D4", "D1 C2 B3 A4").toString(), SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveNotesOp(1, "A1 B2 D3 E4", "E1 D2 B3 A4").toString());
    }

    // it('should handle deleted source anchor', function () {
    @Test
    public void insertColumnsMoveNotes02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveNotesOp(1, "A1 XFD2 XFD3 A4", "E1 E2 XFD1 E4").toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteNoteOp(1, "E2"), SheetHelper.createDeleteNoteOp(1, "XFD1"), SheetHelper.createInsertColsOp(1, "C")), SheetHelper.createMoveNotesOp(1, "A1 A4", "F1 F4").toString());
    }

    // it('should handle deleted target anchor', function () {
    @Test
    public void insertColumnsMoveNotes03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveNotesOp(1, "E1 E2 E3", "A1 XFD2 A3").toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteNoteOp(1, "XFD2"), SheetHelper.createInsertColsOp(1, "C")), Helper.createArrayFromJSON(SheetHelper.createDeleteNoteOp(1,  "F2"), SheetHelper.createMoveNotesOp(1, "F1 F3", "A1 A3")));
    }

    // it('should not transform cell anchor in different sheets', function () {
    @Test
    public void insertColumnsMoveNotes04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveNotesOp(2, "D4", "E5").toString());
    }

/*
    describe('"insertColumns" and "moveComments"', function () {
        it('should transform cell anchors', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), moveComments(1, 'A1 B2 C3 D4', 'D1 C2 B3 A4'),
                insertCols(1, 'C'), moveComments(1, 'A1 B2 D3 E4', 'E1 D2 B3 A4')
            );
        });
        it('should handle deleted source anchor', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), moveComments(1, 'A1 XFD2 XFD3 A4', 'E1 E2 XFD1 E4'),
                [deleteComment(1, 'E2', 0), deleteComment(1, 'XFD1', 0), insertCols(1, 'C')], moveComments(1, 'A1 A4', 'F1 F4')
            );
        });
        it('should handle deleted target anchor', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), moveComments(1, 'E1 E2 E3', 'A1 XFD2 A3'),
                [deleteComment(1, 'XFD2', 0), insertCols(1, 'C')], [deleteComment(1, 'F2', 0), moveComments(1, 'F1 F3', 'A1 A3')]
            );
        });
        it('should not transform cell anchor in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), moveComments(2, 'D4', 'E5'));
        });
    });
*/
    // it('should transform cell anchors', function () {
    @Test
    public void insertColumnsMoveComments01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveCommentsOp(1, "A1 B2 C3 D4", "D1 C2 B3 A4").toString(), SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveCommentsOp(1, "A1 B2 D3 E4", "E1 D2 B3 A4").toString());
    }

    // it('should handle deleted source anchor', function () {
    @Test
    public void insertColumnsMoveComments02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveCommentsOp(1, "A1 XFD2 XFD3 A4", "E1 E2 XFD1 E4").toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteCommentOp(1, "E2", 0), SheetHelper.createDeleteCommentOp(1, "XFD1", 0), SheetHelper.createInsertColsOp(1, "C")), SheetHelper.createMoveCommentsOp(1, "A1 A4", "F1 F4").toString());
    }

    // it('should handle deleted target anchor', function () {
    @Test
    public void insertColumnsMoveComments03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveCommentsOp(1, "E1 E2 E3", "A1 XFD2 A3").toString(),
            Helper.createArrayFromJSON(SheetHelper.createDeleteCommentOp(1, "XFD2", 0), SheetHelper.createInsertColsOp(1, "C")), Helper.createArrayFromJSON(SheetHelper.createDeleteCommentOp(1,  "F2", 0), SheetHelper.createMoveCommentsOp(1, "F1 F3", "A1 A3")));
    }

    // it('should not transform cell anchor in different sheets', function () {
    @Test
    public void insertColumnsMoveComments04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createMoveCommentsOp(2, "D4", "E5").toString());
    }


/*
    describe('"insertColumns" and drawing anchor', function () {
        it('should transform drawing anchor', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 B1 10 10 C2 0 20', '2 B1 10 10 C2 20 20', '2 C1 10 10 D2 20 20', '2 G1 10 10 XFD2 20 20'),
                insertCols(1, 'C'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 B1 10 10 C2 0 20', '2 B1 10 10 D2 20 20', '2 D1 10 10 E2 20 20', '2 G1 10 10 XFD2 20 20'),
                { warnings: true } // missing transformation of absolute drawing position

            );
            testRunner.runBidiTest(
                insertCols(1, 'C:D D:E'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 B1 10 10 C2 20 20', '2 C1 10 10 D2 20 20', '2 D1 10 10 E2 20 20', '2 G1 10 10 XFC2 20 20'),
                insertCols(1, 'C:D D:E'), drawingAnchorOps(1, '2 A1 10 10 B2 20 20', '2 B1 10 10 E2 20 20', '2 E1 10 10 H2 20 20', '2 H1 10 10 I2 20 20', '2 H1 10 10 XFD2 20 20'),
                { warnings: true } // missing transformation of absolute drawing position
            );
        });
        it('should log warnings for absolute anchor', function () {
            var anchorOps = drawingAnchorOps(1, '2 A1 0 0 C3 0 0');
            testRunner.expectBidiWarnings(insertCols(1, 'C'), anchorOps, anchorOps.length);
        });
        it('should not log warnings without anchor attribute', function () {
            testRunner.expectBidiWarnings(insertCols(1, 'C'), drawingNoAnchorOps(1), 0);
        });
        it('should not transform drawing anchor in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), drawingAnchorOps(2, '2 A1 10 10 D4 20 20'));
        });
    });
*/
    // it('should transform drawing anchor', function () {
    public void insertColumnsDrawingAnchor01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createDrawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 0 20", "2 B1 10 10 C2 20 20", "2 C1 10 10 D2 20 20", "2 G1 10 10 XFD2 20 20").toString(),
                                SheetHelper.createInsertColsOp(1, "C").toString(), SheetHelper.createDrawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 0 20", "2 B1 10 10 D2 20 20", "2 D1 10 10 E2 20 20", "2 G1 10 10 XFD2 20 20").toString());
    }

    public void insertColumnsDrawingAnchor02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C:D D:E").toString(), SheetHelper.createDrawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 C2 20 20", "2 C1 10 10 D2 20 20", "2 D1 10 10 E2 20 20", "2 G1 10 10 XFC2 20 20").toString(),
                                SheetHelper.createInsertColsOp(1, "C:D D:E").toString(), SheetHelper.createDrawingAnchorOps(1, "2 A1 10 10 B2 20 20", "2 B1 10 10 E2 20 20", "2 E1 10 10 H2 20 20", "2 H1 10 10 I2 20 20", "2 H1 10 10 XFD2 20 20").toString());
    }

    // it('should log warnings for absolute anchor', function () {
    // no warnings on the backend ...

    // it('should not log warnings without anchor attribute', function () {
    // no warnings on the backend

    // it('should not transform drawing anchor in different sheets', function () {
    @Test
    public void insertColumnsDrawingAnchor05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createInsertColsOp(1, "C").toString(),SheetHelper.createDrawingAnchorOps(2, "2 A1 10 10 D4 20 20").toString());
    }

/* there is no selection on backend side ... these tests are not necessary
    describe('"insertColumns" and "sheetSelection"', function () {
        it('should transform selected ranges', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C:D D:E'), sheetSelection(1, 'A1 B2 C3 D4 A1:B2 B2:C3 C3:D4 D4:E5 XEW1:XEZ4 XEX1:XFA4 XEY1:XFB4 XEZ1:XFC4', 0, 'A1', [0, 1]),
                insertCols(1, 'C:D D:E'), sheetSelection(1, 'A1 B2 E3 H4 A1:B2 B2:E3 E3:H4 H4:I5 XFA1:XFD4 XFB1:XFD4 XFC1:XFD4 XFD1:XFD4', 0, 'A1', [0, 1])
            );
        });
        it('should transform active address', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C:D D:E'), opSeries2(sheetSelection, 1, 'A1:XFD4', 0, ['A1', 'B2', 'C3', 'D4', 'XEZ1', 'XFA2']),
                insertCols(1, 'C:D D:E'), opSeries2(sheetSelection, 1, 'A1:XFD4', 0, ['A1', 'B2', 'E3', 'H4', 'XFD1', 'XFD2'])
            );
        });
        it('should transform origin address', function () {
            testRunner.runBidiTest(
                insertCols(1, 'C:D D:E'), opSeries2(sheetSelection, 1, 'A1:XFD4', 0, 'A1', ['A1', 'B2', 'C3', 'D4', 'XEZ1', 'XFA2']),
                insertCols(1, 'C:D D:E'), opSeries2(sheetSelection, 1, 'A1:XFD4', 0, 'A1', ['A1', 'B2', 'E3', 'H4', 'XFD1', 'XFD2'])
            );
        });
        it('should transform deleted active range', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), sheetSelection(1, 'D4 C3 XFD1:XFD4 B2 A1', 0, 'D4',   'D4'), null, sheetSelection(1, 'E4 D3 B2 A1', 0, 'E4', 'E4'));
            testRunner.runBidiTest(insertCols(1, 'C'), sheetSelection(1, 'D4 C3 XFD1:XFD4 B2 A1', 1, 'C3',   'C3'), null, sheetSelection(1, 'E4 D3 B2 A1', 1, 'D3', 'D3'));
            testRunner.runBidiTest(insertCols(1, 'C'), sheetSelection(1, 'D4 C3 XFD1:XFD4 B2 A1', 2, 'XFD1', 'A1'), null, sheetSelection(1, 'E4 D3 B2 A1', 0, 'E4'));
            testRunner.runBidiTest(insertCols(1, 'C'), sheetSelection(1, 'D4 C3 XFD1:XFD4 B2 A1', 3, 'B2',   'B2'), null, sheetSelection(1, 'E4 D3 B2 A1', 2, 'B2', 'B2'));
            testRunner.runBidiTest(insertCols(1, 'C'), sheetSelection(1, 'D4 C3 XFD1:XFD4 B2 A1', 4, 'A1',   'A1'), null, sheetSelection(1, 'E4 D3 B2 A1', 3, 'A1', 'A1'));
        });
        it('should keep active cell when deleting the selection', function () {
            testRunner.runBidiTest(insertCols(1, 'C:D D:E'), sheetSelection(1, 'XFC6:XFD9 XFA1:XFD4', 0, 'XFC7'), null, sheetSelection(1, 'XFD7', 0, 'XFD7'));
            testRunner.runBidiTest(insertCols(1, 'C:D D:E'), sheetSelection(1, 'XFC6:XFD9 XFA1:XFD4', 1, 'XFB2'), null, sheetSelection(1, 'XFD2', 0, 'XFD2'));
        });
        it('should not transform ranges in different sheets', function () {
            testRunner.runBidiTest(insertCols(1, 'C'), sheetSelection(2, 'A1:D4', 0, 'A1'));
        });
    });
*/
}
