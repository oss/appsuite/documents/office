/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class MoveSheet {

/*
        describe('"moveSheet" and independent operations', function () {
            it('should skip transformations', function () {
                testRunner.runBidiTest(moveSheet(2, 4), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
            });
        });
*/
    @Test
    public void moveSheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 4).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.AUTOSTYLE_OPS
            )
        );
    }

/*
    describe('"moveSheet" and "moveSheet"', function () {
        it('should handle two move operations transforming each other', function () {
            testRunner.runTest(moveSheet(2, 4), moveSheet(3, 1), moveSheet(3, 4), moveSheet(2, 1));
            testRunner.runTest(moveSheet(1, 3), moveSheet(4, 2), moveSheet(1, 4), moveSheet(4, 1));
            testRunner.runTest(moveSheet(1, 3), moveSheet(2, 3), moveSheet(1, 2), moveSheet(1, 3));
            testRunner.runTest(moveSheet(1, 2), moveSheet(2, 4), [],              moveSheet(1, 4));
            testRunner.runTest(moveSheet(2, 4), moveSheet(1, 2), moveSheet(1, 4), []);
            testRunner.runTest(moveSheet(1, 3), moveSheet(1, 4), moveSheet(4, 3), []);
            testRunner.runTest(moveSheet(1, 3), moveSheet(1, 3), [],              []);
        });
    });
*/
    @Test
    public void moveSheetMoveSheet01() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createMoveSheetOp(3, 1).toString(), SheetHelper.createMoveSheetOp(3, 4).toString(), SheetHelper.createMoveSheetOp(2, 1).toString(), null);
    }

    @Test
    public void moveSheetMoveSheet02() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMoveSheetOp(1, 3).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createMoveSheetOp(1, 4).toString(), SheetHelper.createMoveSheetOp(4, 1).toString(), null);
    }

    @Test
    public void moveSheetMoveSheet03() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMoveSheetOp(1, 3).toString(), SheetHelper.createMoveSheetOp(2, 3).toString(), SheetHelper.createMoveSheetOp(1, 2).toString(), SheetHelper.createMoveSheetOp(1, 3).toString(), null);
    }

    @Test
    public void moveSheetMoveSheet04() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMoveSheetOp(1, 2).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), "[]", SheetHelper.createMoveSheetOp(1, 4).toString(), null);
    }

   @Test
    public void moveSheetMoveSheet05() throws JSONException {
        SheetHelper.runTest(SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createMoveSheetOp(1, 2).toString(), SheetHelper.createMoveSheetOp(1, 4).toString(), "[]", null);
    }

   @Test
   public void moveSheetMoveSheet06() throws JSONException {
       SheetHelper.runTest(SheetHelper.createMoveSheetOp(1, 3).toString(), SheetHelper.createMoveSheetOp(1, 4).toString(), SheetHelper.createMoveSheetOp(4, 3).toString(), "[]", null);
   }

   @Test
   public void moveSheetMoveSheet07() throws JSONException {
       SheetHelper.runTest(SheetHelper.createMoveSheetOp(1, 3).toString(), SheetHelper.createMoveSheetOp(1, 3).toString(), "[]", "[]", null);
   }

/*
    describe('"moveSheet" and "copySheet"', function () {
        it('should handle move operation and copy operation transforming each other', function () {
            testRunner.runBidiTest(moveSheet(2, 4), copySheet(1, 3), moveSheet(2, 5), copySheet(1, 2));
            testRunner.runBidiTest(moveSheet(2, 4), copySheet(2, 3), moveSheet(2, 5), copySheet(4, 2));
            testRunner.runBidiTest(moveSheet(2, 4), copySheet(3, 3), moveSheet(2, 5), copySheet(2, 2));
            testRunner.runBidiTest(moveSheet(2, 4), copySheet(3, 2), moveSheet(3, 5), copySheet(2, 2));
            testRunner.runBidiTest(moveSheet(2, 4), copySheet(3, 1), moveSheet(3, 5), copySheet(2, 1));
            testRunner.runBidiTest(moveSheet(4, 2), copySheet(1, 3), moveSheet(5, 2), copySheet(1, 4));
            testRunner.runBidiTest(moveSheet(4, 2), copySheet(2, 3), moveSheet(5, 2), copySheet(3, 4));
            testRunner.runBidiTest(moveSheet(4, 2), copySheet(3, 3), moveSheet(5, 2), copySheet(4, 4));
            testRunner.runBidiTest(moveSheet(4, 2), copySheet(3, 2), moveSheet(5, 3), copySheet(4, 2));
            testRunner.runBidiTest(moveSheet(4, 2), copySheet(3, 1), moveSheet(5, 3), copySheet(4, 1));
            testRunner.runBidiTest(moveSheet(3, 3), copySheet(2, 4), []);
        });
    });
*/
    @Test
    public void moveSheetCopySheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createCopySheetOp(1, 3, null).toString(), SheetHelper.createMoveSheetOp(2, 5).toString(), SheetHelper.createCopySheetOp(1, 2, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), SheetHelper.createMoveSheetOp(2, 5).toString(), SheetHelper.createCopySheetOp(4, 2, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createCopySheetOp(3, 3, null).toString(), SheetHelper.createMoveSheetOp(2, 5).toString(), SheetHelper.createCopySheetOp(2, 2, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), SheetHelper.createMoveSheetOp(3, 5).toString(), SheetHelper.createCopySheetOp(2, 2, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createCopySheetOp(3, 1, null).toString(), SheetHelper.createMoveSheetOp(3, 5).toString(), SheetHelper.createCopySheetOp(2, 1, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createCopySheetOp(1, 3, null).toString(), SheetHelper.createMoveSheetOp(5, 2).toString(), SheetHelper.createCopySheetOp(1, 4, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), SheetHelper.createMoveSheetOp(5, 2).toString(), SheetHelper.createCopySheetOp(3, 4, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createCopySheetOp(3, 3, null).toString(), SheetHelper.createMoveSheetOp(5, 2).toString(), SheetHelper.createCopySheetOp(4, 4, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), SheetHelper.createMoveSheetOp(5, 3).toString(), SheetHelper.createCopySheetOp(4, 2, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet10() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createCopySheetOp(3, 1, null).toString(), SheetHelper.createMoveSheetOp(5, 3).toString(), SheetHelper.createCopySheetOp(4, 1, null).toString(), null);
    }

    @Test
    public void moveSheetCopySheet11() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(3, 3).toString(), SheetHelper.createCopySheetOp(2, 4, null).toString(), "[]", SheetHelper.createCopySheetOp(2, 4, null).toString(), null);
    }

/*
    describe('"moveSheet" and "moveSheets"', function () {
        it('should transform the vector, and ignore the single move', function () {
            testRunner.runBidiTest(moveSheet(2, 1), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 3, 4, 1, 2, 5]));
            testRunner.runBidiTest(moveSheet(2, 2), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 3, 4, 2, 1, 5]));
            testRunner.runBidiTest(moveSheet(2, 3), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 2, 4, 3, 1, 5]));
            testRunner.runBidiTest(moveSheet(2, 4), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 2, 3, 4, 1, 5]));
            testRunner.runBidiTest(moveSheet(2, 5), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 2, 3, 5, 1, 4]));
            testRunner.runBidiTest(moveSheet(4, 1), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 4, 1, 3, 2, 5]));
            testRunner.runBidiTest(moveSheet(4, 2), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 4, 2, 3, 1, 5]));
            testRunner.runBidiTest(moveSheet(4, 3), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 4, 3, 2, 1, 5]));
            testRunner.runBidiTest(moveSheet(4, 4), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 3, 4, 2, 1, 5]));
            testRunner.runBidiTest(moveSheet(4, 5), moveSheets(MOVE_SHEETS), [], moveSheets([0, 6, 3, 5, 2, 1, 4]));
        });
        it('should detect no-op "moveSheets" operation', function () {
            testRunner.runBidiTest(moveSheet(1, 2), moveSheets([0, 2, 1, 3, 4]), [], []);
        });
    });
*/
    @Test
    public void moveSheetMoveSheets01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 1).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 3 4 1 2 5").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 2).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 3 4 2 1 5").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 3).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 2 4 3 1 5").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 2 3 4 1 5").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 5).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 2 3 5 1 4").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 1).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 4 1 3 2 5").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 4 2 3 1 5").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 3).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 4 3 2 1 5").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 4).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 3 4 2 1 5").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets10() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 5).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), "[]", SheetHelper.createMoveSheetsOp("0 6 3 5 2 1 4").toString(), null);
    }

    @Test
    public void moveSheetMoveSheets11() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(1, 2).toString(), SheetHelper.createMoveSheetsOp("0 2 1 3 4").toString(), "[]", "[]", null);
    }

/*
    describe('"moveSheet" and sheet index operations', function () {
        it('should shift sheet index', function () {
            testRunner.runBidiTest(
                moveSheet(2, 4), allSheetIndexOps(1, 2, 3, 4, 5),
                moveSheet(2, 4), allSheetIndexOps(1, 4, 2, 3, 5)
            );
            testRunner.runBidiTest(
                moveSheet(4, 2), allSheetIndexOps(1, 2, 3, 4, 5),
                moveSheet(4, 2), allSheetIndexOps(1, 3, 4, 2, 5)
            );
            testRunner.runBidiTest(
                moveSheet(3, 3), allSheetIndexOps(1, 2, 3, 4, 5)
            );
        });
    });
*/
    @Test
    public void moveSheetSheetIndexOps00() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 4).toString(), Helper.createMoveDrawingOp("2 0", "0 2").toString(),
                                SheetHelper.createMoveSheetOp(2, 4).toString(), Helper.createMoveDrawingOp("4 0", "0 2").toString(), null);
    }

    @Test
    public void moveSheetSheetIndexOps01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createAllSheetIndexOps("1 2 3 4 5", true),
                                SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createAllSheetIndexOps("1 4 2 3 5", true), null);
    }

    @Test
    public void moveSheetSheetIndexOps02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createAllSheetIndexOps("1 2 3 4 5", true),
                                SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createAllSheetIndexOps("1 3 4 2 5", true), null);
    }

    @Test
    public void moveSheetSheetIndexOps03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createMoveSheetOp(3, 3).toString(), SheetHelper.createAllSheetIndexOps("1 2 3 4 5", true));
    }
}
