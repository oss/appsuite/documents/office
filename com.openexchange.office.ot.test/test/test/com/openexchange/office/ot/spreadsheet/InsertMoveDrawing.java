/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class InsertMoveDrawing {

    /*
     * describe('"insertDrawing" and "moveDrawing"', function () {
            it('should skip different sheets', function () {
                testRunner.runTest(insertShape(1, 0, ATTRS), moveDrawing(0, 1, 2));
            });
            it('should shift positions for top-level shape', function () {
                testRunner.runBidiTest(insertShape(1, 1, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 1, ATTRS), moveDrawing(1, 3, 5));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 2, ATTRS), moveDrawing(1, 3, 5));
                testRunner.runBidiTest(insertShape(1, 3, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 2, ATTRS), moveDrawing(1, 2, 5));
                testRunner.runBidiTest(insertShape(1, 4, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 3, ATTRS), moveDrawing(1, 2, 5));
                testRunner.runBidiTest(insertShape(1, 5, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 5, ATTRS), moveDrawing(1, 2, 4));
                testRunner.runBidiTest(insertShape(1, 6, ATTRS), moveDrawing(1, 2, 4), insertShape(1, 6, ATTRS), moveDrawing(1, 2, 4));
                testRunner.runBidiTest(insertShape(1, 1, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 1, ATTRS), moveDrawing(1, 5, 3));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 2, ATTRS), moveDrawing(1, 5, 3));
                testRunner.runBidiTest(insertShape(1, 3, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 4, ATTRS), moveDrawing(1, 5, 2));
                testRunner.runBidiTest(insertShape(1, 4, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 5, ATTRS), moveDrawing(1, 5, 2));
                testRunner.runBidiTest(insertShape(1, 5, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 5, ATTRS), moveDrawing(1, 4, 2));
                testRunner.runBidiTest(insertShape(1, 6, ATTRS), moveDrawing(1, 4, 2), insertShape(1, 6, ATTRS), moveDrawing(1, 4, 2));
                testRunner.runBidiTest(insertShape(1, 1, ATTRS), moveDrawing(1, 3, 3), null, []);
            });
            it('should shift positions for embedded shape', function () {
                testRunner.runBidiTest(insertShape(1, '1 1', ATTRS), moveDrawing(1, '1 2', '1 4'), insertShape(1, '1 1', ATTRS), moveDrawing(1, '1 3', '1 5'));
                testRunner.runBidiTest(insertShape(1, '1 2', ATTRS), moveDrawing(1, '1 2', '1 4'), insertShape(1, '1 2', ATTRS), moveDrawing(1, '1 3', '1 5'));
                testRunner.runBidiTest(insertShape(1, '1 3', ATTRS), moveDrawing(1, '1 2', '1 4'), insertShape(1, '1 2', ATTRS), moveDrawing(1, '1 2', '1 5'));
                testRunner.runBidiTest(insertShape(1, '1 4', ATTRS), moveDrawing(1, '1 2', '1 4'), insertShape(1, '1 3', ATTRS), moveDrawing(1, '1 2', '1 5'));
                testRunner.runBidiTest(insertShape(1, '1 5', ATTRS), moveDrawing(1, '1 2', '1 4'), insertShape(1, '1 5', ATTRS), moveDrawing(1, '1 2', '1 4'));
                testRunner.runBidiTest(insertShape(1, '1 6', ATTRS), moveDrawing(1, '1 2', '1 4'), insertShape(1, '1 6', ATTRS), moveDrawing(1, '1 2', '1 4'));
                testRunner.runBidiTest(insertShape(1, '1 1', ATTRS), moveDrawing(1, '1 4', '1 2'), insertShape(1, '1 1', ATTRS), moveDrawing(1, '1 5', '1 3'));
                testRunner.runBidiTest(insertShape(1, '1 2', ATTRS), moveDrawing(1, '1 4', '1 2'), insertShape(1, '1 2', ATTRS), moveDrawing(1, '1 5', '1 3'));
                testRunner.runBidiTest(insertShape(1, '1 3', ATTRS), moveDrawing(1, '1 4', '1 2'), insertShape(1, '1 4', ATTRS), moveDrawing(1, '1 5', '1 2'));
                testRunner.runBidiTest(insertShape(1, '1 4', ATTRS), moveDrawing(1, '1 4', '1 2'), insertShape(1, '1 5', ATTRS), moveDrawing(1, '1 5', '1 2'));
                testRunner.runBidiTest(insertShape(1, '1 5', ATTRS), moveDrawing(1, '1 4', '1 2'), insertShape(1, '1 5', ATTRS), moveDrawing(1, '1 4', '1 2'));
                testRunner.runBidiTest(insertShape(1, '1 6', ATTRS), moveDrawing(1, '1 4', '1 2'), insertShape(1, '1 6', ATTRS), moveDrawing(1, '1 4', '1 2'));
                testRunner.runBidiTest(insertShape(1, '1 1', ATTRS), moveDrawing(1, '1 3', '1 3'), null, []);
            });
            it('should shift positions of embedded inserted shapes', function () {
                testRunner.runBidiTest(insertShape(1, '1 1', ATTRS), moveDrawing(1, 2, 4), insertShape(1, '1 1', ATTRS));
                testRunner.runBidiTest(insertShape(1, '2 1', ATTRS), moveDrawing(1, 2, 4), insertShape(1, '4 1', ATTRS));
                testRunner.runBidiTest(insertShape(1, '3 1', ATTRS), moveDrawing(1, 2, 4), insertShape(1, '2 1', ATTRS));
                testRunner.runBidiTest(insertShape(1, '4 1', ATTRS), moveDrawing(1, 2, 4), insertShape(1, '3 1', ATTRS));
                testRunner.runBidiTest(insertShape(1, '5 1', ATTRS), moveDrawing(1, 2, 4), insertShape(1, '5 1', ATTRS));
            });
            it('should shift positions of embedded moved shapes', function () {
                testRunner.runBidiTest(insertShape(1, 1, ATTRS), moveDrawing(1, '3 2', '3 4'), null, moveDrawing(1, '4 2', '4 4'));
                testRunner.runBidiTest(insertShape(1, 2, ATTRS), moveDrawing(1, '3 2', '3 4'), null, moveDrawing(1, '4 2', '4 4'));
                testRunner.runBidiTest(insertShape(1, 3, ATTRS), moveDrawing(1, '3 2', '3 4'), null, moveDrawing(1, '4 2', '4 4'));
                testRunner.runBidiTest(insertShape(1, 4, ATTRS), moveDrawing(1, '3 2', '3 4'));
                testRunner.runBidiTest(insertShape(1, 5, ATTRS), moveDrawing(1, '3 2', '3 4'));
            });
            it('should not shift positions of independent shapes', function () {
                testRunner.runBidiTest(insertShape(1, '1 1',   ATTRS), moveDrawing(1, '0 0',   '0 1'));
                testRunner.runBidiTest(insertShape(1, '1 1',   ATTRS), moveDrawing(1, '2 0',   '2 1'));
                testRunner.runBidiTest(insertShape(1, '1 1 1', ATTRS), moveDrawing(1, '0 1 1', '0 1 2'));
            });
        });
    */

    @Test
    public void insertDrawingMoveDrawing01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runTest(
        //  insertShape(1, 0, ATTRS),
        //  moveDrawing(0, 1, 2));
        SheetHelper.runTest(
            Helper.createInsertShapeOp("1 0", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("0 1", "0 2").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing02() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 1, ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, 1, ATTRS),
        //  moveDrawing(1, 3, 5));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 3", "1 5").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing03() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, 2, ATTRS),
        //  moveDrawing(1, 3, 5));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 3", "1 5").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing04() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 3, ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, 2, ATTRS),
        //  moveDrawing(1, 2, 5));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 5").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing05() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 4, ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, 3, ATTRS),
        //  moveDrawing(1, 2, 5));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 4", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 5").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing06() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 5, ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, 5, ATTRS),
        //  moveDrawing(1, 2, 4));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing07() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 6, ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, 6, ATTRS),
        //  moveDrawing(1, 2, 4));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 6", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 6", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing08() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 1, ATTRS),
        //  moveDrawing(1, 4, 2),
        //  insertShape(1, 1, ATTRS),
        //  moveDrawing(1, 5, 3));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 5", "1 3").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing09() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  moveDrawing(1, 4, 2),
        //  insertShape(1, 2, ATTRS),
        //  moveDrawing(1, 5, 3));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 5", "1 3").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing10() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 3, ATTRS),
        //  moveDrawing(1, 4, 2),
        //  insertShape(1, 4, ATTRS),
        //  moveDrawing(1, 5, 2));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createInsertShapeOp("1 4", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 5", "1 2").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing11() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 4, ATTRS),
        //  moveDrawing(1, 4, 2),
        //  insertShape(1, 5, ATTRS),
        //  moveDrawing(1, 5, 2));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 4", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createInsertShapeOp("1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 5", "1 2").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing12() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 5, ATTRS),
        //  moveDrawing(1, 4, 2),
        //  insertShape(1, 5, ATTRS),
        //  moveDrawing(1, 4, 2));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createInsertShapeOp("1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing13() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 6, ATTRS),
        //  moveDrawing(1, 4, 2),
        //  insertShape(1, 6, ATTRS),
        //  moveDrawing(1, 4, 2));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 6", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString(),
            Helper.createInsertShapeOp("1 6", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4", "1 2").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing14() throws JSONException {
        // Result: Should shift positions for top-level shape.
        // testRunner.runBidiTest(
        //  insertShape(1, 1, ATTRS),
        //  moveDrawing(1, 3, 3),
        //  null,
        // []);
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 3", "1 3").toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            "[]"
        );
    }

    @Test
    public void insertDrawingMoveDrawing15() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1', ATTRS),
        //  moveDrawing(1, '1 2', '1 4'),
        //  insertShape(1, '1 1', ATTRS),
        //  moveDrawing(1, '1 3', '1 5'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 3", "1 1 5").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing16() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 2', ATTRS),
        //  moveDrawing(1, '1 2', '1 4'),
        //  insertShape(1, '1 2', ATTRS),
        //  moveDrawing(1, '1 3', '1 5'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createInsertShapeOp("1 1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 3", "1 1 5").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing17() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 3', ATTRS),
        //  moveDrawing(1, '1 2', '1 4'),
        //  insertShape(1, '1 2', ATTRS),
        //  moveDrawing(1, '1 2', '1 5'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 3", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createInsertShapeOp("1 1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 5").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing18() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 4', ATTRS),
        //  moveDrawing(1, '1 2', '1 4'),
        //  insertShape(1, '1 3', ATTRS),
        //  moveDrawing(1, '1 2', '1 5'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 4", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createInsertShapeOp("1 1 3", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 5").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing19() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 5', ATTRS),
        //  moveDrawing(1, '1 2', '1 4'),
        //  insertShape(1, '1 5', ATTRS),
        //  moveDrawing(1, '1 2', '1 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createInsertShapeOp("1 1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing20() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 6', ATTRS),
        //  moveDrawing(1, '1 2', '1 4'),
        //  insertShape(1, '1 6', ATTRS),
        //  moveDrawing(1, '1 2', '1 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 6", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString(),
            Helper.createInsertShapeOp("1 1 6", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 2", "1 1 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing21() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1', ATTRS),
        //  moveDrawing(1, '1 4', '1 2'),
        //  insertShape(1, '1 1', ATTRS),
        //  moveDrawing(1, '1 5', '1 3'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 5", "1 1 3").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing22() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 2', ATTRS),
        //  moveDrawing(1, '1 4', '1 2'),
        //  insertShape(1, '1 2', ATTRS),
        //  moveDrawing(1, '1 5', '1 3'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createInsertShapeOp("1 1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 5", "1 1 3").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing23() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 3', ATTRS),
        //  moveDrawing(1, '1 4', '1 2'),
        //  insertShape(1, '1 4', ATTRS),
        //  moveDrawing(1, '1 5', '1 2'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 3", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createInsertShapeOp("1 1 4", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 5", "1 1 2").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing24() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 4', ATTRS),
        //  moveDrawing(1, '1 4', '1 2'),
        //  insertShape(1, '1 5', ATTRS),
        //  moveDrawing(1, '1 5', '1 2'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 4", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createInsertShapeOp("1 1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 5", "1 1 2").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing25() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 5', ATTRS),
        //  moveDrawing(1, '1 4', '1 2'),
        //  insertShape(1, '1 5', ATTRS),
        //  moveDrawing(1, '1 4', '1 2'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createInsertShapeOp("1 1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing26() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 6', ATTRS),
        //  moveDrawing(1, '1 4', '1 2'),
        //  insertShape(1, '1 6', ATTRS),
        //  moveDrawing(1, '1 4', '1 2'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 6", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString(),
            Helper.createInsertShapeOp("1 1 6", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 4", "1 1 2").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing27() throws JSONException {
        // Result: Should shift positions for embedded shape.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1', ATTRS),
        //  moveDrawing(1, '1 3', '1 3'),
        //  null,
        //  []);
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 1 3", "1 1 3").toString(),
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            "[]"
        );
    }

    @Test
    public void insertDrawingMoveDrawing28() throws JSONException {
        // Result: Should shift positions of embedded inserted shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1', ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, '1 1', ATTRS));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing29() throws JSONException {
        // Result: Should shift positions of embedded inserted shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '2 1', ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, '4 1', ATTRS));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 4 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing30() throws JSONException {
        // Result: Should shift positions of embedded inserted shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '3 1', ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, '2 1', ATTRS));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 3 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 2 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing31() throws JSONException {
        // Result: Should shift positions of embedded inserted shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '4 1', ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, '3 1', ATTRS));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 4 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 3 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing32() throws JSONException {
        // Result: Should shift positions of embedded inserted shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '5 1', ATTRS),
        //  moveDrawing(1, 2, 4),
        //  insertShape(1, '5 1', ATTRS));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 5 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString(),
            Helper.createInsertShapeOp("1 5 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2", "1 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing33() throws JSONException {
        // Result: Should shift positions of embedded moved shapes
        // testRunner.runBidiTest(
        //  insertShape(1, 1, ATTRS),
        //  moveDrawing(1, '3 2', '3 4'),
        //  null,
        //  moveDrawing(1, '4 2', '4 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString(),
            Helper.createInsertShapeOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4 2", "1 4 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing34() throws JSONException {
        // Result: Should shift positions of embedded moved shapes
        // testRunner.runBidiTest(
        //  insertShape(1, 2, ATTRS),
        //  moveDrawing(1, '3 2', '3 4'),
        //  null,
        //  moveDrawing(1, '4 2', '4 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString(),
            Helper.createInsertShapeOp("1 2", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4 2", "1 4 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing35() throws JSONException {
        // Result: Should shift positions of embedded moved shapes
        // testRunner.runBidiTest(
        //  insertShape(1, 3, ATTRS),
        //  moveDrawing(1, '3 2', '3 4'),
        //  null,
        //  moveDrawing(1, '4 2', '4 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString(),
            Helper.createInsertShapeOp("1 3", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 4 2", "1 4 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing36() throws JSONException {
        // Result: Should shift positions of embedded moved shapes
        // testRunner.runBidiTest(
        //  insertShape(1, 4, ATTRS),
        //  moveDrawing(1, '3 2', '3 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 4", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing37() throws JSONException {
        // Result: Should shift positions of embedded moved shapes
        // testRunner.runBidiTest(
        //  insertShape(1, 5, ATTRS),
        //  moveDrawing(1, '3 2', '3 4'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 5", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 3 2", "1 3 4").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing38() throws JSONException {
        // Result: Should not shift positions of independent shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1',   ATTRS),
        //  moveDrawing(1, '0 0',   '0 1'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 0 0", "1 0 1").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing39() throws JSONException {
        // Result: Should not shift positions of independent shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1',   ATTRS),
        //  moveDrawing(1, '2 0',   '2 1'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 2 0", "1 2 1").toString()
        );
    }

    @Test
    public void insertDrawingMoveDrawing40() throws JSONException {
        // Result: Should not shift positions of independent shapes.
        // testRunner.runBidiTest(
        //  insertShape(1, '1 1 1', ATTRS),
        //  moveDrawing(1, '0 1 1', '0 1 2'));
        SheetHelper.runBidiTest(
            Helper.createInsertShapeOp("1 1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createMoveDrawingOp("1 0 1 1", "1 0 1 2").toString()
        );
    }
}
