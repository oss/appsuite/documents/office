/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class DeleteSheet {

/*
        describe('"deleteSheet" and independent operations', function () {
            it('should skip transformations', function () {
                testRunner.runBidiTest(deleteSheet(1), [GLOBAL_OPS, STYLESHEET_OPS, AUTOSTYLE_OPS]);
            });
        });
*/
    @Test
    public void deleteSheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(),
            Helper.createArrayFromJSON(
                SheetHelper.GLOBAL_OPS,
                SheetHelper.STYLESHEET_OPS,
                SheetHelper.AUTOSTYLE_OPS
            )
        );
    }

/*
        describe('"deleteSheet" and "deleteSheet"', function () {
            it('should shift sheet indexes', function () {
                testRunner.runBidiTest(deleteSheet(2), deleteSheet(0), deleteSheet(1), deleteSheet(0));
                testRunner.runBidiTest(deleteSheet(2), deleteSheet(1), deleteSheet(1), deleteSheet(1));
                testRunner.runBidiTest(deleteSheet(2), deleteSheet(2), [],             []);
                testRunner.runBidiTest(deleteSheet(2), deleteSheet(3), deleteSheet(2), deleteSheet(2));
                testRunner.runBidiTest(deleteSheet(2), deleteSheet(4), deleteSheet(2), deleteSheet(3));
            });
        });
*/
    @Test
    public void deleteSheetDeleteSheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createDeleteSheetOp(0).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createDeleteSheetOp(0).toString(), null);
    }

    @Test
    public void deleteSheetDeleteSheet02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createDeleteSheetOp(1).toString(), null);
    }

    @Test
    public void deleteSheetDeleteSheet03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createDeleteSheetOp(2).toString(), "[]", "[]", null);
    }

    @Test
    public void deleteSheetDeleteSheet04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createDeleteSheetOp(2).toString(), null);
    }

    @Test
    public void deleteSheetDeleteSheet05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createDeleteSheetOp(3).toString(), null);
    }

/*
        describe('"deleteSheet" and "moveSheet"', function () {
            it('should handle sheet moved to end', function () {
                testRunner.runBidiTest(deleteSheet(1), moveSheet(2, 4), deleteSheet(1), moveSheet(1, 3));
                testRunner.runBidiTest(deleteSheet(2), moveSheet(2, 4), deleteSheet(4), []);
                testRunner.runBidiTest(deleteSheet(3), moveSheet(2, 4), deleteSheet(2), moveSheet(2, 3));
                testRunner.runBidiTest(deleteSheet(4), moveSheet(2, 4), deleteSheet(3), moveSheet(2, 3));
                testRunner.runBidiTest(deleteSheet(5), moveSheet(2, 4), deleteSheet(5), moveSheet(2, 4));
            });
            it('should handle sheet moved to front', function () {
                testRunner.runBidiTest(deleteSheet(1), moveSheet(4, 2), deleteSheet(1), moveSheet(3, 1));
                testRunner.runBidiTest(deleteSheet(2), moveSheet(4, 2), deleteSheet(3), moveSheet(3, 2));
                testRunner.runBidiTest(deleteSheet(3), moveSheet(4, 2), deleteSheet(4), moveSheet(3, 2));
                testRunner.runBidiTest(deleteSheet(4), moveSheet(4, 2), deleteSheet(2), []);
                testRunner.runBidiTest(deleteSheet(5), moveSheet(4, 2), deleteSheet(5), moveSheet(4, 2));
            });
            it('should handle no-op move', function () {
                testRunner.runBidiTest(deleteSheet(1), moveSheet(3, 3), null, []);
            });
        });
*/
    @Test
    public void deleteSheetMoveSheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createMoveSheetOp(1, 3).toString(), null);
    }

    @Test
    public void deleteSheetMoveSheet02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createDeleteSheetOp(4).toString(), "[]", null);
    }

    @Test
    public void deleteSheetMoveSheet03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createMoveSheetOp(2, 3).toString(), null);
    }

    @Test
    public void deleteSheetMoveSheet04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createMoveSheetOp(2, 3).toString(), null);
    }

    @Test
    public void deleteSheetMoveSheet05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createMoveSheetOp(2, 4).toString(), null);
    }

    @Test
    public void deleteSheetMoveSheet06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createMoveSheetOp(3, 1).toString(), null);
    }

    @Test
    public void deleteSheetMoveSheet07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createMoveSheetOp(3, 2).toString(), null);
    }

    @Test
    public void deleteSheetMoveSheet08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createMoveSheetOp(3, 2).toString(), null);
    }

    @Test
    public void deleteSheetMoveSheet09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createDeleteSheetOp(2).toString(), "[]", null);
    }

    @Test
    public void deleteSheetMoveSheet10() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createMoveSheetOp(4, 2).toString(), null);
    }

    @Test
    public void deleteSheetMoveSheet11() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createMoveSheetOp(3, 3).toString(), SheetHelper.createDeleteSheetOp(1).toString(), "[]", null);
    }

/*
        describe('"deleteSheet" and "copySheet"', function () {
            it('should handle sheet copied to end', function () {
                testRunner.runBidiTest(deleteSheet(1), copySheet(2, 4), deleteSheet(1), copySheet(1, 3));
                testRunner.runBidiTest(deleteSheet(2), copySheet(2, 4), opSeries1(deleteSheet, 2, 3), []);
                testRunner.runBidiTest(deleteSheet(3), copySheet(2, 4), deleteSheet(3), copySheet(2, 3));
                testRunner.runBidiTest(deleteSheet(4), copySheet(2, 4), deleteSheet(5), copySheet(2, 4));
                testRunner.runBidiTest(deleteSheet(5), copySheet(2, 4), deleteSheet(6), copySheet(2, 4));
                testRunner.runBidiTest(deleteSheet(1), copySheet(2, 3), deleteSheet(1), copySheet(1, 2));
                testRunner.runBidiTest(deleteSheet(2), copySheet(2, 3), opSeries1(deleteSheet, 2, 2), []);
                testRunner.runBidiTest(deleteSheet(3), copySheet(2, 3), deleteSheet(4), copySheet(2, 3));
                testRunner.runBidiTest(deleteSheet(4), copySheet(2, 3), deleteSheet(5), copySheet(2, 3));
            });
            it('should handle sheet copied to front', function () {
                testRunner.runBidiTest(deleteSheet(1), copySheet(4, 2), deleteSheet(1), copySheet(3, 1));
                testRunner.runBidiTest(deleteSheet(2), copySheet(4, 2), deleteSheet(3), copySheet(3, 2));
                testRunner.runBidiTest(deleteSheet(3), copySheet(4, 2), deleteSheet(4), copySheet(3, 2));
                testRunner.runBidiTest(deleteSheet(4), copySheet(4, 2), opSeries1(deleteSheet, 5, 2), []);
                testRunner.runBidiTest(deleteSheet(5), copySheet(4, 2), deleteSheet(6), copySheet(4, 2));
                testRunner.runBidiTest(deleteSheet(1), copySheet(3, 2), deleteSheet(1), copySheet(2, 1));
                testRunner.runBidiTest(deleteSheet(2), copySheet(3, 2), deleteSheet(3), copySheet(2, 2));
                testRunner.runBidiTest(deleteSheet(3), copySheet(3, 2), opSeries1(deleteSheet, 4, 2), []);
                testRunner.runBidiTest(deleteSheet(4), copySheet(3, 2), deleteSheet(5), copySheet(3, 2));
            });
            it('should handle sheet copied to itself', function () {
                testRunner.runBidiTest(deleteSheet(2), copySheet(3, 3), deleteSheet(2), copySheet(2, 2));
                testRunner.runBidiTest(deleteSheet(3), copySheet(3, 3), opSeries1(deleteSheet, 4, 3), []);
                testRunner.runBidiTest(deleteSheet(4), copySheet(3, 3), deleteSheet(5), copySheet(3, 3));
            });
        });
*/
    @Test
    public void deleteSheetCopySheet01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createCopySheetOp(1, 3, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createCopySheetOp(2, 4, null).toString(), Helper.createArrayFromJSON(SheetHelper.createDeleteSheetOp(2), SheetHelper.createDeleteSheetOp(3)), "[]", null);
    }

    @Test
    public void deleteSheetCopySheet03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createCopySheetOp(2, 4, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createCopySheetOp(2, 4, null).toString(), SheetHelper.createDeleteSheetOp(6).toString(), SheetHelper.createCopySheetOp(2, 4, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createCopySheetOp(1, 2, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), Helper.createArrayFromJSON(SheetHelper.createDeleteSheetOp(2), SheetHelper.createDeleteSheetOp(2)), "[]", null);
    }

    @Test
    public void deleteSheetCopySheet08() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet09() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createCopySheetOp(2, 3, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet10() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createCopySheetOp(4, 2, null).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createCopySheetOp(3, 1, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet11() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createCopySheetOp(4, 2, null).toString(), SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet12() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createCopySheetOp(4, 2, null).toString(), SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet13() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createCopySheetOp(4, 2, null).toString(), Helper.createArrayFromJSON(SheetHelper.createDeleteSheetOp(5), SheetHelper.createDeleteSheetOp(2)), "[]", null);
    }

    @Test
    public void deleteSheetCopySheet14() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createCopySheetOp(4, 2, null).toString(), SheetHelper.createDeleteSheetOp(6).toString(), SheetHelper.createCopySheetOp(4, 2, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet15() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createCopySheetOp(2, 1, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet16() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createCopySheetOp(2, 2, null).toString(), null);
    }

    @Test
    public void deleteSheetCopySheet17() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), Helper.createArrayFromJSON(SheetHelper.createDeleteSheetOp(4), SheetHelper.createDeleteSheetOp(2)), "[]", null);
    }

    @Test
    public void deleteSheetCopySheet18() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createCopySheetOp(3, 2, null).toString(), null);
    }
/*
        describe('"deleteSheet" and "moveSheets"', function () {
            it('should handle deleted sheet', function () {
                testRunner.runBidiTest(deleteSheet(0), moveSheets(MOVE_SHEETS), deleteSheet(0), moveSheets([5, 2, 3, 1, 0, 4]));
                testRunner.runBidiTest(deleteSheet(1), moveSheets(MOVE_SHEETS), deleteSheet(5), moveSheets([0, 5, 2, 3, 1, 4]));
                testRunner.runBidiTest(deleteSheet(2), moveSheets(MOVE_SHEETS), deleteSheet(4), moveSheets([0, 5, 2, 3, 1, 4]));
                testRunner.runBidiTest(deleteSheet(3), moveSheets(MOVE_SHEETS), deleteSheet(2), moveSheets([0, 5, 3, 2, 1, 4]));
                testRunner.runBidiTest(deleteSheet(4), moveSheets(MOVE_SHEETS), deleteSheet(3), moveSheets([0, 5, 3, 2, 1, 4]));
                testRunner.runBidiTest(deleteSheet(5), moveSheets(MOVE_SHEETS), deleteSheet(6), moveSheets([0, 5, 3, 4, 2, 1]));
                testRunner.runBidiTest(deleteSheet(6), moveSheets(MOVE_SHEETS), deleteSheet(1), moveSheets([0, 3, 4, 2, 1, 5]));
            });
            it('should detect no-op "moveSheets" operation', function () {
                testRunner.runBidiTest(deleteSheet(1), moveSheets([0, 2, 1, 3, 4]), deleteSheet(2), []);
            });
        });
*/
    @Test
    public void deleteSheetCopyMoveSheetst01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(0).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createDeleteSheetOp(0).toString(), SheetHelper.createMoveSheetsOp("5 2 3 1 0 4").toString(), null);
    }

    @Test
    public void deleteSheetCopyMoveSheetst02() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createMoveSheetsOp("0 5 2 3 1 4").toString(), null);
    }

    @Test
    public void deleteSheetCopyMoveSheetst03() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createMoveSheetsOp("0 5 2 3 1 4").toString(), null);
    }

    @Test
    public void deleteSheetCopyMoveSheetst04() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createDeleteSheetOp(2).toString(), SheetHelper.createMoveSheetsOp("0 5 3 2 1 4").toString(), null);
    }

    @Test
    public void deleteSheetCopyMoveSheetst05() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(4).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createDeleteSheetOp(3).toString(), SheetHelper.createMoveSheetsOp("0 5 3 2 1 4").toString(), null);
    }

    @Test
    public void deleteSheetCopyMoveSheetst06() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(5).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createDeleteSheetOp(6).toString(), SheetHelper.createMoveSheetsOp("0 5 3 4 2 1").toString(), null);
    }

    @Test
    public void deleteSheetCopyMoveSheetst07() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(6).toString(), SheetHelper.createMoveSheetsOp(SheetHelper.MOVE_SHEETS).toString(), SheetHelper.createDeleteSheetOp(1).toString(), SheetHelper.createMoveSheetsOp("0 3 4 2 1 5").toString(), null);
    }

/*
        describe('"deleteSheet" and sheet index operations', function () {
            it('should shift sheet index', function () {
                testRunner.runBidiTest(
                    deleteSheet(1), allSheetIndexOps(0, 3, 1, 4, 2),
                    deleteSheet(1), allSheetIndexOps(0, 2,    3, 1)
                );
            });
        });
*/

    @Test
    public void deleteSheetSheetIndex01() throws JSONException {
        SheetHelper.runBidiTest(SheetHelper.createDeleteSheetOp(1).toString(),  SheetHelper.createAllSheetIndexOps("0 3 1 4 2", false), SheetHelper.createDeleteSheetOp(1).toString(),  SheetHelper.createAllSheetIndexOps("0 2 3 1", false), null);
    }
}
