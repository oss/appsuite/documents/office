/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class MoveNoteInsertComment {

    /*
     * describe('"moveNotes" and "insertComment"', function () {
            it('should skip different sheets', function () {
                testRunner.runBidiTest(moveNotes(1, 'A1 B2', 'B2 A1'), insertComment(2, 'A1', 0));
                testRunner.runBidiTest(moveNotes(1, 'B2 C3', 'C3 B2'), insertComment(1, 'A1', 0));
            });
            it('should fail to insert comment for existing note', function () {
                testRunner.expectBidiError(moveNotes(1, 'A1 B2', 'B2 A1'), insertComment(1, 'A1', 0));
            });
            it('should delete moved note before inserting a comment to the address', function () {
                testRunner.runBidiTest(
                    moveNotes(1, 'B2 C3', 'A1 B2'), insertComment(1, 'A1', 0),
                    moveNotes(1, 'C3', 'B2'), [deleteNote(1, 'A1'), insertComment(1, 'A1', 0)]
                );
                testRunner.runBidiTest(
                    moveNotes(1, 'B2', 'A1'), insertComment(1, 'A1', 0),
                    [], [deleteNote(1, 'A1'), insertComment(1, 'A1', 0)]
                );
            });
        });
     */

    @Test
    public void moveNoteInsertComment01() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runBidiTest(
        //  moveNotes(1, 'A1 B2', 'B2 A1'),
        //  insertComment(2, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createMoveNotesOp(1, "A1 B2", "B2 A1").toString(),
            SheetHelper.createInsertCommentOp(2, "A1", 0, null).toString()
        );
    }

    @Test
    public void moveNoteInsertComment02() throws JSONException {
        // Result: Should skip different sheets.
        // testRunner.runBidiTest(
        //  moveNotes(1, 'B2 C3', 'C3 B2'),
        //  insertComment(1, 'A1', 0));
        SheetHelper.runBidiTest(
            SheetHelper.createMoveNotesOp(1, "B2 C3", "C3 B2").toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString()
        );
    }

    @Test
    public void moveNoteInsertComment03() throws JSONException {
        // Result: Should fail to insert comment for existing note.
        // testRunner.expectBidiError(
        //  moveNotes(1, 'A1 B2', 'B2 A1'),
        //  insertComment(1, 'A1', 0));
        SheetHelper.expectBidiError(
            SheetHelper.createMoveNotesOp(1, "A1 B2", "B2 A1").toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString()
        );
    }

    @Test
    public void moveNoteInsertComment04() throws JSONException {
        // Result: Should delete moved note before inserting a comment to the address.
        // testRunner.runBidiTest(
        //  moveNotes(1, 'B2 C3', 'A1 B2'),
        //  insertComment(1, 'A1', 0),
        //  moveNotes(1, 'C3', 'B2'),
        //  [deleteNote(1, 'A1'), insertComment(1, 'A1', 0)]);
        SheetHelper.runBidiTest(
            SheetHelper.createMoveNotesOp(1, "B2 C3", "A1 B2").toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            SheetHelper.createMoveNotesOp(1, "C3", "B2").toString(),
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "A1"),
                SheetHelper.createInsertCommentOp(1, "A1", 0, null)
            )
        );
    }

    @Test
    public void moveNoteInsertComment05() throws JSONException {
        // Result: Should delete moved note before inserting a comment to the address.
        // testRunner.runBidiTest(
        //  moveNotes(1, 'B2', 'A1'),
        //  insertComment(1, 'A1', 0),
        //  [],
        //  [deleteNote(1, 'A1'), insertComment(1, 'A1', 0)]);
        SheetHelper.runBidiTest(
            SheetHelper.createMoveNotesOp(1, "B2", "A1").toString(),
            SheetHelper.createInsertCommentOp(1, "A1", 0, null).toString(),
            "[]",
            Helper.createArrayFromJSON(
                SheetHelper.createDeleteNoteOp(1, "A1"),
                SheetHelper.createInsertCommentOp(1, "A1", 0, null)
            )
        );
    }
}
