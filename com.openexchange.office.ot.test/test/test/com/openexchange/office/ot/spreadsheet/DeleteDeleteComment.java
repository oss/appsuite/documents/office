/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;

public class DeleteDeleteComment {

    /*
     * describe('"deleteComment" and "deleteComment"', function () {
            it('should skip different sheets and anchors', function () {
                testRunner.runTest(deleteComment(1, 'A1', 0), deleteComment(2, 'A1', 0));
                testRunner.runTest(deleteComment(1, 'A1', 0), deleteComment(2, 'B2', 0));
            });
            it('should shift comment index', function () {
                testRunner.runTest(deleteComment(1, 'A1', 1), deleteComment(1, 'A1', 1), [],                        []);
                testRunner.runTest(deleteComment(1, 'A1', 1), deleteComment(1, 'A1', 2), deleteComment(1, 'A1', 1), deleteComment(1, 'A1', 1));
                testRunner.runTest(deleteComment(1, 'A1', 1), deleteComment(1, 'A1', 3), deleteComment(1, 'A1', 1), deleteComment(1, 'A1', 2));
                testRunner.runTest(deleteComment(1, 'A1', 2), deleteComment(1, 'A1', 1), deleteComment(1, 'A1', 1), deleteComment(1, 'A1', 1));
                testRunner.runTest(deleteComment(1, 'A1', 2), deleteComment(1, 'A1', 2), [],                        []);
                testRunner.runTest(deleteComment(1, 'A1', 2), deleteComment(1, 'A1', 3), deleteComment(1, 'A1', 2), deleteComment(1, 'A1', 2));
                testRunner.runTest(deleteComment(1, 'A1', 3), deleteComment(1, 'A1', 1), deleteComment(1, 'A1', 2), deleteComment(1, 'A1', 1));
                testRunner.runTest(deleteComment(1, 'A1', 3), deleteComment(1, 'A1', 2), deleteComment(1, 'A1', 2), deleteComment(1, 'A1', 2));
                testRunner.runTest(deleteComment(1, 'A1', 3), deleteComment(1, 'A1', 3), [],                        []);
            });
            it('should handle deleted threads', function () {
                testRunner.runTest(deleteComment(1, 'A1', 0), deleteComment(1, 'A1', 2), null, []);
                testRunner.runTest(deleteComment(1, 'A1', 0), deleteComment(1, 'A1', 1), null, []);
                testRunner.runTest(deleteComment(1, 'A1', 0), deleteComment(1, 'A1', 0), [], []);
                testRunner.runTest(deleteComment(1, 'A1', 1), deleteComment(1, 'A1', 0), []);
                testRunner.runTest(deleteComment(1, 'A1', 2), deleteComment(1, 'A1', 0), []);
            });
        });
      */

    @Test
    public void deleteCommentDeleteComment01() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runTest(
        //  deleteComment(1, 'A1', 0),
        //  deleteComment(2, 'A1', 0));
        SheetHelper.runTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            SheetHelper.createDeleteCommentOp(2, "A1", 0).toString()
        );
    }

    @Test
    public void deleteCommentDeleteComment02() throws JSONException {
        // Result: Should skip different sheets and anchors.
        // testRunner.runTest(
        //  deleteComment(1, 'A1', 0),
        //  deleteComment(2, 'B2', 0));
        SheetHelper.runTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            SheetHelper.createDeleteCommentOp(2, "B2", 0).toString()
        );
    }

    @Test
    public void deleteCommentDeleteComment03() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  deleteComment(1, 'A1', 1),
        //  deleteComment(1, 'A1', 1),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteCommentDeleteComment04() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  deleteComment(1, 'A1', 1),
        //  deleteComment(1, 'A1', 2),
        //  deleteComment(1, 'A1', 1),
        //  deleteComment(1, 'A1', 1));
        SheetHelper.runTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 2).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString()
        );
    }

    @Test
    public void deleteCommentDeleteComment05() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  deleteComment(1, 'A1', 1),
        //  deleteComment(1, 'A1', 3),
        //  deleteComment(1, 'A1', 1),
        //  deleteComment(1, 'A1', 2));
        SheetHelper.runTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 3).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 2).toString()
        );
    }

    @Test
    public void deleteCommentDeleteComment06() throws JSONException {
        // Result: Should shift comment index.
        // testRunner.runTest(
        //  deleteComment(1, 'A1', 2),
        //  deleteComment(1, 'A1', 1),
        //  deleteComment(1, 'A1', 1),
        //  deleteComment(1, 'A1', 1));
        SheetHelper.runTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 2).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 1).toString()
        );
    }

    @Test
    public void deleteCommentDeleteComment07() throws JSONException {
        // Result: Should handle deleted threads.
        // testRunner.runTest(
        //  deleteComment(1, 'A1', 0),
        //  deleteComment(1, 'A1', 0),
        //  [],
        //  []);
        SheetHelper.runTest(
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            SheetHelper.createDeleteCommentOp(1, "A1", 0).toString(),
            "[]",
            "[]"
        );
    }
}
