/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.spreadsheet;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;

public class ChangeDrawingChangeDrawing {

    /*
     * describe('"changeDrawing" and independent operations', function () {
            it('should skip transformations', function () {
                testRunner.runBidiTest(changeDrawing(1, 0, ATTRS), [selectOps(1), chartOps(1), drawingTextOps(1), positionOp(1, 0)]);
            });
        });

        describe('"changeDrawing" and "changeDrawing"', function () {
            it('should not process different shapes', function () {
                testRunner.runTest(changeDrawing(1, 0, ATTRS), changeDrawing(0, 1, ATTRS));
                testRunner.runTest(changeDrawing(1, 0, ATTRS), changeDrawing(1, 1, ATTRS));
                testRunner.runBidiTest(changeDrawing(1, 0, ATTRS), changeDrawing(1, '0 1', ATTRS));
                testRunner.runBidiTest(changeDrawing(1, '0 0', ATTRS), changeDrawing(1, '0 1', ATTRS));
                testRunner.runBidiTest(changeDrawing(1, '1 1', ATTRS), changeDrawing(1, '0 1', ATTRS));
            });
            it('should not process operations for independent attributes', function () {
                testRunner.runTest(changeDrawing(1, 1, ATTRS_I1), changeDrawing(1, 1, ATTRS_I2));
            });
            it('should reduce attribute sets', function () {
                testRunner.runTest(
                    changeDrawing(1, 1, ATTRS_O1), changeDrawing(1, 1, ATTRS_O2),
                    changeDrawing(1, 1, ATTRS_R1), changeDrawing(1, 1, ATTRS_R2)
                );
            });
            it('should set equal operations to "removed" state', function () {
                testRunner.runTest(changeDrawing(1, 1, ATTRS), changeDrawing(1, 1, ATTRS), [], []);
                testRunner.runBidiTest(changeDrawing(1, 1, {}), changeDrawing(1, 1, ATTRS), []);
            });
        });
    */

    @Test
    public void changeDrawingIndependentOperations01() throws JSONException {
        // Result: Should skip transformations.
        //  testRunner.runBidiTest(
        //  changeDrawing(1, 0, ATTRS),
        //  [selectOps(1),
        //  chartOps(1),
        //  drawingTextOps(1),
        //  positionOp(1, 0)]);
        SheetHelper.runTest(
            Helper.createChangeDrawingOp("1 0", SheetHelper.ATTRS).toString(),
            Helper.createArrayFromJSON(
                // SelectOps not working in backend
                SheetHelper.createChartOps("1"),
                SheetHelper.createDrawingTextOps("1")
                // Position not working in backend
            )
        );
    }

    @Test
    public void changeDrawingChangeDrawing01() throws JSONException {
        // Result: Should not process different shapes.
        // testRunner.runTest(
        //  changeDrawing(1, 0, ATTRS),
        //  changeDrawing(0, 1, ATTRS));
        SheetHelper.runTest(
            Helper.createChangeDrawingOp("1 0", SheetHelper.ATTRS).toString(),
            Helper.createChangeDrawingOp("0 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void changeDrawingChangeDrawing02() throws JSONException {
        // Result: Should not process different shapes.
        // testRunner.runTest(
        //  changeDrawing(1, 0, ATTRS),
        //  changeDrawing(1, 1, ATTRS));
        SheetHelper.runTest(
            Helper.createChangeDrawingOp("1 0", SheetHelper.ATTRS).toString(),
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void changeDrawingChangeDrawing03() throws JSONException {
        // Result: Should not process different shapes.
        // testRunner.runBidiTest(
        //  changeDrawing(1, 0, ATTRS),
        //  changeDrawing(1, '0 1', ATTRS));
        SheetHelper.runTest(
            Helper.createChangeDrawingOp("1 0", SheetHelper.ATTRS).toString(),
            Helper.createChangeDrawingOp("1 0 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void changeDrawingChangeDrawing04() throws JSONException {
        // Result: Should not process different shapes.
        // testRunner.runBidiTest(
        //  changeDrawing(1, '0 0', ATTRS),
        //  changeDrawing(1, '0 1', ATTRS));
        SheetHelper.runTest(
            Helper.createChangeDrawingOp("1 0 0", SheetHelper.ATTRS).toString(),
            Helper.createChangeDrawingOp("1 0 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void changeDrawingChangeDrawing05() throws JSONException {
        // Result: Should not process different shapes.
        // testRunner.runBidiTest(
        //  changeDrawing(1, '1 1', ATTRS),
        //  changeDrawing(1, '0 1', ATTRS));
        SheetHelper.runTest(
            Helper.createChangeDrawingOp("1 1 1", SheetHelper.ATTRS).toString(),
            Helper.createChangeDrawingOp("1 0 1", SheetHelper.ATTRS).toString()
        );
    }

    @Test
    public void changeDrawingChangeDrawing06() throws JSONException {
        // Result: Should not process operations for independent attributes.
        // testRunner.runTest(changeDrawing(1, 1, ATTRS_I1),
        //  changeDrawing(1, 1, ATTRS_I2));
        SheetHelper.runTest(
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS_I1).toString(),
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS_I2).toString()
        );
    }

    @Test
    public void changeDrawingChangeDrawing07() throws JSONException {
        // Result: Should reduce attribute sets.
        // testRunner.runTest(
        //  changeDrawing(1, 1, ATTRS_O1),
        //  changeDrawing(1, 1, ATTRS_O2),
        //  changeDrawing(1, 1, ATTRS_R1),
        //  changeDrawing(1, 1, ATTRS_R2));
        SheetHelper.runTest(
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS_O1).toString(),
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS_O2).toString(),
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS_R1).toString(),
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS_R2).toString()
        );
    }

    @Test
    public void changeDrawingChangeDrawing08() throws JSONException {
        // Result: Should set equal operations to "removed" state.
        // testRunner.runTest(
        //  changeDrawing(1, 1, ATTRS),
        //  changeDrawing(1, 1, ATTRS),
        //  [],
        //  []);
        SheetHelper.runTest(
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS).toString(),
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS).toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeDrawingChangeDrawing09() throws JSONException {
        // Result: Should set equal operations to "removed" state.
        // testRunner.runBidiTest(changeDrawing(1, 1, {}), changeDrawing(1, 1, ATTRS), []);
        SheetHelper.runBidiTest(
            Helper.createChangeDrawingOp("1 1", Helper.createJSONObject("{}")).toString(),
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS).toString(),
            "[]",
            Helper.createChangeDrawingOp("1 1", SheetHelper.ATTRS).toString()
        );
    }
}
