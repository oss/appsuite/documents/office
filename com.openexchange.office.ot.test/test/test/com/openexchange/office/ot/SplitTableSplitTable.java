/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SplitTableSplitTable {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 8] }",                        // local operations
            "{ name: 'splitTable', start: [1, 10] }",                       // external operations
            "{ name: 'splitTable', start: [2, 2] }",                        // expected local
            "{ name: 'splitTable', start: [1, 8] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [1, 8] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]); // modified
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 8] }",                        // local operations
            "{ name: 'splitTable', start: [1, 6] }",                        // external operations
            "{ name: 'splitTable', start: [1, 6] }",                        // expected local
            "{ name: 'splitTable', start: [2, 2] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [1, 8] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 6] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 6] }",                        // local operations
            "{ name: 'splitTable', start: [1, 6] }",                        // external operations
            "{ name: 'splitTable', start: [1, 6] }",                        // expected local !!!! expectations are swapped ...
            "{ name: 'splitTable', start: [2, 0] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [1, 6] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 6] }] }]; // same position
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6]); // external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0]); // local operation is modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external splitTable operation not has the marker for local ignoring
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 8] }",                        // local operations
            "{ name: 'splitTable', start: [1, 10] }",                       // external operations
            "{ name: 'splitTable', start: [2, 10] }",                       // expected local
            "{ name: 'splitTable', start: [0, 8] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [0, 8] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 10]); // modified
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 2, 2, 4, 2] }",               // local operations
            "{ name: 'splitTable', start: [1, 10] }",                       // external operations
            "{ name: 'splitTable', start: [1, 10] }",                       // expected local
            "{ name: 'splitTable', start: [0, 2, 2, 4, 2] }");              // expected external
            /*
                oneOperation = { name: 'splitTable', start: [0, 2, 2, 4, 2] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2, 2, 4, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]); // not modified
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 2, 2, 4, 2] }",               // local operations
            "{ name: 'splitTable', start: [0, 2, 2, 4, 1] }",               // external operations
            "{ name: 'splitTable', start: [0, 2, 2, 4, 1] }",               // expected local
            "{ name: 'splitTable', start: [0, 2, 2, 5, 1] }");              // expected external
            /*
                oneOperation = { name: 'splitTable', start: [0, 2, 2, 4, 2] };
                localActions = [{ operations: [{ name: 'splitTable', start: [0, 2, 2, 4, 1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2, 2, 5, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2, 2, 4, 1]); // not modified
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 2, 1, 4, 2] }",               // local operations
            "{ name: 'splitTable', start: [0, 2, 2, 3, 1] }",               // external operations
            "{ name: 'splitTable', start: [0, 2, 2, 3, 1] }",               // expected local
            "{ name: 'splitTable', start: [0, 2, 1, 4, 2] }");              // expected external
            /*
                oneOperation = { name: 'splitTable', start: [0, 2, 1, 4, 2] };
                localActions = [{ operations: [{ name: 'splitTable', start: [0, 2, 2, 3, 1] }] }]; // neighbour cell
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2, 1, 4, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2, 2, 3, 1]); // not modified
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 8] }",                        // local operations
            "{ name: 'splitTable', start: [0, 10] }",                       // external operations
            "{ name: 'splitTable', start: [0, 10] }",                       // expected local
            "{ name: 'splitTable', start: [2, 8] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [1, 8] };
                localActions = [{ operations: [{ name: 'splitTable', start: [0, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 10]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 2] }",                        // local operations
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",                  // external operations
            "{ name: 'splitTable', start: [2, 5, 2, 2] }",                  // expected local
            "{ name: 'splitTable', start: [0, 2] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [0, 2] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 2, 2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 5] }",                        // local operations
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",                  // external operations
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",                  // expected local
            "{ name: 'splitTable', start: [2, 5] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 5] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 2, 2]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 6, 1, 1] }",                  // local operations
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",                  // external operations
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",                  // expected local
            "{ name: 'splitTable', start: [1, 6, 1, 1] }");                 // expected external
            /*
                oneOperation = { name: 'splitTable', start: [1, 6, 1, 1] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 2, 2]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",                        // local operations
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",                  // external operations
            "{ name: 'splitTable', start: [2, 5, 2, 2] }",                  // expected local
            "{ name: 'splitTable', start: [0, 7] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [0, 7] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 2, 2]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0] }",                        // local operations
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",                  // external operations
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",                  // expected local
            "{ name: 'splitTable', start: [2, 0] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 0] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 2, 2]);
             */
    }
}
