/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsInsertColumn {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8, 5, 2], end: [1, 8, 5, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 5, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 5, 5]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 8, 3, 2], end: [1, 8, 3, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 3, 2], end: [1, 8, 3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 4, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 4, 5]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 8, 2, 2], end: [1, 8, 2, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8, 2, 2], end: [1, 8, 2, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 2, 2], end: [1, 8, 2, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 2, 5]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'setAttributes', start: [1, 8, 2, 2], end: [1, 8, 2, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8, 3, 2], end: [1, 8, 3, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 2, 2], end: [1, 8, 2, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 3, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 3, 5]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'setAttributes', start: [1, 8, 1, 2], end: [1, 8, 1, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8, 1, 2], end: [1, 8, 1, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 1, 2], end: [1, 8, 1, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 1, 5]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 4, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 4, 5]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 4, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 4, 5]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 8, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1, 8, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 8, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 8, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 4, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 4, 5]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 8, 4, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1, 8, 4, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 8, 4, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8, 4, 2], end: [1, 8, 4, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 8, 4, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 4, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 4, 5]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 22]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 16]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 3, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 5]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 2, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 5]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 4]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 2, 2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 2, 2], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 1, 2, 6]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3], end: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [3, 1, 2, 3, 3, 5], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 1, 2, 3, 3, 6], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 3, 3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 3, 6]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [3, 1, 2, 4, 5], end: [3, 1, 2, 4, 9], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 1, 2, 4, 5], end: [3, 1, 2, 4, 9], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4, 5], end:  [3, 1, 2, 4, 9], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 2, 4, 9]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2]); // external operation is not modified
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1]); // external operation is not modified
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([3]); // external operation is not modified
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 1, 4, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [2, 1, 4, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 4, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 1, 4, 5]); // external operation is not modified
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 2, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 5]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1]); // external operation is not modified
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1, 0, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 0, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 0, 4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 5]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1]); // external operation is not modified
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true }, table: { tableGrid: [1, 2, 3] } } }",
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 0, 4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 5]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1]); // external operation is not modified
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true }, table: { width: 320, tableGrid: [1, 2, 3] } } }",
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true }, table: { width: 320 } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 0, 4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 5]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1]); // external operation is not modified
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true }, table: { tableGrid: [1, 2, 3] } } }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 0, 4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 5]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1]); // external operation is not modified
             */
    }
}

