/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class UpdateFieldUpdateField {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 4], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 4], type: 'PAGE' }");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 4], type: 'PAGE' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }], localActions);
                expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], type: 'PAGE' }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 2], type: 'PAGE', target: 'abc' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE', target: 'abc' }");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', target: 'abc' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }], localActions);
                expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', target: 'abc' }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', _REMOVED_OPERATION_: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 2], type: 'DATE' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "[]");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'DATE' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 2], type: 'PAGE', representation: 'abc' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "[]");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'abc' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 2], type: 'PAGE', representation: 'abc' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE', representation: 'def' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE', representation: 'def' }",
            "[]");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'abc' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'def' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'def' }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 2], type: 'PAGE', representation: 'abc' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE', representation: 'abc' }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'abc' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'abc' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE', representation: 'abc', _REMOVED_OPERATION_: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }
}
