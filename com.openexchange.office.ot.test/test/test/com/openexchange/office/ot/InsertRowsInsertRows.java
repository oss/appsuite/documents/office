/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertRowsInsertRows {

    // it('should calculate valid transformed insertRows operation after local insertRows operation', function () {
    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 2], count: 1 }",
            "{ name: 'insertRows', start: [3, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 3 }",
            "{ name: 'insertRows', start: [3, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 4], count: 1 }",
            "{ name: 'insertRows', start: [3, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 1], count: 1 }",
            "{ name: 'insertRows', start: [2, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [2, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 1]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 1 }",
            "{ name: 'insertRows', start: [2, 1], count: 3 }",
            "{ name: 'insertRows', start: [2, 1], count: 3 }",
            "{ name: 'insertRows', start: [3, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [2, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 4], count: 3 }",
            "{ name: 'insertRows', start: [3, 5], count: 3 }",
            "{ name: 'insertRows', start: [3, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 4], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 6], count: 1 }",
            "{ name: 'insertRows', start: [3, 4], count: 3 }",
            "{ name: 'insertRows', start: [3, 4], count: 3 }",
            "{ name: 'insertRows', start: [3, 9], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 6], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 4], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 9]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 6, 0, 1, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 4], count: 3 }",
            "{ name: 'insertRows', start: [3, 4], count: 3 }",
            "{ name: 'insertRows', start: [3, 9, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 6, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 4], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 9, 0, 1, 1]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 4], count: 3 }",
            "{ name: 'insertRows', start: [3, 4], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 4], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 3, 0, 0, 1], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 0, 0, 1], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 0, 0, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 0, 0, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 4], count: 1 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 0, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 3, 0, 1, 4], count: 1 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 7], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 4], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 0, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 7]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 2], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 3], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 0, 1, 2], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 0, 1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertRows', start: [3, 3, 1, 1, 2], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 1, 1, 2], count: 3 }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [3, 3, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 1, 1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 1], count: 1 }",
            "{ name: 'insertRows', start: [0, 5, 1, 1], count: 3 }",
            "{ name: 'insertRows', start: [0, 5, 1, 1], count: 3 }",
            "{ name: 'insertRows', start: [1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [0, 5, 1, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 5, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [0, 1], count: 1 }",
            "{ name: 'insertRows', start: [1, 5, 1, 1], count: 3 }",
            "{ name: 'insertRows', start: [1, 5, 1, 1], count: 3 }",
            "{ name: 'insertRows', start: [0, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertRows', start: [0, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertRows', start: [1, 5, 1, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 1]);
             */
    }

    // insertRows and local insertRows operation (handleInsertRowsInsertRows)
    @Test
    public void test01a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 1 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 1 }",
            "{ name: 'insertRows', start: [3, 2], referenceRow: 0, count: 1 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 1 }");
           /*
            oneOperation = { name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 2], count: 1, referenceRow: 0, opl: 1, osn: 1 }] }], localActions); // "referenceRow" does not change
            expectOp([{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 1 }",
            "{ name: 'insertRows', start: [3, 2], referenceRow: 1, count: 1 }",
            "{ name: 'insertRows', start: [3, 3], referenceRow: 2, count: 1 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 1 }");
           /*
            oneOperation = { name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 2], count: 1, referenceRow: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 3], count: 1, referenceRow: 2, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 3 }",
            "{ name: 'insertRows', start: [3, 2], referenceRow: 1, count: 2 }",
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 2 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 3 }",
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 2 }",
            "{ name: 'insertRows', start: [3, 8], referenceRow: 7, count: 2 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 8], count: 2, referenceRow: 7, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 1, 0, 2], referenceRow: 1, count: 3 }",
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 2 }",
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 2 }",
            "{ name: 'insertRows', start: [3, 1, 1, 0, 2], referenceRow: 1, count: 3 }");
           /*
            oneOperation = { name: 'insertRows', start: [3, 1, 1, 0, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 1, 1, 0, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 2], referenceRow: 1, count: 1 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 1 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 1 }",
            "{ name: 'insertRows', start: [3, 3], referenceRow: 2, count: 1 }");
           /*
            oneOperation = { name: 'insertRows', start: [3, 2], count: 1, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 3], count: 1, referenceRow: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 2], referenceRow: 1, count: 2 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 3 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 3 }",
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 2 }");
           /*
            oneOperation = { name: 'insertRows', start: [3, 2], count: 2, referenceRow: 1, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 2 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 3 }",
            "{ name: 'insertRows', start: [3, 1], referenceRow: 0, count: 3 }",
            "{ name: 'insertRows', start: [3, 8], referenceRow: 7, count: 2 }");
           /*
            oneOperation = { name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 8], count: 2, referenceRow: 7, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 2 }",
            "{ name: 'insertRows', start: [3, 1, 1, 0, 2], referenceRow: 1, count: 3 }",
            "{ name: 'insertRows', start: [3, 1, 1, 0, 2], referenceRow: 1, count: 3 }",
            "{ name: 'insertRows', start: [3, 5], referenceRow: 4, count: 2 }");
           /*
            oneOperation = { name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 };
            localActions = [{ operations: [{ name: 'insertRows', start: [3, 1, 1, 0, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }] }];
            transformedOps = otManager.transformOperation(oneOperation, localActions);
            expectAction([{ operations: [{ name: 'insertRows', start: [3, 1, 1, 0, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }] }], localActions);
            expectOp([{ name: 'insertRows', start: [3, 5], count: 2, referenceRow: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    // it('should calculate valid transformed insertCells operation after local insertCells operation', function () {
    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 1, 2], count: 1 }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 1, 4], count: 1 }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 1], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 1], count: 3 }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 1], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 1], count: 3 }",
            "{ name: 'insertCells', start: [2, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [2, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 1, 1]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 2], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 0], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 0], count: 3 }",
            "{ name: 'insertCells', start: [2, 1, 2], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [2, 1, 2], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 0], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 1, 2]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2], count: 1 }",
            "{ name: 'insertCells', start: [2, 1, 2], count: 3 }",
            "{ name: 'insertCells', start: [2, 1, 2], count: 3 }",
            "{ name: 'insertCells', start: [3, 1, 2], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 1, 2], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [2, 1, 2], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 4], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 5], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 0, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 4], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 1]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 6], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 4], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 4], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 9], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 0, 6], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 4], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 9]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 6, 0, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 4], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 4], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 9, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 0, 6, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 4], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 9, 0, 1, 1]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 4], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 4], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 4], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 0, 1], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 0, 1], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 0, 0, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 3, 0, 0, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 4], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 3 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 3, 0, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 4], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 7], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 4], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 3, 0, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 3, 0, 1, 7]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 2], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 3], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 0, 1, 2], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 3, 0, 1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1 }",
            "{ name: 'insertCells', start: [3, 0, 3, 1, 1, 2], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 1, 1, 2], count: 3 }",
            "{ name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [3, 0, 3, 0, 1, 1], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [3, 0, 3, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 3, 1, 1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 1, 2], count: 1 }",
            "{ name: 'insertCells', start: [0, 5, 1, 1, 2], count: 3 }",
            "{ name: 'insertCells', start: [0, 5, 1, 1, 2], count: 3 }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [1, 1, 2], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [0, 5, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 5, 1, 1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [0, 1, 2], count: 1 }",
            "{ name: 'insertCells', start: [1, 5, 1, 1, 2], count: 3}",
            "{ name: 'insertCells', start: [1, 5, 1, 1, 2], count: 3}",
            "{ name: 'insertCells', start: [0, 1, 2], count: 1 }");
           /*
                oneOperation = { name: 'insertCells', start: [0, 1, 2], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertCells', start: [1, 5, 1, 1, 2], count: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 1, 1, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 1, 2]);
             */
    }
}
