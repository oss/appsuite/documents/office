/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsInsertPara {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [2, 8], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 8]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [2] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 22]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [2] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 16]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [2, 12], end: [2, 16],  attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 16]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([3]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2, 2] }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 1, 2, 2] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 2, 1, 2, 2], end: [3, 2, 1, 2, 6], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 2, 6]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 1, 2 ,0], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, before split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 1, 2, 5], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }]; // changing paragraph in same cell, but behind split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }");
            /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
                localActions = [{ operations: [{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1], end: [5], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [2] }");
            /*
                // local change inside one paragraph and several following paragraphs completely and an external insertTable in the middle of the paragraphs
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2]); // external operation is not modified
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [2], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                // local change of several paragraphs completely and an external insertTable before the first of the paragraphs
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1]); // external operation is not modified
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3] }");
            /*
                // local change of several paragraphs completely and an external insertTable in the last of the paragraphs
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([3]); // external operation is not modified
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2, 1, 4, 5] }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [2, 1, 4, 5] }");
            /*
                // local change of several paragraphs and an external insertTable inside the table
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 4, 5] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]); // the end position of the locally saved operation is also NOT modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([2, 1, 4, 5]); // external operation is not modified
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [1], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }");
            /*
                // local change of one complete paragraph and an external insertTable before this paragraph
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'setAttributes', opl: 1, osn: 1, start: [1], attrs: { character: { italic: true } } }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1]); // external operation is not modified
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [2, 8], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [1, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [1, 22], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 22]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [2, 12], end: [2, 16], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 12]);
                expect(oneOperation.end).to.deep.equal([2, 16]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [1, 12], end: [1, 16], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 12]);
                expect(oneOperation.end).to.deep.equal([1, 16]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [0], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [0], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [3], attrs: { character: { italic: true } } }");
             /*
                oneOperation = { name: 'setAttributes', start: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [0], end: [1], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.end).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [0], end: [3], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.end).to.deep.equal([3]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1, 1, 2, 2] }",
            "{ name: 'insertTable', start: [1, 1, 2, 2] }",
            "{ name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [0], end: [2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1, 1, 2, 2] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.end).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 2, 2]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [3, 2, 1, 2, 2], end: [3, 2, 1, 2, 6], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [2, 2, 1, 2, 2], end: [2, 2, 1, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 2, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([3, 2, 1, 2, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 3]);
                expect(oneOperation.end).to.deep.equal([0, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [3, 1, 2, 0], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] }] }]; // changing paragraph in same cell, before split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 0]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'setAttributes', start: [3, 1, 2, 5], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [3, 1, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] }] }]; // changing paragraph in same cell, but behind split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'setAttributes', start: [4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [3], end: [4], attrs: { character: { italic: true } } }");
            /*
                // external change inside one paragraph and a following paragraph completely and an intenal insertTable in front of the paragraph range
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [3], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1); // no additional external operation
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'setAttributes', start: [1], end: [5], attrs: { character: { italic: true } } }");;
            /*
                // external change inside one paragraph and several following paragraphs completely and an internal insertTable in the middle of these paragraphs
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [4], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.end).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1); // no additional external operation
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'setAttributes', start: [2], end: [4], attrs: { character: { italic: true } } }");
            /*
                // external change of several paragraphs completely and an internal insertTable in the first paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1); // no additional external operation
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }");
             /*
                // external change of several paragraphs completely and an internal insertTable in the last of the paragraphs
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1], end: [3], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1); // no additional external operation
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'insertTable', start: [2, 1, 4, 5] }",
            "{ name: 'insertTable', start: [2, 1, 4, 5] }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } }");
             /*
                // external change of several paragraphs and an internal insertTable inside the table
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 8], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 4, 5] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 4, 5]); // the start position of the locally saved operation is not modified
                expect(oneOperation.start).to.deep.equal([3, 4]);
                expect(oneOperation.end).to.deep.equal([3, 8]);
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(transformedOps.length).to.equal(1); // no additional external operation
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [1, 2, 3, 1] }",
            "{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } } }",
            "{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } } }",
            "{ name: 'insertParagraph', start: [1, 2, 3, 1] }");
             /*
                oneOperation = { name: 'insertParagraph', start: [1, 2, 3, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [1, 2, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [1, 2, 3], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertParagraph', start: [1, 2, 3, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } } }",
            "{ name: 'insertParagraph', start: [1, 2, 3, 1] }",
            "{ name: 'insertParagraph', start: [1, 2, 3, 1] }",
            "{ name: 'setAttributes', start: [1, 2, 3], attrs: { character: { italic: true } } }");
             /*
                oneOperation = { name: 'setAttributes', start: [1, 2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [1, 2, 3, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertParagraph', start: [1, 2, 3, 1], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [1, 2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

}

