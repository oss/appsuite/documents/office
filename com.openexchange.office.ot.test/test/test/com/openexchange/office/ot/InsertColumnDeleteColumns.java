/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertColumnDeleteColumns {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 4 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(3);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'before' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 4 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(3);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 4 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(3);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 4 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(3);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'before' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 4 }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(3);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 4 }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(3);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 4, insertMode: 'before' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 4, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 4, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 4, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 3, endGrid: 4 }",
            "{ name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(3);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 1]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 4 }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 1]);
                expect(oneOperation.gridPosition).to.equal(3);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3, 1, 2, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 1, 2, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 2, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 1]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3, 2, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 2, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 2, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 1]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 1]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3, 5, 2, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 5, 2, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 5, 2, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 2, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 3, endGrid: 4 }",
            "{ name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 5, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 2, 2]);
                expect(localActions[0].operations[0].startGrid).to.equal(3);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 4 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(4);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 2, 6, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "{ name: 'insertColumn', start: [3, 2, 4, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 2, 6, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(2);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 4, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 2, 0, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "{ name: 'insertColumn', start: [3, 2, 0, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 2, 0, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(2);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 0, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 2, 1, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 2, 1, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(2);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 1, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 2, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 2, 2, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(2);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 2, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 2, 3, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2 }",
            "{ name: 'insertColumn', start: [3, 2, 1, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 2, 3, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(2);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 1, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 2, 4, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3, 2, 1, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 2, 4, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 1, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 2, 5, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3, 2, 4, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 2, 5, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(3);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 4, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 2, 5, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3, 2, 5, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 2, 5, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 5, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 2, 5, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [4], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3, 2, 5, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3, 2, 5, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [4], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 5, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 0, 4, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 4, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'before' }",
            "{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 0, 4, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 4, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(3);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 3, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(3);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 4, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 4, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 4, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 0, 3, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 3, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(4);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4, 2, 4, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [4, 2, 4, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [4, 2, 4, 2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 2, 4, 2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1]);
                expect(localActions[0].operations[0].startGrid).to.equal(2);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 5, 4, 0, 0, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2, 5, 1, 0, 0, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 5, 4, 0, 0, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 0, 0, 1, 1]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 5, 3, 0, 0, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 5, 3, 0, 0, 1, 1], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 3, 0, 0, 1, 1]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 5, 1, 0], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 5, 1, 0], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 0]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 5, 2, 0], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 5, 2, 0], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 2, 0]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 5, 3, 0], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 5, 3, 0], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 3, 0]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 5, 4, 0], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2, 5, 1, 0], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2, 5, 4, 0], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 0]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 0]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }",
            "{ name: 'deleteColumns', start: [2, 0, 2, 2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 0, 3, 2], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 1, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 0, 2, 2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 3, 2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'deleteColumns', start: [2, 0, 2, 2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 0, 3, 2], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 0, 2, 2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 3, 2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'before' }",
            "{ name: 'deleteColumns', start: [2, 0, 2, 2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 0, 2, 2], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 3, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 0, 2, 2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 2]);
                expect(localActions[0].operations[0].startGrid).to.equal(1);
                expect(localActions[0].operations[0].endGrid).to.equal(3);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.gridPosition).to.equal(3);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }
}
