/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertParaDeleteColumns {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertTable', start: [3] }");
           /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertTable', start: [2] }");
           /*
                oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertTable', start: [4] }");
           /*
                oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 3, 0, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertTable', start: [3, 3, 0, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [3, 3, 0, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 0, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 3, 1, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', start: [3, 3, 1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 3, 2, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', start: [3, 3, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 2, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 3, 3, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', start: [3, 3, 3, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 3, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 3, 4, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertTable', start: [3, 3, 1, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [3, 3, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 0, 1, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertTable', start: [3, 0, 1, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [3, 0, 1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 5, 2, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', start: [3, 5, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4, 2, 4, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertTable', start: [4, 2, 4, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [4, 2, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 2, 4, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4, 2, 4, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 5 }",
            "{ name: 'insertTable', start: [4, 2, 4, 2] }");
           /*
                oneOperation = { name: 'insertTable', start: [4, 2, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 2, 4, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertTable', start: [2] }");
           /*
                oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertTable', start: [3] }");
           /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertTable', start: [4] }");
           /*
                oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2, 5, 2, 0, 0, 1, 1] }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }",
            "{ name: 'insertTable', start: [2, 5, 1, 0, 0, 1, 1] }");
           /*
                oneOperation = { name: 'insertTable', start: [2, 5, 2, 0, 0, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 0, 0, 1, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2, 5, 2, 0, 0, 1, 1] }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2 }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', start: [2, 5, 2, 0, 0, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 2, 0, 0, 1, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertTable', start: [2] }");
           /*
                oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'deleteColumns', start: [2, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertTable', start: [2] }");
           /*
                oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [3] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [2] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [4] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [4] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3, 3, 0, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [3, 3, 0, 2] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [3, 3, 0, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 0, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3, 3, 1, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertParagraph', start: [3, 3, 1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3, 3, 2, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertParagraph', start: [3, 3, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 2, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3, 3, 3, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertParagraph', start: [3, 3, 3, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 3, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3, 3, 4, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [3, 3, 1, 2] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [3, 3, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3, 0, 1, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [3, 0, 1, 2] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [3, 0, 1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3, 5, 2, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "[]");
           /*
                oneOperation = { name: 'insertParagraph', start: [3, 5, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [4, 2, 4, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [4, 2, 4, 2] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [4, 2, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 2, 4, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [4, 2, 4, 2] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 5 }",
            "{ name: 'insertParagraph', start: [4, 2, 4, 2] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [4, 2, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 2, 4, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [2] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [3] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [4] }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [4] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 1, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [2, 5, 2, 0, 0, 1, 1] }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }",
            "{ name: 'insertParagraph', start: [2, 5, 1, 0, 0, 1, 1] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [2, 5, 2, 0, 0, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 0, 0, 1, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [2, 5, 2, 0, 0, 1, 1] }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2 }",
            "[]");
           /*
                oneOperation = { name: 'insertParagraph', start: [2, 5, 2, 0, 0, 1, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 5, 2, 0, 0, 1, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [2] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'deleteColumns', start: [2, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'insertParagraph', start: [2] }");
           /*
                oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }
}
