/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MergeParaInsertPara {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [3] }",
            "{ name: 'mergeParagraph', start: [4] }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [4] }",
            "{ name: 'mergeParagraph', start: [5] }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'mergeParagraph', start: [2] }",
            "{ name: 'mergeParagraph', start: [2] }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "[]",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }]");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // the local mergeParagraph operation has the marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
                expect(oneOperation.start).to.deep.equal([3]);
                expect(transformedOps.length).to.equal(2); // an additional external operation
                expect(transformedOps[0].name).to.equal('splitParagraph'); // inserted before insertTable operation
                expect(transformedOps[0].start).to.deep.equal([2, 5]);
                expect(transformedOps[1]).to.deep.equal(oneOperation);
             */
    }

    @Test
    public void test04A() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "[{ name: 'mergeParagraph', start: [2], paralength: 5 }, { name: 'insertText', start: [2, 8], text: 'ooo' }]",
            "[{ name: 'insertText', start: [4, 3], text: 'ooo' }]",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }]");
    }

    @Test
    public void test04B() {
        TransformerTest.transform(
            "[{ name: 'mergeParagraph', start: [2], paralength: 5 }, { name: 'insertText', start: [2, 8], text: 'ooo' }]",
            "{ name: 'insertTable', start: [3] }",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }]",
            "[{ name: 'insertText', start: [4, 3], text: 'ooo' }]");
    }

    @Test
    public void test04C() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "{ name: 'insertTable', start: [3] }",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }]",
            "[]");
    }

    @Test
    public void test04D() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "[{ name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], text: 'ooo' }]",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], text: 'ooo' }]",
            "[]");
    }

    @Test
    public void test04E() {
        TransformerTest.transform(
            "[{ name: 'mergeParagraph', start: [2], paralength: 5 }, { name: 'insertParagraph', start: [0] }]",
            "[{ name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], text: 'ooo' }]",
            "[{ name: 'splitParagraph', start: [3, 5] }, { name: 'insertTable', start: [4] }, { name: 'insertText', start: [5, 0], text: 'ooo' }]",
            "{ name: 'insertParagraph', start: [0] }");
    }

    @Test
    public void test04F() {
        TransformerTest.transform(
            "[{ name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], text: 'ooo' }]",
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "[]",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], text: 'ooo' }]");
    }

    @Test
    public void test04G() {
        TransformerTest.transform(
            "[{ name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], text: 'ooo' }]",
            "[{ name: 'mergeParagraph', start: [2], paralength: 5 }, { name: 'insertParagraph', start: [0] }]",
            "{ name: 'insertParagraph', start: [0] }",
            "[{ name: 'splitParagraph', start: [3, 5] }, { name: 'insertTable', start: [4] }, { name: 'insertText', start: [5, 0], text: 'ooo' }]");
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [2, 3, 3, 1], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 3, 3, 1], paralength: 5 }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 3, 3, 1], paralength: 5, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 3, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
                expect(oneOperation.start).to.deep.equal([3]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [4, 1, 2, 2] }",
            "{ name: 'insertTable', start: [2] }");
            /*
                oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2] }",
            "{ name: 'insertTable', start: [4] }");
            /*
                oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [4, 1, 2, 2] }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [3] }",
            "{ name: 'mergeParagraph', start: [3] }",
            "{ name: 'insertTable', start: [4, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [6] }",
            "{ name: 'mergeParagraph', start: [6] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([6]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [5, 1, 2, 3] }",
            "{ name: 'mergeParagraph', start: [5, 1, 2, 4] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [5, 1, 2, 0] }",
            "{ name: 'mergeParagraph', start: [5, 1, 2, 0] }",
            "{ name: 'insertTable', start: [5, 1, 2, 1] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 2, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 1]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [5, 1, 1, 0] }",
            "{ name: 'mergeParagraph', start: [5, 1, 1, 0] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 1, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 1, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [5, 1, 1, 4] }",
            "{ name: 'mergeParagraph', start: [5, 1, 1, 4] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 1, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeParagraph', start: [5, 1, 2, 3] }",
            "{ name: 'mergeParagraph', start: [5, 1, 2, 4] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeParagraph', start: [0], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 5 }",
            "{ name: 'insertTable', start: [0, 6, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 6, 2]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeParagraph', start: [1] }",
            "{ name: 'mergeParagraph', start: [1] }",
            "{ name: 'insertTable', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "{ name: 'insertTable', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeParagraph', start: [1, 0, 1], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 0, 1], paralength: 5 }",
            "{ name: 'insertTable', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 0, 1], opl: 1, osn: 1, paralength: 5 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeParagraph', start: [1, 1, 0], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 1, 0], paralength: 5 }",
            "{ name: 'insertTable', start: [1, 1, 1] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 1, 0], opl: 1, osn: 1, paralength: 5 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 1]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeParagraph', start: [1, 1, 4], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 1, 5], paralength: 5 }",
            "{ name: 'insertTable', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 1, 4], opl: 1, osn: 1, paralength: 5 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [4] }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [4] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [5] }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2] }",
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [2] }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2] }",
            "{ name: 'insertTable', start: [4, 2, 2, 0] }",
            "{ name: 'insertTable', start: [3, 2, 2, 0] }",
            "{ name: 'mergeParagraph', start: [2] }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [4, 2, 2, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "{ name: 'insertTable', start: [3] }",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('splitParagraph');
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[1].name).to.equal('insertTable');
                expect(localActions[0].operations[1].start).to.deep.equal([3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2, 3, 3, 1], paralength: 5 }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [2, 3, 3, 1], paralength: 5 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2, 3, 3, 1], paralength: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation.start).to.deep.equal([2, 3, 3, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test27A() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "{ name: 'insertTable', start: [3] }",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [2, 5], opl: 1, osn: 1 }, { name: 'insertTable', start: [3], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test27B() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "[{ name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], test: 'ooo' }]",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], test: 'ooo' }]",
            "[]");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }, { name: 'insertText', start: [4, 0], test: 'ooo', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [2, 5], opl: 1, osn: 1 }, { name: 'insertTable', start: [3], opl: 1, osn: 1 }, { name: 'insertText', start: [4, 0], test: 'ooo', opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test27C() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "[]",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }]");
    }

    @Test
    public void test27D() {
        TransformerTest.transform(
            "[{ name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], test: 'ooo' }]",
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "[]",
            "[{ name: 'splitParagraph', start: [2, 5] }, { name: 'insertTable', start: [3] }, { name: 'insertText', start: [4, 0], test: 'ooo' }]");
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeTable', start: [3] }",
            "{ name: 'mergeTable', start: [4] }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeTable', start: [4] }",
            "{ name: 'mergeTable', start: [5] }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'mergeTable', start: [2] }",
            "{ name: 'mergeTable', start: [2] }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "[]",
            "[{ name: 'splitTable', start: [2, 5] }, { name: 'insertTable', start: [3] }]");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // the local mergeTable operation has the marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
                expect(oneOperation.start).to.deep.equal([3]);
                expect(transformedOps.length).to.equal(2); // an additional external operation
                expect(transformedOps[0].name).to.equal('splitTable'); // inserted before insertTable operation
                expect(transformedOps[0].start).to.deep.equal([2, 5]);
                expect(transformedOps[1]).to.deep.equal(oneOperation);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5 }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 3, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
                expect(oneOperation.start).to.deep.equal([3]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [4, 1, 2, 2] }",
            "{ name: 'insertTable', start: [2] }");
            /*
                oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2] }",
            "{ name: 'insertTable', start: [4] }");
            /*
                oneOperation = { name: 'insertTable', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [4, 1, 2, 2] }",
            "{ name: 'insertTable', start: [3] }");
            /*
                oneOperation = { name: 'insertTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [3] }",
            "{ name: 'mergeTable', start: [3] }",
            "{ name: 'insertTable', start: [4, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [6] }",
            "{ name: 'mergeTable', start: [6] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([6]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 3] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 4] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 0] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 0] }",
            "{ name: 'insertTable', start: [5, 1, 2, 1] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 1]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 1, 0] }",
            "{ name: 'mergeTable', start: [5, 1, 1, 0] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 1, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 1, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 1, 4] }",
            "{ name: 'mergeTable', start: [5, 1, 1, 4] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 1, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 3] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 4] }",
            "{ name: 'insertTable', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [0], rowcount: 5 }",
            "{ name: 'mergeTable', start: [0], rowcount: 5 }",
            "{ name: 'insertTable', start: [0, 6, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 6, 2]);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",
            "{ name: 'insertTable', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "{ name: 'insertTable', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [1, 0, 1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 0, 1], rowcount: 5 }",
            "{ name: 'insertTable', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 0, 1], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [1, 1, 0], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 1, 0], rowcount: 5 }",
            "{ name: 'insertTable', start: [1, 1, 1] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 1, 0], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 1]);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [1, 1, 4], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 1, 5], rowcount: 5 }",
            "{ name: 'insertTable', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertTable', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 1, 4], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeTable', start: [4] }");
            /*
                oneOperation = { name: 'mergeTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [4] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeTable', start: [5] }");
            /*
                oneOperation = { name: 'mergeTable', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5]);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2] }",
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeTable', start: [2] }");
            /*
                oneOperation = { name: 'mergeTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2] }",
            "{ name: 'insertTable', start: [4, 2, 2, 0] }",
            "{ name: 'insertTable', start: [3, 2, 2, 0] }",
            "{ name: 'mergeTable', start: [2] }");
            /*
                oneOperation = { name: 'mergeTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [4, 2, 2, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "{ name: 'insertTable', start: [3] }",
            "[{ name: 'splitTable', start: [2, 5] }, { name: 'insertTable', start: [3] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('splitTable');
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[1].name).to.equal('insertTable');
                expect(localActions[0].operations[1].start).to.deep.equal([3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5 }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5 }");
            /*
                oneOperation = { name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation.start).to.deep.equal([2, 3, 3, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'mergeTable', start: [3] }",
            "{ name: 'mergeTable', start: [4] }",
            "{ name: 'insertParagraph', start: [3] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'mergeTable', start: [4] }",
            "{ name: 'mergeTable', start: [5] }",
            "{ name: 'insertParagraph', start: [3] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [4] }",
            "{ name: 'mergeTable', start: [2] }",
            "{ name: 'mergeTable', start: [2] }",
            "{ name: 'insertParagraph', start: [3] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "[]",
            "[{ name: 'splitTable', start: [2, 5] }, { name: 'insertParagraph', start: [3] }]");
            /*
                oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // the local mergeTable operation has the marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
                expect(oneOperation.start).to.deep.equal([3]);
                expect(transformedOps.length).to.equal(2); // an additional external operation
                expect(transformedOps[0].name).to.equal('splitTable'); // inserted before insertTable operation
                expect(transformedOps[0].start).to.deep.equal([2, 5]);
                expect(transformedOps[1]).to.deep.equal(oneOperation);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5 }",
            "{ name: 'insertParagraph', start: [3] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 3, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
                expect(oneOperation.start).to.deep.equal([3]);
                expect(transformedOps.length).to.equal(1);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [4, 1, 2, 2] }",
            "{ name: 'insertParagraph', start: [2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [4] }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2] }",
            "{ name: 'insertParagraph', start: [4] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [4, 1, 2, 2] }",
            "{ name: 'insertParagraph', start: [3] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [3] }",
            "{ name: 'mergeTable', start: [3] }",
            "{ name: 'insertParagraph', start: [4, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2, 2]);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [6] }",
            "{ name: 'mergeTable', start: [6] }",
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([6]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 3] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 4] }",
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 0] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 0] }",
            "{ name: 'insertParagraph', start: [5, 1, 2, 1] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 1]);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 1, 0] }",
            "{ name: 'mergeTable', start: [5, 1, 1, 0] }",
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 1, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 1, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test68() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 1, 4] }",
            "{ name: 'mergeTable', start: [5, 1, 1, 4] }",
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 1, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test69() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 3] }",
            "{ name: 'mergeTable', start: [5, 1, 2, 4] }",
            "{ name: 'insertParagraph', start: [5, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [5, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5, 1, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5, 1, 2, 2]);
             */
    }

    @Test
    public void test70() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [1, 1, 2, 1, 1, 2] }",
            "{ name: 'mergeTable', start: [0], rowcount: 5 }",
            "{ name: 'mergeTable', start: [0], rowcount: 5 }",
            "{ name: 'insertParagraph', start: [0, 6, 2, 1, 1, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [1, 1, 2, 1, 1, 2], opl: 1, osn: 1 }; // in a text frame in a cell
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 6, 2, 1, 1, 2]);
             */
    }

    @Test
    public void test71() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [1, 1, 2, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",
            "{ name: 'insertParagraph', start: [1, 1, 2, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [1, 1, 2, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2]);
             */
    }

    @Test
    public void test72() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "{ name: 'insertParagraph', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test73() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [1, 0, 1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 0, 1], rowcount: 5 }",
            "{ name: 'insertParagraph', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 0, 1], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test74() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [1, 1, 0], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 1, 0], rowcount: 5 }",
            "{ name: 'insertParagraph', start: [1, 1, 1] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 1, 0], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 1]);
             */
    }

    @Test
    public void test75() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [1, 1, 2] }",
            "{ name: 'mergeTable', start: [1, 1, 4], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 1, 5], rowcount: 5 }",
            "{ name: 'insertParagraph', start: [1, 1, 2] }");
            /*
                oneOperation = { name: 'insertParagraph', start: [1, 1, 2], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 1, 4], opl: 1, osn: 1, rowcount: 5 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2]);
             */
    }

    @Test
    public void test76() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'mergeTable', start: [4] }");
            /*
                oneOperation = { name: 'mergeTable', start: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test77() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [4] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'mergeTable', start: [5] }");
            /*
                oneOperation = { name: 'mergeTable', start: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([5]);
             */
    }

    @Test
    public void test78() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2] }",
            "{ name: 'insertParagraph', start: [4] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'mergeTable', start: [2] }");
            /*
                oneOperation = { name: 'mergeTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test79() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2] }",
            "{ name: 'insertParagraph', start: [4, 2, 2, 0] }",
            "{ name: 'insertParagraph', start: [3, 2, 2, 0] }",
            "{ name: 'mergeTable', start: [2] }");
            /*
                oneOperation = { name: 'mergeTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [4, 2, 2, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test80() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "{ name: 'insertParagraph', start: [3] }",
            "[{ name: 'splitTable', start: [2, 5] }, { name: 'insertParagraph', start: [3] }]",
            "[]");
            /*
                oneOperation = { name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('splitTable');
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[1].name).to.equal('insertParagraph');
                expect(localActions[0].operations[1].start).to.deep.equal([3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
                expect(transformedOps.length).to.equal(0);
             */
    }

    @Test
    public void test81() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5 }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5 }");
            /*
                oneOperation = { name: 'mergeTable', start: [2, 3, 3, 1], rowcount: 5, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation.start).to.deep.equal([2, 3, 3, 1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps.length).to.equal(1);
             */
    }
}
