/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ChangeChangeAutoStyle {

    /**
     * The client tests are using T1 as a default
     * name for a type, but the backend is using cell.
     * In order to let client tests run in the backend,
     * auto style type tests without a style have to
     * use this default.
     * e.g. see insertAutoStyleInsertAutoStyle06()
     */
    private final static String DEFAULT_AUTO_STYLE_TYPE = "t1";
    private final String        otis                    = Helper.createJSONObject("{ otIndexShift: true }").toString();
    private final JSONObject    attrs                   = Helper.createJSONObject("{ f1: { a1: 10 } }");
    // overlapping attribute sets (will be reduced)
    private final JSONObject    attrs01                 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a1: 10, a2: 20, a3: 30 } }");
    private final JSONObject    attrs02                 = Helper.createJSONObject("{ f3: { a1: 10 }, f2: { a1: 10, a2: 22, a4: 40 } }");
    // the reduced result of ATTRS_Ox
    private final JSONObject    attrsR1                 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a2: 20, a3: 30 } }");
    private final JSONObject    attrsR2                 = Helper.createJSONObject("{ f3: { a1: 10 }, f2: { a4: 40 } }");
    // independent attribute sets (will not be reduced) ATTRS_I
    private final JSONObject    attrsI1                 = Helper.createJSONObject("{ f1: { a1: 10 }, f2: { a1: 10 } }");
    private final JSONObject    attrsI2                 = Helper.createJSONObject("{ f2: { a2: 10 }, f3: { a1: 10 } }");

    /*
     * describe('"changeAutoStyle" and independent operations', function () {
            it('should skip the transformations', function () {
                testRunner.runBidiTest(changeAutoStyle('a1', ATTRS), CHART_OPS);
            });
        });
    */

    @Test
    public void changeChangeAutoStyleAndIndependentOperations01() throws JSONException {
        final String chartOps = "[]";
        final JSONObject changeAutoStyle = Helper.createChangeAutoStyleOp("a1", attrs);
        TransformerTest.transformBidi(changeAutoStyle.toString(), chartOps);
    }

    /*
     * describe('"changeAutoStyle" and "changeAutoStyle"', function () {
            it('should not process different styles', function () {
                testRunner.runTest(
                    opSeries1(changeAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
                    opSeries1(changeAutoStyle, ['a2', ATTRS], ['t1', 'a2', ATTRS], ['t2', 'a1', ATTRS], ['t2', 'a2', ATTRS])
                );
            });
            it('should not process operations for independent attributes', function () {
                testRunner.runTest(changeAutoStyle('t1', 'a1', ATTRS_I1), changeAutoStyle('t1', 'a1', ATTRS_I2));
            });
            it('should reduce attribute sets', function () {
                testRunner.runTest(
                    changeAutoStyle('t1', 'a1', ATTRS_O1), changeAutoStyle('t1', 'a1', ATTRS_O2),
                    changeAutoStyle('t1', 'a1', ATTRS_R1), changeAutoStyle('t1', 'a1', ATTRS_R2)
                );
            });
            it('should set equal operations to "removed" state', function () {
                testRunner.runTest(
                    opSeries1(changeAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
                    opSeries1(changeAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
                    [], []
                );
                testRunner.runBidiTest(changeAutoStyle('a1', {}), changeAutoStyle('a1', ATTRS), [], null);
            });
        });
     */

    @Test
    public void changeAutoStyleChangeAutoStyle01() throws JSONException {
        // Result: Should reduce attribute sets.
        // testRunner.runTest(
        //  changeAutoStyle('t1', 'a1', ATTRS_I1),
        //  changeAutoStyle('t1', 'a1', ATTRS_I2));
        TransformerTest.transform(
            Helper.createChangeAutoStyleOp("t1", "a1", attrsI1, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a1", attrsI2, null).toString()
        );
    }

    @Test
    public void changeAutoStyleChangeAutoStyle02() throws JSONException {
        // Result: Should reduce attribute sets.
        // testRunner.runTest(
        //  changeAutoStyle('t1', 'a1', ATTRS_O1),
        //  changeAutoStyle('t1', 'a1', ATTRS_O2),
        //  changeAutoStyle('t1', 'a1', ATTRS_R1),
        //  changeAutoStyle('t1', 'a1', ATTRS_R2)
        TransformerTest.transform(
            Helper.createChangeAutoStyleOp("t1", "a1", attrs01, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a1", attrs02, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a1", attrsR2, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a1", attrsR1, null).toString()
        );
    }

    @Test
    public void changeAutoStyleChangeAutoStyle03() throws JSONException {
        // Result: Should reduce attribute sets.
        // testRunner.runTest(
        //  opSeries1(changeAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
        //  opSeries1(changeAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
        //  [],
        //  []);
        TransformerTest.transform(
            Helper.createArrayFromJSON(Helper.createChangeAutoStyleOp("a1", attrs),
                Helper.createChangeAutoStyleOp("t1", "a1", attrs, null)),
            Helper.createArrayFromJSON(Helper.createChangeAutoStyleOp("a1", attrs),
                Helper.createChangeAutoStyleOp("t1", "a1", attrs, null)),
            "[]",
            "[]"
        );
    }

    @Test
    public void changeAutoStyleChangeAutoStyle04() throws JSONException {
        // Result: Should reduce attribute sets.
        // testRunner.runBidiTest(
        //  changeAutoStyle('a1', {}),
        //  changeAutoStyle('a1', ATTRS),
        //  [],
        //  null);
        TransformerTest.transformBidi(
            Helper.createChangeAutoStyleOp("a1", Helper.createJSONObject("{}")).toString(),
            Helper.createChangeAutoStyleOp("a1", attrs).toString(),
            null,
            "[]"
        );
    }
}
