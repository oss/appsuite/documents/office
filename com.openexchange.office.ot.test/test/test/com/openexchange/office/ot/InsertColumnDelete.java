/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertColumnDelete {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 8, 4, 4], end: [1, 8, 4, 5] }",
            "{ name: 'delete', start: [1, 8, 5, 4], end: [1, 8, 5, 5] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8, 4, 4], end: [1, 8, 4, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 5, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 5, 5]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 8, 3, 4], end: [1, 8, 3, 5] }",
            "{ name: 'delete', start: [1, 8, 4, 4], end: [1, 8, 4, 5] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8, 3, 4], end: [1, 8, 3, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 4, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 4, 5]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 8, 2, 4], end: [1, 8, 2, 5] }",
            "{ name: 'delete', start: [1, 8, 2, 4], end: [1, 8, 2, 5] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8, 2, 4], end: [1, 8, 2, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 2, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 2, 5]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'delete', start: [1, 8, 2, 4], end: [1, 8, 2, 5] }",
            "{ name: 'delete', start: [1, 8, 3, 4], end: [1, 8, 3, 5] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8, 2, 4], end: [1, 8, 2, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 3, 5]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 8, 2], end: [1, 8, 4] }",
            "{ name: 'delete', start: [1, 8, 2], end: [1, 8, 5] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8, 2], end: [1, 8, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8, 5]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 8, 2], end: [1, 9, 4] }",
            "{ name: 'delete', start: [1, 8, 2], end: [1, 9, 5] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8, 2], end: [1, 9, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 9, 5]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 8, 5], end: [1, 9, 2] }",
            "{ name: 'delete', start: [1, 8, 6], end: [1, 9, 2] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8, 5], end: [1, 9, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 9, 2]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertColumn operation has NOT the marker for local ignoring
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertColumn operation has the marker for local ignoring
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'insertColumn', start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [0, 3, 2, 2, 2], end: [0, 3, 2, 2, 4] }",
            "{ name: 'delete', start: [0, 3, 2, 2, 2], end: [0, 3, 2, 2, 4] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [0, 3, 2, 2, 2], end: [0, 3, 2, 2, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3, 2, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 3, 2, 2, 4]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertColumn', start: [2, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'insertColumn', start: [3, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before insert
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind insert
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 1, 3, 4, 6] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 4, 7] }",
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 4, 7]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 1, 3, 4, 2] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 4, 2] }",
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 4, 2]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'delete', start: [3, 1, 1, 3, 4, 2] }",
            "{ name: 'delete', start: [3, 1, 1, 3, 4, 3] }",
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 1, 3, 4, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 3, 4, 3]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 2, 1] }",
            "{ name: 'delete', start: [3, 1, 2, 1] }",
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 1], end: [1, 8] }",
            "{ name: 'delete', start: [1, 1], end: [1, 8] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 8] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertColumn operation has not the marker for local ignoring
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[{ name: 'delete', start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[{ name: 'delete', start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
                expect(localActions[0].operations.length).to.equal(2); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
                expect(localActions[0].operations.length).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertColumn operation has the marker for local ignoring
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertColumn operation has the marker for local ignoring
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 2], end: [1, 4] }",
            "{ name: 'delete', start: [1, 2], end: [1, 4] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertColumn operation has not the marker for local ignoring
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [0, 1], end: 1 }",
            "{ name: 'delete', start: [0, 1], end: 1 }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertColumn operation has the marker for local ignoring
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertColumn operation has the marker for local ignoring
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 1, 1, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 1, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertColumn operation has the marker for local ignoring
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 1, 1, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 1, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 1], end: [2, 1, 1, 4, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2, 1, 1, 2, 2, 3], end: [2, 1, 1, 2, 2, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 2, 2, 4], end: [2, 1, 1, 2, 2, 6] }",
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 3], end: [2, 1, 1, 2, 2, 5] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 2, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 2, 2, 6]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2, 1, 1, 2, 2, 2], end: [2, 1, 1, 2, 2, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 2, 2, 2], end: [2, 1, 1, 2, 2, 6] }",
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 2], end: [2, 1, 1, 2, 2, 5] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 2, 2, 6]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2, 1, 1, 2, 2, 2], end: [2, 1, 1, 2, 4, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 2, 2, 2], end: [2, 1, 1, 2, 4, 6] }",
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 2], end: [2, 1, 1, 2, 4, 5] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 2, 4, 6]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'delete', start: [2, 1, 1, 2, 2, 2], end: [2, 1, 1, 2, 2, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 2, 2, 3], end: [2, 1, 1, 2, 2, 6] }",
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2, 2, 2], end: [2, 1, 1, 2, 2, 5] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2, 2, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 2, 2, 6]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }",
            "{ name: 'delete', start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }",
            "{ name: 'insertColumn', start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 2, 2, 1], end: [2, 1, 2, 4, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 2, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 2, 4, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2, 1, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 4], end: [3, 8] }",
            "{ name: 'delete', start: [1, 4], end: [3, 8] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2, 1, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
                expect(localActions[0].operations.length).to.equal(2);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]);
                expect(localActions[0].operations.length).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1], end: [4] }",
            "{ name: 'delete', start: [1], end: [4] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [4] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertColumn', start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 7, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'insertColumn', start: [1, 5, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 7, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 5], end: [1, 7] }",
            "{ name: 'delete', start: [1, 5], end: [1, 7] }",
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 5], end: [1, 7], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 7]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 7, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'insertColumn', start: [1, 7, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
           /*
                oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1, 7, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 7, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10], end: [1, 11] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 10], end: [1, 11] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [1, 11] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(oneOperation.end).to.deep.equal([1, 11]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10, 5], end: [1, 10, 6] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 10, 6], end: [1, 10, 7] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10, 5], end: [1, 10, 6] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10, 6]);
                expect(oneOperation.end).to.deep.equal([1, 10, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[]",
            "{ name: 'delete', start: [1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(oneOperation.end).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(oneOperation.end).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1, 4], end: [1, 2, 4] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 1, 5], end: [1, 2, 5] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1, 4], end: [1, 2, 4] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1, 5]);
                expect(oneOperation.end).to.deep.equal([1, 2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3, 4, 8] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 3, 5, 8] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 4, 8] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3, 5, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3, 2, 8] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 3, 2, 8]}");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 2, 8] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3] }",
            "{ name: 'insertColumn', start: [1, 4, 1, 1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1, 3, 1, 1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 3] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1, 4, 1, 1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 1, 1]);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 3] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'delete', start: [0] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [0] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertColumn', start: [2, 1, 1, 1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1, 1, 1, 1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 1, 1, 1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 1, 1]);
             */
    }

    @Test
    public void test68() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertColumn', start: [3, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [4] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2]);
             */
    }

    @Test
    public void test69() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertColumn', start: [3, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2]);
             */
    }

    @Test
    public void test70() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2, 1] }",
            "{ name: 'insertColumn', start: [3, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [2, 1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2, 1] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2]);
             */
    }

    @Test
    public void test71() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 8] }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 2, 3, 8] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test72() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 2] }",
            "{ name: 'insertColumn', start: [3, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 2, 3, 2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 2] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
             */
    }

    @Test
    public void test73() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 1, 2] }",
            "{ name: 'insertColumn', start: [3, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 2, 1, 2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 1, 2] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
             */
    }

    @Test
    public void test74() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertColumn', start: [3, 1, 2, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3, 1, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 2, 4] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3, 1, 2, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test75() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3 ,1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [3, 1, 2, 4, 8]  }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test76() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3], end: [1, 6] }",
            "{ name: 'insertColumn', start: [1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 3], end: [1, 6] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 6] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3]);
                expect(oneOperation.end).to.deep.equal([1, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test77() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test78() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertColumn', start: [5, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [4, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [5, 1, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 1, 1, 2]);
             */
    }

    @Test
    public void test79() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1], end: [3] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.end).to.deep.equal([3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test80() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 2], end: [1, 6] }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 2], end: [1, 6] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 6] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(oneOperation.end).to.deep.equal([1, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test81() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[]",
            "{ name: 'delete', start: [2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test82() {
        TransformerTest.transform(
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertColumn', start: [2, 2, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[]",
            "{ name: 'delete', start: [2] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [2, 2, 1, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test83() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [3, 18] }",
            "{ name: 'insertColumn', start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'delete', start: [1, 14], end: [3, 18] }");
           /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [3, 18] };
                localActions = [{ operations: [{ name: 'insertColumn', start: [0], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 14]);
                expect(oneOperation.end).to.deep.equal([3, 18]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }
}
