/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ParaSplitInsertChar {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 10], text: 'o' }",
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'insertText', start: [2, 2], text: 'o' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 10], text: 'o' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]); // not modified
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'splitParagraph', start: [1, 11] }",
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 11]); // modified
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'insertText', start: [2, 0], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 6], opl: 1, osn: 1 }] }]; // same position
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0]); // local split operation shifts insert into second paragraph
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 0, 2, 2, 2], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'insertText', start: [3, 0, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 0, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 0, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 0, 2, 2, 2], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'insertText', start: [1, 0, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 0, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 0, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 1, 4] }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 1, 4] }",
            "{ name: 'insertText', start: [2, 2, 2, 3, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2, 2, 1, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 1, 4]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 3, 4] }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 3, 4] }",
            "{ name: 'insertText', start: [2, 2, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2, 2, 3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 3, 4]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 1, 4] }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 1, 4] }",
            "{ name: 'insertText', start: [2, 2, 2, 3, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2, 2, 1, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 1, 4]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 2, 3], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 2, 3] }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 2, 3] }",
            "{ name: 'insertText', start: [2, 2, 2, 3, 0], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 3], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2, 2, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 3, 0]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 2, 3]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 5, 2, 3] }",
            "{ name: 'splitParagraph', start: [2, 8, 2, 3] }",
            "{ name: 'insertText', start: [2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 5, 2, 3], opl: 1, osn: 1 }] }]; // split in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 8, 2, 3]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 1, 2, 3] }",
            "{ name: 'splitParagraph', start: [2, 1, 2, 3] }",
            "{ name: 'insertText', start: [2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 1, 2, 3], opl: 1, osn: 1 }] }]; // split in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 3]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 5, 2, 3] }",
            "{ name: 'splitParagraph', start: [2, 5, 2, 3] }",
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 5, 2, 3], opl: 1, osn: 1 }] }]; // split in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 2, 3]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'insertText', start: [3, 3, 2, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3], text: 'ooo' }; // insert text in a text frame
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 3, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'insertText', start: [1, 10], text: 'o' }",
            "{ name: 'insertText', start: [2, 2], text: 'o' }",
            "{ name: 'splitParagraph', start: [1, 8] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 10], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]); // modified
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }",
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [1, 11] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 11]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }",
            "{ name: 'insertText', start: [2, 0], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [1, 6] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' }] }]; // same position
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0]); // modified -> insert is shifted by the split
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 8] }",
            "{ name: 'insertText', start: [1, 10], text: 'o' }",
            "{ name: 'insertText', start: [2, 10], text: 'o' }",
            "{ name: 'splitParagraph', start: [0, 8] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 10], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 10]); // modified
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 2, 2, 4, 2] }",
            "{ name: 'insertText', start: [1, 10], text: 'o' }",
            "{ name: 'insertText', start: [1, 10], text: 'o' }",
            "{ name: 'splitParagraph', start: [0, 2, 2, 4, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 10], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2, 2, 4, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]); // not modified
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 2, 2, 4, 2] }",
            "{ name: 'insertText', start: [0, 2, 2, 4, 1], text: 'ooo' }",
            "{ name: 'insertText', start: [0, 2, 2, 4, 1], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [0, 2, 2, 4, 5] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 2, 2, 4, 1], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2, 2, 4, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2, 2, 4, 1]); // not modified
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 2, 2, 3, 1] }",
            "{ name: 'insertText', start: [0, 2, 1, 4, 2], text: 'ooo' }",
            "{ name: 'insertText', start: [0, 2, 1, 4, 2], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [0, 2, 2, 3, 1] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 3, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 2, 1, 4, 2], text: 'ooo' }] }]; // neighbour cell
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2, 2, 3, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2, 1, 4, 2]); // not modified
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'insertText', start: [0, 10], text: 'o' }",
            "{ name: 'insertText', start: [0, 10], text: 'o' }",
            "{ name: 'splitParagraph', start: [1, 8] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 10], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 10]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 2, 2, 2, 3] }",
            "{ name: 'insertText', start: [2, 2, 2, 2, 3], text: 'ooo' }",
            "{ name: 'insertText', start: [2, 2, 2, 3, 0], text: 'ooo' }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 2, 3] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2, 2, 2, 3], text: 'ooo' };
                localActions = [{ operations: [{ name: 'insertText', start: [2, 2, 2, 2, 3], text: 'ooo', opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 3, 0]);
             */
    }
}
