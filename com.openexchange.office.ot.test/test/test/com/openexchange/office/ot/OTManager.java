/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class OTManager {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "[{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } } }," +
            "{ name: 'insertFontDescription' }," +
            "{ name: 'insertStyleSheet', type: 'character' }," +
            "{ name: 'changeStyleSheet', type: 'character' }," +
            "{ name: 'deleteStyleSheet', type: 'character' }," +
            "{ name: 'insertTheme' }," +
            "{ name: 'insertParagraph', start: [1] }," +
            "{ name: 'delete', start: [1], end: [1] }," +
            "{ name: 'insertText', start: [1, 0], text: 'a' }," +
            "{ name: 'insertTab', start: [1, 1] }," +
            "{ name: 'insertBookmark', start: [1, 2] }," +
            "{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1' }," +
            "{ name: 'insertField', start: [1, 5], instruction: 'PAGE' }," +
            "{ name: 'updateField', start: [1, 5], instruction: 'PAGE' }," +
            "{ name: 'splitParagraph', start: [1, 1] }," +
            "{ name: 'mergeParagraph', start: [1], paralength: 1 }," +
            "{ name: 'insertHardBreak', start: [1, 0] }," +
            "{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } } }," +
            "{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] } }," +
            "{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true }," +
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind' }," +
            "{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0 }," +
            "{ name: 'insertCells', start: [1, 1, 1], count: 1 }," +
            "{ name: 'splitTable', start: [1, 1] }," +
            "{ name: 'insertParagraph', start: [2] }," +
            "{ name: 'delete', start: [2], end: [2] }," +
            "{ name: 'mergeTable', start: [1], rowcount: 1 }," +
            "{ name: 'delete', start: [1], end: [1] }]",
            "[{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } } }," +
            "{ name: 'insertFontDescription' }," +
            "{ name: 'insertStyleSheet', type: 'paragraph' }," +
            "{ name: 'changeStyleSheet', type: 'paragraph' }," +
            "{ name: 'deleteStyleSheet', type: 'paragraph' }," +
            "{ name: 'insertTheme' }," +
            "{ name: 'insertParagraph', start: [3] }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'insertText', start: [3, 0], text: 'a' }," +
            "{ name: 'insertTab', start: [3, 1] }," +
            "{ name: 'insertBookmark', start: [3, 2] }," +
            "{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1' }," +
            "{ name: 'insertField', start: [3, 5], instruction: 'PAGE' }," +
            "{ name: 'updateField', start: [3, 5], instruction: 'PAGE' }," +
            "{ name: 'splitParagraph', start: [3, 1] }," +
            "{ name: 'mergeParagraph', start: [3], paralength: 1 }," +
            "{ name: 'insertHardBreak', start: [3, 0] }," +
            "{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } } }," +
            "{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] } }," +
            "{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true }," +
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind' }," +
            "{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0 }," +
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }," +
            "{ name: 'splitTable', start: [3, 1] }," +
            "{ name: 'insertParagraph', start: [3] }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'mergeTable', start: [3], rowcount: 1 }," +
            "{ name: 'delete', start: [3], end: [3] }]",
            "[{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } } }," +
            "{ name: 'insertFontDescription' }," +
            "{ name: 'insertStyleSheet', type: 'paragraph' }," +
            "{ name: 'changeStyleSheet', type: 'paragraph' }," +
            "{ name: 'deleteStyleSheet', type: 'paragraph' }," +
            "{ name: 'insertTheme' }," +
            "{ name: 'insertParagraph', start: [3] }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'insertText', start: [3, 0], text: 'a' }," +
            "{ name: 'insertTab', start: [3, 1] }," +
            "{ name: 'insertBookmark', start: [3, 2] }," +
            "{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1' }," +
            "{ name: 'insertField', start: [3, 5], instruction: 'PAGE' }," +
            "{ name: 'updateField', start: [3, 5], instruction: 'PAGE' }," +
            "{ name: 'splitParagraph', start: [3, 1] }," +
            "{ name: 'mergeParagraph', start: [3], paralength: 1 }," +
            "{ name: 'insertHardBreak', start: [3, 0] }," +
            "{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } } }," +
            "{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] } }," +
            "{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true }," +
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind' }," +
            "{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0 }," +
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }," +
            "{ name: 'splitTable', start: [3, 1] }," +
            "{ name: 'insertParagraph', start: [3] }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'mergeTable', start: [3], rowcount: 1 }," +
            "{ name: 'delete', start: [3], end: [3] }]",
            "[{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } } }," +
            "{ name: 'insertFontDescription' }," +
            "{ name: 'insertStyleSheet', type: 'character' }," +
            "{ name: 'changeStyleSheet', type: 'character' }," +
            "{ name: 'deleteStyleSheet', type: 'character' }," +
            "{ name: 'insertTheme' }," +
            "{ name: 'insertParagraph', start: [1] }," +
            "{ name: 'delete', start: [1], end: [1] }," +
            "{ name: 'insertText', start: [1, 0], text: 'a' }," +
            "{ name: 'insertTab', start: [1, 1] }," +
            "{ name: 'insertBookmark', start: [1, 2] }," +
            "{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1' }," +
            "{ name: 'insertField', start: [1, 5], instruction: 'PAGE' }," +
            "{ name: 'updateField', start: [1, 5], instruction: 'PAGE' }," +
            "{ name: 'splitParagraph', start: [1, 1] }," +
            "{ name: 'mergeParagraph', start: [1], paralength: 1 }," +
            "{ name: 'insertHardBreak', start: [1, 0] }," +
            "{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } } }," +
            "{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] } }," +
            "{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true }," +
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind' }," +
            "{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0 }," +
            "{ name: 'insertCells', start: [1, 1, 1], count: 1 }," +
            "{ name: 'splitTable', start: [1, 1] }," +
            "{ name: 'insertParagraph', start: [2] }," +
            "{ name: 'delete', start: [2], end: [2] }," +
            "{ name: 'mergeTable', start: [1], rowcount: 1 }," +
            "{ name: 'delete', start: [1], end: [1] }]");

/*
                var externalActions = [
                    { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [1, 0], text: 'a', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTab', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertBookmark', start: [1, 2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitParagraph', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeParagraph', start: [1], paralength: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHardBreak', start: [1, 0], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertCells', start: [1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitTable', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [2], end: [2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeTable', start: [1], rowcount: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }
                ];
                localActions = [
                    { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [3, 0], text: 'a', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTab', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertBookmark', start: [3, 2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeParagraph', start: [3], paralength: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHardBreak', start: [3, 0], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitTable', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeTable', start: [3], rowcount: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] }
                ];
                var transformedActions = testRunner.transformActions(externalActions, localActions);
                expectActions([
                    { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [3, 0], text: 'a', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTab', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertBookmark', start: [3, 2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeParagraph', start: [3], paralength: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHardBreak', start: [3, 0], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitTable', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeTable', start: [3], rowcount: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] }
                ], localActions);
                expectActions([
                    { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [1, 0], text: 'a', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTab', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertBookmark', start: [1, 2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitParagraph', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeParagraph', start: [1], paralength: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHardBreak', start: [1, 0], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertCells', start: [1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitTable', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [2], end: [2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeTable', start: [1], rowcount: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }
                ], transformedActions);
*/
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'insertText', opl: 1, osn: 1, start: [0, 5], text: 'remote' }",
            "[{ name: 'delete', start: [0, 4], end: [0, 6], opl: 1, osn: 1 }, { name: 'insertText', opl: 1, osn: 1, start: [0, 7], text: 'local' }]",
            "[{ name: 'delete', start: [0, 4], end: [0, 12], opl: 1, osn: 1 }, { name: 'insertText', opl: 1, osn: 1, start: [0, 7], text: 'local' }]",
            "[]");
            /*
                // an insertText operation that follows a delete operation -> the delete operation includes the remotely inserted text and therefore marks the remote operation as removed.
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 5], text: 'remote' };
                localActions = [{ operations: [
                    { name: 'delete', start: [0, 4], end: [0, 6], opl: 1, osn: 1 },
                    { name: 'insertText', opl: 1, osn: 1, start: [0, 7], text: 'local' }
                ] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'delete', start: [0, 4], end: [0, 12], opl: 1, osn: 1 },
                    { name: 'insertText', opl: 1, osn: 1, start: [0, 7], text: 'local' }
                ] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }",
            "[{ name: 'delete', start: [1, 2], end: [2, 8], opl: 1, osn: 1 }, { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 7 }]",
            "[{ name: 'delete', start: [1, 2], end: [3, 8], opl: 1, osn: 1 }, { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 7 }]",
            "[]");
            /*
                // a mergeParagraph operation that follows a delete operation -> the delete operation includes the remotely splitted paragraph and therefore marks the remote operation as removed.
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [
                    { name: 'delete', start: [1, 2], end: [2, 8], opl: 1, osn: 1 },
                    { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 7 }
                ] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'delete', start: [1, 2], end: [3, 8], opl: 1, osn: 1 },
                    { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 7 }
                ] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } }",
            "[{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }]",
            "[{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }]",
            "[{ name: 'setAttributes', opl: 1, osn: 1, start: [2, 4], end: [2, 5], attrs: { character: { italic: true } } }, { name: 'setAttributes', opl: 1, osn: 1, start: [3, 0], end: [3, 2], attrs: { character: { italic: true } } }]");
            /*
                // external change inside one paragraph and local splitParagraph in this paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } };
                localActions = [{ operations: [
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }
                ] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }
                ] }], localActions);
                expectOp([
                    { name: 'setAttributes', opl: 1, osn: 1, start: [2, 4], end: [2, 5], attrs: { character: { italic: true } } },
                    { name: 'setAttributes', opl: 1, osn: 1, start: [3, 0], end: [3, 2], attrs: { character: { italic: true } } }
                ], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } }",
            "[{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }, { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }]",
            "[{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }, { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }]",
            "[{ name: 'setAttributes', opl: 1, osn: 1, start: [2, 4], end: [2, 5], attrs: { character: { italic: true } } }, { name: 'setAttributes', opl: 1, osn: 1, start: [3, 0], end: [3, 5], attrs: { character: { italic: true } } }]");
            /*
                // external change inside one paragraph and local splitParagraph in this paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } };
                localActions = [{ operations: [
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] },
                    { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }
                ] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] },
                    { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }
                ] }], localActions);
                expectOp([
                    { name: 'setAttributes', opl: 1, osn: 1, start: [2, 4], end: [2, 5], attrs: { character: { italic: true } } },
                    { name: 'setAttributes', opl: 1, osn: 1, start: [3, 0], end: [3, 5], attrs: { character: { italic: true } } }
                ], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } }",
            "[{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 23] },{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }, { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }]",
            "[{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 23] },{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] }, { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }]",
            "[{ name: 'setAttributes', opl: 1, osn: 1, start: [2, 4], end: [2, 5], attrs: { character: { italic: true } } }, { name: 'setAttributes', opl: 1, osn: 1, start: [3, 0], end: [3, 5], attrs: { character: { italic: true } } }]");
            /*
                // external change inside one paragraph and local splitParagraph in this paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } };
                localActions = [{ operations: [
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 23] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] },
                    { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }
                ] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 23] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 14] },
                    { name: 'insertText', opl: 1, osn: 1, start: [3, 1], text: 'bbb' }
                ] }], localActions);
                expectOp([
                    { name: 'setAttributes', opl: 1, osn: 1, start: [2, 4], end: [2, 5], attrs: { character: { italic: true } } },
                    { name: 'setAttributes', opl: 1, osn: 1, start: [3, 0], end: [3, 5], attrs: { character: { italic: true } } }
                ], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } }",
            "[{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 23] },{ name: 'splitParagraph', opl: 1, osn: 1, start: [2, 20] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 14] }, { name: 'insertText', opl: 1, osn: 1, start: [4, 1], text: 'bbb' }]",
            "[{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 23] },{ name: 'splitParagraph', opl: 1, osn: 1, start: [2, 20] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 14] }, { name: 'insertText', opl: 1, osn: 1, start: [4, 1], text: 'bbb' }]",
            "[{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 5], attrs: { character: { italic: true } } }, { name: 'setAttributes', opl: 1, osn: 1, start: [4, 0], end: [4, 5], attrs: { character: { italic: true } } }]");
            /*
                // external change inside one paragraph and local splitParagraph in this paragraph
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 18], end: [1, 22], attrs: { character: { italic: true } } };
                localActions = [{ operations: [
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 23] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 20] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 14] },
                    { name: 'insertText', opl: 1, osn: 1, start: [4, 1], text: 'bbb' }
                ] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 23] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 20] },
                    { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 14] },
                    { name: 'insertText', opl: 1, osn: 1, start: [4, 1], text: 'bbb' }
                ] }], localActions);
                expectOp([
                    { name: 'setAttributes', opl: 1, osn: 1, start: [3, 4], end: [3, 5], attrs: { character: { italic: true } } },
                    { name: 'setAttributes', opl: 1, osn: 1, start: [4, 0], end: [4, 5], attrs: { character: { italic: true } } }
                ], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[{ name: 'mergeTable', start: [4], rowcount: 6 }," +
                "{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo' }," +
                "{ name: 'insertRows', start: [4, 1], count: 3 }," +
                "{ name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } } }," +
                "{ name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true }," +
                "{ name: 'splitTable', start: [0, 2] }," +
                "{ name: 'insertParagraph', start: [1] }]",
            "[{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'mergeTable', start: [4], rowcount: 6 }," +
                "{ name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo' }," +
                "{ name: 'insertRows', start: [4, 1], count: 3 }," +
                "{ name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } } }," +
                "{ name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true }," +
                "{ name: 'splitTable', start: [0, 2] }," +
                "{ name: 'insertParagraph', start: [1] }]",
            "{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                var externalActions = [
                    { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }
                ];
                localActions = [
                    { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                        { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                    ] },
                    { operations: [
                        { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                    ] }
                ];
                var transformedActions = testRunner.transformActions(externalActions, localActions);
                expectActions([
                    { operations: [
                        { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                        { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                        { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                    ] },
                    { operations: [
                        { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                    ] }
                ], localActions);
                expectActions([
                    { operations: [{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }
                ], transformedActions);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "[{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp' }," +
                "{ name: 'insertBookmark', start: [5, 1, 4, 0, 0], position: 'start', type: 'comment', id: 'cmt6' }," +
                "{ name: 'insertTab', start: [5, 1, 4, 0, 1] }," +
                "{ name: 'splitParagraph', start: [1, 0] }," +
                "{ name: 'splitParagraph', start: [2, 0] }]",
            "[{ name: 'mergeTable', start: [4], rowcount: 6 }," +
                "{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo' }," +
                "{ name: 'insertRows', start: [4, 1], count: 3 }," +
                "{ name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } } }," +
                "{ name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true }," +
                "{ name: 'splitTable', start: [0, 2] }," +
                "{ name: 'insertParagraph', start: [1] }"
                + "]",
            "[{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'mergeTable', start: [6], rowcount: 6 }," +
                "{ name: 'insertText', start: [6, 4, 5, 0, 0], text: 'ooo' }," +
                "{ name: 'insertRows', start: [6, 1], count: 3 }," +
                "{ name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }}," +
                "{ name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true }," +
                "{ name: 'splitTable', start: [0, 2] }," +
                "{ name: 'insertParagraph', start: [1] }]",
            "[{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'insertText', start: [7, 10, 5, 0, 0], text: 'ppp' }," +
                "{ name: 'insertBookmark', start: [7, 10, 5, 0, 0], position: 'start', type: 'comment', id: 'cmt6' }," +
                "{ name: 'insertTab', start: [7, 10, 5, 0, 1] }," +
                "{ name: 'splitParagraph', opl: 1, osn: 1, start: [4, 0] }," +
                "{ name: 'splitParagraph', opl: 1, osn: 1, start: [5, 0] }]");

            /*
                var externalActions = [
                    { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertBookmark', start: [5, 1, 4, 0, 0], opl: 1, osn: 1 },
                        { name: 'insertTab', start: [5, 1, 4, 0, 1], opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 0] }] }
                ];
                localActions = [
                    { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                        { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                    ] },
                    { operations: [
                        { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                    ] }
                ];
                var transformedActions = testRunner.transformActions(externalActions, localActions);
                expectActions([
                    { operations: [
                        { name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                        { name: 'mergeTable', start: [6], rowcount: 6, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'insertText', start: [6, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [6, 1], count: 3, opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                        { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                    ] },
                    { operations: [
                        { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                    ] }
                ], localActions);
                expectActions([
                    { operations: [{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [7, 10, 5, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertBookmark', start: [7, 10, 5, 0, 0], opl: 1, osn: 1 },
                        { name: 'insertTab', start: [7, 10, 5, 0, 1], opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [4, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 0] }] }
                ], transformedActions);
            */

    }
}
