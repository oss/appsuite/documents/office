/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsInsertChar {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 5], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2], end: [1, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 5], end: [1, 7], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], end: [1, 4], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(oneOperation.end).to.deep.equal([1, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2], end: [2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 5], end: [2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], end: [2, 4], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(oneOperation.end).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 5], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 5], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 5], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [2, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [2, 1], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [2, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 7], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 4], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 7]); // the insert must shift the setAttributes
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]); // the insert must shift the setAttributes
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 4], end: [1, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 7], end: [1, 7], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 4], end: [1, 4], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 7]); // the insert must shift the setAttributes
                expect(oneOperation.end).to.deep.equal([1, 7]); // the insert must shift the setAttributes
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]); // the insert must shift the setAttributes
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 4], end: [1, 6], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 4], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 7], end: [1, 9], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 4], end: [1, 6], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 7]);
                expect(oneOperation.end).to.deep.equal([1, 9]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1, 2, 2, 2], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",
            "{ name: 'setAttributes', start: [0, 1, 2, 2, 2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1, 2, 2, 2], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1, 2, 2, 3]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1, 2, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",
            "{ name: 'setAttributes', start: [0, 1, 2, 2, 7], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1, 2, 2, 4], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1, 2, 2, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1, 2, 2, 3]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1, 2, 3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",
            "{ name: 'setAttributes', start: [0, 1, 2, 3, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1, 2, 3, 4], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1, 2, 2, 3]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1, 1, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",
            "{ name: 'insertText', start: [0, 1, 2, 2, 3], text: 'ppp' }",
            "{ name: 'setAttributes', start: [0, 1, 1, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [0, 1, 1, 2, 4], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1, 2, 2, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1, 1, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1, 2, 2, 3]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 2, 2, 2], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 2, 2, 2, 2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2, 2], attrs: { character: { italic: true } } };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 2, 2], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 5, 2, 2], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2], attrs: { character: { italic: true } } }; // in a text frame
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 5, 2, 2], end: [1, 5, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }; // in a text frame
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(oneOperation.end).to.deep.equal([1, 5, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [1, 3], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 3], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }; // in a text frame
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 3], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]);
                expect(oneOperation.end).to.deep.equal([1, 2, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [0, 1], text: 'ppp' }",
            "{ name: 'setAttributes', start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [1, 2, 2, 2], end: [1, 2, 2, 4], attrs: { character: { italic: true } } }; // in a text frame
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [0, 1], text: 'ppp' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]);
                expect(oneOperation.end).to.deep.equal([1, 2, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'updateField', start: [1, 5], type: 'PAGE' }");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }], localActions);
                expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 5], type: 'PAGE' }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 0], type: 'PAGE' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'updateField', start: [1, 0], type: 'PAGE' }");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 0], type: 'PAGE' };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }], localActions);
                expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 0], type: 'PAGE' }], transformedOps);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'updateField', start: [1, 1], type: 'PAGE' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'updateField', start: [1, 4], type: 'PAGE' }");
            /*
                oneOperation = { name: 'updateField', opl: 1, osn: 1, start: [1, 1], type: 'PAGE' };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }], localActions);
                expectOp([{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], type: 'PAGE' }], transformedOps);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'updateField', start: [1, 2], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 5], type: 'PAGE' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 2], type: 'PAGE' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 5], type: 'PAGE' }] }], localActions);
                expectOp([{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }], transformedOps);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'updateField', start: [1, 0], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 0], type: 'PAGE' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 0], type: 'PAGE' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 0], type: 'PAGE' }] }], localActions);
                expectOp([{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'updateField', start: [1, 1], type: 'PAGE' }",
            "{ name: 'updateField', start: [1, 4], type: 'PAGE' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
                localActions = [{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 1], type: 'PAGE' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'updateField', opl: 1, osn: 1, start: [1, 4], type: 'PAGE' }] }], localActions);
                expectOp([{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }], transformedOps);
             */
    }
}

