/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsDelete {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 7], end: [3, 7] }",
            "{ name: 'delete', start: [3, 7], end: [3, 7] }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is in front of the local delete in the same paragraph (no ranges)
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [3, 7], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 7]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 7]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }


    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 7], end: [3, 9] }",
            "{ name: 'delete', start: [3, 7], end: [3, 9] }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is in front of the local delete in the same paragraph (with ranges)
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [3, 9], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 7]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 9]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 6], end: [3, 6], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'setAttributes', start: [3, 5], end: [3, 5], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is behind the local delete in the same paragraph (no ranges)
                oneOperation = { name: 'setAttributes', start: [3, 6], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 5]); // modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",
            "{ name: 'setAttributes', start: [3, 3], end: [3, 5], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is behind the local delete in the same paragraph (with ranges)
                oneOperation = { name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 3]); // modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",
            "{ name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is in a paragraph before the local delete
                oneOperation = { name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",
            "{ name: 'delete', start: [3, 2], end: [3, 4] }",
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is in a paragraph behind the local delete
                oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([5, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([5, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [1, 6], end: [3, 4] }",
            "{ name: 'delete', start: [1, 6], end: [3, 4] }",
            "{ name: 'setAttributes', start: [4, 6], end: [4, 8], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is in a paragraph behind the local delete over several paragraphs (without following merge)
                oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 6]); // modified
                expect(oneOperation.end).to.deep.equal([4, 8]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } } }",
            "[{ name: 'delete', start: [1, 6], end: [3, 4]}, { name: 'mergeParagraph', start: [1], paralength: 6 }]",
            "[{ name: 'delete', start: [1, 6], end: [3, 4]}, { name: 'mergeParagraph', start: [1], paralength: 6 }]",
            "{ name: 'setAttributes', start: [3, 6], end: [3, 8], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is in a paragraph behind the local delete over several paragraphs (with following merge)
                oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(2); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 6]); // modified
                expect(oneOperation.end).to.deep.equal([3, 8]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is in a paragraph behind the local delete over several complete paragraphs (without following merge)
                oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } } }",
            "[{ name: 'delete', start: [1, 3], end: [3] }, { name: 'mergeParagraph', start: [1], paralength: 3 }]",
            "[{ name: 'delete', start: [1, 3], end: [3] }, { name: 'mergeParagraph', start: [1], paralength: 3 }]",
            "{ name: 'setAttributes', start: [2, 6], end: [2, 8], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is in a paragraph behind the local delete over several complete paragraphs (with following merge)
                oneOperation = { name: 'setAttributes', start: [5, 6], end: [5, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 3], end: [3], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[1].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[1].paralength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(2); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 6]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [1, 6], end: [3, 4] }",
            "{ name: 'delete', start: [1, 6], end: [3, 4] }",
            "{ name: 'setAttributes', start: [2, 1], end: [4], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is inside the last paragraph behind the local delete over several paragraphs (without following merge)
                oneOperation = { name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1]); // modified (5 characters inside the paragraph deleted)
                expect(oneOperation.end).to.deep.equal([4]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { italic: true } } }",
            "[{ name: 'delete', start: [1, 6], end: [3, 4] }, { name: 'mergeParagraph', start: [1], paralength: 6 }]",
            "[{ name: 'delete', start: [1, 6], end: [3, 4] }, { name: 'mergeParagraph', start: [1], paralength: 6 }]",
            "{ name: 'setAttributes', start: [1, 7], end: [3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes is in a paragraph behind the local delete over several paragraphs (with following merge)
                oneOperation = { name: 'setAttributes', start: [3, 6], end: [5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6], end: [3, 4], opl: 1, osn: 1 }, { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[1].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[1].paralength).to.equal(6); // not modified
                expect(localActions[0].operations.length).to.equal(2); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 7]); // modified (deleting the 8th character after merge)
                expect(oneOperation.end).to.deep.equal([3]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3, 2], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 7], end: [6] }",
            "{ name: 'delete', start: [3, 7], end: [6] }",
            "{ name: 'setAttributes', start: [2], end: [3, 2], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes ends in the first paragraph of the local delete (without following merge)
                oneOperation = { name: 'setAttributes', start: [2], end: [3, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 7], end: [6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 7]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 2]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 7], end: [6] }",
            "{ name: 'delete', start: [4, 7], end: [6] }",
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several paragraphs before the local delete
                oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7], end: [6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [8], end: [9], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 7], end: [6] }",
            "{ name: 'delete', start: [4, 7], end: [6] }",
            "{ name: 'setAttributes', start: [6], end: [7], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several paragraphs behind the local delete
                oneOperation = { name: 'setAttributes', start: [8], end: [9], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7], end: [6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([6]); // modified
                expect(oneOperation.end).to.deep.equal([7]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6] }",
            "{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6] }",
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several top level paragraphs before the local delete inside a table
                oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 2, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [2], end: [3] }",
            "{ name: 'delete', start: [2], end: [3] }",
            "{ name: 'setAttributes', start: [2, 7, 2, 2, 1], end: [2, 7, 2, 2, 6], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes content inside a table behind the local removal of paragraphs before the table
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 7, 2, 2, 1]); // modified
                expect(oneOperation.end).to.deep.equal([2, 7, 2, 2, 6]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [5], end: [6], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6] }",
            "{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6] }",
            "{ name: 'setAttributes', start: [5], end: [6], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several top level paragraphs behind the local delete inside a table
                oneOperation = { name: 'setAttributes', start: [5], end: [6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2, 1], end: [4, 7, 2, 2, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 2, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([5]); // not modified
                expect(oneOperation.end).to.deep.equal([6]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 6], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [5], end: [6] }",
            "{ name: 'delete', start: [5], end: [6] }",
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 6], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table in front of the local deleted top level paragraphs
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [5], end: [6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 6]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8] }",
            "{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8] }",
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table in front of the local deleted paragraphs in the same table cell
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 4, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 4, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 3]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8] }",
            "{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8] }",
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table in front of the local deleted paragraphs in another cell
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 3, 4, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 3, 4, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 3]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8] }",
            "{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8] }",
            "{ name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table behind the local deleted paragraphs in the same table cell
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 4, 2], end: [4, 7, 2, 4, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 4, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 4, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 6, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 6, 8]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 4, 1, 2], end: [4, 7, 4, 1, 5], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8] }",
            "{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8] }",
            "{ name: 'setAttributes', start: [4, 7, 4, 1, 2], end: [4, 7, 4, 1, 5], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table behind the local deleted paragraphs in another cell
                oneOperation = { name: 'setAttributes', start: [4, 7, 4, 1, 2], end: [4, 7, 4, 1, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 3, 4, 2], end: [4, 7, 3, 4, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 3, 4, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 3, 4, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 4, 1, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([4, 7, 4, 1, 5]); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] }",
            "{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3] }",
            "{ name: 'setAttributes', start: [4, 7, 2, 4, 4], end: [4, 7, 2, 4, 8], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes changes several paragraphs inside a table behind the local deleted paragraphs in the same table cell
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 6, 4], end: [4, 7, 2, 6, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 7, 2, 2], end: [4, 7, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 7, 2, 2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 7, 2, 3]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 4, 4]); // modified
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 4, 8]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [2], end: [3] }",
            "{ name: 'delete', start: [2], end: [3] }",
            "{ name: 'setAttributes', start: [2, 7, 2, 2], end: [2, 7, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].end).to.deep.equal([3]);
                expect(oneOperation.start).to.deep.equal([2, 7, 2, 2]);
                expect(oneOperation.end).to.deep.equal([2, 7, 2, 4]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 2], end: [4, 3] }",
            "{ name: 'delete', start: [4, 2], end: [4, 3] }",
            "{ name: 'setAttributes', start: [4, 5, 2, 2], end: [4, 5, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [4, 2], end: [4, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([4, 3]);
                expect(oneOperation.start).to.deep.equal([4, 5, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 5, 2, 4]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 2] }",
            "{ name: 'delete', start: [4, 2] }",
            "{ name: 'setAttributes', start: [4, 6, 2, 2], end: [4, 6, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [4, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 2]);
                expect(oneOperation.start).to.deep.equal([4, 6, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 6, 2, 4]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 8], end: [4, 9] }",
            "{ name: 'delete', start: [4, 8], end: [4, 9] }",
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [4, 8], end: [4, 9], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 8]);
                expect(localActions[0].operations[0].end).to.deep.equal([4, 9]);
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 4]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 8] }",
            "{ name: 'delete', start: [4, 8] }",
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [4, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 8]);
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 4]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 1], end: [3, 3] }",
            "{ name: 'delete', start: [3, 1], end: [3, 3] }",
            "{ name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } } }");
            /*
                oneOperation = { name: 'setAttributes', start: [4, 7, 2, 2], end: [4, 7, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
                expect(oneOperation.start).to.deep.equal([4, 7, 2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 7, 2, 4]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "[]");
            /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // setAttributes operation got marker for ignoring
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "[]");
            /*
                oneOperation = { name: 'setAttributes', start: [4], end: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // setAttributes operation got marker for ignoring
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 3, 2, 2, 4] }",
            "{ name: 'delete', start: [4, 3, 2, 2, 4] }",
            "[]");
            /*
                oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2, 4], end: [4, 3, 2, 2, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 3, 2, 2, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // setAttributes operation got marker for ignoring
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [4, 3, 2, 2] }",
            "{ name: 'delete', start: [4, 3, 2, 2] }",
            "[]");
            /*
                oneOperation = { name: 'setAttributes', start: [4, 3, 2, 2], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [4, 3, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // setAttributes operation got marker for ignoring
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 3], end: [3, 6] }",
            "{ name: 'delete', start: [3, 3], end: [3, 6] }",
            "[]");
            /*
                // the external setAttributes is surrounded by the local delete in the same paragraph
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external setAttributes operation has the marker for ignoring
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 5], end: [3, 8], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 5], end: [3, 7] }",
            "{ name: 'delete', start: [3, 5], end: [3, 7] }",
            "{ name: 'setAttributes', start: [3, 5], end: [3, 5], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes surrounds the local delete in the same paragraph
                oneOperation = { name: 'setAttributes', start: [3, 5], end: [3, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 5], end: [3, 7], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 7]); // not modified
                expect(oneOperation.start).to.deep.equal([3, 5]); // modified
                expect(oneOperation.end).to.deep.equal([3, 5]); // modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'setAttributes', start: [3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes surrounds the local delete because it is an ancestor node
                oneOperation = { name: 'setAttributes', start: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.equal(undefined); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // local delete operation does not got marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes surrounds the local delete because it is an ancestor node
                oneOperation = { name: 'setAttributes', start: [3], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'delete', start: [3, 4], end: [3, 4] }",
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes surrounds the local delete because its final paragraph is an ancestor node
                oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6] }",
            "{ name: 'delete', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6] }",
            "{ name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes surrounds the local delete because its final table is an ancestor node
                oneOperation = { name: 'setAttributes', start: [2], end: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 2, 2, 0]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 2, 2, 6]); // not modified
                expect(oneOperation.start).to.deep.equal([2]); // not modified
                expect(oneOperation.end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external setAttributes operation has not the marker for ignoring
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [2], end: [5] }",
            "{ name: 'delete', start: [2], end: [5] }",
            "[]");
            /*
                // the external setAttributes is surrounded by the local delete because several paragraphs are removed
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external setAttributes operation has the marker for ignoring
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [2], end: [3] }",
            "{ name: 'delete', start: [2], end: [3] }",
            "[]");
            /*
                // the external setAttributes inside a table is surrounded by the local delete because several paragraphs are removed
                oneOperation = { name: 'setAttributes', start: [3, 4, 2, 2, 0], end: [3, 4, 2, 2, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external setAttributes operation has the marker for ignoring
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [2], end: [5] }",
            "{ name: 'delete', start: [2], end: [5] }",
            "[]");
            /*
                // the external setAttributes of paragraphs is surrounded by the local delete because several paragraphs are removed
                oneOperation = { name: 'setAttributes', start: [3], end: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2], end: [5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external setAttributes operation has the marker for ignoring
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 5, 2, 1], end: [3, 5, 2, 4] }",
            "{ name: 'delete', start: [3, 5, 2, 1], end: [3, 5, 2, 4] }",
            "{ name: 'setAttributes', start: [3], attrs: { character: { italic: true } } }");
            /*
                // the external setAttributes surrounds the local delete because that is an ancestor table
                oneOperation = { name: 'setAttributes', start: [3], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 5, 2, 1], end: [3, 5, 2, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 2, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5, 2, 4]); // not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined); // delete operation does not get marker for ignoring
                expect(oneOperation.start).to.deep.equal([3]); // not modified
                expect(oneOperation.end).to.equal(undefined); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 3], end: [3, 5] }",
            "{ name: 'delete', start: [3, 3], end: [3, 5] }",
            "{ name: 'setAttributes', start: [3, 3], end: [3, 3], attrs: { character: { italic: true } } }");
            /*
                // the two ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 3]); // modified, only one character must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 3]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 1], end: [3, 5] }",
            "{ name: 'delete', start: [3, 1], end: [3, 5] }",
            "{ name: 'setAttributes', start: [3, 1], end: [3, 1], attrs: { character: { italic: true } } }");
            /*
                // the two ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 6], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1], end: [3, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 1]); // modified, only one character must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 1]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [0, 1], end: [0, 5] }",
            "{ name: 'delete', start: [0, 1], end: [0, 5] }",
            "{ name: 'setAttributes', start: [0, 1], end: [0, 3], attrs: { character: { italic: true } } }");
            /*
                // the two ranges overlap each other inside the same paragraph (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [0, 3], end: [0, 8], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([0, 5]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([0, 1]); // modified, only three characters must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([0, 3]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [4], end: [9], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [1], end: [4] }",
            "{ name: 'delete', start: [1], end: [4] }",
            "{ name: 'setAttributes', start: [1], end: [5], attrs: { character: { italic: true } } }");
            /*
                // the two ranges overlap on paragraph level (local operation before external operation)
                oneOperation = { name: 'setAttributes', start: [4], end: [9], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // modified, 5 instead of 6 paragraphs must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([5]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 7], end: [3, 7] }",
            "{ name: 'delete', start: [3, 7], end: [3, 7] }",
            "{ name: 'setAttributes', start: [3, 4], end: [3, 4], attrs: { character: { italic: true } } }");
            /*
                // the two ranges overlap each other inside the same paragraph (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 3], end: [3, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 6]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 2]); // modified, only one character must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 2]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3, 4], end: [3, 10] }",
            "{ name: 'delete', start: [3, 4], end: [3, 10] }",
            "{ name: 'setAttributes', start: [3, 1], end: [3, 3], attrs: { character: { italic: true } } }");
            /*
                // the two ranges overlap each other inside the same paragraph (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 4], end: [3, 10], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 10]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 1]); // modified, only three characters must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([3, 3]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [3], end: [8] }",
            "{ name: 'delete', start: [3], end: [8] }",
            "{ name: 'setAttributes', start: [1], end: [2], attrs: { character: { italic: true } } }");
            /*
                // the two ranges overlap on paragraph level (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [1], end: [4], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3], end: [8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // modified, 2 instead of 4 paragraphs must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([2]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 2], end: [0, 5], attrs: { character: { italic: true } } }",
            "{ name: 'delete', start: [0, 4], end: [0, 8] }",
            "{ name: 'delete', start: [0, 4], end: [0, 8] }",
            "{ name: 'setAttributes', start: [0, 2], end: [0, 3], attrs: { character: { italic: true } } }");
            /*
                // the two ranges overlap each other inside the same paragraph (local operation after external operation)
                oneOperation = { name: 'setAttributes', start: [0, 2], end: [0, 5], attrs: { character: { italic: true } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [0, 4], end: [0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 4]); // not modified
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([0, 2]); // modified, only two characters must be changed by the external operation
                expect(oneOperation.end).to.deep.equal([0, 3]); // modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external operation has not the marker for ignoring
             */
    }

    // it('should calculate valid transformed setAttributes operation after local delete operation even for invalid setAttributes operations', function () {
    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 10], end: [10, 10] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'setAttributes', start: [2, 10], end: [9, 10] }");
            /*
        oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
        localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [2, 10], end: [9, 10] }], transformedOps);
             */
    }
    
    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 10], end: [10, 10] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'setAttributes', start: [3, 10], end: [9, 10] }");
            /*
        oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
        localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [9, 10] }], transformedOps);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 10], end: [10, 10] }",
            "{ name: 'delete', start: [11] }",
            "{ name: 'delete', start: [11] }",
            "{ name: 'setAttributes', start: [3, 10], end: [10, 10] }");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
    localActions = [{ operations: [{ name: 'delete', start: [11], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [11], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] }], transformedOps);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 10], end: [10, 10] }",
            "{ name: 'delete', start: [3] }",
            "{ name: 'delete', start: [3] }",
            "{ name: 'setAttributes', start: [3], end: [9, 10] }");
            /*
    // Info: New valid start position is [3] (this is fine, because the setAttributes operation is not supported)
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
    localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [9, 10] }], transformedOps);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 10], end: [10, 10] }",
            "{ name: 'delete', start: [3], end: [4] }",
            "{ name: 'delete', start: [3], end: [4] }",
            "{ name: 'setAttributes', start: [3], end: [8, 10] }");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
    localActions = [{ operations: [{ name: 'delete', start: [3], end: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [2], end: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [8, 10] }], transformedOps);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 10], end: [10, 10] }",
            "{ name: 'delete', start: [10] }",
            "{ name: 'delete', start: [10] }",
            "{ name: 'setAttributes', start: [3, 10], end: [9, 10] }");
            /*
    // Info: New valid end position is [9, 10] (this is fine, because the setAttributes operation is not supported)
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
    localActions = [{ operations: [{ name: 'delete', start: [10], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [10], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [9, 10] }], transformedOps);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 10], end: [10, 10] }",
            "{ name: 'delete', start: [4], end: [5] }",
            "{ name: 'delete', start: [4], end: [5] }",
            "{ name: 'setAttributes', start: [3, 10], end: [8, 10] }");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [10, 10] };
    localActions = [{ operations: [{ name: 'delete', start: [4], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3, 10], end: [8, 10] }], transformedOps);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [10] }",
            "{ name: 'delete', start: [3] }",
            "{ name: 'delete', start: [3] }",
            "{ name: 'setAttributes', start: [3], end: [9] }");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
    localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [9] }], transformedOps);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [10] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'setAttributes', start: [3], end: [9] }");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
    localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [9] }], transformedOps);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [10] }",
            "{ name: 'delete', start: [4], end: [5] }",
            "{ name: 'delete', start: [4], end: [5] }",
            "{ name: 'setAttributes', start: [3], end: [8] }");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
    localActions = [{ operations: [{ name: 'delete', start: [4], end: [5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [4], end: [5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [8] }], transformedOps);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [10] }",
            "{ name: 'delete', start: [5], end: [11] }",
            "{ name: 'delete', start: [5], end: [11] }",
            "{ name: 'setAttributes', start: [3], end: [4] }");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
    localActions = [{ operations: [{ name: 'delete', start: [5], end: [11], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [5], end: [11], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [4] }], transformedOps);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [10] }",
            "{ name: 'delete', start: [4], end: [11] }",
            "{ name: 'delete', start: [4], end: [11] }",
            "{ name: 'setAttributes', start: [3], end: [3] }");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
    localActions = [{ operations: [{ name: 'delete', start: [4], end: [11], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [4], end: [11], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [3] }], transformedOps);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [10] }",
            "{ name: 'delete', start: [3], end: [11] }",
            "{ name: 'delete', start: [3], end: [11] }",
            "[]");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
    localActions = [{ operations: [{ name: 'delete', start: [3], end: [11], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3], end: [11], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [10] }",
            "{ name: 'delete', start: [2], end: [11] }",
            "{ name: 'delete', start: [2], end: [11] }",
            "[]");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
    localActions = [{ operations: [{ name: 'delete', start: [2], end: [11], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [2], end: [11], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3], end: [10] }",
            "{ name: 'delete', start: [2], end: [6] }",
            "{ name: 'delete', start: [2], end: [6] }",
            "{ name: 'setAttributes', start: [2], end: [5] }");
            /*
    oneOperation = { name: 'setAttributes', opl: 1, osn: 1, start: [3], end: [10] };
    localActions = [{ operations: [{ name: 'delete', start: [2], end: [6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [2], end: [6], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'setAttributes', opl: 1, osn: 1, start: [2], end: [5] }], transformedOps);
             */
    }
}
