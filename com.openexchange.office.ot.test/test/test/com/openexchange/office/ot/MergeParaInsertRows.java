/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MergeParaInsertRows {

    // insertRows and local mergeParagraph operation (handleMergeCompInsertRows)
    // it('should calculate valid transformed insertRows operation after local mergeParagraph operation', function () {
    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeParagraph', start: [5], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [5], paralength: 6 }",
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [5], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 4, 2, 2], paralength: 6 }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 6, 2, 2], paralength: 6 }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 6, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0 }",
            "{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 4, 2, 2], paralength: 6 }",
            "{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'insertRows', start: [3, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'insertRows', start: [3, 1, 2, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6 }",
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 1, 3, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'insertRows', start: [3, 1, 1, 3, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1, 1, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 6 }",
            "{ name: 'insertRows', start: [0, 9, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 9, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 6, 1, 3], paralength: 6 }",
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 6, 1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    // insertRows and local mergeTable operation (handleMergeCompInsertRows)
    // it('should calculate valid transformed insertRows operation after local mergeTable operation', function () {
    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [2, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeTable', start: [1], rowcount: 7 }",
            "{ name: 'mergeTable', start: [1], rowcount: 7 }",
            "{ name: 'insertRows', start: [1, 8], count: 3, referenceRow: 7 }");
            /*
                oneOperation = { name: 'insertRows', start: [2, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 7, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 8]);
                expect(oneOperation.referenceRow).to.equal(7);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeTable', start: [5], rowcount: 6 }",
            "{ name: 'mergeTable', start: [5], rowcount: 6 }",
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 4, 2, 2], rowcount: 6 }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 6, 2, 2], rowcount: 6 }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 6, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0 }",
            "{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 4, 2, 2], rowcount: 6 }",
            "{ name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1], count: 1, referenceRow: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1]);
                expect(oneOperation.referenceRow).to.equal(0);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'insertRows', start: [3, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'insertRows', start: [3, 1, 2, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6 }",
            "{ name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1, 2, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [3, 1, 1, 3, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'insertRows', start: [3, 1, 1, 3, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [3, 1, 1, 3, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeTable', start: [0], rowcount: 6 }",
            "{ name: 'mergeTable', start: [0], rowcount: 6 }",
            "{ name: 'insertRows', start: [0, 9, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 9, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 6, 1, 3], rowcount: 6 }",
            "{ name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1 }");
            /*
                oneOperation = { name: 'insertRows', start: [1, 3, 2, 2], count: 3, referenceRow: 1, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 6, 1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2]);
                expect(oneOperation.referenceRow).to.equal(1);
             */
    }

    // insertRows and local mergeTable operation (handleMergeCompInsertRows)
    // it('should calculate valid transformed insertRows operation after local mergeTable operation with special handling of property referenceRow', function () {
    @Test
    public void test01a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2 }",
            "{ name: 'mergeTable', start: [3], rowcount: 4 }",
            "{ name: 'mergeTable', start: [3], rowcount: 4 }",
            "{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 6 }");
            /*
        oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 6, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0 }",
            "{ name: 'mergeTable', start: [3], rowcount: 4 }",
            "{ name: 'mergeTable', start: [3], rowcount: 4 }",
            "{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 4 }");
            /*
        oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2 }",
            "{ name: 'mergeTable', start: [2], rowcount: 4 }",
            "{ name: 'mergeTable', start: [2], rowcount: 4 }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 2 }");
            /*
        oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04a() {
        TransformerTest.transform(
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2 }",
            "{ name: 'mergeTable', start: [4], rowcount: 4 }",
            "{ name: 'mergeTable', start: [4], rowcount: 4 }",
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2 }");
            /*
        oneOperation = { name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'mergeTable', start: [4], rowcount: 4, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeTable', start: [4], rowcount: 4, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }

    // mergeTable and local insertRows operation (handleMergeCompInsertRows)
    // it('should calculate valid transformed mergeTable operation after local insertRows operation with special handling of property referenceRow', function () {
    @Test
    public void test01b() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 4 }",
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2 }",
            "{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 6 }",
            "{ name: 'mergeTable', start: [3], rowcount: 4 }");
            /*
        oneOperation = { name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 6, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02b() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 4 }",
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0 }",
            "{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 4 }",
            "{ name: 'mergeTable', start: [3], rowcount: 4 }");
            /*
        oneOperation = { name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', start: [3, 5], count: 3, referenceRow: 4, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'mergeTable', start: [3], rowcount: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03b() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2], rowcount: 4 }",
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2 }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 2 }",
            "{ name: 'mergeTable', start: [2], rowcount: 4 }");
            /*
        oneOperation = { name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04b() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [4], rowcount: 4 }",
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2 }",
            "{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2 }",
            "{ name: 'mergeTable', start: [4], rowcount: 4 }");
            /*
        oneOperation = { name: 'mergeTable', start: [4], rowcount: 4, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', start: [4, 1], count: 3, referenceRow: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'mergeTable', start: [4], rowcount: 4, opl: 1, osn: 1 }], transformedOps);
             */
    }

    // insertCells and local mergeParagraph operation (handleMergeCompInsertRows)
    // it('should calculate valid transformed insertCells operation after local mergeParagraph operation', function () {
    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [4, 1, 1], count: 3 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [4, 1, 1], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [4, 1, 1], count: 3 }",
            "{ name: 'mergeParagraph', start: [5], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [5], paralength: 6 }",
            "{ name: 'insertCells', start: [4, 1, 1], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [4, 1, 1], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [5], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 1]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 0], count: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 5, 2], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 1, 0], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 0], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 5, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 0]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 0, 0], count: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 0, 0], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 0, 0], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 0, 0]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 2, 0], count: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 2, 0], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 2, 0], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 0]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 3, 3], count: 1 }",
            "{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 3, 3], count: 1 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 3, 3], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 3]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 2, 2], count: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 2, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 2]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 2, 2]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2, 2]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 2, 2], count: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 1, 3, 2, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 2, 2]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 6 }",
            "{ name: 'insertCells', start: [0, 9, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 9, 2, 2, 2]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 2]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 2]);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6 }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 2]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 1], count: 3 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 2, 1, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 2, 4, 3], paralength: 6 }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 1], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 1], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 2, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 2, 4, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 1]);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 3], count: 3 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 2, 2, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 2, 2, 3], paralength: 6 }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 3], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 3], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 2, 2, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 2, 2, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 3]);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [4, 1, 2], count: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'insertCells', start: [3, 1, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [4, 1, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2]);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [2, 1, 2], count: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 7 }",
            "{ name: 'mergeTable', start: [1], rowcount: 7 }",
            "{ name: 'insertCells', start: [1, 8, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [2, 1, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 7, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 8, 2]);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [4, 1, 2], count: 3 }",
            "{ name: 'mergeTable', start: [5], rowcount: 6 }",
            "{ name: 'mergeTable', start: [5], rowcount: 6 }",
            "{ name: 'insertCells', start: [4, 1, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [4, 1, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4, 1, 2]);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2], count: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 5, 2], rowcount: 6 }",
            "{ name: 'insertCells', start: [3, 1, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 5, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2]);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2], count: 3 }",
            "{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6 }",
            "{ name: 'insertCells', start: [3, 1, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2]);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 3, 2], count: 1 }",
            "{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 3, 3, 2], rowcount: 6 }",
            "{ name: 'insertCells', start: [3, 3, 2], count: 1 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 3, 2], count: 1, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 3, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 3, 2]);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 2, 2], count: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'insertCells', start: [3, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 2, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 2, 2]);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'insertCells', start: [3, 1, 2, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 2, 2]);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6 }",
            "{ name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 2, 3, 2, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 2, 2]);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [3, 1, 1, 3, 2, 2], count: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'insertCells', start: [3, 1, 1, 3, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [3, 1, 1, 3, 2, 2], count: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3, 2, 2]);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'mergeTable', start: [0], rowcount: 6 }",
            "{ name: 'mergeTable', start: [0], rowcount: 6 }",
            "{ name: 'insertCells', start: [0, 9, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 9, 2, 2, 2]);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 2]);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 2]);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6 }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 2]);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 2, 2, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 2, 5, 3], rowcount: 6 }",
            "{ name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3 }");
            /*
                oneOperation = { name: 'insertCells', start: [1, 3, 2, 2, 2], count: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 2, 5, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 2, 2]);
             */
    }
}
