/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MergeParaInsertColumn {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [5], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [5], paralength: 6 }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [5], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2] , paralength: 6}",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2] , paralength: 6}",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 3, 2], paralength: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 3, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [3, 1, 3, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 4, 2], paralength: 6 }",
            "{ name: 'insertColumn', start: [3], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 3, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 4, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'mergeParagraph', start: [3, 3, 3, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 3, 4, 2], paralength: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 3, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 4, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'insertColumn', start: [3, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6 }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [0] , paralength: 6}",
            "{ name: 'mergeParagraph', start: [0], paralength: 6 }",
            "{ name: 'insertColumn', start: [0, 9, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 9, 2]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 4, 3], paralength: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 4, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 3]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [5], rowcount: 6 }",
            "{ name: 'mergeTable', start: [5], rowcount: 6 }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2] ,rowcount: 6}",
            "{ name: 'mergeTable', start: [3, 1, 3, 2], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 3, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [3, 1, 3, 2] ,rowcount: 6}",
            "{ name: 'mergeTable', start: [3, 1, 4, 2], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 3, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 4, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'mergeTable', start: [3, 3, 3, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 3, 4, 2], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 3, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 4, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6}",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3],tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [3, 1, 1, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [0], rowcount: 6 }",
            "{ name: 'mergeTable', start: [0], rowcount: 6 }",
            "{ name: 'insertColumn', start: [0, 9, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 9, 2, 3]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 3]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 3]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 1, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6}",
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 2, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 4, 3], rowcount: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 4, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6 }",
            "{ name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                oneOperation = { name: 'insertColumn', start: [1, 3, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 3]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [4], rowcount: 6 }",
            "[{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [4], rowcount: 6 }]",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
                // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('insertColumn');
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations[1].name).to.equal('mergeTable');
                expect(localActions[0].operations[1].start).to.deep.equal([4]);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0]).to.deep.equal(oneOperation);
             */
    }

    @Test
    public void test37A() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[{ name: 'mergeTable', start: [4], rowcount: 6 }, { name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo' }]",
            "[{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [4], rowcount: 6 }, { name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo' }]",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
                // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
                // -> also the insertText position must be adapted, even though a new operation is inserted before
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [
                    { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 },
                    { name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }
                ] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                    { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 },
                    { name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }
                ] }], localActions);
                expectOp([{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test37B() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[{ name: 'mergeTable', start: [4], rowcount: 6 }, { name: 'insertText', start: [4, 4, 1, 0, 0], text: 'ooo' }]",
            "[{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [4], rowcount: 6 }, { name: 'insertText', start: [4, 4, 1, 0, 0], text: 'ooo' }]",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
                // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
                // -> the insertText position must not be adapted
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [
                    { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 },
                    { name: 'insertText', start: [4, 4, 1, 0, 0], text: 'ooo', opl: 1, osn: 1 }
                ] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                    { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 },
                    { name: 'insertText', start: [4, 4, 1, 0, 0], text: 'ooo', opl: 1, osn: 1 }
                ] }], localActions);
                expectOp([{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test37C() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[{ name: 'mergeTable', start: [4], rowcount: 6 }, { name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo' }]",
            "[{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [4], rowcount: 6 }, { name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo' }]",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
                // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
                // -> also the insertText position in a following action must be increased
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [
                    { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] }
                ];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectActions([
                    { operations: [
                        { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                        { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] }
                ], localActions);
                expectOp([{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test37D() {
        TransformerTest.transform(
            "[{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp' }]",
            "[{ name: 'mergeTable', start: [4], rowcount: 6 }, { name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo' }]",
            "[{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [4], rowcount: 6 }, { name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo' }]",
            "[{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'insertText', start: [4, 7, 5, 0, 0], text: 'ppp' }]");
            /*
                // the local merge is in the same table: local mergeTable [4] and external insertColumn [4]
                // -> an additional internal operation: 'insertColumn [5]' before 'mergeTable [4]' is required
                // -> also the insertText position in a following action must be adapted
                var externalActions = [
                    { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp', opl: 1, osn: 1 }] }
                ];
                localActions = [
                    { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] }
                ];
                var transformedActions = testRunner.transformActions(externalActions, localActions);
                expectActions([
                    { operations: [
                        { name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                        { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'insertText', start: [4, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] }
                ], localActions);
                expectActions([
                    { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [4, 7, 5, 0, 0], text: 'ppp', opl: 1, osn: 1 }] }
                ], transformedActions);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6 }",
            "[{ name: 'insertColumn', start: [1, 2, 2, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6 }]",
            "{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                // the local merge is in the same table: local mergeTable [1, 2, 2, 4] and external insertColumn [1, 2, 2, 4]
                // -> an additional internal operation: 'insertColumn [1, 2, 2, 5]' before 'mergeTable [1, 2, 2, 4]' is required
                oneOperation = { name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('insertColumn');
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 5]);
                expect(localActions[0].operations[1].name).to.equal('mergeTable');
                expect(localActions[0].operations[1].start).to.deep.equal([1, 2, 2, 4]);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0]).to.deep.equal(oneOperation);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [3], rowcount: 6 }",
            "[{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [3], rowcount: 6 }]",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                // the local merge is in the previous table: local mergeTable [3] and external insertColumn [4]
                // 1. the external insertColumn operation must be modified, because of the locally already executed mergeTable [3] operation
                // 2. an additional internal operation: 'insertColumn [3]' before 'mergeTable [3]' is required
                oneOperation = { name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('insertColumn');
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[1].name).to.equal('mergeTable');
                expect(localActions[0].operations[1].start).to.deep.equal([3]);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6 }",
            "[{ name: 'insertColumn', start: [1, 2, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6 }]",
            "{ name: 'insertColumn', start: [1, 2, 2, 3], tableGrid: [1, 1, 2, 2], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
            /*
                // the local merge is in the previous table: local mergeTable [1, 2, 2, 3] and external insertColumn [1, 2, 2, 4]
                // 1. the external insertColumn operation must be modified, because of the locally already executed mergeTable [1, 2, 2, 3] operation
                // 2. an additional internal operation: 'insertColumn [1, 2, 2, 3]' before 'mergeTable [1, 2, 2, 3]' is required
                oneOperation = { name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('insertColumn');
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3]);
                expect(localActions[0].operations[1].name).to.equal('mergeTable');
                expect(localActions[0].operations[1].start).to.deep.equal([1, 2, 2, 3]);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([1, 2, 2, 3]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [4], rowcount: 6 }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[{ name: 'insertColumn', start: [5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [4], rowcount: 6 }]");
            /*
                // the merge is in the same table: local insertColumn [3] and external mergeTable [3]
                // -> an additional external operation: 'insertColumn [4]' before 'mergeTable [3]' is required
                oneOperation = { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0].name).to.equal('insertColumn');
                expect(transformedOps[0].start).to.deep.equal([5]);
                expect(transformedOps[1].name).to.equal('mergeTable');
                expect(transformedOps[1].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6 }",
            "{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[{ name: 'insertColumn', start: [1, 2, 2, 5], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6 }]");
            /*
                // the merge is in the same table: local insertColumn [1, 2, 2, 3] and external mergeTable [1, 2, 2, 3]
                // -> an additional external operation: 'insertColumn [1, 2, 2, 4]' before 'mergeTable [1, 2, 2, 3]' is required
                oneOperation = { name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0].name).to.equal('insertColumn');
                expect(transformedOps[0].start).to.deep.equal([1, 2, 2, 5]);
                expect(transformedOps[1].name).to.equal('mergeTable');
                expect(transformedOps[1].start).to.deep.equal([1, 2, 2, 4]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 6 }",
            "{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [3], rowcount: 6, rowcount: 6 }]");
            /*
                // the external merge is in the previous table: local insertColumn [4] and external mergeTable [3]
                // 1. the internal insertColumn operation must be modified, because of the external executed mergeTable [3] operation
                // 2. an additional external operation: 'insertColumn [3]' before 'mergeTable [3]' is required
                oneOperation = { name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0].name).to.equal('insertColumn');
                expect(transformedOps[0].start).to.deep.equal([3]);
                expect(transformedOps[1].name).to.equal('mergeTable');
                expect(transformedOps[1].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6 }",
            "{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1, 2, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "[{ name: 'insertColumn', start: [1, 2, 2, 3], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }, { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6 }]");
            /*
                // the external merge is in the previous table: local insertColumn [1, 2, 2, 4] and external mergeTable [1, 2, 2, 3]
                // 1. the internal insertColumn operation must be modified, because of the external executed mergeTable [1, 2, 2, 3] operation
                // 2. an additional external operation: 'insertColumn [1, 2, 2, 3]' before 'mergeTable [1, 2, 2, 3]' is required
                oneOperation = { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertColumn', start: [1, 2, 2, 4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0].name).to.equal('insertColumn');
                expect(transformedOps[0].start).to.deep.equal([1, 2, 2, 3]);
                expect(transformedOps[1].name).to.equal('mergeTable');
                expect(transformedOps[1].start).to.deep.equal([1, 2, 2, 3]);
             */
    }
}
