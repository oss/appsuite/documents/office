/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsMove {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8] }",    // local operations
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",     // external operations
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",     // expected local
            "{ name: 'setAttributes', start: [3, 3], end: [3, 7] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 3]);
                expect(oneOperation.end).to.deep.equal([3, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",     // external operations
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",     // expected local
            "{ name: 'setAttributes', start: [2, 5], end: [2, 9] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]);
                expect(oneOperation.end).to.deep.equal([2, 9]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 8] }",     // external operations
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 8] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 9] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 9]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 8]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [2, 6], end: [2, 6], to: [3, 0] }",     // external operations
            "{ name: 'move', start: [2, 6], end: [2, 6], to: [3, 0] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 7] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 6], end: [2, 6], to: [3, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 6]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 6]);
                expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 11] }",     // external operations
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 11] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 7] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 11], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 11]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 7] }",     // external operations
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 7] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 7], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 7]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 8] }",     // external operations
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 8] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 7] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 8]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 7] }",     // external operations
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 7] }",     // expected local
            "{ name: 'setAttributes', start: [2, 3], end: [2, 8] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 7], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3]);
                expect(oneOperation.end).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 7]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 2] }",     // external operations
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 2] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 10] }",     // external operations
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 10] }",     // expected local
            "{ name: 'setAttributes', start: [2, 3], end: [2, 7] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 10], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3]);
                expect(oneOperation.end).to.deep.equal([2, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 10]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [2, 10], end: [2, 10], to: [2, 0] }",     // external operations
            "{ name: 'move', start: [2, 10], end: [2, 10], to: [2, 0] }",     // expected local
            "{ name: 'setAttributes', start: [2, 5], end: [2, 9] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 10], end: [2, 10], to: [2, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]);
                expect(oneOperation.end).to.deep.equal([2, 9]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [3, 0] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [3, 0] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [3, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }",    // local operations
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0] }",     // external operations
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0] }",     // expected local
            "{ name: 'setAttributes', start: [2, 3, 3], end: [2, 3, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3, 3]);
                expect(oneOperation.end).to.deep.equal([2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }",    // local operations
            "{ name: 'move', start: [2, 2, 1, 0], end: [2, 2, 1, 0], to:  [3, 0] }",     // external operations
            "{ name: 'move', start: [2, 2, 1, 0], end: [2, 2, 1, 0], to:  [3, 0] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 2, 1, 0], end: [2, 2, 1, 0], to: [3, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3]);
                expect(oneOperation.end).to.deep.equal([2, 4, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 0]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 1, 0]);
                expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4] }",     // expected local
            "{ name: 'setAttributes', start: [2, 5, 3], end: [2, 5, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 3]);
                expect(oneOperation.end).to.deep.equal([2, 5, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 5] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 5] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3]);
                expect(oneOperation.end).to.deep.equal([2, 4, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 5]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 3, 1, 0] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 3, 1, 0] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 3, 1, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3]);
                expect(oneOperation.end).to.deep.equal([2, 4, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 3, 1, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 0] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 0] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3]);
                expect(oneOperation.end).to.deep.equal([2, 4, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 4, 3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 0] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 0] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3, 3], end: [2, 4, 3, 7] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3, 3]);
                expect(oneOperation.end).to.deep.equal([2, 4, 3, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 4, 3, 0]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 2] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 2] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3, 3], end: [2, 4, 3, 7] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3, 3]);
                expect(oneOperation.end).to.deep.equal([2, 4, 3, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 4, 3, 2]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 4] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 4] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 7] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3, 2]);
                expect(oneOperation.end).to.deep.equal([2, 4, 3, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 4, 3, 4]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 6] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 6] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 7] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3, 2]);
                expect(oneOperation.end).to.deep.equal([2, 4, 3, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 4, 3, 6]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6] }",    // local operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 7] }",     // external operations
            "{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 7] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 10], end: [1, 10], to: [2, 4, 3, 7], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3, 2]);
                expect(oneOperation.end).to.deep.equal([2, 4, 3, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 4, 3, 7]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 4] }",    // local operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // external operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // expected local
            "{ name: 'setAttributes', start: [1, 5], end: [1, 5] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(oneOperation.end).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 0], end: [2, 4, 2] }",    // local operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // external operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // expected local
            "{ name: 'setAttributes', start: [1, 5, 0], end: [1, 5, 2] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 0], end: [2, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 0]);
                expect(oneOperation.end).to.deep.equal([1, 5, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 2, 1], end: [2, 4, 2, 6] }",    // local operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // external operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // expected local
            "{ name: 'setAttributes', start: [1, 5, 2, 1], end: [1, 5, 2, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 2, 1], end: [2, 4, 2, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 1]);
                expect(oneOperation.end).to.deep.equal([1, 5, 2, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 2, 1], end: [2, 4, 2, 6] }",    // local operations
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [1, 5] }",     // external operations
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [1, 5] }",     // expected local
            "{ name: 'setAttributes', start: [2, 3, 2, 1], end: [2, 3, 2, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 2, 1], end: [2, 4, 2, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [1, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3, 2, 1]);
                expect(oneOperation.end).to.deep.equal([2, 3, 2, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4] }",    // local operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // external operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // expected local
            "{ name: 'setAttributes', start: [1, 5, 3, 2, 2], end: [1, 5, 3, 2, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 3, 2, 2]);
                expect(oneOperation.end).to.deep.equal([1, 5, 3, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4] }",    // local operations
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [1, 5] }",     // external operations
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [1, 5] }",     // expected local
            "{ name: 'setAttributes', start: [2, 3, 3, 2, 2], end: [2, 3, 3, 2, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [1, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 3, 3, 2, 2]);
                expect(oneOperation.end).to.deep.equal([2, 3, 3, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4] }",    // local operations
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1] }",     // external operations
            "{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 3], end: [2, 3], to: [2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3, 2, 2]);
                expect(oneOperation.end).to.deep.equal([2, 4, 3, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 3]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4] }",    // local operations
            "{ name: 'move', start: [2, 4, 3, 2], end: [2, 4, 3, 2], to: [1, 10] }",     // external operations
            "{ name: 'move', start: [2, 4, 3, 2], end: [2, 4, 3, 2], to: [1, 10] }",     // expected local
            "{ name: 'setAttributes', start: [1, 10, 2], end: [1, 10, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 4, 3, 2], end: [2, 4, 3, 2], to: [1, 10], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10, 2]);
                expect(oneOperation.end).to.deep.equal([1, 10, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 3, 2]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 10]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 2] }",    // local operations
            "{ name: 'move', start: [2, 4, 3, 2], end: [2, 4, 3, 2], to: [1, 10] }",     // external operations
            "{ name: 'move', start: [2, 4, 3, 2], end: [2, 4, 3, 2], to: [1, 10] }",     // expexted local
            "{ name: 'setAttributes', start: [1, 10], end: [1, 10] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2], end: [2, 4, 3, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 4, 3, 2], end: [2, 4, 3, 2], to: [1, 10], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 10]);
                expect(oneOperation.end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 3, 2]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 10]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4] }",    // local operations
            "{ name: 'move', start: [2, 4, 3, 1], end: [2, 4, 3 ,1], to: [1, 10] }",     // external operations
            "{ name: 'move', start: [2, 4, 3, 1], end: [2, 4, 3 ,1], to: [1, 10] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3, 1, 2], end: [2, 4, 3, 1, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3, 2, 2], end: [2, 4, 3, 2, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 4, 3, 1], end: [2, 4, 3, 1], to: [1, 10], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3, 1, 2]);
                expect(oneOperation.end).to.deep.equal([2, 4, 3, 1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 3, 1]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 10]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6] }",    // local operations
            "{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 3, 2, 2] }",     // external operations
            "{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 3, 2, 2] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 3, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3]);
                expect(oneOperation.end).to.deep.equal([2, 4, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 3, 2, 2]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6] }",    // local operations
            "{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 3] }",     // external operations
            "{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 3] }",     // expected local
            "{ name: 'setAttributes', start: [2, 5, 3], end: [2, 5, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 3]);
                expect(oneOperation.end).to.deep.equal([2, 5, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 3]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6] }",    // local operations
            "{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 4] }",     // external operations
            "{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 4] }",     // expected local
            "{ name: 'setAttributes', start: [2, 5, 3], end: [2, 5, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 3]);
                expect(oneOperation.end).to.deep.equal([2, 5, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6] }",    // local operations
            "{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 5] }",     // external operations
            "{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 5] }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 3], end: [2, 4, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [1, 5], end: [1, 5], to: [2, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 3]);
                expect(oneOperation.end).to.deep.equal([2, 4, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].to).to.deep.equal([2, 5]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [2], end: [4] }",    // local operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // external operations
            "{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5] }",     // expected local
            "{ name: 'setAttributes', start: [2], end: [4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2], end: [4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 4], end: [2, 4], to: [1, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.end).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
                expect(localActions[0].operations[0].to).to.deep.equal([1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
             */
    }
}
