/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveInsertChar {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0] }",
            "{ name: 'insertText', start: [3, 3], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [3, 5], end: [3, 5], to: [2, 0] }",
            "{ name: 'move', start: [3, 8], end: [3, 8], to: [2, 0] }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 5], end: [3, 5], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 8]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 8]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'move', start: [3, 7], end: [3, 7], to: [2, 0] }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 7]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 7]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 7] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 10] }",
            "{ name: 'insertText', start: [3, 3], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 7], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 10]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0] }",
            "{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0] }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 7, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1], text: 'ooo' }",
            "{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0] }",
            "{ name: 'move', start: [3, 5, 1, 2], end: [3, 5, 1, 2], to: [3, 5, 7, 0] }",
            "{ name: 'insertText', start: [3, 1], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 1], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 1, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 5, 1, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5, 7, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [2, 1], text: 'ooo' }",
            "{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0] }",
            "{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0] }",
            "{ name: 'insertText', start: [2, 1], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [2, 1], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 7, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 1]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [4, 1], text: 'ooo' }",
            "{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0] }",
            "{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0] }",
            "{ name: 'insertText', start: [4, 1], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [4, 1], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 7, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([4, 1]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1, 1, 0], text: 'ooo' }",
            "{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0] }",
            "{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0] }",
            "{ name: 'insertText', start: [3, 1, 1, 0], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 1, 1, 0], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 2], end: [3, 2, 1, 2], to: [3, 2, 7, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 7, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 0]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0] }",
            "{ name: 'insertText', start: [3, 5], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 4] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 7] }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 7], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11A() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3] }",
            "{ name: 'insertText', start: [3, 5], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertText', start: [3, 5], text: 'ooo', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11B() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 8] }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 8], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11C() {
        TransformerTest.transformText(
            "{ name: 'insertDrawing', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 4] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5] }",
            "{ name: 'insertDrawing', start: [3, 4], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 6] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 9] }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 6], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 9]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4, 1, 2], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 6] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 6] }",
            "{ name: 'insertText', start: [3, 4, 1, 2], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 4, 1, 2], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 6], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 1, 2]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [4, 6] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [4, 6] }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [4, 6], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 1, 2, 0] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 1, 2, 0] }",
            "{ name: 'insertText', start: [3, 1, 1, 2, 3], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 1, 2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 1, 2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 2, 3]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 1, 2, 5] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 1, 2, 8] }",
            "{ name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 1, 2, 5], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 1, 2, 8]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 2, 2]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 2, 2, 5] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 2, 2, 5] }",
            "{ name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 1, 1, 2, 2], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 1, 2, 2, 5], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 2, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 2, 2]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1, 1, 2], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0] }",
            "{ name: 'insertText', start: [3, 2, 1, 2], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 1, 1, 2], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 1, 2]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1, 1, 2], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3] }",
            "{ name: 'insertText', start: [3, 1, 1, 2], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 1, 1, 2], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 2]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [2, 2, 1, 2], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3] }",
            "{ name: 'insertText', start: [3, 3, 1, 2], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [2, 2, 1, 2], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 1, 2]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [2, 2, 1, 2], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 0, 3, 0] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 0, 3, 0] }",
            "{ name: 'insertText', start: [3, 3, 0, 3, 0, 1, 2], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [2, 2, 1, 2], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 0, 3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 0, 3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 3, 0, 1, 2]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [2, 2, 1, 1, 2, 1, 0], text: 'ooo' }",
            "{ name: 'move', start: [2, 2, 1, 1, 2], end: [2, 2, 1, 1, 2], to: [3, 3] }",
            "{ name: 'move', start: [2, 2, 1, 1, 2], end: [2, 2, 1, 1, 2], to: [3, 3] }",
            "{ name: 'insertText', start: [3, 3, 1, 0], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [2, 2, 1, 1, 2, 1, 0], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2, 1, 1, 2], end: [2, 2, 1, 1, 2], to: [3, 3], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 1, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 1, 1, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 1, 0]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [2, 2, 1, 1, 2, 1, 0], text: 'ooo' }",
            "{ name: 'move', start: [2, 2, 1, 1, 3], end: [2, 2, 1, 1, 3], to: [3, 3] }",
            "{ name: 'move', start: [2, 2, 1, 1, 3], end: [2, 2, 1, 1, 3], to: [3, 3] }",
            "{ name: 'insertText', start: [2, 2, 1, 1, 2, 1, 0], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [2, 2, 1, 1, 2, 1, 0], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2, 1, 1, 3], end: [2, 2, 1, 1, 3], to: [3, 3], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 1, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 1, 1, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 2, 1, 1, 2, 1, 0]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [2, 2, 0, 1, 2, 1, 0], text: 'ooo' }",
            "{ name: 'move', start: [2, 2, 1, 1, 2], end: [2, 2, 1, 1, 2], to: [3, 3] }",
            "{ name: 'move', start: [2, 2, 1, 1, 2], end: [2, 2, 1, 1, 2], to: [3, 3] }",
            "{ name: 'insertText', start: [2, 2, 0, 1, 2, 1, 0], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [2, 2, 0, 1, 2, 1, 0], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2, 1, 1, 2], end: [2, 2, 1, 1, 2], to: [3, 3], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 1, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 1, 1, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 2, 0, 1, 2, 1, 0]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 1, 2, 0, 2, 2, 1, 0] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 6, 1, 2, 0, 2, 2, 1, 0] }",
            "{ name: 'insertText', start: [3, 1], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 1], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 1, 2, 0, 2, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6, 1, 2, 0, 2, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1, 1, 0], text: 'ooo' }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 1, 2, 0, 2, 2, 1, 0] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 1, 2, 0, 2, 2, 1, 0] }",
            "{ name: 'insertText', start: [3, 1, 1, 0], text: 'ooo' }");
            /*
    oneOperation = { name: 'insertText', start: [3, 1, 1, 0], text: 'ooo', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3, 1, 2, 0, 2, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 1, 2, 0, 2, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 0]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformText(
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 7], opl: 1, osn: 1 }");
            /*
                oneOperation = { name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformText(
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }",
            "{ name: 'insertText', start: [3, 5], text: 'ooo', opl: 1, osn: 1 }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }");
            /*
                oneOperation = { name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', start: [3, 5], text: 'ooo', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformText(
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }",
            "{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 8], opl: 1, osn: 1 }");
            /*
                oneOperation = { name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', start: [3, 4], text: 'ooo', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformText(
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 }",
            "{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }",
            "{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 }");
            /*
                oneOperation = { name: 'move', start: [2, 2], end: [2, 2], to: [3, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertDrawing', start: [3, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [2, 2], end: [2, 2], to: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

}
