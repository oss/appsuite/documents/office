/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertComplexFieldInsertChar {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'insertComplexField', start: [1, 2], instruction: 'PAGE' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertComplexField', start: [1, 5], instruction: 'PAGE' }");
            /*
    oneOperation = { name: 'insertComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'PAGE' };
    localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }], localActions);
    expectOp([{ name: 'insertComplexField', opl: 1, osn: 1, start: [1, 5], instruction: 'PAGE' }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'insertComplexField', start: [1, 0], instruction: 'PAGE' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertText', start: [1, 2], text: 'ppp' }",
            "{ name: 'insertComplexField', start: [1, 0], instruction: 'PAGE' }");
            /*
    oneOperation = { name: 'insertComplexField', opl: 1, osn: 1, start: [1, 0], instruction: 'PAGE' };
    localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ppp' }] }], localActions);
    expectOp([{ name: 'insertComplexField', opl: 1, osn: 1, start: [1, 0], instruction: 'PAGE' }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertComplexField', start: [1, 2], instruction: 'PAGE' }",
            "{ name: 'insertComplexField', start: [1, 5], instruction: 'PAGE' }",
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }");
            /*
    oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
    localActions = [{ operations: [{ name: 'insertComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'PAGE' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComplexField', opl: 1, osn: 1, start: [1, 5], instruction: 'PAGE' }] }], localActions);
    expectOp([{ name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [1, 1], text: 'ppp' }",
            "{ name: 'insertComplexField', start: [1, 0], instruction: 'PAGE' }",
            "{ name: 'insertComplexField', start: [1, 0], instruction: 'PAGE' }",
            "{ name: 'insertText', start: [1, 2], text: 'ppp' }");
            /*
    oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 1], text: 'ppp' };
    localActions = [{ operations: [{ name: 'insertComplexField', opl: 1, osn: 1, start: [1, 0], instruction: 'PAGE' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertComplexField', opl: 1, osn: 1, start: [1, 0], instruction: 'PAGE' }] }], localActions);
    expectOp([{ name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ppp' }], transformedOps);
             */
    }
}
