/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveDeleteColumns {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 2, 4, 1, 0], end: [3, 2, 4, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 4, 1, 0], end: [3, 2, 4, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3 }",
            "{ name: 'move', start: [3, 2, 4, 1, 0], end: [3, 2, 4, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 2, 3, 1, 0], end: [3, 2, 3, 1, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 4, 1, 0], end: [3, 2, 4, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 3, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 3, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 4 }",
            "{ name: 'move', start: [3, 2, 3, 1, 0], end: [3, 2, 3, 1, 0], to: [3, 2, 4, 1, 0] }",
            "[]",
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 4 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 4, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 3, 1, 0], end: [3, 2, 3, 1, 0], to: [3, 2, 4, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 3, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 3, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 4, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // the move operation can be ignored, no delete operation required
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 4, 1, 0] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 2, 1, 0] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 4, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 4, 1, 0] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 3, 1, 0] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 3, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 4, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 3, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 1, 1, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 4, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 2, 1, 1] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 4, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 1, 1, 1, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 1, 1, 1, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 2, 1, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 3, 1, 1] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 3, 1, 1] }",
            "{ name: 'deleteColumns', start: [3, 2, 1, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 1, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 3, 1, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 3, 1, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 2, 1, 0]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 1, 1, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0],  end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0],  end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 1, 1, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 1, 1, 0]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3, 3, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 0]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 8] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 8] }",
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [3, 8], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 8]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [3, 0] }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [3, 0] }",
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'deleteColumns', start: [3, 5, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'deleteColumns', start: [3, 5, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 3, 1, 0], end: [3, 3, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 3, 1, 0], end: [3, 3, 1, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3, 1, 0], end: [3, 3, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'deleteColumns', start: [2, 0, 0, 1, 0, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 1, 0, 0]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'deleteColumns', start: [3, 3, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 0]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [2, 0, 0], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 0, 0]);

             */
    }

    @Test
    public void test27() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0, 1, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [2, 0, 1], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0, 1, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 0, 1]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 0, 1, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'deleteColumns', start: [2, 0, 0, 0, 0, 1], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 0, 1, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 0, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 0, 0, 0, 0, 1]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 4, 1, 1, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3, 4, 1, 1, 0, 1], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 4, 1, 1, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 4, 1, 1, 0, 1]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3, 3, 0, 1, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3, 3, 0, 1, 0, 1], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3, 3, 0, 1, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 3, 0, 1, 0, 1]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [3, 3, 3, 1, 0] }",
            "[]",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [3, 3, 3, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 3, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // the move operation can be ignored, no delete operation required
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0] }",
            "[]",
            "[{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }, { name: 'delete', start: [2, 0], end:[2, 0] }]");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(transformedOps.length).to.equal(2);
    expect(transformedOps[0].start).to.deep.equal([3]);
    expect(transformedOps[1].name).to.equal('delete');
    expect(transformedOps[1].start).to.deep.equal([2, 0]);
    expect(transformedOps[1].end).to.deep.equal([2, 0]);
    expect(transformedOps[1]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformText(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2, 2, 1, 0] }",
            "{ name: 'delete', start: [2, 0], end: [2, 0] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2, 2, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(2);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations[1].name).to.equal('delete');
    expect(localActions[0].operations[1].start).to.deep.equal([2, 0]); // sending the delete operation to the server instead of the move operation
    expect(localActions[0].operations[1].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(undefined);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [3, 3, 3, 1, 0] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [3, 3, 3, 1, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(transformedOps.length).to.equal(0); // the only external operation was removed, no delete operation required
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "[{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }, { name: 'delete', start: [2, 0], end: [2, 0] }]",
            "[]");
            /*
    oneOperation = { name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(2);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[1].name).to.equal('delete'); // sending additional delete operation to the server
    expect(localActions[0].operations[1].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[1].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(undefined);
    expect(transformedOps.length).to.equal(0); // the only external operation was removed, no delete operation required
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformText(
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2, 2, 1, 0] }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'delete', start: [2, 0], end: [2, 0] }");
            /*
    oneOperation = { name: 'move', start: [2, 0], end: [2, 0], to: [3, 2, 2, 1, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].name).to.equal('delete'); // applying locally a delete operation instead of the merge operation
    expect(transformedOps[0].start).to.deep.equal([2, 0]);
    expect(transformedOps[0].end).to.deep.equal([2, 0]);
    expect(transformedOps[0]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }
}
