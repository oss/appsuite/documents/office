/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class UpdateComplexFieldInsertPara {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'updateComplexField', start: [2, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [1] }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertTable', opl: 1, osn: 1, start: [2] }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [2, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertTable', start: [1] }");
            /*
    oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertTable', opl: 1, osn: 1, start: [1] }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertTable', start: [2] }");
            /*
    oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertTable', opl: 1, osn: 1, start: [2] }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'updateComplexField', start: [2, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
        oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
        localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }] }], localActions);
        expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [2, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertParagraph', start: [1] }");
            /*
        oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [1] };
        localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [2, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
        expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertParagraph', start: [2] }");
            /*
        oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [2] };
        localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
        expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }], transformedOps);
             */
    }
}
