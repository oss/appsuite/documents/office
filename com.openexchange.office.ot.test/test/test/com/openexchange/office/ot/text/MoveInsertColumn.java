/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveInsertColumn {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2, 3, 1, 0], end: [3, 2, 3, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 2, 4, 1, 0], end: [3, 2, 4, 1, 0], to: [2, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 3, 1, 0], end: [3, 2, 3, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 4, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 4, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2, 3, 1, 0], end: [3, 2, 3, 1, 0], to: [3, 3, 3, 1, 0] }",
            "{ name: 'move', start: [3, 2, 4, 1, 0], end: [3, 2, 4, 1, 0], to: [3, 3, 4, 1, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 3, 1, 0], end: [3, 2, 3, 1, 0], to: [3, 3, 3, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 4, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 4, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 4, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [3, 2, 3, 1, 0] }",
            "{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [3, 2, 4, 1, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 2, 1, 0], end: [3, 2, 2, 1, 0], to: [3, 2, 3, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 4, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 2, 1, 0] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 2, 1, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 5, 1, 0] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 6, 1, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 2, 5, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 6, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 2, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 2, 1, 1] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 2, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 3, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 4, 1, 1] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 3, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 4, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 3, 1, 1] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 4, 1, 1] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 3, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 4, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [2], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 2, 1, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 3, 1, 1] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 3, 1, 1] }",
            "{ name: 'insertColumn', start: [3, 2, 1, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 2, 1, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 3, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 3, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 1, 0]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0] }",
            "{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 3, 1, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 3, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 1, 1, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'insertColumn', start: [3, 1, 1, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 1, 1, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 0]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 3, 1, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'before', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 3, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'insertColumn', start: [3, 3, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'insertColumn', start: [3, 5, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'insertColumn', start: [3, 5, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 3, 1, 0], end: [3, 3, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 3, 1, 0], end: [3, 3, 1, 0], to: [2, 0] }",
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3, 1, 0], end: [3, 3, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'insertColumn', start: [2, 0, 0, 1, 0, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 1, 0, 0]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'insertColumn', start: [3, 3, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'insertColumn', start: [2, 0, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'insertColumn', start: [2, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 1]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 0, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'insertColumn', start: [2, 0, 0, 0, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 0, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 0, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 0, 0, 1]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 4, 1, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'insertColumn', start: [3, 4, 1, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 4, 1, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 1, 1, 0, 1]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [3, 3, 0, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'insertColumn', start: [3, 3, 0, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind' }");
            /*
    oneOperation = { name: 'insertColumn', start: [3, 3, 0, 1, 0, 1], tableGrid: [1, 1, 1], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 0, 1]);
             */
    }
}
