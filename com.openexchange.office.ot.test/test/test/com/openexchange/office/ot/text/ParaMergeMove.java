/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ParaMergeMove {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [0], paralength: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 0] }",
            "{ name: 'mergeParagraph', start: [0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 5] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [1], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([1]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [2, 5], end: [2, 5], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 5]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.paralength).to.equal(6);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 4 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(4);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [4], paralength: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [4], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [4], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([4]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 5] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 4 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.paralength).to.equal(4);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 6 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(6);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [4], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'mergeParagraph', start: [4], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [4], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([4]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [2, 2, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'mergeParagraph', start: [2, 1, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [2, 2, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 1, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 2, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'mergeParagraph', start: [3, 3, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 2, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2] }",
            "{ name: 'mergeParagraph', start: [3, 3, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 2, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3] }",
            "{ name: 'mergeParagraph', start: [3, 2, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 2, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'mergeParagraph', start: [3, 2, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 2, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0] }",
            "{ name: 'mergeParagraph', start: [3, 2, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 2, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([0, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [0, 2, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0] }",
            "{ name: 'mergeParagraph', start: [0, 3, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [0, 2, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([0, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([0, 3, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [2, 6, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4] }",
            "{ name: 'mergeParagraph', start: [2, 6, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [2, 6, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 4]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 6, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [2, 6, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8] }",
            "{ name: 'mergeParagraph', start: [2, 5, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [2, 6, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 8]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 5, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [2, 6, 2], paralength: 5 }",
            "{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0] }",
            "{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [2, 7, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [2, 6, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 8]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 8]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 7, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [2, 6, 2, 3, 2, 3, 4], paralength: 5 }",
            "{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0] }",
            "{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [2, 7, 2, 3, 2, 3, 4], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [2, 6, 2, 3, 2, 3, 4], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 8]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 8]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 7, 2, 3, 2, 3, 4]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [3, 2, 1, 0], end: [3, 2, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 2, 1, 0], end: [3, 2, 1, 0], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 0], end: [3, 2, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(5); // ! The outer drawing at [3, 2] remains, the inner drawing is moved
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 4 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(4); // ! The drawing at [3, 2] is moved away
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 7, 1, 1, 0], end: [3, 7, 1, 1, 0], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'move', start: [3, 7, 1, 1, 0], end: [3, 7, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(5); // the drawing at position [3, 0] was already there before the the move
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [3, 7, 1, 1, 0], end: [3, 7, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(5); // the drawing at position [3, 1] was already there before the the move
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 6] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 6] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 4 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 6], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(4);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [5], paralength: 5 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'mergeParagraph', start: [5], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [5], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [5], paralength: 5 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [5, 1] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [5, 1] }",
            "{ name: 'mergeParagraph', start: [5], paralength: 6 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [5], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [5, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([5, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5]);
    expect(oneOperation.paralength).to.equal(6);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 1, 1, 0], paralength: 5 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 0, 5], end: [3, 1, 1, 0, 5], to: [3, 1, 2, 1, 0] }",
            "{ name: 'mergeParagraph', start: [3, 1, 1, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 1, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 0, 5]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 0, 5]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 1, 1, 1], paralength: 5 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'mergeParagraph', start: [3, 1, 1, 1], paralength: 4 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 1, 1], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 1]);
    expect(oneOperation.paralength).to.equal(4);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 1, 1, 2], paralength: 5 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'mergeParagraph', start: [3, 1, 1, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 1, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], paralength: 5, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 1, 2, 0], paralength: 5 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 0, 5] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 2, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 0, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 2, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 5 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 2, 1]);
    expect(oneOperation.paralength).to.equal(6);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 5 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(5); // the drawing at [3, 1] was already there before
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 6, 2, 0] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 4 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 6, 2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.paralength).to.equal(4);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [3, 3, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [3, 4 ,0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'mergeParagraph', start: [3, 5, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'mergeParagraph', start: [2, 0, 0, 1, 0, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 1, 0, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'mergeParagraph', start: [3, 3, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [3, 3, 2, 0], end: [3, 3, 2, 0], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 3, 2, 0], end: [3, 3, 2, 0], to: [2, 0, 0, 1, 0] }",
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3, 2, 0], end: [3, 3, 2, 0], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3, 2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3, 2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0], paralength: 5 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [2, 0, 0], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0, 1, 0, 1], paralength: 5 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [2, 0, 1], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0, 1, 0, 1], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 1]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 0, 1, 0, 1], paralength: 5 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'mergeParagraph', start: [2, 0, 0, 0, 0, 1], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 0, 1, 0, 1], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 0, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 0, 0, 1]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 4, 1, 1, 0, 1], paralength: 5 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [3, 4, 1, 1, 0, 1], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 4, 1, 1, 0, 1], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 1, 1, 0, 1]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [3, 3, 0, 1, 0, 1], paralength: 5 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'mergeParagraph', start: [3, 3, 0, 1, 0, 1], paralength: 5 }");
            /*
    oneOperation = { name: 'mergeParagraph', start: [3, 3, 0, 1, 0, 1], paralength: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 0, 1]);
    expect(oneOperation.paralength).to.equal(5);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [0], rowcount: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 0] }",
            "{ name: 'mergeTable', start: [0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0, 1, 1, 1] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 5, 1, 1, 1] }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [1], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 5, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([1]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "{ name: 'move', start: [3, 0, 1, 1, 1], end: [3, 0, 1, 1, 1], to: [2, 0, 1, 1, 1] }",
            "{ name: 'move', start: [2, 5, 1, 1, 1], end: [2, 5, 1, 1, 1], to: [2, 0, 1, 1, 1] }",
            "{ name: 'mergeTable', start: [2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0, 1, 1, 1], end: [3, 0, 1, 1, 1], to: [2, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 1, 1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 5, 1, 1, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [4], rowcount: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'mergeTable', start: [4], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [4], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([4]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0, 1, 1, 1], end: [2, 0, 1, 1, 1], to: [4, 0] }",
            "{ name: 'move', start: [2, 0, 1, 1, 1], end: [2, 0, 1, 1, 1], to: [3, 0] }",
            "{ name: 'mergeTable', start: [2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0, 1, 1, 1], end: [2, 0, 1, 1, 1], to: [4, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 1, 1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0, 1, 1, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [2, 2, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'mergeTable', start: [2, 1, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [2, 2, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 1, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 2, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'mergeTable', start: [3, 3, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 2, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2] }",
            "{ name: 'mergeTable', start: [3, 3, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 2, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3] }",
            "{ name: 'mergeTable', start: [3, 2, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 2, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'mergeTable', start: [3, 2, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 2, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0] }",
            "{ name: 'mergeTable', start: [3, 2, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 2, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([0, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [0, 2, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0] }",
            "{ name: 'mergeTable', start: [0, 3, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [0, 2, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [0, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([0, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([0, 3, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test68() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [2, 6, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4] }",
            "{ name: 'mergeTable', start: [2, 6, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [2, 6, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 4], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 4]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 6, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test69() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [2, 6, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8] }",
            "{ name: 'mergeTable', start: [2, 5, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [2, 6, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [2, 8], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 8]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 5, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test70() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [2, 6, 2], rowcount: 5 }",
            "{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0] }",
            "{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0] }",
            "{ name: 'mergeTable', start: [2, 7, 2], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [2, 6, 2], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 8]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 8]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 7, 2]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test71() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [2, 6, 2, 3, 2, 3, 4], rowcount: 5 }",
            "{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0] }",
            "{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0] }",
            "{ name: 'mergeTable', start: [2, 7, 2, 3, 2, 3, 4], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [2, 6, 2, 3, 2, 3, 4], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 8], end: [2, 8], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 8]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 8]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 7, 2, 3, 2, 3, 4]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test72() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test73() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 7, 1, 1, 0], end: [3, 7, 1, 1, 0], to: [2, 0] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test74() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'move', start: [3, 7, 1, 1, 0], end: [3, 7, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test75() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 4, 1, 1, 1] }",
            "{ name: 'move', start: [3, 7, 1, 1, 0], end: [3, 7, 1, 1, 0], to: [3, 4, 1, 1, 1] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 4, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 7, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test76() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test77() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [5], rowcount: 5 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'mergeTable', start: [5], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [5], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test78() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [5], rowcount: 5 }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [6, 1, 1, 1, 1] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [5, 6, 1, 1, 1] }",
            "{ name: 'mergeTable', start: [5], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [5], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [6, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([5, 6, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test79() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0, 1, 1, 1], end: [3, 1, 1, 1, 0, 1, 1, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0, 1, 1, 1], end: [3, 1, 1, 1, 0, 1, 1, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0, 1, 1, 1], end: [3, 1, 1, 1, 0, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0, 1, 1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0, 1, 1, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 1]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test80() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5 }",
            "{ name: 'move', start: [3, 1, 1, 2, 0, 1, 1, 1], end: [3, 1, 1, 2, 0, 1, 1, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 5, 1, 1, 1], end: [3, 1, 1, 1, 5, 1, 1, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 2, 0, 1, 1, 1], end: [3, 1, 1, 2, 0, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 5, 1, 1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 5, 1, 1, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 1]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test81() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5 }",
            "{ name: 'move', start: [3, 1, 1, 3, 0, 1, 1, 1], end: [3, 1, 1, 3, 0, 1, 1, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 2, 0, 1, 1, 1], end: [3, 1, 1, 2, 0, 1, 1, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 1, 1, 1], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 3, 0, 1, 1, 1], end: [3, 1, 1, 3, 0, 1, 1, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 2, 0, 1, 1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 2, 0, 1, 1, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 1]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test82() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test83() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [4, 1, 2, 1, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 6, 2, 1, 0] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [4, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test84() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [5, 1, 2, 1, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [4, 1, 2, 1, 0] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [5, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test85() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3], rowcount: 5 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1] }",
            "{ name: 'mergeTable', start: [3], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test86() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'mergeTable', start: [3, 3, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test87() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test88() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test89() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'mergeTable', start: [3, 5, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test90() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'mergeTable', start: [3, 5, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test91() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test92() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test93() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test94() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'mergeTable', start: [2, 0, 0, 1, 0, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 1, 0, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test95() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'mergeTable', start: [3, 3, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test96() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0], rowcount: 5 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'mergeTable', start: [2, 0, 0], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test97() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0, 1, 0, 1], rowcount: 5 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'mergeTable', start: [2, 0, 1], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0, 1, 0, 1], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 1]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test98() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 0, 1, 0, 1], rowcount: 5 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'mergeTable', start: [2, 0, 0, 0, 0, 1], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 0, 1, 0, 1], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 0, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 0, 0, 1]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test99() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 4, 1, 1, 0 ,1], rowcount: 5 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'mergeTable', start: [3, 4, 1, 1, 0, 1], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 4, 1, 1, 0, 1], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 1, 1, 0, 1]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }

    @Test
    public void test100() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [3, 3, 0, 1, 0, 1], rowcount: 5 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'mergeTable', start: [3, 3, 0, 1, 0, 1], rowcount: 5 }");
            /*
    oneOperation = { name: 'mergeTable', start: [3, 3, 0, 1, 0, 1], rowcount: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 0, 1]);
    expect(oneOperation.rowcount).to.equal(5);
             */
    }
}
