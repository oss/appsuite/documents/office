/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveInsertRows {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 5, 1, 1, 0], end: [3, 5, 1, 1, 0], to: [2, 0] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 5, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 4, 1, 1, 1] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [3, 5, 1, 1, 0], end: [3, 5, 1, 1, 0], to: [3, 4, 1, 1, 1] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 5, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0] }",
            "{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 4, 1, 1, 0], end: [3, 4, 1, 1, 0], to: [3, 4, 2, 1, 0] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1, 1, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'insertRows', start: [3, 1, 1, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1, 1, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 0, 1]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 4, 2, 1, 0] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1] }",
            "{ name: 'move', start: [3, 4, 1, 1, 0], end: [3, 4, 1, 1, 0], to: [2, 1] }",
            "{ name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'insertRows', start: [3, 3, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 1]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'insertRows', start: [3, 5, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0, 1]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'insertRows', start: [3, 5, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0, 1]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'insertRows', start: [2, 0, 0, 1, 0, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 1, 0, 0, 1]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'insertRows', start: [3, 3, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 1]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'insertRows', start: [2, 0, 0, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 1]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1, 0, 1, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'insertRows', start: [2, 0, 1, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1, 0, 1, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 1, 1]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 0, 1, 0, 1, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'insertRows', start: [2, 0, 0, 0, 0, 1, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 0, 1, 0, 1, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 0, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 0, 0, 1, 1]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 4, 1, 1, 0, 1, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'insertRows', start: [3, 4, 1, 1, 0, 1, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 4, 1, 1, 0, 1, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 1, 1, 0, 1, 1]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [3, 3, 0, 1, 0, 1, 1], count: 3, referenceRow: 0 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'insertRows', start: [3, 3, 0, 1, 0, 1, 1], count: 3, referenceRow: 0 }");
            /*
    oneOperation = { name: 'insertRows', start: [3, 3, 0, 1, 0, 1, 1], count: 3, referenceRow: 0, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 0, 1, 1]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 2, 1], count: 3 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 2, 4, 1, 0], end: [3, 2, 4, 1, 0], to: [2, 0] }",
            "{ name: 'insertCells', start: [3, 2, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 2, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 4, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 4, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 1]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 2, 1], count: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0] }",
            "{ name: 'insertCells', start: [3, 2, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 2, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 1]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 2, 1], count: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'insertCells', start: [3, 2, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 2, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 1]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 0, 1], count: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 4, 1, 1] }",
            "{ name: 'insertCells', start: [3, 0, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 0, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 0, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 4, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 0, 1]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 4, 1, 1] }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 4, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 2, 1], count: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'insertCells', start: [3, 2, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 2, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 1]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1] }",
            "{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 4, 1, 1] }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 2, 1, 1, 0], end: [3, 2, 1, 1, 0], to: [3, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 4, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1] }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 2, 1, 1, 0], end: [4, 2, 1, 1, 0], to: [4, 1, 1, 1, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 2, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 1, 1, 1, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0] }",
            "{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0] }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [3, 0, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 4, 1, 0], end: [3, 1, 4, 1, 0], to: [3, 1, 5, 1, 0] }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 4, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 4, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 5, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 1, 1, 0, 0, 1], count: 3 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'insertCells', start: [3, 1, 1, 0, 0, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 1, 1, 0, 0, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1, 0, 0, 1]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 1, 1, 1, 0, 1, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0] }",
            "{ name: 'insertCells', start: [3, 1, 2, 1, 0, 1, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 1, 1, 1, 0, 1, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 2, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 2, 1, 0, 1, 1, 1]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 5, 1, 0] }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 1, 2, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1, 5, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1] }",
            "{ name: 'move', start: [3, 1, 4, 1, 0], end: [3, 1, 4, 1, 0], to: [2, 1] }",
            "{ name: 'insertCells', start: [3, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1, 1, 1, 0], end: [3, 1, 1, 1, 0], to: [2, 1], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 4, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1, 4, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 1, 1]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'insertCells', start: [3, 3, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0] }",
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1, 1]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1, 1]);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0] }",
            "{ name: 'insertCells', start: [3, 5, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0, 1, 1]);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4] }",
            "{ name: 'insertCells', start: [3, 5, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 4], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 4]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 0, 1, 1]);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5] }",
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 5], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1, 1]);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0] }",
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [4, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1, 1]);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0] }",
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [3, 3, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 3, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 0, 1, 1]);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0] }",
            "{ name: 'insertCells', start: [2, 0, 0, 1, 0, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 1, 0, 0, 1, 1]);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0] }",
            "{ name: 'insertCells', start: [3, 3, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 3], end: [3, 3], to: [2, 0, 0, 1, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 1, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 1]);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'insertCells', start: [2, 0, 0, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 1, 1]);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 0, 1, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'insertCells', start: [2, 0, 1, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 0, 1, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 1, 1, 1]);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 0, 1, 0, 1, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0] }",
            "{ name: 'insertCells', start: [2, 0, 0, 0, 0, 1, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 0, 1, 0, 1, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0, 0, 0, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 0, 0, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 0, 0, 0, 0, 1, 1, 1]);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 4, 1, 1, 0, 1, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'insertCells', start: [3, 4, 1, 1, 0, 1, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 4, 1, 1, 0, 1, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 4, 1, 1, 0, 1, 1, 1]);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [3, 3, 0, 1, 0, 1, 1, 1], count: 3 }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0] }",
            "{ name: 'insertCells', start: [3, 3, 0, 1, 0, 1, 1, 1], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', start: [3, 3, 0, 1, 0, 1, 1, 1], count: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 4, 0, 1, 0], end: [3, 4, 0, 1, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 4, 0, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 3, 0, 1, 0, 1, 1, 1]);
             */
    }
}
