/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class UpdateComplexFieldDelete {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 6], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [3, 5], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 6], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 5], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 1], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [3, 1], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 6], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 5] }",
            "{ name: 'delete', start: [3, 2], end: [3, 5] }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 6], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 4, 1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [3, 3, 1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 4, 1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 3, 1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [4, 4], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [4, 4], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [4, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [4, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "[]");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 5] }",
            "{ name: 'delete', start: [3, 2], end: [3, 5] }",
            "[]");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 0], end: [3, 2] }",
            "{ name: 'delete', start: [3, 0], end: [3, 2] }",
            "[]");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 0], end: [3, 5] }",
            "{ name: 'delete', start: [3, 0], end: [3, 5] }",
            "[]");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc' }",
            "{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc' }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3] }",
            "{ name: 'delete', start: [3] }",
            "[]");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'delete', start: [3], opl: 1, osn: 1 }] }], localActions);
    expectOp([], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [3, 6], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [3, 5], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }");
            /*
    oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 6], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 5], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [3, 1], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [3, 1], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }");
            /*
    oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 1], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 2], end: [3, 5] }",
            "{ name: 'updateComplexField', start: [3, 6], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 6], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [3, 4, 1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [3, 3, 1, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }");
            /*
    oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 4, 1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 3, 1, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [4, 4], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [4, 4], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }");
            /*
    oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [4, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [4, 4], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 2], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "[]",
            "{ name: 'delete', start: [3, 2], end: [3, 2] }");
            /*
    oneOperation = { name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"', _REMOVED_OPERATION_: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 2], end: [3, 5] }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "[]",
            "{ name: 'delete', start: [3, 2], end: [3, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"', _REMOVED_OPERATION_: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 2], end: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 0], end: [3, 2] }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "[]",
            "{ name: 'delete', start: [3, 0], end: [3, 2] }");
            /*
    oneOperation = { name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"', _REMOVED_OPERATION_: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 0], end: [3, 2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 0], end: [3, 5] }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "[]",
            "{ name: 'delete', start: [3, 0], end: [3, 5] }");
            /*
    oneOperation = { name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"', _REMOVED_OPERATION_: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 0], end: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc' }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc' }");
            /*
    oneOperation = { name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'delete', start: [3, 0], end: [3, 5], target: 'abc', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3] }",
            "{ name: 'updateComplexField', start: [3, 2], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "[]",
            "{ name: 'delete', start: [3] }");
            /*
    oneOperation = { name: 'delete', start: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [3, 2], instruction: 'DATE·\\@ "DD-MMM-YY"', _REMOVED_OPERATION_: 1 }] }], localActions);
    expectOp([{ name: 'delete', start: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }
}
