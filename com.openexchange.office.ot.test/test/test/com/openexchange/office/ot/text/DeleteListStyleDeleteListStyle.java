/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class DeleteListStyleDeleteListStyle {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'deleteListStyle', listStyleId: 'L1' }",
            "{ name: 'deleteListStyle', listStyleId: 'L2' }",
            "{ name: 'deleteListStyle', listStyleId: 'L2' }",
            "{ name: 'deleteListStyle', listStyleId: 'L1' }");
            /*
                oneOperation = { name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'deleteListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'deleteListStyle', listStyleId: 'L1' }",
            "{ name: 'deleteListStyle', listStyleId: 'L1' }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1, _REMOVED_OPERATION_: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

}
