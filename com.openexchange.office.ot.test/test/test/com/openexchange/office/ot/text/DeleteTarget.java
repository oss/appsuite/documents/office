/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class DeleteTarget {

    @Test
    public void test01a() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_100' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_100' }",
            "[]");
    }

    @Test
    public void test01b() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100' }");
    }

    @Test
    public void test01c() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [0, 2], text: 'a' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'insertText', start: [0, 2], text: 'a' }");
    }

/*
    // insertText and local deleteHeaderFooter operation
    it('should calculate valid transformed insertText operation after local deleteHeaderFooter operation', function () {

        oneOperation = { name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertText', start: [0, 2], text: 'a', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'insertText', start: [0, 2], text: 'a', opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test02a() {
        TransformerTest.transformText(
            "{ name: 'insertParagraph', start: [3], target: 'rid_100' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_100' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_100' }",
            "[]");
    }

    @Test
    public void test02b() {
        TransformerTest.transformText(
            "{ name: 'insertParagraph', start: [3], target: 'rid_100' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'insertParagraph', start: [3], target: 'rid_100' }");
    }

    @Test
    public void test02c() {
        TransformerTest.transformText(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'insertParagraph', start: [3] }");
    }

/*
    // insertParagraph and local deleteHeaderFooter operation
    it('should calculate valid transformed insertParagraph operation after local deleteHeaderFooter operation', function () {

        oneOperation = { name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertParagraph', start: [3], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test03a() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'rid_100' }",
            "{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100' }",
            "[]",
            "{ name: 'deleteHeaderFooter', id: 'rid_100' }");
    }

    @Test
    public void test03b() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100' }",
            "{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }");
    }

    @Test
    public void test03c() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'insertText', start: [0, 2], text: 'a' }",
            "{ name: 'insertText', start: [0, 2], text: 'a' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }");
    }

/*
    // deleteHeaderFooter and local insertText
    it('should calculate valid transformed insertText operation after external deleteHeaderFooter operation', function () {

        oneOperation = { name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [] }], localActions);
        expectOp([{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', target: 'rid_100', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertText', start: [0, 2], text: 'a', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test04a() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'rid_100' }",
            "{ name: 'insertParagraph', start: [3], target: 'rid_100' }",
            "[]",
            "{ name: 'deleteHeaderFooter', id: 'rid_100' }");
    }

    @Test
    public void test04b() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'insertParagraph', start: [3], target: 'rid_100' }",
            "{ name: 'insertParagraph', start: [3], target: 'rid_100' }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }");
    }

    @Test
    public void test04c() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'deleteHeaderFooter', id: 'rid_101' }");
    }

/*
    // deleteHeaderFooter and local insertParagraph
    it('should calculate valid transformed insertParagraph operation after external deleteHeaderFooter operation', function () {

        oneOperation = { name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [] }], localActions);
        expectOp([{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertParagraph', start: [3], target: 'rid_100', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'deleteHeaderFooter', id: 'rid_101', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'deleteHeaderFooter', id: 'rid_100', opl: 1, osn: 1 }], transformedOps);
    });
*/
}
