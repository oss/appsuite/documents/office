/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class UpdateComplexFieldInsertRows {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 8, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 11, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 5, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 1, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 1, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [1, 2], count: 3, target: 'abc' }",
            "{ name: 'insertRows', start: [1, 2], count: 3, target: 'abc' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [0, 2], count: 3 }",
            "{ name: 'insertRows', start: [0, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [2, 2], count: 3 }",
            "{ name: 'insertRows', start: [2, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 8, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 11, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 5, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 1, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 1, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [1, 2], count: 3, target: 'abc' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [1, 2], count: 3, target: 'abc' }");
            /*
    oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'abc' }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [0, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [0, 2], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [2, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertRows', start: [2, 2], count: 3 }");
            /*
    oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }], transformedOps);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 7, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [1, 2, 5], count: 3 }",
            "{ name: 'insertCells', start: [1, 2, 5], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3, target: 'abc' }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3, target: 'abc' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [0, 2, 2], count: 3 }",
            "{ name: 'insertCells', start: [0, 2, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 3 }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }");
            /*
    oneOperation = { name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' };
    localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }], localActions);
    expectOp([{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }], transformedOps);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2, 7, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }], transformedOps);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [1, 2, 5], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [1, 2, 5], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }], transformedOps);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }], transformedOps);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3, target: 'abc' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3, target: 'abc' }");
            /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'abc' }], transformedOps);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [0, 2, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [0, 2, 2], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [2, 2, 2], count: 3 }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'updateComplexField', start: [1, 2, 4, 2, 3], instruction: 'DATE \\\\@ \"DD-MMM-YY\"' }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 3 }");
            /*
    oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 };
    localActions = [{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expectAction([{ operations: [{ name: 'updateComplexField', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], instruction: 'DATE·\\@ "DD-MMM-YY"' }] }], localActions);
    expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }], transformedOps);
             */
    }
}
