/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class OTManager {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "[{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } } }," +
            "{ name: 'insertFontDescription' }," +
            "{ name: 'insertStyleSheet', type: 'character' }," +
            "{ name: 'changeStyleSheet', type: 'character' }," +
            "{ name: 'deleteStyleSheet', type: 'character' }," +
            "{ name: 'insertTheme' }," +
            "{ name: 'insertListStyle', listStyleId: 'L1' }," +
            "{ name: 'deleteListStyle', listStyleId: 'L1' }," +
            "{ name: 'insertParagraph', start: [1] }," +
            "{ name: 'delete', start: [1], end: [1] }," +
            "{ name: 'insertText', start: [1, 0], text: 'a' }," +
            "{ name: 'insertTab', start: [1, 1] }," +
            "{ name: 'insertBookmark', start: [1, 2] }," +
            "{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1' }," +
            "{ name: 'insertComment', start: [1, 4], uid: '1234', id: 'cmt1' }," +
            "{ name: 'insertField', start: [1, 5], instruction: 'PAGE' }," +
            "{ name: 'updateField', start: [1, 5], instruction: 'PAGE' }," +
            "{ name: 'insertComplexField', start: [1, 6], instruction: 'PAGE' }," +
            "{ name: 'updateComplexField', start: [1, 6], instruction: 'PAGE' }," +
            "{ name: 'insertDrawing', start: [1, 7], type: 'shape' }," +
            "{ name: 'move', start: [1, 7], end: [1, 7], to: [1, 8] }," +
            "{ name: 'splitParagraph', start: [1, 1] }," +
            "{ name: 'mergeParagraph', start: [1], paralength: 1 }," +
            "{ name: 'insertHardBreak', start: [1, 0] }," +
            "{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } } }," +
            "{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] } }," +
            "{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true }," +
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind' }," +
            "{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0 }," +
            "{ name: 'insertCells', start: [1, 1, 1], count: 1 }," +
            "{ name: 'splitTable', start: [1, 1] }," +
            "{ name: 'insertParagraph', start: [2] }," +
            "{ name: 'delete', start: [2], end: [2] }," +
            "{ name: 'mergeTable', start: [1], rowcount: 1 }," +
            "{ name: 'delete', start: [1], end: [1] }," +
            "{ name: 'insertHeaderFooter', id: 'hf1', type: 'header_default' }," +
            "{ name: 'deleteHeaderFooter', id: 'hf1' }]",
            "[{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } } }," +
            "{ name: 'insertFontDescription' }," +
            "{ name: 'insertStyleSheet', type: 'paragraph' }," +
            "{ name: 'changeStyleSheet', type: 'paragraph' }," +
            "{ name: 'deleteStyleSheet', type: 'paragraph' }," +
            "{ name: 'insertTheme' }," +
            "{ name: 'insertListStyle', listStyleId: 'L2' }," +
            "{ name: 'deleteListStyle', listStyleId: 'L2' }," +
            "{ name: 'insertParagraph', start: [3] }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'insertText', start: [3, 0], text: 'a' }," +
            "{ name: 'insertTab', start: [3, 1] }," +
            "{ name: 'insertBookmark', start: [3, 2] }," +
            "{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1' }," +
            "{ name: 'insertComment', start: [3, 4], uid: '1234', id: 'cmt2' }," +
            "{ name: 'insertField', start: [3, 5], instruction: 'PAGE' }," +
            "{ name: 'updateField', start: [3, 5], instruction: 'PAGE' }," +
            "{ name: 'insertComplexField', start: [3, 6], instruction: 'PAGE' }," +
            "{ name: 'updateComplexField', start: [3, 6], instruction: 'PAGE' }," +
            "{ name: 'insertDrawing', start: [3, 7], type: 'shape' }," +
            "{ name: 'move', start: [3, 7], end: [3, 7], to: [3, 8] }," +
            "{ name: 'splitParagraph', start: [3, 1] }," +
            "{ name: 'mergeParagraph', start: [3], paralength: 1 }," +
            "{ name: 'insertHardBreak', start: [3, 0] }," +
            "{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } } }," +
            "{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] } }," +
            "{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true }," +
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind' }," +
            "{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0 }," +
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }," +
            "{ name: 'splitTable', start: [3, 1] }," +
            "{ name: 'insertParagraph', start: [3] }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'mergeTable', start: [3], rowcount: 1 }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'insertHeaderFooter', id: 'hf2', type: 'footer_first' }," +
            "{ name: 'deleteHeaderFooter', id: 'hf2' }]",
            "[{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } } }," +
            "{ name: 'insertFontDescription' }," +
            "{ name: 'insertStyleSheet', type: 'paragraph' }," +
            "{ name: 'changeStyleSheet', type: 'paragraph' }," +
            "{ name: 'deleteStyleSheet', type: 'paragraph' }," +
            "{ name: 'insertTheme' }," +
            "{ name: 'insertListStyle', listStyleId: 'L2' }," +
            "{ name: 'deleteListStyle', listStyleId: 'L2' }," +
            "{ name: 'insertParagraph', start: [3] }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'insertText', start: [3, 0], text: 'a' }," +
            "{ name: 'insertTab', start: [3, 1] }," +
            "{ name: 'insertBookmark', start: [3, 2] }," +
            "{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1' }," +
            "{ name: 'insertComment', start: [3, 4], uid: '1234', id: 'cmt2' }," +
            "{ name: 'insertField', start: [3, 5], instruction: 'PAGE' }," +
            "{ name: 'updateField', start: [3, 5], instruction: 'PAGE' }," +
            "{ name: 'insertComplexField', start: [3, 6], instruction: 'PAGE' }," +
            "{ name: 'updateComplexField', start: [3, 6], instruction: 'PAGE' }," +
            "{ name: 'insertDrawing', start: [3, 7], type: 'shape' }," +
            "{ name: 'move', start: [3, 7], end: [3, 7], to: [3, 8] }," +
            "{ name: 'splitParagraph', start: [3, 1] }," +
            "{ name: 'mergeParagraph', start: [3], paralength: 1 }," +
            "{ name: 'insertHardBreak', start: [3, 0] }," +
            "{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } } }," +
            "{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] } }," +
            "{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true }," +
            "{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind' }," +
            "{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0 }," +
            "{ name: 'insertCells', start: [3, 1, 1], count: 1 }," +
            "{ name: 'splitTable', start: [3, 1] }," +
            "{ name: 'insertParagraph', start: [3] }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'mergeTable', start: [3], rowcount: 1 }," +
            "{ name: 'delete', start: [3], end: [3] }," +
            "{ name: 'insertHeaderFooter', id: 'hf2', type: 'footer_first' }," +
            "{ name: 'deleteHeaderFooter', id: 'hf2' }]",
            "[{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } } }," +
            "{ name: 'insertFontDescription' }," +
            "{ name: 'insertStyleSheet', type: 'character' }," +
            "{ name: 'changeStyleSheet', type: 'character' }," +
            "{ name: 'deleteStyleSheet', type: 'character' }," +
            "{ name: 'insertTheme' }," +
            "{ name: 'insertListStyle', listStyleId: 'L1' }," +
            "{ name: 'deleteListStyle', listStyleId: 'L1' }," +
            "{ name: 'insertParagraph', start: [1] }," +
            "{ name: 'delete', start: [1], end: [1] }," +
            "{ name: 'insertText', start: [1, 0], text: 'a' }," +
            "{ name: 'insertTab', start: [1, 1] }," +
            "{ name: 'insertBookmark', start: [1, 2] }," +
            "{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1' }," +
            "{ name: 'insertComment', start: [1, 4], uid: '1234', id: 'cmt1' }," +
            "{ name: 'insertField', start: [1, 5], instruction: 'PAGE' }," +
            "{ name: 'updateField', start: [1, 5], instruction: 'PAGE' }," +
            "{ name: 'insertComplexField', start: [1, 6], instruction: 'PAGE' }," +
            "{ name: 'updateComplexField', start: [1, 6], instruction: 'PAGE' }," +
            "{ name: 'insertDrawing', start: [1, 7], type: 'shape' }," +
            "{ name: 'move', start: [1, 7], end: [1, 7], to: [1, 8] }," +
            "{ name: 'splitParagraph', start: [1, 1] }," +
            "{ name: 'mergeParagraph', start: [1], paralength: 1 }," +
            "{ name: 'insertHardBreak', start: [1, 0] }," +
            "{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } } }," +
            "{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] } }," +
            "{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true }," +
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind' }," +
            "{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0 }," +
            "{ name: 'insertCells', start: [1, 1, 1], count: 1 }," +
            "{ name: 'splitTable', start: [1, 1] }," +
            "{ name: 'insertParagraph', start: [2] }," +
            "{ name: 'delete', start: [2], end: [2] }," +
            "{ name: 'mergeTable', start: [1], rowcount: 1 }," +
            "{ name: 'delete', start: [1], end: [1] }," +
            "{ name: 'insertHeaderFooter', id: 'hf1', type: 'header_default' }," +
            "{ name: 'deleteHeaderFooter', id: 'hf1' }]");

/*
                var externalActions = [
                    { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [1, 0], text: 'a', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTab', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertBookmark', start: [1, 2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertComment', start: [1, 4], uid: '1234', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertComplexField', start: [1, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateComplexField', start: [1, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertDrawing', start: [1, 7], type: 'shape', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'move', start: [1, 7], end: [1, 7], to: [1, 8], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitParagraph', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeParagraph', start: [1], paralength: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHardBreak', start: [1, 0], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertCells', start: [1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitTable', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [2], end: [2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeTable', start: [1], rowcount: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHeaderFooter', id: 'hf1', type: 'header_default', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }
                ];
                var localActions = [
                    { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [3, 0], text: 'a', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTab', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertBookmark', start: [3, 2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertComment', start: [3, 4], uid: '1234', id: 'cmt2', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertComplexField', start: [3, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateComplexField', start: [3, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertDrawing', start: [3, 7], type: 'shape', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'move', start: [3, 7], end: [3, 7], to: [3, 8], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeParagraph', start: [3], paralength: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHardBreak', start: [3, 0], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitTable', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeTable', start: [3], rowcount: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHeaderFooter', id: 'hf2', type: 'footer_first', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteHeaderFooter', id: 'hf2', opl: 1, osn: 1 }] }
                ];
                var transformedActions = testRunner.transformActions(externalActions, localActions);
                expectActions([
                    { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p2: 10 } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'changeStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteStyleSheet', type: 'paragraph', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteListStyle', listStyleId: 'L2', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [3, 0], text: 'a', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTab', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertBookmark', start: [3, 2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRange', start: [3, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertComment', start: [3, 4], uid: '1234', id: 'cmt2', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateField', start: [3, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertComplexField', start: [3, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateComplexField', start: [3, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertDrawing', start: [3, 7], type: 'shape', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'move', start: [3, 7], end: [3, 7], to: [3, 8], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeParagraph', start: [3], paralength: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHardBreak', start: [3, 0], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'setAttributes', start: [3], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTable', start: [3], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [3, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertColumn', start: [3], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteColumns', start: [3], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertCells', start: [3, 1, 1], count: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitTable', start: [3, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeTable', start: [3], rowcount: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [3], end: [3], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHeaderFooter', id: 'hf2', type: 'footer_first', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteHeaderFooter', id: 'hf2', opl: 1, osn: 1 }] }
                ], localActions);
                expectActions([
                    { operations: [{ name: 'setDocumentAttributes', attrs: { f1: { p1: 10 } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertFontDescription', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'changeStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteStyleSheet', type: 'character', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTheme', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteListStyle', listStyleId: 'L1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [1, 0], text: 'a', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTab', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertBookmark', start: [1, 2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRange', start: [1, 3], position: 'start', type: 'comment', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertComment', start: [1, 4], uid: '1234', id: 'cmt1', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateField', start: [1, 5], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertComplexField', start: [1, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'updateComplexField', start: [1, 6], instruction: 'PAGE', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertDrawing', start: [1, 7], type: 'shape', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'move', start: [1, 7], end: [1, 7], to: [1, 8], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitParagraph', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeParagraph', start: [1], paralength: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHardBreak', start: [1, 0], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'setAttributes', start: [1], attrs: { paragraph: { alignment: 'center' } }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertTable', start: [1], attrs: { tableGrid: [1, 1, 1] }, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [1, 0], count: 3, insertDefaultCells: true, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 1, 1], gridPosition: 0, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteColumns', start: [1], startGrid: 0, endGrid: 0, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertCells', start: [1, 1, 1], count: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'splitTable', start: [1, 1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [2], end: [2], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'mergeTable', start: [1], rowcount: 1, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertHeaderFooter', id: 'hf1', type: 'header_default', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }
                ], transformedActions);
        */
    }

    @Test
    public void test02() {

        TransformerTest.transformText(
            "[{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp' }," +
                "{ name: 'insertRange', start: [5, 1, 4, 0, 0], position: 'start', type: 'comment', id: 'cmt6' }," +
                "{ name: 'insertRange', start: [5, 1, 4, 0, 2], position: 'end', type: 'comment', id: 'cmt6' }," +
                "{ name: 'insertComment', start: [5, 1, 4, 0, 3], id: 'cmt6', author: 'me', date: '1.1.1', uid: 101 }," +
                "{ name: 'insertParagraph', start: [0], target: 'cmt6' }," +
                "{ name: 'insertStyleSheet', styleName: 'annotation text' }," +
                "{ name: 'setAttributes', start: [0], target: 'cmt6', attrs: { styleId: 'annotation text' } }," +
                "{ name: 'splitParagraph', start: [1, 0] }," +
                "{ name: 'splitParagraph', start: [2, 0] }]",
            "[{ name: 'mergeTable', start: [4], rowcount: 6 }," +
                "{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo' }," +
                "{ name: 'insertRows', start: [4, 1], count: 3 }," +
                "{ name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } } }," +
                "{ name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true }," +
                "{ name: 'splitTable', start: [0, 2] }," +
                "{ name: 'insertParagraph', start: [1] }]",
            "[{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'mergeTable', start: [6], rowcount: 6 }," +
                "{ name: 'insertText', start: [6, 4, 5, 0, 0], text: 'ooo' }," +
                "{ name: 'insertRows', start: [6, 1], count: 3 }," +
                "{ name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } } }," +
                "{ name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true }," +
                "{ name: 'splitTable', start: [0, 2] }," +
                "{ name: 'insertParagraph', start: [1] }]",
            "[{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'insertText', start: [7, 10, 5, 0, 0], text: 'ppp' }," +
                "{ name: 'insertRange', start: [7, 10, 5, 0, 0], position: 'start', type: 'comment', id: 'cmt6' }," +
                "{ name: 'insertRange', start: [7, 10, 5, 0, 2], position: 'end', type: 'comment', id: 'cmt6' }," +
                "{ name: 'insertComment', start: [7, 10, 5, 0, 3], id: 'cmt6', author: 'me', date: '1.1.1', uid: 101 }," +
                "{ name: 'insertParagraph', start: [0], target: 'cmt6' }," +
                "{ name: 'insertStyleSheet', styleName: 'annotation text' }," +
                "{ name: 'setAttributes', start: [0], target: 'cmt6', attrs: { styleId: 'annotation text' } }," +
                "{ name: 'splitParagraph', start: [4, 0] }," +
                "{ name: 'splitParagraph', start: [5, 0] }]");

            /*
                var externalActions = [
                    { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertRange', start: [5, 1, 4, 0, 0], position: 'start', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                        { name: 'insertRange', start: [5, 1, 4, 0, 2], position: 'end', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                        { name: 'insertComment', start: [5, 1, 4, 0, 3], id: 'cmt6', author: 'me', date: '1.1.1', uid: 101, opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [0], target: 'cmt6', opl: 1, osn: 1 },
                        { name: 'insertStyleSheet', styleName: 'annotation text', opl: 1, osn: 1 },
                        { name: 'setAttributes', start: [0], target: 'cmt6', attrs: { styleId: 'annotation text' }, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 0] }] }
                ];
                var localActions = [
                    { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                        { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                    ] },
                    { operations: [
                        { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                    ] }
                ];
                var transformedActions = testRunner.transformActions(externalActions, localActions);
                expectActions([
                    { operations: [
                        { name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                        { name: 'mergeTable', start: [6], rowcount: 6, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'insertText', start: [6, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [6, 1], count: 3, opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                        { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                    ] },
                    { operations: [
                        { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                    ] }
                ], localActions);
                expectActions([
                    { operations: [{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [7, 10, 5, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertRange', start: [7, 10, 5, 0, 0], position: 'start', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                        { name: 'insertRange', start: [7, 10, 5, 0, 2], position: 'end', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                        { name: 'insertComment', start: [7, 10, 5, 0, 3], id: 'cmt6', author: 'me', date: '1.1.1', uid: 101, opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [0], target: 'cmt6', opl: 1, osn: 1 },
                        { name: 'insertStyleSheet', styleName: 'annotation text', opl: 1, osn: 1 },
                        { name: 'setAttributes', start: [0], target: 'cmt6', attrs: { styleId: 'annotation text' }, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [4, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 0] }] }
                ], transformedActions);
            });
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "[{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp' }," +
                "{ name: 'insertBookmark', start: [5, 1, 4, 0, 0], position: 'start', type: 'comment', id: 'cmt6' }," +
                "{ name: 'insertDrawing', start: [5, 1, 4, 0, 3], type: 'shape' }," +
                "{ name: 'insertParagraph', start: [5, 1, 4, 0, 3, 0] }," +
                "{ name: 'setAttributes', start: [5, 1, 4, 0, 3, 0], attrs: { paragraph: { marginBottom: 0 } } }," +
                "{ name: 'splitParagraph', start: [1, 0] }," +
                "{ name: 'splitParagraph', start: [2, 0] }]",
            "[{ name: 'mergeTable', start: [4], rowcount: 6 }," +
                "{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo' }," +
                "{ name: 'insertRows', start: [4, 1], count: 3 }," +
                "{ name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } } }," +
                "{ name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true }," +
                "{ name: 'splitTable', start: [0, 2] }," +
                "{ name: 'insertParagraph', start: [1] }]",
            "[{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'mergeTable', start: [6], rowcount: 6 }," +
                "{ name: 'insertText', start: [6, 4, 5, 0, 0], text: 'ooo' }," +
                "{ name: 'insertRows', start: [6, 1], count: 3 }," +
                "{ name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }}," +
                "{ name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true }," +
                "{ name: 'splitTable', start: [0, 2] }," +
                "{ name: 'insertParagraph', start: [1] }]",
            "[{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }," +
                "{ name: 'insertText', start: [7, 10, 5, 0, 0], text: 'ppp' }," +
                "{ name: 'insertBookmark', start: [7, 10, 5, 0, 0], position: 'start', type: 'comment', id: 'cmt6' }," +
                "{ name: 'insertDrawing', start: [7, 10, 5, 0, 3], type: 'shape' }," +
                "{ name: 'insertParagraph', start: [7, 10, 5, 0, 3, 0] }," +
                "{ name: 'setAttributes', start: [7, 10, 5, 0, 3, 0], attrs: { paragraph: { marginBottom: 0 } } }," +
                "{ name: 'splitParagraph', opl: 1, osn: 1, start: [4, 0] }," +
                "{ name: 'splitParagraph', opl: 1, osn: 1, start: [5, 0] }]");

            /*
                var externalActions = [
                    { operations: [{ name: 'insertColumn', start: [4], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [5, 1, 4, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertBookmark', start: [5, 1, 4, 0, 0], position: 'start', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                        { name: 'insertDrawing', start: [5, 1, 4, 0, 3], type: 'shape', opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [5, 1, 4, 0, 3, 0], opl: 1, osn: 1 },
                        { name: 'setAttributes', start: [5, 1, 4, 0, 3, 0], attrs: { paragraph: { marginBottom: 0 } }, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 0] }] }
                ];
                var localActions = [
                    { operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [4, 4, 4, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [4, 1], count: 3, opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                        { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                    ] },
                    { operations: [
                        { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                    ] }
                ];
                var transformedActions = testRunner.transformActions(externalActions, localActions);
                expectActions([
                    { operations: [
                        { name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 },
                        { name: 'mergeTable', start: [6], rowcount: 6, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'insertText', start: [6, 4, 5, 0, 0], text: 'ooo', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertRows', start: [6, 1], count: 3, opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertTable', start: [0], attrs: { table: { tableGrid: [1, 1, 1] } }, opl: 1, osn: 1 },
                        { name: 'insertRows', start: [0, 0], count: 4, insertDefaultCells: true, opl: 1, osn: 1 }
                    ] },
                    { operations: [
                        { name: 'splitTable', start: [0, 2], opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [1], opl: 1, osn: 1 }
                    ] }
                ], localActions);
                expectActions([
                    { operations: [{ name: 'insertColumn', start: [7], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind', opl: 1, osn: 1 }] },
                    { operations: [{ name: 'insertText', start: [7, 10, 5, 0, 0], text: 'ppp', opl: 1, osn: 1 }] },
                    { operations: [
                        { name: 'insertBookmark', start: [7, 10, 5, 0, 0], position: 'start', type: 'comment', id: 'cmt6', opl: 1, osn: 1 },
                        { name: 'insertDrawing', start: [7, 10, 5, 0, 3], type: 'shape', opl: 1, osn: 1 },
                        { name: 'insertParagraph', start: [7, 10, 5, 0, 3, 0], opl: 1, osn: 1 },
                        { name: 'setAttributes', start: [7, 10, 5, 0, 3, 0], attrs: { paragraph: { marginBottom: 0 } }, opl: 1, osn: 1 }
                    ] },
                    { operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [4, 0] }, { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 0] }] }
                ], transformedActions);
            */
    }

}
