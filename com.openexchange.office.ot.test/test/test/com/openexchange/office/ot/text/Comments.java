/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class Comments {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 1], text: '123' }",
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'insertComment', start: [3, 8] }",
            "{ name: 'insertText', start: [3, 1], text: '123' }");
            /*
                oneOperation = { name: 'insertText', start: [3, 1], text: '123', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertComment', start: [3, 8], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertText', start: [3, 1], text: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
         TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 7], text: '123' }",
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'insertText', start: [3, 8], text: '123' }");
            /*
                oneOperation = { name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertText', start: [3, 8], text: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [3, 7], text: '123' }",
            "{ name: 'insertComment', start: [3, 7] }",
            "{ name: 'insertComment', start: [3, 10] }",
            "{ name: 'insertText', start: [3, 7], text: '123' }");
            /*
                oneOperation = { name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertComment', start: [3, 7], opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertComment', start: [3, 10], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'insertText', start: [3, 1], text: '123' }",
            "{ name: 'insertText', start: [3, 1], text: '123' }",
            "{ name: 'insertComment', start: [3, 8] }");
            /*
                oneOperation = { name: 'insertComment', start: [3, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', start: [3, 1], text: '123', opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', start: [3, 1], text: '123', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertComment', start: [3, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'insertText', start: [3, 7], text: '123' }",
            "{ name: 'insertText', start: [3, 8], text: '123' }",
            "{ name: 'insertComment', start: [3, 5] }");
            /*
                oneOperation = { name: 'insertComment', start: [3, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', start: [3, 8], text: '123', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'insertComment', start: [3, 7] }",
            "{ name: 'insertText', start: [3, 7], text: '123' }",
            "{ name: 'insertText', start: [3, 8], text: '123' }",
            "{ name: 'insertComment', start: [3, 7] }");
            /*
                oneOperation = { name: 'insertComment', start: [3, 7], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', start: [3, 7], text: '123', opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', start: [3, 8], text: '123', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertComment', start: [3, 7], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'splitParagraph', start: [3, 1] }",
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'insertComment', start: [4, 4] }",
            "{ name: 'splitParagraph', start: [3, 1] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertComment', start: [4, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'splitParagraph', start: [3, 7] }",
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'splitParagraph', start: [3, 8] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [3, 7], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'splitParagraph', start: [3, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'splitParagraph', start: [3, 1] }",
            "{ name: 'splitParagraph', start: [3, 1] }",
            "{ name: 'insertComment', start: [4, 4] }");
            /*
                oneOperation = { name: 'insertComment', start: [3, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 1], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertComment', start: [4, 4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'insertComment', start: [3, 5] }",
            "{ name: 'splitParagraph', start: [3, 7] }",
            "{ name: 'splitParagraph', start: [3, 8] }",
            "{ name: 'insertComment', start: [3, 5] }");
            /*
                oneOperation = { name: 'insertComment', start: [3, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 7], opl: 1, osn: 1 }] }];
                transformedOps = otManagerText.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'splitParagraph', start: [3, 8], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertComment', start: [3, 5], opl: 1, osn: 1 }], transformedOps);
             */
    }
}
