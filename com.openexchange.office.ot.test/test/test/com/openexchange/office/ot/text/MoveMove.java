/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveMove {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0] }");
            /*
                oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0, 1, 0], end: [5, 0, 1, 0], to: [4, 0, 1, 0] }",
            "{ name: 'move', start: [5, 0, 1, 0], end: [5, 0, 1, 0], to: [4, 0, 1, 0] }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'move', start: [5, 0, 1, 0], end: [5, 0, 1, 0], to: [4, 0, 1, 0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [5, 0, 1, 0], end: [5, 0, 1, 0], to: [4, 0, 1, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [5, 0, 1, 0], end: [5, 0, 1, 0], to: [4, 0, 1, 0], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [4, 0] }",
            "[]");
            /*
                oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [4, 0], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test04A() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [3, 0] }",
            "[]");
            /*
                oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [4, 0], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0, 1, 1, 0] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0] }",
            "{ name: 'move', start: [3, 0, 1, 1, 0], end: [3, 0, 1, 1, 0], to: [4, 0] }",
            "[]");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0, 1, 1, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [4, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1, 1, 0]); // setting the source to the dest of the external operation
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0, 1, 1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(0); // external operation must not be applied
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 1] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1] }",
            "[]");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]); // setting the source to the dest of the external operation
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(0); // external operation must not be applied
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 2] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [3, 0] }",
            "[]");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]); // setting the source to the dest of the external operation
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(0); // external operation must not be applied
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 2] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [3, 6] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [3, 7] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 2] }");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 7]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 0]);
    expect(oneOperation.end).to.deep.equal([5, 0]);
    expect(oneOperation.to).to.deep.equal([3, 2]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 6] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [3, 2] }",
            "{ name: 'move', start: [4, 0], end: [4, 0], to: [3, 2] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 7] }");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 0], end: [4, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 0]);
    expect(oneOperation.end).to.deep.equal([5, 0]);
    expect(oneOperation.to).to.deep.equal([3, 7]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 6] }",
            "{ name: 'move', start: [5, 2], end: [5, 2], to: [3, 2] }",
            "{ name: 'move', start: [5, 1], end: [5, 1], to: [3, 2] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 7] }");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [5, 2], end: [5, 2], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([5, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([5, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 0]);
    expect(oneOperation.end).to.deep.equal([5, 0]);
    expect(oneOperation.to).to.deep.equal([3, 7]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 4], end: [5, 4], to: [3, 6] }",
            "{ name: 'move', start: [5, 2], end: [5, 2], to: [3, 2] }",
            "{ name: 'move', start: [5, 2], end: [5, 2], to: [3, 2] }",
            "{ name: 'move', start: [5, 3], end: [5, 3], to: [3, 7] }");
            /*
    oneOperation = { name: 'move', start: [5, 4], end: [5, 4], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [5, 2], end: [5, 2], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([5, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([5, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 3]);
    expect(oneOperation.end).to.deep.equal([5, 3]);
    expect(oneOperation.to).to.deep.equal([3, 7]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 4], end: [5, 4], to: [3, 6] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 1] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 1] }",
            "{ name: 'move', start: [5, 4], end: [5, 4], to: [3, 6] }");
            /*
    oneOperation = { name: 'move', start: [5, 4], end: [5, 4], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([5, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([5, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([5, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 4]);
    expect(oneOperation.end).to.deep.equal([5, 4]);
    expect(oneOperation.to).to.deep.equal([3, 6]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 4], end: [5, 4], to: [3, 6] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 6] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 5] }",
            "{ name: 'move', start: [5, 3], end: [5, 3], to: [3, 6] }");
            /*
    oneOperation = { name: 'move', start: [5, 4], end: [5, 4], to: [3, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([5, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([5, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([5, 5]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 3]);
    expect(oneOperation.end).to.deep.equal([5, 3]);
    expect(oneOperation.to).to.deep.equal([3, 6]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 4, 1, 0], end: [5, 4, 1, 0], to: [3, 6] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 6] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 6] }",
            "{ name: 'move', start: [5, 3, 1, 0], end: [5, 3, 1, 0], to: [3, 6] }");
            /*
    oneOperation = { name: 'move', start: [5, 4, 1, 0], end: [5, 4, 1, 0], to: [3, 6], opl: 1, osn: 1 }; // only the inner drawing is moved
    localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [5, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([5, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([5, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([5, 6]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 3, 1, 0]);
    expect(oneOperation.end).to.deep.equal([5, 3, 1, 0]);
    expect(oneOperation.to).to.deep.equal([3, 6]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 4, 1, 0], end: [5, 4, 1, 0], to: [3, 1, 1, 0] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 2] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 2] }",
            "{ name: 'move', start: [5, 3, 1, 0], end: [5, 3, 1, 0], to: [3, 1, 1, 0] }");
            /*
    oneOperation = { name: 'move', start: [5, 4, 1, 0], end: [5, 4, 1, 0], to: [3, 1, 1, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 2], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([5, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([5, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 3, 1, 0]);
    expect(oneOperation.end).to.deep.equal([5, 3, 1, 0]);
    expect(oneOperation.to).to.deep.equal([3, 1, 1, 0]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0] }",
            "{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0] }");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 0]);
    expect(oneOperation.end).to.deep.equal([5, 0]);
    expect(oneOperation.to).to.deep.equal([3, 0]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0] }");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 0]);
    expect(oneOperation.end).to.deep.equal([5, 0]);
    expect(oneOperation.to).to.deep.equal([3, 0]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [3, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [5, 0] }",
            "{ name: 'move', start: [3, 1], end: [3, 1], to: [5, 0] }",
            "{ name: 'move', start: [5, 1], end: [5, 1], to: [3, 0] }");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [3, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [5, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([5, 0]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 1]);
    expect(oneOperation.end).to.deep.equal([5, 1]);
    expect(oneOperation.to).to.deep.equal([3, 0]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 1] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [2, 1] }");
            /*
    oneOperation = { name: 'move', start: [5, 0], end: [5, 0], to: [2, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([5, 0]);
    expect(oneOperation.end).to.deep.equal([5, 0]);
    expect(oneOperation.to).to.deep.equal([2, 1]);
             */
    }
}
