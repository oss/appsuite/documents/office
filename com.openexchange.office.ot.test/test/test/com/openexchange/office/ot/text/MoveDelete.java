/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MoveDelete {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [3, 2], end: [3, 3] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'delete', start: [3, 2], end: [3, 3] }");
            /*
                oneOperation = { name: 'delete', start: [3, 2], end: [3, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'delete', start: [3, 2], end: [3, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 2], end: [2, 3] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 4] }",
            "{ name: 'delete', start: [2, 2], end: [2, 3] }");
            /*
                oneOperation = { name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 4], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 8], end: [2, 9] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6] }",
            "{ name: 'delete', start: [2, 9], end: [2, 10] }");
            /*
                oneOperation = { name: 'delete', start: [2, 8], end: [2, 9], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'delete', start: [2, 9], end: [2, 10], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 2], end: [2, 3] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [4, 6] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [4, 6] }",
            "{ name: 'delete', start: [2, 2], end: [2, 3] }");
            /*
    oneOperation = { name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [4, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 6]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 2]);
    expect(transformedOps[0].end).to.deep.equal([2, 3]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 2], end: [2, 3] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 1] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 1] }",
            "{ name: 'delete', start: [2, 3], end: [2, 4] }");
            /*
    oneOperation = { name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 1], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 3]);
    expect(transformedOps[0].end).to.deep.equal([2, 4]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 2], end: [2, 3] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 6] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 6] }",
            "{ name: 'delete', start: [2, 1], end: [2, 2] }");
            /*
    oneOperation = { name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 6]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 1]);
    expect(transformedOps[0].end).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 6], end: [2, 7] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 0] }",
            "{ name: 'delete', start: [2, 6], end: [2, 7] }");
            /*
    oneOperation = { name: 'delete', start: [2, 6], end: [2, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 6]);
    expect(transformedOps[0].end).to.deep.equal([2, 7]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 1, 0, 4], end: [2, 1, 0, 6] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 0] }",
            "{ name: 'delete', start: [2, 0, 0, 4], end: [2, 0, 0, 6] }");
            /*
    oneOperation = { name: 'delete', start: [2, 1, 0, 4], end: [2, 1, 0, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2, 0, 0, 4]);
    expect(transformedOps[0].end).to.deep.equal([2, 0, 0, 6]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 1, 0, 4], end: [2, 1, 0, 6] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 2, 2, 1, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 2, 2, 1, 0] }",
            "{ name: 'delete', start: [3, 2, 2, 1, 0, 0, 4], end: [3, 2, 2, 1, 0, 0, 6] }");
            /*
    oneOperation = { name: 'delete', start: [2, 1, 0, 4], end: [2, 1, 0, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [3, 2, 2, 1, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 2, 2, 1, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([3, 2, 2, 1, 0, 0, 4]);
    expect(transformedOps[0].end).to.deep.equal([3, 2, 2, 1, 0, 0, 6]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 1, 0, 4], end: [2, 1, 0, 6] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [0, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [0, 0] }",
            "{ name: 'delete', start: [0, 0, 0, 4], end: [0, 0, 0, 6] }");
            /*
    oneOperation = { name: 'delete', start: [2, 1, 0, 4], end: [2, 1, 0, 6], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [0, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([0, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([0, 0, 0, 4]);
    expect(transformedOps[0].end).to.deep.equal([0, 0, 0, 6]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 1, 0], end: [2, 1, 2] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [0, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [0, 0] }",
            "{ name: 'delete', start: [0, 0, 0], end: [0, 0, 2] }");
            /*
    oneOperation = { name: 'delete', start: [2, 1, 0], end: [2, 1, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 1], end: [2, 1], to: [0, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([0, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([0, 0, 0]);
    expect(transformedOps[0].end).to.deep.equal([0, 0, 2]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2], end: [3] }",
            "{ name: 'move', start: [4, 1], end: [4, 1], to: [0, 0] }",
            "{ name: 'move', start: [2, 1], end: [2, 1], to: [0, 0] }",
            "{ name: 'delete', start: [2], end: [3] }");
            /*
    oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [4, 1], end: [4, 1], to: [0, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([0, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2]);
    expect(transformedOps[0].end).to.deep.equal([3]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2], end: [3] }",
            "{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0] }",
            "{ name: 'move', start: [1, 1], end: [1, 1], to: [4, 0] }",
            "{ name: 'delete', start: [2], end: [3] }");
            /*
    oneOperation = { name: 'delete', start: [2], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([4, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2]);
    expect(transformedOps[0].end).to.deep.equal([3]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0] }",
            "[]",
            "[{ name: 'delete', start: [1], end: [3] }, { name: 'delete', start: [3, 0], end: [3, 0] }]");
            /*
    oneOperation = { name: 'delete', start: [1], end: [3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([3, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(2);
    expect(transformedOps[0].start).to.deep.equal([1]);
    expect(transformedOps[0].end).to.deep.equal([3]);
    expect(transformedOps[1].name).to.equal('delete');
    expect(transformedOps[1].start).to.deep.equal([3, 0]);
    expect(transformedOps[1].end).to.deep.equal([3, 0]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1] }",
            "{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0] }",
            "[]",
            "[{ name: 'delete', start: [1] }, { name: 'delete', start: [5, 0], end: [5, 0] }]");
            /*
    oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([5, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(2);
    expect(transformedOps[0].start).to.deep.equal([1]);
    expect(transformedOps[0].end).to.equal(undefined);
    expect(transformedOps[1].name).to.equal('delete');
    expect(transformedOps[1].start).to.deep.equal([5, 0]);
    expect(transformedOps[1].end).to.deep.equal([5, 0]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [7] }",
            "{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0] }",
            "{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0] }",
            "{ name: 'delete', start: [7] }");
            /*
    oneOperation = { name: 'delete', start: [7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 1], end: [1, 1], to: [6, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 1]);
    expect(localActions[0].operations[0].to).to.deep.equal([6, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([7]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'move', start: [1, 1, 2, 2, 2], end: [1, 1, 2, 2, 2], to: [6, 0] }",
            "[]",
            "[{ name: 'delete', start: [1, 1], end: [1, 2] }, { name: 'delete', start: [6, 0], end: [6, 0] }]");
            /*
    oneOperation = { name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 1, 2, 2, 2], end: [1, 1, 2, 2, 2], to: [6, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 2, 2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 1, 2, 2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([6, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(2);
    expect(transformedOps[0].start).to.deep.equal([1, 1]);
    expect(transformedOps[0].end).to.deep.equal([1, 2]);
    expect(transformedOps[1].name).to.equal('delete');
    expect(transformedOps[1].start).to.deep.equal([6, 0]);
    expect(transformedOps[1].end).to.deep.equal([6, 0]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'move', start: [1, 2, 0, 0], end: [1, 2, 0, 0], to: [2, 0] }",
            "[]",
            "[{ name: 'delete', start: [1, 1], end: [1, 2] }, { name: 'delete', start: [2, 0], end: [2, 0] }]");
            /*
    oneOperation = { name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 2, 0, 0], end: [1, 2, 0, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 0, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 2, 0, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(2);
    expect(transformedOps[0].start).to.deep.equal([1, 1]);
    expect(transformedOps[0].end).to.deep.equal([1, 2]);
    expect(transformedOps[1].name).to.equal('delete');
    expect(transformedOps[1].start).to.deep.equal([2, 0]);
    expect(transformedOps[1].end).to.deep.equal([2, 0]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [2, 0] }",
            "{ name: 'move', start: [1, 1, 0, 0], end: [1, 1, 0, 0], to: [2, 0] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }");
            /*
    oneOperation = { name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 0, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 1, 0, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1, 1]);
    expect(transformedOps[0].end).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [2, 0] }",
            "[]",
            "{ name: 'delete', start: [1], end: [2] }");
            /*
    oneOperation = { name: 'delete', start: [1], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 0, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 3, 0, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1]);
    expect(transformedOps[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1] }",
            "{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [1, 6] }",
            "[]",
            "{ name: 'delete', start: [1] }");
            /*
    oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [1, 6], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 0, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 3, 0, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 6]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 3] }",
            "{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [2, 0] }",
            "[]",
            "[{ name: 'delete', start: [1, 3] }, { name: 'delete', start: [2, 0], end: [2, 0] }]");
            /*
    oneOperation = { name: 'delete', start: [1, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 3, 0, 0], end: [1, 3, 0, 0], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 0, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 3, 0, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(2);
    expect(transformedOps[0].start).to.deep.equal([1, 3]);
    expect(transformedOps[0].end).to.equal(undefined);
    expect(transformedOps[1].name).to.equal('delete');
    expect(transformedOps[1].start).to.deep.equal([2, 0]);
    expect(transformedOps[1].end).to.deep.equal([2, 0]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3, 0, 0] }",
            "{ name: 'delete', start: [1, 0], end: [1, 0] }",
            "{ name: 'delete', start: [1] }");
            /*
                oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3, 0, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'move', start: [1, 0], end: [1, 0], to: [1, 3, 0, 0], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 },
                    { name: 'delete', start: [1, 0], end: [1, 0], opl: 1, osn: 1 }
                ] }], localActions);
                expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23A() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1] }",
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [1, 3, 0, 0] }",
            "{ name: 'delete', start: [4, 0], end: [4, 0] }",
            "{ name: 'delete', start: [1] }");
            /*
                oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [1, 3, 0, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'move', start: [4, 0], end: [4, 0], to: [1, 3, 0, 0], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 },
                    { name: 'delete', start: [4, 0], end: [4, 0], opl: 1, osn: 1 }
                ] }], localActions);
                expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test23B() {
        TransformerTest.transformText(
            "{ name: 'move', start: [5, 0], end: [5, 0], to: [1, 3, 0, 0] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [4, 0], end: [4, 0] }");
            /*
                oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'move', start: [5, 0], end: [5, 0], to: [1, 3, 0, 0], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'move', start: [4, 0], end: [4, 0], to: [1, 3, 0, 0], _REMOVED_OPERATION_: 1, opl: 1, osn: 1 },
                    { name: 'delete', start: [4, 0], end: [4, 0], opl: 1, osn: 1 }
                ] }], localActions);
                expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1], end: [4] }",
            "{ name: 'move', start: [7, 3], end: [7, 3], to: [1, 3, 0, 0] }",
            "{ name: 'delete', start: [3, 3], end: [3, 3] }",
            "{ name: 'delete', start: [1], end: [4] }");
            /*
    oneOperation = { name: 'delete', start: [1], end: [4], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [7, 3], end: [7, 3], to: [1, 3, 0, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(2);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 3, 0, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations[1].name).to.equal('delete');
    expect(localActions[0].operations[1].start).to.deep.equal([3, 3]);
    expect(localActions[0].operations[1].end).to.deep.equal([3, 3]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1]);
    expect(transformedOps[0].end).to.deep.equal([4]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2, 0] }",
            "{ name: 'move', start: [2, 0, 0, 0], end: [2, 0, 0, 0], to: [1, 3, 0, 0] }",
            "[]",
            "[{ name: 'delete', start: [2, 0] }, { name: 'delete', start: [1, 3, 0, 0], end: [1, 3, 0, 0] }]");
            /*
    oneOperation = { name: 'delete', start: [2, 0], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0, 0, 0], end: [2, 0, 0, 0], to: [1, 3, 0, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 0, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0, 0, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 3, 0, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(transformedOps.length).to.equal(2);
    expect(transformedOps[0].start).to.deep.equal([2, 0]);
    expect(transformedOps[0].end).to.equal(undefined);
    expect(transformedOps[1].name).to.equal('delete');
    expect(transformedOps[1].start).to.deep.equal([1, 3, 0, 0]);
    expect(transformedOps[1].end).to.deep.equal([1, 3, 0, 0]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 4] }",
            "[]",
            "{ name: 'delete', start: [2] }");
            /*
    oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2], end: [2] }",
            "{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 4] }",
            "[]",
            "{ name: 'delete', start: [2], end: [2] }");
            /*
    oneOperation = { name: 'delete', start: [2], end: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 2], end: [2, 2], to: [2, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 4]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2]);
    expect(transformedOps[0].end).to.deep.equal([2]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [2] }",
            "{ name: 'move', start: [2, 0, 0, 1, 4], end: [2, 0, 0, 1, 4], to: [2, 0, 1, 1, 4] }",
            "[]",
            "{ name: 'delete', start: [2] }");
            /*
    oneOperation = { name: 'delete', start: [2], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0, 0, 1, 4], end: [2, 0, 0, 1, 4], to: [2, 0, 1, 1, 4], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 0, 1, 4]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0, 0, 1, 4]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0, 1, 1, 4]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([2]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 0] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 2] }",
            "{ name: 'delete', start: [1, 0] }");
            /*
    oneOperation = { name: 'delete', start: [1, 0], opl: 1, osn: 1 }; // this might be a character
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 2]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1, 0]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 2] }",
            "{ name: 'delete', start: [1, 2] }");
            /*
    oneOperation = { name: 'delete', start: [1, 2], opl: 1, osn: 1 }; // this might be a character
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 2]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1, 2]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 3] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'delete', start: [1, 4] }");
            /*
    oneOperation = { name: 'delete', start: [1, 3], opl: 1, osn: 1 }; // this might be a character at exactly the same position
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1, 4]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 4] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'delete', start: [1, 5] }");
            /*
    oneOperation = { name: 'delete', start: [1, 4], opl: 1, osn: 1 }; // this might be a character
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1, 5]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 5] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'delete', start: [1, 6] }");
            /*
    oneOperation = { name: 'delete', start: [1, 5], opl: 1, osn: 1 }; // this might be a character
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1, 6]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'delete', start: [1, 0], end: [1, 0] }",
            "{ name: 'delete', start: [1] }");
            /*
    // deleting the destination paragraph
    oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 }; // deleting the complete paragraph
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(2);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations[1].name).to.equal('delete');
    expect(localActions[0].operations[1].start).to.deep.equal([1, 0]); // deleting the drawing that is now on position [1, 0] on server side, ignoring move operation
    expect(localActions[0].operations[1].end).to.deep.equal([1, 0]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1]);
    expect(transformedOps[0].end).to.equal(undefined);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 1], end: [1, 7] }",
            "{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3] }",
            "{ name: 'delete', start: [2, 0], end: [2, 0] }",
            "{ name: 'delete', start: [1, 1], end: [1, 8] }");
            /*
    // deleting the destination range
    oneOperation = { name: 'delete', start: [1, 1], end: [1, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [2, 0], end: [2, 0], to: [1, 3], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(2);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].end).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0].to).to.deep.equal([1, 3]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations[1].name).to.equal('delete');
    expect(localActions[0].operations[1].start).to.deep.equal([2, 0]); // deleting the drawing that is still on position [2, 0] on server side, ignoring move operation
    expect(localActions[0].operations[1].end).to.deep.equal([2, 0]);
    expect(transformedOps.length).to.equal(1);
    expect(transformedOps[0].start).to.deep.equal([1, 1]);
    expect(transformedOps[0].end).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 1], end: [1, 7] }",
            "{ name: 'move', start: [1, 3], end: [1, 3], to: [2, 0] }",
            "[]",
            "[{ name: 'delete', start: [1, 1], end: [1, 6] }, { name: 'delete', start: [2, 0], end: [2, 0] }]");
            /*
    // deleting the source range
    oneOperation = { name: 'delete', start: [1, 1], end: [1, 7], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations.length).to.equal(1);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // move operation must not be sent to the server
    expect(transformedOps.length).to.equal(2);
    expect(transformedOps[0].start).to.deep.equal([1, 1]);
    expect(transformedOps[0].end).to.deep.equal([1, 6]);
    expect(transformedOps[1].name).to.equal('delete');
    expect(transformedOps[1].start).to.deep.equal([2, 0]); // deleting the drawing that is locally on position [2, 0]
    expect(transformedOps[1].end).to.deep.equal([2, 0]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 3], end: [1, 3] }",
            "{ name: 'move', start: [1, 3], end: [1, 3], to: [2, 0] }",
            "[]",
            "{ name: 'delete', start: [2, 0], end: [2, 0] }");
            /*
    // deleting the source drawing
    oneOperation = { name: 'delete', start: [1, 3], end: [1, 3], opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'move', start: [1, 3], end: [1, 3], to: [2, 0], opl: 1, osn: 1 }] }];
    transformedOps = otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 3]);
    expect(localActions[0].operations[0].end).to.deep.equal([1, 3]);
    expect(localActions[0].operations[0].to).to.deep.equal([2, 0]);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(transformedOps.length).to.equal(1); // should be 1
    expect(transformedOps[0].name).to.equal('delete'); // a new delete operation must be generated to remove the locally moved drawing
    expect(transformedOps[0].start).to.deep.equal([2, 0]);
    expect(transformedOps[0].end).to.deep.equal([2, 0]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 0] }",
            "{ name: 'delete', start: [3, 2], end: [3, 3] }",
            "{ name: 'delete', start: [3, 2], end: [3, 3] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }");
            /*
                oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [2, 0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [3, 2], end: [3, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [3, 2], end: [3, 3], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6] }",
            "{ name: 'delete', start: [2, 2], end: [2, 3] }",
            "{ name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 4] }");
            /*
                oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [2, 2], end: [2, 3], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 4], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6] }",
            "{ name: 'delete', start: [2, 8], end: [2, 9] }",
            "{ name: 'delete', start: [2, 9], end: [2, 10] }",
            "{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6] }");
            /*
                oneOperation = { name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', start: [2, 8], end: [2, 9], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [2, 9], end: [2, 10], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'move', start: [3, 6], end: [3, 6], to: [2, 6], opl: 1, osn: 1 }], transformedOps);
             */
    }

}
