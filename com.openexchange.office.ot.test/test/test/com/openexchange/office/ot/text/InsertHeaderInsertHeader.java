/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertHeaderInsertHeader {

    @Test
    public void test01() {
        TransformerTest.transformText(
            "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }",
            "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transformText(
            "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }",
            "{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'footer_default' }",
            "{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'footer_default' }",
            "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }");
            /*
                oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'footer_default', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'footer_default', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transformText(
            "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }",
            "{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default' }",
            "[{ name: 'deleteHeaderFooter', id: 'OX_rId100' }, { name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default' }]",
            "[]");
            /*
                // same type but different ID -> is this possible?
                oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 },
                    { name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default', opl: 1, osn: 1 }
                ] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transformText(
            "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'footer_default' }",
            "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }",
            "[{ name: 'deleteHeaderFooter', id: 'OX_rId100' }, { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }]",
            "[]");
            /*
                // same ID but different type -> is this possible?
                oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'footer_first', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [
                    { name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 },
                    { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }
                ] }], localActions);
                expectOp([], transformedOps);
             */
    }

    // @Test
    // public void test05() {
    //     TransformerTest.transformText(
    //         "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }",
    //         "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
    //         "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
    //         "[]");
            /*
                oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    // }

    @Test
    public void test06() {
        TransformerTest.transformText(
            "{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default' }",
            "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
            "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
            "{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default' }");
            /*
                oneOperation = { name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertHeaderFooter', id: 'OX_rId101', type: 'header_default', opl: 1, osn: 1 }], transformedOps);
             */
    }

    // @Test
    // public void test07() {
    //     TransformerTest.transformText(
    //         "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
    //         "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }",
    //         "[]",
    //         "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }");
            /*
                oneOperation = { name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', _REMOVED_OPERATION_: 1, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }], transformedOps);
             */
    // }

    @Test
    public void test08() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'OX_rId101' }",
            "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }",
            "{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default' }",
            "{ name: 'deleteHeaderFooter', id: 'OX_rId101' }");
            /*
                oneOperation = { name: 'deleteHeaderFooter', id: 'OX_rId101', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertHeaderFooter', id: 'OX_rId100', type: 'header_default', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'deleteHeaderFooter', id: 'OX_rId101', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transformText(
            "{ name: 'insertText', target: 'OX_rId100', text: 'ooo' }",
            "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
            "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
            "[]");
            /*
                oneOperation = { name: 'insertText', target: 'OX_rId100', text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transformText(
            "{ name: 'insertText', target: 'OX_rId101', text: 'ooo' }",
            "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
            "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
            "{ name: 'insertText', target: 'OX_rId101', text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', target: 'OX_rId101', text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertText', target: 'OX_rId101', text: 'ooo', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }",
            "{ name: 'insertText', target: 'OX_rId100', text: 'ooo' }",
            "[]",
            "{ name: 'deleteHeaderFooter', id: 'OX_rId100' }");
            /*
                oneOperation = { name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', target: 'OX_rId100', text: 'ooo', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', target: 'OX_rId100', text: 'ooo', opl: 1, osn: 1, _REMOVED_OPERATION_: 1 }] }], localActions);
                expectOp([{ name: 'deleteHeaderFooter', id: 'OX_rId100', opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'OX_rId101' }",
            "{ name: 'insertText', target: 'OX_rId100', text: 'ooo' }",
            "{ name: 'insertText', target: 'OX_rId100', text: 'ooo' }",
            "{ name: 'deleteHeaderFooter', id: 'OX_rId101' }");
            /*
                oneOperation = { name: 'deleteHeaderFooter', id: 'OX_rId101', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertText', target: 'OX_rId100', text: 'ooo', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertText', target: 'OX_rId100', text: 'ooo', opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'deleteHeaderFooter', id: 'OX_rId101', opl: 1, osn: 1 }], transformedOps);
             */
    }
}
