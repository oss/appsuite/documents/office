/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.text;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ChangeComment {

    @Test
    public void test01a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content A' }",
            "{ name: 'changeComment', start: [1, 2], text: 'Mofied comment content B' }",
            "{ name: 'changeComment', start: [1, 2], text: 'Mofied comment content B' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content A' }");
    }

    @Test
    public void test01b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2], text: 'Mofied comment content A' }",
            "{ name: 'changeComment', start: [1, 2], text: 'Mofied comment content B' }",
            "{ name: 'changeComment', start: [1, 2], text: 'Mofied comment content B' }",
            "[]");
    }

    @Test
    public void test01c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2], target: 'hf1', text: 'Mofied comment content A' }",
            "{ name: 'changeComment', start: [1, 2], text: 'Mofied comment content B' }",
            "{ name: 'changeComment', start: [1, 2], text: 'Mofied comment content B' }",
            "{ name: 'changeComment', start: [1, 2], target: 'hf1', text: 'Mofied comment content A' }");
    }
    @Test
    public void test01d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2], target: 'hf1', text: 'Mofied comment content A' }",
            "{ name: 'changeComment', start: [1, 2], target: 'hf1', text: 'Mofied comment content B' }",
            "{ name: 'changeComment', start: [1, 2], target: 'hf1', text: 'Mofied comment content B' }",
            "[]");
    }
    @Test
    public void test01e() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2], target: 'hf1', text: 'Mofied comment content A' }",
            "{ name: 'changeComment', start: [1, 2], target: 'hf2', text: 'Mofied comment content B' }",
            "{ name: 'changeComment', start: [1, 2], target: 'hf2', text: 'Mofied comment content B' }",
            "{ name: 'changeComment', start: [1, 2], target: 'hf1', text: 'Mofied comment content A' }");
    }
/*
    // changeComment and local changeComment
    it('should calculate valid transformed changeComment operation after local changeComment operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content A' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content A' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content A' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content B' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content B' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content B' }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf2', text: 'Mofied comment content B' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf2', text: 'Mofied comment content B' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], target: 'hf1', text: 'Mofied comment content A' }], transformedOps);
    });
*/

    @Test
    public void test02a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [0, 2] }",
            "{ name: 'delete', start: [0, 2] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test02b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'changeComment', start: [1, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test02c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2], end: [1, 3] }",
            "{ name: 'delete', start: [1, 2], end: [1, 3] }",
            "{ name: 'changeComment', start: [1, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test02d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2], end: [1, 4] }",
            "{ name: 'delete', start: [1, 2], end: [1, 4] }",
            "[]");
    }

    @Test
    public void test02e() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2], end: [2, 2] }",
            "{ name: 'delete', start: [1, 2], end: [2, 2] }",
            "[]");
    }

    @Test
    public void test02f() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 5], end: [1, 9] }",
            "{ name: 'delete', start: [1, 5], end: [1, 9] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test02g() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 3], end: [1, 3] }",
            "{ name: 'delete', start: [1, 3], end: [1, 3] }",
            "{ name: 'changeComment', start: [1, 3, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test02h() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 4], end: [1, 4] }",
            "{ name: 'delete', start: [1, 4], end: [1, 4] }",
            "[]");
    }

    @Test
    public void test02i() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [0], end: [0] }",
            "{ name: 'delete', start: [0], end: [0] }",
            "{ name: 'changeComment', start: [0, 4, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test02j() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 2, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'delete', start: [1], end: [1] }",
            "[]");
    }

    @Test
    public void test02k() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 2, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'changeComment', start: [1, 1, 2, 4, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test02l() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }",
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test02m() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], target: 'hf1', text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }",
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }",
            "[]");
    }

    @Test
    public void test02n() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], target: 'hf2', text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }",
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], target: 'hf2', text: 'Mofied comment content' }");
    }

/*
    // changeComment and local delete
    it('should calculate valid transformed changeComment operation after local delete operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 3] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 3] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2] }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 5], end: [1, 9] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 5], end: [1, 9] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 3] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 3] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 3, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 4] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 4] }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0], end: [0] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0], end: [0] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [0, 4, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 4, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [1] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [1] }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 4, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 2, 4, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content', target: 'hf1' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content', target: 'hf2' };
        localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content', target: 'hf2' }], transformedOps);
    });
*/

    @Test
    public void test03a() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [0, 2] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [0, 2] }");
    }

    @Test
    public void test03b() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 3], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2] }");
    }

    @Test
    public void test03c() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 2], end: [1, 3] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2], end: [1, 3] }");
    }

    @Test
    public void test03d() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 2], end: [1, 4] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "[]",
            "{ name: 'delete', start: [1, 2], end: [1, 4] }");
    }

    @Test
    public void test03e() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 2], end: [2, 2] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "[]",
            "{ name: 'delete', start: [1, 2], end: [2, 2] }");
    }

    @Test
    public void test03f() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 5], end: [1, 9] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 5], end: [1, 9] }");
    }

    @Test
    public void test03g() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 3], end: [1, 3] }",
            "{ name: 'changeComment', start: [1, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 3, 2], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 3], end: [1, 3] }");
    }

    @Test
    public void test03h() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 4], end: [1, 4] }",
            "{ name: 'changeComment', start: [1, 4, 2], text: 'Mofied comment content' }",
            "[]",
            "{ name: 'delete', start: [1, 4], end: [1, 4] }");
    }

    @Test
    public void test03i() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [0], end: [0] }",
            "{ name: 'changeComment', start: [1, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [0, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [0], end: [0] }");
    }

    @Test
    public void test03j() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'changeComment', start: [1, 4, 2, 4, 2], text: 'Mofied comment content' }",
            "[]",
            "{ name: 'delete', start: [1], end: [1] }");
    }

    @Test
    public void test03k() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 1], end: [1, 3] }",
            "{ name: 'changeComment', start: [1, 4, 2, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 1, 2, 4, 2], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 1], end: [1, 3] }");
    }

    @Test
    public void test03l() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }");
    }

    @Test
    public void test03m() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], target: 'hf1', text: 'Mofied comment content' }",
            "[]",
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }");
    }

    @Test
    public void test03n() {
        TransformerTest.transformText(
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], target: 'hf2', text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], target: 'hf2', text: 'Mofied comment content' }",
            "{ name: 'delete', start: [1, 2], end: [2, 2], target: 'hf1' }");
    }

/*
    // changeComment and external delete
    it('should calculate valid transformed changeComment operation after external delete operation', function () {

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0, 2] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [0, 2] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 3] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 3] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 5], end: [1, 9] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 5], end: [1, 9] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 3] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 3, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 3] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 4] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 4] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0], end: [0] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [0, 4, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [0], end: [0] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [1] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 4, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1], end: [1] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 4, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 2, 4, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 3] }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf1', text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }], transformedOps);

        oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [2, 2], target: 'hf1' }], transformedOps);
    });
*/

    @Test
    public void test04a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'deleteHeaderFooter', id: 'hf1' }",
            "{ name: 'deleteHeaderFooter', id: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test04b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], target: 'hf1', text: 'Mofied comment content' }",
            "{ name: 'deleteHeaderFooter', id: 'hf1' }",
            "{ name: 'deleteHeaderFooter', id: 'hf1' }",
            "[]");
    }

    @Test
    public void test04c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], target: 'hf2', text: 'Mofied comment content' }",
            "{ name: 'deleteHeaderFooter', id: 'hf1' }",
            "{ name: 'deleteHeaderFooter', id: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], target: 'hf2', text: 'Mofied comment content' }");
    }

/*
    // changeComment and local deleteHeaderFooter
    it('should calculate valid transformed changeComment operation after local deleteHeaderFooter operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf1', text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }], localActions);
        expectOp([], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test05a() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'deleteHeaderFooter', id: 'hf1' }");
    }

    @Test
    public void test05b() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], target: 'hf1', text: 'Mofied comment content' }",
            "[]",
            "{ name: 'deleteHeaderFooter', id: 'hf1' }");
    }

    @Test
    public void test05c() {
        TransformerTest.transformText(
            "{ name: 'deleteHeaderFooter', id: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], target: 'hf2', text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], target: 'hf2', text: 'Mofied comment content' }",
            "{ name: 'deleteHeaderFooter', id: 'hf1' }");
    }

/*
    // changeComment and external deleteHeaderFooter
    it('should calculate valid transformed changeComment operation after external deleteHeaderFooter operation', function () {

        oneOperation = { name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf1', text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [] }], localActions);
        expectOp([{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], target: 'hf2', text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'deleteHeaderFooter', id: 'hf1', opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test06a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [0, 3], text: '123' }",
            "{ name: 'insertText', start: [0, 3], text: '123' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test06b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 3], text: '123' }",
            "{ name: 'insertText', start: [1, 3], text: '123' }",
            "{ name: 'changeComment', start: [1, 7], text: 'Mofied comment content' }");
    }

    @Test
    public void test06c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 4], text: '123' }",
            "{ name: 'insertText', start: [1, 4], text: '123' }",
            "{ name: 'changeComment', start: [1, 7], text: 'Mofied comment content' }");
    }

    @Test
    public void test06d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 5], text: '123' }",
            "{ name: 'insertText', start: [1, 5], text: '123' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test06e() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 3], target: 'hf1', text: '123' }",
            "{ name: 'insertText', start: [1, 3], target: 'hf1', text: '123' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test06f() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 3], text: '123' }",
            "{ name: 'insertText', start: [1, 3], text: '123' }",
            "{ name: 'changeComment', start: [1, 7, 2, 3], text: 'Mofied comment content' }");
    }

/*
    // changeComment and local insertText
    it('should calculate valid transformed changeComment operation after local insertText operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertText', start: [0, 3], text: '123', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertText', start: [0, 3], text: '123', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertText', start: [1, 4], text: '123', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertText', start: [1, 4], text: '123', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertText', start: [1, 5], text: '123', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertText', start: [1, 5], text: '123', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertText', start: [1, 3], text: '123', target: 'hf1', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertText', start: [1, 3], text: '123', target: 'hf1', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7, 2, 3], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test07a() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [0, 3], text: '123' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [0, 3], text: '123' }");
    }

    @Test
    public void test07b() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [1, 3], text: '123' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 7], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 3], text: '123' }");
    }

    @Test
    public void test07c() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [1, 4], text: '123' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 7], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 4], text: '123' }");
    }

    @Test
    public void test07d() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [1, 5], text: '123' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 5], text: '123' }");
    }

    @Test
    public void test07e() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [1, 3], target: 'hf1', text: '123' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 3], target: 'hf1', text: '123' }");
    }

    @Test
    public void test07f() {
        TransformerTest.transformText(
            "{ name: 'insertText', start: [1, 3], text: '123' }",
            "{ name: 'changeComment', start: [1, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 7, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertText', start: [1, 3], text: '123' }");
    }

/*
    // changeComment and external insertText
    it('should calculate valid transformed changeComment operation after external insertText operation', function () {

        oneOperation = { name: 'insertText', start: [0, 3], text: '123', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertText', start: [0, 3], text: '123', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertText', start: [1, 4], text: '123', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertText', start: [1, 4], text: '123', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertText', start: [1, 5], text: '123', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertText', start: [1, 5], text: '123', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertText', start: [1, 3], text: '123', target: 'hf1', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertText', start: [1, 3], text: '123', target: 'hf1', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 7, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertText', start: [1, 3], text: '123', opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test08a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertParagraph', start: [0] }",
            "{ name: 'insertParagraph', start: [0] }",
            "{ name: 'changeComment', start: [2, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test08b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'changeComment', start: [2, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test08c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test08d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertParagraph', start: [1], target: 'hf1' }",
            "{ name: 'insertParagraph', start: [1], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

/*
    // changeComment and local insertParagraph
    it('should calculate valid transformed changeComment operation after local insertParagraph operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertParagraph', start: [0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertParagraph', start: [0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertParagraph', start: [1], target: 'hf1', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertParagraph', start: [1], target: 'hf1', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test09a() {
        TransformerTest.transformText(
            "{ name: 'insertParagraph', start: [0] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 4], text: 'Mofied comment content' }",
            "{ name: 'insertParagraph', start: [0] }");
    }

    @Test
    public void test09b() {
        TransformerTest.transformText(
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 4], text: 'Mofied comment content' }",
            "{ name: 'insertParagraph', start: [1] }");
    }

    @Test
    public void test09c() {
        TransformerTest.transformText(
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertParagraph', start: [2] }");
    }

    @Test
    public void test09d() {
        TransformerTest.transformText(
            "{ name: 'insertParagraph', start: [1], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertParagraph', start: [1], target: 'hf1' }");
    }

/*
    // changeComment and external insertParagraph
    it('should calculate valid transformed changeComment operation after external insertParagraph operation', function () {

        oneOperation = { name: 'insertParagraph', start: [0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertParagraph', start: [0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertParagraph', start: [1], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertParagraph', start: [2], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertParagraph', start: [2], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertParagraph', start: [1], target: 'hf1', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertParagraph', start: [1], target: 'hf1', opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test10a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertTable', start: [0] }",
            "{ name: 'insertTable', start: [0] }",
            "{ name: 'changeComment', start: [2, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test10b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'changeComment', start: [2, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test10c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test10d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertTable', start: [1], target: 'hf1' }",
            "{ name: 'insertTable', start: [1], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

/*
    // changeComment and local insertTable
    it('should calculate valid transformed changeComment operation after local insertTable operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertTable', start: [0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertTable', start: [0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertTable', start: [2], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertTable', start: [2], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertTable', start: [1], target: 'hf1', opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertTable', start: [1], target: 'hf1', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test11a() {
        TransformerTest.transformText(
            "{ name: 'insertTable', start: [0] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 4], text: 'Mofied comment content' }",
            "{ name: 'insertTable', start: [0] }");
    }

    @Test
    public void test11b() {
        TransformerTest.transformText(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 4], text: 'Mofied comment content' }",
            "{ name: 'insertTable', start: [1] }");
    }

    @Test
    public void test11c() {
        TransformerTest.transformText(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertTable', start: [2] }");
    }

    @Test
    public void test11d() {
        TransformerTest.transformText(
            "{ name: 'insertTable', start: [1], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'insertTable', start: [1], target: 'hf1' }");
    }

/*
    // changeComment and external insertTable
    it('should calculate valid transformed changeComment operation after external insertTable operation', function () {

        oneOperation = { name: 'insertTable', start: [0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertTable', start: [0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertTable', start: [1], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertTable', start: [1], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertTable', start: [2], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'insertTable', start: [1], target: 'hf1', opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertTable', start: [1], target: 'hf1', opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test12a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test12b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test12c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 1], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'changeComment', start: [1, 1], text: 'Mofied comment content' }");
    }

    @Test
    public void test12d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [2, 2, 4, 5], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'changeComment', start: [1, 4, 4, 5], text: 'Mofied comment content' }");
    }

    @Test
    public void test12e() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 2 }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test12f() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2 }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }");
    }

/*
    // changeComment and local mergeParagraph
    it('should calculate valid transformed changeComment operation after local mergeParagraph operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 1], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', start: [1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 4, 5], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', start: [1, 4, 4, 5], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test13a() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }");
    }

    @Test
    public void test13b() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }");
    }

    @Test
    public void test13c() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'changeComment', start: [1, 1], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 1], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }");
    }

    @Test
    public void test13d() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'changeComment', start: [2, 2, 4, 5], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4, 4, 5], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }");
    }

    @Test
    public void test13e() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 2 }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 2 }");
    }

    @Test
    public void test13f() {
        TransformerTest.transformText(
            "{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2 }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2 }");
    }

/*
    // changeComment and external mergeParagraph
    it('should calculate valid transformed changeComment operation after external mergeParagraph operation', function () {

        oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', start: [1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 4, 5], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 4, 5], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'mergeParagraph', start: [1, 4, 4], paralength: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'mergeParagraph', start: [1, 4, 4], paralength: 2, opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'mergeParagraph', start: [1], target: 'hf1', paralength: 2, opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test14a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }");
    }

    @Test
    public void test14b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test14c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 1, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'changeComment', start: [1, 1, 0, 1, 1], text: 'Mofied comment content' }");
    }

/*
    // changeComment and local mergeTable
    it('should calculate valid transformed changeComment operation after local mergeTable operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 0, 1, 1], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', start: [1, 1, 0, 1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }], transformedOps);
    });
*/


    @Test
    public void test15a() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'changeComment', start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }");
    }

    @Test
    public void test15b() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }");
    }

    @Test
    public void test15c() {
        TransformerTest.transformText(
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'changeComment', start: [1, 1, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 1, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }");
    }
/*
    // changeComment and external mergeTable
    it('should calculate valid transformed changeComment operation after external mergeTable operation', function () {

        oneOperation = { name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 0, 1, 1], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', start: [1, 1, 0, 1, 1], text: 'Mofied comment content', opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test16a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test16b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test16c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'splitParagraph', start: [1, 4] }",
            "{ name: 'splitParagraph', start: [1, 4] }",
            "{ name: 'changeComment', start: [2, 0], text: 'Mofied comment content' }");
    }

    @Test
    public void test16d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'splitParagraph', start: [1, 2], target: 'hf1' }",
            "{ name: 'splitParagraph', start: [1, 2], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }");
    }
/*
    // changeComment and local splitParagraph
    it('should calculate valid transformed changeComment operation after local splitParagraph operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test17a() {
        TransformerTest.transformText(
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 2], text: 'Mofied comment content' }",
            "{ name: 'splitParagraph', start: [1, 2] }");
    }

    @Test
    public void test17b() {
        TransformerTest.transformText(
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'splitParagraph', start: [1, 6] }");
    }

    @Test
    public void test17c() {
        TransformerTest.transformText(
            "{ name: 'splitParagraph', start: [1, 4] }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 0], text: 'Mofied comment content' }",
            "{ name: 'splitParagraph', start: [1, 4] }");
    }

    @Test
    public void test17d() {
        TransformerTest.transformText(
            "{ name: 'splitParagraph', start: [1, 2], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4], text: 'Mofied comment content' }",
            "{ name: 'splitParagraph', start: [1, 2], target: 'hf1' }");
    }
/*
    // changeComment and external splitParagraph
    it('should calculate valid transformed changeComment operation after external splitParagraph operation', function () {

        oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] }], transformedOps);

        oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }], transformedOps);

        oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] }], transformedOps);

        oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2], target: 'hf1' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }], transformedOps);
    });
*/

    @Test
    public void test18a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'changeComment', start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }");
    }

    @Test
    public void test18b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'splitTable', start: [1, 6] }",
            "{ name: 'splitTable', start: [1, 6] }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }");
    }

    @Test
    public void test18c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'splitTable', start: [1, 4] }",
            "{ name: 'splitTable', start: [1, 4] }",
            "{ name: 'changeComment', start: [2, 0, 0, 1, 1], text: 'Mofied comment content' }");
    }

    @Test
    public void test18d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'splitTable', start: [1, 2], target: 'hf1' }",
            "{ name: 'splitTable', start: [1, 2], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }");
    }
/*
    // changeComment and local splitTable
    it('should calculate valid transformed changeComment operation after local splitTable operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0, 0, 1, 1], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test19a() {
        TransformerTest.transformText(
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'splitTable', start: [1, 2] }");
    }

    @Test
    public void test19b() {
        TransformerTest.transformText(
            "{ name: 'splitTable', start: [1, 6] }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'splitTable', start: [1, 6] }");
    }

    @Test
    public void test19c() {
        TransformerTest.transformText(
            "{ name: 'splitTable', start: [1, 4] }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 0, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'splitTable', start: [1, 4] }");
    }

    @Test
    public void test19d() {
        TransformerTest.transformText(
            "{ name: 'splitTable', start: [1, 2], target: 'hf1' }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }",
            "{ name: 'splitTable', start: [1, 2], target: 'hf1' }");
    }
/*
    // changeComment and external splitTable
    it('should calculate valid transformed changeComment operation after external splitTable operation', function () {

        oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 2, 0, 1, 1], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2] }], transformedOps);

        oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 6] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 6] }], transformedOps);

        oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0, 0, 1, 1], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 4] }], transformedOps);

        oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2], target: 'hf1' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 0, 1, 1], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'splitTable', opl: 1, osn: 1, start: [1, 2], target: 'hf1' }], transformedOps);
    });
*/

    @Test
    public void test20a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 8, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 11, 4, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test20b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 5, 4, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test20c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test20d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [1, 2], target: 'hf1', count: 3 }",
            "{ name: 'insertRows', start: [1, 2], target: 'hf1', count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test20e() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [0, 2], count: 3 }",
            "{ name: 'insertRows', start: [0, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test20f() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [2, 2], count: 3 }",
            "{ name: 'insertRows', start: [2, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }");
    }
/*
    // changeComment and local insertRows
    it('should calculate valid transformed changeComment operation after local insertRows operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'hf1' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'hf1' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test210a() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 8, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 11, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }");
    }

    @Test
    public void test21b() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 5, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }");
    }

    @Test
    public void test21c() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [1, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [1, 2], count: 3 }");
    }

    @Test
    public void test21d() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [1, 2], target: 'hf1', count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [1, 2], target: 'hf1', count: 3 }");
    }

    @Test
    public void test21e() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [0, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [0, 2], count: 3 }");
    }

    @Test
    public void test21f() {
        TransformerTest.transformText(
            "{ name: 'insertRows', start: [2, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertRows', start: [2, 2], count: 3 }");
    }
/*
    // changeComment and external insertRows
    it('should calculate valid transformed changeComment operation after external insertRows operation', function () {

        oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 8, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 11, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

        oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 5, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

        oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 1, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3 }], transformedOps);

        oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'hf1' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [1, 2], count: 3, target: 'hf1' }], transformedOps);

        oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [0, 2], count: 3 }], transformedOps);

        oneOperation = { name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertRows', opl: 1, osn: 1, start: [2, 2], count: 3 }], transformedOps);
    });
*/

    @Test
    public void test22a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 7, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test22b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [1, 2, 5], count: 3 }",
            "{ name: 'insertCells', start: [1, 2, 5], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test22c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test22d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [1, 2, 2], target: 'hf1', count: 3 }",
            "{ name: 'insertCells', start: [1, 2, 2], target: 'hf1', count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test22e() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [0, 2, 2], count: 3 }",
            "{ name: 'insertCells', start: [0, 2, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }");
    }

    @Test
    public void test22f() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 3 }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }");
    }
/*
    // changeComment and local insertCells
    it('should calculate valid transformed changeComment operation after local insertCells operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'hf1' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'hf1' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test23a() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2, 7, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [1, 2, 2], count: 3 }");
    }

    @Test
    public void test23b() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [1, 2, 5], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [1, 2, 5], count: 3 }");
    }

    @Test
    public void test23c() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [1, 1, 2], count: 3 }");
    }

    @Test
    public void test23d() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [1, 2, 2], target: 'hf1', count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [1, 2, 2], target: 'hf1', count: 3 }");
    }

    @Test
    public void test23e() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [0, 2, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [0, 2, 2], count: 3 }");
    }

    @Test
    public void test23f() {
        TransformerTest.transformText(
            "{ name: 'insertCells', start: [2, 2, 2], count: 3 }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }",
            "{ name: 'insertCells', start: [2, 2, 2], count: 3 }");
    }
/*
    // changeComment and external insertCells
    it('should calculate valid transformed changeComment operation after external insertCells operation', function () {

        oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 7, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3 }], transformedOps);

        oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 5], count: 3 }], transformedOps);

        oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 1, 2], count: 3 }], transformedOps);

        oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'hf1' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [1, 2, 2], count: 3, target: 'hf1' }], transformedOps);

        oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [0, 2, 2], count: 3 }], transformedOps);

        oneOperation = { name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 2, 4, 2, 3], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertCells', opl: 1, osn: 1, start: [2, 2, 2], count: 3 }], transformedOps);
    });
*/

    @Test
    public void test24a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'changeComment', start: [1, 4, 4, 2, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test24b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'changeComment', start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test24c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'changeComment', start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }");
    }
/*
    // changeComment and local insertColumn
    it('should calculate valid transformed changeComment operation after local insertColumn operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 4, 2, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test25a() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'changeComment', start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4, 4, 2, 2], text: 'Mofied comment content' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
    }

    @Test
    public void test25b() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }",
            "{ name: 'changeComment', start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }");
    }

    @Test
    public void test25c() {
        TransformerTest.transformText(
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }",
            "{ name: 'changeComment', start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }",
            "{ name: 'insertColumn', start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }");
    }
/*
    // changeComment and external insertColumn
    it('should calculate valid transformed changeComment operation after external insertColumn operation', function () {

        oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 4, 2, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }], transformedOps);

        oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'behind' }], transformedOps);

        oneOperation = { name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 2, 2, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [1, 4, 3, 2, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'insertColumn', opl: 1, osn: 1, start: [1], tableGrid: [1, 1, 2, 2], gridPosition: 2, insertMode: 'before' }], transformedOps);
    })
*/

    @Test
    public void test26a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'changeComment', start: [3, 1], text: 'Mofied comment content' }");
    }

    @Test
    public void test26b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test26c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2, 1, 4], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0] }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0] }",
            "{ name: 'changeComment', start: [2, 0, 1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test26d() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2, 1, 4], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0] }",
            "{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0] }",
            "{ name: 'changeComment', start: [3, 1, 1, 4], text: 'Mofied comment content' }");
    }

    @Test
    public void test26e() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test26f() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2] }",
            "{ name: 'changeComment', start: [3, 1], text: 'Mofied comment content' }");
    }

    @Test
    public void test26g() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'changeComment', start: [3, 1], text: 'Mofied comment content' }");
    }
/*
    // changeComment and local move
    it('should calculate valid transformed changeComment operation after local move operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2, 1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0, 1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2, 1, 4], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1, 1, 4], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test27a() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [3, 1], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0] }");
    }

    @Test
    public void test27b() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0] }");
    }

    @Test
    public void test27c() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0] }",
            "{ name: 'changeComment', start: [3, 2, 1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [2, 0, 1, 4], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0] }");
    }

    @Test
    public void test27d() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0] }",
            "{ name: 'changeComment', start: [3, 2, 1, 4], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [3, 1, 1, 4], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0] }");
    }

    @Test
    public void test27e() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1] }");
    }

    @Test
    public void test27f() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [3, 1], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2] }");
    }

    @Test
    public void test27g() {
        TransformerTest.transformText(
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [3, 1], text: 'Mofied comment content' }",
            "{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3] }");
    }
/*
    // changeComment and external move
    it('should calculate valid transformed changeComment operation after external move operation', function () {

        oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 4], end: [3, 4], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2, 1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [2, 0, 1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 2], end: [3, 2], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2, 1, 4], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1, 1, 4], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 1], end: [3, 1], to: [2, 0], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 1], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 2], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 1], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'move', start: [3, 0], end: [3, 0], to: [3, 3], opl: 1, osn: 1 }], transformedOps);
    });
*/

    @Test
    public void test28a() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'setAttributes', start: [3], end: [3] }",
            "{ name: 'setAttributes', start: [3], end: [3] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test28b() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'setAttributes', start: [3, 1], end: [3, 3] }",
            "{ name: 'setAttributes', start: [3, 1], end: [3, 3] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }");
    }

    @Test
    public void test28c() {
        TransformerTest.transformText(
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 2] }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 2] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }");
    }
/*
    // changeComment and local setAttributes
    it('should calculate valid transformed changeComment operation after local setAttributes operation', function () {

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'setAttributes', start: [3, 1], end: [3, 3], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'setAttributes', start: [3, 1], end: [3, 3], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);

        oneOperation = { name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' };
        localActions = [{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'setAttributes', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }] }], localActions);
        expectOp([{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }], transformedOps);
    });
*/

    @Test
    public void test29a() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [3], end: [3] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'setAttributes', start: [3], end: [3] }");
    }

    @Test
    public void test29b() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [3, 1], end: [3, 3] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'setAttributes', start: [3, 1], end: [3, 3] }");
    }

    @Test
    public void test29c() {
        TransformerTest.transformText(
            "{ name: 'setAttributes', start: [3, 2], end: [3, 2] }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'changeComment', start: [3, 2], text: 'Mofied comment content' }",
            "{ name: 'setAttributes', start: [3, 2], end: [3, 2] }");
    }
/*
    // changeComment and external setAttributes
    it('should calculate valid transformed changeComment operation after external setAttributes operation', function () {

        oneOperation = { name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'setAttributes', start: [3], end: [3], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 3], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'setAttributes', start: [3, 1], end: [3, 3], opl: 1, osn: 1 }], transformedOps);

        oneOperation = { name: 'setAttributes', start: [3, 2], end: [3, 2], opl: 1, osn: 1 };
        localActions = [{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }];
        transformedOps = otManager.transformOperation(oneOperation, localActions);
        expectAction([{ operations: [{ name: 'changeComment', opl: 1, osn: 1, start: [3, 2], text: 'Mofied comment content' }] }], localActions);
        expectOp([{ name: 'setAttributes', start: [3, 2], end: [3, 2], opl: 1, osn: 1 }], transformedOps);
    });
*/
}
