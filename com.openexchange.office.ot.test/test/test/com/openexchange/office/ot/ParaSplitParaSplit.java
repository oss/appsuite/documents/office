/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ParaSplitParaSplit {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'splitParagraph', start: [1, 10] }",
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'splitParagraph', start: [1, 8] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]); // modified
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'splitParagraph', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 6]); // not modified
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'splitParagraph', start: [2, 0] }",
            "{ name: 'splitParagraph', start: [1, 6] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] }] }]; // same position
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6]); // external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0]); // local operation is modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external splitParagraph operation not has the marker for local ignoring
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 8] }",
            "{ name: 'splitParagraph', start: [1, 10] }",
            "{ name: 'splitParagraph', start: [2, 10] }",
            "{ name: 'splitParagraph', start: [0, 8] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 10]); // modified
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 2, 2, 4, 2] }",
            "{ name: 'splitParagraph', start: [1, 10] }",
            "{ name: 'splitParagraph', start: [1, 10] }",
            "{ name: 'splitParagraph', start: [0, 2, 2, 4, 2]  }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2, 2, 4, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]); // not modified
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 2, 2, 4, 2] }",
            "{ name: 'splitParagraph', start: [0, 2, 2, 4, 1] }",
            "{ name: 'splitParagraph', start: [0, 2, 2, 4, 1] }",
            "{ name: 'splitParagraph', start: [0, 2, 2, 5, 1] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 2, 2, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2, 2, 4, 1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2, 2, 5, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2, 2, 4, 1]); // not modified
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 2, 1, 4, 2] }",
            "{ name: 'splitParagraph', start: [0, 2, 2, 3, 1] }",
            "{ name: 'splitParagraph', start: [0, 2, 2, 3, 1] }",
            "{ name: 'splitParagraph', start: [0, 2, 1, 4, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 2, 1, 4, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2, 2, 3, 1] }] }]; // neighbour cell
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 2, 1, 4, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2, 2, 3, 1]); // not modified
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'splitParagraph', start: [0, 10] }",
            "{ name: 'splitParagraph', start: [0, 10] }",
            "{ name: 'splitParagraph', start: [2, 8] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [0, 10] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 10]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'splitParagraph', start: [2, 3, 2, 2] }",
            "{ name: 'splitParagraph', start: [1, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 2, 2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5] }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'splitParagraph', start: [2, 0, 2, 2] }",
            "{ name: 'splitParagraph', start: [1, 5] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 2]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 7] }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'splitParagraph', start: [1, 7] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [1, 7], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 2, 2]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 7] }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'splitParagraph', start: [2, 5, 2, 2] }",
            "{ name: 'splitParagraph', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [0, 7], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 2, 2]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 0] }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'splitParagraph', start: [2, 0] }");
            /*
                oneOperation = { name: 'splitParagraph', start: [2, 0], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 2, 2]);
             */
    }
}
