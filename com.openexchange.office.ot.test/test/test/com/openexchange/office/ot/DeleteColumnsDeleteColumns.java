/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class DeleteColumnsDeleteColumns {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([4]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "[]",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 2, 6, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 2, 3, 2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 6, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 3, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 2, 0, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 2, 0, 2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 0, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 0, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 2, 1, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 1, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 1, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 2, 2, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 2, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 2, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 3, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 2, 4, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 2, 1, 2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 4, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 1, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 2, 2, 2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 2, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 5, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 2, 5, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [4], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([4]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 2, 5, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 5, 2, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 5, 2, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 5, 2, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 2, 1]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3 }",
            "[]",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3, 5, 2, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3, 5, 2, 2]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([3, 5, 2, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 3, 0, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 0, 1]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([1]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [4, 2, 4, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4, 2, 4, 2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [4, 2, 4, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([3]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([4, 2, 4, 2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2, 5, 4, 0, 0, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 5, 1, 0, 0, 1, 1], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2, 5, 4, 0, 0, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 5, 1, 0, 0, 1, 1]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2, 5, 3, 0, 0, 1, 1], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2, 5, 3, 0, 0, 1, 1], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 5, 3, 0, 0, 1, 1]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 3, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 0]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }


    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(1);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "[]",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2, 1, 1, 0], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2, 1, 1, 0], startGrid: 1, endGrid: 3 }",
            "[]",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2, 1, 1, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2, 1, 1, 0], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 0]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2, 1, 1, 0]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 7 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "[]",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 7, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(4);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 7 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 7, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(4);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 8 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 8, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(2);
    expect(localActions[0].operations[0].endGrid).to.equal(5);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(3);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 8 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 4, endGrid: 7 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 8, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(4);
    expect(localActions[0].operations[0].endGrid).to.equal(7);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(1);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 8 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 5 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 8, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(5);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 2 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 3, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(3);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(2);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(4);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(1);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 2 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(4);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(2);
    expect(oneOperation.endGrid).to.equal(2);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(4);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(5);
    expect(oneOperation.endGrid).to.equal(5);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(1);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(4);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }",
            "[]",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(4);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(1);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 3, endGrid: 4 }",
            "[]",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 6 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 3, endGrid: 4, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(3);
    expect(localActions[0].operations[0].endGrid).to.equal(4);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(6);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 3, endGrid: 4 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 6 }",
            "[]");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 3, endGrid: 4, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(6);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(3);
    expect(oneOperation.endGrid).to.equal(4);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
         */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(4);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(1);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(4);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(4);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 1 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 8, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(1);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(4);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }",
            "{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 4 }");
        /*
    oneOperation = { name: 'deleteColumns', start: [2], startGrid: 5, endGrid: 9, opl: 1, osn: 1 };
    localActions = [{ operations: [{ name: 'deleteColumns', start: [2], startGrid: 1, endGrid: 5, opl: 1, osn: 1 }] }];
    otManager.transformOperation(oneOperation, localActions);
    expect(localActions[0].operations[0].start).to.deep.equal([2]);
    expect(localActions[0].operations[0].startGrid).to.equal(1);
    expect(localActions[0].operations[0].endGrid).to.equal(4);
    expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
    expect(localActions[0].operations.length).to.equal(1);
    expect(oneOperation.start).to.deep.equal([2]);
    expect(oneOperation.startGrid).to.equal(1);
    expect(oneOperation.endGrid).to.equal(4);
    expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
         */
    }
}
