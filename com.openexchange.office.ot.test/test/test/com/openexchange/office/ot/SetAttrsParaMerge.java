/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class SetAttrsParaMerge {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8] }",    // local operations
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",     // external operations
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }");   // expected external
            /*
                // the external setAttributes is (far) behind the mergeParagraph operation
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 4]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([2, 8]); // the position of the external operation is modified
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 9] }",    // local operations
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",     // external operations
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",     // expected local
            "{ name: 'setAttributes', start: [1, 6], end: [1, 11] }");   // expected external
            /*
                // the external setAttributes is directly behind the local merge
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 9], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 6]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([1, 11]); // the position of the external operation is modified
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 4], end: [1, 6] }",    // local operations
            "{ name: 'mergeParagraph', start: [1], paralength: 8 }",     // external operations
            "{ name: 'mergeParagraph', start: [1], paralength: 8 }",     // expected local
            "{ name: 'setAttributes', start: [1, 4], end: [1, 6] }");   // expected external
            /*
                // the external setAttributes is exactly in the paragraph of the local merge
                oneOperation = { name: 'setAttributes', start: [1, 4], end: [1, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 8, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(8); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 6]); // not modified
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 4], end: [1, 6] }",    // local operations
            "{ name: 'mergeParagraph', start: [2], paralength: 4 }",     // external operations
            "{ name: 'mergeParagraph', start: [2], paralength: 4 }",     // expected local
            "{ name: 'setAttributes', start: [1, 4], end: [1, 6] }");   // expected external
            /*
                // the external setAttributes is in front of the local merge
                oneOperation = { name: 'setAttributes', start: [1, 4], end: [1, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 4, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 6]); // not modified
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2, 2, 1], end: [3, 2, 2, 5] }",    // local operations
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",     // external operations
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",     // expected local
            "{ name: 'setAttributes', start: [2, 2, 2, 1], end: [2, 2, 2, 5] }");   // expected external
            /*
                // the external setAttributes is inside a table cell behind the top level local merge
                oneOperation = { name: 'setAttributes', start: [3, 2, 2, 1], end: [3, 2, 2, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 1]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([2, 2, 2, 5]); // the position of the external operation is modified
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 2, 1], end: [1, 2, 2, 5] }",    // local operations
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",     // external operations
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",     // expected local
            "{ name: 'setAttributes', start: [1, 2, 2, 1], end: [1, 2, 2, 5] }");   // expected external
            /*
                // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
                oneOperation = { name: 'setAttributes', start: [1, 2, 2, 1], end: [1, 2, 2, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 2, 2, 5]); // not modified
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 1], end: [1, 4] }",    // local operations
            "{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3 }",     // external operations
            "{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3 }",     // expected local
            "{ name: 'setAttributes', start: [1, 1], end: [1, 4] }");   // expected external
            /*
                // the external setAttributes is top level and in front of the local merge inside a table cell -> no influence
                oneOperation = { name: 'setAttributes', start: [1, 1], end: [1, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 1]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 4]); // not modified
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 1], end: [3, 6] }",    // local operations
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3 }",     // external operations
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3 }",     // expected local
            "{ name: 'setAttributes', start: [3, 1], end: [3, 6] }");   // expected external
            /*
                // the external setAttributes is top level and behind the local merge inside a table cell -> no influence
                oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 6], paralength: 6, nextparalength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 1]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 6]); // not modified
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 2, 3, 2], end: [1, 2, 2, 3, 6] }",    // local operations
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2 }",     // external operations
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2 }",     // expected local
            "{ name: 'setAttributes', start: [1, 2, 2, 2, 2], end: [1, 2, 2, 2, 6] }");   // expected external
            /*
                // the external setAttributes is (far) behind the local merge inside the same table cell
                oneOperation = { name: 'setAttributes', start: [1, 2, 2, 3, 2], end: [1, 2, 2, 3, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2, 2]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([1, 2, 2, 2, 6]); // the position of the external operation is modified
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 3, 2, 2, 4], end: [1, 3, 2, 2, 7] }",    // local operations
            "{ name: 'mergeParagraph', start: [1, 3, 2, 1], paralength: 3 }",     // external operations
            "{ name: 'mergeParagraph', start: [1, 3, 2, 1], paralength: 3 }",     // expected local
            "{ name: 'setAttributes', start: [1, 3, 2, 1, 7], end: [1, 3, 2, 1, 10] }");   // expected external
            /*
                // the external setAttributes is directly behind the local merge inside the same table cell
                oneOperation = { name: 'setAttributes', start: [1, 3, 2, 2, 4], end: [1, 3, 2, 2, 7], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 1, 7]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([1, 3, 2, 1, 10]); // the position of the external operation is modified
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }",    // local operations
            "{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5 }",     // external operations
            "{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5 }",     // expected local
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }");   // expected external
            /*
                // the external setAttributes and the local merge are in different table cells -> no influence
                oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 1, 2, 6]); // not modified
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }",    // local operations
            "{ name: 'mergeParagraph', start: [0], paralength: 5 }",     // external operations
            "{ name: 'mergeParagraph', start: [0], paralength: 5 }",     // expected local
            "{ name: 'setAttributes', start: [1, 1, 2, 2], end: [1, 1, 2, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([1, 1, 2, 6]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }",    // local operations
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }",     // external operations
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }",     // expected local
            "{ name: 'setAttributes', start: [1, 6, 2, 2], end: [1, 6, 2, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 6, 2, 2]);
                expect(oneOperation.end).to.deep.equal([1, 6, 2, 6]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }",    // local operations
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",     // external operations
            "{ name: 'mergeParagraph', start: [2], paralength: 5 }",     // expected local
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([2, 1, 2, 6]);
             */
    }

    @Test
    public void test14A() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'setAttributes', start: [1] }");
            /*
                oneOperation = { name: 'setAttributes', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test14B() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'setAttributes', start: [2] }",
            "{ name: 'setAttributes', start: [1] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [1], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }], transformedOps);
             */
    }


    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4], end: [3, 8] }",    // local operations
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",     // external operations
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",     // expected local
            "{ name: 'setAttributes', start: [2, 4], end: [2, 8] }");   // expected external
            /*
                // the external setAttributes is (far) behind the mergeParagraph operation
                oneOperation = { name: 'setAttributes', start: [3, 4], end: [3, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 4]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([2, 8]); // the position of the external operation is modified
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 4, 0, 0, 2], end: [3, 4, 0, 0, 6] }",    // local operations
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",     // external operations
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",     // expected local
            "{ name: 'setAttributes', start: [2, 4, 0, 0, 2], end: [2, 4, 0, 0, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [3, 4, 0, 0, 2], end: [3, 4, 0, 0, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 4, 0, 0, 2]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([2, 4, 0, 0, 6]); // the position of the external operation is modified
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 4], end: [2, 9] }",    // local operations
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",     // external operations
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",     // expected local
            "{ name: 'setAttributes', start: [1, 6], end: [1, 11] }");   // expected external
            /*
                // the external setAttributes is directly behind the local merge
                oneOperation = { name: 'setAttributes', start: [2, 4], end: [2, 9], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 6]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([1, 11]); // the position of the external operation is modified
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 4, 0, 0, 2], end: [2, 4, 0, 0, 4] }",    // local operations
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",     // external operations
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",     // expected local
            "{ name: 'setAttributes', start: [1, 6, 0, 0, 2], end: [1, 6, 0, 0, 4] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 4, 0, 0, 2], end: [2, 4, 0, 0, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 6, 0, 0, 2]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([1, 6, 0, 0, 4]); // the position of the external operation is modified
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 4], end: [1, 6] }",    // local operations
            "{ name: 'mergeTable', start: [1], rowcount: 8 }",     // external operations
            "{ name: 'mergeTable', start: [1], rowcount: 8 }",     // expected local
            "{ name: 'setAttributes', start: [1, 4], end: [1, 6] }");   // expected external
            /*
                // the external setAttributes is exactly in the paragraph of the local merge
                oneOperation = { name: 'setAttributes', start: [1, 4], end: [1, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 8, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(8); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 6]); // not modified
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 4, 0, 0, 2], end: [1, 4, 0, 0, 6] }",    // local operations
            "{ name: 'mergeTable', start: [1], rowcount: 8 }",     // external operations
            "{ name: 'mergeTable', start: [1], rowcount: 8 }",     // expected local
            "{ name: 'setAttributes', start: [1, 4, 0, 0, 2], end: [1, 4, 0, 0, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [1, 4, 0, 0, 2], end: [1, 4, 0, 0, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 8, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(8); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 4, 0, 0, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 4, 0, 0, 6]); // not modified
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 4], end: [1, 6] }",    // local operations
            "{ name: 'mergeTable', start: [2], rowcount: 4 }",     // external operations
            "{ name: 'mergeTable', start: [2], rowcount: 4 }",     // expected local
            "{ name: 'setAttributes', start: [1, 4], end: [1, 6] }");   // expected external
            /*
                // the external setAttributes is in front of the local merge
                oneOperation = { name: 'setAttributes', start: [1, 4], end: [1, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 4]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 6]); // not modified
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 4, 0, 0, 2], end: [1, 4, 0, 0, 6] }",    // local operations
            "{ name: 'mergeTable', start: [2], rowcount: 4 }",     // external operations
            "{ name: 'mergeTable', start: [2], rowcount: 4 }",     // expected local
            "{ name: 'setAttributes', start: [1, 4, 0, 0, 2], end: [1, 4, 0, 0, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [1, 4, 0, 0, 2], end: [1, 4, 0, 0, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 4, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 4, 0, 0, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 4, 0, 0, 6]); // not modified
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 2, 2, 1], end: [3, 2, 2, 5] }",    // local operations
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",     // external operations
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",     // expected local
            "{ name: 'setAttributes', start: [2, 2, 2, 1], end: [2, 2, 2, 5] }");   // expected external
            /*
                // the external setAttributes is inside a table cell behind the top level local merge
                oneOperation = { name: 'setAttributes', start: [3, 2, 2, 1], end: [3, 2, 2, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 1]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([2, 2, 2, 5]); // the position of the external operation is modified
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 2, 1], end: [1, 2, 2, 5] }",    // local operations
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",     // external operations
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",     // expected local
            "{ name: 'setAttributes', start: [1, 2, 2, 1], end: [1, 2, 2, 5] }");   // expected external
            /*
                // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
                oneOperation = { name: 'setAttributes', start: [1, 2, 2, 1], end: [1, 2, 2, 5], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 2, 2, 5]); // not modified
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 1], end: [1, 4] }",    // local operations
            "{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3 }",     // external operations
            "{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3 }",     // expected local
            "{ name: 'setAttributes', start: [1, 1], end: [1, 4] }");   // expected external
            /*
                // the external setAttributes is top level and in front of the local merge inside a table cell -> no influence
                oneOperation = { name: 'setAttributes', start: [1, 1], end: [1, 4], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 1]); // not modified
                expect(oneOperation.end).to.deep.equal([1, 4]); // not modified
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [3, 1], end: [3, 6] }",    // local operations
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3 }",     // external operations
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3 }",     // expected local
            "{ name: 'setAttributes', start: [3, 1], end: [3, 6] }");   // expected external
            /*
                // the external setAttributes is top level and behind the local merge inside a table cell -> no influence
                oneOperation = { name: 'setAttributes', start: [3, 1], end: [3, 6], rowcount: 6, nextrowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3, 1]); // not modified
                expect(oneOperation.end).to.deep.equal([3, 6]); // not modified
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 2, 2, 3, 2], end: [1, 2, 2, 3, 6] }",    // local operations
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2 }",     // external operations
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2 }",     // expected local
            "{ name: 'setAttributes', start: [1, 2, 2, 2, 2], end: [1, 2, 2, 2, 6] }");   // expected external
            /*
                // the external setAttributes is (far) behind the local merge inside the same table cell
                oneOperation = { name: 'setAttributes', start: [1, 2, 2, 3, 2], end: [1, 2, 2, 3, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2, 2]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([1, 2, 2, 2, 6]); // the position of the external operation is modified
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [1, 3, 2, 2, 4], end: [1, 3, 2, 2, 7] }",    // local operations
            "{ name: 'mergeTable', start: [1, 3, 2, 1], rowcount: 3 }",     // external operations
            "{ name: 'mergeTable', start: [1, 3, 2, 1], rowcount: 3 }",     // expected local
            "{ name: 'setAttributes', start: [1, 3, 2, 1, 7], end: [1, 3, 2, 1, 10] }");   // expected external
            /*
                // the external setAttributes is directly behind the local merge inside the same table cell
                oneOperation = { name: 'setAttributes', start: [1, 3, 2, 2, 4], end: [1, 3, 2, 2, 7], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 1, 7]); // the position of the external operation is modified
                expect(oneOperation.end).to.deep.equal([1, 3, 2, 1, 10]); // the position of the external operation is modified
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }",    // local operations
            "{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5 }",     // external operations
            "{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5 }",     // expected local
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }");   // expected external
            /*
                // the external setAttributes and the local merge are in different table cells -> no influence
                oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 2]); // not modified
                expect(oneOperation.end).to.deep.equal([2, 1, 2, 6]); // not modified
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }",    // local operations
            "{ name: 'mergeTable', start: [0], rowcount: 5 }",     // external operations
            "{ name: 'mergeTable', start: [0], rowcount: 5 }",     // expected local
            "{ name: 'setAttributes', start: [1, 1, 2, 2], end: [1, 1, 2, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([1, 1, 2, 6]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }",    // local operations
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",     // external operations
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",     // expected local
            "{ name: 'setAttributes', start: [1, 6, 2, 2], end: [1, 6, 2, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 6, 2, 2]);
                expect(oneOperation.end).to.deep.equal([1, 6, 2, 6]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }",    // local operations
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",     // external operations
            "{ name: 'mergeTable', start: [2], rowcount: 5 }",     // expected local
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([2, 1, 2, 6]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }",    // local operations
            "{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 5 }",     // external operations
            "{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 5 }",     // expected local
            "{ name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6] }");   // expected external
            /*
                oneOperation = { name: 'setAttributes', start: [2, 1, 2, 2], end: [2, 1, 2, 6], opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 2]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 2]);
                expect(oneOperation.end).to.deep.equal([2, 1, 2, 6]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } } }",
            "{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0 }",
            "{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0 }",
            "{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } } }");
            // Testing the automatically generated external setAttributes operations with noUndo for font size adaptions (handleSetAttrsMergeComp)
            /*
                oneOperation = { name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }], transformedOps);
            */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0 }",
            "{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } } }",
            "{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } } }",
            "{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0 }");
            // Testing the automatically generated internal setAttributes operations with noUndo for font size adaptions (handleSetAttrsMergeComp)
            /*
                oneOperation = { name: 'mergeParagraph', start: [0, 1, 4], paralength: 0, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'setAttributes', start: [0, 1], noUndo: true, attrs: { shape: { fontScale: 0.4, lineReduction: 0, autoResizeText: true, autoResizeHeight: false, noAutoResize: false } }, opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'mergeParagraph', start: [0, 1, 4], paralength: 0, opl: 1, osn: 1 }], transformedOps);
            */
    }

}
