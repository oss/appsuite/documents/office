/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.tools;

import java.util.Iterator;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;

public class Helper {

    public static String createPropsFromAttrs(JSONObject attrs) {
        try {
            return new JSONObject().put(OCKey.ATTRS.value(), attrs).toString();
        }
        catch(JSONException e) {
            return null;
        }
    }

    public static JSONObject createJSONObject(String v) {
        try {
            return new JSONObject(v);
        }
        catch(JSONException e) {
            return null;
        }
    }

    public static JSONArray createJSONArray(String v) {
        try {
            return new JSONArray(v);
        }
        catch(JSONException e) {
            return null;
        }
    }

    public static JSONArray createJSONArrayFromJSON(Object...json) {
        try {
            final JSONArray a = new JSONArray();
            for(Object o:json) {
                if(o!=null) {
                    if(o instanceof JSONObject) {
                        a.put(o);
                    }
                    else if(o instanceof JSONArray) {
                        for(int i=0; i < ((JSONArray)o).length(); i++) {
                            a.put(((JSONArray)o).get(i));
                        }
                    }
                    else throw new JSONException("createArrayFromJSON::wrong argument");
                }
            }
            return a;
        }
        catch(JSONException e) {
            return null;
        }
    }

    public static String createArrayFromJSON(Object...json) {
        return createJSONArrayFromJSON(json).toString();
    }

    final static public JSONArray createGlobalOps() {
        try {
            return new JSONArray()
                .put(createNoOp())
                .put(createChangeConfigOp(createJSONObject("{ f1: { p2: 42 } }")))
                .put(createInsertFontOp("f1"))
                .put(createInsertThemeOp("t1"));
        }
        catch(JSONException e) {
            return null;
        }
    }

    final static public JSONObject createNoOp() throws JSONException {
        return new JSONObject(1).put(OCKey.NAME.value(), OCValue.NO_OP.value());
    };

    final static public JSONObject createChangeConfigOp(JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.SET_DOCUMENT_ATTRIBUTES.value());
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    };

    final static public JSONObject createInsertFontOp(String fontName) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.INSERT_FONT_DESCRIPTION.value());
        if(fontName!=null && !fontName.isEmpty()) {
            op.put(OCKey.ATTRS.value(), new JSONObject().put(OCKey.FONT_NAME.value(), fontName));
        }
        return op;
    };

    final static public JSONObject createInsertThemeOp(String themeName) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.INSERT_THEME.value()).put(OCKey.COLOR_SCHEME.value(), new JSONObject());
        if(themeName!=null && !themeName.isEmpty()) {
            op.put(OCKey.THEME_NAME.value(), themeName);
        }
        return op;
    };

    final static public JSONArray createStyleOps(String type, String id) {
        try {
            return new JSONArray()
                .put(createInsertStyleOp(type, id, null, null))
                .put(createDeleteStyleOp(type, id))
                .put(createChangeStyleOp(type, id, null, null));
        }
        catch(JSONException e) {
            return null;
        }
    }

    final static public JSONObject createInsertStyleOp(String type, String id, JSONObject attrs, String props) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_STYLE_SHEET.value());
        if(type!=null) {
            op.put(OCKey.TYPE.value(), type);
        }
        if(id!=null) {
            op.put(OCKey.ID.value(), id);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        mergeProps(op, props);
        return op;
    };

    final static public JSONObject createDeleteStyleOp(String type, String id) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_STYLE_SHEET.value());
        if(type!=null) {
            op.put(OCKey.TYPE.value(), type);
        }
        if(id!=null) {
            op.put(OCKey.ID.value(), id);
        }
        return op;
    };

    final static public JSONObject createChangeStyleOp(String type, String id, JSONObject attrs, String props) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_STYLE_SHEET.value());
        if(type!=null) {
            op.put(OCKey.TYPE.value(), type);
        }
        if(id!=null) {
            op.put(OCKey.ID.value(), id);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        mergeProps(op, props);
        return op;
    }

    final static public JSONArray createAutoStyleOps() {
        try {
            return new JSONArray()
                .put(createInsertAutoStyleOp("a1", null, null))
                .put(createDeleteAutoStyleOp("a1"))
                .put(createChangeAutoStyleOp("a1", null));
        }
        catch(JSONException e) {
            return null;
        }
    }

    final static public JSONObject createInsertAutoStyleOp(String styleId, JSONObject attrs, String props) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.INSERT_AUTO_STYLE.value()).put(OCKey.STYLE_ID.value(), styleId);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        mergeProps(op, props);
        return op;
    }

    final static public JSONObject createInsertAutoStyleOp(String type, String styleId, JSONObject attrs, String props) throws JSONException {
        final JSONObject op = createInsertAutoStyleOp(styleId, attrs, props);
        if(null != type) {
          op.put(OCKey.TYPE.value(), type);
        }
        return op;
    }

    final static public JSONObject createDeleteAutoStyleOp(String styleId) throws JSONException {
        return new JSONObject(2).put(OCKey.NAME.value(), OCValue.DELETE_AUTO_STYLE.value()).put(OCKey.STYLE_ID.value(), styleId);
    }

    final static public JSONObject createDeleteAutoStyleOp(String styleId, String props) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.DELETE_AUTO_STYLE.value()).put(OCKey.STYLE_ID.value(), styleId);
        mergeProps(op, props);
        return op;
    }

    final static public JSONObject createDeleteAutoStyleWithTypeOp(String type, String styleId) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_AUTO_STYLE.value()).put(OCKey.TYPE.value(), type).put(OCKey.STYLE_ID.value(), styleId);
        return op;
    }

    final static public JSONObject createDeleteAutoStyleOp(String type, String styleId, String props) throws JSONException {
        final JSONObject op = createDeleteAutoStyleOp(styleId, props);
        if(null != type) {
          op.put(OCKey.TYPE.value(), type);
        }
        return op;
    }

    final static public JSONObject createChangeAutoStyleOp(String styleId, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.CHANGE_AUTO_STYLE.value()).put(OCKey.STYLE_ID.value(), styleId);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    final static public JSONObject createChangeAutoStyleOp(String styleId, JSONObject attrs, String props) throws JSONException {
        final JSONObject op = createChangeAutoStyleOp(styleId, attrs);
        mergeProps(op, props);
        return op;
    }


    final static public JSONObject createChangeAutoStyleOp(String type, String styleId, JSONObject attrs, String props) throws JSONException {
        final JSONObject op = createChangeAutoStyleOp(styleId, attrs, props);
        if(null != type) {
            op.put(OCKey.TYPE.value(), type);
        }
        return op;
    }

    final static public JSONObject createChangeCommentOp(int sheet, String anchor, int index, JSONObject attrs, String text) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_COMMENT.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ANCHOR.value(), anchor).put(OCKey.INDEX.value(), index);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        if (null != text) {
            op.put(OCKey.TEXT.value(), text);
        }
        return op;
    }

    final static public JSONObject createChangeCommentOp(int sheet, String anchor, int index, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_COMMENT.value()).put(OCKey.SHEET.value(), sheet).put(OCKey.ANCHOR.value(), anchor).put(OCKey.INDEX.value(), index);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    final static public JSONObject createInsertDrawingOp(String pos, String type, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        putPosition(op, OCKey.START, pos);
        if(type!=null && !type.isEmpty()) {
            op.put(OCKey.TYPE.value(), type);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    final static public JSONObject createInsertShapeOp(String pos, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        putPosition(op, OCKey.START, pos);
        op.put(OCKey.TYPE.value(), "shape");
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    final static public JSONObject createInsertChartOp(String pos, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        putPosition(op, OCKey.START, pos);
        op.put(OCKey.TYPE.value(), "chart");
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    final static public JSONObject createDeleteDrawingOp(String pos) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.DELETE_DRAWING.value());
        putPosition(op, OCKey.START, pos);
        return op;
    }

    final static public JSONObject createChangeDrawingOp(String pos, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.CHANGE_DRAWING.value());
        putPosition(op, OCKey.START, pos);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "moveDrawing" operation.
     * @throws JSONException
     */
    final static public JSONObject createMoveDrawingOp(String from, String to) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.MOVE_DRAWING.value());
        putPosition(op, OCKey.START, from);
        putPosition(op, OCKey.TO, to);
        return op;
    }

    /**
     * Creates a "moveDrawing" operation from sheets.
     * @throws JSONException
     */
    final static public JSONObject createMoveDrawingFromSheetOp(String from, String to) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.MOVE_DRAWING.value());
        putPosition(op, OCKey.START, from);
        putPosition(op, OCKey.TO, to);
        op.getJSONArray(OCKey.TO.value()).add(0, op.getJSONArray(OCKey.FROM.value()).getInt(0));
        return op;
    }

    /**
     * Creates an "insertChartSeries" operation.
     * @throws JSONException
     */
    final static public JSONObject createInsertChSeriesOp(String pos, int index, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.INSERT_CHART_SERIES.value()).put(OCKey.INDEX.value(), index);
        putPosition(op, OCKey.START, pos);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    };

    /**
     * Creates a "deleteChartSeries" operation.
     * @throws JSONException
     */
    final static public JSONObject createDeleteChSeriesOp(String pos, int index) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_CHART_SERIES.value()).put(OCKey.INDEX.value(), index);
        putPosition(op, OCKey.START, pos);
        return op;
    };

    /**
     * Creates a "changeChartSeries" operation.
     * @throws JSONException
     */
    final static public JSONObject createChangeChSeriesOp(String pos, int index, JSONObject attrs) throws JSONException {
        final JSONObject op =  new JSONObject(4).put(OCKey.NAME.value(), OCValue.DELETE_CHART_SERIES.value()).put(OCKey.INDEX.value(), index);
        putPosition(op, OCKey.START, pos);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "deleteChartAxis" operation.
     * @throws JSONException
     */
    final static public JSONObject createDeleteChAxisOp(String pos, int axis) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE_CHART_AXIS.value()).put(OCKey.AXIS.value(), axis);
        putPosition(op, OCKey.START, pos);
        return op;
    };

    /**
     * Creates a "changeChartAxis" operation.
     * @throws JSONException
     */
    final static public JSONObject createChangeChAxisOp(String pos, int axis, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_CHART_AXIS.value()).put(OCKey.AXIS.value(), axis);
        putPosition(op, OCKey.START, pos);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "changeChartGrid" operation.
     * @throws JSONException
     */
    final static public JSONObject createChangeChGridOp(String pos, int axis, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_CHART_GRID.value()).put(OCKey.AXIS.value(), axis);
        putPosition(op, OCKey.START, pos);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "changeChartTitle" operation.
     * @throws JSONException
     */
    final static public JSONObject createChangeChTitleOp(String pos, Integer axis, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_CHART_TITLE.value());
        if(pos!=null) {
            putPosition(op, OCKey.START, pos);
        }
        if(axis!=null) {
            op.put(OCKey.AXIS.value(), axis);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "changeChartLegend" operation.
     * @throws JSONException
     */
    final static public JSONObject createChangeLegendOp(String pos, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.CHANGE_CHART_TITLE.value());
        putPosition(op, OCKey.START, pos);
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    };

    /**
     * Creates an "insertText" operation for text contents.
     * @throws JSONException
     */
    final static public JSONObject createInsertTextOp(String pos, String text) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.INSERT_TEXT.value()).put(OCKey.TEXT.value(), text);
        putPosition(op, OCKey.START, pos);
        return op;
    }

    /**
     * Creates an "insertTab" operation for text contents.
     * @throws JSONException
     */
    final static public JSONObject createInsertTabOp(String pos) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.INSERT_TAB.value());
        putPosition(op, OCKey.START, pos);
        return op;
    }

    /**
     * Creates an "insertHardBreak" operation for text contents.
     * @throws JSONException
     */
    final static public JSONObject createInsertHardBreakOp(String pos) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.INSERT_HARD_BREAK.value());
        putPosition(op, OCKey.START, pos);
        return op;
    }

    /**
     * Creates an "insertParagraph" operation for text contents.
     * @throws JSONException
     */
    final static public JSONObject createInsertParaOp(String pos) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.INSERT_PARAGRAPH.value());
        putPosition(op, OCKey.START, pos);
        return op;
    }

    /**
     * Creates a "splitParagraph" operation for text contents.
     * @throws JSONException
     */
    final static public JSONObject createSplitParaOp(String pos) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.SPLIT_PARAGRAPH.value());
        putPosition(op, OCKey.START, pos);
        return op;
    }

    /**
     * Creates a "mergeParagraph" operation for text contents.
     * @throws JSONException
     */
    final static public JSONObject createMergeParaOp(String pos) throws JSONException {
        final JSONObject op = new JSONObject(2).put(OCKey.NAME.value(), OCValue.MERGE_PARAGRAPH.value());
        putPosition(op, OCKey.START, pos);
        return op;
    }

    /**
     * Creates a "setAttributes" operation for text contents.
     * @throws JSONException
     */
    final static public JSONObject createSetAttrsOp(String start, JSONObject attrs) throws JSONException {
        return createSetAttrsOp(start, null, attrs);
    }

    final static public JSONObject createSetAttrsOp(String start, String end, JSONObject attrs) throws JSONException {
        final JSONObject op = new JSONObject(4).put(OCKey.NAME.value(), OCValue.SET_ATTRIBUTES.value());
        putPosition(op, OCKey.START, start);
        if(end!=null) {
            putPosition(op, OCKey.END, end);
        }
        if(attrs!=null) {
            op.put(OCKey.ATTRS.value(), attrs);
        }
        return op;
    }

    /**
     * Creates a "delete" operation for text contents.
     * @throws JSONException
     */
    final static public JSONObject createDeleteOp(String start, String end) throws JSONException {
        final JSONObject op = new JSONObject(3).put(OCKey.NAME.value(), OCValue.DELETE.value());
        putPosition(op, OCKey.START, start);
        if(end!=null) {
            putPosition(op, OCKey.END, end);
        }
        return op;
    }

    public static void putPosition(JSONObject op, OCKey key, Object pos) throws JSONException {
        final JSONArray position = new JSONArray();
        if(pos instanceof Integer) {
            position.put(pos);
        }
        else if(pos instanceof String) {
            final String[] positions = ((String)pos).split(" ", -1);
            for(String e:positions) {
                position.put(Integer.parseInt(e));
            }
        }
        op.put(key.value(), position);
    }

    public static void mergeProps(JSONObject dest, String props) throws JSONException {
        if(props!=null && !props.isEmpty()) {
            final JSONObject s = new JSONObject(props);
            final Iterator<Entry<String, Object>> iter = s.entrySet().iterator();
            while(iter.hasNext()) {
                final Entry<String, Object> e = iter.next();
                dest.put(e.getKey(), e.getValue());
            }
        }
    }

    public static int[] createIntArray(String ints) {
        final String[] a = ints.split(" ", -1);
        int[] ret = new int[a.length];
        for(int i = 0; i < a.length; i++) {
            ret[i] = Integer.parseInt(a[i]);
        }
        return ret;
    }
}
