/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.tools;

import java.util.Stack;
import org.json.JSONArray;
import org.json.JSONObject;

public class OpCompareException extends RuntimeException {

    static class Entry {

        final String message;

        public Entry(String message) {
            this.message = message;
        }
    }

    static class OpJSONArray extends Entry {

        final JSONArray a;
        final Integer index;

        public OpJSONArray(String message, JSONArray a, Integer index) {
            super(message);

            this.a = a;
            this.index = index;
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append(message)
                   .append("index:")
                   .append(Integer.valueOf(index).toString())
                   .append(a.toString());
            return builder.toString();
        }
    }

    static class OpJSONObject extends Entry {

        final JSONObject o;

        public OpJSONObject(String message, JSONObject o) {
            super(message);

            this.o = o;
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append(message)
                   .append(o.toString());
            return builder.toString();
        }
    }

    static class OpMessage extends Entry {

        public OpMessage(String message) {
            super(message);
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append(message);
            return builder.toString();
        }
    }

    static class OpJSONArrayCompare extends Entry {

        final JSONArray a1;
        final Integer index1;
        final JSONArray a2;

        public OpJSONArrayCompare(String message, JSONArray a1, Integer index1, JSONArray a2) {
            super(message);

            this.a1 = a1;
            this.index1 = index1;
            this.a2 = a2;
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append(message)
                   .append("index:")
                   .append(Integer.valueOf(index1).toString())
                   .append(a1.toString())
                   .append(a2.toString());
            return builder.toString();
        }
    }

    static class OpJSONObjectCompare extends Entry {

        final JSONObject o1;
        final JSONObject o2;;
        final String key;

        public OpJSONObjectCompare(String message, JSONObject o1, JSONObject o2, String key) {
            super(message);

            this.o1 = o1;
            this.o2 = o2;
            this.key = key;
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append(message)
                   .append("key:")
                   .append(key)
                   .append(o1.toString())
                   .append(o2.toString());
            return builder.toString();
        }
    }

    final Stack<Entry> stack = new Stack<Entry>();

    public OpCompareException(Throwable e) {
        super(e);
    }

    public OpCompareException(Entry entry) {
        super();

        stack.push(entry);
    }

    public static OpCompareException createCompareException(Throwable e, Entry entry) {
        final OpCompareException op;
        if(e instanceof OpCompareException) {
            op = (OpCompareException)e;
        }
        else {
            op = new OpCompareException(e);
        }
        op.stack.push(entry);
        return op;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        while(!stack.isEmpty()) {
            if(builder.length()!=0) {
                builder.append("\n");
            }
            builder.append(stack.pop().toString());
        }
        return builder.toString();
    }
}
