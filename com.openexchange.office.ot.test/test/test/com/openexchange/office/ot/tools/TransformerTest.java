/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot.tools;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.ot.OTException;
import com.openexchange.office.ot.TransformHandlerBasic;
import com.openexchange.office.ot.TransformOptions;
import com.openexchange.office.ot.tools.ITransformHandler;
import com.openexchange.office.ot.tools.ITransformHandlerMap;
import com.openexchange.office.ot.tools.OpPair;
import com.openexchange.office.tools.doc.DocumentFormat;
import com.openexchange.office.tools.doc.DocumentType;

public class TransformerTest {


    // basic transform

    @Test   // creating all basic op combinations and checking them against handler map
    public void checkOpCompletenessBasic() {
        ITransformHandlerMap[] maps = {
            new TransformHandlerBasic()
        };
        checkOpCompleteness(maps);
    }

    /**
     * Change local and external ops and call transform to test
     * the bidi version.
     * @param localOps
     * @param externalOps
     */
    static public void transformBidi(String localOps, String externalOps) {
        String expectedLocal = externalOps;
        String expectedExternal = localOps;
        // transform the bidi version
        transform(expectedLocal, expectedExternal, localOps, externalOps, null);
    }

    /**
     * Change local and external ops and call transform to test
     * the bidi version.
     * @param localOps
     * @param externalOps
     * @param localOps
     * @param externalOps
     */
    static public void transformBidi(String localOps, String externalOps, String expectedLocal, String expectedExternal) {
        // transform the bidi version
        // Note: Due to the test setup by Daniels otmanagers tests
        // expectedLocal and expectedExternal must be exchanged.
        // transform(localOps, externalOps, expectedExternal, expectedLocal, null);
        if (null == expectedExternal ) {
            expectedExternal = localOps;
        }
        if (null == expectedLocal ) {
            expectedLocal = externalOps;
        }
        transform(externalOps, localOps, expectedExternal, expectedLocal, null);
    }

    static public void transform(String localOps, String externalOps) {
        String expectedLocal = externalOps;
        String expectedExternal = localOps;
        transform(localOps, externalOps, expectedLocal, expectedExternal, null);
    }

    static public void transform(String localOps, String externalOps, String expectedLocal, String expectedExternal) {
        transform(localOps, externalOps, expectedLocal, expectedExternal, null);
    }

    static public void transform(String localOps, String externalOps, String expectedLocal, String expectedExternal, ImmutableSet<OCKey> ignorables) {
        ITransformHandlerMap[] maps = {
            new TransformHandlerBasic()
        };
        transform(new TransformOptions(DocumentType.NONE, DocumentFormat.NONE), maps, localOps, externalOps, expectedLocal, expectedExternal, ignorables);
    }

    // text transform

    @Test
    public void checkOpCompletenessText() {
        checkOpCompleteness(com.openexchange.office.ot.text.Transformer.getTransformHandlerMaps());
    }

    static public void transformText(String localOps, String externalOps, String expectedLocal, String expectedExternal) {
        transformText(localOps, externalOps, expectedLocal, expectedExternal, null);
    }

    static public void transformText(String localOps, String externalOps, String expectedLocal, String expectedExternal, ImmutableSet<OCKey> ignorables) {
        transform(new TransformOptions(DocumentType.TEXT, DocumentFormat.DOCX), com.openexchange.office.ot.text.Transformer.getTransformHandlerMaps(), localOps, externalOps, expectedLocal, expectedExternal, ignorables);
    }

    // presentation transform

    @Test
    public void checkOpCompletenessPresentation() {
        checkOpCompleteness(com.openexchange.office.ot.presentation.Transformer.getTransformHandlerMaps());
    }

    static public void transformPresentation(String localOps, String externalOps, String expectedLocal, String expectedExternal) {
        transformPresentation(localOps, externalOps, expectedLocal, expectedExternal, null);
    }

    static public void transformPresentation(String localOps, String externalOps, String expectedLocal, String expectedExternal, ImmutableSet<OCKey> ignorables) {
        transform(new TransformOptions(DocumentType.PRESENTATION, DocumentFormat.PPTX), com.openexchange.office.ot.presentation.Transformer.getTransformHandlerMaps(), localOps, externalOps, expectedLocal, expectedExternal, ignorables);
    }

    // spreadsheet transform

    @Test
    public void checkOpCompletenessSpreadsheet() {
        checkOpCompleteness(com.openexchange.office.ot.spreadsheet.Transformer.getTransformHandlerMaps());
    }

    static public void transformSpreadsheet(String localOps, String externalOps, String expectedLocal, String expectedExternal) {
        transformSpreadsheet(localOps, externalOps, expectedLocal, expectedExternal, null);
    }

    static public void transformSpreadsheet(String localOps, String externalOps, String expectedLocal, String expectedExternal, ImmutableSet<OCKey> ignorables) {
        transform(new TransformOptions(DocumentType.SPREADSHEET, DocumentFormat.XLSX), com.openexchange.office.ot.spreadsheet.Transformer.getTransformHandlerMaps(), localOps, externalOps, expectedLocal, expectedExternal, ignorables);
    }

    // checking completeness of application specific ops against itself and against all basic operations
    static private void checkOpCompleteness(ITransformHandlerMap[] appMaps) {
        final StringBuilder missingHandler = new StringBuilder();
        final Set<OpPair> allOps = new HashSet<OpPair>();
        final Set<OpPair> reqOps = new HashSet<OpPair>();
        final Set<OCValue> allIgnorableOps = new HashSet<OCValue>();
        for(ITransformHandlerMap handler : appMaps) {
            final Set<OCValue> v = handler.getIgnoreOperations();
            if(v!=null) {
                allIgnorableOps.addAll(v);
            }
        }
        for(ITransformHandlerMap handler : appMaps) {
            createAllOpCombinations(allOps, reqOps, handler.getSupportedOperations(), allIgnorableOps);
        }
        checkOpPairs(allOps, reqOps, appMaps, missingHandler);
        if(missingHandler.length()!=0) {
            assertTrue(false, "Missing OT handler:" + missingHandler.toString());
        }
    }

    static void createAllOpCombinations(Set<OpPair> allOps, Set<OpPair> reqOps, Set<OCValue> ops, Set<OCValue> allIgnorableOps) {
        final Iterator<OCValue> opsIterA = ops.iterator();
        while(opsIterA.hasNext()) {
            final OCValue a = opsIterA.next();
            final Iterator<OCValue> opsIterB = ops.iterator();
            while(opsIterB.hasNext()) {
                final OCValue b = opsIterB.next();
                if(allIgnorableOps.contains(a) || allIgnorableOps.contains(b)) {
                    allOps.add(new OpPair(a, b));
                    allOps.add(new OpPair(b, a));
                }
                else {
                    reqOps.add(new OpPair(a, b));
                    reqOps.add(new OpPair(b, a));
                }
            }
        }
    }

    // checks that each op combination in ops is available in map...
    static private void checkOpPairs(Set<OpPair> allOps, Set<OpPair> reqOps, ITransformHandlerMap[] maps, StringBuilder missingHandler) {
        final Iterator<OpPair> opsIter = reqOps.iterator();
        while(opsIter.hasNext()) {
            final OpPair opPair = opsIter.next();
            ITransformHandler handler = null;
            for(ITransformHandlerMap handlerMap : maps) {
                handler = handlerMap.getTransformHandlerMap().get(opPair);
                if(handler!=null) {
                    break;
                }
            }
            if(handler==null) {
                if(missingHandler.length()!=0) {
                    missingHandler.append(',');
                }
                missingHandler.append('"');
                missingHandler.append(opPair.toString());
                missingHandler.append('"');
            }
        }
    }

    // ------------

    static private void transform(TransformOptions options, ITransformHandlerMap[] appHandler, String localOps, String externalOps, String expectedLocal, String expectedExternal, ImmutableSet<OCKey> ignorables) {

        boolean reloadRequested = false;

        JSONArray jsonLocal = null;
        JSONArray jsonExternal = null;
        JSONArray jsonExpectedLocal = null;
        JSONArray jsonExpectedExternal = null;
        JSONArray jsonTransformed = null;

        StringBuilder failMessage = new StringBuilder();

        try {
            jsonLocal = TransformerTest.createJSONArray(localOps);
            jsonExternal = TransformerTest.createJSONArray(externalOps);
            jsonExpectedLocal = TransformerTest.createJSONArray(expectedLocal);
            jsonExpectedExternal = TransformerTest.createJSONArray(expectedExternal);

            reloadRequested = (!jsonExpectedLocal.isEmpty() && jsonExpectedLocal.getJSONObject(0).optBoolean("_CONFLICT_RELOAD_REQUIRED_", false))
                || (!jsonExpectedExternal.isEmpty() && jsonExpectedExternal.getJSONObject(0).optBoolean("_CONFLICT_RELOAD_REQUIRED_", false));

            jsonTransformed = com.openexchange.office.ot.tools.OTUtils.transformOperations(options, appHandler, jsonLocal, jsonExternal);

            try {
                compare(jsonExpectedLocal, jsonTransformed, ignorables);
            }
            catch(Exception e) {
                throw OpCompareException.createCompareException(e, new OpCompareException.OpMessage("compareLocal:"));
            }
            try {
                compare(jsonLocal, jsonExpectedExternal, ignorables);
            }
            catch(Exception e) {
                throw OpCompareException.createCompareException(e, new OpCompareException.OpMessage("compareExternal:"));
            }
        }
        catch(OTException e) {
            if(e.getType()==OTException.Type.RELOAD_REQUIRED) {
                if(!reloadRequested) {
                    failMessage.append("Unexpected document reload" + (e.getMessage()!=null ? " was triggered by " + e.getMessage() : ""));
                }
            }
            else {
                failMessage.append("Unexpected document reload" + (e.getMessage()!=null ? " was triggered by " + e.getMessage() : ""));
            }
        }
        catch(OpCompareException e) {
            failMessage.append(e.toString());
        }
        catch(JSONException e) {
            failMessage.append("JSONException localOp: " + localOps + " externalOps: " + externalOps + " ," + e.getMessage());
        }

        if(failMessage.length()!=0&&jsonLocal!=null&&externalOps!=null&&jsonExpectedLocal!=null&&jsonExpectedExternal!=null&&jsonTransformed!=null) {
            failMessage.append("\n----------------------------------------------------------------------------------------------------------" +
            "\nlocalOps:" +  localOps +
            "\nexternalOps:" +  externalOps +
            "\n----------------------------------------------------------------------------------------------------------" +
            "\nexpected local:" + jsonExpectedLocal + "->" + jsonTransformed +
            "\nexpected external:" + jsonExpectedExternal + "->" + jsonLocal);
        }
        if(failMessage.length()!=0) {
            fail(failMessage.toString());
        }
    }

    static public void compare(JSONArray opsArray, JSONArray compareOpsArray, ImmutableSet<OCKey> ignorables) {
        try {
            removePlaceholderOperations(opsArray);
            removePlaceholderOperations(compareOpsArray);
            if(opsArray.length()!=compareOpsArray.length()) {
                throw new OpCompareException(new OpCompareException.OpMessage("length does not match"));
            }
            for(int i=0; i<opsArray.length(); i++) {
                compareJSONObject(opsArray.getJSONObject(i), compareOpsArray.getJSONObject(i), ignorables);
            }
        }
        catch(Exception e) {
            throw OpCompareException.createCompareException(e, new OpCompareException.OpMessage("compare:"));
        }
    }

    static final public ImmutableSet<OCKey> comp_ignorables = new ImmutableSet.Builder<OCKey>()
        .add(OCKey.OSN)
        .add(OCKey.OPL)
        .build();

    static private void compareJSONObject(JSONObject ops, JSONObject compareOps, ImmutableSet<OCKey> ignorables) {

        String key = "";

        try {
            final Set<String> keys = new HashSet<String>(ops.keySet());
            keys.addAll(compareOps.keySet());
            for(String _key:keys) {

                key = _key;

                final OCKey k = OCKey.fromValue(key);
                if(!comp_ignorables.contains(k) && (ignorables==null||!ignorables.contains(k))) {
                    Object o;
                    if(k==OCKey.END) {
                        o = ops.opt(key);
                        if(o==null) {
                            o = ops.get(OCKey.START.value());   // defaulting end with start
                        }
                        if(!compareOps.has(key)) {
                            key = OCKey.START.value();
                        }
                    }
                    else {
                        o = ops.get(key);
                    }
                    if(o instanceof JSONObject) {
                        compareJSONObject((JSONObject)o, compareOps.getJSONObject(key), ignorables);
                    }
                    else if(o instanceof JSONArray) {
                        compareJSONArray((JSONArray)o, compareOps.getJSONArray(key), ignorables);
                    }
                    else if(!o.toString().equals(compareOps.get(key).toString())) {
                        throw new OpCompareException(new OpCompareException.OpMessage(o.toString() + "!=" + compareOps.get(key).toString()));
                    }
                }
            }
        }
        catch(Exception e) {
            throw OpCompareException.createCompareException(e, new OpCompareException.OpJSONObjectCompare("compareJSONObject:", ops, compareOps, key));
        }
    }

    static private void compareJSONArray(JSONArray ops, JSONArray compareOps, ImmutableSet<OCKey> ignorables) {
        int i = 0;
        try {
            if(ops.length()!=compareOps.length()) {
                throw new OpCompareException(new OpCompareException.OpMessage("length does not match"));
            }
            for(; i < ops.length(); i++) {
                final Object o = ops.get(i);
                if(o instanceof JSONObject) {
                    compareJSONObject((JSONObject)o, compareOps.getJSONObject(i), ignorables);
                }
                else if(o instanceof JSONArray) {
                    compareJSONArray((JSONArray)o, compareOps.getJSONArray(i), ignorables);
                }
                else if(!o.toString().equals(compareOps.get(i).toString())) {
                    throw new OpCompareException(new OpCompareException.OpMessage(o.toString() + "!=" + compareOps.get(i).toString()));
                }
            }
        }
        catch(Exception e) {
            throw OpCompareException.createCompareException(e, new OpCompareException.OpJSONArrayCompare("compareJSONArray:", ops, i, compareOps));
        }
    }

    static public JSONArray createJSONArray(String v) {
        try {
            return applyShortNames(new JSONArray(v.startsWith("[") ? v : "[" + v + "]"), true);
        }
        catch(Exception e) {
            throw OpCompareException.createCompareException(e, new OpCompareException.OpMessage("createJSONArray:"));
        }
    }

    //-------------------------------------------------------------------------

    static private void removePlaceholderOperations(JSONArray a) throws JSONException {
        for(int i=a.length()-1; i>=0; i--) {
            final JSONObject o = a.optJSONObject(i);
            if(o!=null) {
                if(o.has("_REMOVED_OPERATION_")) {
                    a.remove(i);
                }
                if(o.has("_GENERATED_OPERATION_")) {
                    o.remove("_GENERATED_OPERATION_");
                }
            }
        }
    }

    static private JSONArray applyShortNames(JSONArray array, boolean ignoreStrings) {
        int i = 0;
        try {
            for(; i < array.length(); i++) {
                final Object o = array.get(i);
                if(o instanceof JSONObject) {
                    array.put(i, applyShortNames((JSONObject)(o)));
                }
                else if(o instanceof JSONArray) {
                    applyShortNames((JSONArray)o, ignoreStrings);
                }
                else if(!ignoreStrings && o instanceof String) {
                    final OCValue v = OCValue.fromLongNameValue((String)o);
                    if(v!=OCValue.UNKNOWN_VALUE) {
                        if(v.value()!=(String)o) {
                            array.put(i, v.value());
                        }
                    }
                }
            }
        }
        catch(Exception e) {
            throw OpCompareException.createCompareException(e, new OpCompareException.OpJSONArray("applyShortNames:", array, i));
        }
        return array;
    }

    static private JSONObject applyShortNames(JSONObject o) {
        final JSONObject ret = new JSONObject();
        try {
            final Iterator<Entry<String, Object>> iter = o.entrySet().iterator();
            while(iter.hasNext()) {
                final Entry<String, Object> entry = iter.next();
                String key = entry.getKey();
                final OCKey k = OCKey.fromLongNameValue(key);
                if(k!=OCKey.UNKNOWN_KEY) {
                    key = k.value();
                }
                Object v = entry.getValue();
                if(v instanceof JSONObject) {
                    v = applyShortNames((JSONObject)v);
                }
                else if(v instanceof JSONArray) {
    //              if there is need to translate arrays:
    //              v = applyShortNames((JSONArray)v, false);
                }
                else if(v instanceof String) {
                    if(k==OCKey.NAME) {
                        final OCValue vN = OCValue.fromLongNameValue((String)v);
                        if(vN!=OCValue.UNKNOWN_VALUE) {
                            v = vN.value();
                        }
                    }
                }
                ret.put(key, v);
            }
        }
        catch(Exception e) {
            throw OpCompareException.createCompareException(e, new OpCompareException.OpJSONObject("applyShortNames:", o));
        }
        return ret;
    }
}
