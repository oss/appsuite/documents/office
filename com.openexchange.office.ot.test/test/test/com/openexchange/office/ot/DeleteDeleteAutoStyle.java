/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class DeleteDeleteAutoStyle {

    /**
     * The client tests are using T1 as a default
     * name for a type, but the backend is using cell.
     * In order to let client tests run in the backend,
     * auto style type tests without a style have to
     * use this default.
     * e.g. see insertAutoStyleInsertAutoStyle06()
     */
    private final static String DEFAULT_AUTO_STYLE_TYPE = "t1";
    private final String        otis                    = Helper.createJSONObject("{ otIndexShift: true }").toString();
    private final JSONObject    attrs                   = Helper.createJSONObject("{ f1: { a1: 10 } }");

    /*
     describe('"deleteAutoStyle" and independent operations', function () {
            it('should skip the transformations', function () {
                testRunner.runBidiTest(deleteAutoStyle('a1'), CHART_OPS);
            });
        });
     */

    @Test
    public void deleteDeleteAutoStyleAndIndependentOperations01() throws JSONException {
        final String chartOps = "[]";
        final JSONObject deleteAutoStyle = Helper.createDeleteAutoStyleOp("a1");
        TransformerTest.transformBidi(deleteAutoStyle.toString(), chartOps);
    }

    /*
     * describe('"deleteAutoStyle" and "deleteAutoStyle"', function () {
            var OTIS = { otIndexShift: true };
            it('should not process style sheets of different types', function () {
                testRunner.runTest(opSeries1(deleteAutoStyle, 'a1', ['t1', 'a1']), opSeries2(deleteAutoStyle, 't2', ['a1', 'a2']));
            });
            it('should shift style identifiers in indexed mode', function () {
                testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a1'), deleteAutoStyle('a2'),       deleteAutoStyle('a1', OTIS));
                testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a2'), deleteAutoStyle('a2'),       deleteAutoStyle('a2', OTIS));
                testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a3'), [],                          []);
                testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a4'), deleteAutoStyle('a3', OTIS), deleteAutoStyle('a3'));
                testRunner.runTest(deleteAutoStyle('a3'), deleteAutoStyle('a5'), deleteAutoStyle('a3', OTIS), deleteAutoStyle('a4'));
                // with/without type
                testRunner.runTest(deleteAutoStyle('a3'),       deleteAutoStyle('t1', 'a1'), deleteAutoStyle('a2'),       deleteAutoStyle('t1', 'a1', OTIS));
                testRunner.runTest(deleteAutoStyle('t1', 'a3'), deleteAutoStyle('a1'),       deleteAutoStyle('t1', 'a2'), deleteAutoStyle('a1', OTIS));
                testRunner.runTest(deleteAutoStyle('t1', 'a3'), deleteAutoStyle('t1', 'a1'), deleteAutoStyle('t1', 'a2'), deleteAutoStyle('t1', 'a1', OTIS));
            });
            it('should fail for invalid style identifiers in indexed mode', function () {
                testRunner.expectError(deleteAutoStyle('a2'), deleteAutoStyle('b2'));
                testRunner.expectError(deleteAutoStyle('b2'), deleteAutoStyle('a2'));
                testRunner.expectError(deleteAutoStyle('b2'), deleteAutoStyle('b1'));
            });
            it('should not shift style identifiers in freestyle mode', function () {
                testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a1'));
                testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a2'));
                testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a3'), [], []);
                testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a4'));
                testRunner.runTest(deleteAutoStyle('t2', 'a3'), deleteAutoStyle('t2', 'a5'));
            });
        });
     */

    @Test
    public void deleteAutoStyleDeleteAutoStyle01() throws JSONException {
        // Result: Should not process style sheets of different types.
        // testRunner.runTest(
        //  opSeries1(deleteAutoStyle, 'a1', ['t1', 'a1']),
        //  opSeries2(deleteAutoStyle, 't2', ['a1', 'a2']));
        TransformerTest.transform(
            Helper.createArrayFromJSON(Helper.createDeleteAutoStyleOp("a1"),
            Helper.createDeleteAutoStyleWithTypeOp("t1", "a1")),
            Helper.createArrayFromJSON(Helper.createDeleteAutoStyleWithTypeOp("t2", "a1"),
            Helper.createDeleteAutoStyleWithTypeOp("t2", "a2")));
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle02() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runTest(
        //  deleteAutoStyle('a3'),
        //  deleteAutoStyle('a1'),
        //  deleteAutoStyle('a2'),
        //  deleteAutoStyle('a1', OTIS));
        TransformerTest.transform(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createDeleteAutoStyleOp("a1").toString(),
            Helper.createDeleteAutoStyleOp("a1", otis).toString(),
            Helper.createDeleteAutoStyleOp("a2").toString()
        );
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle03() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runTest(
        //  deleteAutoStyle('a3'),
        //  deleteAutoStyle('a2'),
        //  deleteAutoStyle('a2'),
        //  deleteAutoStyle('a2', OTIS));
        TransformerTest.transform(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createDeleteAutoStyleOp("a2").toString(),
            Helper.createDeleteAutoStyleOp("a2", otis).toString(),
            Helper.createDeleteAutoStyleOp("a2").toString()
        );
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle04() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runTest(
        //  deleteAutoStyle('a3'),
        //  deleteAutoStyle('a3'),
        //  [],
        //  []);
        TransformerTest.transform(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createDeleteAutoStyleOp("a3").toString(),
            "[]",
            "[]"
        );
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle05() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runTest(
        //  deleteAutoStyle('a3'),
        //  deleteAutoStyle('a4'),
        //  deleteAutoStyle('a3', OTIS),
        //  deleteAutoStyle('a3'));
        TransformerTest.transform(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createDeleteAutoStyleOp("a4").toString(),
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createDeleteAutoStyleOp("a3", otis).toString()
        );
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle06() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runTest(
        //  deleteAutoStyle('a3'),
        //  deleteAutoStyle('a5'),
        //  deleteAutoStyle('a3', OTIS),
        //  deleteAutoStyle('a4'));
        TransformerTest.transform(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createDeleteAutoStyleOp("a5").toString(),
            Helper.createDeleteAutoStyleOp("a4").toString(),
            Helper.createDeleteAutoStyleOp("a3", otis).toString()
        );
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle07() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runTest(
        //  deleteAutoStyle('a3'),
        //  deleteAutoStyle('t1', 'a1'),
        //  deleteAutoStyle('a2'),
        //  deleteAutoStyle('t1', 'a1', OTIS));
        TransformerTest.transform(
            Helper.createDeleteAutoStyleWithTypeOp(DEFAULT_AUTO_STYLE_TYPE,"a3").toString(),
            Helper.createDeleteAutoStyleWithTypeOp("t1", "a1").toString(),
            Helper.createDeleteAutoStyleOp("t1","a1", otis).toString(),
            Helper.createDeleteAutoStyleWithTypeOp(DEFAULT_AUTO_STYLE_TYPE, "a2").toString()
        );
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle08() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runTest(
        //  deleteAutoStyle('t1', 'a3'),
        //  deleteAutoStyle('t1', 'a1'),
        //  deleteAutoStyle('t1', 'a2'),
        //  deleteAutoStyle('t1', 'a1', OTIS));
        TransformerTest.transform(
            Helper.createDeleteAutoStyleWithTypeOp("t1","a3").toString(),
            Helper.createDeleteAutoStyleWithTypeOp("t1", "a1").toString(),
            Helper.createDeleteAutoStyleOp("t1","a1", otis).toString(),
            Helper.createDeleteAutoStyleWithTypeOp("t1", "a2").toString()
        );
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle09() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode.
        // testRunner.expectError(
        //  deleteAutoStyle('a2'),
        //  deleteAutoStyle('b2'));
        TransformerTest.transform(
            Helper.createDeleteAutoStyleOp("a2").toString(),
            Helper.createDeleteAutoStyleOp("b2").toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle10() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode.
        // testRunner.expectError(
        //  deleteAutoStyle('b2'),
        //  deleteAutoStyle('a2'));
        TransformerTest.transform(
            Helper.createDeleteAutoStyleOp("b2").toString(),
            Helper.createDeleteAutoStyleOp("a2").toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }

    @Test
    public void deleteAutoStyleDeleteAutoStyle11() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode.
        // testRunner.expectError(
        //  deleteAutoStyle('b2'),
        //  deleteAutoStyle('b1'));
        TransformerTest.transform(
            Helper.createDeleteAutoStyleOp("b2").toString(),
            Helper.createDeleteAutoStyleOp("b1").toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }
}
