/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ParaSplitDelete {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 10] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'splitParagraph', start: [1, 9] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 10] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 9]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 20] }",
            "{ name: 'delete', start: [1, 22] }",
            "{ name: 'delete', start: [2, 2] }",
            "{ name: 'splitParagraph', start: [1, 20] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 20] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 22], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 20]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 30] }",
            "{ name: 'delete', start: [1, 12], end: [1, 16] }",
            "{ name: 'delete', start: [1, 12], end: [1, 16] }",
            "{ name: 'splitParagraph', start: [1, 25] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 12], end: [1, 16], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 25]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 16]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 30] }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'splitParagraph', start: [0, 30] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 30] };
                localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 30]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 40] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [3] }",
            "{ name: 'splitParagraph', start: [1, 40] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 40] };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 40]);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 50] }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'splitParagraph', start: [1, 50] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 50] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 50]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'splitParagraph', start: [2, 1, 2, 3, 4] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 2, 4] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 5] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 5] }",
            "{ name: 'delete', start: [0, 1], end: [0, 8] }",
            "[{ name: 'delete', start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[]");
            /*
                // local delete inside one paragraph (without a mergeParagraph operation) and an external splitParagraph in this paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [0, 8] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation generated
                generatedOperation = localActions[0].operations[1];
                expect(generatedOperation.name).to.equal('mergeParagraph');
                expect(generatedOperation.start).to.deep.equal([0]);
                expect(generatedOperation.paralength).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 3] }",
            "{ name: 'delete', start: [1, 2], end: [1, 5] }",
            "[{ name: 'delete', start: [1, 2], end: [2, 2] }, { name: 'mergeParagraph', start: [1], paralength: 2 }]",
            "[]");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 5] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2]);
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation generated that will be sent to the server
                generatedOperation = localActions[0].operations[1];
                expect(generatedOperation.name).to.equal('mergeParagraph');
                expect(generatedOperation.start).to.deep.equal([1]);
                expect(generatedOperation.paralength).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 1, 1, 0, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 0, 1], end: [2, 1, 1, 0, 8] }",
            "[{ name: 'delete', start: [2, 1, 1, 0, 1], end: [2, 1, 1, 1, 3] }, { name: 'mergeParagraph', start: [2, 1, 1, 0], paralength: 1 }]",
            "[]");
            /*
                // local delete inside one paragraph (without a mergeParagraph operation) and an external splitParagraph in this paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 1, 0, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 0, 1], end: [2, 1, 1, 0, 8] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 0, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 1, 3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation generated
                generatedOperation = localActions[0].operations[1];
                expect(generatedOperation.name).to.equal('mergeParagraph');
                expect(generatedOperation.start).to.deep.equal([2, 1, 1, 0]);
                expect(generatedOperation.paralength).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 1, 1, 3, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 8] }",
            "[{ name: 'delete', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 4, 3] }, { name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 1 }]",
            "[]");
            /*
                // local delete inside one paragraph (without a mergeParagraph operation) and an external splitParagraph in this paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 1, 3, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 8] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4, 3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation generated
                generatedOperation = localActions[0].operations[1];
                expect(generatedOperation.name).to.equal('mergeParagraph');
                expect(generatedOperation.start).to.deep.equal([2, 1, 1, 3]);
                expect(generatedOperation.paralength).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 4] }",
            "[{ name: 'delete', start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[{ name: 'delete', start: [0, 1], end: [2, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[]");
            /*
                // local delete inside several paragraphs (including a mergeParagraph operation) and an external splitParagraph in the first of the removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 4] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
                expect(localActions[0].operations.length).to.equal(2); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 3]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(2); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 1] }",
            "[{ name: 'delete', start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[{ name: 'delete', start: [0, 1], end: [2, 2] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[]");
            /*
                // local delete inside several paragraphs (including a mergeParagraph operation) and an external splitParagraph in the last of the removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
                expect(localActions[0].operations.length).to.equal(2); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(2); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 1] }",
            "[{ name: 'delete', start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[{ name: 'delete', start: [0, 1], end: [4, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[]");
            /*
                // local delete inside several paragraphs (including a mergeParagraph operation) and an external splitParagraph in the middle of the removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
                expect(localActions[0].operations.length).to.equal(2); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4, 3]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(2); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 1] }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "{ name: 'delete', start: [0, 1], end: [3] }",
            "[]");
            /*
                // local delete inside several paragraphs and of full paragraphs (without a mergeParagraph operation) and an external splitParagraph in the middle of the removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 3] }",
            "[{ name: 'delete', start: [1, 2], end: [1, 4] }, { name: 'mergeParagraph', start: [1], paralength: 2 }]",
            "[{ name: 'delete', start: [1, 2], end: [2, 1] }, { name: 'mergeParagraph', start: [1], paralength: 2 }, { name: 'mergeParagraph', start: [1], paralength: 2 }]",
            "[]");
            /*
                // local delete inside one paragraph (until its end with a mergeParagraph operation) and an external splitParagraph in the first of the removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 2 }] }];
                expect(localActions[0].operations.length).to.equal(2); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(3); // there is an additional local operation generated
                generatedOperation = localActions[0].operations[2];
                expect(generatedOperation.name).to.equal('mergeParagraph');
                expect(generatedOperation.start).to.deep.equal([1]);
                expect(generatedOperation.paralength).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 3] }",
            "{ name: 'delete', start: [0, 1], end: [1] }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "[]");
            /*
                // local delete inside one paragraph and a following paragraph completely and an external splitParagraph in the completely the removed paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "{ name: 'delete', start: [0, 1], end: [5] }",
            "[]");
            /*
                // local delete inside one paragraph and several following paragraphs completely and an external splitParagraph in the middle of the completely removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [4] }",
            "[]");
            /*
                // local delete of several paragraphs completely and an external splitParagraph in the first of the completely removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [4] }",
            "[]");
            /*
                // local delete of several paragraphs completely and an external splitParagraph in the last of the completely removed paragraphs
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring

             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 1] }",
            "{ name: 'delete', start: [1], end: [2, 2] }",
            "{ name: 'delete', start: [1], end: [3, 2] }",
            "[]");
            /*
                // local delete of several paragraphs completely and partly a final paragraph and an external splitParagraph in the completely removed paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 2]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'delete', start: [1], end: [2, 7] }",
            "{ name: 'delete', start: [1], end: [3, 5] }",
            "[]");
            /*
                // local delete of several paragraphs completely and partly a final paragraph and an external splitParagraph in the partly removed paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 7] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 1, 4, 5, 3] }",
            "[{ name: 'delete',start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', start: [1], paralength: 4 }]",
            "[{ name: 'delete',start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', start: [1], paralength: 4 }]",
            "[]");
            /*
                // local delete of several paragraphs (including a table and a following merge operation) and an external splitParagraph inside the table
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1, 4, 5, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
                expect(localActions[0].operations.length).to.equal(2); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]); // the end position of the locally saved operation is also NOT modified
                expect(localActions[0].operations.length).to.equal(2); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
            /*
                // local delete of one complete paragraph and an external splitParagraph inside this paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2]); // the end position of the locally saved operation is generated, so that both paragraphs are deleted
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'splitParagraph', start: [1, 2] }");
            /*
                // local delete of one complete paragraph and an external splitParagraph inside the following paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.equal(undefined); // the end position of the locally saved operation is not generated
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1, 2]); // the start position of the external operation is modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external splitParagraph operation has NOT the marker for local ignoring
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 2] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'splitParagraph', start: [0, 2] }");
            /*
                // local delete of one complete paragraph and an external splitParagraph inside the previous paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].end).to.equal(undefined); // the end position of the locally saved operation is not generated
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([0, 2]); // the start position of the external operation is not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external splitParagraph operation has NOT the marker for local ignoring
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [2, 2, 0, 3], end: [2, 2, 0, 8] }",
            "{ name: 'splitParagraph', start: [1, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 4] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [2, 0, 0, 3], end: [2, 0, 0, 8] }",
            "{ name: 'splitParagraph', start: [1, 4] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 0, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'splitParagraph', start: [1, 5] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 5] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'splitParagraph', start: [2, 5] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 5] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [2, 4, 0, 3], end: [2, 4, 0, 8] }",
            "{ name: 'splitParagraph', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 7, 2, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 7, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2, 2, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "[]");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2, 2, 2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external splitParagraph operation has the marker for local ignoring
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 7, 2, 2] }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'splitParagraph', start: [1, 7, 2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 7, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 7, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10] }",
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'delete', start: [2, 2] }");
            /*
                    oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
                    localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
                    otManager.transformOperation(oneOperation, localActions);
                    expect(oneOperation.start).to.deep.equal([2, 2]);
                    expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3] }",
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'delete', start: [4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 8] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4, 4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 5, 4] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 5, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 5, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5, 4]);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }",
            "{ name: 'splitParagraph', start: [3, 1, 2, 3, 4] }",
            "{ name: 'splitParagraph', start: [3, 1, 2 ,3, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 5, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 5, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10], end:[3, 6] }",
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'splitParagraph', start: [1, 8] }",
            "{ name: 'delete', start: [2, 2], end: [4, 6] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [1, 18] }",
            "{ name: 'splitParagraph', start: [1, 10] }",
            "{ name: 'splitParagraph', start: [1, 10] }",
            "{ name: 'delete', start: [2, 4], end: [2, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 10], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [1, 18] }",
            "{ name: 'splitParagraph', start: [1, 20] }",
            "{ name: 'splitParagraph', start: [1, 15] }",
            "{ name: 'delete', start: [1, 14], end: [1, 18] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 20], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 14]);
                expect(oneOperation.end).to.deep.equal([1, 18]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 15]);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [1, 18] }",
            "{ name: 'splitParagraph', start: [0, 20] }",
            "{ name: 'splitParagraph', start: [0, 20] }",
            "{ name: 'delete', start: [2, 14], end: [2, 18] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [0, 20], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 14]);
                expect(oneOperation.end).to.deep.equal([2, 18]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 20]);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 2], end: [1, 5] }",
            "{ name: 'splitParagraph', start: [1, 3] }",
            "[]",
            "[{ name: 'delete', start: [1, 2], end: [2, 2] }, { name: 'mergeParagraph', start: [1], paralength: 2 }]");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 5] };
                localActions = [{ operations: [{ name: 'splitParagraph', opl: 1, osn: 1, start: [1, 3] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(1);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // the local position is not modified
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // the local operation will be ignored
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0].name).to.equal('delete');
                expect(transformedOps[0].start).to.deep.equal([1, 2]);
                expect(transformedOps[0].end).to.deep.equal([2, 2]);
                expect(transformedOps[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps[1].name).to.equal('mergeParagraph');
                expect(transformedOps[1].start).to.deep.equal([1]);
                expect(transformedOps[1].paralength).to.equal(2);
                expect(transformedOps[1]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 10] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'splitTable', start: [1, 9] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 10] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 9]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 20] }",
            "{ name: 'delete', start: [1, 22] }",
            "{ name: 'delete', start: [2, 2] }",
            "{ name: 'splitTable', start: [1, 20] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 20] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 22], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 20]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 30] }",
            "{ name: 'delete', start: [1, 12], end: [1, 16] }",
            "{ name: 'delete', start: [1, 12], end: [1, 16] }",
            "{ name: 'splitTable', start: [1, 25] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 30] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 12], end: [1, 16], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 25]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 12]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 16]);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 30] }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'splitTable', start: [0, 30] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 30] };
                localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 30]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 40] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [3] }",
            "{ name: 'splitTable', start: [1, 40] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 40] };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 40]);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 50] }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'splitTable', start: [1, 50] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 50] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 50]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'splitTable', start: [2, 1, 2, 3, 4] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'splitTable', start: [3, 1, 2, 2, 4] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 5] }",
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind split
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5]);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 4] }",
            "{ name: 'delete', start: [0, 4] }",
            "{ name: 'splitTable', start: [0, 4] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 4]);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 5] }",
            "{ name: 'delete', start: [1, 0] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0]);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 6] }",
            "{ name: 'delete', start: [1, 1] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [1, 6] }",
            "{ name: 'delete', start: [2, 6] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 6]);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 4, 2, 2] }",
            "{ name: 'delete', start: [0, 4, 2, 2] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 4, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 4, 2, 2]);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 5, 2, 2] }",
            "{ name: 'delete', start: [1, 0, 2, 2] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 5, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 2, 2]);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 6, 2, 2] }",
            "{ name: 'delete', start: [1, 1, 2, 2] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 6, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 2, 2]);
             */
    }

    @Test
    public void test68() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [1, 6, 2, 2] }",
            "{ name: 'delete', start: [2, 6, 2, 2] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6, 2, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 6, 2, 2]);
             */
    }

    @Test
    public void test69() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 4, 2, 2, 2], end: [0, 4, 2, 2, 6] }",
            "{ name: 'delete', start: [0, 4, 2, 2, 2], end: [0, 4, 2, 2, 6] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 4, 2, 2, 2], end: [0, 4, 2, 2, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 4, 2, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 4, 2, 2, 6]);
             */
    }

    @Test
    public void test70() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 5, 2, 2, 2], end: [0, 5, 2, 2, 6] }",
            "{ name: 'delete', start: [1, 0, 2, 2, 2], end: [1, 0, 2, 2, 6] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 5, 2, 2, 2], end: [0, 5, 2, 2, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 2, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 0, 2, 2, 6]);
             */
    }

    @Test
    public void test71() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 6, 2, 2, 2], end: [0, 6, 2, 2, 6] }",
            "{ name: 'delete', start: [1, 1, 2, 2, 2], end: [1, 1, 2, 2, 6] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 6, 2, 2, 2], end: [0, 6, 2, 2, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1, 2, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 1, 2, 2, 6]);
             */
    }

    @Test
    public void test72() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [1, 6, 2, 2, 2], end: [1, 6, 2, 2, 6] }",
            "{ name: 'delete', start: [2, 6, 2, 2, 2], end: [2, 6, 2, 2, 6] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 6, 2, 2, 2], end: [1, 6, 2, 2, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 6, 2, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 6, 2, 2, 6]);
             */
    }

    @Test
    public void test73() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [0, 1], end: [0, 8] }",
            "[{ name: 'delete', start: [0, 1], end: [1, 3] }, { name: 'mergeTable', start: [0], rowcount: 1 }]",
            "[]");
            /*
                // local delete inside one table and an external splitTable in this table
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [0, 8] }] }]; // -> no such range in delete operation
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([1, 3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation generated
                generatedOperation = localActions[0].operations[1];
                expect(generatedOperation.name).to.equal('mergeTable');
                expect(generatedOperation.start).to.deep.equal([0]);
                expect(generatedOperation.rowcount).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test74() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 1, 0, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 0, 1], end: [2, 1, 1, 0, 8] }",
            "[{ name: 'delete', start: [2, 1, 1, 0, 1], end: [2, 1, 1, 1, 3] }, { name: 'mergeTable', start: [2, 1, 1, 0], rowcount: 1 }]",
            "[]");
            /*
                // local delete inside one table and an external splitTable in this table
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1, 1, 0, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 0, 1], end: [2, 1, 1, 0, 8] }] }]; // -> no such range in delete operation
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 0, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 1, 3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation generated
                generatedOperation = localActions[0].operations[1];
                expect(generatedOperation.name).to.equal('mergeTable');
                expect(generatedOperation.start).to.deep.equal([2, 1, 1, 0]);
                expect(generatedOperation.rowcount).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test75() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 1, 3, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 8] }",
            "[{ name: 'delete', start: [2, 1, 1, 3, 1], end: [2, 1, 1, 4, 3] }, { name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 1 }]",
            "[]");
            /*
                // local delete inside one table and an external splitTable in this table
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1, 1, 3, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 3, 1], end: [2, 1, 1, 3, 8] }] }]; // -> no such range in delete operation
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4, 3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation generated
                generatedOperation = localActions[0].operations[1];
                expect(generatedOperation.name).to.equal('mergeTable');
                expect(generatedOperation.start).to.deep.equal([2, 1, 1, 3]);
                expect(generatedOperation.rowcount).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test76() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1, 1, 3, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 3, 3], end: [2, 1, 1, 3, 8] }",
            "[{ name: 'delete', start: [2, 1, 1, 3, 3], end: [2, 1, 1, 4, 3] }, { name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 3 }]",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1, 1, 3, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 3, 3], end: [2, 1, 1, 3, 8] }] }]; // -> no such range in delete operation
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3, 3]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4, 3]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.equal(2); // there is a new local operation generated
                generatedOperation = localActions[0].operations[1];
                expect(generatedOperation.name).to.equal('mergeTable');
                expect(generatedOperation.start).to.deep.equal([2, 1, 1, 3]);
                expect(generatedOperation.rowcount).to.equal(3);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test77() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1] }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "{ name: 'delete', start: [0, 1], end: [3] }",
            "[]");
            /*
                // local delete inside one table and an external splitTable in this table
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test78() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 3] }",
            "{ name: 'delete', start: [0, 1], end: [1] }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test79() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "{ name: 'delete', start: [0, 1], end: [5] }",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test80() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [4] }",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test81() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [4] }",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test82() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1] }",
            "{ name: 'delete', start: [1], end: [2, 2] }",
            "{ name: 'delete', start: [1], end: [3, 2] }",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 2] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 2]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test83() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'delete', start: [1], end: [2, 7] }",
            "{ name: 'delete', start: [1], end: [3, 5] }",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 7] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3, 5]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test84() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([2]); // the end position of the locally saved operation is generated, so that both paragraphs are deleted
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test85() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'splitTable', start: [1, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.equal(undefined); // the end position of the locally saved operation is not generated
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([1, 2]); // the start position of the external operation is modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test86() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 2] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'splitTable', start: [0, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].end).to.equal(undefined); // the end position of the locally saved operation is not generated
                expect(localActions[0].operations.length).to.equal(1); // there is still only one local operation
                expect(oneOperation.start).to.deep.equal([0, 2]); // the start position of the external operation is not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test87() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [2, 2, 0, 3], end: [2, 2, 0, 8] }",
            "{ name: 'splitTable', start: [1, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test88() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 6] }",
            "{ name: 'delete', start: [2, 2, 0, 3, 4], end: [2, 2, 0, 3, 6] }",
            "{ name: 'splitTable', start: [1, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 6], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 0, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2, 0, 3, 6]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test89() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 4] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [2, 0, 0, 3], end: [2, 0, 0, 8] }",
            "{ name: 'splitTable', start: [1, 4] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 0, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test90() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 4] }",
            "{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 8] }",
            "{ name: 'delete', start: [2, 0, 0, 3, 4], end: [2, 0, 0, 3, 8] }",
            "{ name: 'splitTable', start: [1, 4] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 0, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 0, 0, 3, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test91() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'splitTable', start: [1, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test92() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5] }",
            "{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 8] }",
            "{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 8] }",
            "{ name: 'splitTable', start: [1, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3, 4], end: [1, 4, 0, 3, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 0, 3, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 0, 3, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test93() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 5] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'splitTable', start: [2, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test94() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 5] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [2, 4, 0, 3], end: [2, 4, 0, 8] }",
            "{ name: 'splitTable', start: [0, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 5] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 0, 8]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test95() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 7, 2, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'splitTable', start: [1, 5, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 7, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test96() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2, 2, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test97() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2, 2, 2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test98() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 7, 2, 2] }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'splitTable', start: [1, 7, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 7, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 7, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test99() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10] }",
            "{ name: 'splitTable', start: [1, 8] }",
            "{ name: 'splitTable', start: [1, 8] }",
            "{ name: 'delete', start: [2, 2] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test100() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10, 3, 2, 1], end: [1, 10, 3, 2, 2] }",
            "{ name: 'splitTable', start: [1, 8] }",
            "{ name: 'splitTable', start: [1, 8] }",
            "{ name: 'delete', start: [2, 2, 3, 2, 1], end: [2, 2, 3, 2, 2] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10, 3, 2, 1], end: [1, 10, 3, 2, 2] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 3, 2, 1]);
                expect(oneOperation.end).to.deep.equal([2, 2, 3, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test101() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3] }",
            "{ name: 'splitTable', start: [1, 8] }",
            "{ name: 'splitTable', start: [1, 8] }",
            "{ name: 'delete', start: [4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test102() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4] }",
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
                localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test103() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 8] }",
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4, 4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
                localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test104() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }",
            "{ name: 'splitTable', start: [3, 1, 2, 5, 4] }",
            "{ name: 'splitTable', start: [3, 1, 2, 5, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
                localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 5, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5, 4]);
             */
    }

    @Test
    public void test105() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }",
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'splitTable', start: [3, 1, 2, 3, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 5, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
                localActions = [{ operations: [{ name: 'splitTable', start: [3, 1, 2, 3, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 5, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3, 4]);
             */
    }

    @Test
    public void test106() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10], end: [3, 6] }",
            "{ name: 'splitTable', start: [1, 8] }",
            "{ name: 'splitTable', start: [1, 8] }",
            "{ name: 'delete', start: [2, 2], end: [4, 6] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(oneOperation.end).to.deep.equal([4, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test107() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [1, 18] }",
            "{ name: 'splitTable', start: [1, 10] }",
            "{ name: 'splitTable', start: [1, 10] }",
            "{ name: 'delete', start: [2, 4], end: [2, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 10], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4]);
                expect(oneOperation.end).to.deep.equal([2, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
             */
    }

    @Test
    public void test108() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [1, 18] }",
            "{ name: 'splitTable', start: [1, 20] }",
            "{ name: 'splitTable', start: [1, 15] }",
            "{ name: 'delete', start: [1, 14], end: [1, 18] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 20], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 14]);
                expect(oneOperation.end).to.deep.equal([1, 18]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 15]);
             */
    }

    @Test
    public void test109() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [1, 18] }",
            "{ name: 'splitTable', start: [0, 20] }",
            "{ name: 'splitTable', start: [0, 20] }",
            "{ name: 'delete', start: [2, 14], end: [2, 18] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [1, 18] };
                localActions = [{ operations: [{ name: 'splitTable', start: [0, 20], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 14]);
                expect(oneOperation.end).to.deep.equal([2, 18]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 20]);
             */
    }
}
