/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertParaInsertChar {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 4], text: 'o' }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertText', start: [4, 4], text: 'o' }");
           /*
                oneOperation = { name: 'insertText', start: [3, 4], text: 'o', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([4, 4]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 4], text: 'o' }",
            "{ name: 'insertTable', start: [3], target: 'cmt0' }",
            "{ name: 'insertTable', start: [3], target: 'cmt0' }",
            "{ name: 'insertText', start: [3, 4], text: 'o' }");
           /*
                oneOperation = { name: 'insertText', start: [3, 4], text: 'o', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], target: 'cmt0', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0' }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0' }");
           /*
                oneOperation = { name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0' }",
            "{ name: 'insertTable', start: [3], target: 'cmt0' }",
            "{ name: 'insertTable', start: [3], target: 'cmt0' }",
            "{ name: 'insertText', start: [4, 4], text: 'o', target: 'cmt0' }");
           /*
                oneOperation = { name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], target: 'cmt0', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([4, 4]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0' }",
            "{ name: 'insertTable', start: [3], target: 'cmt1' }",
            "{ name: 'insertTable', start: [3], target: 'cmt1' }",
            "{ name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0' }");
           /*
                oneOperation = { name: 'insertText', start: [3, 4], text: 'o', target: 'cmt0', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], target: 'cmt1', opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 4], text: 'o' }",
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'insertText', start: [3, 4], text: 'o' }");
           /*
                oneOperation = { name: 'insertText', start: [3, 4], text: 'o', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([3, 4]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o' }",
            "{ name: 'insertTable', start: [2, 2, 4, 4] }",
            "{ name: 'insertTable', start: [2, 2, 4, 4] }",
            "{ name: 'insertText', start: [2, 2, 4, 7, 2], text: 'o' }");
           /*
                oneOperation = { name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [2, 2, 4, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 4, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([2, 2, 4, 7, 2]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o' }",
            "{ name: 'insertTable', start: [2, 2, 3, 4] }",
            "{ name: 'insertTable', start: [2, 2, 3, 4] }",
            "{ name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o' }");
           /*
                oneOperation = { name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [2, 2, 3, 4], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 3, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([2, 2, 4, 6, 2]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o' }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'insertText', start: [3, 2, 4, 6, 2], text: 'o' }");
           /*
                oneOperation = { name: 'insertText', start: [2, 2, 4, 6, 2], text: 'o', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([3, 2, 4, 6, 2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2], text: 'o' }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'insertText', start: [2, 2], text: 'o' }");
           /*
                oneOperation = { name: 'insertText', start: [2, 2], text: 'o', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertTable', start: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2], text: 'ooo' }",
            "{ name: 'insertParagraph', start: [2, 5, 0] }",
            "{ name: 'insertParagraph', start: [2, 8, 0] }",
            "{ name: 'insertText', start: [2, 2], text: 'ooo' }");
           /*
                oneOperation = { name: 'insertText', start: [2, 2], text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [2, 5, 0], opl: 1, osn: 1 }] }]; // in a textframe
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 8, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2], text: 'ooo' }",
            "{ name: 'insertParagraph', start: [2, 1, 0] }",
            "{ name: 'insertParagraph', start: [2, 1, 0] }",
            "{ name: 'insertText', start: [2, 2], text: 'ooo' }");
           /*
                oneOperation = { name: 'insertText', start: [2, 2], text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [2, 1, 0], opl: 1, osn: 1 }] }]; // in a textframe
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([2, 2]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 1], text: 'ooo' }",
            "{ name: 'insertParagraph', start: [2, 1, 0] }",
            "{ name: 'insertParagraph', start: [2, 1, 0] }",
            "{ name: 'insertText', start: [1, 1], text: 'ooo' }");
           /*
                oneOperation = { name: 'insertText', start: [1, 1], text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [2, 1, 0], opl: 1, osn: 1 }] }]; // in a textframe
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 0]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([1, 1]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 1, 3, 1], text: 'ooo' }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'insertText', start: [3, 1, 3, 1], text: 'ooo' }");
           /*
                oneOperation = { name: 'insertText', start: [2, 1, 3, 1], text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([3, 1, 3, 1]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 1, 3, 1], text: 'ooo' }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'insertText', start: [2, 1, 3, 1], text: 'ooo' }");
           /*
                oneOperation = { name: 'insertText', start: [1, 1, 3, 1], text: 'ooo', opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([2, 1, 3, 1]);
             */
    }
}
