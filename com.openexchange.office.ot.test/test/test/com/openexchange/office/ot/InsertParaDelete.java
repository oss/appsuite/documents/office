/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertParaDelete {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [2, 8] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 8]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'delete', start: [1, 2] }",
            "{ name: 'insertTable', start: [2] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'delete', start: [1], end: [1] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'delete', start: [1], end:[2] }",
            "{ name: 'delete', start: [1], end:[2] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertTable operation has NOT the marker for local ignoring
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertTable operation has the marker for local ignoring
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'delete', start: [0] }",
            "{ name: 'insertTable', start: [0] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', start: [0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [3] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'delete', start: [0, 3], end: [0, 8] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', start: [0, 3], end: [0, 8], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 8]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertTable', start: [2, 1, 2, 3] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
                localActions = [{ operations: [{ name: 'delete', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'delete', start: [3, 1, 2, 0] }",
            "{ name: 'insertTable', start: [3, 1, 2, 2] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 0], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, before insert
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 0]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 5] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }]; // deleting paragraph in same cell, but behind insert
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 5]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 2, 3] };
                localActions = [{ operations: [{ name: 'delete', start: [4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 1, 3] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertTable', start: [3, 1, 1, 3] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 1, 3] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 4], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3, 1, 1, 3] }",
            "{ name: 'delete', start: [3, 1, 2, 1] }",
            "{ name: 'delete', start: [3, 1, 2, 1] }",
            "{ name: 'insertTable', start: [3, 1, 1, 3] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3, 1, 1, 3] };
                localActions = [{ operations: [{ name: 'delete', start: [3, 1, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [0, 1], end: [1, 8] }",
            "{ name: 'delete', start: [0, 1], end: [2, 8] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 8] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is a one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 8]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertTable operation has the marker for local ignoring
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "[{ name: 'delete', start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[{ name: 'delete', start: [0, 1], end: [2, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
                expect(localActions[0].operations.length).to.equal(2); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 3]);
                expect(localActions[0].operations.length).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertTable operation has the marker for local ignoring
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "[{ name: 'delete', start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[{ name: 'delete', start: [0, 1], end: [4, 3] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [3, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
                expect(localActions[0].operations.length).to.equal(2); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([4, 3]);
                expect(localActions[0].operations.length).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertTable operation has the marker for local ignoring
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "{ name: 'delete', start: [0, 1], end: [3] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertTable operation has the marker for local ignoring
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "[{ name: 'delete', start: [1, 2], end: [1, 4] }, { name: 'mergeParagraph', start: [1], paralength: 2 }]",
            "[{ name: 'delete', start: [2, 2], end: [2, 4] }, { name: 'mergeParagraph', start: [2], paralength: 2 }]",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 2], end: [1, 4] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 2 }] }];
                expect(localActions[0].operations.length).to.equal(2); // there are two local operations
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4]);
                expect(localActions[0].operations[1].start).to.deep.equal([2]);
                expect(localActions[0].operations[1].paralength).to.equal(2);
                expect(localActions[0].operations.length).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertTable operation has not the marker for local ignoring
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [0, 1], end: [1] }",
            "{ name: 'delete', start: [0, 1], end: [2] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([2]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertTable operation has the marker for local ignoring
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'delete', start: [0, 1], end: [4] }",
            "{ name: 'delete', start: [0, 1], end: [5] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertTable operation has the marker for local ignoring
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 5] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 1, 4] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertTable operation has the marker for local ignoring
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2, 1, 1, 5] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'insertTable', start: [2, 1, 1, 2] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 1, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2, 1, 1, 2] }",
            "{ name: 'delete', start: [2, 1, 1, 2], end: [2, 1, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 1, 3], end: [2, 1, 1, 5] }",
            "{ name: 'insertTable', start: [2, 1, 1, 2] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 1, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 1, 2], end: [2, 1, 1, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1); // there is one local operation
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 1, 5]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2, 1, 1, 2] }",
            "{ name: 'delete', start: [2, 1, 2, 2], end: [2, 1, 2, 4] }",
            "{ name: 'delete', start: [2, 1, 2, 2], end: [2, 1, 2, 4] }",
            "{ name: 'insertTable', start: [2, 1, 1, 2] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 1, 2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 2, 2], end: [2, 1, 2, 4] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 2]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [1], end: [2, 3] }",
            "{ name: 'delete', start: [2], end: [3, 3] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 3] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [4] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'delete', start: [1], end: [2, 7] }",
            "{ name: 'delete', start: [1], end: [3, 7] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [2, 7] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 7]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2, 1, 4, 5] }",
            "{ name: 'delete', start: [1, 4], end: [3, 8] }",
            "{ name: 'delete', start: [1, 4], end: [3, 8] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2, 1, 4, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [3, 8] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [1], paralength: 4 }] }];
                expect(localActions[0].operations.length).to.equal(2);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 8]);
                expect(localActions[0].operations.length).to.equal(2);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [0] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'insertTable', start: [0] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [0] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 1, 4, 5] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 1, 4, 5] };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                expect(localActions[0].operations.length).to.equal(1);
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'delete', start: [1, 14], end: [2, 18] }",
            "{ name: 'delete', start: [1, 14], end: [3, 18] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', start: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [2, 18] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 14]);
                expect(localActions[0].operations[0].end).to.deep.equal([3, 18]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [2, 4, 0, 3], end: [2, 4, 0, 8] }",
            "{ name: 'insertTable', start: [1] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 0, 8]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'insertTable', start: [2] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [2] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 4, 0, 8]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [0] }",
            "{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8] }",
            "{ name: 'delete', start: [2, 4, 0, 3], end: [2, 4, 0, 8] }",
            "{ name: 'insertTable', start: [0] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [0] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 4, 0, 3], end: [1, 4, 0, 8], opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 0, 3]);
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4, 0, 8]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 7, 2] }",
            "{ name: 'delete', start: [1, 1], end:[1, 2] }",
            "{ name: 'delete', start: [1, 1], end:[1, 2] }",
            "{ name: 'insertTable', start: [1, 5, 2] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 7, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 3, 2] }",
            "{ name: 'delete', start: [1, 5], end: [1, 7] }",
            "{ name: 'delete', start: [1, 5], end: [1, 7] }",
            "{ name: 'insertTable', start: [1, 3, 2] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 3, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 5], end: [1, 7], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 7]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 2, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1, 1], end: [1, 2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 2, 2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "[]");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'insertTable', start: [1, 7, 2] }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'delete', start: [0, 1], end: [0, 2] }",
            "{ name: 'insertTable', start: [1, 7, 2] }");
           /*
                oneOperation = { name: 'insertTable', opl: 1, osn: 1, start: [1, 7, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', start: [0, 1], end: [0, 2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 7, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([0, 2]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [2, 10] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10] };
                localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 10]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10], end: [1, 14] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [2, 10], end: [2, 14] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [1, 14] };
                localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 10]);
                expect(oneOperation.end).to.deep.equal([2, 14]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'delete', start: [4] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'delete', start: [4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [4] };
                localActions = [{ operations: [{ name: 'insertTable', start: [3, 1, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 3, 8] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 3, 8] };
                localActions = [{ operations: [{ name: 'insertTable', start: [3, 1, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4] }",
            "{ name: 'insertTable', start: [3, 1, 2, 5] }",
            "{ name: 'insertTable', start: [3, 1, 2, 4] }",
            "{ name: 'delete', start: [3, 1, 2, 4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4] };
                localActions = [{ operations: [{ name: 'insertTable', start: [3, 1, 2, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'delete', start: [3, 1, 2, 4, 8] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'insertTable', start: [3, 1, 2, 3] }",
            "{ name: 'delete', start: [3, 1, 2, 5, 8] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [3, 1, 2, 4, 8] };
                localActions = [{ operations: [{ name: 'insertTable', start: [3, 1, 2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 5, 8]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 10], end: [3, 6] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [2, 10], end: [4, 6] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 10], end: [3, 6] };
                localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 10]);
                expect(oneOperation.end).to.deep.equal([4, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertTable', start: [5] }",
            "{ name: 'insertTable', start: [4] }",
            "{ name: 'delete', start: [1] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertTable', start: [5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'insertTable', start: [1] }",
            "{ name: 'delete', start: [2] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'insertTable', start: [1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'insertTable', start: [5] }",
            "{ name: 'insertTable', start: [2] }",
            "{ name: 'delete', start: [1], end: [3] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1], end: [3] };
                localActions = [{ operations: [{ name: 'insertTable', start: [5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.end).to.deep.equal([3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [2, 18] }",
            "{ name: 'insertTable', start: [2] }",
            "[]",
            "{ name: 'delete', start: [1, 14], end: [3, 18] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [2, 18] };
                localActions = [{ operations: [{ name: 'insertTable', start: [2], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 14]);
                expect(oneOperation.end).to.deep.equal([3, 18]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 14], end: [3, 18] }",
            "{ name: 'insertTable', start: [0] }",
            "{ name: 'insertTable', start: [0] }",
            "{ name: 'delete', start: [2, 14], end: [4, 18] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 14], end: [3, 18] };
                localActions = [{ operations: [{ name: 'insertTable', start: [0], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 14]);
                expect(oneOperation.end).to.deep.equal([4, 18]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [2, 8] }",
            "{ name: 'insertParagraph', start: [1] }");
            /*
                oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [1] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [2, 8], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }], transformedOps);
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'insertParagraph', start: [3] }");
            /*
                oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [3] }], transformedOps);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertParagraph', start: [2] }");
            /*
                oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [3] };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }], transformedOps);
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [1, 2, 3, 4] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "[]");
            /*
                oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [2, 2, 3, 4] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertParagraph', start: [1, 2, 3, 4] }");
            /*
                 oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [2, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }], transformedOps);
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3, 2, 3, 4] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'insertParagraph', start: [1, 2, 3, 4] }");
            /*
                oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [3, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }] }], localActions);
                expectOp([{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }], transformedOps);
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'insertParagraph', start: [3, 2, 3, 4] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "[]");
            /*
                 oneOperation = { name: 'insertParagraph', opl: 1, osn: 1, start: [3, 2, 3, 4] };
                localActions = [{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'insertParagraph', start: [1] }",
            "{ name: 'delete', start: [2, 8] }");
            /*
                 oneOperation = { name: 'delete', start: [1, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1] }] }], localActions);
                expectOp([{ name: 'delete', start: [2, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 8] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'delete', start: [1, 8] }");
            /*
                oneOperation = { name: 'delete', start: [1, 8], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3] }] }], localActions);
                expectOp([{ name: 'delete', start: [1, 8], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertParagraph', start: [3] }",
            "{ name: 'insertParagraph', start: [2] }",
            "{ name: 'delete', start: [1] }");
            /*
                oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [2] }] }], localActions);
                expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertParagraph', start: [1, 2, 3, 4] }",
            "[]",
            "{ name: 'delete', start: [1] }");
            /*
                oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4], _REMOVED_OPERATION_: 1 }] }], localActions);
                expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertParagraph', start: [2, 2, 3, 4] }",
            "{ name: 'insertParagraph', start: [1, 2, 3, 4] }",
            "{ name: 'delete', start: [1] }");
            /*
                oneOperation = { name: 'delete', start: [1], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [2, 2, 3, 4] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }] }], localActions);
                expectOp([{ name: 'delete', start: [1], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test68() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1], end: [2] }",
            "{ name: 'insertParagraph', start: [3, 2, 3, 4] }",
            "{ name: 'insertParagraph', start: [1, 2, 3, 4] }",
            "{ name: 'delete', start: [1], end: [2] }");
            /*
                oneOperation = { name: 'delete', start: [1], end: [2], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3, 2, 3, 4] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4] }] }], localActions);
                expectOp([{ name: 'delete', start: [1], end: [2], opl: 1, osn: 1 }], transformedOps);
             */
    }

    @Test
    public void test69() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'insertParagraph', start: [3, 2, 3, 4] }",
            "[]",
            "{ name: 'delete', start: [1], end: [3] }");
            /*
                oneOperation = { name: 'delete', start: [1], end: [3], opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3, 2, 3, 4] }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'insertParagraph', opl: 1, osn: 1, start: [3, 2, 3, 4], _REMOVED_OPERATION_: 1 }] }], localActions);
                expectOp([{ name: 'delete', start: [1], end: [3], opl: 1, osn: 1 }], transformedOps);
             */
    }
}
