/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ChangeAutoStyle {

    /**
     * The client tests are using T1 as a default
     * name for a type, but the backend is using cell.
     * In order to let client tests run in the backend,
     * auto style type tests without a style have to
     * use this default.
     * e.g. see insertAutoStyleInsertAutoStyle06()
     */
    private final static String DEFAULT_AUTO_STYLE_TYPE = "t1";
    private final String        otis                    = Helper.createJSONObject("{ otIndexShift: true }").toString();
    private final JSONObject    attrs                   = Helper.createJSONObject("{ f1: { a1: 10 } }");

    /*
     * describe('"insertAutoStyle" and "changeAutoStyle"', function () {
            var OTIS = { otIndexShift: true };
            it('should not process style sheets of different types', function () {
                testRunner.runBidiTest(opSeries1(insertAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]), opSeries2(changeAutoStyle, 't2', ['a1', 'a2'], ATTRS));
            });
            it('should shift style identifiers in indexed mode', function () {
                testRunner.runBidiTest(insertAutoStyle('a2', ATTRS), changeAutoStyle('a0', ATTRS), insertAutoStyle('a2', ATTRS),       changeAutoStyle('a0', ATTRS));
                testRunner.runBidiTest(insertAutoStyle('a2', ATTRS), changeAutoStyle('a1', ATTRS), insertAutoStyle('a2', ATTRS),       changeAutoStyle('a1', ATTRS));
                testRunner.runBidiTest(insertAutoStyle('a2', ATTRS), changeAutoStyle('a2', ATTRS), insertAutoStyle('a2', ATTRS, OTIS), changeAutoStyle('a3', ATTRS));
                testRunner.runBidiTest(insertAutoStyle('a2', ATTRS), changeAutoStyle('a3', ATTRS), insertAutoStyle('a2', ATTRS, OTIS), changeAutoStyle('a4', ATTRS));
                // with/without type
                testRunner.runBidiTest(insertAutoStyle('a2', ATTRS),       changeAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('a2', ATTRS, OTIS),       changeAutoStyle('t1', 'a3', ATTRS));
                testRunner.runBidiTest(insertAutoStyle('t1', 'a2', ATTRS), changeAutoStyle('a2', ATTRS),       insertAutoStyle('t1', 'a2', ATTRS, OTIS), changeAutoStyle('a3', ATTRS));
                testRunner.runBidiTest(insertAutoStyle('t1', 'a2', ATTRS), changeAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('t1', 'a2', ATTRS, OTIS), changeAutoStyle('t1', 'a3', ATTRS));
            });
            it('should fail for invalid style identifiers in indexed mode', function () {
                testRunner.expectBidiError(insertAutoStyle('a2', ATTRS), changeAutoStyle('b2', ATTRS));
                testRunner.expectBidiError(insertAutoStyle('b2', ATTRS), changeAutoStyle('a2', ATTRS));
                testRunner.expectBidiError(insertAutoStyle('b2', ATTRS), changeAutoStyle('b2', ATTRS));
            });
            it('should not shift style identifiers in freestyle mode', function () {
                testRunner.runBidiTest(insertAutoStyle('t2', 'a2', ATTRS), changeAutoStyle('t2', 'a1', ATTRS));
                testRunner.runBidiTest(insertAutoStyle('t2', 'a2', ATTRS), changeAutoStyle('t2', 'a3', ATTRS));
            });
            it('should fail to insert existing auto-style in freestyle mode', function () {
                testRunner.expectBidiError(insertAutoStyle('t2', 'a2', ATTRS), changeAutoStyle('t2', 'a2', ATTRS));
            });
        });
     */

    @Test
    public void insertAutoStyleChangeAutoStyle01() throws JSONException {
        // Result: Should not process style sheets of different types.
        // testRunner.runBidiTest(
        //  opSeries1(insertAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
        //  opSeries2(changeAutoStyle, 't2', ['a1', 'a2'], ATTRS));
        TransformerTest.transformBidi(
            Helper.createArrayFromJSON(Helper.createInsertAutoStyleOp("a1", attrs, null),
            Helper.createInsertAutoStyleOp("t1", "a1", attrs, null)),
            Helper.createArrayFromJSON(Helper.createChangeAutoStyleOp("t2", "a1", attrs, null),
            Helper.createChangeAutoStyleOp("t2", "a2", attrs, null)));
    }

    @Test
    public void insertAutoStyleChangeAutoStyle02() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a2', ATTRS),
        //  changeAutoStyle('a0', ATTRS),
        //  insertAutoStyle('a2', ATTRS),
        //  changeAutoStyle('a0', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("a0", attrs).toString(),
            Helper.createChangeAutoStyleOp("a0", attrs).toString(),
            Helper.createInsertAutoStyleOp("a2", attrs, null).toString()
        );
    }

    @Test
    public void insertAutoStyleChangeAutoStyle03() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a2', ATTRS),
        //  changeAutoStyle('a1', ATTRS),
        //  insertAutoStyle('a2', ATTRS),
        //  changeAutoStyle('a1', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("a1", attrs).toString(),
            Helper.createChangeAutoStyleOp("a1", attrs).toString(),
            Helper.createInsertAutoStyleOp("a2", attrs, null).toString()
        );
    }

    @Test
    public void insertAutoStyleChangeAutoStyle04() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a2', ATTRS),
        //  changeAutoStyle('a2', ATTRS),
        //  insertAutoStyle('a2', ATTRS, OTIS),
        //  changeAutoStyle('a3', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("a2", attrs).toString(),
            Helper.createChangeAutoStyleOp("a3", attrs).toString(),
            Helper.createInsertAutoStyleOp("a2", attrs, otis).toString()
        );
    }

    @Test
    public void insertAutoStyleChangeAutoStyle05() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a2', ATTRS),
        //  changeAutoStyle('a3', ATTRS),
        //  insertAutoStyle('a2', ATTRS, OTIS),
        //  changeAutoStyle('a4', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("a3", attrs).toString(),
            Helper.createChangeAutoStyleOp("a4", attrs).toString(),
            Helper.createInsertAutoStyleOp("a2", attrs, otis).toString()
        );
    }

    @Test
    public void insertAutoStyleChangeAutoStyle06() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a2', ATTRS),
        //  changeAutoStyle('t1', 'a2', ATTRS),
        //  insertAutoStyle('a2', ATTRS, OTIS),
        //  changeAutoStyle('t1', 'a3', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a3", attrs, null).toString(),
            Helper.createInsertAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a2", attrs, otis).toString()
        );
    }

    @Test
    public void insertAutoStyleChangeAutoStyle07() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('t1', 'a2', ATTRS),
        //  changeAutoStyle('a2', ATTRS),
        //  insertAutoStyle('t1', 'a2', ATTRS, OTIS),
        //  changeAutoStyle('a3', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("t1", "a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a3", attrs, null).toString(),
            Helper.createInsertAutoStyleOp("t1", "a2", attrs, otis).toString()
        );
    }

    @Test
    public void insertAutoStyleChangeAutoStyle08() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('t1', 'a2', ATTRS),
        //  changeAutoStyle('t1', 'a2', ATTRS),
        //  insertAutoStyle('t1', 'a2', ATTRS, OTIS),
        //  changeAutoStyle('t1', 'a3', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("t1", "a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a3", attrs, null).toString(),
            Helper.createInsertAutoStyleOp("t1", "a2", attrs, otis).toString()
        );
    }

    @Test
    public void insertAutoStyleChangeAutoStyle09() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.expectBidiError(
        //  insertAutoStyle('a2', ATTRS),
        //  changeAutoStyle('b2', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("b2", attrs).toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }

    @Test
    public void insertAutoStyleChangeAutoStyle10() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.expectBidiError(
        //  insertAutoStyle('b2', ATTRS),
        //  changeAutoStyle('a2', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("b2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("a2", attrs).toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }

    @Test
    public void insertAutoStyleChangeAutoStyle11() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.expectBidiError(
        //  insertAutoStyle('b2', ATTRS),
        //  changeAutoStyle('b2', ATTRS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("b2", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("b2", attrs).toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }
}
