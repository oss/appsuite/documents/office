/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertAutoStyle {

    /**
     * The client tests are using T1 as a default
     * name for a type, but the backend is using cell.
     * In order to let client tests run in the backend,
     * auto style type tests without a style have to
     * use this default.
     * e.g. see insertAutoStyleInsertAutoStyle06()
     */
    private final static String DEFAULT_AUTO_STYLE_TYPE = "t1";
    private final String        otis                    = Helper.createJSONObject("{ otIndexShift: true }").toString();
    private final JSONObject    attrs                   = Helper.createJSONObject("{ f1: { a1: 10 } }");

    /*
     * describe('"insertAutoStyle" and independent operations', function () {
     * it('should skip the transformations', function () {
     * testRunner.runBidiTest(insertAutoStyle('a1', ATTRS), CHART_OPS);
     * });
     * });
     */

    @Test
    public void insertAutoStyleAndIndependentOperations01() throws JSONException {
        final String chartOps = "[]";
        final JSONObject insertAutoStyle = Helper.createInsertAutoStyleOp("a1", attrs, null);
        TransformerTest.transformBidi(insertAutoStyle.toString(), chartOps);
    }

    /*
     * describe('"insertAutoStyle" and "insertAutoStyle"', function () {
     * var OTIS = { otIndexShift: true };
     * it('should not process style sheets of different types', function () {
     * testRunner.runTest(opSeries1(insertAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]), opSeries2(insertAutoStyle, 't2', ['a1', 'a2'], ATTRS));
     * });
     * it('should shift style identifiers in indexed mode', function () {
     * testRunner.runTest(insertAutoStyle('a2', ATTRS), insertAutoStyle('a1', ATTRS), insertAutoStyle('a3', ATTRS), insertAutoStyle('a1', ATTRS, OTIS));
     * testRunner.runTest(insertAutoStyle('a2', ATTRS), insertAutoStyle('a2', ATTRS), insertAutoStyle('a3', ATTRS), insertAutoStyle('a2', ATTRS, OTIS));
     * testRunner.runTest(insertAutoStyle('a2', ATTRS), insertAutoStyle('a3', ATTRS), insertAutoStyle('a2', ATTRS, OTIS), insertAutoStyle('a4', ATTRS));
     * testRunner.runTest(insertAutoStyle('a2', ATTRS), insertAutoStyle('a4', ATTRS), insertAutoStyle('a2', ATTRS, OTIS), insertAutoStyle('a5', ATTRS));
     * // with/without type
     * testRunner.runTest(insertAutoStyle('a2', ATTRS), insertAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('a3', ATTRS), insertAutoStyle('t1', 'a2', ATTRS, OTIS));
     * testRunner.runTest(insertAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('a2', ATTRS), insertAutoStyle('t1', 'a3', ATTRS), insertAutoStyle('a2', ATTRS, OTIS));
     * testRunner.runTest(insertAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('t1', 'a2', ATTRS), insertAutoStyle('t1', 'a3', ATTRS), insertAutoStyle('t1', 'a2', ATTRS, OTIS));
     * });
     * it('should fail for invalid style identifiers in indexed mode', function () {
     * testRunner.expectError(insertAutoStyle('a2', ATTRS), insertAutoStyle('b2', ATTRS));
     * testRunner.expectError(insertAutoStyle('b2', ATTRS), insertAutoStyle('a2', ATTRS));
     * testRunner.expectError(insertAutoStyle('b2', ATTRS), insertAutoStyle('b2', ATTRS));
     * });
     * it('should not shift style identifiers in freestyle mode', function () {
     * testRunner.runTest(insertAutoStyle('t2', 'a2', ATTRS), insertAutoStyle('t2', 'a1', ATTRS));
     * testRunner.runTest(insertAutoStyle('t2', 'a2', ATTRS), insertAutoStyle('t2', 'a3', ATTRS));
     * });
     * it('should fail to insert auto-style twice in freestyle mode', function () {
     * testRunner.expectError(insertAutoStyle('t2', 'a2', ATTRS), insertAutoStyle('t2', 'a2', ATTRS));
     * });
     * });
     */

    @Test
    public void insertAutoStyleInsertAutoStyle01() throws JSONException {
        // Result: Should not process style sheets of different types
        // testRunner.runTest(opSeries1(insertAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
        // opSeries2(insertAutoStyle, 't2', ['a1', 'a2'], ATTRS));
        TransformerTest.transform(Helper.createArrayFromJSON(Helper.createInsertAutoStyleOp("a1", attrs, null), Helper.createInsertAutoStyleOp("t1", "a1", attrs, null)), Helper.createArrayFromJSON(Helper.createInsertAutoStyleOp("t2", "a1", attrs, null), Helper.createInsertAutoStyleOp("t2", "a2", attrs, null)));
    }

    @Test
    public void insertAutoStyleInsertAutoStyle02() throws JSONException {
        // Result: Should shift style identifiers in indexed mode
        // testRunner.runTest(
        //    insertAutoStyle('a2', ATTRS),
        //    insertAutoStyle('a1', ATTRS),
        //    insertAutoStyle('a3', ATTRS),
        //    insertAutoStyle('a1', ATTRS, OTIS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp("a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("a1", attrs, null).toString(), Helper.createInsertAutoStyleOp("a1", attrs, otis).toString(), Helper.createInsertAutoStyleOp("a3", attrs, null).toString());
    }

    @Test
    public void insertAutoStyleInsertAutoStyle03() throws JSONException {
        // Result: Should shift style identifiers in indexed mode
        // testRunner.runTest(
        //    insertAutoStyle('a2', ATTRS),
        //    insertAutoStyle('a2', ATTRS),
        //    insertAutoStyle('a3', ATTRS),
        //    insertAutoStyle('a2', ATTRS, OTIS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp("a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("a2", attrs, otis).toString(), Helper.createInsertAutoStyleOp("a3", attrs, null).toString());
    }

    @Test
    public void insertAutoStyleInsertAutoStyle04() throws JSONException {
        // Result: Should shift style identifiers in indexed mode
        // testRunner.runTest(
        //    insertAutoStyle('a2', ATTRS),
        //    insertAutoStyle('a3', ATTRS),
        //    insertAutoStyle('a2', ATTRS, OTIS),
        //    insertAutoStyle('a4', ATTRS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp("a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("a3", attrs, null).toString(), Helper.createInsertAutoStyleOp("a4", attrs, null).toString(), Helper.createInsertAutoStyleOp("a2", attrs, otis).toString());
    }

    @Test
    public void insertAutoStyleInsertAutoStyle05() throws JSONException {
        // Result: Should shift style identifiers in indexed mode
        // testRunner.runTest(
        //    insertAutoStyle('a2', ATTRS),
        //    insertAutoStyle('a4', ATTRS),
        //    insertAutoStyle('a2', ATTRS, OTIS),
        //    insertAutoStyle('a5', ATTRS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp("a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("a4", attrs, null).toString(), Helper.createInsertAutoStyleOp("a5", attrs, null).toString(), Helper.createInsertAutoStyleOp("a2", attrs, otis).toString());
    }

    @Test
    public void insertAutoStyleInsertAutoStyle06() throws JSONException {
        // Result: Should shift style identifiers in indexed mode
        // testRunner.runTest(
        //    insertAutoStyle('a2', ATTRS),
        //    insertAutoStyle('t1', 'a2', ATTRS),
        //    insertAutoStyle('a3', ATTRS),
        //    insertAutoStyle('t1', 'a2', ATTRS, OTIS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("t1", "a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("t1", "a2", attrs, otis).toString(), Helper.createInsertAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a3", attrs, null).toString());
    }

    @Test
    public void insertAutoStyleInsertAutoStyle07() throws JSONException {
        // Result: Should shift style identifiers in indexed mode
        // testRunner.runTest(
        //    insertAutoStyle('t1', 'a2', ATTRS),
        //    insertAutoStyle('a2', ATTRS),
        //    insertAutoStyle('t1', 'a3', ATTRS),
        //    insertAutoStyle('a2', ATTRS, OTIS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp("t1", "a2", attrs, null).toString(), Helper.createInsertAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a2", attrs, null).toString(), Helper.createInsertAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a2", attrs, otis).toString(), Helper.createInsertAutoStyleOp("t1", "a3", attrs, null).toString());
    }

    @Test
    public void insertAutoStyleInsertAutoStyle08() throws JSONException {
        // Result: Should shift style identifiers in indexed mode
        // testRunner.runTest(
        //    insertAutoStyle('t1', 'a2', ATTRS),
        //    insertAutoStyle('t1', 'a2', ATTRS),
        //    insertAutoStyle('t1', 'a3', ATTRS),
        //    insertAutoStyle('t1', 'a2', ATTRS, OTIS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp("t1", "a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("t1", "a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("t1", "a2", attrs, otis).toString(), Helper.createInsertAutoStyleOp("t1", "a3", attrs, null).toString());
    }

    @Test
    public void insertAutoStyleInsertAutoStyle09() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode
        // testRunner.expectError(
        //    insertAutoStyle('a2', ATTRS),
        //    insertAutoStyle('b2', ATTRS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp("a2", attrs, null).toString(), Helper.createInsertAutoStyleOp("b2", attrs, null).toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }");
    }

    @Test
    public void insertAutoStyleInsertAutoStyle10() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode
        // testRunner.expectError(
        //  insertAutoStyle('b2', ATTRS),
        //  insertAutoStyle('a2', ATTRS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp("b2", attrs, null).toString(), Helper.createInsertAutoStyleOp("a2", attrs, null).toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }");
    }

    @Test
    public void insertAutoStyleInsertAutoStyle11() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode
        // testRunner.expectError(
        //  insertAutoStyle('b2', ATTRS),
        //  insertAutoStyle('b2', ATTRS));
        TransformerTest.transform(Helper.createInsertAutoStyleOp("b2", attrs, null).toString(), Helper.createInsertAutoStyleOp("b2", attrs, null).toString(), "{ _CONFLICT_RELOAD_REQUIRED_: true }", "{ _CONFLICT_RELOAD_REQUIRED_: true }");
    }

    // Info: The client can test this combinations, because it's using only
    // only a type name for deciding if auto-style is indexed or not. The backend
    // relies only on types which are specified within the office formats and
    // if it finds an unknown type it checks if an auto-style starts with a.
    //    @Test
    //    public void insertAutoStyleInsertAutoStyle12() throws JSONException {
    //        // Result: Should not shift style identifiers in freestyle mode.
    //        // testRunner.runTest(
    //        //  insertAutoStyle('t2', 'a2', ATTRS),
    //        //  insertAutoStyle('t2', 'a1', ATTRS));
    //        TransformerTest.transform(
    //            Helper.createInsertAutoStyleOp("t2", "a2", attrs, null).toString(),
    //            Helper.createInsertAutoStyleOp("t2", "a1", attrs, null).toString()
    //        );
    //    }

    //    @Test
    //    public void insertAutoStyleInsertAutoStyle13() throws JSONException {
    //        // Result: Should not shift style identifiers in freestyle mode.
    //        // testRunner.runTest(
    //        //  insertAutoStyle('t2', 'a2', ATTRS),
    //        //  insertAutoStyle('t2', 'a3', ATTRS));
    //        TransformerTest.transform(
    //            Helper.createInsertAutoStyleOp("t2", "a2", attrs, null).toString(),
    //            Helper.createInsertAutoStyleOp("t2", "a3", attrs, null).toString()
    //        );
    //    }
}
