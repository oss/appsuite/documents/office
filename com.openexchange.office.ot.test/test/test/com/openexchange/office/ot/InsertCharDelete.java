/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class InsertCharDelete {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 4], text: 'o' }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'insertText', start: [1, 2], text: 'o' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'o' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2], text: 'o' }",
            "{ name: 'delete', start: [1, 4], end: [1, 7] }",
            "{ name: 'delete', start: [1, 5], end: [1, 8] }",
            "{ name: 'insertText', start: [1, 2], text: 'o' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'o' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4], end: [1, 7] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].end).to.deep.equal([1, 8]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 4], text: 'o' }",
            "{ name: 'delete', start: [1, 4] }",
            "{ name: 'delete', start: [1, 5] }",
            "{ name: 'insertText', start: [1, 4], text: 'o' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'o' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]); // the local delete operation must not shift the insert operation
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5]); // the locally saved operation is modified
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 4], text: 'oooo' }",
            "{ name: 'delete', start: [1, 3], end: [1, 5] }",
            "{ name: 'delete', start: [1, 3], end: [1, 9] }",
            "[]");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'oooo' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 5] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([1, 9]); // the end position of the locally saved operation is modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external operation has the marker, that it is not executed locally
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [0, 4], text: 'a' }",
            "{ name: 'delete', start: [0, 1], end: [0, 8] }",
            "{ name: 'delete', start: [0, 1], end: [0, 9] }",
            "[]");
            /*
                // local delete inside one paragraph
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 4], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [0, 8] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([0, 9]); // the end position of the locally saved operation is modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertText operation has the marker, that it is not executed locally
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [0, 4], text: 'a' }",
            "{ name: 'delete', start: [0, 1], end: [2, 1] }",
            "{ name: 'delete', start: [0, 1], end: [2, 1] }",
            "[]");
            /*
                // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText into first paragraph
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 4], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 1], end: [2, 1] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 1]); // the end position of the locally saved operation is also NOT modified
                expect(localActions[0].operations.length).to.deep.equal(2); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertText operation has the marker, that it is not executed locally
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 4], text: 'a' }",
            "[{ name: 'delete', start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', start: [0], paralength: 2 }]",
            "[{ name: 'delete', start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', start: [0], paralength: 2 }]",
            "[]");
            /*
                // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText into a middle paragraph
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2]); // the end position of the locally saved operation is also NOT modified
                expect(localActions[0].operations.length).to.deep.equal(2); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertText operation has the marker, that it is not executed locally
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2], text: 'a' }",
            "{ name: 'delete', start: [0, 3], end: [2, 3] }",
            "{ name: 'delete', start: [0, 3], end: [2, 4] }",
            "[]");
            /*
                // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText in the last paragraph
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 3], end: [2, 3] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 3]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4]); // the end position of the locally saved operation is modified
                expect(localActions[0].operations.length).to.deep.equal(2); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertText operation has the marker, that it is not executed locally
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 4], text: 'a' }",
            "[{ name: 'delete', start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', start: [0], paralength: 6 }]",
            "[{ name: 'delete', start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', start: [0], paralength: 6 }]",
            "{ name: 'insertText', start: [0, 7], text: 'a' }");
            /*
                // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText in the last paragraph behind the delete range
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 4], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 6 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.deep.equal(2); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertText operation has NOT the marker for local ignoring
                // the external insertText operation has a new position:
                // - 6 positions are specified from the paragraph length of paragraph 0
                // - in the second paragraph the first three chars are deleted -> one char remains before the inserted 'a'
                // - in sum there are 7 chars before the inserted 'a', so that it is inserted at position [0,7]
                expect(oneOperation.start).to.deep.equal([0, 7]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 7], text: 'a' }",
            "{ name: 'delete', start: [0, 2], end: [2, 4] }",
            "{ name: 'delete', start: [0, 2], end: [2, 4] }",
            "{ name: 'insertText', start: [1, 2], text: 'a' }");
            /*
                // local delete over several paragraphs (without a following mergeParagraph operation) and an external insertText in the last paragraph behind the delete range
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 7], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2], end: [2, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 4]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.deep.equal(1); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertText operation has NOT the marker for local ignoring
                expect(oneOperation.start).to.deep.equal([1, 2]); // the following (local) merge is not included here (one paragraph is removed completely)
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 4], text: 'a' }",
            "[{ name: 'delete', start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "[{ name: 'delete', start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', start: [0], paralength: 1 }]",
            "{ name: 'insertText', start: [1, 4], text: 'a' }");
            /*
                // local delete over several paragraphs (includes a mergeParagraph operation) and an external insertText in the following paragraph behind the delete range
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 4], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [0, 2], end: [2, 2] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [0], paralength: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 2]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 2]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.deep.equal(2); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertText operation has NOT the marker for local ignoring
                expect(oneOperation.start).to.deep.equal([1, 4]); // the external insertText operation has a new position
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 4], text: 'a' }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertText', start: [1, 4], text: 'a' }");
            /*
                // local delete of one complete paragraph (no additional mergeParagraph operation) and an external insertText in the following paragraph behind the delete paragraph
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 4], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations.length).to.deep.equal(1); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertText operation has NOT the marker for local ignoring
                expect(oneOperation.start).to.deep.equal([1, 4]); // the external insertText operation has a new position
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [4, 4], text: 'a' }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'insertText', start: [1, 4], text: 'a' }");
            /*
                // local delete of several complete paragraphs (no additional mergeParagraph operation) and an external insertText in the following paragraph behind the last delete paragraph
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [4, 4], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([3]); // the end position of the locally saved operation is not not modified
                expect(localActions[0].operations.length).to.deep.equal(1); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertText operation has NOT the marker for local ignoring
                expect(oneOperation.start).to.deep.equal([1, 4]); // the external insertText operation has a new position
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 4], text: 'o' }",
            "{ name: 'delete', start: [1], end: [3] }",
            "{ name: 'delete', start: [1], end: [3] }",
            "[]");
            /*
                // local delete of one complete paragraph (no additional mergeParagraph operation) and an external insertText in this deleted paragraph
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 4], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [3] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // the start position of the locally saved operation is NOT modified
                expect(localActions[0].operations.length).to.deep.equal(1); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertText operation has the marker for local ignoring
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [4, 4], text: 'o' }",
            "{ name: 'delete', start: [1], end: [5] }",
            "{ name: 'delete', start: [1], end: [5] }",
            "");
            /*
                // local delete of several complete paragraphs (no additional mergeParagraph operation) and an external insertText inside this deleted paragraphs
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [4, 4], text: 'a' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1], end: [5] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].end).to.deep.equal([5]); // the end position of the locally saved operation is not modified
                expect(localActions[0].operations.length).to.deep.equal(1); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external insertText operation has the marker for local ignoring
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 1], text: 'ooo' }",
            "[{ name: 'delete', start: [2, 2], end: [2, 5] }, { name: 'mergeParagraph', start: [2], paralength: 2 }]",
            "[{ name: 'delete', start: [2, 5], end: [2, 8] }, { name: 'mergeParagraph', start: [2], paralength: 5 }]",
            "{ name: 'insertText', start: [2, 1], text: 'ooo' }");
            /*
                // local delete of text range and following merge (example: selection from [2, 2] to [3, 0] and following merge) and an external insertText before the deleted range
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1], text: 'ooo' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 2], end: [2, 5] }, { name: 'mergeParagraph', opl: 1, osn: 2, start: [2], paralength: 2 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]); // the start position of the locally saved delete operation is modified
                expect(localActions[0].operations[0].end).to.deep.equal([2, 8]); // the end position of the locally saved delete operation is also modified
                expect(localActions[0].operations[1].start).to.deep.equal([2]); // the start position of the locally saved merge operation is NOT modified
                expect(localActions[0].operations[1].paralength).to.equal(5); // the paragraph length of the locally saved merge operation is modified(!)
                expect(localActions[0].operations.length).to.deep.equal(2); // there is no new local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external insertText operation has not the marker, that it is not executed locally
                expect(oneOperation.start).to.deep.equal([2, 1]); // the external insertText operation is not modified
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 1], text: 'ooo' }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "[]");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1], text: 'ooo' };
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.deep.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 1, 4, 2], text: 'ooo' }",
            "{ name: 'delete', start: [2] }",
            "{ name: 'delete', start: [2] }",
            "[]");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 4, 2], text: 'ooo' }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.deep.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 1, 4, 2], text: 'ooo' }",
            "{ name: 'delete', start: [2, 1] }",
            "{ name: 'delete', start: [2, 1] }",
            "[]");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 4, 2], text: 'ooo' }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.deep.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 1, 4, 2], text: 'ooo' }",
            "{ name: 'delete', start: [2, 1, 4] }",
            "{ name: 'delete', start: [2, 1, 4] }",
            "[]");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 4, 2], text: 'ooo' }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [2, 1, 4] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 4]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.deep.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 1, 4, 2], text: 'ooo' }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'delete', start: [1] }",
            "{ name: 'insertText', start: [1, 1, 4, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 4, 2], text: 'ooo' }; // in a text frame
                localActions = [{ operations: [{ name: 'delete', opl: 1, osn: 1, start: [1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].end).to.equal(undefined);
                expect(localActions[0].operations.length).to.deep.equal(1);
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined);
                expect(oneOperation.start).to.deep.equal([1, 1, 4, 2]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 1], end: [1, 2] }",
            "{ name: 'insertText', start: [1, 4], text: 'o' }",
            "{ name: 'insertText', start: [1, 2], text: 'o' }",
            "{ name: 'delete', start: [1, 1], end: [1, 2] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 1], end: [1, 2] };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(oneOperation.end).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 6], end: [1, 8] }",
            "{ name: 'insertText', start: [1, 4], text: 'ooo' }",
            "{ name: 'insertText', start: [1, 4], text: 'ooo' }",
            "{ name: 'delete', start: [1, 9], end: [1, 11] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 6], end: [1, 8] };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 9]);
                expect(oneOperation.end).to.deep.equal([1, 11]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3] }",
            "{ name: 'insertText', start: [1, 3], text: 'o' }",
            "{ name: 'insertText', start: [1, 3], text: 'o' }",
            "{ name: 'delete', start: [1, 4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 3], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 12], end: [1, 14] }",
            "{ name: 'insertText', start: [1, 12], text: 'ooo' }",
            "{ name: 'insertText', start: [1, 12], text: 'ooo' }",
            "{ name: 'delete', start: [1, 15], end: [1, 17] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 12], end: [1, 14] };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 12], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 15]);
                expect(oneOperation.end).to.deep.equal([1, 17]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 12]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3], end: [1, 7] }",
            "{ name: 'insertText', start: [1, 4], text: 'ooo' }",
            "[]",
            "{ name: 'delete', start: [1, 3], end: [1, 10] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 7] };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3]);
                expect(oneOperation.end).to.deep.equal([1, 10]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3] }",
            "{ name: 'insertText', start: [1, 5, 2, 3], text: 'ooo' }",
            "{ name: 'insertText', start: [1, 4, 2, 3], text: 'ooo' }",
            "{ name: 'delete', start: [1, 3] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3] };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 5, 2, 3], text: 'ooo' }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 2, 3]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3, 4, 5] }",
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }",
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }",
            "{ name: 'delete', start: [1, 6, 4, 5] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 4, 5] }; // in a text frame
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6, 4, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3], end: [1, 4] }",
            "{ name: 'insertText', start: [1, 5, 2, 3], text: 'ooo' }",
            "{ name: 'insertText', start: [1, 3, 2, 3], text: 'ooo' }",
            "{ name: 'delete', start: [1, 3], end: [1, 4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 4] };
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 5, 2, 3], text: 'ooo' }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3]);
                expect(oneOperation.end).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3, 4, 5], end: [1, 3, 4, 6] }",
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }",
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }",
            "{ name: 'delete', start: [1, 6, 4, 5], end: [1, 6, 4, 6] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3, 4, 5], end: [1, 3, 4, 6] }; // in a text frame
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6, 4, 5]);
                expect(oneOperation.end).to.deep.equal([1, 6, 4, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'delete', start: [1, 3], end: [1, 4] }",
            "{ name: 'insertText', start: [1, 4, 2, 3], text: 'ooo' }",
            "[]",
            "{ name: 'delete', start: [1, 3], end: [1, 4] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [1, 3], end: [1, 4] }; // in a text frame
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [1, 4, 2, 3], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 3]);
                expect(oneOperation.end).to.deep.equal([1, 4]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 2, 3]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'delete', start: [0], end: [5, 10] }",
            "{ name: 'insertText', start: [2, 7], text: '1' }",
            "[]",
            "{ name: 'delete', start: [0], end: [5, 10] }");
            /*
                oneOperation = { name: 'delete', opl: 1, osn: 1, start: [0], end: [5, 10] }; // in a text frame
                localActions = [{ operations: [{ name: 'insertText', opl: 1, osn: 1, start: [2, 7], text: '1' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.end).to.deep.equal([5, 10]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
             */
    }
}
