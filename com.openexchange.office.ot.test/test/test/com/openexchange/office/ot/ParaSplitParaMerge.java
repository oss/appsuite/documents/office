/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ParaSplitParaMerge {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }",
            "{ name: 'splitParagraph', start: [1, 5] }");
            /*
                // split long before the local merge
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 5] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }",
            "{ name: 'splitParagraph', start: [2, 5] }");
            /*
                // split in the previous paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 5] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 8 }",
            "{ name: 'splitParagraph', start: [2, 2] }");
            /*
                // split and merge in the same paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(8); // the paragraph length does change
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 5] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 10 }",
            "{ name: 'splitParagraph', start: [2, 15] }");
            /*
                // split in the directly following paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 5] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 15]); // the external operation is modified because of local merge operation
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [5, 5] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'splitParagraph', start: [4, 5] }");
            /*
                // split far after the local merge
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 5] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4, 5]); // the external operation is modified
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 2, 1, 4, 2] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }",
            "{ name: 'splitParagraph', start: [2, 2, 1, 4, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 2, 1, 4, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 1, 4, 2]); // the external operation is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 2] }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 5 }",
            "{ name: 'splitParagraph', start: [3, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 3], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 2]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [3, 2, 2, 3], paralength: 5 }",
            "{ name: 'splitParagraph', start: [1, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 3], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 3]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 4] }",
            "{ name: 'mergeParagraph', start: [1, 2, 3, 2], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 2, 3, 3], paralength: 5 }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 4] }");
            /*
                // split and merge with in the same table cell
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 3, 2], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1, 4]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 3]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 4] }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 2], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 2], paralength: 5 }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 4] }");
            /*
                // split and merge with in different table cells
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 2], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1, 4]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 2]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 7] }",
            "{ name: 'mergeParagraph', start: [0, 11, 1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1, 4, 1], paralength: 10 }",
            "{ name: 'splitParagraph', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 11, 1], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [2, 3, 3], paralength: 10 }",
            "{ name: 'splitParagraph', start: [1, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5] }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [2, 0, 3], paralength: 10 }",
            "{ name: 'splitParagraph', start: [1, 5] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 6] }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'splitParagraph', start: [1, 6] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 6] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
          */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 1] }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'splitParagraph', start: [2, 1] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 1] }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [2, 5, 3], paralength: 10 }",
            "{ name: 'splitParagraph', start: [0, 1] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 1] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1, 5, 4], paralength: 10 }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 4]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'mergeParagraph', start: [0], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 10 }",
            "{ name: 'splitParagraph', start: [0, 15, 2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 15, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'splitParagraph', start: [3, 5] }",
            "{ name: 'splitParagraph', start: [2, 5] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }");
            /*
                // merge long before the local split
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 10 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 10 }",
            "{ name: 'splitParagraph', start: [3, 5] }",
            "{ name: 'splitParagraph', start: [2, 15] }",
            "{ name: 'mergeParagraph', start: [2], paralength: 10 }");
            /*
                // merge in the previous paragraph
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 10 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 15]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 10 }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 7 }");
            /*
                // merge and split in the same paragraph
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [2], paralength: 10 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3]); // the external operation is modified
                expect(oneOperation.paralength).to.equal(7); // the paragraph length does change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }");
            /*
                // merge in the directly following paragraph
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 10 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]); // the external operation is modified
                expect(oneOperation.paralength).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'mergeParagraph', start: [5], paralength: 10 }");
            /*
                // split far after the local merge
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([5]); // the external operation is modified
                expect(oneOperation.paralength).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3, 2, 1, 4], paralength: 6 }",
            "{ name: 'splitParagraph', start: [1, 3] }",
            "{ name: 'splitParagraph', start: [1, 3] }",
            "{ name: 'mergeParagraph', start: [4, 2, 1, 4], paralength: 6 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 2, 1, 4], paralength: 6 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4, 2, 1, 4]); // the external operation is modified
                expect(oneOperation.paralength).to.equal(6); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 6 }",
            "{ name: 'splitParagraph', start: [1, 2, 2, 3, 1] }",
            "{ name: 'splitParagraph', start: [1, 2, 2, 3, 1] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 6 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 6 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 2, 3, 1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(6); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3, 1]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'splitParagraph', start: [3, 2, 2, 3, 1] }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 3, 1] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 2 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 2, 2, 3, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(2); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 3, 1]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 2, 3, 1], paralength: 4 }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 2, 1] }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 5] }",
            "{ name: 'mergeParagraph', start: [1, 2, 3, 1], paralength: 4 }");
            /*
                // split and merge with in the same table cell
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1], paralength: 4 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(4); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 1, 5]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 2, 3, 1], paralength: 4 }",
            "{ name: 'splitParagraph', start: [1, 2, 2, 2, 1] }",
            "{ name: 'splitParagraph', start: [1, 2, 2, 2, 1] }",
            "{ name: 'mergeParagraph', start: [1, 2, 3, 1], paralength: 4 }");
            /*
                // split and merge with in different table cells
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1], paralength: 4 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 2, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(4); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 2, 1]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }",
            "{ name: 'splitTable', start: [1, 5] }");
            /*
                // split long before the local merge
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 5] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }",
            "{ name: 'splitTable', start: [2, 5] }");
            /*
                // split in the previous paragraph
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'mergeParagraph', start: [2, 4, 5, 6], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [3, 2, 5, 6], paralength: 10 }",
            "{ name: 'splitTable', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 5, 6], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 5, 6]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'mergeParagraph', start: [2, 1, 5, 6], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [2, 1, 5, 6], paralength: 10 }",
            "{ name: 'splitTable', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 5, 6], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 5, 6]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'mergeParagraph', start: [3, 3, 1, 1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [4, 3, 1, 1], paralength: 10 }",
            "{ name: 'splitTable', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 3, 1, 1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 3, 1, 1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 5] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'splitTable', start: [2, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5, 0, 2] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'splitTable', start: [1, 5, 0, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 0, 2] }; // inside a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 5, 0, 2] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'splitTable', start: [1, 15, 0, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5, 0, 2] }; // inside a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 15, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 5, 0, 2] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'splitTable', start: [2, 5, 0, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5, 0, 2] }; // inside a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [5, 5] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'splitTable', start: [4, 5] }");
            /*
                // split far after the local merge
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [5, 5] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4, 5]); // the external operation is modified
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 2, 1, 4, 2] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }",
            "{ name: 'splitTable', start: [2, 2, 1, 4, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 2, 1, 4, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 1, 4, 2]); // the external operation is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 2] }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 5 }",
            "{ name: 'splitTable', start: [3, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 3], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 2]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [3, 2, 2, 3], paralength: 5 }",
            "{ name: 'splitTable', start: [1, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 3], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 3]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2, 3, 1, 4] }",
            "{ name: 'mergeParagraph', start: [1, 2, 3, 2], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 2, 3, 3], paralength: 5 }",
            "{ name: 'splitTable', start: [1, 2, 3, 1, 4] }");
                        /*
                // split and merge with in the same table cell
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 3, 2], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1, 4]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 3]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2, 3, 1, 4] }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 2], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 2], paralength: 5 }",
            "{ name: 'splitTable', start: [1, 2, 3, 1, 4] }");
            /*
                // split and merge with in different table cells
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 2], opl: 1, osn: 1, paralength: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1, 4]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 2]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeParagraph', start: [0, 11, 1, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1, 4, 1, 3], paralength: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 11, 1, 3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 1, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeParagraph', start: [0, 7, 1, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1, 0, 1, 3], paralength: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 7, 1, 3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeParagraph', start: [0, 7, 1, 3, 2, 1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1, 0, 1, 3, 2, 1], paralength: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 7, 1, 3, 2, 1], opl: 1, osn: 1, paralength: 10 }] }]; // textframe in table
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1, 3, 2, 1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeParagraph', start: [1, 7, 1, 3, 2, 1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [2, 7, 1, 3, 2, 1], paralength: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 7, 1, 3, 2, 1], opl: 1, osn: 1, paralength: 10 }] }]; // textframe in table
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 7, 1, 3, 2, 1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeParagraph', start: [0, 6, 1, 3, 2, 1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [0, 6, 1, 3, 2 ,1], paralength: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 6, 1, 3, 2, 1], opl: 1, osn: 1, paralength: 10 }] }]; // textframe in table
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6, 1, 3, 2, 1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeParagraph', start: [0, 6, 1, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [0, 6, 1, 3], paralength: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0, 6, 1, 3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6, 1, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'splitTable', start: [2, 1] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 1] }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [2, 5, 3], paralength: 10 }",
            "{ name: 'splitTable', start: [0, 1] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 1] };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",
            "{ name: 'mergeParagraph', start: [1, 5, 3], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1, 5, 4], paralength: 10 }",
            "{ name: 'splitTable', start: [1, 5, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 5, 3], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 4]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'splitTable', start: [1, 5, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);

             */
    }
    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",
            "{ name: 'mergeParagraph', start: [0], paralength: 10 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 10 }",
            "{ name: 'splitTable', start: [0, 15, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], opl: 1, osn: 1, paralength: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 15, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'splitTable', start: [3, 5] }",
            "{ name: 'splitTable', start: [2, 5] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }");
            /*
                // merge long before the local split
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 10 };
                localActions = [{ operations: [{ name: 'splitTable', start: [3, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 10 }",
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }");
            /*
                // merge in the directly following paragraph
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 10 };
                localActions = [{ operations: [{ name: 'splitTable', start: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]); // the external operation is modified
                expect(oneOperation.paralength).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [4], paralength: 10 }",
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'mergeParagraph', start: [5], paralength: 10 }");
            /*
                // split far after the local merge
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [4], paralength: 10 };
                localActions = [{ operations: [{ name: 'splitTable', start: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([5]); // the external operation is modified
                expect(oneOperation.paralength).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3, 2, 1, 4], paralength: 6 }",
            "{ name: 'splitTable', start: [1, 3] }",
            "{ name: 'splitTable', start: [1, 3] }",
            "{ name: 'mergeParagraph', start: [4, 2, 1, 4], paralength: 6 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3, 2, 1, 4], paralength: 6 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4, 2, 1, 4]); // the external operation is modified
                expect(oneOperation.paralength).to.equal(6); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 6 }",
            "{ name: 'splitTable', start: [1, 2, 2, 3, 1] }",
            "{ name: 'splitTable', start: [1, 2, 2, 3, 1] }",
            "{ name: 'mergeParagraph', start: [3], paralength: 6 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [3], paralength: 6 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 2, 3, 1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(6); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3, 1]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'splitTable', start: [3, 2, 2, 3, 1] }",
            "{ name: 'splitTable', start: [2, 2, 2, 3, 1] }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1], paralength: 2 };
                localActions = [{ operations: [{ name: 'splitTable', start: [3, 2, 2, 3, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(2); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 3, 1]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 2, 3, 0], paralength: 4 }",
            "{ name: 'splitTable', start: [1, 2, 3, 2, 1] }",
            "{ name: 'splitTable', start: [1, 2, 3, 1, 1] }",
            "{ name: 'mergeParagraph', start: [1, 2, 3, 0], paralength: 4 }");
            /*
                // split and merge with in the same table cell
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 0], paralength: 4 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 0]); // the external operation is not modified
                expect(oneOperation.paralength).to.equal(4); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 1, 1]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 2, 3, 4], paralength: 4 }",
            "{ name: 'splitTable', start: [1, 2, 3, 2, 1] }",
            "{ name: 'splitTable', start: [1, 2, 3, 2, 1] }",
            "{ name: 'mergeParagraph', start: [1, 2, 3, 5], paralength: 4 }");
            /*
                // split and merge with in the same table cell
                oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 4], paralength: 4 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 5]);
                expect(oneOperation.paralength).to.equal(4);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 2, 1]);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 2, 3, 1], paralength: 4 }",
            "{ name: 'splitTable', start: [1, 2, 2, 2, 1] }",
            "{ name: 'splitTable', start: [1, 2, 2, 2, 1] }",
            "{ name: 'mergeParagraph', start: [1, 2, 3, 1], paralength: 4 }");
            /*
            // split and merge with in different table cells
            oneOperation = { name: 'mergeParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1], paralength: 4 };
            localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 2, 2, 1], opl: 1, osn: 1 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([1, 2, 3, 1]); // the external operation is not modified
            expect(oneOperation.paralength).to.equal(4); // the paragraph length does not change
            expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 2, 1]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5] }",
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [4], rowcount: 10 }",
            "{ name: 'splitTable', start: [1, 5] }");
            /*
                // split long before the local merge
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test68() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 5] }",
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [4], rowcount: 10 }",
            "{ name: 'splitTable', start: [2, 5] }");
            /*
                    oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5] };
                    localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
                    otManager.transformOperation(oneOperation, localActions);
                    expect(oneOperation.start).to.deep.equal([2, 5]); // the external operation is not modified
                    expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                    expect(localActions[0].operations[0].rowcount).to.equal(10); // the paragraph length does not change
                  */
    }

    @Test
    public void test69() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 5] }",
            "{ name: 'mergeTable', start: [3], rowcount: 9 }",
            "{ name: 'mergeTable', start: [4], rowcount: 4 }",
            "{ name: 'splitTable', start: [3, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 9 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 5]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(4); // the paragraph length does not change
             */
    }

    @Test
    public void test70() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'mergeTable', start: [2, 4, 5, 6], rowcount: 10 }",
            "{ name: 'mergeTable', start: [3, 2, 5, 6], rowcount: 10 }",
            "{ name: 'splitTable', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 5, 6], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 5, 6]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test71() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'mergeTable', start: [2, 1, 5, 6], rowcount: 10 }",
            "{ name: 'mergeTable', start: [2, 1, 5, 6], rowcount: 10 }",
            "{ name: 'splitTable', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 5, 6], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 5, 6]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test72() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",
            "{ name: 'mergeTable', start: [3, 3, 1, 1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [4, 3, 1, 1], rowcount: 10 }",
            "{ name: 'splitTable', start: [2, 2] }");
            /*
                 oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 1, 1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 3, 1, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
            */
    }

    @Test
    public void test73() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 5] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitTable', start: [2, 5] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test74() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5, 0, 2, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitTable', start: [1, 5, 0, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 0, 2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 0, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test75() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 5, 0, 2, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitTable', start: [1, 15, 0, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 5, 0, 2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 15, 0, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test76() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 5, 0, 2, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitTable', start: [2, 5, 0, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 5, 0, 2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 0, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test77() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [5, 5] }",
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'splitTable', start: [4, 5] }");
            /*
                // split far after the local merge
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [5, 5] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4, 5]); // the external operation is modified
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test78() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 2, 1, 4, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",
            "{ name: 'splitTable', start: [2, 2, 1, 4, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 2, 1, 4, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 1, 4, 2]); // the external operation is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test79() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [3, 2] }",
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 5 }",
            "{ name: 'splitTable', start: [3, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [3, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 3], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 2]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test80() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2] }",
            "{ name: 'mergeTable', start: [2, 2, 2, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [3, 2, 2, 3], rowcount: 5 }",
            "{ name: 'splitTable', start: [1, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 3], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 3]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test81() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2, 3, 1, 4] }",
            "{ name: 'mergeTable', start: [1, 2, 3, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 2, 3, 3], rowcount: 5 }",
            "{ name: 'splitTable', start: [1, 2, 3, 1, 4] }");
        /*
        // split and merge with in the same table cell
        oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
        localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 3, 2], opl: 1, osn: 1, rowcount: 5 }] }];
        otManager.transformOperation(oneOperation, localActions);
        expect(oneOperation.start).to.deep.equal([1, 2, 3, 1, 4]); // the external operation is not modified
    expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 3]); // the start position of the locally saved operation is modified
    expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
     */
    }

    @Test
    public void test82() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 2, 3, 1, 4] }",
            "{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 5 }",
            "{ name: 'splitTable', start: [1, 2, 3, 1, 4] }");
            /*
                // split and merge with in different table cells
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 2], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1, 4]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 2]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test83() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 11, 1, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 4, 1, 3], rowcount: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 11, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 1, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test84() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 7, 1, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 0, 1, 3], rowcount: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 7, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test85() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 7, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 0, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 7, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1, 3, 2, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
            */
    }

    @Test
    public void test86() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeTable', start: [1, 7, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [2, 7, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 7, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 7, 1, 3, 2, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test87() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 6, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [0, 6, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 6, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6, 1, 3, 2, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test88() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 6, 1, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [0, 6, 1, 3], rowcount: 10 }",
            "{ name: 'splitTable', start: [0, 7] }");
                /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 6, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6, 1, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test89() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",
            "{ name: 'mergeTable', start: [1, 5, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 5, 3], rowcount: 10 }",
            "{ name: 'splitTable', start: [2, 1] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [2, 1] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test90() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [0, 1] }",
            "{ name: 'mergeTable', start: [1, 5, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [2, 5, 3], rowcount: 10 }",
            "{ name: 'splitTable', start: [0, 1] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [0, 1] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test91() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5, 2, 2] }",
            "{ name: 'mergeTable', start: [1, 5, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 5, 4], rowcount: 10 }",
            "{ name: 'splitTable', start: [1, 5, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 4]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test92() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5, 2, 2, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitTable', start: [1, 5, 2, 2, 2] }");
            /*
                oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test93() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 5, 2, 2, 2] }",
            "{ name: 'mergeTable', start: [0], rowcount: 10 }",
            "{ name: 'mergeTable', start: [0], rowcount: 10 }",
            "{ name: 'splitTable', start: [0, 15, 2, 2, 2] }");
            /*
            oneOperation = { name: 'splitTable', opl: 1, osn: 1, start: [1, 5, 2, 2, 2] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([0, 15, 2, 2, 2]);
            expect(localActions[0].operations[0].start).to.deep.equal([0]);
            expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test94() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitTable', start: [3, 5] }",
            "{ name: 'splitTable', start: [2, 5] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }");
        /*
            // merge long before the local split
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 10 };
                localActions = [{ operations: [{ name: 'splitTable', start: [3, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]); // the external operation is not modified
                expect(oneOperation.rowcount).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test95() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'mergeTable', start: [4], rowcount: 10 }");
            /*
                // merge in the directly following paragraph
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 10 };
                localActions = [{ operations: [{ name: 'splitTable', start: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]); // the external operation is modified
                expect(oneOperation.rowcount).to.equal(10); // the paragraph length does not change
                    expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test96() {

        TransformerTest.transform(
            "{ name: 'mergeTable', start: [4], rowcount: 10 }",
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'splitTable', start: [2, 3] }",
            "{ name: 'mergeTable', start: [5], rowcount: 10 }");
            /*
                // split far after the local merge
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [4], rowcount: 10 };
                localActions = [{ operations: [{ name: 'splitTable', start: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([5]); // the external operation is modified
                expect(oneOperation.rowcount).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test97() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3, 2, 1, 4], rowcount: 6 }",
            "{ name: 'splitTable', start: [1, 3] }",
            "{ name: 'splitTable', start: [1, 3] }",
            "{ name: 'mergeTable', start: [4, 2, 1, 4], rowcount: 6 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 2, 1, 4], rowcount: 6 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4, 2, 1, 4]); // the external operation is modified
                expect(oneOperation.rowcount).to.equal(6); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test98() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 6 }",
            "{ name: 'splitTable', start: [1, 2, 2, 3, 1] }",
            "{ name: 'splitTable', start: [1, 2, 2, 3, 1] }",
            "{ name: 'mergeTable', start: [3], rowcount: 6 }");
            /*
    // split and merge on different paragraph levels
        oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 6 };
        localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 2, 3, 1] }] }];
        otManager.transformOperation(oneOperation, localActions);
        expect(oneOperation.start).to.deep.equal([3]); // the external operation is not modified
        expect(oneOperation.rowcount).to.equal(6); // the paragraph length does not change
        expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3, 1]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test99() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'splitTable', start: [3, 2, 2, 3, 1] }",
            "{ name: 'splitTable', start: [2, 2, 2, 3 ,1] }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 2 };
                localActions = [{ operations: [{ name: 'splitTable', start: [3, 2, 2, 3, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]); // the external operation is not modified
                expect(oneOperation.rowcount).to.equal(2); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 3, 1]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test100() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 3, 0], rowcount: 4 }",
            "{ name: 'splitTable', start: [1, 2, 3, 2, 1] }",
            "{ name: 'splitTable', start: [1, 2, 3, 1, 1] }",
            "{ name: 'mergeTable', start: [1, 2, 3, 0], rowcount: 4 }");
            /*
                // split and merge with in the same table cell
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 0], rowcount: 4 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 0]); // the external operation is not modified
                expect(oneOperation.rowcount).to.equal(4); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 1, 1]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test101() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 3, 4], rowcount: 4 }",
            "{ name: 'splitTable', start: [1, 2, 3, 2, 1] }",
            "{ name: 'splitTable', start: [1, 2, 3, 2, 1] }",
            "{ name: 'mergeTable', start: [1, 2, 3, 5], rowcount: 4 }");
            /*
                // split and merge with in the same table cell
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 4], rowcount: 4 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 5]);
                expect(oneOperation.rowcount).to.equal(4);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 2, 1]);
             */
    }

    @Test
    public void test102() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 3, 1], rowcount: 4 }",
            "{ name: 'splitTable', start: [1, 2, 2, 2, 1] }",
            "{ name: 'splitTable', start: [1, 2, 2, 2, 1] }",
            "{ name: 'mergeTable', start: [1, 2, 3, 1], rowcount: 4 }");
            /*
                // split and merge with in different table cells
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 1], rowcount: 4 };
                localActions = [{ operations: [{ name: 'splitTable', start: [1, 2, 2, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1]); // the external operation is not modified
                expect(oneOperation.rowcount).to.equal(4); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 2, 1]); // the start position of the locally saved operation is not modified
            */
    }

    @Test
    public void test103() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5] }",
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [4], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [1, 5] }");
            /*
                // split long before the local merge
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test104() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 5] }",
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [4], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [2, 5] }");
            /*
                // split in the previous paragraph
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 5] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test105() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 5] }",
            "{ name: 'mergeTable', start: [3], rowcount: 9 }",
            "{ name: 'mergeTable', start: [4], rowcount: 4 }",
            "{ name: 'splitParagraph', start: [3, 5] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 5] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 9 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 5]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([4]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(4); // the paragraph length does not change
                         */
    }

    @Test
    public void test106() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'mergeTable', start: [2, 4, 5, 6], rowcount: 10 }",
            "{ name: 'mergeTable', start: [3, 2, 5, 6], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 5, 6], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 5, 6]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test107() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'mergeTable', start: [2, 1, 5, 6], rowcount: 10 }",
            "{ name: 'mergeTable', start: [2, 1, 5, 6], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 5, 6], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 5, 6]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test108() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 2] }",
            "{ name: 'mergeTable', start: [3, 3, 1, 1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [4, 3, 1, 1], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 3, 1, 1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([4, 3, 1, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test109() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 5] }",
        "{ name: 'mergeTable', start: [1], rowcount: 10 }",
        "{ name: 'mergeTable', start: [1], rowcount: 10 }",
        "{ name: 'splitParagraph', start: [2, 5] }");
        /*
             oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 5] };
            localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
            otManager.transformOperation(oneOperation, localActions);
            expect(oneOperation.start).to.deep.equal([2, 5]);
            expect(localActions[0].operations[0].start).to.deep.equal([1]);
            expect(localActions[0].operations[0].rowcount).to.equal(10);
            */
    }

    @Test
    public void test110() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5, 0, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [1, 5, 0, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 0, 2] }; // inside a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test111() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 5, 0, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [1, 15, 0, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 5, 0, 2] }; // inside a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 15, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test112() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 5, 0, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [2, 5, 0, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 5, 0, 2] }; // inside a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 0, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test113() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [5, 5] }",
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [4, 5] }");
            /*
                // split far after the local merge
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [5, 5] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4, 5]); // the external operation is modified
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(10); // the paragraph length does not change
             */
    }

    @Test
    public void test114() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 2, 1, 4, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }",
            "{ name: 'splitParagraph', start: [2, 2, 1, 4, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 2, 1, 4, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 1, 4, 2]); // the external operation is modified
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test115() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [3, 2] }",
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 5 }",
            "{ name: 'splitParagraph', start: [3, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [3, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 3], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3, 2]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test116() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2] }",
            "{ name: 'mergeTable', start: [2, 2, 2, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [3, 2, 2, 3], rowcount: 5 }",
            "{ name: 'splitParagraph', start: [1, 2] }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 3], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([3, 2, 2, 3]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test117() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 4] }",
            "{ name: 'mergeTable', start: [1, 2, 3, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 2, 3, 3], rowcount: 5 }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 4] }");
            /*
                // split and merge with in the same table cell
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 3, 2], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1, 4]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 3]); // the start position of the locally saved operation is modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test118() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 4] }",
            "{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 5 }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 4] }");
            /*
                // split and merge with in different table cells
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 2, 3, 1, 4] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 2], opl: 1, osn: 1, rowcount: 5 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1, 4]); // the external operation is not modified
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 2]); // the start position of the locally saved operation is not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // the paragraph length does not change
             */
    }

    @Test
    public void test119() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 11, 1, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 4, 1, 3], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 11, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 1, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test120() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 7, 1, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 0, 1, 3], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 7, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test121() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 7, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 0, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 7, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 0, 1, 3, 2, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test122() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 7] }",
            "{ name: 'mergeTable', start: [1, 7, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [2, 7, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 7, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 7, 1, 3, 2, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test123() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 6, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [0, 6, 1, 3, 2, 1], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 6, 1, 3, 2, 1], opl: 1, osn: 1, rowcount: 10 }] }]; // textframe in table
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6, 1, 3, 2, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test124() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 7] }",
            "{ name: 'mergeTable', start: [0, 6, 1, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [0, 6, 1, 3], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [0, 7] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 7] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0, 6, 1, 3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 7]);
                expect(localActions[0].operations[0].start).to.deep.equal([0, 6, 1, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test125() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [2, 1] }",
            "{ name: 'mergeTable', start: [1, 5, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 5, 3], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [2, 1] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [2, 1] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test126() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [0, 1] }",
            "{ name: 'mergeTable', start: [1, 5, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [2, 5, 3], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [0, 1] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [0, 1] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test127() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }",
            "{ name: 'mergeTable', start: [1, 5, 3], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1, 5, 4], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 5, 3], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 4]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test128() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5, 2, 2, 2] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [1, 5, 2, 2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2, 2] };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test129() {
        TransformerTest.transform(
            "{ name: 'splitParagraph', start: [1, 5, 2, 2, 2] }",
            "{ name: 'mergeTable', start: [0], rowcount: 10 }",
            "{ name: 'mergeTable', start: [0], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [0, 15, 2, 2, 2] }");
            /*
                oneOperation = { name: 'splitParagraph', opl: 1, osn: 1, start: [1, 5, 2, 2, 2] }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], opl: 1, osn: 1, rowcount: 10 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 15, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0].rowcount).to.equal(10);
             */
    }

    @Test
    public void test130() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [3, 5] }",
            "{ name: 'splitParagraph', start: [2, 5] }",
            "{ name: 'mergeTable', start: [1], rowcount: 10 }");
            /*
                // merge long before the local split
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 10 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 5], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]); // the external operation is not modified
                expect(oneOperation.rowcount).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]); // the start position of the locally saved operation is modified

             */
    }
    @Test
    public void test131() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'mergeTable', start: [4], rowcount: 10 }");
            /*
                // merge in the directly following paragraph
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 10 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4]); // the external operation is modified
                expect(oneOperation.rowcount).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test132() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [4], rowcount: 10 }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'splitParagraph', start: [2, 3] }",
            "{ name: 'mergeTable', start: [5], rowcount: 10 }");
            /*
                // split far after the local merge
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [4], rowcount: 10 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [2, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([5]); // the external operation is modified
                expect(oneOperation.rowcount).to.equal(10); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 3]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test133() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3, 2, 1, 4], rowcount: 6 }",
            "{ name: 'splitParagraph', start: [1, 3] }",
            "{ name: 'splitParagraph', start: [1, 3] }",
            "{ name: 'mergeTable', start: [4, 2, 1, 4], rowcount: 6 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3, 2, 1, 4], rowcount: 6 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 3], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([4, 2, 1, 4]); // the external operation is modified
                expect(oneOperation.rowcount).to.equal(6); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]); // the start position of the locally saved operation is not modified
            */
    }

    @Test
    public void test134() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 6 }",
            "{ name: 'splitParagraph', start: [1, 2, 2, 3, 1] }",
            "{ name: 'splitParagraph', start: [1, 2, 2, 3, 1] }",
            "{ name: 'mergeTable', start: [3], rowcount: 6 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [3], rowcount: 6 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 2, 3, 1] }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([3]); // the external operation is not modified
                expect(oneOperation.rowcount).to.equal(6); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3, 1]); // the start position of the locally saved operation is not modified
             */
    }

    @Test
    public void test135() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'splitParagraph', start: [3, 2, 2, 3, 1] }",
            "{ name: 'splitParagraph', start: [2, 2, 2, 3, 1] }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }");
            /*
                // split and merge on different paragraph levels
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1], rowcount: 2 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [3, 2, 2, 3, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1]); // the external operation is not modified
                expect(oneOperation.rowcount).to.equal(2); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 3, 1]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test136() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 3, 0], rowcount: 4 }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 2, 1] }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 1, 1] }",
            "{ name: 'mergeTable', start: [1, 2, 3, 0], rowcount: 4 }");
            /*
                // split and merge with in the same table cell
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 0], rowcount: 4 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 0]); // the external operation is not modified
                expect(oneOperation.rowcount).to.equal(4); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 1, 1]); // the start position of the locally saved operation is modified
             */
    }

    @Test
    public void test137() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 3, 4], rowcount: 4 }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 2, 1] }",
            "{ name: 'splitParagraph', start: [1, 2, 3, 2, 1] }",
            "{ name: 'mergeTable', start: [1, 2, 3, 5], rowcount: 4 }");
            /*
                // split and merge with in the same table cell
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 4], rowcount: 4 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 3, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 5]);
                expect(oneOperation.rowcount).to.equal(4);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 3, 2, 1]);
             */
    }

    @Test
    public void test138() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 3, 1], rowcount: 4 }",
            "{ name: 'splitParagraph', start: [1, 2, 2, 2, 1] }",
            "{ name: 'splitParagraph', start: [1, 2, 2, 2, 1] }",
            "{ name: 'mergeTable', start: [1, 2, 3, 1], rowcount: 4 }");
            /*
                // split and merge with in different table cells
                oneOperation = { name: 'mergeTable', opl: 1, osn: 1, start: [1, 2, 3, 1], rowcount: 4 };
                localActions = [{ operations: [{ name: 'splitParagraph', start: [1, 2, 2, 2, 1], opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 3, 1]); // the external operation is not modified
                expect(oneOperation.rowcount).to.equal(4); // the paragraph length does not change
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 2, 1]); // the start position of the locally saved operation is not modified
             */
    }
}
