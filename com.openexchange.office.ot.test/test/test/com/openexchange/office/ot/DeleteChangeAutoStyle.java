/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class DeleteChangeAutoStyle {
    /**
     * The client tests are using T1 as a default
     * name for a type, but the backend is using cell.
     * In order to let client tests run in the backend,
     * auto style type tests without a style have to
     * use this default.
     * e.g. see insertAutoStyleInsertAutoStyle06()
     */
    private final static String DEFAULT_AUTO_STYLE_TYPE = "t1";
    private final String        otis                    = Helper.createJSONObject("{ otIndexShift: true }").toString();
    private final JSONObject    attrs                   = Helper.createJSONObject("{ f1: { a1: 10 } }");

    /*
     * describe('"deleteAutoStyle" and "changeAutoStyle"', function () {
            var OTIS = { otIndexShift: true };
            it('should not process style sheets of different types', function () {
                testRunner.runBidiTest(opSeries1(deleteAutoStyle, 'a1', ['t1', 'a1']), opSeries2(changeAutoStyle, 't2', ['a1', 'a2'], ATTRS));
            });
            it('should shift style identifiers in indexed mode', function () {
                testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a1', ATTRS), deleteAutoStyle('a3'),       changeAutoStyle('a1', ATTRS));
                testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a2', ATTRS), deleteAutoStyle('a3'),       changeAutoStyle('a2', ATTRS));
                testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a3', ATTRS), null,                        []);
                testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a4', ATTRS), deleteAutoStyle('a3', OTIS), changeAutoStyle('a3', ATTRS));
                testRunner.runBidiTest(deleteAutoStyle('a3'), changeAutoStyle('a5', ATTRS), deleteAutoStyle('a3', OTIS), changeAutoStyle('a4', ATTRS));
                // with/without type
                testRunner.runBidiTest(deleteAutoStyle('a3'),       changeAutoStyle('t1', 'a5', ATTRS), deleteAutoStyle('a3', OTIS),       changeAutoStyle('t1', 'a4', ATTRS));
                testRunner.runBidiTest(deleteAutoStyle('t1', 'a3'), changeAutoStyle('a5', ATTRS),       deleteAutoStyle('t1', 'a3', OTIS), changeAutoStyle('a4', ATTRS));
                testRunner.runBidiTest(deleteAutoStyle('t1', 'a3'), changeAutoStyle('t1', 'a5', ATTRS), deleteAutoStyle('t1', 'a3', OTIS), changeAutoStyle('t1', 'a4', ATTRS));
            });
            it('should fail for invalid style identifiers in indexed mode', function () {
                testRunner.expectBidiError(deleteAutoStyle('a2'), changeAutoStyle('b2', ATTRS));
                testRunner.expectBidiError(deleteAutoStyle('b2'), changeAutoStyle('a2', ATTRS));
                testRunner.expectBidiError(deleteAutoStyle('b2'), changeAutoStyle('b3', ATTRS));
            });
            it('should not shift style identifiers in freestyle mode', function () {
                testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a1', ATTRS));
                testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a2', ATTRS));
                testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a3', ATTRS), null, []);
                testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a4', ATTRS));
                testRunner.runBidiTest(deleteAutoStyle('t2', 'a3'), changeAutoStyle('t2', 'a5', ATTRS));
            });
        });
    */

    @Test
    public void deleteAutoStyleChangeAutoStyle01() throws JSONException {
        // Result: Should not process style sheets of different types.
        // testRunner.runBidiTest(
        //  opSeries1(deleteAutoStyle, 'a1', ['t1', 'a1']),
        //  opSeries2(changeAutoStyle, 't2', ['a1', 'a2'], ATTRS));
        TransformerTest.transform(
            Helper.createArrayFromJSON(Helper.createDeleteAutoStyleOp("a1"),
            Helper.createDeleteAutoStyleWithTypeOp("t1", "a1")),
            Helper.createArrayFromJSON(Helper.createChangeAutoStyleOp("t2", "a1", attrs, null),
            Helper.createChangeAutoStyleOp("t2", "a2", attrs, null)));
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle02() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  deleteAutoStyle('a3'),
        //  changeAutoStyle('a1', ATTRS),
        //  deleteAutoStyle('a3'),
        //  changeAutoStyle('a1', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createChangeAutoStyleOp("a1", attrs).toString(),
            Helper.createChangeAutoStyleOp("a1", attrs).toString(),
            Helper.createDeleteAutoStyleOp("a3").toString()
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle03() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  deleteAutoStyle('a3'),
        //  changeAutoStyle('a2', ATTRS),
        //  deleteAutoStyle('a3'),
        //  changeAutoStyle('a2', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createChangeAutoStyleOp("a2", attrs).toString(),
            Helper.createChangeAutoStyleOp("a2", attrs).toString(),
            Helper.createDeleteAutoStyleOp("a3").toString()
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle04() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  deleteAutoStyle('a3'),
        //  changeAutoStyle('a3', ATTRS),
        //  null,
        //  []);
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createChangeAutoStyleOp("a3", attrs).toString(),
            "[]",
            null
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle05() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  deleteAutoStyle('a3'),
        //  changeAutoStyle('a4', ATTRS),
        //  deleteAutoStyle('a3', OTIS),
        //  changeAutoStyle('a3', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createChangeAutoStyleOp("a4", attrs).toString(),
            Helper.createChangeAutoStyleOp("a3", attrs).toString(),
            Helper.createDeleteAutoStyleOp("a3", otis).toString()
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle06() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  deleteAutoStyle('a3'),
        //  changeAutoStyle('a5', ATTRS),
        //  deleteAutoStyle('a3', OTIS),
        //  changeAutoStyle('a4', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createChangeAutoStyleOp("a5", attrs).toString(),
            Helper.createChangeAutoStyleOp("a4", attrs).toString(),
            Helper.createDeleteAutoStyleOp("a3", otis).toString()
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle07() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  deleteAutoStyle('a3'),
        //  changeAutoStyle('t1', 'a5', ATTRS),
        //  deleteAutoStyle('a3', OTIS),
        //  changeAutoStyle('t1', 'a4', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleWithTypeOp(DEFAULT_AUTO_STYLE_TYPE, "a3").toString(),
            Helper.createChangeAutoStyleOp("t1", "a5", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a4", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a3", otis).toString()
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle08() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  deleteAutoStyle('t1', 'a3'),
        //  changeAutoStyle('a5', ATTRS),
        //  deleteAutoStyle('t1', 'a3', OTIS),
        //  changeAutoStyle('a4', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleWithTypeOp("t1", "a3").toString(),
            Helper.createChangeAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a5", attrs, null).toString(),
            Helper.createChangeAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a4", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp("t1", "a3", otis).toString()
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle09() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  deleteAutoStyle('t1', 'a3'),
        //  changeAutoStyle('t1', 'a5', ATTRS),
        //  deleteAutoStyle('t1', 'a3', OTIS),
        //  changeAutoStyle('t1', 'a4', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleWithTypeOp("t1", "a3").toString(),
            Helper.createChangeAutoStyleOp("t1", "a5", attrs, null).toString(),
            Helper.createChangeAutoStyleOp("t1", "a4", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp("t1", "a3", otis).toString()
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle10() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode.
        // testRunner.expectBidiError(
        //  deleteAutoStyle('a2'),
        //  changeAutoStyle('b2', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleOp("a2").toString(),
            Helper.createChangeAutoStyleOp("b2", attrs, null).toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle11() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode.
        // testRunner.expectBidiError(
        //  deleteAutoStyle('b2'),
        //  changeAutoStyle('a2', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleOp("b2").toString(),
            Helper.createChangeAutoStyleOp("a2", attrs, null).toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }

    @Test
    public void deleteAutoStyleChangeAutoStyle12() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode.
        // testRunner.expectBidiError(
        //  deleteAutoStyle('b2'),
        //  changeAutoStyle('b3', ATTRS));
        TransformerTest.transformBidi(
            Helper.createDeleteAutoStyleOp("b2").toString(),
            Helper.createChangeAutoStyleOp("b3", attrs, null).toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }
}
