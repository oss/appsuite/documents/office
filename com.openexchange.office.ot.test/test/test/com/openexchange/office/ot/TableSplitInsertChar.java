/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class TableSplitInsertChar {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [1, 1] }",                        // local operations
            "{ name: 'insertText', start: [2,10], text: 'o' }",             // external operations
            "{ name: 'insertText', start: [3,10], text: 'o' }",             // expected local
            "{ name: 'splitTable', start: [1, 1] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [1, 1] };
                localActions = [{ operations: [{ name: 'insertText', start: [2, 10], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 10]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",                        // local operations
            "{ name: 'insertText', start: [1,10], text: 'o' }",             // external operations
            "{ name: 'insertText', start: [1,10], text: 'o' }",             // expected local
            "{ name: 'splitTable', start: [2, 1] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 1] };
                localActions = [{ operations: [{ name: 'insertText', start: [1, 10], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 10]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",                        // local operations
            "{ name: 'insertText', start: [2,2,1,0,1], text: 'o' }",        // external operations
            "{ name: 'insertText', start: [3,1,1,0,1], text: 'o' }",        // expected local
            "{ name: 'splitTable', start: [2, 1] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 1] };
                localActions = [{ operations: [{ name: 'insertText', start: [2, 2, 1, 0, 1], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 0, 1]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 1] }",                        // local operations
            "{ name: 'insertText', start: [2,5,1,0,1], text: 'o' }",        // external operations
            "{ name: 'insertText', start: [3,4,1,0,1], text: 'o' }",        // expected local
            "{ name: 'splitTable', start: [2, 1] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 1] };
                localActions = [{ operations: [{ name: 'insertText', start: [2, 5, 1, 0, 1], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 4, 1, 0, 1]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 2] }",                        // local operations
            "{ name: 'insertText', start: [2,2,1,0,1], text: 'o' }",        // external operations
            "{ name: 'insertText', start: [3,0,1,0,1], text: 'o' }",        // expected local
            "{ name: 'splitTable', start: [2, 2] }");                       // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 2] };
                localActions = [{ operations: [{ name: 'insertText', start: [2, 2, 1, 0, 1], text: 'o' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 0, 1, 0, 1]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1] }",                  // local operations
            "{ name: 'insertText', start: [2,5], text: 'ooo' }",            // external operations
            "{ name: 'insertText', start: [2,5], text: 'ooo' }",            // expected local
            "{ name: 'splitTable', start: [2, 11, 1, 1] }");                // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1] };
                localActions = [{ operations: [{ name: 'insertText', start: [2, 5], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 11, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1] }",                  // local operations
            "{ name: 'insertText', start: [2,8], text: 'ooo' }",            // external operations
            "{ name: 'insertText', start: [2,8], text: 'ooo' }",            // expected local
            "{ name: 'splitTable', start: [2, 11, 1, 1] }");                // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1] };
                localActions = [{ operations: [{ name: 'insertText', start: [2, 8], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 11, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 8]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1] }",                  // local operations
            "{ name: 'insertText', start: [2,9], text: 'ooo' }",            // external operations
            "{ name: 'insertText', start: [2,9], text: 'ooo' }",            // expected local
            "{ name: 'splitTable', start: [2, 8, 1, 1] }");                 // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1] };
                localActions = [{ operations: [{ name: 'insertText', start: [2, 9], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 9]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1] }",                  // local operations
            "{ name: 'insertText', start: [1,3], text: 'ooo' }",            // external operations
            "{ name: 'insertText', start: [1,3], text: 'ooo' }",            // expected local
            "{ name: 'splitTable', start: [2, 8, 1, 1] }");                 // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1] };
                localActions = [{ operations: [{ name: 'insertText', start: [1, 3], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 8, 1, 1] }",                  // local operations
            "{ name: 'insertText', start: [3, 3], text: 'ooo' }",           // external operations
            "{ name: 'insertText', start: [3, 3], text: 'ooo' }",           // expected local
            "{ name: 'splitTable', start: [2, 8, 1, 1] }");                 // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 8, 1, 1] };
                localActions = [{ operations: [{ name: 'insertText', start: [3, 3], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 8, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 3]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }",               // local operations
            "{ name: 'insertText', start: [2,0,2,2,0], text: 'ooo' }",      // external operations
            "{ name: 'insertText', start: [2,0,2,3,0], text: 'ooo' }",      // expected local
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }");              // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 2] }; // table in table
                localActions = [{ operations: [{ name: 'insertText', start: [2, 0, 2, 2, 0], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 3, 0]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }",               // local operations
            "{ name: 'insertText', start: [2,0,2,0,0], text: 'ooo' }",      // external operations
            "{ name: 'insertText', start: [2,0,2,0,0], text: 'ooo' }",      // expected local
            "{ name: 'splitTable', start: [2, 0, 2, 1, 2] }");              // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 2] }; // table in table
                localActions = [{ operations: [{ name: 'insertText', start: [2, 0, 2, 0, 0], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 0, 0]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }",               // local operations
            "{ name: 'insertText', start: [2,0,2,1,4,1,0,0], text: 'ooo' }",// external operations
            "{ name: 'insertText', start: [2,0,2,2,3,1,0,0], text: 'ooo' }",// expected local
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }");              // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1] }; // table in table
                localActions = [{ operations: [{ name: 'insertText', start: [2, 0, 2, 1, 4, 1, 0, 0], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 2, 3, 1, 0, 0]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }",               // local operations
            "{ name: 'insertText', start: [2,0,2,1,0,1,0,0], text: 'ooo' }",// external operations
            "{ name: 'insertText', start: [2,0,2,1,0,1,0,0], text: 'ooo' }",// expected local
            "{ name: 'splitTable', start: [2, 0, 2, 1, 1] }");              // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 0, 2, 1, 1] }; // table in table
                localActions = [{ operations: [{ name: 'insertText', start: [2, 0, 2, 1, 0, 1, 0, 0], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 1, 0, 1, 0, 0]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'splitTable', start: [2, 0, 1, 1, 1] }",               // local operations
            "{ name: 'insertText', start: [2,0,2,1,2,1,0,0], text: 'ooo' }",// external operations
            "{ name: 'insertText', start: [2,0,2,1,2,1,0,0], text: 'ooo' }",// expected local
            "{ name: 'splitTable', start: [2, 0, 1, 1, 1] }");              // expected external
            /*
                oneOperation = { name: 'splitTable', start: [2, 0, 1, 1, 1] }; // table in table
                localActions = [{ operations: [{ name: 'insertText', start: [2, 0, 2, 1, 2, 1, 0, 0], text: 'ooo' }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 1, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 0, 2, 1, 2, 1, 0, 0]);
             */
    }
}
