/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ParaMergeParaMerge {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }");
            /*
                // the external merge is (far) behind the local merge
                oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // the position of the external operation is modified
                expect(oneOperation.paralength).to.equal(3); // the paragraph length does not change
                expect(oneOperation.nextparalength).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 5 }");
            /*
                // the external merge is directly behind the local merge
                oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // the position of the external operation is modified
                expect(oneOperation.paralength).to.equal(5); // the paragraph length is the sum of the paralength values
                expect(oneOperation.nextparalength).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "[]",
            "[]");
            /*
                // the external merge is exactly the local merge -> simply ignoring the external merge
                oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 9 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }");
            /*
                // the external merge is directly in front of the local merge
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // modified
                expect(localActions[0].operations[0].paralength).to.equal(9); // the sum of both paralength values
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }");
            /*
                // the external merge is far in front of the local merge
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 1], paralength: 6 }");
            /*
                // the external merge is inside a table cell behind the top level local merge
                oneOperation = { name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 1]); // modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 6 }");
            /*
                // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
                oneOperation = { name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 1]); // modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }");
            /*
                // the external merge is top level and in front of the local merge inside a table cell
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 1]); // modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 6 }");
            /*
                // the external merge is top level and behind the local merge inside a table cell -> both operations do not influence each other
                oneOperation = { name: 'mergeParagraph', start: [3], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3]); // modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 2], paralength: 3 }");
            /*
                // the external merge is (far) behind the local merge inside the same table cell
                oneOperation = { name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]); // the position of the external operation is modified
                expect(oneOperation.paralength).to.equal(3); // the paragraph length does not change
                expect(oneOperation.nextparalength).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 3, 2, 2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 1], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 1], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 1], paralength: 8 }");
            /*
                // the external merge is directly behind the local merge inside the same table cell
                oneOperation = { name: 'mergeParagraph', start: [1, 3, 2, 2], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 1], paralength: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 1]); // the position of the external operation is modified
                expect(oneOperation.paralength).to.equal(8); // the paragraph length is the sum of the paralength values
                expect(oneOperation.nextparalength).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3 }",
            "[]",
            "[]");
            /*
                // the external merge is exactly the local merge  inside the same table cell -> simply ignoring the external merge
                oneOperation = { name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 2]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3 }");
            /*
                // the external and the local merge are in different table cells -> both merges do not influence each other
                oneOperation = { name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 2]); // not modified
                expect(oneOperation.paralength).to.equal(3); // the paragraph length is the sum of the paralength values
                expect(oneOperation.nextparalength).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeParagraph operation has not the marker for ignoring
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "[]",
            "[]");
            /*
                // the external and the local merge are identical -> both merges need to get the remove flag
                oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // the local mergeParagraph operation has the marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeParagraph operation has the marker for ignoring
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [0], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [0], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 7, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 7, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2, 4, 0], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 4, 2], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 4, 0], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2, 4, 0], paralength: 3, opl: 1, osn: 1 }; // in text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 2]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([2, 4, 0]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }");
            /*
                // the external merge is (far) behind the local merge
                oneOperation = { name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // the position of the external operation is modified
                expect(oneOperation.rowcount).to.equal(3); // the paragraph length does not change
                expect(oneOperation.nextrowcount).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1], rowcount: 5 }");
            /*
                // the external merge is directly behind the local merge
                oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // the position of the external operation is modified
                expect(oneOperation.rowcount).to.equal(5); // the paragraph length is the sum of the rowcount values
                expect(oneOperation.nextrowcount).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "[]",
            "[]");
            /*
                // the external merge is exactly the local merge -> simply ignoring the external merge
                oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 9 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }");
            /*
                // the external merge is directly in front of the local merge
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // modified
                expect(localActions[0].operations[0].rowcount).to.equal(9); // the sum of both rowcount values
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }");
            /*
                // the external merge is far in front of the local merge
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2, 1], rowcount: 6 }");
            /*
                // the external merge is inside a table cell behind the top level local merge
                oneOperation = { name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 1]); // modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 6 }");
            /*
                // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
                oneOperation = { name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 1]); // modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2, 1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }");
            /*
                // the external merge is top level and in front of the local merge inside a table cell
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 1]); // modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [3], rowcount: 6 }");
            /*
                // the external merge is top level and behind the local merge inside a table cell -> both operations do not influence each other
                oneOperation = { name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3]); // modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 3 }");
            /*
                // the external merge is (far) behind the local merge inside the same table cell
                oneOperation = { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]); // the position of the external operation is modified
                expect(oneOperation.rowcount).to.equal(3); // the paragraph length does not change
                expect(oneOperation.nextrowcount).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 3, 2, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 1], rowcount: 8 }");
            /*l
                // the external merge is directly behind the local merge inside the same table cell
                oneOperation = { name: 'mergeTable', start: [1, 3, 2, 2], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 1], rowcount: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 1]); // the position of the external operation is modified
                expect(oneOperation.rowcount).to.equal(8); // the paragraph length is the sum of the rowcount values
                expect(oneOperation.nextrowcount).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3 }",
            "[]",
            "[]");
            /*
                // the external merge is exactly the local merge  inside the same table cell -> simply ignoring the external merge
                oneOperation = { name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 2, 2]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(3); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3 }");
            /*
                // the external and the local merge are in different table cells -> both merges do not influence each other
                oneOperation = { name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 2]); // not modified
                expect(oneOperation.rowcount).to.equal(3); // the paragraph length is the sum of the rowcount values
                expect(oneOperation.nextrowcount).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "[]",
            "[]");
            /*
                // the external and the local merge are identical -> both merges need to get the remove flag
                oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1); // the local mergeTable operation has the marker for ignoring
                expect(oneOperation._REMOVED_OPERATION_).to.equal(1); // the external mergeTable operation has the marker for ignoring
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [0], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 4, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [0], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [0], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2, 1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 5, 2, 1], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 1], rowcount: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 2, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 5, 1, 1, 2, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame in a table
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 1, 1, 2, 2]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame in a table
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 1, 2, 2]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [3], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2, 4, 0], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 4, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 4, 0], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [2, 4, 0], rowcount: 3, opl: 1, osn: 1 }; // in text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 2]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([2, 4, 0]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 2 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }");
            /*
                // the external merge is (far) behind the local merge
                oneOperation = { name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // the position of the external operation is modified
                expect(oneOperation.rowcount).to.equal(3); // the paragraph length does not change
                expect(oneOperation.nextrowcount).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }");
            /*
                // the external merge is far in front of the local merge
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 6 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2, 1], rowcount: 6 }");
            /*
                // the external merge is inside a table cell behind the top level local merge
                oneOperation = { name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 1]); // modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 6 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 6 }");
            /*
                // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
                oneOperation = { name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 1]); // modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 1], paralength: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }");
            /*
                // the external merge is top level and in front of the local merge inside a table cell
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 1]); // modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 6 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3 }",
            "{ name: 'mergeTable', start: [3], rowcount: 6 }");
            /*
                // the external merge is top level and behind the local merge inside a table cell -> both operations do not influence each other
                oneOperation = { name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3]); // modified
                expect(oneOperation.rowcount).to.equal(6); // not modified
                expect(oneOperation.nextrowcount).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 2], rowcount: 3 }");
            /*
                // the external merge is (far) behind the local merge inside the same table cell
                oneOperation = { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]); // the position of the external operation is modified
                expect(oneOperation.rowcount).to.equal(3); // the paragraph length does not change
                expect(oneOperation.nextrowcount).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5 }",
            "{ name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3 }");
            /*
                // the external and the local merge are in different table cells -> both merges do not influence each other
                oneOperation = { name: 'mergeTable', start: [2, 1, 2, 2], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 1, 1, 3], paralength: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3]); // not modified
                expect(localActions[0].operations[0].paralength).to.equal(5); // not modified
                expect(localActions[0].operations[0].nextparalength).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 2]); // not modified
                expect(oneOperation.rowcount).to.equal(3); // the paragraph length is the sum of the rowcount values
                expect(oneOperation.nextrowcount).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [0], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 4, 3], paralength: 5 }",
            "{ name: 'mergeTable', start: [0], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [0], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 1], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 5, 2, 1], paralength: 5 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 1], paralength: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 2, 1]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 1, 1, 2, 2], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [1, 5, 1, 1, 2, 2], paralength: 5 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 1, 1, 2, 2], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame in a table
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 1, 1, 2, 2]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 1, 1, 2, 2], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 2, 1, 1, 2, 2], paralength: 5 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [2], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 1, 1, 2, 2], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame in a table
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 1, 2, 2]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeTable', start: [3], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test53() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [2, 4, 0], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5 }",
            "{ name: 'mergeParagraph', start: [2, 4, 2], paralength: 5 }",
            "{ name: 'mergeTable', start: [2, 4, 0], rowcount: 3 }");
            /*
                oneOperation = { name: 'mergeTable', start: [2, 4, 0], rowcount: 3, opl: 1, osn: 1 }; // in text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 4, 3], paralength: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 2]);
                expect(localActions[0].operations[0].paralength).to.equal(5);
                expect(oneOperation.start).to.deep.equal([2, 4, 0]);
                expect(oneOperation.rowcount).to.equal(3);
             */
    }

    @Test
    public void test54() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1], rowcount: 2 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }");
            /*
                // the external merge is (far) behind the local merge
                oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2]); // the position of the external operation is modified
                expect(oneOperation.paralength).to.equal(3); // the paragraph length does not change
                expect(oneOperation.nextparalength).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test55() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }");
            /*
                // the external merge is far in front of the local merge
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]); // modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test56() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 6 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 1], paralength: 6 }");
            /*
                // the external merge is inside a table cell behind the top level local merge
                oneOperation = { name: 'mergeParagraph', start: [3, 2, 2, 1], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]); // modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 1]); // modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test57() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 6 }",
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeTable', start: [3], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 6 }");
            /*
                // the external merge is inside a table cell in front of the top level local merge -> both operations do not influence each other
                oneOperation = { name: 'mergeParagraph', start: [1, 2, 2, 1], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 1]); // modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test58() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2, 1], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }");
            /*
                // the external merge is top level and in front of the local merge inside a table cell
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 2, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 1]); // modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1]); // not modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test59() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 6 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 6 }");
            /*
                // the external merge is top level and behind the local merge inside a table cell -> both operations do not influence each other
                oneOperation = { name: 'mergeParagraph', start: [3], paralength: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 3, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(3); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([3]); // modified
                expect(oneOperation.paralength).to.equal(6); // not modified
                expect(oneOperation.nextparalength).to.equal(3); // not modified
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test60() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 3 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2 }",
            "{ name: 'mergeParagraph', start: [1, 2, 2, 2], paralength: 3 }");
            /*
                // the external merge is (far) behind the local merge inside the same table cell
                oneOperation = { name: 'mergeParagraph', start: [1, 2, 2, 3], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 1], rowcount: 2, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 1]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(2); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(2); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([1, 2, 2, 2]); // the position of the external operation is modified
                expect(oneOperation.paralength).to.equal(3); // the paragraph length does not change
                expect(oneOperation.nextparalength).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test61() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3 }",
            "{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5 }",
            "{ name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3 }");
            /*
                // the external and the local merge are in different table cells -> both merges do not influence each other
                oneOperation = { name: 'mergeParagraph', start: [2, 1, 2, 2], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 1, 1, 3], rowcount: 5, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 1, 1, 3]); // not modified
                expect(localActions[0].operations[0].rowcount).to.equal(5); // not modified
                expect(localActions[0].operations[0].nextrowcount).to.equal(4); // not modified
                expect(localActions[0].operations.length).to.equal(1); // there is no additional local operation generated
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 2]); // not modified
                expect(oneOperation.paralength).to.equal(3); // the paragraph length is the sum of the rowcount values
                expect(oneOperation.nextparalength).to.equal(3); // the next paragraph length does not change
                expect(oneOperation._REMOVED_OPERATION_).to.equal(undefined); // the external mergeTable operation has not the marker for ignoring
             */
    }

    @Test
    public void test62() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [0], paralength: 3 }",
            "{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 4, 3], rowcount: 5 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [0], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([0]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test63() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 5, 2], rowcount: 5 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 2]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test64() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [1, 5, 1, 1, 2, 2], rowcount: 5 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [1], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame in a table
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 5, 1, 1, 2, 2]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([1]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test65() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 1, 1, 2, 2], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame in a table
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1, 1, 2, 2]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([2]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test66() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }",
            "{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5 }",
            "{ name: 'mergeParagraph', start: [3], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [3], paralength: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 3]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([3]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }

    @Test
    public void test67() {
        TransformerTest.transform(
            "{ name: 'mergeParagraph', start: [2, 4, 0], paralength: 3 }",
            "{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5 }",
            "{ name: 'mergeTable', start: [2, 4, 2], rowcount: 5 }",
            "{ name: 'mergeParagraph', start: [2, 4, 0], paralength: 3 }");
            /*
                oneOperation = { name: 'mergeParagraph', start: [2, 4, 0], paralength: 3, opl: 1, osn: 1 }; // in text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 4, 3], rowcount: 5, opl: 1, osn: 1 }] }]; // in a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 4, 2]);
                expect(localActions[0].operations[0].rowcount).to.equal(5);
                expect(oneOperation.start).to.deep.equal([2, 4, 0]);
                expect(oneOperation.paralength).to.equal(3);
             */
    }
}
