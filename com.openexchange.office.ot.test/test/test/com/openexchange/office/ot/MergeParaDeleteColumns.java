/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class MergeParaDeleteColumns {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [5], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [5], paralength: 6 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [5], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 0, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 0, 2], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 0, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 0, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 1, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 1, 2], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 1, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 3, 2], paralength: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 3, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 3, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 4, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 4, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }",
            "{ name: 'mergeParagraph', start: [3, 8, 0, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 8, 0, 2], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 8, 0, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 8, 0, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }",
            "{ name: 'mergeParagraph', start: [3, 8, 1, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 8, 1, 2], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 8, 1, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 8, 1, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }",
            "{ name: 'mergeParagraph', start: [3, 8, 2, 2], paralength: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 8, 2, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 8, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }",
            "{ name: 'mergeParagraph', start: [3, 8, 3, 2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 8, 2, 2], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 8, 3, 2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 8, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3, 1, 2, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1 ,2, 4], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 4], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 1, 1, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6 }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3, 1, 1, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [3, 1, 2, 1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [0], paralength: 6 }",
            "{ name: 'deleteColumns', start: [0, 9, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [0], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 9, 2]);
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 1, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 1, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 2, 3], paralength: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 2, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 2, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 3, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 4, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 2 ,3], paralength: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 4, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 2, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6 }",
            "{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 3, 2, 3, 3, 3], paralength: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 3, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 3]);
             */
    }


    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [5], rowcount: 6 }",
            "{ name: 'mergeTable', start: [5], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [5], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([4]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 0, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 0, 2], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 0, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 0, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 1, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 1, 2], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 1, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 1, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 3, 2], rowcount: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
             3   localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 3, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 3, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 4, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2 ,2], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 4, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }",
            "{ name: 'mergeTable', start: [3, 1, 3, 2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 2], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 2, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 3, 2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [3, 1, 2, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 2]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3, 1, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 4]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 2, 3]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [3, 1, 1, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [3, 1, 1, 3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [3, 1, 1, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3, 1, 2, 1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3, 1, 2, 1]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([3, 1, 1, 3]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [0], rowcount: 6 }",
            "{ name: 'mergeTable', start: [0], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [0, 9, 2, 3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [0], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([0]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([0, 9, 2, 3]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 3]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'mergeTable', start: [2], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2, 3]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 0, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 0, 3], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 0, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 0, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 1, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 1, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 2, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6 }",
            "[]",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 3, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(1);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test43() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 4, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 2, 3], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 2], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 4, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 2, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 2]);
             */
    }

    @Test
    public void test44() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 3, 3], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6 }",
            "{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [1, 3, 3], startGrid: 2, endGrid: 3 }");
            /*
                oneOperation = { name: 'deleteColumns', start: [1, 3, 3], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }; // in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 3, 2, 3, 3, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 3, 2, 3, 3, 3]);
                expect(localActions[0].operations[0]._REMOVED_OPERATION_).to.equal(undefined);
                expect(localActions[0].operations.length).to.equal(1);
                expect(oneOperation.start).to.deep.equal([1, 3, 3]);
             */
    }


    @Test
    public void test45() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [4], rowcount: 6 }",
            "[{ name: 'deleteColumns', start: [5], startGrid: 2, endGrid: 3 }, { name: 'mergeTable', start: [4], rowcount: 6 }]",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }");
            /*
                // the local merge is in the same table: local mergeTable [4] and external deleteColumns [4]
                // -> an additional internal operation: 'deleteColumns [5]' before 'mergeTable [4]' is required
                oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('deleteColumns');
                expect(localActions[0].operations[0].start).to.deep.equal([5]);
                expect(localActions[0].operations[1].name).to.equal('mergeTable');
                expect(localActions[0].operations[1].start).to.deep.equal([4]);
                expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0]).to.deep.equal(oneOperation);
             */
    }

    @Test
    public void test46() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6 }",
            "[{ name: 'deleteColumns', start: [1, 2, 2, 5], startGrid: 2, endGrid: 3 }, { name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6 }]",
            "{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3 }");
            /*
                // the local merge is in the same table: local mergeTable [1, 2, 2, 4] and external deleteColumns [1, 2, 2, 4]
                // -> an additional internal operation: 'deleteColumns [1, 2, 2, 5]' before 'mergeTable [1, 2, 2, 4]' is required
                oneOperation = { name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('deleteColumns');
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 5]);
                expect(localActions[0].operations[1].name).to.equal('mergeTable');
                expect(localActions[0].operations[1].start).to.deep.equal([1, 2, 2, 4]);
                expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0]).to.deep.equal(oneOperation);
             */
    }

    @Test
    public void test47() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [3], rowcount: 6 }",
            "[{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }, { name: 'mergeTable', start: [3], rowcount: 6 }]",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }");
            /*
                // the local merge is in the previous table: local mergeTable [3] and external deleteColumns [4]
                // 1. the external deleteColumns operation must be modified, because of the locally already executed mergeTable [3] operation
                // 2. an additional internal operation: 'deleteColumns [3]' before 'mergeTable [3]' is required
                oneOperation = { name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('deleteColumns');
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations[1].name).to.equal('mergeTable');
                expect(localActions[0].operations[1].start).to.deep.equal([3]);
                expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([3]);
             */
    }

    @Test
    public void test48() {
        TransformerTest.transform(
            "{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3 }",
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6 }",
            "[{ name: 'deleteColumns', start: [1, 2, 2, 3], startGrid: 2, endGrid: 3 }, { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6 }]",
            "{ name: 'deleteColumns', start: [1, 2, 2, 3], startGrid: 2, endGrid: 3 }");
            /*
                // the local merge is in the previous table: local mergeTable [1, 2, 2, 3] and external deleteColumns [1, 2, 2, 4]
                // 1. the external deleteColumns operation must be modified, because of the locally already executed mergeTable [1, 2, 2, 3] operation
                // 2. an additional internal operation: 'deleteColumns [1, 2, 2, 3]' before 'mergeTable [1, 2, 2, 3]' is required
                oneOperation = { name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations.length).to.equal(2);
                expect(localActions[0].operations[0].name).to.equal('deleteColumns');
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3]);
                expect(localActions[0].operations[1].name).to.equal('mergeTable');
                expect(localActions[0].operations[1].start).to.deep.equal([1, 2, 2, 3]);
                expect(localActions[0].operations[1]._REMOVED_OPERATION_).to.equal(undefined);
                expect(transformedOps.length).to.equal(1);
                expect(transformedOps[0].start).to.deep.equal([1, 2, 2, 3]);
             */
    }

    @Test
    public void test49() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [4], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "[{ name: 'deleteColumns', start: [5], startGrid: 2, endGrid: 3 }, { name: 'mergeTable', start: [4], rowcount: 6 }]");
            /*
                // the merge is in the same table: local deleteColumns [3] and external mergeTable [3]
                // -> an additional external operation: 'deleteColumns [4]' before 'mergeTable [3]' is required
                oneOperation = { name: 'mergeTable', start: [4], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0].name).to.equal('deleteColumns');
                expect(transformedOps[0].start).to.deep.equal([5]);
                expect(transformedOps[1].name).to.equal('mergeTable');
                expect(transformedOps[1].start).to.deep.equal([4]);
                expect(transformedOps[1]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test50() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3 }",
            "[{ name: 'deleteColumns', start: [1, 2, 2, 5], startGrid: 2, endGrid: 3 }, { name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6 }]");
            /*
                // the merge is in the same table: local deleteColumns [1, 2, 2, 3] and external mergeTable [1, 2, 2, 3]
                // -> an additional external operation: 'deleteColumns [1, 2, 2, 4]' before 'mergeTable [1, 2, 2, 3]' is required
                oneOperation = { name: 'mergeTable', start: [1, 2, 2, 4], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 4]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0].name).to.equal('deleteColumns');
                expect(transformedOps[0].start).to.deep.equal([1, 2, 2, 5]);
                expect(transformedOps[1].name).to.equal('mergeTable');
                expect(transformedOps[1].start).to.deep.equal([1, 2, 2, 4]);
                expect(transformedOps[1]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test51() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [3], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }",
            "[{ name: 'deleteColumns', start: [3], startGrid: 2, endGrid: 3 }, { name: 'mergeTable', start: [3], rowcount: 6 }]");
            /*
                // the external merge is in the previous table: local deleteColumns [4] and external mergeTable [3]
                // 1. the internal deleteColumns operation must be modified, because of the external executed mergeTable [3] operation
                // 2. an additional external operation: 'deleteColumns [3]' before 'mergeTable [3]' is required
                oneOperation = { name: 'mergeTable', start: [3], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0].name).to.equal('deleteColumns');
                expect(transformedOps[0].start).to.deep.equal([3]);
                expect(transformedOps[1].name).to.equal('mergeTable');
                expect(transformedOps[1].start).to.deep.equal([3]);
                expect(transformedOps[1]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }

    @Test
    public void test52() {
        TransformerTest.transform(
            "{ name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6 }",
            "{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3 }",
            "{ name: 'deleteColumns', start: [1, 2, 2, 3], startGrid: 2, endGrid: 3 }",
            "[{ name: 'deleteColumns', start: [1, 2, 2, 3], startGrid: 2, endGrid: 3 }, { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6 }]");
            /*
                // the external merge is in the previous table: local deleteColumns [1, 2, 2, 4] and external mergeTable [1, 2, 2, 3]
                // 1. the internal deleteColumns operation must be modified, because of the external executed mergeTable [1, 2, 2, 3] operation
                // 2. an additional external operation: 'deleteColumns [1, 2, 2, 3]' before 'mergeTable [1, 2, 2, 3]' is required
                oneOperation = { name: 'mergeTable', start: [1, 2, 2, 3], rowcount: 6, opl: 1, osn: 1 };
                localActions = [{ operations: [{ name: 'deleteColumns', start: [1, 2, 2, 4], startGrid: 2, endGrid: 3, opl: 1, osn: 1 }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 2, 2, 3]);
                expect(localActions[0].operations.length).to.equal(1);
                expect(transformedOps.length).to.equal(2);
                expect(transformedOps[0].name).to.equal('deleteColumns');
                expect(transformedOps[0].start).to.deep.equal([1, 2, 2, 3]);
                expect(transformedOps[1].name).to.equal('mergeTable');
                expect(transformedOps[1].start).to.deep.equal([1, 2, 2, 3]);
                expect(transformedOps[1]._REMOVED_OPERATION_).to.equal(undefined);
             */
    }
}
