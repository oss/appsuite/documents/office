/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ParaMergeInsertChar {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 5], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 7 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 10 }",
            "{ name: 'insertText', start: [1, 5], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 5], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 7 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].paralength).to.equal(10);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2], paralength: 1 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 1 }",
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].paralength).to.equal(1);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 6], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'insertText', start: [1, 9], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 9]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 6, 3, 2], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'insertText', start: [1, 9, 3, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6, 3, 2], text: 'ooo' }; // inside text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 9, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1, 7, 2], paralength: 3 }",
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 4, 2], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 7, 2]);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 5], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1, 4, 2], paralength: 3 }",
            "{ name: 'insertText', start: [1, 5], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 5], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1, 4, 2], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 2]);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 0, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [1], paralength: 3 }",
            "{ name: 'insertText', start: [2, 0, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 0, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [1], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 0, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 3 }",
            "{ name: 'insertText', start: [1, 0, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 0, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 0, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test09() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 1], paralength: 3 }",
            "{ name: 'insertText', start: [2, 2, 2, 1, 5], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 1], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 1]);
             */
    }

    @Test
    public void test10() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 3, 4], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 2, 3, 4], paralength: 3 }",
            "{ name: 'insertText', start: [2, 2, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 2, 3, 4], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 3, 4]);
             */
    }

    @Test
    public void test11() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2, 2, 1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 2, 1], paralength: 3 }",
            "{ name: 'insertText', start: [2, 5, 2, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3], text: 'ooo' }; // insert text in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 2, 1], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 2, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 1]);
             */
    }

    @Test
    public void test12() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2, 5, 0], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 5, 0], paralength: 3 }",
            "{ name: 'insertText', start: [2, 5, 1, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3], text: 'ooo' }; // insert text in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 5, 0], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 0]);
             */
    }

    @Test
    public void test13() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2, 5, 1], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 5, 1], paralength: 3 }",
            "{ name: 'insertText', start: [2, 5, 1, 6], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3], text: 'ooo' }; // insert text in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 5, 1], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 1]);
                expect(localActions[0].operations[0].paralength).to.equal(3);
             */
    }

    @Test
    public void test14() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 2], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2, 5, 2], paralength: 3 }",
            "{ name: 'mergeParagraph', start: [2, 5, 2], paralength: 6 }",
            "{ name: 'insertText', start: [2, 5, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 2], text: 'ooo' }; // insert text in a text frame
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2, 5, 2], opl: 1, osn: 1, paralength: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 2]);
                expect(localActions[0].operations[0].paralength).to.equal(6);
             */
    }

    @Test
    public void test15() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2], paralength: 8 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 11 }",
            "{ name: 'insertText', start: [2, 5], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 8 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].paralength).to.equal(11);
             */
    }

    @Test
    public void test16() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 1, 1], text: 'ooo' }",
            "{ name: 'mergeParagraph', start: [2], paralength: 8 }",
            "{ name: 'mergeParagraph', start: [2], paralength: 8 }",
            "{ name: 'insertText', start: [2, 5, 1, 1], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 1, 1], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeParagraph', start: [2], opl: 1, osn: 1, paralength: 8 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].paralength).to.equal(8); // the drawing at position [2, 5] was already there
             */
    }

    @Test
    public void test17() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2, 0, 0, 1], text: 'ooo' }",
            "{ name: 'mergeTable', start: [1], rowcount: 7 }",
            "{ name: 'mergeTable', start: [1], rowcount: 7 }",
            "{ name: 'insertText', start: [1, 2, 0, 0, 1], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2, 0, 0, 1], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 7 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2, 0, 0, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
                expect(localActions[0].operations[0].rowcount).to.equal(7);
             */
    }

    @Test
    public void test18() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2], rowcount: 1 }",
            "{ name: 'mergeTable', start: [2], rowcount: 1 }",
            "{ name: 'insertText', start: [1, 6], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 6], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 1 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 6]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
                expect(localActions[0].operations[0].rowcount).to.equal(1);
             */
    }

    @Test
    public void test19() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 6, 0, 0, 1], text: 'ooo' }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'insertText', start: [1, 9, 0, 0, 1], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6, 0, 0, 1], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 9, 0, 0, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test20() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 6, 3, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 6, 3, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 6, 3, 2], text: 'ooo' }; // inside text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 6, 3, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test21() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 6, 3, 2, 0, 0, 1], text: 'ooo' }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'insertText', start: [1, 9, 3, 2, 0, 0, 1], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6, 3, 2, 0, 0, 1], text: 'ooo' }; // inside text frame in table
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 9, 3, 2, 0, 0, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test22() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [0, 6, 3, 2, 0, 0, 1], text: 'ooo' }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'insertText', start: [0, 6, 3, 2, 0, 0, 1], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [0, 6, 3, 2, 0, 0, 1], text: 'ooo' }; // inside text frame in table
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([0, 6, 3, 2, 0, 0, 1]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test23() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [1, 4, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1, 7, 2], rowcount: 3 }",
            "{ name: 'insertText', start: [1, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 4, 2], opl: 1, osn: 1, rowcount: 3 }] }]; // inside a text frame
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 7, 2]);
             */
    }

    @Test
    public void test24() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 5], text: 'ooo' }",
            "{ name: 'mergeTable', start: [1, 4, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1, 4, 2], rowcount: 3 }",
            "{ name: 'insertText', start: [1, 5], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 5], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1, 4, 2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 5]);
                expect(localActions[0].operations[0].start).to.deep.equal([1, 4, 2]);
             */
    }

    @Test
    public void test25() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 0, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 0, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 0, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeTable', start: [1], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 0, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([1]);
             */
    }

    @Test
    public void test26() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [1, 0, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [1], rowcount: 3 }",
            "{ name: 'insertText', start: [1, 0, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [1, 0, 2, 2, 2], text: 'ooo' };
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([1, 0, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test27() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [3, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 5, 2, 2, 2, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [3, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }; // table in table
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 2, 2, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test28() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }; // table in table
                localActions = [{ operations: [{ name: 'mergeTable', start: [2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 2, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2]);
             */
    }

    @Test
    public void test29() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 2, 2, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2, 2], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 2, 2, 2, 2, 2], text: 'ooo' }; // table in table
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 2, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 2]);
             */
    }

    @Test
    public void test30() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 4, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 2, 2, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2, 2], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 2, 2, 3, 2, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 4, 2, 2, 2, 2], text: 'ooo' }; // table in table
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 3, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 2]);
             */
    }

    @Test
    public void test31() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 2, 3, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 2, 2, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2, 2], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 2, 2, 2, 5, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 2, 3, 2, 2, 2, 2], text: 'ooo' }; // table in table
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 2, 2, 5, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 2]);
             */
    }

    @Test
    public void test32() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 2, 1, 3, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 2, 2, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2 ,2], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 2, 1, 3, 2, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 2, 1, 3, 2, 2, 2, 2], text: 'ooo' }; // table in table
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 2, 1, 3, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 2]);
             */
    }

    @Test
    public void test33() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 1, 2, 3, 2, 2, 2, 2], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 2, 2, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 2, 2, 2], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 1, 2, 3, 2, 2, 2, 2], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 1, 2, 3, 2, 2, 2, 2], text: 'ooo' }; // table in table
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 2, 2, 2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 1, 2, 3, 2, 2, 2, 2]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 2, 2, 2]);
             */
    }

    @Test
    public void test34() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 5, 1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 5, 1], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 5, 1, 6, 0, 0, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 1], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 6, 0, 0, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 1]);
             */
    }

    @Test
    public void test35() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 5, 0], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 5, 0], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 5, 1, 3, 0, 0, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 0], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 3, 0, 0, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 0]);
             */
    }

    @Test
    public void test36() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 5, 2], rowcount: 4 }",
            "{ name: 'mergeTable', start: [2, 5, 2], rowcount: 4 }",
            "{ name: 'insertText', start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 2], opl: 1, osn: 1, rowcount: 4 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 2, 3, 0, 0, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 2]);
             */
    }

    @Test
    public void test37() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 5, 3], rowcount: 4 }",
            "{ name: 'mergeTable', start: [2, 5, 3], rowcount: 4 }",
            "{ name: 'insertText', start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 3], opl: 1, osn: 1, rowcount: 4 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 2, 3, 0, 0, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 3]);
             */
    }

    @Test
    public void test38() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 4, 2, 3, 0, 0, 3], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 5, 3], rowcount: 4 }",
            "{ name: 'mergeTable', start: [2, 5, 3], rowcount: 4 }",
            "{ name: 'insertText', start: [2, 4, 2, 3, 0, 0, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 4, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 3], opl: 1, osn: 1, rowcount: 4 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 4, 2, 3, 0, 0, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 3]);
             */
    }

    @Test
    public void test39() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 6, 2, 3, 0, 0, 3], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 5, 3], rowcount: 4 }",
            "{ name: 'mergeTable', start: [2, 5, 3], rowcount: 4 }",
            "{ name: 'insertText', start: [2, 6, 2, 3, 0, 0, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 6, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a table in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 3], opl: 1, osn: 1, rowcount: 4 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 6, 2, 3, 0, 0, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 3]);
             */
    }

    @Test
    public void test40() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 5, 0], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 5, 0], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 5, 1, 3, 0, 0, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 0], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 3, 0, 0, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 0]);
             */
    }

    @Test
    public void test41() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 5, 1], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 5, 1], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 5, 1, 6, 0, 0, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 3, 0, 0, 3], text: 'ooo' }; // insert text in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 1], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 1, 6, 0, 0, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 1]);
                expect(localActions[0].operations[0].rowcount).to.equal(3);
             */
    }

    @Test
    public void test42() {
        TransformerTest.transform(
            "{ name: 'insertText', start: [2, 5, 2, 2, 0, 0, 3], text: 'ooo' }",
            "{ name: 'mergeTable', start: [2, 5, 2], rowcount: 3 }",
            "{ name: 'mergeTable', start: [2, 5, 2], rowcount: 3 }",
            "{ name: 'insertText', start: [2, 5, 2, 2, 0, 0, 3], text: 'ooo' }");
            /*
                oneOperation = { name: 'insertText', opl: 1, osn: 1, start: [2, 5, 2, 2, 0, 0, 3], text: 'ooo' }; // insert text in a text frame
                localActions = [{ operations: [{ name: 'mergeTable', start: [2, 5, 2], opl: 1, osn: 1, rowcount: 3 }] }];
                otManager.transformOperation(oneOperation, localActions);
                expect(oneOperation.start).to.deep.equal([2, 5, 2, 2, 0, 0, 3]);
                expect(localActions[0].operations[0].start).to.deep.equal([2, 5, 2]);
                expect(localActions[0].operations[0].rowcount).to.equal(3);
             */
    }
}
