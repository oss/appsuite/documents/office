/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.Helper;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class DeleteAutoStyle {

    /**
     * The client tests are using T1 as a default
     * name for a type, but the backend is using cell.
     * In order to let client tests run in the backend,
     * auto style type tests without a style have to
     * use this default.
     * e.g. see insertAutoStyleInsertAutoStyle06()
     */
    private final static String DEFAULT_AUTO_STYLE_TYPE = "t1";
    private final String        otis                    = Helper.createJSONObject("{ otIndexShift: true }").toString();
    private final JSONObject    attrs                   = Helper.createJSONObject("{ f1: { a1: 10 } }");

    /*
     * describe('"insertAutoStyle" and "deleteAutoStyle"', function () {
            var OTIS = { otIndexShift: true };
            it('should not process style sheets of different types', function () {
                testRunner.runBidiTest(opSeries1(insertAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]), opSeries2(deleteAutoStyle, 't2', ['a1', 'a2']));
            });
            it('should shift style identifiers in indexed mode', function () {
                testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a1'), insertAutoStyle('a2', ATTRS),       deleteAutoStyle('a1', OTIS));
                testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a2'), insertAutoStyle('a2', ATTRS),       deleteAutoStyle('a2', OTIS));
                testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a3'), insertAutoStyle('a3', ATTRS, OTIS), deleteAutoStyle('a4'));
                testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a4'), insertAutoStyle('a3', ATTRS, OTIS), deleteAutoStyle('a5'));
                testRunner.runBidiTest(insertAutoStyle('a3', ATTRS), deleteAutoStyle('a5'), insertAutoStyle('a3', ATTRS, OTIS), deleteAutoStyle('a6'));
                // with/without type
                testRunner.runBidiTest(insertAutoStyle('a3', ATTRS),       deleteAutoStyle('t1', 'a1'), insertAutoStyle('a2', ATTRS),       deleteAutoStyle('t1', 'a1', OTIS));
                testRunner.runBidiTest(insertAutoStyle('t1', 'a3', ATTRS), deleteAutoStyle('a1'),       insertAutoStyle('t1', 'a2', ATTRS), deleteAutoStyle('a1', OTIS));
                testRunner.runBidiTest(insertAutoStyle('t1', 'a3', ATTRS), deleteAutoStyle('t1', 'a1'), insertAutoStyle('t1', 'a2', ATTRS), deleteAutoStyle('t1', 'a1', OTIS));
            });
            it('should fail for invalid style identifiers in indexed mode', function () {
                testRunner.expectBidiError(insertAutoStyle('a2', ATTRS), deleteAutoStyle('b2'));
                testRunner.expectBidiError(insertAutoStyle('b2', ATTRS), deleteAutoStyle('a2'));
                testRunner.expectBidiError(insertAutoStyle('b2', ATTRS), deleteAutoStyle('b2'));
            });
            it('should not shift style identifiers in freestyle mode', function () {
                testRunner.runBidiTest(insertAutoStyle('t2', 'a3', ATTRS), deleteAutoStyle('t2', 'a2'));
                testRunner.runBidiTest(insertAutoStyle('t2', 'a3', ATTRS), deleteAutoStyle('t2', 'a4'));
            });
            it('should fail to insert and delete same auto-style in freestyle mode', function () {
                testRunner.expectBidiError(insertAutoStyle('t2', 'a3', ATTRS), deleteAutoStyle('t2', 'a3'));
            });
        });
     */

    @Test
    public void insertAutoStyleDeleteAutoStyle01() throws JSONException {
        // Result: Should not process style sheets of different types.
        // testRunner.runBidiTest(
        //  opSeries1(insertAutoStyle, ['a1', ATTRS], ['t1', 'a1', ATTRS]),
        //  opSeries2(deleteAutoStyle, 't2', ['a1', 'a2']));
        TransformerTest.transformBidi(
            Helper.createArrayFromJSON(Helper.createInsertAutoStyleOp("a1", attrs, null),
            Helper.createInsertAutoStyleOp("t1", "a1", attrs, null)),
            Helper.createArrayFromJSON(Helper.createDeleteAutoStyleWithTypeOp("t2", "a1"),
            Helper.createDeleteAutoStyleWithTypeOp("t2", "a2")));
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle02() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a3', ATTRS),
        //  deleteAutoStyle('a1'),
        //  insertAutoStyle('a2', ATTRS),
        //  deleteAutoStyle('a1', OTIS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a3", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp("a1").toString(),
            Helper.createDeleteAutoStyleOp("a1", otis).toString(),
            Helper.createInsertAutoStyleOp("a2", attrs, null).toString()
        );
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle03() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a3', ATTRS),
        //  deleteAutoStyle('a3'),
        //  insertAutoStyle('a3', ATTRS, OTIS),
        //  deleteAutoStyle('a4'));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a3", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp("a3").toString(),
            Helper.createDeleteAutoStyleOp("a4").toString(),
            Helper.createInsertAutoStyleOp("a3", attrs, otis).toString()
        );
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle04() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a3', ATTRS),
        //  deleteAutoStyle('a4'),
        //  insertAutoStyle('a3', ATTRS, OTIS),
        //  deleteAutoStyle('a5'));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a3", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp("a4").toString(),
            Helper.createDeleteAutoStyleOp("a5").toString(),
            Helper.createInsertAutoStyleOp("a3", attrs, otis).toString()
        );
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle05() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a3', ATTRS),
        //  deleteAutoStyle('a5'),
        //  insertAutoStyle('a3', ATTRS, OTIS),
        //  deleteAutoStyle('a6'));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a3", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp("a5").toString(),
            Helper.createDeleteAutoStyleOp("a6").toString(),
            Helper.createInsertAutoStyleOp("a3", attrs, otis).toString()
        );
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle06() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('a3', ATTRS),
        //  deleteAutoStyle('t1', 'a1'),
        //  insertAutoStyle('a2', ATTRS),
        //  deleteAutoStyle('t1', 'a1', OTIS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a3", attrs, null).toString(),
            Helper.createDeleteAutoStyleWithTypeOp("t1", "a1").toString(),
            Helper.createDeleteAutoStyleOp("t1", "a1", otis).toString(),
            Helper.createInsertAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a2", attrs, null).toString()
        );
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle07() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('t1', 'a3', ATTRS),
        //  deleteAutoStyle('a1'),
        //  insertAutoStyle('t1', 'a2', ATTRS),
        //  deleteAutoStyle('a1', OTIS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("t1", "a3", attrs, null).toString(),
            Helper.createDeleteAutoStyleWithTypeOp(DEFAULT_AUTO_STYLE_TYPE, "a1").toString(),
            Helper.createDeleteAutoStyleOp(DEFAULT_AUTO_STYLE_TYPE, "a1", otis).toString(),
            Helper.createInsertAutoStyleOp("t1", "a2", attrs, null).toString()
        );
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle08() throws JSONException {
        // Result: Should shift style identifiers in indexed mode.
        // testRunner.runBidiTest(
        //  insertAutoStyle('t1', 'a3', ATTRS),
        //  deleteAutoStyle('t1', 'a1'),
        //  insertAutoStyle('t1', 'a2', ATTRS),
        //  deleteAutoStyle('t1', 'a1', OTIS));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("t1", "a3", attrs, null).toString(),
            Helper.createDeleteAutoStyleWithTypeOp("t1", "a1").toString(),
            Helper.createDeleteAutoStyleOp("t1", "a1", otis).toString(),
            Helper.createInsertAutoStyleOp("t1", "a2", attrs, null).toString()
        );
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle09() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode
        // testRunner.expectBidiError(
        //  insertAutoStyle('a2', ATTRS),
        //  deleteAutoStyle('b2'));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("a2", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp("b2").toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle10() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode
        // testRunner.expectBidiError(
        //  insertAutoStyle('b2', ATTRS),
        //  deleteAutoStyle('a2'));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("b2", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp("a2").toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }

    @Test
    public void insertAutoStyleDeleteAutoStyle11() throws JSONException {
        // Result: Should fail for invalid style identifiers in indexed mode
        // testRunner.expectBidiError(
        //  insertAutoStyle('b2', ATTRS),
        //  deleteAutoStyle('b2'));
        TransformerTest.transformBidi(
            Helper.createInsertAutoStyleOp("b2", attrs, null).toString(),
            Helper.createDeleteAutoStyleOp("b2").toString(),
            "{ _CONFLICT_RELOAD_REQUIRED_: true }",
            "{ _CONFLICT_RELOAD_REQUIRED_: true }"
        );
    }
}
