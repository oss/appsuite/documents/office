/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package test.com.openexchange.office.ot;

import org.junit.jupiter.api.Test;
import test.com.openexchange.office.ot.tools.TransformerTest;

public class ChangeStyleSheetChangeStyleSheet {

    @Test
    public void test01() {
        TransformerTest.transform(
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 2' }",
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 2' }",
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 1' }");
            /*
                oneOperation = { name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 1' };
                localActions = [{ operations: [{ name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 2' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 2' }] }], localActions);
                expectOp([{ name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 1' }], transformedOps);
             */
    }

    @Test
    public void test02() {
        TransformerTest.transform(
            "{ name: 'changeStyleSheet', attrs: { family1: { bold: false } }, type: 'paragraph', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { family1: { bold: false } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { family1: { bold: false } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { family1: { bold: false } }, type: 'paragraph', styleId: 'style 1' }");
            /*
                oneOperation = { name: 'changeStyleSheet', attrs: { family1: { bold: false } }, type: 'paragraph', styleId: 'style 1' };
                localActions = [{ operations: [{ name: 'changeStyleSheet', attrs: { family1: { bold: false } }, type: 'character', styleId: 'style 1' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'changeStyleSheet', attrs: { family1: { bold: false } }, type: 'character', styleId: 'style 1' }] }], localActions);
                expectOp([{ name: 'changeStyleSheet', attrs: { family1: { bold: false } }, type: 'paragraph', styleId: 'style 1' }], transformedOps);
             */
    }

    @Test
    public void test03() {
        TransformerTest.transform(
            "{ name: 'changeStyleSheet', attrs: { paragraph: { bold: false } }, type: 'paragraph', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { paragraph: { bold: false } }, type: 'paragraph', styleId: 'style 1' }",
            "[]",
            "[]");
            /*
                oneOperation = { name: 'changeStyleSheet', attrs: { paragraph: { bold: false } }, type: 'paragraph', styleId: 'style 1' };
                localActions = [{ operations: [{ name: 'changeStyleSheet', attrs: { paragraph: { bold: false } }, type: 'paragraph', styleId: 'style 1' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'changeStyleSheet', type: 'paragraph', styleId: 'style 1', _REMOVED_OPERATION_: 1 }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test04() {
        TransformerTest.transform(
            "{ name: 'changeStyleSheet', attrs: { paragraph: { bold: false } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { paragraph: { bold: false, italic: true } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { paragraph: { italic: true } }, type: 'character', styleId: 'style 1' }",
            "[]");
            /*
                oneOperation = { name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 1' };
                localActions = [{ operations: [{ name: 'changeStyleSheet', attrs: { character: { bold: false, italic: true } }, type: 'character', styleId: 'style 1' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'changeStyleSheet', attrs: { character: { italic: true } }, type: 'character', styleId: 'style 1' }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test05() {
        TransformerTest.transform(
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { character: { bold: true, italic: true } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { character: { bold: true, italic: true } }, type: 'character', styleId: 'style 1' }",
            "[]");
            /*
                oneOperation = { name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 1' };
                localActions = [{ operations: [{ name: 'changeStyleSheet', attrs: { character: { bold: true, italic: true } }, type: 'character', styleId: 'style 1' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'changeStyleSheet', attrs: { character: { bold: true, italic: true } }, type: 'character', styleId: 'style 1' }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test06() {
        TransformerTest.transform(
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false }, paragraph: { italic: true } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { paragraph: { italic: true } }, type: 'character', styleId: 'style 1' }",
            "[]");
            /*
                oneOperation = { name: 'changeStyleSheet', attrs: { character: { bold: false } }, type: 'character', styleId: 'style 1' };
                localActions = [{ operations: [{ name: 'changeStyleSheet', attrs: { character: { bold: false }, paragraph: { italic: true } }, type: 'character', styleId: 'style 1' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'changeStyleSheet', attrs: { paragraph: { italic: true } }, type: 'character', styleId: 'style 1' }] }], localActions);
                expectOp([], transformedOps);
             */
    }

    @Test
    public void test07() {
        TransformerTest.transform(
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false, underline: true } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false, italic: true } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { character: { italic: true } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { character: { underline: true } }, type: 'character', styleId: 'style 1' }");
            /*
                oneOperation = { name: 'changeStyleSheet', attrs: { character: { bold: false, underline: true } }, type: 'character', styleId: 'style 1' };
                localActions = [{ operations: [{ name: 'changeStyleSheet', attrs: { character: { bold: false, italic: true } }, type: 'character', styleId: 'style 1' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'changeStyleSheet', attrs: { character: { italic: true } }, type: 'character', styleId: 'style 1' }] }], localActions);
                expectOp([{ name: 'changeStyleSheet', attrs: { character: { underline: true } }, type: 'character', styleId: 'style 1' }], transformedOps);
             */
    }


    @Test
    public void test08() {
        TransformerTest.transform(
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false, italic: false } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { character: { bold: false, italic: true, underline: true } }, type: 'character', styleId: 'style 1' }",
            "{ name: 'changeStyleSheet', attrs: { character: { italic: true, underline: true } }, type: 'character', styleId: 'style 1' }",
            "[]");
            /*
                oneOperation = { name: 'changeStyleSheet', attrs: { character: { bold: false, italic: false } }, type: 'character', styleId: 'style 1' };
                localActions = [{ operations: [{ name: 'changeStyleSheet', attrs: { character: { bold: false, italic: true, underline: true } }, type: 'character', styleId: 'style 1' }] }];
                transformedOps = otManager.transformOperation(oneOperation, localActions);
                expectAction([{ operations: [{ name: 'changeStyleSheet', attrs: { character: { italic: true, underline: true } }, type: 'character', styleId: 'style 1' }] }], localActions);
                expectOp([], transformedOps);
             */
    }

}
