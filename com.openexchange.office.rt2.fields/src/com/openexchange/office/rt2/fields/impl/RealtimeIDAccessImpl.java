package com.openexchange.office.rt2.fields.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.openexchange.file.storage.DelegatingFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.UserizedFile;
import com.openexchange.java.Strings;
import com.openexchange.office.rt2.fields.RealtimeIDAccess;
import com.openexchange.office.rt2.fields.ResourceIDField;
import com.openexchange.office.tools.annotation.RegisteredService;

@Service
@RegisteredService(registeredClass = RealtimeIDAccess.class)
public class RealtimeIDAccessImpl implements RealtimeIDAccess {

    private static final Logger LOG                = LoggerFactory.getLogger(RealtimeIDAccessImpl.class);
    private static final String HIDRIVE_SCHEMA     = "hidrive://";
    private static final int    HIDRIVE_PATH_PARTS = 2;
    private static final String PATH_SEPARATOR     = "/";

    // ---------------------------------------------------------------
    /**
     * Initializes a new {@link ResourceIDField}.
     */
    public RealtimeIDAccessImpl() {
        super();
    }

    // ---------------------------------------------------------------
    @Override
    public String getRealtimeID(File file) {
        return getRealtimeIDFromFile(file);
    }

    // ---------------------------------------------------------------
    /**
     * Gets the realtime file identifier for a specific file. By default, this is the file's (fully qualified) identifier, while for files
     * from the <code>infostore</code> account, only the (relative) file identifier is used in order to support scenarios where the same
     * file appears in different folders for different users.
     *
     * @param file The file to get the realtime identifier for
     * @return The realtime identifier, or <code>null</code> if no identifier can be extracted from the passed file reference
     */
    private static String getRealtimeIDFromFile(File file) {
        if (null != file) {
            final String fileId = file.getId();
            if (StringUtils.isNotEmpty(fileId) && fileId.startsWith(HIDRIVE_SCHEMA)) {
                return extractRealtimeIDFromHiDriveFile(file);
            }
            File accessFile = file;
            if (DelegatingFile.class.isInstance(accessFile)) {
                // Mandatory for Guard support - see DOCS-2723
                // Guard wraps file with a MetaDataAddFile which is not in the
                // class hierarchy regarding UserizedFile. Therefore we need to
                // extract the delegate file to have access to the original file
                // instance.
                accessFile = ((DelegatingFile) accessFile).getDelegate();
            }
            if (UserizedFile.class.isInstance(accessFile)) {
                String originalID = ((UserizedFile) accessFile).getOriginalId();
                return Strings.isEmpty(originalID) ? accessFile.getId() : originalID;
            }
        }
        return null;
    }

    // ---------------------------------------------------------------
    /**
     * Extracts the unique part from a HiDrive file.
     *
     * @param file instance from a hidrive file
     * @return the unique part from the hidrive file to be used as
     *         unique identifier.
     */
    private static String extractRealtimeIDFromHiDriveFile(File file) {
        // Extract static unique part from the hidrive url.
        // ATTENTION: This is highly implementation specific and will
        // break as soon as someone changes the hidrive url structure.
        // We currently assume the following hidrive file-id "synatx"
        //
        // "hidrive://"<account-id>"/"<folderid>"/"<fileId>
        //
        final String hiDriveFileId = file.getId();
        if (hiDriveFileId.length() > HIDRIVE_SCHEMA.length()) {
            final String pathPart = hiDriveFileId.substring(HIDRIVE_SCHEMA.length());
            final int numPathParts = (StringUtils.countMatches(pathPart, PATH_SEPARATOR));
            if (numPathParts == HIDRIVE_PATH_PARTS) {
                // We cut-out the user/account-specific part from the path part
                // to have a global unique id for the file.
                final int offsetFirstSep = pathPart.indexOf(PATH_SEPARATOR);
                String realtimeId = pathPart.substring(offsetFirstSep + 1);
                return realtimeId;
            }
        }

        LOG.warn("Malformed hidrive url detected while trying to parse it {}", hiDriveFileId);
        return file.getId();
    }
}
