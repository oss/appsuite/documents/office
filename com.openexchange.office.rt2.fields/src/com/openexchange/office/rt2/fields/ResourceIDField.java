/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.fields;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openexchange.ajax.customizer.file.AdditionalFileField;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link ResourceIDField}
 *
 * @author <a href="mailto:tobias.friedrich@open-xchange.com">Tobias Friedrich</a>
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.8.0
 */
@Service
@RegisteredService(registeredClass = AdditionalFileField.class)
public class ResourceIDField implements AdditionalFileField {

    @Autowired
    private RealtimeIDAccess resourceID;

    // ---------------------------------------------------------------
    /**
     * Initializes a new {@link ResourceIDField}.
     */
    public ResourceIDField() {
        super();
    }

    // ---------------------------------------------------------------
    @Override
    public Field[] getRequiredFields() {
        return new Field[] { Field.FOLDER_ID, Field.ID };
    }

    // ---------------------------------------------------------------
    @Override
    public int getColumnID() {
        return 7020;
    }

    // ---------------------------------------------------------------
    @Override
    public String getColumnName() {
        return "com.openexchange.realtime.resourceID";
    }

    // ---------------------------------------------------------------
    @Override
    public Object getValue(File file, ServerSession session) {
        return resourceID.getRealtimeID(file);
    }

    // ---------------------------------------------------------------
    @Override
    public List<Object> getValues(List<File> files, ServerSession session) {
        if (null == files) {
            return null;
        }
        List<Object> values = new ArrayList<Object>(files.size());
        for (File file : files) {
            values.add(resourceID.getRealtimeID(file));
        }
        return values;
    }

    // ---------------------------------------------------------------
    @Override
    public Object renderJSON(AJAXRequestData requestData, Object value) {
        return value;
    }

}
