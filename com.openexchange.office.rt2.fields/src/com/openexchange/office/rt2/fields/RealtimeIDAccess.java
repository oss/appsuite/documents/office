package com.openexchange.office.rt2.fields;

import com.openexchange.file.storage.File;

public interface RealtimeIDAccess {

    public String getRealtimeID(File file);
    
}
