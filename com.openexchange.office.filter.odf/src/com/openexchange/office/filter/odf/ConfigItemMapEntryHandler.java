/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import org.xml.sax.Attributes;

/**
 * @author sven.jacobi@open-xchange.com
 */
public class ConfigItemMapEntryHandler extends SaxContextHandler {

	final private ConfigItemMapEntry configItemMapEntry;

	public ConfigItemMapEntryHandler(SaxContextHandler parentContextHandler, ConfigItemMapEntry configItemMapEntry) {
   		super(parentContextHandler);

   		this.configItemMapEntry = configItemMapEntry;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("config:config-item")) {
    		final String name = attributes.getValue("config:name");
    		if(name!=null&&!name.isEmpty()) {
    			final ConfigItem configItem = new ConfigItem(name, attributes);
    			configItemMapEntry.getItems().put(name, configItem);
    			return new ConfigItemHandler(this, configItem);
    		}
    	}
    	else if(qName.equals("config:config-item-map-indexed")) {
    		final String name = attributes.getValue("config:name");
    		if(name!=null&&!name.isEmpty()) {
    			final ConfigItemMapIndexed configItemMapIndexed = new ConfigItemMapIndexed(name);
    			configItemMapEntry.getItems().put(name,  configItemMapIndexed);
    			return new ConfigItemMapIndexedHandler(this, configItemMapIndexed);
    		}
    	}
    	else if(qName.equals("config:config-item-set")) {
    		final String name = attributes.getValue("config:name");
    		if(name!=null&&!name.isEmpty()) {
    			final ConfigItemSet configItemSet = new ConfigItemSet(name);
    			configItemMapEntry.getItems().put(name, configItemSet);
    			return new ConfigItemSetHandler(this, configItemSet);
    		}
    	}
    	else if(qName.equals("config:config-item-map-named")) {
    		final String name = attributes.getValue("config:name");
    		if(name!=null&&!name.isEmpty()) {
    			final ConfigItemMapNamed configItemMapNamed = new ConfigItemMapNamed(name, attributes);
    			configItemMapEntry.getItems().put(name, configItemMapNamed);
    			return new ConfigItemMapNamedHandler(this, configItemMapNamed);
    		}
    	}
     	return this;
	}
}
