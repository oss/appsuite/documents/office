/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.OCKey;

@SuppressWarnings("serial")
public class OpAttrs implements Map<String, Object> {

    private Map<String, Object> map;

    public OpAttrs() {
        map = new HashMap<String, Object>();
    }

    public OpAttrs(int initialCapacity) {
        map = new HashMap<String, Object>(initialCapacity);
    }

    public OpAttrs(Map<String, Object> map) {
        this.map = map;
    }

    public String optString(String key) {
        final Object o = map.get(key);
        return o instanceof String ? (String)o : null;
    }

    @SuppressWarnings("unchecked")
	public OpAttrs getMap(String key, boolean forceCreate) {
        Object o = get(key);
		if(o instanceof OpAttrs) {
		    return (OpAttrs)o;
		}
		if(o instanceof Map) {
			return new OpAttrs((Map<String, Object>)o);
		}
		if(forceCreate) {
		    final OpAttrs op = new OpAttrs();
			map.put(key, op);
			return op;
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	public Object opt(String mapValue, String key) {
		final Object o = map.get(mapValue);
		if(o instanceof Map) {
			return ((Map)o).get(key);
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	public Object remove(String mapValue, String key) {
		final Object o = get(mapValue);
		if(o instanceof Map) {
			return ((Map)o).remove(key);
		}
		return null;
	}

	public void removeEmpty(String mapValue) {
	    final Object o = get(mapValue);
	    if(o instanceof Map&&((Map)o).isEmpty()) {
	        remove(mapValue);
	    }
	}

	public String removeString(String key) {
	    final Object o = remove(key);
	    return o instanceof String ? (String)o : null;
	}

	@Override
    public void clear() {
        map.clear();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Override
    public Set<java.util.Map.Entry<String, Object>> entrySet() {
        return map.entrySet();
    }

    @Override
    public Object get(Object key) {
        return map.get(key);
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public Set<String> keySet() {
        return map.keySet();
    }

    @Override
    public Object put(String key, Object value) {
        return map.put(key, value);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Object> m) {
        map.putAll(m);
    }

    @Override
    public Object remove(Object key) {
        return map.remove(key);
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public Collection<Object> values() {
        return map.values();
    }

/*
    @Override
    public String toString() {
        final Iterator<java.util.Map.Entry<String, Object>> mapIter = map.entrySet().iterator();
        final StringBuffer buffer = new StringBuffer();
        while(mapIter.hasNext()) {
            final java.util.Map.Entry<String, Object> mapEntry = mapIter.next();
            buffer.append(mapEntry.getKey() + "={");
            buffer.append(mapEntry.getValue().toString());
            buffer.append("}");
            if(mapIter.hasNext()) {
                buffer.append(", ");
            }
        }
        return buffer.toString();
    }
*/

    // is copying the content of each hashmap into dest.
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void deepCopy(Map<String, Object> source, Map<String, Object> dest) {
        final Iterator<Entry<String, Object>> sourceIter = source.entrySet().iterator();
        while(sourceIter.hasNext()) {
            final Entry<String, Object> sourceEntry = sourceIter.next();
            if(sourceEntry.getValue() instanceof Map) {
                Object o = dest.get(sourceEntry.getKey());
                if(!(o instanceof Map)) {
                    o = new OpAttrs();
                    dest.put(sourceEntry.getKey(), o);
                }
                deepCopy((Map)sourceEntry.getValue(), (Map)o);
            }
            else {
                dest.put(sourceEntry.getKey(), sourceEntry.getValue());
            }
        }
    }

    /*
     * each source value that is not available within dest will be copied
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void deepMerge(Map<String, Object> source, Map<String, Object> dest) {
        final Iterator<Entry<String, Object>> sourceIter = source.entrySet().iterator();
        while(sourceIter.hasNext()) {
            final Entry<String, Object> sourceEntry = sourceIter.next();
            if(sourceEntry.getValue() instanceof Map) {
                Object o = dest.get(sourceEntry.getKey());
                if(!(o instanceof Map)) {
                    o = new OpAttrs();
                    dest.put(sourceEntry.getKey(), o);
                }
                deepMerge((Map)sourceEntry.getValue(), (Map)o);
            }
            else if(!dest.containsKey(sourceEntry.getKey())) {
                dest.put(sourceEntry.getKey(), sourceEntry.getValue());
            }
        }
    }

    // -------------------------------------------------------------------------

    static final private ImmutableSet<String> deepFamilies = ImmutableSet.<String> builder()
        .add(OCKey.CHARACTER.value())
        .add(OCKey.PARAGRAPH.value())
        .build();

    public static OpAttrs createDifference(Map<String, Object> currentAttrs, Map<String, Object> newAttrs) {

        final OpAttrs difference = new OpAttrs();
        final Iterator<Entry<String, Object>> currentAttrsIter = currentAttrs.entrySet().iterator();
        while(currentAttrsIter.hasNext()) {

            final Entry<String, Object> currentAttrsEntry = currentAttrsIter.next();
            final Object currentValue = currentAttrsEntry.getValue();
            final String key = currentAttrsEntry.getKey();
            final Object newValue = newAttrs !=null ? newAttrs.get(key) : null;

            if((newValue instanceof Map) && (currentValue instanceof Map && deepFamilies.contains(key))) {
                // key is available on both sides as jsonObject
                final OpAttrs result = createDifference((Map)currentValue, (Map)newValue);
                if(result!=null) {
                    difference.put(key, result);
                }
            }
            else if(newValue!=null) {
                // key is available on both sides
                if(!currentValue.equals(newValue)) {
                    difference.put(key, newValue);
                    currentAttrs.put(key, newValue);
                }
            }
            else {
                // key is available only in current attributes
                if(currentValue instanceof Map && deepFamilies.contains(key)) {
                    final OpAttrs result = createDifference((Map)currentValue, null);
                    if(result!=null) {
                        difference.put(key, result);
                    }
                }
                else {
                    difference.put(key, JSONObject.NULL);
                    currentAttrsIter.remove();
                }
            }
        }

        if(newAttrs!=null) {
            // now only applying everything that is not part in lastAttrs
            final Iterator<Entry<String, Object>> newAttrsIter = newAttrs.entrySet().iterator();
            while(newAttrsIter.hasNext()) {
                final Entry<String, Object> newAttrsEntry = newAttrsIter.next();
                if(!currentAttrs.containsKey(newAttrsEntry.getKey())) {
                    Object value = newAttrsEntry.getValue();
                    if(value instanceof Map) {
                        value = cloneMap((Map)value);
                    }
                    else if(value instanceof Collection) {
                        value = cloneCollection((Collection)value);
                    }
                    difference.put(newAttrsEntry.getKey(),  value);
                    currentAttrs.put(newAttrsEntry.getKey(), newAttrsEntry.getValue());
                }
            }
        }
        return difference.isEmpty() ? null : difference;
    }

    // -------------------------------------------------------------------------

    public static Collection cloneCollection(Collection source) {
        final List dest = new ArrayList(source.size());
        int i = 0;
        final Iterator iter = source.iterator();
        while(iter.hasNext()) {
            final Object s = iter.next();
            Object d;
            if(s instanceof Map) {
                d = cloneMap((Map)s);
            }
            else if(s instanceof Collection) {
                d = cloneCollection((Collection)s);
            }
            else {
                // should be java primitive now
                d = s;
            }
            dest.add(i, d);
            i++;
        }
        return dest;
    }

    public static Map cloneMap(Map source) {
        final Map dest = new HashMap(source.size());
        final Iterator<Entry<String, Object>> sourceIter = source.entrySet().iterator();
        while(sourceIter.hasNext()) {
            final Entry<String, Object> sourceEntry = sourceIter.next();
            final Object s = sourceEntry.getValue();
            Object d;
            if(s instanceof Map) {
                d = cloneMap((Map)s);
            }
            else if(s instanceof Collection) {
                d = cloneCollection((Collection)s);
            }
            else {
                // should be java primitive now
                d = s;
            }
            dest.put(sourceEntry.getKey(), d);
        }
        return dest;
    }
}
