/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.xml.parsers.DocumentBuilder;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;

abstract public class ElementToBase64Handler extends SaxContextHandler {

    final StringBuffer buffer;
    final SaxContextHandler parentContextHandler;

	public ElementToBase64Handler(SaxContextHandler parentContextHandler, Attributes attributes, String uri, String localName, String qName) {
		super(parentContextHandler);

		this.buffer = new StringBuffer();
		this.parentContextHandler = parentContextHandler;

		startElement(attributes, uri, localName, qName);
	}

	abstract protected void setBase64ElementData(String data);

	@Override
	public void characters(String characters) {
	    buffer.append(characters);
	}

	@Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        OdfOperationDoc.abortOnLowMemory(getFileDom());
		buffer.append('<');
		buffer.append(qName);
		for(int i=0; i<attributes.getLength(); i++) {
		    buffer.append(' ');
		    buffer.append(attributes.getQName(i));
		    buffer.append("=\"");
		    buffer.append(attributes.getValue(i));
		    buffer.append("\"");
		}
		buffer.append('>');
    	return this;
	}

    @Override
    public void endElement(String localName, String qName) {
        buffer.append("</");
        buffer.append(qName);
        buffer.append(">");
    }

    @Override
    public void endContext(String qName, String characters) {
        if(getContextHandler()==parentContextHandler) {
            endElement(null, qName);
            final ByteArrayOutputStream output = new ByteArrayOutputStream();
            final Base64OutputStream b64os = new Base64OutputStream(output);
            GZIPOutputStream gzip;
            try {
                gzip = new GZIPOutputStream(b64os);
                final String data = buffer.toString();
                gzip.write(data.getBytes());
                IOUtils.closeQuietly(gzip);
                output.close();
            } catch (IOException e) {
                // ohoh
            }
            IOUtils.closeQuietly(b64os);
            setBase64ElementData(output.toString());
            IOUtils.closeQuietly(output);
        }
    }

    public static void writeString(SerializationHandler output, String base64Data) {
        if(base64Data!=null) {
            final ByteArrayInputStream input = new ByteArrayInputStream(base64Data.getBytes());
            final Base64InputStream b64is = new Base64InputStream(input);
            GZIPInputStream gzip;
            try {
                gzip = new GZIPInputStream(b64is);
                output.getWriter().write(IOUtils.toString(gzip, "UTF-8"));
                IOUtils.closeQuietly(gzip);
            } catch (IOException e) {
                // ohoh
            }
            IOUtils.closeQuietly(b64is);
            IOUtils.closeQuietly(input);
        }
    }

    public static Document getDOMFromBase64Data(String base64Data) {
        Document doc = null;
        if(base64Data!=null&&!base64Data.isEmpty()) {
            DocumentBuilder builder;
            ByteArrayInputStream input = null;
            Base64InputStream b64is = null;
            GZIPInputStream gzip = null;
            try {
                builder = OdfPackage.getDocumentBuilder(false);
                input = new ByteArrayInputStream(base64Data.getBytes());
                b64is = new Base64InputStream(input);
                gzip = new GZIPInputStream(b64is);
                doc = builder.parse(gzip);
            } catch (Exception e) {
                //
            }
            finally {
                IOUtils.closeQuietly(gzip);
                IOUtils.closeQuietly(b64is);
                IOUtils.closeQuietly(input);
            }
        }
        return doc;
    }
}
