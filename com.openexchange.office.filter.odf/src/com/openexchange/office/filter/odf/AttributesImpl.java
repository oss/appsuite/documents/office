/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class AttributesImpl implements Cloneable {

	private HashMap<String, AttributeImpl> attributes;
	private int hash;

	public AttributesImpl() {
		this.attributes = new HashMap<String, AttributeImpl>();
	}

	public AttributesImpl(Attributes attributes) {
		this.attributes = new HashMap<String, AttributeImpl>(attributes.getLength());
		setAttributes(attributes);
	}

	public void setAttributes(Attributes attributes) {
		for(int i=0; i<attributes.getLength(); i++) {
			final String qName = attributes.getQName(i);
			final AttributeImpl attributeImpl = new AttributeImpl(attributes.getURI(i), qName, attributes.getLocalName(i), attributes.getValue(i));
			this.attributes.put(qName, attributeImpl);
			hash ^= attributeImpl.hashCode();
		}
	}

	public boolean containsKey(String qName) {
	    return attributes.containsKey(qName);
	}

	public boolean isEmpty() {
		return attributes.isEmpty();
	}

	public AttributeImpl remove(String qName) {
		final AttributeImpl attributeImpl = attributes.remove(qName);
		if(attributeImpl!=null) {
			hash ^= attributeImpl.hashCode();
		}
		return attributeImpl;
	}

	public String getValue(String qName) {
		final AttributeImpl attr = attributes.get(qName);
		if(attr!=null) {
			return attr.getValue();
		}
		return null;
	}

	public void setValue(AttributeImpl attributeImpl) {
		hash ^= attributeImpl.hashCode();
		final AttributeImpl oldAttr = attributes.put(attributeImpl.getQName(), attributeImpl);
		if(oldAttr!=null) {
		    hash ^= oldAttr.hashCode(); 
		}
	}

	public void setValue(String uri, String localName, String qName, String value) {
		if(value==null) {
			remove(qName);
		}
		else {
			final AttributeImpl attributeImpl = new AttributeImpl(uri, qName, localName, value);
			hash ^= attributeImpl.hashCode();
			final AttributeImpl oldAttr = attributes.put(qName, attributeImpl);
			if(oldAttr!=null) {
			    hash ^= oldAttr.hashCode();
			}
		}
	}

	public Integer getIntValue(String qName) {
	    return getInteger(getValue(qName), null);
	}

	public Integer getIntValue(String qName, Integer defaultValue) {
	    return getInteger(getValue(qName), defaultValue);
	}

	public Double getDoubleValue(String qName) {
	    Double ret = null;
	    try {
	        final String val = getValue(qName);
	        if(val!=null) {
	            ret = Double.valueOf(Double.parseDouble(val));
	        }
	    } catch (NumberFormatException e) {
	        //
	    }
	    return ret;
	}

	public void setIntValue(String uri, String localName, String qName, Integer value) {
		if(value==null) {
			remove(qName);
		}
		else {
			setValue(uri, localName, qName, Integer.valueOf(value).toString());
		}
	}

	public Boolean getBooleanValue(String qName, Boolean defaultValue) {
	    return getBoolean(getValue(qName), defaultValue);
	}

	public void setBooleanValue(String uri, String localName, String qName, Boolean value) {
		if(value==null) {
			remove(qName);
		}
		else {
			setValue(uri, localName, qName, value.booleanValue() ? "true" : "false");
		}
	}

	public Integer getLength100thmm(String qName, boolean ignorePercentages) {
		final AttributeImpl attr = attributes.get(qName);
	    if(attr!=null) {
	    	return normalizeLength(attr.getValue(), ignorePercentages);
	    }
	    return null;
	}

	public void setLength100thmm(String uri, String localName, String qName, Integer value) {
		if(value==null) {
			remove(qName);
		}
		else {
			setValue(uri, localName, qName, (value.intValue() / 100.0 + "mm"));
		}
	}

	public void write(SerializationHandler output)
		throws SAXException {

		final Iterator<AttributeImpl> attributesIter = attributes.values().iterator();
		while(attributesIter.hasNext()) {
			attributesIter.next().write(output);
		}
	}

	public void merge(AttributesImpl attrs) {
		final Iterator<Entry<String, AttributeImpl>> iter = attrs.getUnmodifiableMap().entrySet().iterator();
		while(iter.hasNext()) {
			final Entry<String, AttributeImpl> entry = iter.next();
			if(!attributes.containsKey(entry.getKey())) {
				setValue(entry.getValue().clone());
			}
		}
	}

	public Map<String, AttributeImpl> getUnmodifiableMap() {
		return Collections.unmodifiableMap(attributes);
	}

	@Override
	public AttributesImpl clone() {
		try {
			final AttributesImpl clone = (AttributesImpl)super.clone();
			clone.attributes = new HashMap<String, AttributeImpl>();
			final Iterator<AttributeImpl> attributeIter = attributes.values().iterator();
			while(attributeIter.hasNext()) {
				final AttributeImpl attributeClone = attributeIter.next().clone();
				clone.attributes.put(attributeClone.getQName(), attributeClone);
			}
			clone.hash = hash;
			return clone;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int hashCode() {
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttributesImpl other = (AttributesImpl) obj;
		if (hash != other.hash) {
			return false;
		}
		if (!attributes.equals(other.attributes))
			return false;
		return true;
	}

	public static Integer normalizeLength(String value, boolean ignorePercentages) {
    	if(ignorePercentages&&value.contains("%")) {
    		return null;
    	}
    	return normalizeLength(value);
	}

	public static int normalizeLength(String value) {
        return (int) Math.round(new Length(value).getMicrometer() / 10.0);
    }

	public static int lengthToPoint(String value) {
	    return (int) Math.round(new Length(value).getPoint());
	}

	public static Integer getPercentage(String value, Integer defaultValue) {
		Integer percentage = defaultValue;
		if(value!=null&&!value.isEmpty()&&value.charAt(value.length()-1)=='%') {
			try {
				percentage = Integer.parseInt(value.substring(0, value.length()-1));
			}
			catch(NumberFormatException e) {
			    //
			}
		}
		return percentage;
	}

	public static Boolean getBoolean(String value, Boolean defaultValue) {
        if(value==null||value.isEmpty()) {
            return defaultValue;
        }
        try {
            return Boolean.parseBoolean(value);
        }
        catch(NumberFormatException e) {
            return defaultValue;
        }
	}

    public static Integer getInteger(String value, Integer defaultValue) {
        Integer ret = defaultValue;
        try {
            if(value!=null) {
                ret = Integer.valueOf(Integer.parseInt(value));
            }
        } catch (NumberFormatException e) {
            //
        }
        return ret;
    }

    @Override
    public String toString() {
        return "AttributesImpl " + getUnmodifiableMap();
    }

    @SafeVarargs
    final public void applyToken(String namespace, String localName, String qName, final Pair<String, Object>... pairs) {
        final Set<String> tokens = new HashSet<String>(Arrays.asList(ArrayUtils.nullToEmpty(StringUtils.split(getValue(qName)))));
        for(Pair<String, Object> pair:pairs) {
            final String key = pair.getKey();
            final Object value = pair.getValue();
            if(pair.getValue() instanceof Boolean) {
                if(((Boolean)value).booleanValue()) {
                    tokens.add(key);
                }
                else {
                    tokens.remove(key);
                }
            }
            else if(value==JSONObject.NULL) {
                tokens.remove(key);
            }
        }
        if(tokens.isEmpty()) {
            remove(qName);
        }
        else {
            final StringBuilder builder = new StringBuilder();
            final Iterator<String> protectTokenIter = tokens.iterator();
            while(protectTokenIter.hasNext()) {
                final String v = protectTokenIter.next();
                if(builder.length()>0) {
                    builder.append(' ');
                }
                builder.append(v);
            }
            setValue(namespace, localName, qName, builder.toString());
        }
    }
}
