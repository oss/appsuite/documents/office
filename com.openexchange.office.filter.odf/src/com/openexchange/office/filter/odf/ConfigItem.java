/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class ConfigItem implements IElementWriter {

	final String name;
	String type;
	String value;

    public ConfigItem(String name, Short val) {
        this.name = name;
        value = val.toString();
        type = "short";
    }

	public ConfigItem(String name, Integer val) {
		this.name = name;
		value = val.toString();
		type = "int";
	}

	public ConfigItem(String name, Boolean val) {
		this.name = name;
		this.type = "boolean";
		this.value = val ? "true" : "false";
	}

	public ConfigItem(String name, Attributes attributes) {
		this.name = name;
		type = attributes.getValue("config:type");
		value = null;
	}

	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.CONFIG, "config-item", "config:config-item");
		SaxContextHandler.addAttribute(output, Namespaces.CONFIG, "name", "config:name", name);
		SaxContextHandler.addAttribute(output, Namespaces.CONFIG, "type", "config:type", type!=null?type:"");
		if(value!=null) {
			output.characters(value);
		}
		SaxContextHandler.endElement(output, Namespaces.CONFIG, "config-item", "config:config-item");
	}
}
