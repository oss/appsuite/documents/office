/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.listlevel;

import java.util.Iterator;
import java.util.Map;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.BinaryData;
import com.openexchange.office.filter.odf.Length;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.PropertyHelper;
import com.openexchange.office.filter.odf.properties.StylePropertiesBase;
import com.openexchange.office.filter.odf.properties.TextProperties;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.TextListStyle;

abstract public class ListLevelStyleEntry extends StylePropertiesBase {

    protected ListLevelStyleEntry(AttributesImpl attributesImpl) {
        super(attributesImpl);
    }

    private ListLevelProperties listLevelProperties = null;
    private TextProperties textProperties = null;
    private BinaryData binaryData = null;

    public Integer getListLevel(Integer defaultValue) {
        final Integer listLevel = attributes.getIntValue("text:level");
        return listLevel==null ? defaultValue : listLevel;
    }

    public void setListLevel(Integer value) {
        attributes.setIntValue(Namespaces.TEXT, "level", "text:level", value);
    }

    @Override
    protected DLList<IElementWriter> getContent() {
        return null;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        if(ListLevelProperties.hasData(listLevelProperties)) {
            hash = hash * 31 + listLevelProperties.hashCode();
        }
        if(TextProperties.hasData(textProperties)) {
            hash = hash * 31 + textProperties.hashCode();
        }
        if(BinaryData.hasData(binaryData)) {
            hash = hash * 31 + binaryData.hashCode();
        }
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(!super.equals(obj)) {
            return false;
        }
        final ListLevelStyleEntry other = (ListLevelStyleEntry)obj;
        if(ListLevelProperties.hasData(listLevelProperties)) {
            if(!ListLevelProperties.hasData(other.listLevelProperties)) {
                return false;
            }
            if(!listLevelProperties.equals(other.listLevelProperties)) {
                return false;
            }
        }
        else {
            if(ListLevelProperties.hasData(other.listLevelProperties)) {
                return false;
            }
        }
        if(TextProperties.hasData(textProperties)) {
            if(!TextProperties.hasData(other.textProperties)) {
                return false;
            }
            if(!textProperties.equals(other.textProperties)) {
                return false;
            }
        }
        else {
            if(TextProperties.hasData(other.textProperties)) {
                return false;
            }
        }
        if(BinaryData.hasData(binaryData)) {
            if(!BinaryData.hasData(other.binaryData)) {
                return false;
            }
            if(!binaryData.equals(other.binaryData)) {
                return false;
            }
        }
        else {
            if(BinaryData.hasData(other.binaryData)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void writeObject(SerializationHandler output)
        throws SAXException {

        if(!attributes.isEmpty()||(getContent()!=null&&!getContent().isEmpty())||!getTextContent().isEmpty()) {
            SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
            attributes.write(output);
            if(!text.isEmpty()) {
                output.characters(text);
            }
            if(ListLevelProperties.hasData(listLevelProperties)) {
                listLevelProperties.writeObject(output);
            }
            if(TextProperties.hasData(textProperties)) {
                textProperties.writeObject(output);
            }
            if(BinaryData.hasData(binaryData)) {
                binaryData.writeObject(output);
            }
            final DLList<IElementWriter> content = getContent();
            if(content!=null) {
                final Iterator<IElementWriter> childIter = content.iterator();
                while(childIter.hasNext()) {
                    childIter.next().writeObject(output);
                }
            }
            SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
        }
    }

    @Override
    abstract public ListLevelStyleEntry clone();

    @Override
    protected Object _clone() {
        final ListLevelStyleEntry clone = (ListLevelStyleEntry)super._clone();
        if(listLevelProperties!=null) {
            clone.setListLevelProperties(listLevelProperties.clone());
        }
        if(textProperties!=null) {
            clone.setTextProperties(textProperties.clone());
        }
        if(binaryData!=null) {
            clone.setBinaryData(binaryData.clone());
        }
        return clone;
    }

    public TextProperties getTextProperties(boolean forceCreate) {
        if(textProperties==null&&forceCreate) {
            textProperties = new TextProperties(new AttributesImpl());
        }
        return textProperties;
    }

    public void setTextProperties(TextProperties textProperties) {
        this.textProperties = textProperties;
    }

    public ListLevelProperties getListLevelProperties(boolean forceCreate) {
        if(listLevelProperties==null&&forceCreate) {
            listLevelProperties = new ListLevelProperties(new AttributesImpl());
        }
        return listLevelProperties;
    }

    public void setListLevelProperties(ListLevelProperties listLevelProperties) {
        this.listLevelProperties = listLevelProperties;
    }

    public BinaryData getBinaryData(boolean forceCreate) {
        if(binaryData==null&&forceCreate) {
            binaryData = new BinaryData(new AttributesImpl());
        }
        return binaryData;
    }

    public void setBinaryData(BinaryData binaryData) {
        this.binaryData = binaryData;
    }

    public void createPresentationAttrs(StyleManager styleManager, String listStyleType, boolean contentAutoStyle, OpAttrs paragraphAttrs) {

        final TextProperties textProperties = getTextProperties(false);
        if(textProperties!=null) {
            /*
             * seems that bullets with title objects does not work in OO
             */
            if(!listStyleType.equals("title")) {
                final String fontSize = textProperties.getAttribute("fo:font-size");
                if(fontSize!=null) {

                    final OpAttrs bulletSize = paragraphAttrs.getMap(OCKey.BULLET_SIZE.value(), true);
                    if(fontSize.contains("%")) {
/*  TODO: frontend is having some problems with type percent
                        bulletSize.put(OCKey.TYPE.shortName(), "percent");
                        bulletSize.put(OCKey.HEIGHT.shortName(), AttributesImpl.getPercentage(fontSize, 50));
*/
                        bulletSize.put(OCKey.TYPE.value(), "followText");
                    }
                    else {
                        bulletSize.put(OCKey.TYPE.value(), "point");
                        bulletSize.put(OCKey.HEIGHT.value(), AttributesImpl.lengthToPoint(fontSize));
                    }
                }
                final String foColor = textProperties.getAttribute("fo:color");
                if(foColor!=null) {
                    final Map<String, Object> color = PropertyHelper.createColorMap(foColor);
                    if(color!=null) {
                        paragraphAttrs.getMap(OCKey.BULLET_COLOR.value(), true).put(OCKey.COLOR.value(), color);
                    }
                }
            }
            String fontName = null;
            final String fontFamily = textProperties.getAttribute("fo:font-family");
            if(fontFamily!=null) {
                fontName = fontFamily;
            }
            final String _fontName = textProperties.getAttribute("style:font-name");
            if(_fontName!=null) {
                fontName = _fontName;
            }
            if(fontName!=null) {
                final OpAttrs bulletFont = paragraphAttrs.getMap(OCKey.BULLET_FONT.value(), true);
                bulletFont.put(OCKey.FOLLOW_TEXT.value(), false);
                bulletFont.put(OCKey.NAME.value(), fontName);
            }
        }

        final ListLevelProperties listLevelProperties = getListLevelProperties(false);
        if(listLevelProperties!=null) {
            /*
            final String height = styleListLevelProperties.getAttribute("fo:height");
            if(height!=null) {
                listLevelDefinition.put(OCKey.HEIGHT.shortName(), AttributesImpl.normalizeLength(height));
            }
*/
            final String textAlign = listLevelProperties.getAttribute("fo:text-align");
            if(textAlign!=null) {
                paragraphAttrs.put(OCKey.TEXT_ALIGN.value(), PropertyHelper.mapFoTextAlign(textAlign));
            }
            final String height = listLevelProperties.getAttribute("fo:height");
            // final String width = listLevelProperties.getAttribute("fo:width");
            if(height!=null) {
                final OpAttrs bulletSize = paragraphAttrs.getMap(OCKey.BULLET_SIZE.value(), true);
                bulletSize.put(OCKey.TYPE.value(), "point");
                bulletSize.put(OCKey.SIZE.value(), AttributesImpl.lengthToPoint(height));
            }
/*
            final String verticalPos = styleListLevelProperties.getAttribute("style:vertical-pos");
            if(verticalPos!=null) {
                listLevelDefinition.put(OCKey.VERTICAL_POS.value(), verticalPos);
            }
            final String verticalRel = styleListLevelProperties.getAttribute("style:vertical-rel");
            if(verticalRel!=null) {
                listLevelDefinition.put(OCKey.VERTICAL_REL.value(), verticalRel);
            }
            final String svgY = styleListLevelProperties.getAttribute("svg:y");
            if(svgY!=null) {
                listLevelDefinition.put("y", svgY);
            }
*/

/* TODO: dont know what to do with this attributes
            final String positionAndSpaceMode = styleListLevelProperties.getAttribute("text:list-level-position-and-space-mode");
            if(positionAndSpaceMode!=null) {
                listLevelDefinition.put(OCKey.LIST_LEVEL_POSITION_AND_SPACE_MODE.value(), positionAndSpaceMode);
            }
            final String minLabelDistance = styleListLevelProperties.getAttribute("text:min-label-distance");
            if(minLabelDistance!=null&&!minLabelDistance.isEmpty()) {
                listLevelDefinition.put(OCKey.MIN_LABEL_DISTANCE.value(), AttributesImpl.normalizeLength(minLabelDistance));
            }
*/
            final String minLabelWidth = listLevelProperties.getAttribute("text:min-label-width");
            if(minLabelWidth!=null) {
                paragraphAttrs.put(OCKey.MIN_LABEL_WIDTH.value(), minLabelWidth);
            }
            final String spaceBefore = listLevelProperties.getAttribute("text:space-before");
            if(spaceBefore!=null) {
                paragraphAttrs.put(OCKey.SPACE_BEFORE.value(), spaceBefore);
            }
            final ListLevelLabelAlignment listLevelLabelAlignment = listLevelProperties.getListLevelLabelAlignment(false);
            if(listLevelLabelAlignment!=null) {
                final String marginLeft = listLevelLabelAlignment.getAttribute("fo:margin-left");
                if(Length.isValid(marginLeft)) {
                    paragraphAttrs.put(OCKey.INDENT_LEFT.value(), AttributesImpl.normalizeLength(marginLeft));
                }
                final String textIndent = listLevelLabelAlignment.getAttribute("fo:text-indent");
                if(Length.isValid(textIndent)) {
                    paragraphAttrs.put(OCKey.INDENT_FIRST_LINE.value(), AttributesImpl.normalizeLength(textIndent));
                }
                final String listTabStopPosition = listLevelLabelAlignment.getAttribute("text:list-tab-stop-position");
                if(listTabStopPosition!=null&&!listTabStopPosition.isEmpty()) {
                    paragraphAttrs.put(OCKey.TAB_STOP_POSITION.value(), AttributesImpl.normalizeLength(listTabStopPosition));
                }
                final String labelFollowedBy = listLevelLabelAlignment.getAttribute("text:label-followed-by");
                if(labelFollowedBy!=null) {
                    paragraphAttrs.put(OCKey.LABEL_FOLLOWED_BY.value(), labelFollowedBy);
                }
            }
        }
    }

    public static void finalizeParagraphAttrs(OpAttrs paragraphAttrs) {
        final String minLabelWidth = paragraphAttrs.removeString(OCKey.MIN_LABEL_WIDTH.value());
        final String spaceBefore = paragraphAttrs.removeString(OCKey.SPACE_BEFORE.value());
        final String indentLeft = paragraphAttrs.removeString(OCKey.INDENT_LEFT.value());
        final String indentFirstLine = paragraphAttrs.removeString(OCKey.INDENT_FIRST_LINE.value());
        TextListStyle.mapIndent(minLabelWidth, spaceBefore, indentLeft, paragraphAttrs);
        TextListStyle.mapFirstLineIndent(minLabelWidth, spaceBefore, indentFirstLine, paragraphAttrs);
    }

    public void applyPresentationAttrs(StyleManager styleManager, JSONObject paragraphAttrs)
        throws JSONException {

        TextProperties textProperties = null;

        final Object bulletColor = paragraphAttrs.opt(OCKey.BULLET_COLOR.value());
        if(bulletColor!=null) {
            if (bulletColor==JSONObject.NULL) {
                textProperties = getTextProperties(false);
                if(textProperties!=null) {
                    textProperties.getAttributes().remove("fo:color");
                }
            }
            else {
                textProperties = getTextProperties(true);
                final JSONObject color = (JSONObject)bulletColor;
                if (color.hasAndNotNull(OCKey.TYPE.value())) {
                    String type = color.optString(OCKey.TYPE.value(), "");
                    if(!type.equals("auto")) {
                        textProperties.getAttributes().setValue(Namespaces.FO, "color", "fo:color", PropertyHelper.getColor(color, null));
                        textProperties.getAttributes().remove("style:use-window-font-color");
                    } else {
                        textProperties.getAttributes().setValue(Namespaces.STYLE, "use-window-font-color", "style:use-window-font-color", "true");
                    }
                } else {
                    textProperties.getAttributes().setValue(Namespaces.STYLE, "use-window-font-color", "style:use-window-font-color", "true");
                }
            }
        }
        final Object bulletFont = paragraphAttrs.opt(OCKey.BULLET_FONT.value());
        if(bulletFont!=null) {
            if(bulletFont==JSONObject.NULL) {
                if(textProperties==null) {
                    textProperties = getTextProperties(false);
                }
                if(textProperties!=null) {
                    textProperties.getAttributes().remove("fo:font");
                }
            }
            else {
                if(textProperties==null) {
                    textProperties = getTextProperties(true);
                }
                // TODO: final Object followText = ((JSONObject)bulletFont).opt(OCKey.FOLLOW_TEXT.shortName());
                final Object fontName = ((JSONObject)bulletFont).opt(OCKey.NAME.value());
                if(fontName!=null) {
                    if(fontName==JSONObject.NULL) {
                        textProperties.getAttributes().remove("fo:font");
                    }
                    else {
                        textProperties.getAttributes().setValue(Namespaces.STYLE, "font-name", "style:font-name", (String)fontName);
                    }
                }
            }
        }
    }
}
