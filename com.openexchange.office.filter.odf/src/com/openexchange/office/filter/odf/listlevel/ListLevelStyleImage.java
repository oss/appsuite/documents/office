/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.listlevel;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.TextListStyle;

public class ListLevelStyleImage extends ListLevelStyleEntry {

	public ListLevelStyleImage(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	@Override
	public ListLevelStyleImage clone() {
		return (ListLevelStyleImage)super._clone();
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

    @Override
    public void createPresentationAttrs(StyleManager styleManager, String listStyleType, boolean contentAutoStyle, OpAttrs paragraphAttrs) {
        final OpAttrs bullet = paragraphAttrs.getMap(OCKey.BULLET.value(), true);
        bullet.put(OCKey.TYPE.value(), "bitmap");
        bullet.put(OCKey.IMAGE_URL.value(), getAttribute("xlink:href"));
        super.createPresentationAttrs(styleManager, listStyleType, contentAutoStyle, paragraphAttrs);
    }

    @Override
    public void applyPresentationAttrs(StyleManager styleManager, JSONObject paragraphAttrs)
        throws JSONException {

        final Object bulletAttrs = paragraphAttrs.opt(OCKey.BULLET.value());
        if(bulletAttrs instanceof JSONObject) {
            final String imageUrl = ((JSONObject)bulletAttrs).optString(OCKey.IMAGE_URL.value());
            if(imageUrl!=null&&!imageUrl.isEmpty()) {
                attributes.setValue(Namespaces.XLINK, "href", "xlink:href", imageUrl);
                attributes.setValue(Namespaces.XLINK, "actuate", "xlink:actuate", "onLoad");
                attributes.setValue(Namespaces.XLINK, "show", "xlink:show", "embed");
                attributes.setValue(Namespaces.XLINK, "type", "xlink:type", "simple");
            }
        }
        super.applyPresentationAttrs(styleManager, paragraphAttrs);
    }

    @Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
        final Integer listLevel = getListLevel(0);
		if(listLevel>=1&&listLevel<=10) {
			TextListStyle.createListLevelDefinition(this, attrs, listLevel);
		}
	}

	@Override
	public String getQName() {
		return "text:list-level-style-image";
	}

	@Override
	public String getLocalName() {
		return "list-level-style-image";
	}

	@Override
	public String getNamespace() {
		return Namespaces.TEXT;
	}
}
