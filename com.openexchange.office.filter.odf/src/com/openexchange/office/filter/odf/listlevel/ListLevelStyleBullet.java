/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.listlevel;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.properties.TextProperties;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.TextListStyle;

public class ListLevelStyleBullet extends ListLevelStyleEntry {

	public ListLevelStyleBullet(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	/*
	 * level starting from 1 to ...
	 */
	public ListLevelStyleBullet(int level) {
	    super(new AttributesImpl());
	    setListLevel(level);
	}

	@Override
	public ListLevelStyleBullet clone() {
		return (ListLevelStyleBullet)super._clone();
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

    @Override
    public void createPresentationAttrs(StyleManager styleManager, String listStyleType, boolean contentAutoStyle, OpAttrs paragraphAttrs) {
        final OpAttrs bullet = paragraphAttrs.getMap(OCKey.BULLET.value(), true);
        bullet.put(OCKey.TYPE.value(), "character");
        final String c = getAttribute("text:bullet-char");
        if(c!=null) {
            bullet.put(OCKey.CHARACTER.value(), c);
            attributes.setValue(Namespaces.TEXT, "bullet-char", "text:bullet-char", c);
        }
        final TextProperties textProperties = getTextProperties(false);
        if(textProperties!=null) {
	        final String buSize = textProperties.getAttribute("fo:font-size");
	        if(buSize!=null) {
	        	final Integer o = AttributesImpl.getPercentage(buSize, null);
	        	if(o!=null) {
		            final OpAttrs bulletSize = paragraphAttrs.getMap(OCKey.BULLET_SIZE.value(), true);
		            bulletSize.put(OCKey.TYPE.value(), "percent");
		            bulletSize.put(OCKey.SIZE.value(), o);
	        	}
	        }
        }
        super.createPresentationAttrs(styleManager, listStyleType, contentAutoStyle, paragraphAttrs);
    }

    @Override
    public void applyPresentationAttrs(StyleManager styleManager, JSONObject paragraphAttrs)
        throws JSONException {

    	final TextProperties textProperties = getTextProperties(true);
        final Object bulletAttrs = paragraphAttrs.opt(OCKey.BULLET.value());
        if(bulletAttrs instanceof JSONObject) {
            final Object character = ((JSONObject)bulletAttrs).opt(OCKey.CHARACTER.value());
            if(character!=null) {
                String c = "";
                if(character instanceof String) {
                    if(!((String)character).isEmpty()) {
                        c = ((String) character).substring(0, 1);
                    }
                }
                else {
                    c = "*";
                }
                attributes.setValue(Namespaces.TEXT, "bullet-char", "text:bullet-char", c);
            }
        }
        final Object bulletSizeAttrs = paragraphAttrs.opt(OCKey.BULLET_SIZE.value());
        if(bulletSizeAttrs!=null) {
        	if(bulletSizeAttrs instanceof JSONObject) {
        		final String type = ((JSONObject)bulletSizeAttrs).optString(OCKey.TYPE.value());
        		if("percent".equals(type)) {
        			final Object buSize = ((JSONObject)bulletSizeAttrs).opt(OCKey.SIZE.value());
        			if(buSize!=null) {
        				if(buSize instanceof Number) {
        					textProperties.getAttributes().setValue(Namespaces.FO, "font-size", "fo:font-size", Integer.toString(((Number)buSize).intValue())+"%");
        				}
        				else {
        					textProperties.getAttributes().remove("fo:font-size");
        				}
        			}
        		}
        	}
        	else {
    			attributes.remove("fo:font-size");
    			// TODO what is "text:bullet-relative-size");
        	}
        }
        super.applyPresentationAttrs(styleManager, paragraphAttrs);
    }

    @Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
	    final Integer listLevel = getListLevel(0);
		if(listLevel>=1&&listLevel<=10) {
			TextListStyle.createListLevelDefinition(this, attrs, listLevel);
		}
	}

	@Override
	public String getQName() {
		return "text:list-level-style-bullet";
	}

	@Override
	public String getLocalName() {
		return "list-level-style-bullet";
	}

	@Override
	public String getNamespace() {
		return Namespaces.TEXT;
	}
}
