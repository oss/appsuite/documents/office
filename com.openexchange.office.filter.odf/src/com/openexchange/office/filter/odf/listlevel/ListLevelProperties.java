/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.listlevel;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.StylePropertiesBase;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class ListLevelProperties extends StylePropertiesBase {

	private ListLevelLabelAlignment listLevelLabelAlignment = null;

	public ListLevelProperties(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

    public ListLevelLabelAlignment getListLevelLabelAlignment(boolean forceCreate) {
        if(listLevelLabelAlignment==null&&forceCreate) {
            listLevelLabelAlignment = new ListLevelLabelAlignment(new AttributesImpl());
        }
        return listLevelLabelAlignment;
    }

    public void setListLevelLabelAlignment(ListLevelLabelAlignment listLevelLabelAlignment) {
        this.listLevelLabelAlignment = listLevelLabelAlignment;
    }

    static public boolean hasData(ListLevelProperties o) {
        if(o==null) {
            return false;
        }
        if(!o.getAttributes().isEmpty()) {
            return true;
        }
        return ListLevelLabelAlignment.hasData(o.listLevelLabelAlignment);
    }

    @Override
    public void writeObject(SerializationHandler output)
        throws SAXException {

        if(!attributes.isEmpty()||(getContent()!=null&&!getContent().isEmpty())||!getTextContent().isEmpty()) {
            SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
            attributes.write(output);
            if(!text.isEmpty()) {
                output.characters(text);
            }
            if(ListLevelLabelAlignment.hasData(listLevelLabelAlignment)) {
                listLevelLabelAlignment.writeObject(output);
            }
            SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
        }
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        if(ListLevelLabelAlignment.hasData(listLevelLabelAlignment)) {
            hash = hash * 31 + listLevelLabelAlignment.hashCode();
        }
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(!super.equals(obj)) {
            return false;
        }
        final ListLevelProperties other = (ListLevelProperties)obj;
        if(ListLevelLabelAlignment.hasData(listLevelLabelAlignment)) {
            if(!ListLevelLabelAlignment.hasData(other.listLevelLabelAlignment)) {
                return false;
            }
            if(!listLevelLabelAlignment.equals(other.listLevelLabelAlignment)) {
                return false;
            }
        }
        else {
            if(ListLevelLabelAlignment.hasData(other.listLevelLabelAlignment)) {
                return false;
            }
        }
        return true;
    }

	@Override
	public ListLevelProperties clone() {
		final ListLevelProperties clone = (ListLevelProperties)super._clone();
		if(listLevelLabelAlignment!=null) {
		    clone.setListLevelLabelAlignment(listLevelLabelAlignment.clone());
		}
		return clone;
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		// TODO Auto-generated method stub
	}

	@Override
	public String getQName() {
		return "style:list-level-properties";
	}

	@Override
	public String getLocalName() {
		return "list-level-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}
}
