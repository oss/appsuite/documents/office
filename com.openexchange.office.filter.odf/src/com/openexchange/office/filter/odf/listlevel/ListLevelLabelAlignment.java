/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.listlevel;

import org.json.JSONObject;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.properties.StylePropertiesBase;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class ListLevelLabelAlignment extends StylePropertiesBase {

	public ListLevelLabelAlignment(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

    static public boolean hasData(ListLevelLabelAlignment o) {
        if(o==null) {
            return false;
        }
        return !o.getAttributes().isEmpty();
    }

    @Override
	public ListLevelLabelAlignment clone() {
		final ListLevelLabelAlignment newStyleBullet = (ListLevelLabelAlignment)super._clone();
		return newStyleBullet;
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		// TODO Auto-generated method stub
	}

	@Override
	public String getQName() {
		return "style:list-level-label-alignment";
	}

	@Override
	public String getLocalName() {
		return "list-level-label-alignment";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}
}
