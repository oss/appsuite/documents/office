/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.listlevel;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.TextListStyle;

public class ListLevelStyleNumber extends ListLevelStyleEntry {

	public ListLevelStyleNumber(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	@Override
	public ListLevelStyleNumber clone() {
		return (ListLevelStyleNumber)super._clone();
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
    public void createPresentationAttrs(StyleManager styleManager, String listStyleType, boolean contentAutoStyle, OpAttrs paragraphAttrs) {
        final OpAttrs bullet = paragraphAttrs.getMap(OCKey.BULLET.value(), true);
        final String numType = TextListStyle.getNumberFormat(this);
        if(numType.equals("none")) {
            bullet.put(OCKey.TYPE.value(), "none");
        }
        else {
            bullet.put(OCKey.TYPE.value(), "numbering");

            final String numFormat = attributes.getValue("style:num-format");
            final String numSuffix = attributes.getValue("style:num-suffix");
            final String numPrefix = attributes.getValue("style:num-prefix");

            final String destFormat;
            switch(getPrefix(numPrefix) + getNumFormat(numFormat) + getSuffix(numSuffix)) {
            	case "a.":  destFormat = "alphaLcPeriod"; break;
            	case "a":
            	case "a)":  destFormat = "alphaLcParenR"; break;
            	case "(a)": destFormat = "alphaLcParenBoth"; break;
            	case "A.":  destFormat = "alphaUcPeriod"; break;
            	case "A":
            	case "A)":  destFormat = "alphaUcParenR"; break;
            	case "(A)": destFormat = "alphaUcParenBoth"; break;
            	case "i.":  destFormat = "romanLcPeriod"; break;
            	case "i":
            	case "i)":  destFormat = "romanLcParenR"; break;
            	case "(i)": destFormat = "romanLcParenBoth"; break;
            	case "I.":  destFormat = "romanUcPeriod"; break;
            	case "I":
            	case "I)":  destFormat = "romanUcParenR"; break;
            	case "(I)": destFormat = "romanUcParenBoth"; break;
            	case "1.":  destFormat =  "arabicPeriod"; break;
            	case "1":
            	case "1)":
            	default:	destFormat = "arabicParenR"; break;
            	case "(1)": destFormat = "arabicParenBoth"; break;
            }
            bullet.put(OCKey.NUM_TYPE.value(), destFormat);

            final String startValue = getAttribute("text:start-value");
            if (startValue!=null) {
                bullet.put(OCKey.START_AT.value(), Integer.parseInt(startValue));
            }
        }
        super.createPresentationAttrs(styleManager, listStyleType, contentAutoStyle, paragraphAttrs);
    }

	private String getNumFormat(String s) {
		final String numFormat = getFirstChar(s);
		return numFormat.equals("1")||numFormat.equals("a")||numFormat.equals("A")||numFormat.equals("i")||numFormat.equals("I") ? numFormat : "1";
	}

	private String getPrefix(String s) {
		final String prefix = getFirstChar(s);
		return prefix.isEmpty()||prefix.equals("(") ? prefix : "";
	}

	private String getSuffix(String s) {
		final String suffix = getFirstChar(s);
		return suffix.isEmpty()||suffix.equals(".")||suffix.equals(")") ? suffix : "";
	}

	private String getFirstChar(String s) {
		return s==null ? "" : s.isEmpty() ? "" : s.substring(0, 1);
	}

	@Override
    public void applyPresentationAttrs(StyleManager styleManager, JSONObject paragraphAttrs)
        throws JSONException {

	    final Object bulletAttrs = paragraphAttrs.opt(OCKey.BULLET.value());
	    if(bulletAttrs instanceof JSONObject) {
	        final String numType = ((JSONObject)bulletAttrs).optString(OCKey.NUM_TYPE.value());
	        if(numType!=null) {
	            String numFormat = null;
	            String numSuffix = null;
	            String numPrefix = null;
	            switch(numType) {
	                case "alphaLcPeriod": {
	                    numFormat = "a";
	                    numSuffix = ".";
	                    break;
	                }
	                case "alphaLcParenR": {
	                    numFormat = "a";
	                    numSuffix = ")";
	                    break;
	                }
	                case "alphaLcParenBoth": {
	                    numFormat = "a";
	                    numSuffix = ")";
	                    numPrefix = "(";
	                    break;
	                }
	                case "alphaUcPeriod": {
	                    numFormat = "A";
	                    numSuffix = ".";
	                    break;
	                }
	                case "alphaUcParenR": {
	                    numFormat = "A";
	                    numSuffix = ")";
	                    break;
	                }
	                case "alphaUcParenBoth": {
	                    numFormat = "A";
	                    numSuffix = ")";
	                    numPrefix = "(";
	                    break;
	                }
	                case "romanLcPeriod": {
	                    numFormat = "i";
	                    numSuffix = ".";
	                    break;
	                }
	                case "romanLcParenR": {
	                    numFormat = "i";
	                    numSuffix = ")";
	                    break;
	                }
	                case "romanLcParenBoth": {
	                    numFormat = "i";
	                    numSuffix = ")";
	                    numPrefix = "(";
	                    break;
	                }
	                case "romanUcPeriod": {
	                    numFormat = "I";
	                    numSuffix = ".";
	                    break;
	                }
	                case "romanUcParenR": {
	                    numFormat = "I";
	                    numSuffix = ")";
	                    break;
	                }
	                case "romanUcParenBoth": {
	                    numFormat = "I";
	                    numSuffix = ")";
	                    numPrefix = "(";
	                    break;
	                }
	                case "arabicPeriod": {
	                    numFormat = "1";
	                    numSuffix = ".";
	                    break;
	                }
	                case "arabicParenR": {
	                    numFormat = "1";
	                    numSuffix = ")";
	                    break;
	                }
	                case "arabicParenBoth": {
	                    numFormat = "1";
	                    numSuffix = ")";
	                    numPrefix = "(";
	                    break;
	                }
	                case "noNumberListItem": {
	                    break;
	                }
	            }
	            if(numFormat!=null) {
	                attributes.setValue(Namespaces.STYLE, "num-format", "style:num-format", numFormat);
	            }
	            if(numSuffix!=null) {
	                attributes.setValue(Namespaces.STYLE, "num-suffix", "style:num-suffix", numSuffix);
	            }
	            if(numPrefix!=null) {
	                attributes.setValue(Namespaces.STYLE, "num-prefix", "style:num-prefix", numPrefix);
	            }
	        }
	        final Object startAt = ((JSONObject)bulletAttrs).opt(OCKey.START_AT.value());
	        if(startAt!=null) {
    	        if(startAt instanceof Number) {
    	            attributes.setIntValue(Namespaces.TEXT, "start-value", "text:start-value",  ((Number)startAt).intValue());
    	        }
    	        else {
    	            attributes.remove("text:start-value");
    	        }
	        }
	    }
        final Object bulletSizeAttrs = paragraphAttrs.opt(OCKey.BULLET_SIZE.value());
        if(bulletSizeAttrs!=null) {
            if(bulletSizeAttrs instanceof JSONObject) {
                final String type = ((JSONObject)bulletSizeAttrs).optString(OCKey.TYPE.value());
                if("percent".equals(type)) {
                    final Object buSize = ((JSONObject)bulletSizeAttrs).opt(OCKey.SIZE.value());
                    if(buSize!=null) {
                        if(buSize instanceof Number) {
                            getTextProperties(true).getAttributes().setValue(Namespaces.FO, "font-size", "fo:font-size", Integer.toString(((Number)buSize).intValue())+"%");
                        }
                        else {
                            getTextProperties(true).getAttributes().remove("fo:font-size");
                        }
                    }
                }
            }
            else {
                attributes.remove("fo:font-size");
                // TODO what is "text:bullet-relative-size");
            }
        }
        super.applyPresentationAttrs(styleManager, paragraphAttrs);
    }

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
        final Integer listLevel = getListLevel(0);
		if(listLevel>=1&&listLevel<=10) {
			TextListStyle.createListLevelDefinition(this, attrs, listLevel);
		}
	}

	@Override
	public String getQName() {
		return "text:list-level-style-number";
	}
	@Override
	public String getLocalName() {
		return "list-level-style-number";
	}

	@Override
	public String getNamespace() {
		return Namespaces.TEXT;
	}
}
