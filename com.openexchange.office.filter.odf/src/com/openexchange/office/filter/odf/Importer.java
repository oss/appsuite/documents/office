/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import java.io.InputStream;
import java.util.Map;

import org.json.JSONObject;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.springframework.beans.factory.annotation.Autowired;

import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.api.IPartImporter;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.office.tools.service.config.ConfigurationHelper;
import com.openexchange.session.Session;

/**
 * {@link Importer}
 *
 * @author <a href="mailto:svante.schubert@open-xchange.com">Svante Schubert</a>
 */
abstract public class Importer implements IImporter, IPartImporter, OsgiBundleContextAware {

	@Autowired
    private ConfigurationHelper configurationHelper;

	protected OsgiBundleContextAndActivator bundleCtx;

    /**
     * Initializes a new {@link Importer}.
     */
    public Importer() {
        //
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.office.IImporter#createOperations(java.io.InputStream)
     */
    @Override
    public JSONObject createOperations(Session session, InputStream inputDocumentStm, DocumentProperties documentProperties, boolean createFastLoadOperations) {

        JSONObject operations = null;
        try (OdfOperationDoc odfdomDocument = new OdfOperationDoc(session, null, documentProperties)) {
        	bundleCtx.prepareObject(odfdomDocument);
            odfdomDocument.loadDocument(inputDocumentStm);
            odfdomDocument.setCreateFastLoadOperations(createFastLoadOperations);
            operations = odfdomDocument.getOperations();
        }
        catch(Throwable e) {
            OdfOperationDoc.rethrowFilterException(e, (OdfPackage)null);
        }
        return operations;
    }

	@Override
	public void initPartitioning(Session session, InputStream inputDocument, DocumentProperties documentProperties, boolean createFastLoadOperations) {

	    try(OdfOperationDoc odfdomDocument = new OdfOperationDoc(session, null, documentProperties)) {
	    	bundleCtx.prepareObject(odfdomDocument);
            odfdomDocument.loadDocument(inputDocument);
            odfdomDocument.setCreateFastLoadOperations(createFastLoadOperations);
            documentProperties.put(DocumentProperties.PROP_DOCUMENT, odfdomDocument);
    		odfdomDocument.initPartitioning(documentProperties);
	    }
	    catch(Throwable e) {
            OdfOperationDoc.rethrowFilterException(e, (OdfPackage)null);
	    }
	}

	@Override
	public Map<String, Object> getMetaData(DocumentProperties documentProperties) {
        Map<String, Object> metaData = null;
	    final OdfOperationDoc operationDocument = (OdfOperationDoc)documentProperties.get(DocumentProperties.PROP_DOCUMENT);
	    try {
	        operationDocument.registerMemoryListener();
	        metaData = operationDocument.getMetaData(documentProperties);
	    }
	    catch(Throwable e) {
	        OdfOperationDoc.rethrowFilterException(e, operationDocument);
	    }
	    finally {
	        operationDocument.close();
	    }
	    return metaData;
	}

	@Override
	public Map<String, Object> getActivePart(DocumentProperties documentProperties) {
        Map<String, Object> metaData = null;
	    final OdfOperationDoc operationDocument = (OdfOperationDoc)documentProperties.get(DocumentProperties.PROP_DOCUMENT);
	    bundleCtx.injectDependencies(operationDocument);
	    try {
            operationDocument.registerMemoryListener();
	        metaData = operationDocument.getActivePart(documentProperties);
	    }
        catch(Throwable e) {
            OdfOperationDoc.rethrowFilterException(e, operationDocument);
        }
        finally {
            operationDocument.close();
        }
        return metaData;
	}

	@Override
	public Map<String, Object> getNextPart(DocumentProperties documentProperties) {
        Map<String, Object> metaData = null;
	    final OdfOperationDoc operationDocument = (OdfOperationDoc)documentProperties.get(DocumentProperties.PROP_DOCUMENT);
	    try {
            operationDocument.registerMemoryListener();
	        metaData = operationDocument.getNextPart(documentProperties);
	    }
        catch(Throwable e) {
            OdfOperationDoc.rethrowFilterException(e, operationDocument);
        }
        finally {
            operationDocument.close();
        }
        return metaData;
	}

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}
}
