/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * 
 * @author sven.jacobiATopen-xchange.com
 * 
 */

package com.openexchange.office.filter.odf;

import org.odftoolkit.odfdom.dom.OdfSchemaDocument;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;

/**
 * @author sven.jacobi@open-xchange.com
 */
public class MetaDataHandler extends DOMBuilder {

    private final MetaData metaData;
    private OdfSchemaDocument mSchemaDoc = null;

    public MetaDataHandler(Node rootNode, XMLReader xmlReader) {
    	super(rootNode, xmlReader, (MetaData)rootNode);

        // Initialize starting DOM node
    	metaData = (MetaData)rootNode;
        mSchemaDoc = metaData.getDocument();
        mSchemaDoc.setMetaDom(metaData);
    }

    @Override
	public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName){
        if(qName.equals("office:meta")) {
    		MetaDataImpl metaDataImpl = metaData.getMetaDataImpl(true);
    		return new MetaDataImplHandler(this, metaDataImpl);
    	}
        // the default DOMBuilder.startElement creates and inserts the proper odfNode
    	return super.startElement(attributes, uri, localName, qName);
    }
}
