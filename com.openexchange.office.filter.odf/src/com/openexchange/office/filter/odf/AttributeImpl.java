/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf;

import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.SAXException;

final public class AttributeImpl implements Cloneable {

	final private String uri;
	final private String qName;
	final private String localName;
	final private String value;
	final private int hash;

	public AttributeImpl(String uri, String qName, String localName, String value) {
		this.uri = uri;
		this.qName = qName;
		this.localName = localName;
		this.value = value;

		final int prime = 31;
		int result = 1;
		result = prime * result + localName.hashCode();
		result = prime * result + qName.hashCode();
		result = prime * result + uri.hashCode();
		hash = prime * result + value.hashCode();
	}

	public String getUri() {
		return uri;
	}

	public String getQName() {
		return qName;
	}

	public String getLocalName() {
		return localName;
	}

	public String getValue() {
		return value;
	}

	public int getIntValue() {
		int i = 0;
		try {
			i = Integer.parseInt(value);
		}
		catch (NumberFormatException e) {
		    //
		}
		return i;
	}

	public int getIntValue(int defaultValue) {
        try {
            return Integer.parseInt(value);
        }
        catch (NumberFormatException e) {
            return defaultValue;
        }
	}

	public void write(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.addAttribute(output, getUri(), getLocalName(), getQName(), getValue());
	}

	@Override
	public AttributeImpl clone() {
		return this;
	}

	@Override
	public int hashCode() {
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttributeImpl other = (AttributeImpl) obj;
		if (hashCode() != other.hashCode()) {
			return false;
		}
		if (!localName.equals(other.localName))
			return false;
		if (!qName.equals(other.qName))
			return false;
		if (!uri.equals(other.uri))
			return false;
		if (!value.equals(other.value))
			return false;
		return true;
	}

    @Override
    public String toString() {
        return "AttributeImpl [qName=" + qName + ", value=" + value + "]";
    }

}
