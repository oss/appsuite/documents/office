/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * 
 * @author sven.jacobiATopen-xchange.com
 * 
 */

package com.openexchange.office.filter.odf;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.xerces.dom.ElementNSImpl;
import org.odftoolkit.odfdom.dom.OdfSchemaDocument;
import org.odftoolkit.odfdom.dom.OdfSettingsNamespace;
import org.odftoolkit.odfdom.pkg.NamespaceName;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * The DOM representation of the ODF meta.xml file of an ODF document.
 */
public class MetaData extends OdfFileDom {

	private static final long serialVersionUID = 766167617530147885L;

	private MetaDataImpl metaDataImpl;

	public static boolean updateDate = false;

	public MetaData(OdfSchemaDocument odfDocument, String packagePath) throws SAXException {
		super(odfDocument, packagePath);
	}

	@Override
	protected void initialize() throws SAXException {
		for (NamespaceName name : OdfSettingsNamespace.values()) {
			mUriByPrefix.put(name.getPrefix(), name.getUri());
			mPrefixByUri.put(name.getUri(), name.getPrefix());
		}
		final XMLReader xmlReader = mPackage.getXMLReader();
		super.initialize(new MetaDataHandler(this, xmlReader), xmlReader);
		ElementNSImpl rootElement = getRootElement();
		if(rootElement == null){
			rootElement = new ElementNS(this, Namespaces.OFFICE, "office:document-meta");
			this.appendChild(rootElement);
			rootElement.appendChild(new MetaDataImpl(this));
		}
	}

	@Override
	public OdfSchemaDocument getDocument() {
		return (OdfSchemaDocument)mPackageDocument;
	}

	public MetaDataImpl getMetaDataImpl(boolean forceCreate) {
        if(metaDataImpl==null&&forceCreate) {
            metaDataImpl = new MetaDataImpl(this);
            getRootElement().appendChild(metaDataImpl);
        }
        return metaDataImpl;
    }

	public void updataMetaData() {

	    // update late modified date
	    if(updateDate) {
	        setDate(calendarToString(Calendar.getInstance()));
	    }

		// update editing cycles
		setEditingCycles(getEditingCycles()+1);

		// update editing-duration
		getMetaDataImpl(true).removeItem("meta:editing-duration");

		// remove the statistic, it might be incorrect now
		getMetaDataImpl(true).removeItem("meta:document-statistic");
	}

	private String getItemValue(String qName) {
		if(metaDataImpl!=null) {
			final MetaItem metaItem = metaDataImpl.getItem(qName);
			if(metaItem!=null) {
				return metaItem.getValue();
			}
		}
	    return null;
	}

	public String getCreator() {
		return getItemValue("dc:creator");
	}

	public void setCreator(String creator) {
		getMetaDataImpl(true).setItem(new MetaItem(Namespaces.DC, "creator", "dc:creator", creator));
    }

	public String getInitialCreator() {
		return getItemValue("meta:initial-creator");
	}

	public void setInitialCreator(String initialCreator) {
		getMetaDataImpl(true).setItem(new MetaItem(Namespaces.META, "initial-creator", "meta:initial-creator", initialCreator));
	}

    public String getGenerator() {
        return getItemValue("meta:generator");
    }

	public void setGenerator(String generator) {
		getMetaDataImpl(true).setItem(new MetaItem(Namespaces.META, "generator", "meta:generator", generator));
	}

	public void setDate(String date) {
		getMetaDataImpl(true).setItem(new MetaItem(Namespaces.DC, "date", "dc:date", date));
	}

	public int getEditingCycles() {
		final String v =  getItemValue("meta:editing-cycles");
		if(v!=null) {
			try {
				Integer.parseInt(v);
			}
			catch(NumberFormatException e) {
				//
			}
		}
		return 0;
	}

	public void setEditingCycles(int v) {
		getMetaDataImpl(true).setItem(new MetaItem(Namespaces.META, "editing-cycles", "meta:editing-cycles", Integer.valueOf(v).toString()));
	}

	private String calendarToString(Calendar calendar) {
		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(calendar.getTime());
	}
}
