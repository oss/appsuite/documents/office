/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfDocument.OdfMediaType;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.dom.OdfSchemaDocument.OdfXMLFile;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.xml.sax.SAXException;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.DocumentComplexity;
import com.openexchange.office.filter.core.NotSupportedMediaTypeException;
import com.openexchange.office.filter.core.OperationDocument;
import com.openexchange.office.filter.odf.ods.dom.SpreadsheetContent;
import com.openexchange.office.filter.odf.odt.dom.TextContent;
import com.openexchange.office.filter.odf.odt.dom.TextStyles;
import com.openexchange.office.filter.odf.styles.DocumentStyles;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.common.memory.MemoryObserver;
import com.openexchange.session.Session;

/**
 * The access point to the ODF Toolkit, obfuscating all OX Office dependencies
 * from the Toolkit.
 *
 */
public class OdfOperationDoc extends OperationDocument implements InitializingBean {

	private static final Logger LOG = LoggerFactory.getLogger(OdfOperationDoc.class);
    private static final String STATUS_ID_FILE = "oxoffice/extendedData.json";

    final private long FACTOR_FOR_REQUIRED_HEAPSPACE = 80;
    private static final String MODULE = "//module/";
    private static final String SPREADSHEET = "//spreadsheet/";
    private static final String DEBUG_OPERATIONS = "debugoperations";
    private static final String MAX_TABLE_COLUMNS = "maxTableColumns";
    private static final String MAX_TABLE_ROWS = "maxTableRows";
    private static final String MAX_TABLE_CELLS = "maxTableCells";
    private static final String MAX_SHEETS = "maxSheets";

    private static final String ORIGNAL_ODT_FILE = "debug/documentStream.dbg";
    private static final String OPERATION_TEXT_FILE_PREFIX = "debug/operationUpdates.dbg";

    private boolean isMetadataUpdated = false;

	private OdfDocument mDocument;

    private boolean mSaveDebugOperations = false;

    public OdfOperationDoc(Session session, IResourceManager resourceManager, DocumentProperties _documentProperties) {
        super(session, resourceManager, _documentProperties);

    }

    private boolean createFastLoadOperations = false;

    public void setCreateFastLoadOperations(boolean createFastLoadOperations) {
        this.createFastLoadOperations = createFastLoadOperations;
    }

    public boolean isCreateFastLoadOperations() {
        return createFastLoadOperations;
    }

    @Override
	public void afterPropertiesSet() throws Exception {

		registerMemoryListener();

		if (configurationHelper != null) {
			mSaveDebugOperations = Boolean.parseBoolean(configurationHelper.getOfficeConfigurationValue(session, MODULE + DEBUG_OPERATIONS, "false"));
	        documentProperties.put(MAX_TABLE_COLUMNS, configurationHelper.getIntegerOfficeConfigurationValue(session, MODULE + MAX_TABLE_COLUMNS, 15));
	        documentProperties.put(MAX_TABLE_ROWS, configurationHelper.getIntegerOfficeConfigurationValue(session, MODULE + MAX_TABLE_ROWS, 1500));
	        documentProperties.put(MAX_TABLE_CELLS, configurationHelper.getIntegerOfficeConfigurationValue(session, MODULE + MAX_TABLE_CELLS, 1500));
	        documentProperties.put(MAX_SHEETS, configurationHelper.getIntegerOfficeConfigurationValue(session, SPREADSHEET + MAX_SHEETS, 256));
		} else {
			mSaveDebugOperations = false;
	        documentProperties.put(MAX_TABLE_COLUMNS, 15);
	        documentProperties.put(MAX_TABLE_ROWS, 1500);
	        documentProperties.put(MAX_TABLE_CELLS, 1500);
	        documentProperties.put(MAX_SHEETS, 256);
		}
	}

	final public void loadDocument(InputStream _inputStream) {
        try {
            // complexity check... TODO: distinguish between odt and odf
        	long maxAllowedXmlSizeMatchingComplexity = 50000000;
        	if (configurationHelper != null) {
        		maxAllowedXmlSizeMatchingComplexity = configurationHelper.getIntegerOfficeConfigurationValue(session, "//spreadsheet/maxCells", 500000) * 100;
        	}
            final long maxAllowedXmlSizeMatchingMemory = MemoryObserver.calcMaxHeapSize(50) / FACTOR_FOR_REQUIRED_HEAPSPACE;
            final InputStream inputStream = DocumentComplexity.checkDocumentComplexity(_inputStream, maxAllowedXmlSizeMatchingComplexity, maxAllowedXmlSizeMatchingMemory);

            mDocument = OdfDocument.loadDocument(inputStream, documentProperties);
            mDocument.getStyleManager().setResourceManager(resourceManager);
        }
        catch(Throwable e) {
            rethrowFilterException(e, this);
        }
    }

    public InputStream createPackage(OdfMediaType odfMediaType, DocumentProperties docProps) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            mDocument = OdfDocument.loadDocumentFromTemplate(odfMediaType);
            mDocument.save(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
        catch(Throwable e) {
            rethrowFilterException(e, this);
        }
        return null;
    }

    final public void updateDocumentProperties() {

        try {

            final OdfDocument odfDocument = getDocument();

            // Always overwrite uniqueDocumentID when writing changes..
            if (documentProperties != null) {
                final String uniqueDocumentIdentifier = documentProperties.optString(DocumentProperties.PROP_UNIQUE_DOCUMENT_IDENTIFIER, null);
                if (uniqueDocumentIdentifier != null) {
                    putUniqueDocumentIdentifier(uniqueDocumentIdentifier);
                }
                String lastModifiedBy = documentProperties.optString(
                        DocumentProperties.PROP_LAST_MODIFIED_BY, null);
                String creator = documentProperties.optString(
                        DocumentProperties.PROP_CREATOR, null);
                // Manipulating the meta.xml in case the initial author or
                // last author is given..
                if (lastModifiedBy != null || creator != null) {
                    final MetaData metaDom = odfDocument.getOfficeMetaData(false);
                    if(metaDom!=null) {
                        String currentCreator = metaDom.getCreator();
                        String initialCreator = metaDom.getInitialCreator();
                        // only apply a change if the previous author was not the same
                        if (lastModifiedBy != null && (currentCreator == null || !currentCreator.equals(lastModifiedBy))) {
                            metaDom.setCreator(lastModifiedBy);
                        }
                        if (creator != null && (initialCreator == null || !initialCreator.equals(creator))) {
                            metaDom.setInitialCreator(creator);
                        }
                    }
                }
            }
            if (null != odfDocument) {
                odfDocument.getPackage().setCheckUnusedRelations(createFinalDocument);
            }
        }
        catch(Throwable e) {
            rethrowFilterException(e, this);
        }
    }

    final public InputStream save() {
        InputStream inputStream = null;
        try {
            inputStream = mDocument.getPackage().getInputStream();
        }
        catch(Throwable e) {
            rethrowFilterException(e, this);
        }
        return inputStream;
    }

    /**
     * Applies the (known) operations to upon the latest state of the ODF text
     * document
     *
     * @param operationString ODF operations as String
     * @return the number of operations being accepted
     * @throws Exception
     */
    public int applyOperations(String operationString) {
        int operationCount = 0;
        try {
            final JSONObject operations = new JSONObject();
            operations.put("operations", new JSONArray(new JSONTokener(operationString)));
            final JSONArray ops = operations.getJSONArray("operations");
            if (mSaveDebugOperations) {
                addOriginalOdfAsDebug();
                addOperationFileAsDebug(operationString);
            }
            final OdfDocument document = getDocument();
            document.getStylesDom(); // ensure that the dom is loaded for styles and content
            document.getContentDom();
            document.getStyleManager().removeUnusedStyles();
            operationCount = document.applyOperations(this, ops);
            if(operationCount > 0) {
                // remove the cached view
                removeCachedView();
                if (!isMetadataUpdated) {
                    mDocument.updateMetaData();
                    isMetadataUpdated = true;
                }
            }
            // if the document was altered
            if (operationCount > 0) {
                // remove the cached view
                removeCachedView();
            }
        }
        catch(Throwable e) {
            rethrowFilterException(e, this);
        }
        return operationCount;
    }

    /**
     * Receives the (known) operations of the ODF text document
     *
     * @return the operations as JSON
     */
    // ToDo OX - JSONObject is to be considered..
    public JSONObject getOperations() {
        JSONObject ops = null;
        try {
            ops = mDocument.getOperations(this);
            if (ops != null && ops.length() > 0) {
                LOG.debug("\n\n*** ALL OPERATIONS:\n{0}", ops.toString());
            } else {
                LOG.debug("\n\n*** ALL OPERATIONS:\nNo Operation have been extracted!");
            }
        }
        catch(Throwable e) {
            rethrowFilterException(e, this);
        }
        return ops;
    }

    public Map<String, Object> getMetaData(DocumentProperties docProperties) {
        Map<String, Object> metaData = null;
        try {
            metaData = mDocument.getMetaData(docProperties);
        }
        catch(Throwable e) {
            rethrowFilterException(e, this);
        }
        return metaData;
    }

    public Map<String, Object> getActivePart(DocumentProperties docProperties) {
        Map<String, Object> metaData = null;
        try {
            metaData = mDocument.getActivePart(docProperties);
        }
        catch(Throwable e) {
            rethrowFilterException(e, this);
        }
        return metaData;
    }

    public Map<String, Object> getNextPart(DocumentProperties docProperties) {
        Map<String, Object> metaData = null;
        try {
            metaData = mDocument.getNextPart(docProperties);
        }
        catch(Throwable e) {
            rethrowFilterException(e, this);
        }
        return metaData;
    }

	public static boolean isLowMemoryAbort(OdfPackage odfPackage) {
	    return odfPackage!=null ? odfPackage.isLowMemoryAbort() : false;
	}
	public static void setLowMemoryAbort(boolean l, OdfPackage odfPackage) {
	    if(odfPackage!=null) {
	        odfPackage.setLowMemoryAbort(l);
	    }
	}
	public static void abortOnLowMemory(OdfFileDom odfFileDom) {
	    if(odfFileDom!=null&&odfFileDom.getDocument()!=null) {
	        abortOnLowMemory(odfFileDom.getDocument().getPackage());
	    }
	}
	public static void abortOnLowMemory(OdfPackage odfPackage) {
	    if(isLowMemoryAbort(odfPackage)) {
	        throw new RuntimeException();
	    }
	}

    // take care... this function is called from the low-memory
    // thread, so breakpoints will not work... It is called before
    // gc is started and or an OutOfMemory Exception is thrown, so we
    // try to free up as much as possible ... the filter will then
    // most probably exit with an exception
	@Override
	protected void notifyLowMemory() {
        LOG.debug("ODF Filter: Low memory notification received");
        if(mDocument!=null) {

            setLowMemoryAbort(true, mDocument.getPackage());

            // trying to free memory ...
        	try {
        		final DocumentStyles stylesDom = mDocument.getStylesDom();
        		stylesDom.abort();
        	}
        	catch(SAXException e) {
        	    //
        	}
        	if(mDocument instanceof OdfSpreadsheetDocument) {
        		try {
        			final SpreadsheetContent content = (SpreadsheetContent)((OdfSpreadsheetDocument)mDocument).getContentDom();
        			content.getContent().clear();
        		}
        		catch(SAXException e) {
        		    //
        		}
        	}
        	final OdfPackage odfPackage = mDocument.getPackage();
	        if(odfPackage!=null) {
	            odfPackage.freeMemory();
	        }
        }
    }

    public static void rethrowFilterException(Throwable e, OdfOperationDoc odfOperationDoc) {
        rethrowFilterException(e, odfOperationDoc!=null&&odfOperationDoc.mDocument!=null ? odfOperationDoc.getDocument().getPackage() : null);
    }

    public static void rethrowFilterException(Throwable e, OdfPackage odfPackage) {

        ExceptionUtils.handleThrowable(e);

        FilterException ret;
        if(isLowMemoryAbort(odfPackage)) {
            ret = new FilterException("ODS FilterException...", ErrorCode.MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED);
        }
        else if(e instanceof FilterException) {
    		ret = (FilterException)e;
    	}
    	else if (e instanceof java.util.zip.ZipException && e.getMessage().contains("only DEFLATED entries can have EXT descriptor")) {
            ret = new FilterException(e, ErrorCode.UNSUPPORTED_ENCRYPTION_USED);
        }
    	else if (e instanceof OutOfMemoryError) {
    		ret = new FilterException(e, ErrorCode.MEMORY_USAGE_MIN_FREE_HEAP_SPACE_REACHED);
    	}
    	else if (e instanceof NotSupportedMediaTypeException) {
    	    ret = new FilterException(e, ErrorCode.CRITICAL_ERROR, ((NotSupportedMediaTypeException)e).getMediaType());
    	    ret.setSubType(FilterException.ST_MEDIATYPE_NOT_SUPPORTED);
    	}
    	else {
    		ret = new FilterException(e, ErrorCode.CRITICAL_ERROR);
    	}
        if(odfPackage!=null) {
            ret.setOperationCount(odfPackage.getSuccessfulAppliedOperations());
        }
        throw ret;
    }

    public void initPartitioning(DocumentProperties docProperties) {
		mDocument.initPartitioning(docProperties);
	}

    private void removeCachedView() {
        final OdfPackage odfPackage = getDocument().getPackage();
        if(odfPackage!=null) {
            odfPackage.remove("Thumbnails/thumbnail.png");
        }
    }

    private void addOriginalOdfAsDebug()
        throws IOException, SAXException {

        final OdfPackage pkg = mDocument.getPackage();
        // if there is not already an orignal file being stored
        if (!pkg.contains(ORIGNAL_ODT_FILE)) {
            final InputStream input = pkg.getInputStream();
            if(input.markSupported()) {
                input.reset();
                ZipInputStream zipInputStream = new ZipInputStream(input);
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                ZipOutputStream zipOutputStream = new ZipOutputStream(outStream);
                ZipEntry zipEntry = null;
                byte[] buffer = new byte[0x10000];  // 64kb buffer
                while((zipEntry = zipInputStream.getNextEntry())!=null) {

                    // if the source document is containing a debug folder, we will
                    // skip the old debug information and not include it within our copy
                    if(!zipEntry.getName().startsWith("debug/")) {
                        ZipEntry newEntry = new ZipEntry(zipEntry.getName());
                        zipOutputStream.putNextEntry(newEntry);
                        int read;
                        while ((read = zipInputStream.read(buffer))>=0){
                            zipOutputStream.write(buffer,0,read);
                        }
                        zipOutputStream.closeEntry();
                    }
                    zipInputStream.closeEntry();
                }
                zipInputStream.close();
                zipOutputStream.close();
                pkg.insert(outStream.toByteArray(), ORIGNAL_ODT_FILE, "application/octet-stream");
            }
        }
    }

    private void addOperationFileAsDebug(String operations) {
        final OdfPackage pkg = mDocument.getPackage();
        if (!pkg.contains(OPERATION_TEXT_FILE_PREFIX)) {
            pkg.insert(operations.getBytes(), OPERATION_TEXT_FILE_PREFIX, "application/octet-stream");
        }
    }

    /**
     * writes the status ID - which reflects the latest changes of the document,
     * used as cashing ID of web representation to enable performance
     * @throws JSONException
     * @throws UnsupportedEncodingException
     */
    void putUniqueDocumentIdentifier(String uniqueDocumentIdentifier) throws JSONException, UnsupportedEncodingException {
        final OdfPackage pkg = mDocument.getPackage();
        final JSONObject jsonObject = new JSONObject(1);
        jsonObject.put("uniqueDocumentIdentifier", uniqueDocumentIdentifier);
        pkg.insert(jsonObject.toString().getBytes("UTF-8"), STATUS_ID_FILE, "application/octet-stream");
    }

    public long getContentSize() {
        if(mDocument==null) {
            return 0;
        }
        final OdfPackage odfPackage = mDocument.getPackage();
        return odfPackage!=null ? odfPackage.getSize(OdfXMLFile.CONTENT.getFileName()) : 0;
    }

    /**
     * Returns the TextOperationDocument encapsulating the DOM view
     *
     * @return ODF text document
     */
    public OdfDocument getDocument() {
        return mDocument;
    }
}
