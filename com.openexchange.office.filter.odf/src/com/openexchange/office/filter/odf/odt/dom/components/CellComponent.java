/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom.components;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.table.Cell;
import com.openexchange.office.filter.odf.table.Table;

public class CellComponent extends OdfComponent {

	final Cell cell;
	final int gridPosition;

	public CellComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> tableNode, int componentNumber, int gridPosition) {
		super(parentContext, tableNode, componentNumber);
		this.cell = (Cell)getObject();
		this.gridPosition = gridPosition;
	}

	@Override
	public String simpleName() {
	    return "Cell";
	}

	public int getGridPosition() {
		return gridPosition;
	}

	public int getNextGridPosition() {
		return gridPosition + cell.getColumnSpan();
	}

	public Cell getCell() {
		return cell;
	}

	@Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {

        final int nextComponentNumber = previousChildComponent != null ? previousChildComponent.getNextComponentNumber() : 0;
        DLNode<Object> nextNode = previousChildContext != null ? previousChildContext.getNode().getNext() : ((Cell)getObject()).getContent().getFirstNode();
		while(nextNode!=null) {
			if(nextNode.getData() instanceof Paragraph) {
				return new ParagraphComponent(this, nextNode, nextComponentNumber);
			}
			else if(nextNode.getData() instanceof Table) {
				return new TableComponent(this, nextNode, nextComponentNumber);
			}
			nextNode = nextNode.getNext();
		}
		return null;
	}

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> contextNode, int number, IComponent<OdfOperationDoc> child, ComponentType type, JSONObject attrs) {

    	DLList<Object> DLList = ((INodeAccessor)contextNode.getData()).getContent();
        DLNode<Object> referenceNode = child != null && child.getComponentNumber()== number ? child.getNode() : null;

        switch(type) {
            case PARAGRAPH : {
            	final DLNode<Object> newParagraphNode = new DLNode<Object>(new Paragraph(null));
                DLList.addNode(referenceNode, newParagraphNode, true);
                return new ParagraphComponent(parentContext, newParagraphNode, number);
            }
            case TABLE : {
            	final DLNode<Object> newTableNode = new DLNode<Object>(new Table("text"));
                DLList.addNode(referenceNode, newTableNode, true);
                return new TableComponent(parentContext, newTableNode, number);
            }
            default : {
                throw new UnsupportedOperationException();
            }
        }
    }

	@Override
	public void applyAttrsFromJSON(JSONObject attrs)
		throws JSONException {

		cell.setStyleName(operationDocument.getDocument().getStyleManager().createStyle(StyleFamily.TABLE_CELL, cell.getStyleName(), isContentAutoStyle(), attrs));

		final JSONObject cellProperties = attrs.optJSONObject(OCKey.CELL.value());
		if(cellProperties!=null) {
			final Object gridSpan = cellProperties.opt(OCKey.GRID_SPAN.value());
			if(gridSpan instanceof Integer) {
				cell.setColumnSpan((Integer)gridSpan);
			}
		}
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {

		if(cell.getStyleName()!=null&&!cell.getStyleName().isEmpty()) {
			final OpAttrs hardCellAttrs = new OpAttrs();
			operationDocument.getDocument().getStyleManager().
				createAutoStyleAttributes(hardCellAttrs, cell.getStyleName(), StyleFamily.TABLE_CELL, isContentAutoStyle());
			final Map<String, Object> cellAttrs = hardCellAttrs.getMap(OCKey.CELL.value(), false);
			if(cellAttrs!=null) {
				attrs.put(OCKey.CELL.value(), cellAttrs);
			}
		}
		final int columnSpan = cell.getColumnSpan();
		if(columnSpan>1) {
			attrs.getMap(OCKey.CELL.value(), true).put(OCKey.GRID_SPAN.value(), columnSpan);
		}
	}

	public ParagraphComponent getFirstParagraphChildComponent() {
	    IComponent<OdfOperationDoc> childComponent = getNextChildComponent(null, null);
	    while(childComponent!=null) {
	        if(childComponent instanceof ParagraphComponent) {
	            return (ParagraphComponent)childComponent;
	        }
	        childComponent = childComponent.getNextComponent();
	    }
	    return null;
	}
}
