/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleMasterPage;
import com.openexchange.office.filter.odf.styles.StyleMasterPageHandler;
import com.openexchange.office.filter.odf.styles.StylesHandler;

public class TextMasterStylesHandler extends SaxContextHandler {

	final StyleManager styleManager;

	public TextMasterStylesHandler(SaxContextHandler parentContext, StyleManager styleManager) {
		super(parentContext);

		this.styleManager = styleManager;
	}

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("style:master-page")) {
        	final AttributesImpl attributesImpl = new AttributesImpl(attributes);
        	final String name = StylesHandler.removeNameFromAttributes(attributesImpl);
        	if(name!=null) {
	    		return new StyleMasterPageHandler(this, new StyleMasterPage(name, attributesImpl), styleManager);
        	}
    	}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	styleManager.getMasterStyles().getContent().add(element);
		return new UnknownContentHandler(this, element);
    }
}
