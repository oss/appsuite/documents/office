/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.CUtil;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.IParagraph;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.Settings;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.draw.Transformer;
import com.openexchange.office.filter.odf.odt.dom.components.AnnotationComponent;
import com.openexchange.office.filter.odf.odt.dom.components.ParagraphComponent;
import com.openexchange.office.filter.odf.odt.dom.components.RootComponent;
import com.openexchange.office.filter.odf.odt.dom.components.RowComponent;
import com.openexchange.office.filter.odf.odt.dom.components.TableComponent;
import com.openexchange.office.filter.odf.styles.MasterStyles;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleHeaderFooter;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleMasterPage;
import com.openexchange.office.filter.odf.styles.StylePageLayout;
import com.openexchange.office.filter.odf.styles.TextListStyle;

public class JsonOperationConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(JsonOperationConsumer.class);

	private final OdfOperationDoc opsDoc;
    private final OdfTextDocument doc;
    private final TextStyles styles;
    private final TextContent content;
    private final StyleManager styleManager;
    private final Settings settings;

    public JsonOperationConsumer(OdfOperationDoc opsDoc)
		throws SAXException {

		this.opsDoc = opsDoc;
		doc = (OdfTextDocument)opsDoc.getDocument();
		styles = (TextStyles)doc.getStylesDom();
		content = (TextContent)doc.getContentDom();
		styleManager = doc.getStyleManager();
	    settings = doc.getSettingsDom();

	    // only creating target nodes for the correct master page, so this is possible only after loading styles + content
	    createHeaderFooterTargetNodes(content.getBody().getMasterPageName(true));
	}

    public int applyOperations(JSONArray operations)
    	throws Exception {

    	for (int i = 0; i < operations.length(); i++) {
        	doc.getPackage().setSuccessfulAppliedOperations(i);
            final JSONObject op = (JSONObject) operations.get(i);
            final OCValue opName = OCValue.fromValue(op.getString(OCKey.NAME.value()));
            switch(opName)
            {
            	case INSERT_TEXT : {
		            insertText(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.optJSONObject(OCKey.ATTRS.value()), op.getString(OCKey.TEXT.value()).replaceAll("\\p{Cc}", " "));
		            break;
            	}
            	case INSERT_TAB : {
            		insertTab(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.optJSONObject(OCKey.ATTRS.value()));
            		break;
            	}
            	case INSERT_HARD_BREAK : {
            		insertHardBreak(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), /* op.optString(OCKey.TYPE.shortName()), */ op.optJSONObject(OCKey.ATTRS.value()));
            		break;
            	}
            	case INSERT_FIELD : {
            		insertField(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.getString(OCKey.TYPE.value()), op.getString(OCKey.REPRESENTATION.value()), op.optJSONObject(OCKey.ATTRS.value()));
            		break;
            	}
            	case DELETE : {
                    CUtil.delete(content.getRootComponent(opsDoc, op.optString(OCKey.TARGET.value())), op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()));
            		break;
            	}
            	case SET_ATTRIBUTES : {
            		setAttributes(op.getJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.TARGET.value()), op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()));
            		break;
            	}
            	case INSERT_PARAGRAPH : {
                	insertParagraph(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.optJSONObject(OCKey.ATTRS.value()));
                	break;
            	}
            	case SPLIT_PARAGRAPH : {
            		splitParagraph(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()));
            		break;
            	}
            	case MERGE_PARAGRAPH : {
            		mergeParagraph(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()));
            		break;
            	}
            	case MOVE : {
            		move(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.optJSONArray(OCKey.END.value()), op.getJSONArray(OCKey.TO.value()));
            		break;
            	}
            	case INSERT_DRAWING : {
            		insertDrawing(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.getString(OCKey.TYPE.value()), op.optJSONObject(OCKey.ATTRS.value()));
            		break;
            	}
            	case INSERT_TABLE : {
            		insertTable(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.optJSONObject(OCKey.ATTRS.value()));
            		break;
            	}
                case SPLIT_TABLE : {
                    splitTable(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()));
                    break;
                }
                case MERGE_TABLE : {
                    mergeTable(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()));
                    break;
                }
            	case INSERT_ROWS : {
            		insertRows(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.optInt(OCKey.COUNT.value(), 1), op.optBoolean(OCKey.INSERT_DEFAULT_CELLS.value(), false), op.optInt(OCKey.REFERENCE_ROW.value(), -1), op.optJSONObject(OCKey.ATTRS.value()));
            		break;
            	}
            	case INSERT_CELLS : {
            		insertCells(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.optInt(OCKey.COUNT.value(), 1), op.optJSONObject(OCKey.ATTRS.value()));
            		break;
            	}
            	case INSERT_COLUMN : {
            		insertColumn(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.getJSONArray(OCKey.TABLE_GRID.value()), op.getInt(OCKey.GRID_POSITION.value()), op.optString(OCKey.INSERT_MODE.value(), "before"));
            		break;
            	}
            	case DELETE_COLUMNS : {
            		deleteColumns(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.getInt(OCKey.START_GRID.value()), op.optInt(OCKey.END_GRID.value(), op.getInt(OCKey.START_GRID.value())));
            		break;
            	}
            	case INSERT_LIST_STYLE : {
            		insertListStyle(op.getString(OCKey.LIST_STYLE_ID.value()), op.getJSONObject(OCKey.LIST_DEFINITION.value()));
            		break;
            	}
                case CHANGE_COMMENT : {
                    changeComment(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.optString(OCKey.TEXT.value()), op.optJSONArray(OCKey.MENTIONS.value()), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
            	case INSERT_COMMENT : {
            		insertComment(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.getString(OCKey.TEXT.value()), op.optJSONArray(OCKey.MENTIONS.value()),
            		    op.getString(OCKey.ID.value()), op.optString(OCKey.PROVIDER_ID.value()), op.optString(OCKey.USER_ID.value()), op.optString(OCKey.AUTHOR.value()), op.optString(OCKey.DATE.value()), op.optJSONObject(OCKey.ATTRS.value()));
            		break;
            	}
            	case INSERT_RANGE : {
            		insertRange(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value()), op.getString(OCKey.ID.value()), op.optString(OCKey.TYPE.value(), "comment"), op.optString(OCKey.POSITION.value(), "start"), op.optJSONObject(OCKey.ATTRS.value()));
            		break;
            	}
            	case INSERT_HEADER_FOOTER : {
            		insertHeaderFooter(op.getString(OCKey.ID.value()), op.getString(OCKey.TYPE.value()));
            		break;
            	}
            	case DELETE_HEADER_FOOTER : {
            		deleteHeaderFooter(op.getString(OCKey.ID.value()));
            		break;
            	}
                case INSERT_STYLE_SHEET : {
                    final String styleId = op.getString(OCKey.STYLE_ID.value());
                    final String type = op.getString(OCKey.TYPE.value());
                    JSONObject attrs = op.optJSONObject(OCKey.ATTRS.value());
                    if(type.equals("table")) {
                        if(attrs!=null) {
                            if(attrs.hasAndNotNull(OCKey.WHOLE_TABLE.value())) {
                                attrs = attrs.optJSONObject(OCKey.WHOLE_TABLE.value());
                            }
                        }
                    }
                    styleManager.insertStyleSheet(type, styleId, op.optString(OCKey.STYLE_NAME.value()), attrs, op.optString(OCKey.PARENT.value()), op.optBoolean(OCKey.DEFAULT.value()));
                    if(type.equals("table")&&attrs!=null) {
                        styleManager.insertStyleSheet("cell", styleId + ".A1", op.optString(OCKey.STYLE_NAME.value()), attrs, op.optString(OCKey.PARENT.value()), op.optBoolean(OCKey.DEFAULT.value()));
                    }
                    break;
                }
                case CHANGE_STYLE_SHEET : {
                    styleManager.changeStyleSheet(op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()), op.optString(OCKey.STYLE_NAME.value()), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.PARENT.value()));
                    break;
                }
                case SET_DOCUMENT_ATTRIBUTES : {
                	setDocumentAttributes(op.getJSONObject(OCKey.ATTRS.value()));
                	break;
                }
                case INSERT_FONT_DESCRIPTION : {
	                insertFontDescription(op.getString(OCKey.FONT_NAME.value()), op.getJSONObject(OCKey.ATTRS.value()));
	                break;
                }
                case DELETE_LIST_STYLE :
                    // PASSTHROUGH INTENDED
            	case NO_OP : {
            		break;
            	}
                case CREATE_ERROR : {
                    throw new FilterException("createError operation detected: " + opName, ErrorCode.UNSUPPORTED_OPERATION_USED);
                }
                default: {
                    final String unsupportedOps = opName==OCValue.UNKNOWN_VALUE ? op.getString(OCKey.NAME.value()) : opName.value(true);
                    LOG.warn("Ignoring unsupported operation: " + unsupportedOps);
                }
            }
        }
    	final Body body = content.getBody();
    	final String masterPageName = body.getMasterPageName(false);
    	if(masterPageName!=null) {
    	    String currentMasterPageName = null;
            final Paragraph paragraph = ParagraphComponent.getFirstParagraph(new RootComponent(opsDoc, body, true), true);
            final String firstParaStyle = paragraph.getStyleName();
            if(firstParaStyle!=null&&!firstParaStyle.isEmpty()) {
                final StyleBase paraStyle = styleManager.getStyle(firstParaStyle, StyleFamily.PARAGRAPH, true);
                if(paraStyle!=null) {
                    currentMasterPageName = paraStyle.getAttribute("style:master-page-name");
                }
            }
            if(!masterPageName.equals(currentMasterPageName)) {
                final JSONObject attrs = new JSONObject(1);
                attrs.put("masterPageName", masterPageName);
                paragraph.setStyleName(styleManager.createStyle(StyleFamily.PARAGRAPH, paragraph.getStyleName(), true, attrs));
            }
    	}
        Transformer.prepareSave(opsDoc);
        doc.getPackage().setSuccessfulAppliedOperations(operations.length());
        return 1;
    }

    public void createHeaderFooterTargetNodes(String masterPageName) {
        final MasterStyles masterStyles = styleManager.getMasterStyles();
        final Iterator<Object> styleMasterPageIter = masterStyles.getContent().iterator();
        while(styleMasterPageIter.hasNext()) {
            final Object o = styleMasterPageIter.next();
            if(o instanceof StyleMasterPage) {
                if(((StyleMasterPage)o).getName().equals(masterPageName)) {
                    for(StyleHeaderFooter styleHeaderFooter:((StyleMasterPage)o).getStyleHeaderFooters()) {
                        content.getTargetNodes().put(styleHeaderFooter.getId(), styleHeaderFooter);
                    }
                    return;
                }
            }
        }
    }

    public void insertParagraph(JSONArray start, String target, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
    		.insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.PARAGRAPH);
    }

    public void splitParagraph(JSONArray start, String target)
        throws JSONException {

        ((IParagraph)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1))
        	.splitParagraph(start.getInt(start.length()-1));
    }

    public void mergeParagraph(JSONArray start, String target) {

    	((IParagraph)content.getRootComponent(opsDoc, target).getComponent(start, start.length()))
    		.mergeParagraph();
    }

    public void insertText(JSONArray start, String target, JSONObject attrs, String text) throws Exception {

    	if(text.length()>0) {
    		final IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1);
    		((IParagraph)component).insertText(start.getInt(start.length()-1), text, attrs);
    	}
    }

    public void insertTab(JSONArray start, String target, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
    	    .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.TAB);
    }

    public void insertHardBreak(JSONArray start, String target, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
			.insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.HARDBREAK);
    }

    public void splitTable(JSONArray start, String target) throws Exception {

        final TableComponent splitTableComponent = (TableComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-2)
            .insertChildComponent( start.getInt(start.length()-2) + 1, null, ComponentType.TABLE);

        ((TableComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1))
            .splitTable(start.getInt(start.length()-1), splitTableComponent);
    }

    public void mergeTable(JSONArray start, String target) {
        ((TableComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length())).mergeTable();
    }

    public void insertField(JSONArray start, String target, String type, String representation, JSONObject attrs) throws Exception {

    	final TextField textField = (TextField)(content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
			.insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.FIELD)).getObject();

        if(TextField.TYPES.contains(type)) {
            textField.setType(type);
            if(type.equals("creator")) {
                textField.getAttributes().setBooleanValue(Namespaces.TEXT, "fixed", "text:fixed", true);
            }
        }
    	textField.setRepresentation(representation);
    }

    public void setAttributes(JSONObject attrs, String target, JSONArray start, JSONArray end) throws Exception {

    	if(attrs==null) {
    		return;
    	}
        int startIndex = start.getInt(start.length()-1);
        int endIndex = startIndex;

        if(end!=null) {
            if(end.length()!=start.length())
                return;
            endIndex = end.getInt(end.length()-1);
        }
        IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, target).getComponent(start, start.length());
		component.splitStart(startIndex, SplitMode.DELETE);
        while(component!=null&&component.getComponentNumber()<=endIndex) {
        	if(component.getNextComponentNumber()>=endIndex+1) {
        		component.splitEnd(endIndex, SplitMode.DELETE);
        	}
        	component.applyAttrsFromJSON(attrs);
            component = component.getNextComponent();
        }
    }

    public void move(JSONArray start, String target, JSONArray end, JSONArray to)
    	throws JSONException {

        OdfComponent.move(content.getRootComponent(opsDoc, target), start, end, to);
    }

    public void insertDrawing(JSONArray start, String target, String type, JSONObject attrs) throws Exception {

    	ComponentType childComponentType = ComponentType.AC_IMAGE;
    	if(type.equals("shape")) {
    		if(attrs==null||!attrs.has(OCKey.GEOMETRY.value())) {
    			childComponentType = ComponentType.AC_FRAME;
    		}
    		else {
    			childComponentType = ComponentType.AC_SHAPE;
    		}
    	}
    	else if(type.equals("connector")) {
    	    childComponentType = ComponentType.AC_CONNECTOR;
    	}
    	else if(type.equals("group")) {
    	    childComponentType = ComponentType.AC_GROUP;
    	}
    	else if(type.equals("image")) {
    	    childComponentType = ComponentType.AC_IMAGE;
    	}
    	content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
    		.insertChildComponent(start.getInt(start.length()-1), attrs, childComponentType);
    }

    public void insertTable(JSONArray start, String target, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
    		.insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.TABLE);
    }

    public void insertRows(JSONArray start, String target, int count, boolean insertDefaultCells, int referenceRow, JSONObject attrs) throws Exception {

    	((TableComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1))
    		.insertRows(start.getInt(start.length()-1), count, insertDefaultCells, referenceRow, attrs);
    }

    public void insertCells(JSONArray start, String target, int count, JSONObject attrs) throws Exception {

    	((RowComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1))
    		.insertCells(start.getInt(start.length()-1), count, attrs);
    }

    public void insertColumn(JSONArray start, String target, JSONArray tableGrid, int gridPosition, String insertMode)
    	throws JSONException {

    	((TableComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length()))
    		.insertColumn(tableGrid, gridPosition, insertMode);
    }

    public void deleteColumns(JSONArray position, String target, int gridStart, int gridEnd) {

    	((TableComponent)content.getRootComponent(opsDoc, target).getComponent(position, position.length()))
    		.deleteColumns(gridStart, gridEnd);
    }

    public void insertListStyle(String listStyleId, JSONObject listDefinition) {
    	// TODO: listStyle is only created in contentAutoStyles... have to clone them for header/footer usage
    	final TextListStyle textListStyle = new TextListStyle(listStyleId, true, true);
    	styleManager.addStyle(textListStyle);
    	textListStyle.applyAttrs(styleManager, listDefinition);
    }

    public void changeComment(JSONArray start, String target, String text, JSONArray mentions, JSONObject attrs)
        throws Exception {

        final IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, target).getComponent(start, start.length());
        final Annotation annotation = (Annotation)component.getObject();
        if(text!=null) {
            annotation.getContent().clear();
            applyCommentText((AnnotationComponent)component, text);
        }
        if(mentions!=null) {
            annotation.setMentions(mentions);
        }
        if(attrs!=null) {
            component.applyAttrsFromJSON(attrs);
        }
    }

    public void insertComment(JSONArray start, String target, String text, JSONArray mentions, String id, String providerId, String uid, String author, String date, JSONObject attrs)
        throws Exception {

    	final IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
    		.insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.COMMENT_REFERENCE);

    	final Annotation annotation = (Annotation)component.getObject();
    	annotation.setId(id);
    	annotation.setCreator(author);
    	annotation.setDate(date);
    	annotation.setMentions(mentions);
    	annotation.setProviderId(providerId);
	    annotation.setUserId(uid);
    	content.getDocument().getTargetNodes().put(id, annotation);
    	applyCommentText((AnnotationComponent)component, text);
    	if(attrs!=null) {
    	    component.applyAttrsFromJSON(attrs);
    	}
    }

    private void applyCommentText(AnnotationComponent annotationComponent, String text)
        throws Exception {

        final Annotation annotation = (Annotation)annotationComponent.getObject();
        final String[] paragraphs = text.split("\\n", -1);
        for(int i=0; i < paragraphs.length; i++) {
            ((ParagraphComponent)content.getRootComponent(opsDoc, annotation.getId()).insertChildComponent(i, new JSONObject(), ComponentType.PARAGRAPH)).insertText(0, paragraphs[i], new JSONObject());
        }
    }

    public void insertRange(JSONArray start, String target, String id, String type, String position, JSONObject attrs) throws Exception {

    	if(type.equals("comment")&&position.equals("end")) {
	    	final IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
	    		.insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.COMMENT_RANGE_END);

	    	((AnnotationEnd)component.getObject()).setId(id);
    	}
    }

    public void insertHeaderFooter(String id, String type) {

        final String masterPageName = content.getBody().getMasterPageName(true);
    	final DLList<Object> styleMasterPages = styleManager.getMasterStyles().getContent();
    	StyleMasterPage standardMasterPage = null;
    	final Iterator<Object> styleMasterPageIter = styleMasterPages.iterator();
    	while(styleMasterPageIter.hasNext()) {
    		final Object o = styleMasterPageIter.next();
    		if(o instanceof StyleMasterPage) {
        		if(((StyleMasterPage)o).getName().equals(masterPageName)) {
        			standardMasterPage = (StyleMasterPage)o;
        			break;
        		}
        	}
    	}
    	if(standardMasterPage==null) {
    		standardMasterPage = new StyleMasterPage(masterPageName, "");
    		styleMasterPages.add(standardMasterPage);
    	}
    	final StyleHeaderFooter styleHeaderFooter = new StyleHeaderFooter(id, type);
    	standardMasterPage.getStyleHeaderFooters().add(styleHeaderFooter);
    	content.getTargetNodes().put(id, styleHeaderFooter);
    }

    public void deleteHeaderFooter(String id) {
    	content.getTargetNodes().remove(id);
    	final DLList<Object> styleMasterPages = styleManager.getMasterStyles().getContent();
    	final Iterator<Object> styleMasterPageIter = styleMasterPages.iterator();
		while(styleMasterPageIter.hasNext()) {
			final Object o = styleMasterPageIter.next();
			if(o instanceof StyleMasterPage) {
				final DLList<StyleHeaderFooter> styleHeaderFooters = ((StyleMasterPage)o).getStyleHeaderFooters();
				for(StyleHeaderFooter styleHeaderFooter:styleHeaderFooters) {
					if(styleHeaderFooter.getId().equals(id)) {
						styleHeaderFooters.remove(styleHeaderFooter);
						return;
					}
				}
			}
		}
    }

    public void setDocumentAttributes(JSONObject attrs)
    	throws JSONException {

    	final MasterStyles masterStyles = styleManager.getMasterStyles();
    	final Iterator<Object> masterStyleContentIter = masterStyles.getContent().iterator();
    	StyleMasterPage standardMasterPage = null;
    	final String standardMasterPageName = content.getBody().getMasterPageName(true);
    	while(masterStyleContentIter.hasNext()) {
    		final Object c = masterStyleContentIter.next();
    		if(c instanceof StyleMasterPage) {
    			if(standardMasterPageName.equals(((StyleMasterPage)c).getName())) {
    				standardMasterPage = (StyleMasterPage)c;
    				break;
    			}
    		}
    	}
    	if(standardMasterPage!=null) {
    		String pageLayoutName = standardMasterPage.getPageLayoutName();
    		if(pageLayoutName==null||pageLayoutName.isEmpty()) {
    			pageLayoutName = styleManager.getUniqueStyleName(StyleFamily.PAGE_LAYOUT, false);
        		styleManager.insertAutoStyle("page", pageLayoutName, attrs, false);
        		standardMasterPage.setPageLayoutName(pageLayoutName);
    		}
    		else {
    			final StyleBase styleBase = styleManager.getStyle(pageLayoutName, StyleFamily.PAGE_LAYOUT, false);
    			if(styleBase instanceof StylePageLayout) {
    				((StylePageLayout)styleBase).applyAttrs(styleManager, attrs);
    			}
    		}
    	}
    	else {
    		String pageLayoutName = styleManager.getUniqueStyleName(StyleFamily.PAGE_LAYOUT, false);
    		styleManager.insertAutoStyle("page", pageLayoutName, attrs, false);
    		standardMasterPage = new StyleMasterPage(standardMasterPageName, pageLayoutName);
    		styleManager.addStyle(standardMasterPage);
    	}
    }

    public void insertFontDescription(String fontName, JSONObject attrs) {

    	if(!fontName.isEmpty()) {
	    	final JSONArray panose1 = attrs.optJSONArray(OCKey.PANOSE1.value());
	    	String panose1Value = panose1!=null ? panose1.toString() : null;
	    	final String[] altNames = (String[]) attrs.opt(OCKey.ALT_NAMES.value());
	        final String family = attrs.optString(OCKey.FAMILY.value());
	        final String familyGeneric = attrs.optString(OCKey.FAMILY_GENERIC.value());
	        final String pitch = attrs.optString(OCKey.PITCH.value());
	        styleManager.insertFontDescription(fontName, altNames, family, familyGeneric, pitch, panose1Value);
    	}
    }
}
