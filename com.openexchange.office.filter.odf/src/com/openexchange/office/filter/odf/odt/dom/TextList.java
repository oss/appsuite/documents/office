/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import java.util.HashMap;
import java.util.Random;
import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class TextList implements Cloneable {

	private TextListItem parentTextListItem;
	private int listLevel;

	// value or null
	private String id;
	private String styleName;
	private String continueList;
	private Boolean continueNumbering;

	final private StyleManager styleManager;

	public TextList(StyleManager styleManager, TextListItem parentTextListItem) {
        this.styleManager = styleManager;
	    init(parentTextListItem);
	}

	public TextList(StyleManager styleManager, TextListItem parentTextListItem, Attributes attributes) {
	    this.styleManager = styleManager;
	    init(parentTextListItem);

	    id = attributes.getValue("xml:id");
        styleName = attributes.getValue("text:style-name");
        continueList = attributes.getValue("text:continue-list");
        final String contNumbering = attributes.getValue("text:continue-numbering");
        if(contNumbering!=null) {
            continueNumbering = Boolean.parseBoolean(contNumbering);
        }
	}

	private void init(TextListItem parent) {
        this.parentTextListItem = parent;
        if(parentTextListItem==null) {
            applyUniqueId();
            listLevel = 0;
        }
        else {
            setId(null);
            listLevel = parentTextListItem.getParentTextList().getListLevel() + 1;
        }
	}

	public void writeAttributes(SerializationHandler output)
		throws SAXException {

	    final HashMap<String, String> usedTextListStyles = styleManager.getListItemStyleIds();
	    final String lastUsedId = usedTextListStyles.get(styleName);

	    SaxContextHandler.addAttribute(output, Namespaces.XML, "id", "xml:id", id);
		SaxContextHandler.addAttribute(output, Namespaces.TEXT, "style-name", "text:style-name", styleName);
        if(lastUsedId!=null&&!lastUsedId.isEmpty()) {
            SaxContextHandler.addAttribute(output, Namespaces.TEXT, "continue-list", "text:continue-list", lastUsedId);
        }
		if(continueNumbering!=null) {
		    SaxContextHandler.addAttribute(output, Namespaces.TEXT, "continue-numbering", "text:continue-numbering", continueNumbering.toString());
		}
		usedTextListStyles.put(styleName, id);
	}

	public TextListItem getParentTextListItem() {
		return parentTextListItem;
	}

    public TextList getTextListRoot() {
        TextList textListRoot = this;
        while(textListRoot.getParentTextListItem()!=null&&textListRoot.getParentTextListItem().getParentTextList()!=null) {
            textListRoot = textListRoot.getParentTextListItem().getParentTextList();
        }
        return textListRoot;
    }

    public int getListLevel() {
		return listLevel;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
	    this.id = id;
	}

	private void applyUniqueId() {
        this.setId("list" + Integer.toString(StyleManager.getRandom(), 0));
	}

	public String getStyleName(boolean deepSearch) {
	    if(!deepSearch) {
	        return styleName;
	    }
	    TextList textList = this;
	    while(true) {
	        if(textList.styleName!=null&&!textList.styleName.isEmpty()) {
	            return textList.styleName;
	        }
	        if(textList.getParentTextListItem()==null) {
	            return null;
	        }
	        textList = textList.getParentTextListItem().getParentTextList();
	    }
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	public String getContinueList() {
		return continueList;
	}

	public void setContinueList(String continueList) {
	    this.continueList = continueList;
	}

	public Boolean getContinueNumbering() {
		return continueNumbering;
	}

	public void setContinueNumbering(Boolean continueNumbering) {
	    this.continueNumbering = continueNumbering;
	}

    @Override
    public TextList clone() {
        try {
            final TextList clone = (TextList)super.clone();
            if(clone.id!=null) {
                applyUniqueId();
            }
            if(parentTextListItem!=null) {
                parentTextListItem = parentTextListItem.clone();
            }
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
