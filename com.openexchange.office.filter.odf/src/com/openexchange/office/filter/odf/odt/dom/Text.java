/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class Text implements IElementWriter {

	private String text = "";

	public Text(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public void writeObject(SerializationHandler output) throws SAXException {

		int index = 0;
		while(index<text.length()) {

			if(isSpace(text.charAt(index))) {
				int count = countSpaces(text, index);
				if(index!=0&&count==1) {
					output.characters(" ");
				}
				else {
					output.startElement(Namespaces.TEXT, "s", "text:s");
					if(count>1) {
						SaxContextHandler.addAttribute(output, Namespaces.TEXT, "c", "text:c", Integer.toString(count));
					}
					output.endElement(Namespaces.TEXT, "s", "text:s");
				}
				index += count;
			}
			else {
				int count = countNonSpaces(text, index);
				final String s = text.substring(index, index + count);
				output.characters(s);
				index += count;
			}
		}
	}

	public static boolean isSpace(char c) {
	    return c==' '||c==0x9||c==0xa||c==0xd;
	}

	public static int countSpaces(String text, int index) {
		int count = 0;
		while(index + count < text.length()) {
		    if(!isSpace(text.charAt(index+count))) {
				break;
			}
			count++;
		}
		return count;
	}

	public static int countNonSpaces(String text, int index) {
		int count = 0;
		while(index + count < text.length()) {
		    if(isSpace(text.charAt(index + count))) {
				break;
			}
			count++;
		}
		return count;
	}
}
