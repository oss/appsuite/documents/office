/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class TextSpan implements IElementWriter, INodeAccessor, Cloneable {

	private DLList<Object> content;

	// attributes
	private String classNames;
	private String styleName;
	private Hyperlink hyperlink;

	public TextSpan() {
		this.classNames = null;
		this.styleName = null;
		this.hyperlink = null;
	}

	public TextSpan(Attributes attributes, Hyperlink hyperlink, String styleName) {
		if(attributes!=null) {
			classNames = attributes.getValue("text:class-names");
		}
		else {
			classNames = null;
		}
		this.styleName = styleName;
		this.hyperlink = hyperlink!=null ? hyperlink.clone() : null;
	}

	@Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
		return content;
	}

	public void setContent(DLList<Object> content) {
		this.content = content;
	}

	public String getStyleName() {
		if(styleName==null&&hyperlink!=null) {
			return hyperlink.getStyleName();
		}
		return styleName;
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	public Hyperlink getHyperlink(String nmsp, boolean forceCreate) {
		if(hyperlink==null&&forceCreate) {
			hyperlink = new Hyperlink(nmsp);
			hyperlink.setStyleName(styleName);
		}
		return hyperlink;
	}

	public void setHyperlink(Hyperlink hyperlink) {
		this.hyperlink = hyperlink;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		if(!getContent().isEmpty()) {
			if(hyperlink!=null) {
				hyperlink.writeStartElement(output);
				hyperlink.writeAttributes(output);
				if(styleName!=hyperlink.getStyleName()||classNames!=null) {
					writeSpan(output);
				}
				else {
					writeChilds(output);
				}
				hyperlink.writeEndElement(output);
			}
			else {
				writeSpan(output);
			}
		}
	}

	private void writeSpan(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.TEXT, "span", "text:span");
		SaxContextHandler.addAttribute(output, Namespaces.TEXT, "class-names", "text:class-names", classNames);
		SaxContextHandler.addAttribute(output, Namespaces.TEXT, "style-name", "text:style-name", styleName);
		writeChilds(output);
		SaxContextHandler.endElement(output, Namespaces.TEXT, "span", "text:span");
	}

	private void writeChilds(SerializationHandler output)
		throws SAXException {

		for(Object child:getContent()) {
			if(child instanceof IElementWriter) {
				((IElementWriter)child).writeObject(output);
			}
		}
	}

	@Override
    public TextSpan clone() {
		final TextSpan textSpan = new TextSpan();
		textSpan.classNames = classNames;
		textSpan.styleName = styleName;
		textSpan.hyperlink = hyperlink != null ? hyperlink.clone() : null;
		return textSpan;
	}
}
