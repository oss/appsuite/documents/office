/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.json.JSONArray;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;

public class AnnotationHandler extends SaxContextHandler {

	final private Annotation annotation;

	public AnnotationHandler(SaxContextHandler parentContext, Annotation annotation) {
    	super(parentContext);

    	this.annotation = annotation;
	}

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("dc:creator")) {
    		return this;
    	}
    	else if(qName.equals("loext:sender-initials")) {
    	    return this;
    	}
    	else if(qName.equals("dc:date")) {
    		return this;
    	}
    	else if(qName.equals("oxext:mentions")) {
    	    final JSONArray mentions = new JSONArray();
    	    annotation.setMentions(mentions);
            return new MentionsHandler(this, mentions);
        }
    	final SaxContextHandler textHandler = TextContentHelper.startElement(this, annotation, attributes, uri, localName, qName);
    	if(textHandler!=null) {
    		return textHandler;
    	}
		final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
		annotation.getContent().add(element);
		return new UnknownContentHandler(this, element);
    }

	@Override
	public void endContext(String qName, String characters) {
		super.endContext(qName, characters);

		if(qName.equals("dc:creator")) {
			annotation.setCreator(characters);
		}
        else if(qName.equals("loext:sender-initials")) {
            annotation.setInitials(characters);
        }
		else if(qName.equals("dc:date")) {
			annotation.setDate(characters);
		}
	}
}
