/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom.components;

import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.ITable;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.components.TextSpan_Base;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;
import com.openexchange.office.filter.odf.odt.dom.TextSpan;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.table.Cell;
import com.openexchange.office.filter.odf.table.Column;
import com.openexchange.office.filter.odf.table.Row;
import com.openexchange.office.filter.odf.table.Table;

public class TableComponent extends OdfComponent implements ITable {

	final Table table;

	public TableComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> tableNode, int componentNumber) {
		super(parentContext, tableNode, componentNumber);
		this.table = (Table)getObject();
	}

	@Override
	public String simpleName() {
	    return "Table";
	}

	@Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {

        final int nextComponentNumber = previousChildComponent != null ? previousChildComponent.getNextComponentNumber() : 0;
        DLNode<Object> nextNode = previousChildContext != null ? previousChildContext.getNode().getNext() : ((Table)getObject()).getContent().getFirstNode();
		while(nextNode!=null) {
			if(nextNode.getData() instanceof Row) {
				return new RowComponent(this, nextNode, nextComponentNumber);
			}
			nextNode = nextNode.getNext();
		}
		return null;
	}

	@Override
	public void applyAttrsFromJSON(JSONObject attrs)
		throws JSONException {

	    table.applyAttrsFromJSON(operationDocument, attrs, contentAutoStyle);
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {

	    table.createAttrs(operationDocument, attrs, contentAutoStyle);
	}

	@Override
    public Table getTable() {
	    return table;
	}

	@Override
    public void insertRows(int rowPosition, int count, boolean insertDefaultCells, int referenceRowNumber, JSONObject attrs) throws Exception {

		final RowComponent referenceRowComponent = referenceRowNumber!=-1 ? (RowComponent)getChildComponent(referenceRowNumber) : null;
        final DLList<Object> tblContent = table.getContent();
        final IComponent<OdfOperationDoc> oldTr = getChildComponent(rowPosition);
        DLNode<Object> referenceNode = oldTr!=null?oldTr.getNode():null;

        // determining default cell style if inserting default cells
        String tableCellStyle = null;
        if(insertDefaultCells) {
            tableCellStyle = operationDocument.getDocument().getStyleManager().getCellStyleFromTableStyle(table.getStyleName(), isContentAutoStyle(), true);
        }

        for(int i = 0; i<count; i++) {
        	final Row row = !insertDefaultCells && referenceRowComponent != null ? ((Row)referenceRowComponent.getObject()).clone() : new Row();
            final DLNode<Object> rowNode = new DLNode<Object>(row);
            tblContent.addNode(referenceNode, rowNode, true);
            referenceNode = rowNode;
            if(insertDefaultCells) {
                for(int j = 0; j < table.getColumns().size(); j++) {
                	final Cell cell = new Cell();
            	    cell.setStyleName(tableCellStyle);
                	cell.getContent().add(new Paragraph(null));
                	row.getContent().add(cell);
                }
            }
            else if(referenceRowComponent!=null) {       // we try to create the new row from the referenceRow
                CellComponent referenceCellComponent = (CellComponent)referenceRowComponent.getNextChildComponent(null, null);
                while(referenceCellComponent!=null) {
                	final Cell cell = ((Cell)referenceCellComponent.getObject()).clone();
                	final Paragraph p = new Paragraph(null);
                	cell.getContent().add(p);
                	row.getContent().add(cell);
                	applyReferenceCellAttributes(p, referenceCellComponent);
                    referenceCellComponent = (CellComponent)referenceCellComponent.getNextComponent();
                }
            }
        }
        if(attrs!=null) {
            IComponent<OdfOperationDoc> c = getNextChildComponent(null, null).getComponent(rowPosition);
	        for(int i=0; i<count; i++) {
	        	c.applyAttrsFromJSON(attrs);
	        	c = c.getNextComponent();
	        }
        }
	}

	private void applyReferenceCellAttributes(Paragraph p, CellComponent referenceCellComponent) {
	    if(referenceCellComponent!=null) {
	        final ParagraphComponent referenceParagraphComponent = referenceCellComponent.getFirstParagraphChildComponent();
	        if(referenceParagraphComponent!=null) {
	            final Paragraph referenceP = (Paragraph)referenceParagraphComponent.getObject();
	            p.setStyleName(referenceP.getStyleName());
                p.applyTextListItem(referenceP);
	            final OdfComponent c = (OdfComponent)referenceParagraphComponent.getNextChildComponent(null, null);
	            if(c instanceof TextSpan_Base) {
	                final TextSpan textSpan = ((TextSpan_Base)c).getTextSpan();
	                if(textSpan!=null) {
	                    final OpAttrs characterAttrs = new OpAttrs();
	                    try {
                            c.createJSONAttrs(characterAttrs);
                        } catch (SAXException e) {
                            //
                        }
	                    try {
                            p.setStyleName(operationDocument.getDocument().getStyleManager().createStyle(StyleFamily.PARAGRAPH, p.getStyleName(), isContentAutoStyle(), new JSONObject(characterAttrs)));
                        } catch (JSONException e) {
                            //
                        }
	                }
	            }
	        }
	    }
	}

	@Override
    public void insertColumn(JSONArray tableGrid, int gridPosition, String insertMode)
		throws JSONException {

		boolean before = insertMode.equals("before");
		RowComponent trComponent = (RowComponent)getNextChildComponent(null, null);
        while(trComponent!=null) {
        	CellComponent tcReference = (CellComponent)trComponent.getNextChildComponent(null, null);
        	CellComponent tcReferenceLast = null;
        	CellComponent destination = null;
            while(tcReference!=null) {
                tcReferenceLast = tcReference;
                if(gridPosition>=tcReference.getGridPosition()&&gridPosition<tcReference.getNextGridPosition()) {
                    destination = tcReference;
                    break;
                }
                tcReference = (CellComponent)tcReference.getNextComponent();
            }
            final Cell tc = tcReferenceLast!=null ? tcReferenceLast.getCell().clone() : new Cell();
            final Paragraph p = new Paragraph(null);
            tc.setColumnSpan(1);
            tc.getContent().add(p);
            applyReferenceCellAttributes(p, tcReferenceLast);
            final DLList<Object> rowContent = trComponent.getRow().getContent();
            if(destination==null) {
            	rowContent.add(tc);
            }
            else {
            	final ComponentContext<OdfOperationDoc> contextChild = destination.getContextChild(null);
            	rowContent.addNode(contextChild.getNode(), new DLNode<Object>(tc), before);
            }
            trComponent = (RowComponent)trComponent.getNextComponent();
        }
        table.applyTableGrid(operationDocument, null, tableGrid, isContentAutoStyle());
	}

    @Override
    public void deleteColumns(int gridStart, int gridEnd) {

        IComponent<OdfOperationDoc> trComponent = getNextChildComponent(null, null);
        while(trComponent!=null) {
            final DLNode<Object> trNode = trComponent.getNode();
            final DLList<Object> trContent = ((Row)trNode.getData()).getContent();
            DLNode<Object> startNode = null;
        	DLNode<Object> endNode = null;
        	CellComponent tcComponent = (CellComponent)trComponent.getNextChildComponent(null, null);
            while(tcComponent!=null) {
            	final int x1 = tcComponent.getGridPosition();
            	final int x2 = tcComponent.getNextGridPosition();
            	if(Math.max(x2, gridEnd + 1) - Math.min(x1, gridStart) < (x2 - x1) + ((gridEnd+1)-gridStart)) {
               		final ComponentContext<OdfOperationDoc> contextChild = tcComponent.getContextChild(null);
                	if (startNode==null) {
                        startNode = contextChild.getNode();
                	}
                    endNode = contextChild.getNode();
            	}
                if(tcComponent.getNextGridPosition()>gridEnd)
                    break;
                tcComponent = (CellComponent)tcComponent.getNextComponent();
            }
            if(startNode!=null) {
                trContent.removeNodes(startNode, endNode);
            }
            trComponent = trComponent.getNextComponent();
            if(trContent.isEmpty()) {
                table.getContent().removeNode(trNode);
            }
        }
        for(int i = gridEnd; i > gridStart; i--) {
        	table.getColumns().remove(i);
        }
	}

    @Override
    public void splitTable(int splitPos, ITable splitTableComponent) {
        final Table destTable = splitTableComponent.getTable();
        final DLList<Object> sourceContent = table.getContent();
        final DLList<Object> destContent = destTable.getContent();
        final DLNode<Object> splitNode = sourceContent.getNode(splitPos);

        // cloning table attributes
        destTable.setAttributes(table.getAttributes().clone());

        // moving table rows
        sourceContent.moveNodes(splitNode, destContent);

        // cloning table columns
        final Iterator<Column> columnIter = table.getColumns().iterator();
        while(columnIter.hasNext()) {
            destTable.getColumns().add(columnIter.next().clone());
        }

        // taking care of the style:master-page attribute within the style, (we have to remove it, otherwise the split table
        // begins at a new page :(
        final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
        final StyleBase styleBase = styleManager.getStyle(destTable.getStyleName(), StyleFamily.TABLE, isContentAutoStyle());
        if(styleBase!=null) {
            final String masterPage = styleBase.getAttribute("style:master-page-name");
            if(masterPage!=null&&!masterPage.isEmpty()) {
                final StyleBase clonedStyle = styleBase.clone();
                clonedStyle.getAttributes().remove("style:master-page-name");
                final String existingStyleId = styleManager.getExistingStyleIdForStyleBase(clonedStyle);
                if(existingStyleId!=null) {
                    destTable.setStyleName(existingStyleId);
                }
                else {
                    clonedStyle.setName(styleManager.getUniqueStyleName(StyleFamily.TABLE, isContentAutoStyle()));
                    destTable.setStyleName(clonedStyle.getName());
                    styleManager.addStyle(clonedStyle);
                }
            }
        }
    }

    @Override
    public void mergeTable() {
        final TableComponent nextTable = (TableComponent)getNextComponent();
        ((Table)nextTable.getObject()).getContent().moveNodes(table.getContent());
        nextTable.delete(1);
    }
}
