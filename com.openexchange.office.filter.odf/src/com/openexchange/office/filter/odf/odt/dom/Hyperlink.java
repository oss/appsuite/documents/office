/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class Hyperlink implements Cloneable {

	private final AttributesImpl attributesImpl;
	private final String nmsp;

	public Hyperlink(String nmsp) {
		attributesImpl = new AttributesImpl();
		this.nmsp = nmsp;
	};

	public Hyperlink(String nmsp, AttributesImpl attributesImpl) {
		this.attributesImpl = attributesImpl;
		this.nmsp = nmsp;
	}

	public String getStyleName() {
		return attributesImpl.getValue("text:style-name");
	}

	public void setStyleName(String styleName) {
		// style-name only available within text namespace
		if(nmsp.equals(Namespaces.TEXT)) {
			attributesImpl.setValue(Namespaces.TEXT, "style-name", "text:style-name", styleName);
		}
	}

	public String getHRef() {
		return attributesImpl.getValue("xlink:href");
	}

	public void setHRef(String hRef) {
		attributesImpl.setValue(Namespaces.XLINK, "href", "xlink:href", hRef);
	}

	public void writeAttributes(SerializationHandler output)
		throws SAXException {

		attributesImpl.write(output);
	}

	@Override
    public Hyperlink clone() {
		return new Hyperlink(nmsp, attributesImpl.clone());
	}

	public void writeStartElement(SerializationHandler output)
		throws SAXException {

		if(nmsp.equals(Namespaces.TEXT)) {
			SaxContextHandler.startElement(output, Namespaces.TEXT, "a", "text:a");
		}
		else if(nmsp.equals(Namespaces.DRAW)) {
			SaxContextHandler.startElement(output, Namespaces.DRAW, "a", "draw:a");
		}
	}

	public void writeEndElement(SerializationHandler output)
		throws SAXException {

		if(nmsp.equals(Namespaces.TEXT)) {
			SaxContextHandler.endElement(output, Namespaces.TEXT, "a", "text:a");
		}
		else if(nmsp.equals(Namespaces.DRAW)) {
			SaxContextHandler.endElement(output, Namespaces.DRAW, "a", "draw:a");
		}
	}
}
