/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class TextListItemHandler extends SaxContextHandler {

    final INodeAccessor elementWriterAccessor;
    final TextList textList;
    final boolean headerList;
    final AttributesImpl textListAttributes;
    boolean firstParagraph;

    public TextListItemHandler(SaxContextHandler parentContext, INodeAccessor elementWriterAccessor, TextList textList, boolean headerList, Attributes attributes) {
        super(parentContext);

        this.elementWriterAccessor = elementWriterAccessor;
        this.textList = textList;
        this.headerList = headerList;
        this.textListAttributes = new AttributesImpl(attributes);
        firstParagraph = true;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        final TextListItem textListItem = new TextListItem(textList, headerList, textListAttributes);
        if(!firstParagraph) {
            textListItem.setIsHeader(true);
        }
        if(qName.equals("text:number")) {
            return new TextNumberHandler(this, textListItem);
        }
        else if(qName.equals("text:list")) {
            return new TextListHandler(this, elementWriterAccessor, new TextList(getFileDom().getDocument().getStyleManager(), textListItem, attributes));
        }
        else if(qName.equals("text:p")) {
            firstParagraph = false;
            final Paragraph paragraph = new Paragraph(false, attributes, textListItem);
            elementWriterAccessor.getContent().add(paragraph);
            return new ParagraphHandler(this, paragraph);
        }
        else if(qName.equals("text:h")) {
            firstParagraph = false;
            final Paragraph paragraph = new Paragraph(true, attributes, textListItem);
            elementWriterAccessor.getContent().add(paragraph);
            return new ParagraphHandler(this, paragraph);
        }
        else if(qName.equals("text:soft-page-break")) {
            //
        }
        return this;
    }
}
