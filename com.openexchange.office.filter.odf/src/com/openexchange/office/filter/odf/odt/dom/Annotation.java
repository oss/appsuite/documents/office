/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import java.util.Map;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.draw.Transformer;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class Annotation implements IElementWriter, INodeAccessor {

	private final AttributesImpl attributes;
    protected final Transformer transformer;
	private DLList<Object> content;
	private String id;
	private String creator;
	private String initials;
	private String date;
	private boolean modifiedTransformer = false;
	private JSONArray mentions;

	// if and only if the annotation was added into the authors list (odp) then
	// this member contains the id that is used in the authors list.
	private Integer authorId;

	public Annotation(String id, Attributes attributes, boolean useTransformation) {
		this.attributes = new AttributesImpl(attributes);
		this.attributes.remove("office:name");
		this.id = id;
        this.transformer = useTransformation ? new Transformer(this.attributes, false) : null;
	}

	public Annotation(String id, boolean useTransformation) {
		this.attributes = new AttributesImpl();
		this.id = id;
		this.transformer = useTransformation ? new Transformer(false) : null;
	}

	public AttributesImpl getAttributes() {
		return attributes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInitials() {
	    return initials;
	}

	public void setInitials(String initials) {
	    this.initials = initials;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

    public String getStyleName() {
        return attributes.getValue("draw:style-name");
    }

    public void setStyleName(String style) {
        attributes.setValue(Namespaces.DRAW, "style-name", "draw:style-name", style);
    }

    public String getTextStyleName() {
        return attributes.getValue("draw:text-style-name");
    }

    public void setTextStyleName(String name) {
        attributes.setValue(Namespaces.DRAW, "text-style-name", "draw:text-style-name", name);
    }

    public Transformer getTransformer() {
        return transformer;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getProviderId() {
        return attributes.getValue("oxext:provider-id");
    }

    public void setProviderId(String providerId) {
        if(providerId!=null) {
            attributes.setValue(Namespaces.OXEXT, "provider-id", "oxext:provider-id", providerId);
        }
        else {
            attributes.remove("oxext:provider-id");
        }
    }

    public String getUserId() {
        return attributes.getValue("oxext:user-id");
    }

    public void setUserId(String userId) {
        if(userId!=null) {
            attributes.setValue(Namespaces.OXEXT, "user-id", "oxext:user-id", userId);
        }
        else {
            attributes.remove("oxext:user-id");
        }
    }

    public void setMentions(JSONArray mentions) {
        this.mentions = mentions;
    }

    public JSONArray getMentions() {
        return mentions;
    }

    @Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
		return content;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.OFFICE, "annotation", "office:annotation");
		if(id!=null&&!id.isEmpty()) {
			SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "name", "office:name", id);
		}
		if(modifiedTransformer) {
            attributes.setValue(Namespaces.SVG, "x", "svg:x", (transformer.getX() / 100.0 + "mm"));
            attributes.setValue(Namespaces.SVG, "y", "svg:y", (transformer.getY() / 100.0 + "mm"));
            attributes.setValue(Namespaces.SVG, "width", "svg:width", (transformer.getWidth() / 100.0 + "mm"));
            attributes.setValue(Namespaces.SVG, "height", "svg:height", (transformer.getHeight() / 100.0 + "mm"));
	    }
		attributes.write(output);
		if(creator!=null) {
			output.startElement(Namespaces.DC, "creator", "dc:creator");
			output.characters(creator);
			output.endElement(Namespaces.DC, "creator", "dc:creator");
		}
		if(initials!=null) {
		    output.startElement(Namespaces.LOEXT, "sender-initials", "loext:sender-initials");
		    output.characters(initials);
		    output.endElement(Namespaces.LOEXT, "sender-initials", "loext:sender-initials");
		}
		if(date!=null) {
			output.startElement(Namespaces.DC, "date", "dc:date");
			output.characters(date);
			output.endElement(Namespaces.DC, "date", "dc:date");
		}
		if(mentions!=null && !mentions.isEmpty()) {
		    SaxContextHandler.startElement(output, Namespaces.OXEXT, "mentions", "oxext:mentions");
		    for(int i = 0; i < mentions.length(); i++) {
		        final JSONObject mention = mentions.optJSONObject(i);
		        if(mention!=null) {
		            SaxContextHandler.startElement(output, Namespaces.OXEXT, "mention", "oxext:mention");
		            final int pos = mention.optInt(OCKey.POS.value(), -1);
		            final int length = mention.optInt(OCKey.LENGTH.value(), -1);
		            if(pos >= 0 && length >= 0) {
		                final AttributesImpl mentionAttrs = new AttributesImpl();
		                mentionAttrs.setIntValue(Namespaces.OXEXT, "pos", "oxext:pos", pos);
		                mentionAttrs.setIntValue(Namespaces.OXEXT, "length", "oxext:length", length);

                        final String displayName = mention.optString(OCKey.DISPLAY_NAME.value(), null);
    		            if(displayName!=null) {
    		                mentionAttrs.setValue(Namespaces.OXEXT, "display-name", "oxext:display-name", displayName);
    		            }
                        final String email = mention.optString(OCKey.EMAIL.value(), null);
                        if(email!=null) {
                            mentionAttrs.setValue(Namespaces.OXEXT, "email", "oxext:email", email);
                        }
                        final String userId = mention.optString(OCKey.USER_ID.value(), null);
                        if(userId!=null) {
                            mentionAttrs.setValue(Namespaces.OXEXT, "user-id", "oxext:user-id", userId);
                        }
                        final String providerId = mention.optString(OCKey.PROVIDER_ID.value(), null);
                        if(providerId!=null) {
                            mentionAttrs.setValue(Namespaces.OXEXT, "provider-id", "oxext:provider-id", providerId);
                        }
                        mentionAttrs.write(output);
		            }
                    SaxContextHandler.endElement(output, Namespaces.OXEXT, "mention", "oxext:mention");
		        }
		    }
		    SaxContextHandler.endElement(output, Namespaces.OXEXT, "mentions", "oxext:mentions");
		}
		TextContentHelper.write(output, getContent());
		SaxContextHandler.endElement(output, Namespaces.OFFICE, "annotation", "office:annotation");
	}

    public void createAttrs(OdfOperationDoc operationDocument, boolean contentAutoStyle, OpAttrs attrs) {
        if(getStyleName()!=null&&!getStyleName().isEmpty()) {
            operationDocument.getDocument().getStyleManager().
                collectAllGraphicPropertiesFromStyle(attrs, getStyleName(), StyleFamily.GRAPHIC, contentAutoStyle, true);
        }
        final String textStyleName = getTextStyleName();
        if(getTextStyleName()!=null&&!getTextStyleName().isEmpty()) {
            operationDocument.getDocument().getStyleManager().createAutoStyleAttributes(attrs, textStyleName, StyleFamily.PARAGRAPH, contentAutoStyle);
        }

        final OpAttrs drawingAttrs = attrs.getMap(OCKey.DRAWING.value(), true);
        drawingAttrs.put(OCKey.HIDDEN.value(), !attributes.getBooleanValue("office:display", false));
        if(transformer!=null) {
            int x = Double.valueOf(transformer.getX()).intValue();
            int y = Double.valueOf(transformer.getY()).intValue();
            int width = Double.valueOf(transformer.getWidth()).intValue();
            int height = Double.valueOf(transformer.getHeight()).intValue();
            drawingAttrs.put(OCKey.ANCHOR.value(), "a");
            drawingAttrs.put(OCKey.WIDTH.value(), width);
            drawingAttrs.put(OCKey.HEIGHT.value(), height);
            drawingAttrs.put(OCKey.LEFT.value(), x);
            drawingAttrs.put(OCKey.TOP.value(), y);

            final Double rotation = transformer.getRotation();
            if(rotation!=null) {
                drawingAttrs.put(OCKey.ROTATION.value(), rotation);
            }
        }
        if(!drawingAttrs.containsKey(OCKey.HEIGHT.value())) {
            final Map<String, Object> shapeAttrs = attrs.getMap(OCKey.SHAPE.value(), true);
            shapeAttrs.put(OCKey.AUTO_RESIZE_HEIGHT.value(), true);
        }
    }

    public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle)
        throws JSONException {

        final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
        final StyleBase styleBase = styleManager.getStyleBaseClone(StyleFamily.GRAPHIC, getStyleName(), contentAutoStyle);
        styleManager.applyAttrs(styleBase, contentAutoStyle, attrs);
        setStyleName(styleManager.getStyleIdForStyleBase(styleBase));
        final StyleBase textStyleBase = styleManager.getStyleBaseClone(StyleFamily.PARAGRAPH, getTextStyleName(), contentAutoStyle);
        styleManager.applyAttrs(textStyleBase, contentAutoStyle, attrs);
        setTextStyleName(styleManager.getStyleIdForStyleBase(textStyleBase));
        final JSONObject drawingProps = attrs.optJSONObject(OCKey.DRAWING.value());
        if(drawingProps!=null) {
            if (drawingProps.has(OCKey.NAME.value())) {
                final String name = drawingProps.optString(OCKey.NAME.value());
                if (name!=null && !name.isEmpty()) {
                    attributes.setValue(Namespaces.DRAW, "name", "draw:name", name);
                }
            }
            Object hidden = drawingProps.opt(OCKey.HIDDEN.value());
            if(hidden!=null) {
                attributes.setBooleanValue(Namespaces.OFFICE, "display", "office:display", !((Boolean)hidden).booleanValue());
            }
            if(transformer!=null) {
                if (drawingProps.has(OCKey.LEFT.value())) {
                    transformer.setX(drawingProps.optInt(OCKey.LEFT.value()));
                }
                if (drawingProps.has(OCKey.TOP.value())) {
                    transformer.setY(drawingProps.optInt(OCKey.TOP.value()));
                }
                if (drawingProps.has(OCKey.WIDTH.value())) {
                    transformer.setWidth(drawingProps.optInt(OCKey.WIDTH.value()));
                }
                if (drawingProps.has(OCKey.HEIGHT.value())) {
                    transformer.setHeight(drawingProps.optInt(OCKey.HEIGHT.value()));
                }
                final Object rotation = drawingProps.opt(OCKey.ROTATION.value());
                if(rotation!=null) {
                    transformer.setRotation(rotation instanceof Number ? ((Number)rotation).doubleValue() : null);
                }
                modifiedTransformer = true;
            }
        }
        final JSONObject shapeProps = attrs.optJSONObject(OCKey.SHAPE.value());
        if(shapeProps!=null) {
            if (shapeProps.has(OCKey.AUTO_RESIZE_HEIGHT.value()) && shapeProps.optBoolean(OCKey.AUTO_RESIZE_HEIGHT.value()) == true) {
                attributes.remove("svg:height");
            }
        }
    }
}
