/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import java.util.Stack;
import org.apache.commons.lang3.StringUtils;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.table.Table;
import com.openexchange.office.filter.odf.table.TableHandler;

public class TextContentHelper {

    public static SaxContextHandler startElement(SaxContextHandler parentHandler, INodeAccessor contentAccessor, Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("text:p")) {
    		final Paragraph paragraph = new Paragraph(false, attributes, null);
    		contentAccessor.getContent().add(paragraph);
    		return new ParagraphHandler(parentHandler, paragraph);
    	}
    	else if(qName.equals("text:h")) {
    		final Paragraph paragraph = new Paragraph(true, attributes, null);
    		contentAccessor.getContent().add(paragraph);
    		return new ParagraphHandler(parentHandler, paragraph);
    	}
    	else if(qName.equals("text:list")) {
    		return new TextListHandler(parentHandler, contentAccessor, new TextList(parentHandler.getFileDom().getDocument().getStyleManager(), null, attributes));
    	}
    	else if(qName.equals("table:table")) {
    		final Table table = new Table(((OdfDocument)parentHandler.getFileDom().getDocument()).getDocumentType() , attributes);
    		contentAccessor.getContent().add(table);
    		return new TableHandler(parentHandler, table);
    	}
    	return null;
    }

	public static void write(SerializationHandler output, DLList<Object> content)
		throws SAXException {

	    final Stack<Boolean> listHeaderStack = new Stack<Boolean>();
		TextListItem currentTextListItem = null;
		for(Object child:content) {
			if(child instanceof Paragraph) {
				currentTextListItem = checkAndUpdateTextListLevel(output, currentTextListItem, ((Paragraph)child).getTextListItem(), listHeaderStack);
				((Paragraph)child).writeObject(output);
			}
			else if (child instanceof ISectionWriter) {
				currentTextListItem = updateTextListLevel(output, currentTextListItem, null, listHeaderStack);
				((ISectionWriter)child).writeSection(output, content);
			}
			else if (child instanceof IElementWriter){
				currentTextListItem = updateTextListLevel(output, currentTextListItem, null, listHeaderStack);
				((IElementWriter)child).writeObject(output);
			}
			else if(child instanceof Element) {
			    SaxContextHandler.serializeElement(output, (Element)child);
			}
		}
		updateTextListLevel(output, currentTextListItem, null, listHeaderStack);
	}

    public static void setSimpleText(INodeAccessor textParent, String text) {
        final DLList<Object> content = textParent.getContent();
        content.clear();
        final String[] paragraphs = text.split("[\\r]*[\\n][\\r]*", -1);
        for(String p:paragraphs) {
            final Paragraph paragraph = new Paragraph(null);
            final TextSpan span = new TextSpan(null, null, null);
            span.getContent().add(new Text(p.toString()));
            paragraph.getContent().add(span);
            content.add(paragraph);
        }
    }

    public static String getSimpleText(INodeAccessor textParent) {
        if(textParent==null) {
            return "";
        }
        final StringBuffer textBuffer = new StringBuffer();
        for(Object o:textParent.getContent()) {
            if(o instanceof Paragraph) {
                if(textBuffer.length()!=0) {
                    textBuffer.append("\n");
                }
                appendParaContent(textBuffer, ((Paragraph)o).getContent());
            }
        }
        return textBuffer.toString();
    }

    public static Paragraph getFirstParagraph(INodeAccessor<?> textParent) {
        if(textParent==null) {
            return null;
        }
        final Object o = textParent.getContent().getFirst();
        return o instanceof Paragraph ? (Paragraph)o : null;
    }

    private static void appendParaContent(StringBuffer buffer, DLList<Object> childs) {
        for(Object c:childs) {
            if(c instanceof Text) {
                buffer.append(((Text)c).getText());
            }
            else if(c instanceof TextTab) {
                buffer.append("\t");
            }
            else if(c instanceof TextSpan) {
                appendParaContent(buffer, ((TextSpan)c).getContent());
            }
        }
    }

    private static TextListItem checkAndUpdateTextListLevel(SerializationHandler output, TextListItem currentTextListItem, TextListItem targetTextListItem, Stack<Boolean> listHeaderStack)
		throws SAXException {

		// check if both list items have the same parent style
		if(currentTextListItem!=null&&targetTextListItem!=null) {
			final int currentLevel = currentTextListItem.getListLevel();
			final int targetLevel = targetTextListItem.getListLevel();

			final int level = currentLevel <= targetLevel ? currentLevel : targetLevel;
			int sameParentLevel = -1;

			TextListItem currentTextListParent = currentTextListItem.getParentTextListItem(level);
			TextListItem targetTextListParent = targetTextListItem.getParentTextListItem(level);

			for(int l=level; l>=0; l--) {
				if(StringUtils.equals(currentTextListParent.getParentTextList().getStyleName(false), targetTextListParent.getParentTextList().getStyleName(false))) {
					sameParentLevel = l;
					break;
				}
				currentTextListParent = currentTextListParent.getParentTextList().getParentTextListItem();
				targetTextListParent = targetTextListParent.getParentTextList().getParentTextListItem();
			}
			if(sameParentLevel==-1) {
				currentTextListItem = updateTextListLevel(output, currentTextListItem, null, listHeaderStack);
			}
			else {
				currentTextListItem = updateTextListLevel(output, currentTextListItem, targetTextListItem.getParentTextListItem(sameParentLevel), listHeaderStack);
			}
		}
		return updateTextListLevel(output, currentTextListItem, targetTextListItem, listHeaderStack);
	}

	private static TextListItem updateTextListLevel(SerializationHandler output, TextListItem currentTextListItem, TextListItem targetTextListItem, Stack<Boolean> listHeaderStack)
		throws SAXException {

		int currentListLevel = currentTextListItem!=null ? currentTextListItem.getListLevel() : -1;
		int requiredListLevel = targetTextListItem!=null ? targetTextListItem.getListLevel() : -1;

		if(currentListLevel==requiredListLevel) {
			if(currentListLevel>-1) {
				endTextListItem(output, listHeaderStack);
				startTextListItem(output, targetTextListItem, listHeaderStack);
			}
		}
		else if(currentListLevel > requiredListLevel) {
			while(currentListLevel > requiredListLevel) {
				currentListLevel--;
				endTextListItem(output, listHeaderStack);
				endTextList(output);
				currentTextListItem = currentTextListItem.getParentTextList().getParentTextListItem();
			}
		}
		else {
			while(currentListLevel < requiredListLevel) {
				currentListLevel++;
				currentTextListItem = targetTextListItem;
				while(currentTextListItem.getListLevel()!=currentListLevel) {
					currentTextListItem = currentTextListItem.getParentTextList().getParentTextListItem();
				}
				startTextList(output, currentTextListItem.getParentTextList());
				startTextListItem(output, currentTextListItem, listHeaderStack);
			}
		}
		return targetTextListItem;
	}

	private static void startTextList(SerializationHandler output, TextList textList)
		throws SAXException {

		output.startElement(Namespaces.TEXT, "list", "text:list");
		textList.writeAttributes(output);
	}

	private static void endTextList(SerializationHandler output)
		throws SAXException {

		output.endElement(Namespaces.TEXT, "list", "text:list");
	}

	private static void startTextListItem(SerializationHandler output, TextListItem textListItem, Stack<Boolean> listHeaderStack)
		throws SAXException {

	    final boolean isHeader = textListItem.isHeader();
	    listHeaderStack.push(Boolean.valueOf(isHeader));
		if(isHeader) {
			output.startElement(Namespaces.TEXT, "list-header", "text:list-header");
		}
		else {
			output.startElement(Namespaces.TEXT, "list-item", "text:list-item");
		}
		textListItem.writeAttributes(output);
	}

	private static void endTextListItem(SerializationHandler output, Stack<Boolean> listHeaderStack)
		throws SAXException {

	    final boolean isHeader = listHeaderStack.pop();
		if(isHeader) {
			output.endElement(Namespaces.TEXT, "list-header", "text:list-header");
		}
		else {
			output.endElement(Namespaces.TEXT, "list-item", "text:list-item");
		}
	}
}
