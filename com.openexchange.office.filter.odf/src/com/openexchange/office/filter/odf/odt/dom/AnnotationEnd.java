/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class AnnotationEnd implements IElementWriter {

	private final AttributesImpl attributes;
	private String id;

	public AnnotationEnd(String id, Attributes attributes) {
		this.attributes = new AttributesImpl(attributes);
		this.attributes.remove("office:name");
		this.id = id;
	}

	public AnnotationEnd(String id) {
		this.attributes = new AttributesImpl();
		this.id = id;
	}

	public AttributesImpl getAttributes() {
		return attributes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.OFFICE, "annotation-end", "office:annotation-end");
		if(id!=null&&!id.isEmpty()) {
			SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "name", "office:name", id);
		}
		attributes.write(output);
		SaxContextHandler.endElement(output, Namespaces.OFFICE, "annotation-end", "office:annotation-end");
	}
}
