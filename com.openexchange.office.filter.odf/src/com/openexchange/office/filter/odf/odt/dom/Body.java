/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class Body extends ElementNSImpl implements IElementWriter, INodeAccessor {

	private static final long serialVersionUID = 1L;

	private DLList<Object> content;

	// attributes
	private String global;
	private String useSoftPageBreaks;
	private String masterPageName;

	public Body(OdfFileDom ownerDocument, Attributes attributes) {
		super(ownerDocument, Namespaces.OFFICE, "office:text");

		global = attributes.getValue("text:global");
		useSoftPageBreaks = attributes.getValue("text:use-soft-page-breaks");
		masterPageName = null;
	}

	@Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
		return content;
	}

	public String getMasterPageName(boolean standardIfNotAvailable) {
	    if(masterPageName!=null&&!masterPageName.isEmpty()) {
	        return masterPageName;
	    }
	    else if(standardIfNotAvailable) {
	        return "Standard";
	    }
	    return null;
	}

	public void setMasterPageName(String masterPageName) {
	    this.masterPageName = masterPageName;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.OFFICE, "text", "office:text");
		SaxContextHandler.addAttribute(output, Namespaces.TEXT, "global", "text:global", global);
		SaxContextHandler.addAttribute(output, Namespaces.TEXT, "use-soft-page-breaks", "text:use-soft-page-breaks", useSoftPageBreaks);
		TextContentHelper.write(output, getContent());
		SaxContextHandler.endElement(output, Namespaces.OFFICE, "text", "office:text");
	}
}
