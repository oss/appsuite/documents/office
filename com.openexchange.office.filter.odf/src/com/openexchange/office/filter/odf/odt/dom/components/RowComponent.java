/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom.components;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.table.Cell;
import com.openexchange.office.filter.odf.table.Row;

public class RowComponent extends OdfComponent {

	final Row row;

	public RowComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> tableNode, int componentNumber) {
		super(parentContext, tableNode, componentNumber);
		this.row = (Row)getObject();
	}

	@Override
	public String simpleName() {
	    return "Row";
	}

	public Row getRow() {
		return row;
	}

	@Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {

        final int nextComponentNumber = previousChildComponent != null ? previousChildComponent.getNextComponentNumber() : 0;
        final int nextGridPosition =  previousChildComponent instanceof CellComponent ? ((CellComponent)previousChildComponent).getNextGridPosition() : 0;
        DLNode<Object> nextNode = previousChildContext != null ? previousChildContext.getNode().getNext() : ((Row)getObject()).getContent().getFirstNode();
		while(nextNode!=null) {
			if(nextNode.getData() instanceof Cell) {
				return new CellComponent(this, nextNode, nextComponentNumber, nextGridPosition);
			}
			nextNode = nextNode.getNext();
		}
		return null;
	}

	@Override
	public void applyAttrsFromJSON(JSONObject attrs)
		throws JSONException {

		row.setStyleName(operationDocument.getDocument().getStyleManager().createStyle(StyleFamily.TABLE_ROW, row.getStyleName(), isContentAutoStyle(), attrs));
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {

		if(row.getStyleName()!=null&&!row.getStyleName().isEmpty()) {
			final OpAttrs hardRowAttrs = new OpAttrs();
			operationDocument.getDocument().getStyleManager().
				createAutoStyleAttributes(hardRowAttrs, row.getStyleName(), StyleFamily.TABLE_ROW, isContentAutoStyle());
			final Map<String, Object> rowAttrs = hardRowAttrs.getMap(OCKey.ROW.value(), false);
			if(rowAttrs!=null) {
				attrs.put(OCKey.ROW.value(), rowAttrs);
			}
		}
	}

	public void insertCells(int cellPosition, int count, JSONObject attrs) throws Exception {

	    IComponent<OdfOperationDoc> c = getChildComponent(cellPosition);
    	final DLList<Object> rowContent = row.getContent();
        for (int i=0; i<count; i++) {
            final Cell cell = new Cell();
            rowContent.addNode(c!=null?c.getNode():null, new DLNode<Object>(cell), true);
        }
        if(attrs!=null) {
            c = getChildComponent(cellPosition);
            for(int i=0; i<count; i++) {
            	c.applyAttrsFromJSON(attrs);
            	c = c.getNextComponent();
            }
        }
	}
}
