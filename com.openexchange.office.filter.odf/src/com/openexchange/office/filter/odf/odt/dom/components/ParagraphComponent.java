/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom.components;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.IParagraph;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.components.TextComponent;
import com.openexchange.office.filter.odf.components.TextSpan_Base;
import com.openexchange.office.filter.odf.draw.CustomShape;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawImage;
import com.openexchange.office.filter.odf.draw.DrawTextBox;
import com.openexchange.office.filter.odf.draw.GroupShape;
import com.openexchange.office.filter.odf.draw.LineShape;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.odt.dom.Annotation;
import com.openexchange.office.filter.odf.odt.dom.AnnotationEnd;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;
import com.openexchange.office.filter.odf.odt.dom.Text;
import com.openexchange.office.filter.odf.odt.dom.TextField;
import com.openexchange.office.filter.odf.odt.dom.TextLineBreak;
import com.openexchange.office.filter.odf.odt.dom.TextList;
import com.openexchange.office.filter.odf.odt.dom.TextListItem;
import com.openexchange.office.filter.odf.odt.dom.TextSpan;
import com.openexchange.office.filter.odf.odt.dom.TextTab;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleParagraph;

public class ParagraphComponent extends OdfComponent implements IParagraph {

	final Paragraph paragraph;

	public ParagraphComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> paragraphNode, int componentNumber) {
		super(parentContext, paragraphNode, componentNumber);
		this.paragraph = (Paragraph)getObject();
	}

	@Override
	public String simpleName() {
	    return "Para";
	}

	public Paragraph getParagraph() {
	    return paragraph;
	}

	@Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {

        DLNode<Object> nextNode = previousChildContext != null ? previousChildContext.getNode().getNext() : ((Paragraph)getObject()).getContent().getFirstNode();
        while(nextNode!=null) {
			final Object child = nextNode.getData();
			if(child instanceof TextSpan) {
				final TextSpanContext textSpanContext =  new TextSpanContext(this, nextNode);
				final IComponent<OdfOperationDoc> childComponent = textSpanContext.getNextChildComponent(null, previousChildComponent);
				if(childComponent!=null) {
					return childComponent;
				}
			}
			nextNode = nextNode.getNext();
		}
		return null;
	}

	@Override
	public void applyAttrsFromJSON(JSONObject attrs)
		throws JSONException {

		if(attrs!=null) {
			final JSONObject paragraphAttrs = attrs.optJSONObject(OCKey.PARAGRAPH.value());
			final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
			if(paragraphAttrs!=null) {

			    int level = -1;
                final Object listLevel = paragraphAttrs.opt(OCKey.LIST_LEVEL.value());
                if(listLevel!=null) {
                    if(listLevel instanceof Integer) {
                        level = ((Integer)listLevel).intValue();
                    }
                }
                else {
                    final TextListItem listItem = paragraph.getTextListItem();
                    if(listItem!=null) {
                        level = listItem.getListLevel();
                    }
                }

                boolean labelHidden = false;
                final Object listLabelHidden = paragraphAttrs.opt(OCKey.LIST_LABEL_HIDDEN.value());
                if(listLabelHidden!=null) {
                    if(listLabelHidden instanceof Boolean) {
                        labelHidden = ((Boolean)listLabelHidden).booleanValue();
                    }
                    else {
                        labelHidden = false;
                    }
                }
                else {
                    final TextListItem listItem = paragraph.getTextListItem();
                    if(listItem!=null) {
                        labelHidden = listItem.isHeader();
                    }
                }

			    final Object listStyleId = paragraphAttrs.opt(OCKey.LIST_STYLE_ID.value());
                if(listStyleId!=null) {
                    if(listStyleId instanceof String) {
                        final TextListItem textListItem;
                        if(level>0) {
                            paragraph.setListLevel(styleManager, level-1);
                            textListItem = new TextListItem(new TextList(styleManager, paragraph.getTextListItem()));
                        }
                        else {
                            textListItem = new TextListItem(new TextList(styleManager, null));
                        }
                        paragraph.setTextListItem(textListItem);
                        textListItem.getParentTextList().setStyleName((String)listStyleId);
                        textListItem.getParentTextList().setContinueNumbering(true);
                        textListItem.setIsHeader(labelHidden);
                        paragraph.setListLevel(styleManager, level);
                        // Insert listStyle operations are inserting the listStyle into content.xml...
                        // If such a style is used within style.xml (such as header/footer are doing), then
                        // we need to clone the listStyle and also insert it into the style.xml...
                        // (great idea to put header footers into style.xml)
                        if(!isContentAutoStyle()) {
                            // check if this style is available in style.xml
                            StyleBase listStyle = styleManager.getAutoStyle((String)listStyleId, StyleFamily.LIST_STYLE, false);
                            if(listStyle==null) {
                                // clone from content.xml ...
                                listStyle = styleManager.getAutoStyle((String)listStyleId, StyleFamily.LIST_STYLE, true);
                                if(listStyle!=null) {
                                    final StyleBase listStyleClone = listStyle.clone();
                                    listStyleClone.setIsContentStyle(false);
                                    styleManager.addStyle(listStyleClone);
                                }
                            }
                        }
                    }
                    else if(listStyleId==JSONObject.NULL) {
                        paragraph.setListLevel(styleManager, -1);
                    }
                }
                else {
                    if(listLevel!=null) {
                        paragraph.setListLevel(styleManager, level);
                    }
                    if(listLabelHidden!=null) {
                        final TextListItem textListItem = paragraph.getTextListItem();
                        if(textListItem!=null) {
                            textListItem.setIsHeader(labelHidden);
                        }
                    }
                }

                final Object oListStartValue = paragraphAttrs.opt(OCKey.LIST_START_VALUE.value());
                if(oListStartValue!=null) {
                    final Integer listStartValue = oListStartValue instanceof Integer ? (Integer)oListStartValue : null;
                    paragraph.setStartValue(listStartValue);
                    final TextListItem textListItem = paragraph.getTextListItem();
                    if(textListItem!=null) {
                        textListItem.setStartValue(listStartValue);
                    }
                }
			}
			paragraph.setStyleName(styleManager.createStyle(StyleFamily.PARAGRAPH, paragraph.getStyleName(), isContentAutoStyle(), attrs));
		}
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {

	    final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
	    final String styleName = paragraph.getStyleName();
	    final StyleParagraph paragraphStyle = styleName!=null&&!styleName.isEmpty() ? (StyleParagraph)styleManager.getStyle(styleName, StyleFamily.PARAGRAPH, isContentAutoStyle()) : null;
		if(paragraphStyle!=null) {
		    if(paragraphStyle.isAutoStyle()) {
		        styleManager.createAutoStyleAttributes(attrs, styleName, StyleFamily.PARAGRAPH, isContentAutoStyle());
		    }
		    else {
		        attrs.put(OCKey.STYLE_ID.value(), styleName);
		    }
		}
        final Map<String, Object> paragraphAttrs  = attrs.getMap(OCKey.PARAGRAPH.value(), true);
        final Integer outlineLevel = paragraph.getOutlineLevel();
        if(outlineLevel!=null) {
            paragraphAttrs.put(OCKey.OUTLINE_LEVEL.value(), outlineLevel-1);
        }
        final TextListItem textListItem = paragraph.getTextListItem();
		if(textListItem!=null) {
			TextList textList = textListItem.getParentTextList();
			final String styleId = textList.getId();
			if(styleId!=null&&!styleId.isEmpty()) {
				paragraphAttrs.put(OCKey.LIST_ID.value(), styleId);
			}
			if(textListItem.isHeader()) {
				paragraphAttrs.put(OCKey.LIST_LABEL_HIDDEN.value(), true);
			}
			paragraphAttrs.put(OCKey.LIST_LEVEL.value(), textListItem.getListLevel());
			final Integer startValue = textListItem.getStartValue();
			if(startValue!=null) {
				paragraphAttrs.put(OCKey.LIST_START_VALUE.value(), startValue);
			}
			final String listStyleName = textList.getStyleName(true);
			if(listStyleName!=null&&!listStyleName.isEmpty()) {
				paragraphAttrs.put(OCKey.LIST_STYLE_ID.value(), listStyleName);
			}
		}
		else if(paragraph.isHeader()&&!paragraph.isListHeader()) {
		    // check the outline level
		    final int listLevel;
		    if(outlineLevel!=null) {
		        listLevel = outlineLevel.intValue();
		    }
		    else if(paragraphStyle!=null) {
		        listLevel = paragraphStyle.getDefaultOutlineLevel(styleManager);
		    }
		    else {
		        listLevel = 0;
		    }
		    if(listLevel>0) {
		        final HashMap<String, DLNode<Object>> outlineStyles = styleManager.getFamilyStyles(StyleFamily.OUTLINE_STYLE);
		        if(outlineStyles!=null) {
		            final Iterator<Entry<String, DLNode<Object>>> outlineIter = outlineStyles.entrySet().iterator();
		            if(outlineIter.hasNext()) {
		                final Entry<String, DLNode<Object>> outlineEntry = outlineIter.next();
		                paragraphAttrs.put(OCKey.LIST_STYLE_ID.value(), outlineEntry.getKey());
		                paragraphAttrs.put(OCKey.LIST_LEVEL.value(), listLevel-1);
		                final Integer startValue = paragraph.getStartValue();
		                if(startValue!=null) {
		                    paragraphAttrs.put(OCKey.LIST_START_VALUE.value(), startValue);
		                }
		                if(paragraph.isRestartNumbering()) {
		                    paragraphAttrs.put(OCKey.LIST_START_VALUE.value(), 1);
		                }
		            }
		        }
		    }
		}
        if(paragraphAttrs.isEmpty()) {
            attrs.remove(OCKey.PARAGRAPH.value());
        }
	}

	@Override
	public void insertText(int textPosition, String text, JSONObject attrs) throws Exception {

    	if(text.length()>0) {
            Text t = null;
            IComponent<OdfOperationDoc> childComponent = getNextChildComponent(null, null);
            IComponent<OdfOperationDoc> cRet = null;

            if(childComponent!=null) {
            	if(textPosition>0) {
            		childComponent = childComponent.getComponent(textPosition-1);
            	}
                // check if the character could be inserted into an existing text:
                if(childComponent instanceof TextComponent) {
                    t = (Text)childComponent.getObject();
                    final StringBuffer s = new StringBuffer(t.getText());
                    s.insert(textPosition-((TextComponent)childComponent).getComponentNumber(), text);
                    t.setText(s.toString());
                    cRet = childComponent;
                }
                else {
                	t = new Text(text);
                	final TextSpanContext spanContext = (TextSpanContext)childComponent.getParentContext();
                    ((TextSpan)spanContext.getObject()).getContent().addNode(childComponent.getNode(), new DLNode<Object>(t), textPosition==0);
                    cRet = childComponent;

                    if(textPosition>0) {
                        cRet = childComponent.getNextComponent();
                    }
                    else {
                        cRet = getNextChildComponent(null, null);
                    }
                }
            }
            else {

            	// the paragraph is empty, we have to create R and its text
                final TextSpan newRun = new TextSpan();
                paragraph.getContent().add(newRun);
/*
                if(paragraph.getPPr()!=null) {
                    final RPr rPr = TextUtils.cloneParaRPrToRPr(paragraph.getPPr().getRPr());
                    if(rPr!=null) {
                        rPr.setParent(newRun);
                        newRun.setRPr(rPr);
                    }
                }
*/
                t = new Text(text);
                newRun.getContent().add(t);
                cRet = getNextChildComponent(null, null);
            }
            if(attrs!=null) {
            	cRet.splitStart(textPosition, SplitMode.DELETE);
            	cRet.splitEnd(textPosition+text.length()-1, SplitMode.DELETE);
            	cRet.applyAttrsFromJSON(attrs);
            }
        }
	}

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(int textPosition, JSONObject attrs, ComponentType childType) throws Exception {

        Object newChild = null;
        switch(childType) {
            case TAB : {
                newChild = new TextTab(null);
                break;
            }
            case HARDBREAK : {
            	newChild = new TextLineBreak();
            	break;
            }
            case FIELD : {
            	newChild = new TextField();
            	break;
            }
            case COMMENT_REFERENCE : {
            	newChild = new Annotation(null, false);
            	break;
            }
            case COMMENT_RANGE_END : {
            	newChild = new AnnotationEnd(null);
            	break;
            }
            case AC_IMAGE : {
            	final DrawFrame drawFrame = new DrawFrame(operationDocument, null, true, isContentAutoStyle());
            	drawFrame.getContent().add(new DrawImage(drawFrame));
            	newChild = drawFrame;
            	break;
            }
            case AC_CONNECTOR :
            case AC_SHAPE : {
            	newChild = Shape.createShape(operationDocument, attrs, null, true, isContentAutoStyle());
            	break;
            }
            case AC_GROUP : {
            	newChild = new GroupShape(operationDocument, null, true, isContentAutoStyle());
            	break;
            }
            case AC_FRAME : {
            	final DrawFrame drawFrame = new DrawFrame(operationDocument, null, true, isContentAutoStyle());
            	drawFrame.getContent().add(new DrawTextBox(drawFrame));
            	newChild = drawFrame;
            	break;
            }
            default : {
                throw new UnsupportedOperationException();
            }
        }
        IComponent<OdfOperationDoc> childComponent = getNextChildComponent(null, null);
        if(childComponent!=null) {
            if(textPosition>0) {
                childComponent = childComponent.getComponent(textPosition-1);
                childComponent.splitEnd(textPosition-1, SplitMode.DELETE);
            }
    		// the new child can be added into an existing textRun
            final TextSpan textSpan = ((TextSpan_Base)childComponent).getTextSpan();
            textSpan.getContent().addNode(childComponent.getNode(), new DLNode<Object>(newChild), textPosition==0);
        }
        else {
            final TextSpan newTextSpan = new TextSpan();
            paragraph.getContent().add(newTextSpan);
            newTextSpan.getContent().add(newChild);
        }
        if(textPosition>0) {
            childComponent = childComponent.getNextComponent();
        }
        else {
            childComponent = getNextChildComponent(null, null);
        }
        if(attrs!=null) {
            childComponent.splitStart(textPosition, SplitMode.DELETE);
            childComponent.splitEnd(textPosition, SplitMode.DELETE);
            childComponent.applyAttrsFromJSON(attrs);
        }
        return childComponent;
    }

	@Override
    public void splitParagraph(int textPosition) {
        final Paragraph destParagraph = new Paragraph(
        	paragraph.getTextListItem()!=null
        	? new TextListItem(paragraph.getTextListItem().getParentTextList())
        	: null);

        final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
        // taking care of paragraphs sharing the same textListItem
        if(destParagraph.getTextListItem()!=null) {
        	DLNode<Object> nextParaNode = getNode().getNext();
        	while(nextParaNode!=null&&nextParaNode.getData() instanceof Paragraph) {
	        	final Paragraph nextPara = (Paragraph)getNode().getNext().getData();
	        	if(nextPara.getTextListItem()!=paragraph.getTextListItem()) {
	        		break;
	        	}
	        	nextPara.setTextListItem(destParagraph.getTextListItem());
    	        nextParaNode = nextParaNode.getNext();
        	}
        }

        final DLNode<Object> destParagraphNode = new DLNode<Object>(destParagraph);
        ((INodeAccessor)getParentContext().getObject()).getContent().addNode(getNode(), destParagraphNode, false);

        String paragraphStyleName = paragraph.getStyleName();
        final StyleBase paragraphStyle = styleManager.getStyle(paragraphStyleName, StyleFamily.PARAGRAPH, isContentAutoStyle());
        if(paragraphStyle!=null) {
            final AttributesImpl styleAttributes = paragraphStyle.getAttributes();
            if(styleAttributes.containsKey("style:master-page-name")||styleAttributes.containsKey("fo:break-before")||styleAttributes.containsKey("fo:break-after")) {
                final StyleBase paragraphClone = paragraphStyle.clone();
                paragraphClone.getAttributes().remove("style:master-page-name");
                paragraphClone.getAttributes().remove("fo:break-before");
                paragraphClone.getAttributes().remove("fo:break-after");
                paragraphStyleName = styleManager.getExistingStyleIdForStyleBase(paragraphClone);
                if(paragraphStyleName==null||paragraphStyleName.isEmpty()) {
                    paragraphStyleName = styleManager.getUniqueStyleName(StyleFamily.PARAGRAPH, isContentAutoStyle());
                    paragraphClone.setName(paragraphStyleName);
                    styleManager.addStyle(paragraphClone);
                }
            }
        }
        destParagraph.setStyleName(paragraphStyleName);
        IComponent<OdfOperationDoc> component = getNextChildComponent(null, null);
    	while(component!=null&&component.getNextComponentNumber()<=textPosition) {
    		component = component.getNextComponent();
    	}
        if(component!=null) {
        	component.splitStart(textPosition, SplitMode.DELETE);

        	// moving text spans into the new paragraph
        	paragraph.getContent().moveNodes(component.getParentContext().getNode(), -1, destParagraph.getContent(), null, true);
        }
    }

	@Override
	public void mergeParagraph() {
    	final IComponent<OdfOperationDoc> nextParagraphComponent = getNextComponent();
    	if(nextParagraphComponent instanceof ParagraphComponent) {
        	final DLList<Object> sourceContent = ((Paragraph)nextParagraphComponent.getObject()).getContent();
            sourceContent.moveNodes(paragraph.getContent());
            ((INodeAccessor)getParentContext().getObject()).getContent().removeNode(nextParagraphComponent.getNode());
    	}
	}

	public static Paragraph getFirstParagraph(IComponent<OdfOperationDoc> rootComponent, boolean forceCreate) {
	    final ParagraphComponent paragraphComponent = _getFirstParagraphComponent(rootComponent.getChildComponent(0));
	    if(paragraphComponent!=null) {
	        return paragraphComponent.getParagraph();
	    }
	    if(forceCreate) {
	        final Paragraph paragraph = new Paragraph(null);
	        ((INodeAccessor)rootComponent.getObject()).getContent().add(paragraph);
	        return paragraph;
	    }
	    return null;
	}

	private static ParagraphComponent _getFirstParagraphComponent(IComponent<OdfOperationDoc> component) {
        while(component!=null) {
            if(component instanceof ParagraphComponent) {
                return (ParagraphComponent)component;
            }
            final IComponent<OdfOperationDoc> childParagraphComponent = component.getChildComponent(0);
            if(childParagraphComponent!=null) {
                return _getFirstParagraphComponent(childParagraphComponent);
            }
            component = component.getNextComponent();
        }
        return null;
    }
}
