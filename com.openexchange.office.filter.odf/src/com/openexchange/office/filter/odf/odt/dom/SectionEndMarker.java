/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import java.lang.ref.WeakReference;
import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class SectionEndMarker implements ISectionWriter {

	final private WeakReference<SectionStartMarker> sectionStartMarkerRef;

	public SectionEndMarker(SectionStartMarker sectionStartMarker) {
		sectionStartMarkerRef = new WeakReference<SectionStartMarker>(sectionStartMarker);
	}

	@Override
	public void writeObject(SerializationHandler output) {

		// don't write this object without knowing if the corresponding end marker is stored within the same list

	}

	@Override
	public void writeSection(SerializationHandler output, DLList<Object> parentList)
		throws SAXException {

		// taking care that the start marker is available (in the same parent list) otherwise don't close this element

		if(sectionStartMarkerRef!=null) {
			final SectionStartMarker sectionStartMarker = sectionStartMarkerRef.get();
			if(sectionStartMarker!=null) {
				if(parentList.contains(sectionStartMarker)) {
					SaxContextHandler.endElement(output, Namespaces.TEXT, "section", "text:section");
				}
			}
		}
	}
}
