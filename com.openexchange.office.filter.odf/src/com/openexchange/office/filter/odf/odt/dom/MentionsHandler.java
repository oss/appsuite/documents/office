/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class MentionsHandler extends SaxContextHandler {

    final JSONArray mentions;

    public MentionsHandler(SaxContextHandler parentContext, JSONArray mentions) {
    	super(parentContext);

    	this.mentions = mentions;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        if(qName.equals("oxext:mention")) {
            final AttributesImpl attributesImpl = new AttributesImpl(attributes);
            final Integer pos = attributesImpl.getIntValue("oxext:pos", null);
            final Integer length = attributesImpl.getIntValue("oxext:length", null);
            if(pos != null && length != null) {
                final JSONObject mention = new JSONObject();
                try {
                    mention.put(OCKey.POS.value(), pos);
                    mention.put(OCKey.LENGTH.value(), length);
                    final String displayName = attributesImpl.getValue("oxext:display-name");
                    if(displayName!=null) {
                        mention.put(OCKey.DISPLAY_NAME.value(), displayName);
                    }
                    final String email = attributesImpl.getValue("oxext:email");
                    if(email!=null) {
                        mention.put(OCKey.EMAIL.value(), email);
                    }
                    final String userId = attributesImpl.getValue("oxext:user-id");
                    if(userId!=null) {
                        mention.put(OCKey.USER_ID.value(), userId);
                    }
                    final String providerId = attributesImpl.getValue("oxext:provider-id");
                    if(providerId!=null) {
                        mention.put(OCKey.PROVIDER_ID.value(), providerId);
                    }
                    mentions.put(mention);

                } catch (JSONException e) {
                    // ohoh
                }
            }
        }
        return this;
    }
}
