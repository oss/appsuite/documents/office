/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class Paragraph implements IElementWriter, INodeAccessor {

	private DLList<Object> content;
	private final boolean header;
	private TextListItem textListItem;

	// attributes
	private final String id;
	private final String classNames;
	private String styleName;
	private final String conditionalStyleName;
	private final String xHtmlAbout;
	private final String xHtmlContent;
	private final String xHtmlDataType;
	private final String xHtmlProperty;
	private final String xmlId;
	// attributes if header
	private final Boolean isListHeader;
	private Integer outlineLevel;
	private final Boolean restartNumbering;
	private Integer startValue;

	private int listLevel;

	private char lastChar;

	public Paragraph(TextListItem textListItem) {
		header = false;
		this.textListItem = textListItem;
		// attributes
		id = null;
		classNames = null;
		styleName = null;
		conditionalStyleName = null;
		xHtmlAbout = null;
		xHtmlContent = null;
		xHtmlDataType = null;
		xHtmlProperty = null;
		xmlId = null;
		// attributes if header
		isListHeader = null;
		outlineLevel = null;
		restartNumbering = null;
		startValue = null;

		listLevel = textListItem!=null ? textListItem.getListLevel() : -1;

		/* lastChar is used only for loading purposes (whitespace handling) */
		lastChar = ' ';
	}

	public Paragraph(boolean header, Attributes attributes, TextListItem textListItem) {

		this.header = header;
		this.textListItem = textListItem;

		// attributes
		id = attributes.getValue("text:id");
		classNames = attributes.getValue("text:class-names");
		styleName = attributes.getValue("text:style-name");
		conditionalStyleName = attributes.getValue("text:cond-style-name");
		xHtmlAbout = attributes.getValue("xhtml:about");
		xHtmlContent = attributes.getValue("xhtml:content");
		xHtmlDataType = attributes.getValue("xhtml:datatype");
		xHtmlProperty = attributes.getValue("xhtml:property");
		xmlId = attributes.getValue("xml:id");

		// attributes if header:
		if(header) {
			isListHeader = AttributesImpl.getBoolean(attributes.getValue("text:is-list-header"), null);
			restartNumbering = AttributesImpl.getBoolean(attributes.getValue("text:restart-numbering"), null);
			String s = attributes.getValue("text:start-value");
			if(s!=null&&!s.isEmpty()) {
				startValue = Integer.parseInt(s);
			}
			s = attributes.getValue("text:outline-level");
			if(s!=null&&!s.isEmpty()) {
				outlineLevel = Integer.parseInt(s);
			}
		}
		else {
			isListHeader = null;
			outlineLevel = null;
			restartNumbering = null;
			startValue = null;
		}
		listLevel = textListItem!=null ? textListItem.getListLevel() : -1;
		lastChar = ' ';
	}

	public void setListLevel(StyleManager styleManager, int level) {
		listLevel = level;
		if(level==-1) {
			textListItem = null;
		}
		else if(textListItem!=null) {
			final int currentLevel = textListItem.getListLevel();
			final boolean isHeader = textListItem.isHeader();
			int d = level - currentLevel;
			while(d>0) {
				final TextList textList = new TextList(styleManager, textListItem);
				textListItem = new TextListItem(textList);
				textListItem.setIsHeader(isHeader);
				d--;
			}
			while(d<0) {
				textListItem = textListItem.getParentTextList().getParentTextListItem();
				d++;
			}
			textListItem.getParentTextList().setContinueNumbering(true);
		}
	}

	public int getListLevel() {
		return listLevel;
	}

	public TextListItem getTextListItem() {
		return textListItem;
	}

	public void setTextListItem(TextListItem textListItem) {
		this.textListItem = textListItem;
	}

	public void applyTextListItem(Paragraph referenceParagraph) {
        final TextListItem referenceTextListItem = referenceParagraph.getTextListItem();
        if(referenceTextListItem!=null) {
            final TextListItem textListItemClone = referenceTextListItem.clone();
            final TextList root = textListItemClone.getParentTextList().getTextListRoot();
            final String textListId = referenceTextListItem.getParentTextList().getTextListRoot().getId();
            root.setContinueList(textListId);
            setTextListItem(textListItemClone);
        }
        else {
            setTextListItem(null);
        }
    }

	@Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
		return content;
	}

	public String getStyleName() {
		return styleName;
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	public Integer getStartValue() {
		return startValue;
	}

	public void setStartValue(Integer startValue) {
		this.startValue = startValue;
	}

	public Integer getOutlineLevel() {
		return outlineLevel;
	}

	public void setOutlineLevel(Integer outlineLevel) {
		this.outlineLevel = outlineLevel;
	}

	public boolean isHeader() {
		return header;
	}

	/* text:is-list-header attribute specifies the appearance of a specific heading without numbering.
	 * The defined values for the text:is-list-header attribute are:
	 *    false: heading will be numbered.
	 *    true: heading will be not numbered, even if the header has a list-style..
	 *
	 *    The default value for this attribute is false.
	 */
	public boolean isListHeader() {
	    return isListHeader==null ? false : isListHeader.booleanValue();
	}

	public boolean isRestartNumbering() {
	    return restartNumbering==null ? false : restartNumbering.booleanValue();
	}

	public char getLastChar() {
	    return lastChar;
	}

	public void setLastChar(char c) {
	    lastChar = c;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		final String localName = header ? "h" : "p";
		final String qName = "text:" + localName;

		SaxContextHandler.startElement(output, Namespaces.TEXT, localName, qName);

		// writing attributes
		SaxContextHandler.addAttribute(output, Namespaces.TEXT, "id", "text:id", id);
		SaxContextHandler.addAttribute(output, Namespaces.TEXT, "class-names", "text:class-names", classNames);
		SaxContextHandler.addAttribute(output, Namespaces.TEXT, "style-name", "text:style-name", styleName);
		SaxContextHandler.addAttribute(output, Namespaces.TEXT, "cond-style-name", "text:cond-style-name", conditionalStyleName);
		SaxContextHandler.addAttribute(output, Namespaces.XHTML, "about", "xhtml:about", xHtmlAbout);
		SaxContextHandler.addAttribute(output, Namespaces.XHTML, "content", "xhtml:content", xHtmlContent);
		SaxContextHandler.addAttribute(output, Namespaces.XHTML, "datatype", "xhtml:datatype", xHtmlDataType);
		SaxContextHandler.addAttribute(output, Namespaces.XHTML, "property", "xhtml:property", xHtmlProperty);
		SaxContextHandler.addAttribute(output, Namespaces.XML, "id", "xml:id", xmlId);

		// attributes if header
		if(header) {
		    if(isListHeader!=null) {
		        SaxContextHandler.addAttribute(output, Namespaces.TEXT, "is-list-header", "text:is-list-header", isListHeader.toString());
		    }
			SaxContextHandler.addAttribute(output, Namespaces.TEXT, "outline-level", "text:outline-level", outlineLevel);
			if(restartNumbering!=null) {
			    SaxContextHandler.addAttribute(output, Namespaces.TEXT, "restart-numbering", "text:restart-numbering", restartNumbering.toString());
			}
			SaxContextHandler.addAttribute(output, Namespaces.TEXT, "start-value", "text:start-value", startValue);
		}

		for(Object child:getContent()) {
			if(child instanceof IElementWriter) {
				((IElementWriter)child).writeObject(output);
			}
		}
		SaxContextHandler.endElement(output, Namespaces.TEXT, localName, qName);
	}
}
