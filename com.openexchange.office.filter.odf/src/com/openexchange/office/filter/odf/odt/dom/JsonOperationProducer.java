/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.Settings;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.components.TextComponent;
import com.openexchange.office.filter.odf.components.TextFieldComponent;
import com.openexchange.office.filter.odf.components.TextLineBreakComponent;
import com.openexchange.office.filter.odf.components.TextTabComponent;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.draw.IDrawingType;
import com.openexchange.office.filter.odf.odt.dom.components.AnnotationComponent;
import com.openexchange.office.filter.odf.odt.dom.components.AnnotationEndComponent;
import com.openexchange.office.filter.odf.odt.dom.components.CellComponent;
import com.openexchange.office.filter.odf.odt.dom.components.DrawFrameComponent;
import com.openexchange.office.filter.odf.odt.dom.components.FrameRootComponent;
import com.openexchange.office.filter.odf.odt.dom.components.ParagraphComponent;
import com.openexchange.office.filter.odf.odt.dom.components.RootComponent;
import com.openexchange.office.filter.odf.odt.dom.components.RowComponent;
import com.openexchange.office.filter.odf.odt.dom.components.ShapeComponent;
import com.openexchange.office.filter.odf.odt.dom.components.ShapeRootComponent;
import com.openexchange.office.filter.odf.odt.dom.components.TableComponent;
import com.openexchange.office.filter.odf.styles.MasterStyles;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleHeaderFooter;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleMasterPage;
import com.openexchange.office.filter.odf.styles.StylePageLayout;
import com.openexchange.office.filter.odf.table.Table;

public class JsonOperationProducer {

    final JSONArray operationQueue;

    private final OdfOperationDoc opsDoc;
    private final OdfTextDocument doc;
    private final TextStyles styles;
    private final TextContent content;
    private final StyleManager styleManager;
    private final Settings settings;

    public JsonOperationProducer(OdfOperationDoc opsDoc)
    	throws SAXException {

    	operationQueue = new JSONArray();
    	this.opsDoc = opsDoc;
        doc = (OdfTextDocument)opsDoc.getDocument();
        styles = (TextStyles)doc.getStylesDom();
        content = (TextContent)doc.getContentDom();
        settings = doc.getSettingsDom();
        styleManager = doc.getStyleManager();
        styleManager.setTabsRelativeToIndent(settings.hasTabsRelativeToIndent());
    }

    public JSONObject getDocumentOperations()
    	throws JSONException, SAXException {

    	createDocumentAttributes();

        final HashSet<String> createdStyles = new HashSet<String>();
    	styleManager.createInsertStyleOperations(styleManager.getStyles(), "text", operationQueue, createdStyles);
    	styleManager.createInsertAutoListStylesOperations(false, operationQueue);
    	styleManager.createInsertAutoListStylesOperations(true, operationQueue);

    	createHeaderFooterOperations(content.getBody().getMasterPageName(true));
    	createTextOperations(new RootComponent(opsDoc, content.getBody(), true), new ArrayList<Integer>(), null);

    	final JSONObject operations = new JSONObject(1);
        operations.put("operations", operationQueue);
        return operations;
    }

    public void createDocumentAttributes()
    	throws JSONException {

    	final OpAttrs attrs = new OpAttrs();
    	final Map<String, Object> documentAttributes = attrs.getMap(OCKey.DOCUMENT.value(), true);
    	documentAttributes.put(OCKey.FILE_FORMAT.value(), "odf");
        final String masterPageName = content.getBody().getMasterPageName(true);
    	final MasterStyles masterStyles = styleManager.getMasterStyles();
    	final Iterator<Object> styleMasterPageIter = masterStyles.getContent().iterator();
    	while(styleMasterPageIter.hasNext()) {
    		final Object o = styleMasterPageIter.next();
    		if(o instanceof StyleMasterPage) {
				if(((StyleMasterPage)o).getName().equals(masterPageName)) {
					final String pageLayoutName = ((StyleMasterPage)o).getPageLayoutName();
					if(pageLayoutName!=null&&!pageLayoutName.isEmpty()) {
						final StyleBase baseStyle = styleManager.getStyle(pageLayoutName, StyleFamily.PAGE_LAYOUT, false);
						if(baseStyle instanceof StylePageLayout) {
							((StylePageLayout)baseStyle).getPageLayoutProperties().createAttrs(styleManager, false, attrs);
						}
					}
				}
    		}
    	}
        final JSONObject setDocumentAttributesOperation = new JSONObject(5);
        setDocumentAttributesOperation.put(OCKey.NAME.value(), OCValue.SET_DOCUMENT_ATTRIBUTES.value());
        setDocumentAttributesOperation.put(OCKey.ATTRS.value(), attrs);
        Tools.writeFilterVersion(setDocumentAttributesOperation.getJSONObject(OCKey.ATTRS.value()));
        operationQueue.put(setDocumentAttributesOperation);
    }

    public void createHeaderFooterOperations(String name)
    	throws JSONException, SAXException {

    	final MasterStyles masterStyles = styleManager.getMasterStyles();
    	final Iterator<Object> styleMasterPageIter = masterStyles.getContent().iterator();
    	while(styleMasterPageIter.hasNext()) {
    		final Object o = styleMasterPageIter.next();
    		if(o instanceof StyleMasterPage) {
				if(((StyleMasterPage)o).getName().equals(name)) {
	    			for(StyleHeaderFooter styleHeaderFooter:((StyleMasterPage)o).getStyleHeaderFooters()) {
	    				addInsertHeaderFooterOperation(styleHeaderFooter.getId(), styleHeaderFooter.getType());
	    				createTextOperations(new RootComponent(opsDoc, styleHeaderFooter, false), new ArrayList<Integer>(), styleHeaderFooter.getId());
	    			}
	    			return;
				}
    		}
		}
    }

    public void createTextOperations(OdfComponent parentComponent, List<Integer> parentPosition, String target)
    	throws JSONException, SAXException {

        IComponent<OdfOperationDoc> component = parentComponent.getNextChildComponent(null, null);
        while (component!=null) {
        	if(component instanceof ParagraphComponent) {
        		createParagraphOperations((ParagraphComponent)component, parentPosition, target);
        	}
        	else if(component instanceof TableComponent) {
        		createTableOperations((TableComponent)component, parentPosition, target);
        	}
            component = component.getNextComponent();
        }
    }

    public void createParagraphOperations(ParagraphComponent paragraphComponent, List<Integer> parentPosition, String target)
    	throws JSONException, SAXException {

    	// insert paragraph and apply paragraph attributes
        final List<Integer> paragraphPosition = new ArrayList<Integer>(parentPosition);
        paragraphPosition.add(paragraphComponent.getComponentNumber());
        OpAttrs paragraphAttrs = new OpAttrs();
        paragraphComponent.createJSONAttrs(paragraphAttrs);
        addInsertParagraphOperation(paragraphPosition, target, paragraphAttrs);

        // insert components (text and or other drawing objects)
        OpAttrs currentAttrs = new OpAttrs();
        OdfComponent component = (OdfComponent)paragraphComponent.getNextChildComponent(null, null);
        while(component!=null) {
            OpAttrs attrs = new OpAttrs();
            component.createJSONAttrs(attrs);
            if(!opsDoc.isCreateFastLoadOperations()) {
                attrs = OpAttrs.createDifference(currentAttrs, attrs);
            }
        	final List<Integer> textPosition = new ArrayList<Integer>(paragraphPosition);
        	textPosition.add(component.getComponentNumber());
        	if(component instanceof TextComponent) {
        		addInsertTextOperation(textPosition, ((Text)(component.getObject())).getText(), target, attrs);
        	}
        	else if(component instanceof TextTabComponent) {
        		addInsertTabOperation(textPosition, target, attrs);
        	}
        	else if(component instanceof TextLineBreakComponent) {
        		addInsertHardBreakOperation(textPosition, target, attrs);
        	}
        	else if(component instanceof TextFieldComponent) {
        		addInsertFieldOperation(textPosition, ((TextField)component.getObject()), target, attrs);
        	}
        	else if(component instanceof FrameRootComponent) {
        		createFrameOperations((FrameRootComponent)component, textPosition, target, attrs);
        	}
        	else if(component instanceof ShapeRootComponent) {
        		createShapeOperations(component, textPosition, target, attrs);
        	}
        	else if(component instanceof AnnotationComponent) {
        		addInsertCommentOperation(textPosition, target, (AnnotationComponent)component, attrs);
        	}
        	else if(component instanceof AnnotationEndComponent) {
        		addInsertRangeOperation(textPosition, target, (AnnotationEnd)component.getObject(), attrs);
        	}
        	component = (OdfComponent)component.getNextComponent();
        }
    }

    public void createTableOperations(TableComponent tableComponent, List<Integer> parentPosition, String target)
        	throws JSONException, SAXException {

        final List<Integer> tablePosition = new ArrayList<Integer>(parentPosition);
        tablePosition.add(tableComponent.getComponentNumber());
        final OpAttrs attrs = new OpAttrs();
        tableComponent.createJSONAttrs(attrs);
        if(addInsertTableOperation(tablePosition, target, (Table)tableComponent.getObject(), attrs)) {
	    	OdfComponent component= (OdfComponent)tableComponent.getNextChildComponent(null, null);
	    	while(component!=null) {
	    		if(component instanceof RowComponent) {
	    	        final List<Integer> rowPosition = new ArrayList<Integer>(tablePosition);
	    	        rowPosition.add(component.getComponentNumber());
	    			createRowOperations((RowComponent)component, rowPosition, target);
	    		}
	    		component = (OdfComponent)component.getNextComponent();
	    	}
        }
    }

    public void createRowOperations(RowComponent rowComponent, List<Integer> rowPosition, String target)
        	throws JSONException, SAXException {

        OpAttrs attrs = new OpAttrs();
        rowComponent.createJSONAttrs(attrs);
    	addInsertRowsOperation(rowPosition, 1, target, attrs);
    	OdfComponent component= (OdfComponent)rowComponent.getNextChildComponent(null, null);
    	while(component!=null) {
    		if(component instanceof CellComponent) {
    			final List<Integer> cellPosition = new ArrayList<Integer>(rowPosition);
    	        cellPosition.add(component.getComponentNumber());
    	        attrs = new OpAttrs();
    	        component.createJSONAttrs(attrs);
    	        addInsertCellsOperation(cellPosition, target, attrs);
    	   		createTextOperations(component, cellPosition, target);
    		}
    		component = (OdfComponent)component.getNextComponent();
    	}
    }

    public void createFrameOperations(FrameRootComponent frameComponent, List<Integer> position, String target, OpAttrs attrs)
    	throws JSONException, SAXException {
    	addInsertDrawingOperation(position, frameComponent.getType(), attrs, target);
    	// a frame component (textBox, image may have childs)...
        OdfComponent component = (OdfComponent)frameComponent.getNextChildComponent(null, null);
        while(component!=null) {
        	if(component instanceof ParagraphComponent) {
        		createParagraphOperations((ParagraphComponent)component, position, target);
        	}
        	else if(component instanceof TableComponent) {
        		createTableOperations((TableComponent)component, position, target);
        	}
        	component = (OdfComponent)component.getNextComponent();
        }
    }

    public void createShapeOperations(OdfComponent shapeComponent, List<Integer> position, String target, OpAttrs attrs)
    	throws JSONException, SAXException {

    	addInsertDrawingOperation(position, ((IDrawingType)shapeComponent).getType(), attrs, target);
    	OdfComponent component = (OdfComponent)shapeComponent.getNextChildComponent(null, null);
    	while(component!=null) {
    		if(component instanceof ParagraphComponent) {
    			createParagraphOperations((ParagraphComponent)component, position, target);
    		}
    		else if((component instanceof DrawFrameComponent) || (component instanceof ShapeComponent)) {
                final List<Integer> childPosition = new ArrayList<Integer>(position);
                childPosition.add(component.getComponentNumber());
                final OpAttrs childAttrs = new OpAttrs();
                component.createJSONAttrs(childAttrs);
    		    createShapeOperations(component, childPosition, target, childAttrs);
    		}
    		component = (OdfComponent)component.getNextComponent();
    	}
    }

    public void addInsertParagraphOperation(final List<Integer> start, String target, final OpAttrs attrs)
        throws JSONException {

        final JSONObject insertParagraphObject = new JSONObject(4);
        insertParagraphObject.put(OCKey.NAME.value(), OCValue.INSERT_PARAGRAPH.value());
        insertParagraphObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertParagraphObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
        	insertParagraphObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertParagraphObject);
    }

    public boolean addInsertTableOperation(final List<Integer> start, String target, Table table, final OpAttrs attrs)
   		throws JSONException {

    	final boolean tableSizeExceeded = table.getTableSizeExceeded();
        final JSONObject insertTableObject = new JSONObject(4);
        insertTableObject.put(OCKey.NAME.value(), OCValue.INSERT_TABLE.value());
        insertTableObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertTableObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
        	insertTableObject.put(OCKey.ATTRS.value(), attrs);
        }
        if(tableSizeExceeded) {
        	final JSONObject sizeExceeded = new JSONObject(2);
        	sizeExceeded.put(OCKey.ROWS.value(), table.getRows().getContent().size());
        	sizeExceeded.put(OCKey.COLUMNS.value(), table.getColumns().size());
        	insertTableObject.put(OCKey.SIZE_EXCEEDED.value(), sizeExceeded);
        }
        operationQueue.put(insertTableObject);
        return !tableSizeExceeded;
    }

    public void addInsertRowsOperation(final List<Integer> start, int count, String target, final OpAttrs attrs)
        throws JSONException {

        final JSONObject insertRowObject = new JSONObject(4);
        insertRowObject.put(OCKey.NAME.value(), OCValue.INSERT_ROWS.value());
        insertRowObject.put(OCKey.START.value(), start);
        if(count!=1) {
        	insertRowObject.put(OCKey.COUNT.value(), count);
        }
        if(target!=null) {
        	insertRowObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
        	insertRowObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertRowObject);
    }

    public void addInsertCellsOperation(final List<Integer> start, String target, final OpAttrs attrs)
        throws JSONException {

        final JSONObject insertCellsObject = new JSONObject(3);
        insertCellsObject.put(OCKey.NAME.value(), OCValue.INSERT_CELLS.value());
        insertCellsObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertCellsObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
        	insertCellsObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertCellsObject);
    }

    public void addInsertTextOperation(final List<Integer> start, String text, String target, OpAttrs attrs)
        throws JSONException {

        final JSONObject insertTextObject = new JSONObject(4);
        insertTextObject.put(OCKey.NAME.value(), OCValue.INSERT_TEXT.value());
        insertTextObject.put(OCKey.START.value(), start);
        insertTextObject.put(OCKey.TEXT.value(), text);
        if(target!=null) {
        	insertTextObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            insertTextObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertTextObject);
    }

    public void addInsertTabOperation(final List<Integer> start, String target, OpAttrs attrs)
        throws JSONException {

        final JSONObject insertTabObject = new JSONObject(3);
        insertTabObject.put(OCKey.NAME.value(), OCValue.INSERT_TAB.value());
        insertTabObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertTabObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            insertTabObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertTabObject);
    }

    public void addInsertHardBreakOperation(List<Integer> start, String target, OpAttrs attrs)
        throws JSONException {

        final JSONObject insertHardBreakObject = new JSONObject(3);
        insertHardBreakObject.put(OCKey.NAME.value(), OCValue.INSERT_HARD_BREAK.value());
        insertHardBreakObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	insertHardBreakObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            insertHardBreakObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertHardBreakObject);
    }

    public void addInsertFieldOperation(List<Integer> start, TextField field, String target, OpAttrs attrs)
        throws JSONException {
        final JSONObject insertFieldObject = new JSONObject(5);
        insertFieldObject.put(OCKey.NAME.value(), OCValue.INSERT_FIELD.value());
        insertFieldObject.put(OCKey.START.value(), start);
        insertFieldObject.put(OCKey.TYPE.value(), field.getType());
        insertFieldObject.put(OCKey.REPRESENTATION.value(), field.getRepresentation());
        if(target!=null) {
        	insertFieldObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            insertFieldObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertFieldObject);
    }

    public void addInsertDrawingOperation(List<Integer> start, DrawingType type, OpAttrs attrs, String target)
        throws JSONException {

        final JSONObject insertDrawingObject = new JSONObject(5);
        insertDrawingObject.put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        insertDrawingObject.put(OCKey.START.value(), start);
        insertDrawingObject.put(OCKey.TYPE.value(), type.toString());
        if(!attrs.isEmpty()) {
        	insertDrawingObject.put(OCKey.ATTRS.value(), attrs);
        }
        if(target!=null) {
        	insertDrawingObject.put(OCKey.TARGET.value(), target);
        }
        operationQueue.put(insertDrawingObject);
    }

    public void addInsertCommentOperation(final List<Integer> start,  String target, AnnotationComponent annotationComponent, OpAttrs attrs)
        throws JSONException {

        final Annotation annotation = (Annotation)annotationComponent.getObject();
        final JSONObject setCommentObject = new JSONObject(6);
    	setCommentObject.put(OCKey.NAME.value(), OCValue.INSERT_COMMENT.value());
    	setCommentObject.put(OCKey.START.value(), start);
        if(target!=null) {
        	setCommentObject.put(OCKey.TARGET.value(), target);
        }
        setCommentObject.put(OCKey.ID.value(), annotation.getId());
        if(annotation.getCreator()!=null) {
        	setCommentObject.put(OCKey.AUTHOR.value(), annotation.getCreator());
        }
        if(annotation.getDate()!=null) {
        	setCommentObject.put(OCKey.DATE.value(), annotation.getDate());
        }
        if(annotation.getProviderId()!=null) {
            setCommentObject.put(OCKey.PROVIDER_ID.value(), annotation.getProviderId());
        }
        if(annotation.getUserId()!=null) {
            setCommentObject.put(OCKey.USER_ID.value(), annotation.getUserId());
        }
        final MutableBoolean restorable = new MutableBoolean(true);
        setCommentObject.put(OCKey.TEXT.value(), getSimpleTextAndMentionsFromComment(annotationComponent, restorable));
        if(!restorable.booleanValue()) {
            setCommentObject.put(OCKey.RESTORABLE.value(), false);
        }
        final JSONArray mentions = annotation.getMentions();
        if(mentions!=null && !mentions.isEmpty()) {
            setCommentObject.put(OCKey.MENTIONS.value(), mentions);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            setCommentObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(setCommentObject);
    }

    private String getSimpleTextAndMentionsFromComment(AnnotationComponent annotationComponent, MutableBoolean restorable) {
        final Annotation annotation = (Annotation)annotationComponent.getObject();
        final StringBuilder builder = new StringBuilder();

        boolean firstParagraph = true;
        IComponent<OdfOperationDoc> component = new RootComponent(opsDoc, annotation, annotationComponent.isContentAutoStyle()).getNextChildComponent(null, null);
        while (component!=null) {
            if(component instanceof ParagraphComponent) {
                appendSimpleTextAndMentionsFromParagraph(builder, (ParagraphComponent)component, firstParagraph, restorable);
            }
            else {
                restorable.setFalse();
            }
            firstParagraph = false;
            component = component.getNextComponent();
        }
        return builder.toString();
    }

    private void appendSimpleTextAndMentionsFromParagraph(StringBuilder builder, ParagraphComponent paragraphComponent, boolean firstParagraph, MutableBoolean restorable) {
        // insert paragraph and apply paragraph attributes
        if(!firstParagraph) {
            builder.append('\n');   // new line except for the first paragraph
        }

        IComponent<OdfOperationDoc> component = paragraphComponent.getNextChildComponent(null, null);
        while (component!=null) {
            if(component instanceof TextComponent) {
                builder.append(((Text)(component.getObject())).getText());
            }
            else {
                restorable.setFalse();
            }
            component = component.getNextComponent();
        }
    }

    public void addInsertRangeOperation(final List<Integer> start,  String target, AnnotationEnd annotationEnd, OpAttrs attrs)
        throws JSONException {

    	final JSONObject insertRangeObject = new JSONObject(6);
    	insertRangeObject.put(OCKey.NAME.value(), OCValue.INSERT_RANGE.value());
    	insertRangeObject.put(OCKey.START.value(), start);
    	insertRangeObject.put(OCKey.TYPE.value(), "comment");
        if(target!=null) {
        	insertRangeObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            insertRangeObject.put(OCKey.ATTRS.value(), attrs);
        }
    	insertRangeObject.put(OCKey.POSITION.value(), "end");
    	insertRangeObject.put(OCKey.ID.value(), annotationEnd.getId());
        operationQueue.put(insertRangeObject);
    }

    public void addInsertHeaderFooterOperation(String id, String type)
        throws JSONException {

    	final JSONObject insertHeaderFooterObject = new JSONObject(3);
    	insertHeaderFooterObject.put(OCKey.NAME.value(), OCValue.INSERT_HEADER_FOOTER.value());
    	insertHeaderFooterObject.put(OCKey.ID.value(), id);
    	insertHeaderFooterObject.put(OCKey.TYPE.value(), type);
        operationQueue.put(insertHeaderFooterObject);
    }
}
