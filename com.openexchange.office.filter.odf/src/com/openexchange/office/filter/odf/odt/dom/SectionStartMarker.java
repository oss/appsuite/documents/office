/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import java.lang.ref.WeakReference;
import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class SectionStartMarker implements ISectionWriter {

	private WeakReference<SectionEndMarker> sectionEndMarkerRef = null;

	final private AttributesImpl attributes;

	public SectionStartMarker(Attributes attributes) {
		this.attributes = new AttributesImpl(attributes);
	}

	public void setEndMarker(SectionEndMarker sectionEndMarker) {
		sectionEndMarkerRef = new WeakReference<SectionEndMarker>(sectionEndMarker);
	}

	@Override
	public void writeObject(SerializationHandler output) {

		// don't write this object without knowing if the corresponding end marker is stored within the same list

		return;
	}

	@Override
	public void writeSection(SerializationHandler output, DLList<Object> parentList)
		throws SAXException {

		// taking care that the end marker is available (in the same parent list) otherwise don't open this element

		if(sectionEndMarkerRef!=null) {
			final SectionEndMarker sectionEndMarker = sectionEndMarkerRef.get();
			if(sectionEndMarker!=null) {
				if(parentList.contains(sectionEndMarker)) {
					SaxContextHandler.startElement(output, Namespaces.TEXT, "section", "text:section");
					attributes.write(output);
				}
			}
		}
	}
}
