/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class TextListItem implements Cloneable {

    TextList parentTextList;
	boolean listHeader;

	private Integer startValue;
	private final String styleOverride;
	String textNumber;

	// each TextListItem must have a valid parentTextList ... null is not allowed
	// TODO: each textListItem should be part of a list in the parentTextList so it can better be determined which item belongs to which list...
	public TextListItem(TextList parentTextList, boolean listHeader, AttributesImpl attributes) {
		this.parentTextList = parentTextList;
		this.listHeader = listHeader;
		this.textNumber = null;

		final String s = attributes.getValue("text:start-value");
		this.startValue = s!=null && !s.isEmpty() ? Integer.parseInt(s) : null;
		this.styleOverride = listHeader ? null : attributes.getValue("text:style-override");
	}

	public TextListItem(TextList parentTextList) {
		this.parentTextList = parentTextList;
		this.listHeader = false;
		startValue = null;
		styleOverride = null;
	}

    public void writeAttributes(SerializationHandler output)
        throws SAXException {

        if(startValue!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TEXT, "start-value", "text:start-value", startValue.toString());
        }
    }

	public TextList getParentTextList() {
		return parentTextList;
	}

	public void setParentTextList(TextList parentTextList) {
	    this.parentTextList = parentTextList;
	}

	public int getListLevel() {
		return parentTextList.getListLevel();
	}

	public TextListItem getParentTextListItem(int level) {
		if(getListLevel()<level) {
			return null;
		}
		TextListItem ret = this;
		while(ret.getListLevel()>level) {
			ret = ret.getParentTextList().getParentTextListItem();
		}
		return ret;
	}

	public boolean isHeader() {
		return listHeader;
	}

	public void setIsHeader(boolean isHeader) {
	    this.listHeader = isHeader;
	}

    public Integer getStartValue() {
        return startValue;
    }

    public void setStartValue(Integer startValue) {
        this.startValue = startValue;
    }

	public void setTextNumber(String textNumber) {
		this.textNumber = textNumber;
	}

    @Override
    public TextListItem clone() {
        try {
            final TextListItem clone = (TextListItem)super.clone();
            if(parentTextList!=null) {
                parentTextList = parentTextList.clone();
            }
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
