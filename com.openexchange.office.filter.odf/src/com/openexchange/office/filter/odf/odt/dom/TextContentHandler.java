/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.odf.DOMBuilder;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.AutomaticStylesHandler;
import com.openexchange.office.filter.odf.styles.FontFaceDeclsHandler;

/**
 * @author sven.jacobi@open-xchange.com
 */
public class TextContentHandler extends DOMBuilder {

    // the empty XML file to which nodes will be added
    private final TextContent content;
    private OdfTextDocument textDocument = null;

    public TextContentHandler(Node rootNode, XMLReader xmlReader) {
    	super(rootNode, xmlReader, (TextContent)rootNode);

        // Initialize starting DOM node
        content = (TextContent)rootNode;
        textDocument = content.getDocument();
        textDocument.setContentDom(content);
    }

    public TextContent getContentDom() {
    	return content;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("office:automatic-styles")) {
    		return new AutomaticStylesHandler(this, content.getDocument().getStyleManager(), true);
    	}
    	else if(qName.equals("office:text")) {
    		final Body body = new Body(getFileDom(), attributes);
    		appendChild(body);
    		content.setBody(body);
    		return new BodyHandler(this, body);
    	}
    	else if(qName.equals("office:font-face-decls")) {
    		return new FontFaceDeclsHandler(this, content.getDocument().getStyleManager(), true);
    	}
    	// if we come to this postion, the element is inserted normally into the dom...
    	return super.startElement(attributes, uri, localName, qName);
    }

    @Override
    public void endElement(String localName, String qName) {
    	if(qName.equals("office:automatic-styles")||qName.equals("office:font-face-decls")) {
    		// do nothing since we do not want this element to be inserted into the dom,
    		// this is done elsewhere
    		return;
    	}
    	super.endElement(localName, qName);
    }
}
