/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import java.util.Set;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class TextField implements IElementWriter {

	static final public Set<String> TYPES = ImmutableSet.<String>builder()
		.add("author-initials")
		.add("author-name")
		.add("bookmark-ref")
		.add("chapter")
		.add("character-count")
		.add("conditional-text")
		.add("creation-date")
		.add("creation-time")
		.add("creator")
		.add("database-display")
		.add("database-name")
		.add("database-row-number")
		.add("database-row-select")
		.add("date")
		.add("dde-connection")
		.add("description")
		.add("editing-cycles")
		.add("editing-duration")
		.add("execute-macro")
		.add("expression")
		.add("file-name")
		.add("hidden-paragraph")
		.add("hidden-text")
		.add("image-count")
		.add("initial-creator")
		.add("keywords")
		.add("measure")
		.add("meta-field")
		.add("modification-date")
		.add("modification-time")
		.add("note-ref")
		.add("object-count")
		.add("page-continuation")
		.add("page-count")
		.add("page-number")
		.add("page-variable-get")
		.add("page-variable-set")
		.add("paragraph-count")
		.add("placeholder")
		.add("print-date")
		.add("print-time")
		.add("printed-by")
		.add("reference-ref")
		.add("script")
		.add("sender-city")
		.add("sender-company")
		.add("sender-country")
		.add("sender-email")
		.add("sender-fax")
		.add("sender-firstname")
		.add("sender-initials")
		.add("sender-lastname")
		.add("sender-phone-private")
		.add("sender-phone-work")
		.add("sender-position")
		.add("sender-postal-code")
		.add("sender-state-or-province")
		.add("sender-street")
		.add("sender-title")
		.add("sequence-ref")
		.add("sequence")
		.add("sheet-name")
		.add("subject")
		.add("table-count")
		.add("template-name")
		.add("text-input")
		.add("time")
		.add("title")
		.add("user-defined")
		.add("user-field-get")
		.add("user-field-input")
		.add("variable-get")
		.add("variable-input")
		.add("variable-set")
		.add("word-count")
		.add("footer")
		.add("header")
		.add("date-time")
		.build();

	private final AttributesImpl attributes;
	private String namespace;
	private String qName;
	private String localName;
	private String representation;

	public TextField(String namespace, String qName, String localName, Attributes attributes) {
	    this.namespace = namespace;
	    this.qName = qName;
		this.localName = localName;
		this.attributes = new AttributesImpl(attributes);

		representation = "";
	}

	public TextField() {
		this.attributes = new AttributesImpl();
		this.namespace = Namespaces.TEXT;
		this.qName = "text:user-defined";
		this.localName = "user-defined";
		this.representation = "";
	}

	public void setType(String type) {
		localName = type;
		if(type.equals("header")||type.equals("footer")||type.equals("date-time")) {
		    qName = "presentation:" + type;
		    namespace = Namespaces.PRESENTATION;
		}
		else {
		    qName = "text:" + type;
		    namespace = Namespaces.TEXT;
		}
	}

	public String getType() {
		return localName;
	}

	public void setRepresentation(String representation) {
		this.representation = representation;
	}

	public String getRepresentation() {
		return representation;
	}

	public AttributesImpl getAttributes() {
		return attributes;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, namespace, localName, qName);
		attributes.write(output);
		output.characters(representation);
		SaxContextHandler.endElement(output, namespace, localName, qName);
	}
}
