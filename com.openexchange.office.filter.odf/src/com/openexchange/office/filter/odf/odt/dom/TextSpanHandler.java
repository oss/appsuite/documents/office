/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.IContentDom;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawFrameHandler;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.draw.ShapeHelper;

public class TextSpanHandler extends SaxContextHandler {

    private final Paragraph paragraph;
    private final Attributes attributes;
    private TextSpan span = null;
    private String styleName = null;
    private StringBuffer text;
    private Hyperlink hyperlink;

    public TextSpanHandler(SaxContextHandler parentContext, Attributes attributes, Paragraph paragraph, Hyperlink hyperlink, String styleName) {
        super(parentContext);

        this.attributes = attributes;
        this.paragraph = paragraph;
        this.hyperlink = hyperlink;

        if(attributes!=null) {
            this.styleName = attributes.getValue("text:style-name");
        }
        if(this.styleName==null) {
            this.styleName = styleName;
        }
        text = new StringBuffer();
    }

    protected TextSpan getOrCreateSpan() {
        if(span==null) {
            span = new TextSpan(attributes, hyperlink, styleName);
            paragraph.getContent().add(span);
        }
        return span;
    }

    @Override
    public SaxContextHandler startElement(Attributes attr, String uri, String localName, String qName) {
        if(qName.equals("text:s")) {
            final String value = attr.getValue("text:c");
            int c = value != null ? Integer.valueOf(value) : 1;
            final StringBuffer buffer = new StringBuffer(c);
            for(int i=0; i<c; i++) {
                buffer.append(" ");
            }
            addCharacters(buffer.toString(), true);
            return new SaxContextHandler(this);
        }

        // this span is split via nested span or some other elements, so the current span has to be inserted to the paragraph content
        flushSpan();

        if(qName.equals("text:tab")) {
            getOrCreateSpan().getContent().add(new TextTab(attr));
            return this;
        }
        else if(qName.equals("text:line-break")) {
            getOrCreateSpan().getContent().add(new TextLineBreak());
            return this;
        }
        else if(qName.equals("text:span")) {
            span = null;
            return new TextSpanHandler(this, attr, paragraph, hyperlink, styleName);
        }
        else if(qName.equals("office:annotation")) {
            String name = attr.getValue("office:name");
            if(name==null||name.isEmpty()) {
                name = "cmt" + getFileDom().getNextMarkupId();
            }
            final Annotation annotation = new Annotation(name, attr, false);
            getFileDom().getTargetNodes().put(name, annotation);
            getOrCreateSpan().getContent().add(annotation);
            return new AnnotationHandler(this, annotation);
        }
        else if(qName.equals("office:annotation-end")) {
            final String name = attr.getValue("office:name");
            if(name!=null&&!name.isEmpty()) {
                final AnnotationEnd annotationEnd = new AnnotationEnd(name, attr);
                getOrCreateSpan().getContent().add(annotationEnd);
                return new AnnotationEndHandler(this, annotationEnd);
            }
        }
        else if(qName.equals("text:a")) {
            span = null;
            return new TextAHandler(this, attr, paragraph, styleName);
        }
        else if(qName.equals("draw:a")) {
            span = null;
            return new DrawAHandler(this, attr, paragraph, styleName);
        }
        else if(TextField.TYPES.contains(localName)) {
            final TextField field = new TextField(uri, qName, localName, attr);
            getOrCreateSpan().getContent().add(field);
            return new TextFieldHandler(this, field);
        }
        else if(qName.equals("draw:frame")) {
            final DrawFrame drawFrame = new DrawFrame(getFileDom().getDocument(), new AttributesImpl(attr), null, true, getFileDom() instanceof IContentDom);
            getOrCreateSpan().getContent().add(drawFrame);
            return new DrawFrameHandler(this, drawFrame);
        }
        final Shape shape = ShapeHelper.getShape(attr, uri, localName, qName, null, true, getFileDom() instanceof IContentDom);
        if(shape!=null) {
            getOrCreateSpan().getContent().add(shape);
            return shape.getContextHandler(this);
        }
        final ElementNSWriter element = new ElementNSWriter(getFileDom(), attr, uri, qName);
        getOrCreateSpan().getContent().add(element);
        return new UnknownContentHandler(this, element);
    }

    @Override
    public void endContext(String qName, String characters) {
        super.endContext(qName, characters);

        flushSpan();
    }

    @Override
    public void characters(String characters) {
        super.characters(characters);

        addCharacters(characters, false);
    }

    private void flushSpan() {
        if(text.length()!=0) {
            getOrCreateSpan().getContent().add(new Text(text.toString()));
            text.setLength(0);
        }
    }

    public void addCharacters(String t, boolean preserveWhiteSpace) {
        if(!preserveWhiteSpace) {
            int index = 0;
            while(index<t.length()) {
                final char c = t.charAt(index);
                if(c==' '||c==0x9||c==0xa||c==0xd) {
                    final int count = Text.countSpaces(t, index);
                    if(paragraph.getLastChar()!=' ') {
                        text.append(" ");
                        paragraph.setLastChar(' ');
                    }
                    index += count;
                }
                else {
                    final int count = Text.countNonSpaces(t, index);
                    text.append(t.substring(index, index + count));
                    paragraph.setLastChar(text.charAt(text.length()-1));
                    index += count;
                }
            }
        }
        else {
            text.append(t);
            paragraph.setLastChar(text.charAt(text.length()-1));
        }
    }
}
