/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odt.dom.components;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.draw.ConnectorShape;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.draw.Path;
import com.openexchange.office.filter.odf.draw.Shape;

public class ShapeConnectorComponent extends ShapeComponent {

	public ShapeConnectorComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> shapeNode, int componentNumber) {
		super(parentContext, shapeNode, componentNumber);
	}

	@Override
	public String simpleName() {
	    return "Connector";
	}

	@Override
    public DrawingType getType() {
		return ((Shape)getObject()).getType();
	}

	@Override
	public void applyAttrsFromJSON(JSONObject attrs)
			throws JSONException {

        // converting connector to path
        final Path path = ((ConnectorShape)getObject()).convertConnectorToPath(operationDocument, isContentAutoStyle());
        getNode().setData(path);
        path.applyAttrsFromJSON(operationDocument, attrs, isContentAutoStyle());
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {

		((Shape)getObject()).createAttrs(operationDocument, attrs, isContentAutoStyle());
	}
}
