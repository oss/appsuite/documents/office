/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.odt.dom;

import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.dom.OdfDocumentNamespace;
import org.odftoolkit.odfdom.pkg.NamespaceName;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.IContentDom;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.odt.dom.components.RootComponent;
import com.openexchange.office.filter.odf.styles.AutomaticStyles;
import com.openexchange.office.filter.odf.styles.FontFaceDecls;
import com.openexchange.office.filter.odf.styles.StyleManager;

/**
 * @author sven.jacobi@open-xchange.com
 */

/**
 * The DOM representation of the ODS content.xml file of an ODF document.
 */
@SuppressWarnings("serial")
public class TextContent extends OdfFileDom implements IContentDom {

	private Body body;

	/**
	 * Creates the DOM representation of an XML file of an Odf document.
	 *
	 * @param odfDocument   the document the XML files belongs to
	 * @param packagePath   the internal package path to the XML file
	 */
	public TextContent(OdfTextDocument odfDocument, String packagePath) throws SAXException {
		super(odfDocument, packagePath);
	}

	@Override
	protected void initialize() throws SAXException {
		for (NamespaceName name : OdfDocumentNamespace.values()) {
			mUriByPrefix.put(name.getPrefix(), name.getUri());
			mPrefixByUri.put(name.getUri(), name.getPrefix());
		}
		final StyleManager styleManager = getDocument().getStyleManager();
		styleManager.setFontFaceDecls(new FontFaceDecls(this), true);
		styleManager.setAutomaticStyles(new AutomaticStyles(this), true);

        final XMLReader xmlReader = mPackage.getXMLReader();
		super.initialize(new TextContentHandler(this, xmlReader), xmlReader);

        final Node root = getRootElement();
        root.insertBefore(styleManager.getAutomaticStyles(true), root.getFirstChild());
        root.insertBefore(styleManager.getFontFaceDecls(true), root.getFirstChild());
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	@Override
    public RootComponent getRootComponent(OdfOperationDoc operationDocument, String target) {
		INodeAccessor targetAccessor = body;
		if(target!=null&&!target.isEmpty()) {
			targetAccessor = getDocument().getTargetNodes().get(target);
		}
		return new RootComponent(operationDocument, targetAccessor, targetAccessor==body);
	}

    @Override
    public OdfTextDocument getDocument() {
        return (OdfTextDocument)mPackageDocument;
    }
}
