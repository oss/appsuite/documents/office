/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf;

public class Namespaces {

	public static final String ANIM = "urn:oasis:names:tc:opendocument:xmlns:animation:1.0";

	public static final String CALCEXT = "urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0";

	public static final String LOEXT = "urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0";

	public static final String OXEXT = "urn:org:documentfoundation:names:experimental:office:xmlns:oxext:1.0";

	public static final String CHART = "urn:oasis:names:tc:opendocument:xmlns:chart:1.0";

	public static final String CONFIG = "urn:oasis:names:tc:opendocument:xmlns:config:1.0";

	public static final String DB = "urn:oasis:names:tc:opendocument:xmlns:database:1.0";

	public static final String DC = "http://purl.org/dc/elements/1.1/";

	public static final String DR3D = "urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0";

	public static final String DRAW = "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0";

	public static final String FO = "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0";

	public static final String FORM = "urn:oasis:names:tc:opendocument:xmlns:form:1.0";

	public static final String GRDDL = "http://www.w3.org/2003/g/data-view#";

	public static final String MATH = "http://www.w3.org/1998/Math/MathML";

	public static final String META = "urn:oasis:names:tc:opendocument:xmlns:meta:1.0";

	public static final String NUMBER = "urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0";

	public static final String OF = "urn:oasis:names:tc:opendocument:xmlns:of:1.2";

	public static final String OFFICE = "urn:oasis:names:tc:opendocument:xmlns:office:1.0";

	public static final String PRESENTATION = "urn:oasis:names:tc:opendocument:xmlns:presentation:1.0";

	public static final String SCRIPT = "urn:oasis:names:tc:opendocument:xmlns:script:1.0";

	public static final String SMIL = "urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0";

	public static final String STYLE = "urn:oasis:names:tc:opendocument:xmlns:style:1.0";

	public static final String SVG = "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0";

	public static final String TABLE = "urn:oasis:names:tc:opendocument:xmlns:table:1.0";

	public static final String TEXT = "urn:oasis:names:tc:opendocument:xmlns:text:1.0";

	public static final String XFORMS = "http://www.w3.org/2002/xforms";

	public static final String XHTML = "http://www.w3.org/1999/xhtml";

	public static final String XLINK = "http://www.w3.org/1999/xlink";

	public static final String XML = "http://www.w3.org/XML/1998/namespace";
}
