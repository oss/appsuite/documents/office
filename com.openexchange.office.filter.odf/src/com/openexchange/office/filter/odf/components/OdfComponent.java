/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.components;

import java.util.Set;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.odftoolkit.odfdom.doc.OdfChartDocument;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.dom.OdfDocumentNamespace;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.MetaData;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawObject;
import com.openexchange.office.filter.odf.ods.dom.chart.ChartContent;

public abstract class OdfComponent extends ComponentContext<OdfOperationDoc> implements IComponent<OdfOperationDoc> {

    private int componentNumber;

    @Override
    public int getComponentNumber() {
        return componentNumber;
    }

    @Override
    public void setComponentNumber(int componentNumber) {
        this.componentNumber = componentNumber;
    }

    @Override
    public int getNextComponentNumber() {
        return componentNumber + 1;
    }

    protected static boolean contentAutoStyle;

    public OdfComponent(OdfOperationDoc operationDocument, DLNode<Object> o, int componentNumber) {
        super(operationDocument, o);
        this.componentNumber = componentNumber;
    }

    public OdfComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> o, int componentNumber) {
        super(parentContext, o);
        this.componentNumber = componentNumber;
    }

    public boolean isContentAutoStyle() {
        return contentAutoStyle;
    }

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(int number, JSONObject attrs, ComponentType type) throws Exception {
        final IComponent<OdfOperationDoc> c = insertChildComponent(this, getNode(), number, getChildComponent(number), type, attrs);
        if(attrs!=null) {
            c.applyAttrsFromJSON(attrs);
        }
        return c;
    }

    @Override
    public IComponent<OdfOperationDoc> getNextComponent() {
        if(getParentContext()==null) {
            return null;
        }
        ComponentContext<OdfOperationDoc> parentContext = this;
        IComponent<OdfOperationDoc> nextComponent = null;
        do {
            ComponentContext<OdfOperationDoc> previousContext = parentContext;
            parentContext = parentContext.getParentContext();
            nextComponent = parentContext.getNextChildComponent(previousContext, this);
        }
        while(nextComponent==null&&!(parentContext instanceof IComponent));
        return nextComponent;
    }

    @Override
    public IComponent<OdfOperationDoc> getComponent(int number) {
        IComponent<OdfOperationDoc> c = this;
        while(c!=null&&c.getNextComponentNumber()<=number) {
            c = c.getNextComponent();
        }
        return c;
    }

    @Override
    public IComponent<OdfOperationDoc> getComponent(JSONArray position, int positionCount) {
        try {
            if(positionCount==0) {
                return this;
            }
            // a part has to implement the ContentAccessor interface, otherwise no component can be accessed
            IComponent<OdfOperationDoc> c = getNextChildComponent(null, null);
            for(int i=0; c!=null&&i<positionCount;) {
                c = c.getComponent(position.getInt(i++));
                if(i!=positionCount) {
                    c = c.getNextChildComponent(null, null);
                }
            }
            return c;
        }
        catch(JSONException e) {
            // ups
        }
        return null;
    }

    @Override
    public IComponent<OdfOperationDoc> getParentComponent() {
        ComponentContext<OdfOperationDoc> parentContext = getParentContext();
        while(!(parentContext instanceof IComponent)) {
            parentContext = parentContext.getParentContext();
        }
        return (IComponent<OdfOperationDoc>)parentContext;
    }

    @Override
    public IComponent<OdfOperationDoc> getChildComponent(int childComponentNumber) {
        IComponent<OdfOperationDoc> c = getNextChildComponent(null, null);
        if(c!=null) {
            c = c.getComponent(childComponentNumber);
        }
        return c;
    }

    @Override
    public ComponentContext<OdfOperationDoc> getContextChild(Set<Class<?>> parentContextList) {
        ComponentContext<OdfOperationDoc> childContext = this;
        ComponentContext<OdfOperationDoc> parentContext = getParentContext();
        while(!(parentContext instanceof IComponent)&&((parentContextList==null)||(!parentContextList.contains(parentContext.getClass())))) {
            childContext = parentContext;
            parentContext = childContext.getParentContext();
        }
        return childContext;
    }

    public DrawFrame getChart(OdfOperationDoc operationDoc, boolean contentStyle) {

        try {

            final OdfSpreadsheetDocument document = (OdfSpreadsheetDocument)operationDoc.getDocument();

            // the default is two cell anchor ...
            final DrawFrame drawFrame = new DrawFrame(operationDoc, null, true, contentStyle);
            final DrawObject drawObject= new DrawObject(drawFrame);
            drawFrame.getContent().add(drawObject);
            OdfPackage pkg = document.getPackage();

            long index = 1;
            final String prefix = "Object ";

            Set<String> filePaths = pkg.getFilePaths();
            for (String path : filePaths) {
                if (path.startsWith(prefix) && path.endsWith("/content.xml")) {
                    try {
                        long other = Long.parseLong(path.replace(prefix, "").replace("/content.xml", ""));

                        index = Math.max(other + 1, index);

                    } catch (NumberFormatException e) {
                        Logger.getAnonymousLogger().info(e.toString());
                    }
                }
            }

            String name = prefix + index;

            final StringBuilder bContent = new StringBuilder();
            bContent.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><office:document-content");
            for (OdfDocumentNamespace nameSpace : OdfDocumentNamespace.values()) {
                bContent.append(" xmlns:" + nameSpace.getPrefix() + "=\"" + nameSpace.getUri() + "\"");
            }
            bContent.append("><office:body><office:chart></office:chart></office:body></office:document-content>");
            byte[] contentBytes = bContent.toString().getBytes();

            pkg.insert("".getBytes(), name + "/", OdfChartDocument.OdfMediaType.CHART.getMediaTypeString());
            pkg.insert(contentBytes, name + "/content.xml", "text/xml");

            pkg.insert("<?xml version=\"1.0\" encoding=\"UTF-8\"?><office:document-styles xmlns:office=\"urn:oasis:names:tc:opendocument:xmlns:office:1.0\" xmlns:style=\"urn:oasis:names:tc:opendocument:xmlns:style:1.0\" xmlns:text=\"urn:oasis:names:tc:opendocument:xmlns:text:1.0\" xmlns:table=\"urn:oasis:names:tc:opendocument:xmlns:table:1.0\" xmlns:draw=\"urn:oasis:names:tc:opendocument:xmlns:drawing:1.0\" xmlns:fo=\"urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:number=\"urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0\" xmlns:svg=\"urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0\" xmlns:chart=\"urn:oasis:names:tc:opendocument:xmlns:chart:1.0\" xmlns:dr3d=\"urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0\" xmlns:script=\"urn:oasis:names:tc:opendocument:xmlns:script:1.0\" xmlns:ooo=\"http://openoffice.org/2004/office\" xmlns:ooow=\"http://openoffice.org/2004/writer\" xmlns:oooc=\"http://openoffice.org/2004/calc\" xmlns:dom=\"http://www.w3.org/2001/xml-events\" xmlns:rpt=\"http://openoffice.org/2005/report\" xmlns:of=\"urn:oasis:names:tc:opendocument:xmlns:of:1.2\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\" xmlns:grddl=\"http://www.w3.org/2003/g/data-view#\" xmlns:tableooo=\"http://openoffice.org/2009/table\" xmlns:chartooo=\"http://openoffice.org/2010/chart\" xmlns:drawooo=\"http://openoffice.org/2010/draw\" xmlns:calcext=\"urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0\" xmlns:loext=\"urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0\" xmlns:field=\"urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0\" xmlns:css3t=\"http://www.w3.org/TR/css3-text/\" office:version=\"1.2\"><office:styles/></office:document-styles>".getBytes(), name + "/styles.xml", "text/xml");

            OdfChartDocument chartDoc = (OdfChartDocument)document.loadSubDocument("./" + name);

            // retrieving chart content
            final ChartContent chart = (ChartContent) chartDoc.getContentDom();
            drawObject.setChart(chart);

            ElementNS officeChart = (ElementNS) chart.getElementsByTagName("office:chart").item(0);

            ElementNS chartArea = chart.newChartElement("chart");
            ElementNS plotArea = chart.newChartElement("plot-area");
            ElementNS wall = chart.newChartElement("wall");

            JSONObject fillNone = new JSONObject(new JSONTokener("{" + OCKey.FILL.value() + ":{" + OCKey.TYPE.value() + ":'none'}}"));

            chart.applyStyleAttrs(fillNone, wall);

            officeChart.appendChild(chartArea);
            chartArea.appendChild(plotArea);
            plotArea.appendChild(wall);

            chart.setChartArea(chartArea);
            chart.setPlotArea(plotArea);

            chartDoc.getOfficeMetaData(true);
            chartDoc.getStylesDom();

            MetaData docMeta = document.getOfficeMetaData(false);
            if (docMeta != null) {
                String generator = docMeta.getGenerator();
                if (generator != null && !generator.isEmpty()) {
                    chartDoc.getOfficeMetaData(false).setGenerator(generator);
                }

            }

            final AttributesImpl drawObjectAttributes = drawObject.getAttributes();
            drawObjectAttributes.setValue(Namespaces.XLINK, "href", "xlink:href", "./" + name);
            drawObjectAttributes.setValue(Namespaces.XLINK, "actuate", "xlink:actuate", "onLoad");
            drawObjectAttributes.setValue(Namespaces.XLINK, "show", "xlink:show", "embed");
            drawObjectAttributes.setValue(Namespaces.XLINK, "type", "xlink:type", "simple");
            drawObjectAttributes.setValue(Namespaces.DRAW, "name", "draw:name", name);
            return drawFrame;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int count) {

    	final ComponentContext<OdfOperationDoc> contextChild = getContextChild(null);

    	final int endComponent = (getComponentNumber()+count)-1;
        IComponent<OdfOperationDoc> component = this;
        while(true) {
        	if(component.getNextComponentNumber()>endComponent) {
        		break;
        	}
        	component = component.getNextComponent();
        }
        component.splitEnd(endComponent, SplitMode.DELETE);
        final DLList<Object> content = ((INodeAccessor)contextChild.getParentContext().getObject()).getContent();
        content.removeNodes(contextChild.getNode(), component.getContextChild(null).getNode());
    }

    public static void move(OdfComponent rootComponent, JSONArray start, JSONArray end, JSONArray to)
        throws JSONException {

        if(end==null) {
            end = start;
        }
        else if(start.length()!=end.length())
            throw new JSONException("ooxml export: move operation, size of startPosition != size of endPosition");

        final int s = start.getInt(start.length()-1);
        final int t = to.getInt(to.length()-1);
        IComponent<OdfOperationDoc> sourceBegComponent = rootComponent.getComponent(start, start.length());
        IComponent<OdfOperationDoc> sourceEndComponent = rootComponent.getComponent(end, end.length());
        sourceBegComponent.splitStart(s, SplitMode.DELETE);
        sourceEndComponent.splitEnd(end.getInt(end.length()-1), SplitMode.DELETE);

        // destComponent is empty if the content is to be appended
        final IComponent<OdfOperationDoc> destComponent = rootComponent.getComponent(to, to.length());
        IComponent<OdfOperationDoc> destParentComponent;
        if(destComponent!=null) {
            destComponent.splitStart(to.getInt(to.length()-1), SplitMode.DELETE);
            destParentComponent = destComponent.getParentComponent();
        }
        else {
            destParentComponent = rootComponent.getComponent(to, to.length()-1);
        }
        final DLNode<Object> sourceStartContextNode = sourceBegComponent.getContextChild(null).getNode();
        final DLNode<Object> sourceEndContextNode = sourceEndComponent.getContextChild(null).getNode();
        final DLList<Object> sourceContent = ((INodeAccessor)sourceBegComponent.getParentComponent().getObject()).getContent();
        final DLList<Object> destContent = ((INodeAccessor)destParentComponent.getObject()).getContent();
        final boolean before = sourceContent==destContent && s <= t ? false : true;
        sourceContent.moveNodes(sourceStartContextNode, sourceEndContextNode, destContent, destComponent!=null ? destComponent.getContextChild(null).getNode() :  null, before);
    }

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> contextNode, int number, IComponent<OdfOperationDoc> child, ComponentType type, JSONObject attrs) {
    	return null;
    }

    public abstract void createJSONAttrs(OpAttrs attrs)
    	throws SAXException;
}
