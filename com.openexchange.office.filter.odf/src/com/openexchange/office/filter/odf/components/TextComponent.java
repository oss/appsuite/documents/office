/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.components;

import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.odt.dom.Text;
import com.openexchange.office.filter.odf.odt.dom.TextSpan;

public class TextComponent extends TextSpan_Base {

	public TextComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> textNode, int componentNumber) {
		super(parentContext, textNode, componentNumber);
	}

	@Override
	public String simpleName() {
	    final StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Text");
        stringBuffer.append('\'' + ((Text)getObject()).getText() + '\'');
        return stringBuffer.toString();
	}

	@Override
    public int getNextComponentNumber() {
        int textLength = ((Text)getObject()).getText().length();
        if (textLength==0) {
            System.out.println("Error: empty text string is no component... this results in problems with the component iterator");
            textLength++;       // increasing.. preventing an endless loop
        }
        return getComponentNumber() + textLength;
    }

    @Override
    public void splitStart(int componentPosition, SplitMode mode) {
    	final int splitPosition = componentPosition-getComponentNumber();
    	if(splitPosition>0) {
    		splitText(splitPosition, true);
    		setComponentNumber(getComponentNumber() + splitPosition);
    	}
    	splitTextSpan(true);
    }

    @Override
    public void splitEnd(int componentPosition, SplitMode mode) {
    	if(getNextComponentNumber()>++componentPosition) {
    		splitText(componentPosition-getComponentNumber(), false);
    	}
    	splitTextSpan(false);
    }

    private void splitText(int textSplitPosition, boolean splitStart) {
    	final Text text = (Text)getNode().getData();
        if(textSplitPosition>0&&textSplitPosition<text.getText().length()) {
        	final DLNode<Object> textSpanNode = getParentContext().getNode();
        	final TextSpan textSpan = (TextSpan)textSpanNode.getData();
        	final DLList<Object> textSpanContent = textSpan.getContent();
            final StringBuffer s = new StringBuffer(text.getText());
            if(splitStart) {
            	final Text newText = new Text(s.substring(0, textSplitPosition));
                textSpanContent.addNode(getNode(), new DLNode<Object>(newText), true);
                text.setText(s.substring(textSplitPosition));
            }
            else {
            	final Text newText = new Text(s.substring(textSplitPosition));
            	textSpanContent.addNode(getNode(), new DLNode<Object>(newText), false);
                text.setText(s.substring(0, textSplitPosition));
            }
        }
    }
}
