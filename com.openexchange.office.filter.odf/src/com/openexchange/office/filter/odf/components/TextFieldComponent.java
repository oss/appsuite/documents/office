/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.components;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;
import org.odftoolkit.odfdom.dom.OdfDocumentNamespace;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.odt.dom.TextField;
import com.openexchange.office.filter.odf.styles.NumberStyleBase;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class TextFieldComponent extends TextSpan_Base {

    public TextFieldComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> textFieldNode, int componentNumber) {
        super(parentContext, textFieldNode, componentNumber);
    }

    @Override
    public String simpleName() {
        return "Field";
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

        final JSONObject character = attrs.optJSONObject(OCKey.CHARACTER.value());
        if(character!=null) {
            final JSONObject json = character.optJSONObject(OCKey.FIELD.value());
            if(json!=null) {
                final AttributesImpl attributes = ((TextField)getObject()).getAttributes();
                final Iterator<Entry<String, Object>> jsonIter = json.entrySet().iterator();
                final HashMap<String, String> additionalStyleProperties = new HashMap<String, String>();
                Object formatCode = null;
                while(jsonIter.hasNext()) {
                    final Entry<String, Object> jsonEntry = jsonIter.next();
                    if(jsonEntry.getValue() instanceof String) {
                        final String qName = jsonEntry.getKey();
                        if(qName.equals("dateFormat")) {
                            formatCode = jsonEntry.getValue();
                        }
                        else if(qName.equals("pageNumFormat")) {
                            if(jsonEntry.getValue() instanceof String) {
                                attributes.setValue(Namespaces.STYLE, "num-format", "style:num-format", (String)jsonEntry.getValue());
                            }
                        }
                        else if(qName.equals("number:automatic-order")) {
                            additionalStyleProperties.put("number:automatic-order", (String)jsonEntry.getValue());
                        }
                        else if(qName.equals("number:format-source")) {
                            additionalStyleProperties.put("number:format-source", (String)jsonEntry.getValue());
                        }
                        else {
                            final int index = qName.indexOf(':');
                            if(index>0&&qName.length()>index+1) {
                                final String prefix = qName.substring(0, index);
                                final String localName = qName.substring(index + 1);
                                final String uri = OdfDocumentNamespace.getUri(prefix);
                                if(uri!=null) {
                                    attributes.setValue(uri, localName, qName, (String)jsonEntry.getValue());
                                }
                            }
                        }
                    }
                }
                if(formatCode!=null) {
                    if(formatCode==JSONObject.NULL) {
                        attributes.remove("style:date-style-name");
                    }
                    else if(formatCode instanceof String&&!((String)formatCode).isEmpty()) {
                        attributes.setValue(Namespaces.STYLE, "data-style-name", "style:data-style-name",
                            operationDocument.getDocument().getStyleManager().applyDataStyle((String)formatCode, additionalStyleProperties, -1, false, true, isContentAutoStyle()));
                    }
                }
            }
        }
        super.applyAttrsFromJSON(attrs);
    }

    @Override
    public void createJSONAttrs(OpAttrs attrs) {
        super.createJSONAttrs(attrs);

        final Map<String, AttributeImpl> attributes = ((TextField)getObject()).getAttributes().getUnmodifiableMap();
        if(attributes.size()>0) {
            final HashMap<String, String> map = new HashMap<String, String>(attributes.size());
            final Iterator<AttributeImpl> attrIter = attributes.values().iterator();
            while(attrIter.hasNext()) {
                final AttributeImpl attr = attrIter.next();
                if(attr.getQName().equals("style:data-style-name")) {
                    final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
                    final StyleBase dataStyle = styleManager.getStyle(attr.getValue(), StyleFamily.DATA_STYLE, isContentAutoStyle());
                    if(dataStyle instanceof NumberStyleBase) {
                        final String formatCode = ((NumberStyleBase)dataStyle).getFormat(styleManager, map, isContentAutoStyle());
                        if(formatCode!=null&&!formatCode.isEmpty()) {
                            map.put("dateFormat", formatCode);
                        }
                    }
                }
                else if(attr.getQName().equals("style:num-format")) {
                    map.put("pageNumFormat", attr.getValue());
                }
                else {
                    map.put(attr.getQName(), attr.getValue());
                }
            }
            attrs.getMap(OCKey.CHARACTER.value(), true).put(OCKey.FIELD.value(), map);
        }
    }
}
