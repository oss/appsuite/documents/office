/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.components;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.odt.dom.Hyperlink;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;
import com.openexchange.office.filter.odf.odt.dom.TextSpan;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

public abstract class TextSpan_Base extends OdfComponent {

    final Paragraph paragraph;

    public TextSpan_Base(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> _textNode, int _componentNumber) {
        super(parentContext, _textNode, _componentNumber);
        paragraph = ((Paragraph)getParentContext().getParentContext().getNode().getData());
    }

    public TextSpan getTextSpan() {
    	return (TextSpan)getTextSpanNode().getData();
    }

    public DLNode<Object> getTextSpanNode() {
    	return getParentContext().getNode();
    }

	@Override
    public void delete(int count) {
	    if(getComponentNumber()==0&&getNextComponent()==null) {
            final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
            final OpAttrs opAttrs = new OpAttrs();
	        styleManager.collectAllTextPropertiesFromStyle(opAttrs, getTextSpan().getStyleName(), StyleFamily.TEXT, isContentAutoStyle());
	        try {
	            final JSONObject attrs = new JSONObject(opAttrs);
	            final StyleBase styleBase = styleManager.getStyleBaseClone(StyleFamily.PARAGRAPH, paragraph.getStyleName(), isContentAutoStyle());
	            styleManager.applyAttrs(styleBase, isContentAutoStyle(), attrs);
	            paragraph.setStyleName(styleManager.getStyleIdForStyleBase(styleBase));
	        }
	        catch(JSONException e) {
	            //
	        }
	    }
        super.delete(count);
    }

    protected void splitTextSpan(boolean before) {
		final DLNode<Object> refNode = getNode();
		final TextSpan textSpan = getTextSpan();
		final DLNode<Object> textSpanNode = getTextSpanNode();
		final DLList<Object> textSpanContent = ((TextSpan)textSpanNode.getData()).getContent();
		final DLList<Object> paragraphContent = paragraph.getContent();
		if(before&&!refNode.isFirst()) {
			final TextSpan destTextSpan = textSpan.clone();
			final DLNode<Object> destTextSpanNode = new DLNode<Object>(destTextSpan);
			paragraphContent.addNode(textSpanNode, destTextSpanNode, true);

			DLNode<Object> currentNode = textSpanContent.getFirstNode();
			while(currentNode!=refNode) {
				textSpanContent.removeNode(currentNode);
				destTextSpan.getContent().addNode(currentNode);
				currentNode = textSpanContent.getFirstNode();
			}
		}
		else if(!before&&!refNode.isLast()) {
			final TextSpan destTextSpan = textSpan.clone();
			final DLNode<Object> destTextSpanNode = new DLNode<Object>(destTextSpan);
			paragraphContent.addNode(textSpanNode, destTextSpanNode, false);

			while(!refNode.isLast()) {
				destTextSpan.getContent().addNode(textSpanContent.removeNode(refNode.getNext()));
			}
		}
    }

	@Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {
		return null;
	}

	@Override
	public void applyAttrsFromJSON(JSONObject attrs) throws Exception {

	    if(attrs==null||attrs.isEmpty()) {
			return;
		}

		final TextSpan textSpan = getTextSpan();
		try {
		    textSpan.setStyleName(operationDocument.getDocument().getStyleManager().createStyle(StyleFamily.TEXT, textSpan.getStyleName(), isContentAutoStyle(), attrs));
		} catch (JSONException e) {
		    throw new RuntimeException(e);
        }
		final JSONObject characterAttrs = attrs.optJSONObject(OCKey.CHARACTER.value());
		if(characterAttrs!=null) {
			final Object url = characterAttrs.opt(OCKey.URL.value());
			if(url instanceof String) {
				textSpan.getHyperlink(Namespaces.TEXT, true).setHRef((String)url);
			}
			else if(url==JSONObject.NULL) {
				textSpan.setHyperlink(null);
			}
		}
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {

		final TextSpan textSpan = (TextSpan)getParentContext().getNode().getData();
		if(textSpan.getStyleName()!=null) {
			operationDocument.getDocument().getStyleManager().
				createAutoStyleAttributes(attrs, textSpan.getStyleName(), StyleFamily.TEXT, isContentAutoStyle());
		}
		final Hyperlink hyperlink = textSpan.getHyperlink(null, false);
		if(hyperlink!=null) {
			if(hyperlink.getHRef()!=null) {
				attrs.getMap(OCKey.CHARACTER.value(), true).put(OCKey.URL.value(), hyperlink.getHRef());
			}
		}
		return;
	}

	@Override
	public void splitStart(int n, SplitMode splitMode) {
    	splitTextSpan(true);
	}

	@Override
	public void splitEnd(int n, SplitMode splitMode) {
    	splitTextSpan(false);
	}
}
