/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.doc.OdfPresentationDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.CUtil;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Authors;
import com.openexchange.office.filter.odf.IParagraph;
import com.openexchange.office.filter.odf.ITable;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.Settings;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.components.TextFieldComponent;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.GroupShape;
import com.openexchange.office.filter.odf.draw.Transformer;
import com.openexchange.office.filter.odf.odp.dom.components.ParagraphComponent;
import com.openexchange.office.filter.odf.odp.dom.components.RowComponent;
import com.openexchange.office.filter.odf.odp.dom.components.ShapeGroupComponent;
import com.openexchange.office.filter.odf.odp.dom.components.SlideComponent;
import com.openexchange.office.filter.odf.odt.dom.Annotation;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;
import com.openexchange.office.filter.odf.odt.dom.TextField;
import com.openexchange.office.filter.odf.odt.dom.TextList;
import com.openexchange.office.filter.odf.odt.dom.TextListItem;
import com.openexchange.office.filter.odf.styles.DocumentStyles;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StylePresentation;
import com.openexchange.office.filter.odf.styles.TextListStyle;

public class JsonOperationConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(JsonOperationConsumer.class);

    private final OdfOperationDoc opsDoc;
    private final OdfPresentationDocument doc;
    private final DocumentStyles styles;
    private final PresentationContent content;
    private final StyleManager styleManager;
    private final Settings settings;

    public JsonOperationConsumer(OdfOperationDoc opsDoc)
		throws SAXException {

		this.opsDoc = opsDoc;
		doc = (OdfPresentationDocument)opsDoc.getDocument();
		styles = doc.getStylesDom();
		content = (PresentationContent)doc.getContentDom();
		styleManager = doc.getStyleManager();
	    settings = doc.getSettingsDom();
	}

    public int applyOperations(JSONArray operations)
    	throws Exception {

    	for (int i = 0; i < operations.length(); i++) {
    	    doc.getPackage().setSuccessfulAppliedOperations(i);
            final JSONObject op = (JSONObject) operations.get(i);
            final String target = op.optString(OCKey.TARGET.value(), "");
            final OCValue opName = OCValue.fromValue(op.getString(OCKey.NAME.value()));
            switch(opName)
            {
                case INSERT_PARAGRAPH : {
                    insertParagraph(op.getJSONArray(OCKey.START.value()), target, op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case SPLIT_PARAGRAPH : {
                    splitParagraph(op.getJSONArray(OCKey.START.value()), target);
                    break;
                }
                case MERGE_PARAGRAPH : {
                    mergeParagraph(op.getJSONArray(OCKey.START.value()), target);
                    break;
                }
            	case INSERT_TEXT : {
                    insertText(op.getJSONArray(OCKey.START.value()), target, op.optJSONObject(OCKey.ATTRS.value()), op.getString(OCKey.TEXT.value()).replaceAll("\\p{Cc}", " "));
                    break;
            	}
                case DELETE : {
                    CUtil.delete(content.getRootComponent(opsDoc, target), op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()));
                    break;
                }
                case SET_ATTRIBUTES : {
                    final JSONArray start = op.getJSONArray(OCKey.START.value());
                    final JSONObject attrs = op.getJSONObject(OCKey.ATTRS.value());
                    if(start.length()>=3&&start.length()<=3) {
                        if(target!=null&&!target.isEmpty()) {
                            final INodeAccessor nodeAccessor = doc.getTargetNodes().get(target);
                            if(nodeAccessor!=null) {
                                final Object masterPage = nodeAccessor.getContent().get(start.getInt(0));
                                if(masterPage instanceof MasterPage) {
                                    final Object drawFrame = ((MasterPage)masterPage).getContent().get(start.getInt(1));
                                    if(drawFrame instanceof DrawFrame) {
                                        final String presentationClass = ((DrawFrame)drawFrame).getAttributes().getValue("presentation:class");
                                        if(presentationClass!=null) {
                                            if(presentationClass.equals("title")||presentationClass.equals("outline")) {
                                                final Paragraph p = (Paragraph)((DrawFrame)drawFrame).getContent().get(start.getInt(2));
                                                final int listLevel = p.getListLevel() + 1;
                                                String presentationStyleName = ((DrawFrame)drawFrame).getAttributes().getValue("presentation:style-name");
                                                if(presentationStyleName!=null&&!presentationStyleName.isEmpty()) {
                                                    StylePresentation presentationStyle = (StylePresentation)styleManager.getStyle(presentationStyleName, StyleFamily.PRESENTATION, false);
                                                    if(presentationStyle!=null) {
                                                        final TextListStyle listStyle = presentationStyle.getGraphicProperties().getTextListStyle();
                                                        String nextPresentationStyleName = null;
                                                        if(presentationClass.equals("outline")&&presentationStyleName.charAt(presentationStyleName.length()-1)=='1') {
                                                            if(listLevel>1&&listLevel<=9) {
                                                                presentationStyleName = presentationStyleName.substring(0, presentationStyleName.length()-1) + Integer.toString(listLevel);
                                                                presentationStyle = (StylePresentation)styleManager.getStyle(presentationStyleName, StyleFamily.PRESENTATION, false);
                                                            }
                                                            if(listLevel<9) {
                                                                nextPresentationStyleName = presentationStyleName.substring(0, presentationStyleName.length()-1) + Integer.toString(listLevel+1);
                                                            }
                                                        }

                                                        final JSONObject paraAttrs = attrs.optJSONObject(OCKey.PARAGRAPH.value());
                                                        final JSONObject charAttrs = attrs.optJSONObject(OCKey.CHARACTER.value());

                                                        if(nextPresentationStyleName!=null) {
                                                            final StylePresentation nextPresentationStyle = (StylePresentation)styleManager.getStyle(nextPresentationStyleName, StyleFamily.PRESENTATION, false);
                                                            if(nextPresentationStyle!=null) {
                                                                // fixing style that is child of current level
                                                                final OpAttrs parentListLevelAttrs = new OpAttrs();
                                                                styleManager.createPresentationAttributes(presentationStyle, "outline", listLevel, false, parentListLevelAttrs);
                                                                final OpAttrs childAttrs = new OpAttrs();
                                                                nextPresentationStyle.createAttrs(styleManager, childAttrs);

                                                                final JSONObject newChildAttrs = new JSONObject();
                                                                if(paraAttrs!=null&&!paraAttrs.isEmpty()) {
                                                                    final HashSet<String> attrsThatNeedsDefault = new HashSet<String>();
                                                                    this.mergeChildOutlineAttributes(attrsThatNeedsDefault, parentListLevelAttrs, childAttrs, newChildAttrs, listLevel, paraAttrs, OCKey.PARAGRAPH.value());
                                                                    if(!attrsThatNeedsDefault.isEmpty()) {
                                                                        nextPresentationStyle.getParagraphProperties().applyHardDefaultAttributes(attrsThatNeedsDefault);
                                                                    }
                                                                }
                                                                if(charAttrs!=null&&!charAttrs.isEmpty()) {
                                                                    final HashSet<String> attrsThatNeedsDefault = new HashSet<String>();
                                                                    this.mergeChildOutlineAttributes(attrsThatNeedsDefault, parentListLevelAttrs, childAttrs, newChildAttrs, listLevel, charAttrs, OCKey.CHARACTER.value());
                                                                    if(!attrsThatNeedsDefault.isEmpty()) {
                                                                        nextPresentationStyle.getTextProperties().applyHardDefaultAttributes(attrsThatNeedsDefault);
                                                                    }
                                                                }
                                                                if(!newChildAttrs.isEmpty()) {
                                                                    nextPresentationStyle.applyAttrs(styleManager, newChildAttrs);
                                                                }
                                                            }
                                                        }
                                                        presentationStyle.applyAttrs(styleManager, attrs);
                                                        if(listStyle!=null) {
                                                            listStyle.applyPresentationListStyleAttributes(styleManager, attrs, listLevel);
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    setAttributes(attrs, target, start, op.optJSONArray(OCKey.END.value()));
                    break;
                }
                case INSERT_SLIDE : {
                    insertSlide(op.getJSONArray(OCKey.START.value()), target, op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_MASTER_SLIDE : {
                    insertMasterSlide(op.getString(OCKey.ID.value()), op.optInt(OCKey.START.value(), -1), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_DRAWING : {
                    insertDrawing(op.getJSONArray(OCKey.START.value()), target, op.getString(OCKey.TYPE.value()), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case SET_DOCUMENT_ATTRIBUTES : {
                    setDocumentAttributes(op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_HARD_BREAK : {
                    insertHardBreak(op.getJSONArray(OCKey.START.value()), target, /* op.optString(OCKey.TYPE.shortName()), */ op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case MOVE_SLIDE : {
                    moveSlide(op.getJSONArray(OCKey.START.value()), op.getJSONArray(OCKey.END.value()));
                    break;
                }
                case INSERT_TAB : {
                    insertTab(op.getJSONArray(OCKey.START.value()), target, op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_FIELD : {
                    insertField(op.getJSONArray(OCKey.START.value()), target, op.getString(OCKey.TYPE.value()), op.getString(OCKey.REPRESENTATION.value()), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case UPDATE_FIELD : {
                    updateField(op.getJSONArray(OCKey.START.value()), target, op.optString(OCKey.TYPE.value()), op.optString(OCKey.REPRESENTATION.value()), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case GROUP : {
                    group(op.getJSONArray(OCKey.START.value()), target, op.getJSONArray(OCKey.DRAWINGS.value()), op.getJSONObject(OCKey.ATTRS.value()), op.optJSONArray("childAttrs"));
                    break;
                }
                case UNGROUP : {
                    ungroup(op.getJSONArray(OCKey.START.value()), target, op.optJSONArray(OCKey.DRAWINGS.value()));
                    break;
                }
                case MOVE : {
                    move(op.getJSONArray(OCKey.START.value()), target, op.optJSONArray(OCKey.END.value()), op.getJSONArray(OCKey.TO.value()));
                    break;
                }
                case INSERT_ROWS : {
                    insertRows(op.getJSONArray(OCKey.START.value()), target, op.optInt(OCKey.COUNT.value(), 1), op.optBoolean(OCKey.INSERT_DEFAULT_CELLS.value(), false), op.optInt(OCKey.REFERENCE_ROW.value(), -1), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_CELLS : {
                    insertCells(op.getJSONArray(OCKey.START.value()), target, op.optInt(OCKey.COUNT.value(), 1), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_COLUMN : {
                    insertColumn(op.getJSONArray(OCKey.START.value()), target, op.getJSONArray(OCKey.TABLE_GRID.value()), op.getInt(OCKey.GRID_POSITION.value()), op.optString(OCKey.INSERT_MODE.value(), "before"));
                    break;
                }
                case DELETE_COLUMNS : {
                    deleteColumns(op.getJSONArray(OCKey.START.value()), target, op.getInt(OCKey.START_GRID.value()), op.optInt(OCKey.END_GRID.value(), op.getInt(OCKey.START_GRID.value())));
                    break;
                }
                case CHANGE_MASTER : {
                    changeMaster(op.getJSONArray(OCKey.START.value()), target);
                    break;
                }
                case INSERT_STYLE_SHEET : {
                    changeOrInsertStyleSheet(true, op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()), op.getString(OCKey.STYLE_NAME.value()), op.getJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.PARENT.value(), null), op.optInt(OCKey.UI_PRIORITY.value(), 0), op.optBoolean(OCKey.DEFAULT.value(), false));
                    break;
                }
                case CHANGE_STYLE_SHEET : {
                    changeOrInsertStyleSheet(false, op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()), op.getString(OCKey.STYLE_NAME.value()), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.PARENT.value(), null), op.optInt(OCKey.UI_PRIORITY.value(), 0), op.optBoolean(OCKey.DEFAULT.value(), false));
                    break;
                }
                case DELETE_STYLE_SHEET : {
                    deleteStyleSheet(op.getString(OCKey.TYPE.value()), op.getString(OCKey.STYLE_ID.value()));
                    break;
                }
                case INSERT_COMMENT : {
                    insertComment(op.getJSONArray(OCKey.START.value()), target, op.getJSONArray(OCKey.POS.value()), op.getString(OCKey.TEXT.value()), op.getString(OCKey.DATE.value()), op.optString(OCKey.AUTHOR.value()), op.optString(OCKey.INITIALS.value()), op.optInt(OCKey.PARENT.value()));
                    break;
                }
                case CHANGE_COMMENT : {
                    changeComment(op.getJSONArray(OCKey.START.value()), target, op.optJSONArray(OCKey.POS.value()), op.optString(OCKey.TEXT.value()));
                    break;
                }
                case DELETE_COMMENT : {
                    deleteComment(op.getJSONArray(OCKey.START.value()), target);
                    break;
                }
                case NO_OP : {
                    break;
                }
                case CREATE_ERROR : {
                    throw new FilterException("createError operation detected: " + opName, ErrorCode.UNSUPPORTED_OPERATION_USED);
                }
                default: {
                    final String unsupportedOps = opName==OCValue.UNKNOWN_VALUE ? op.getString(OCKey.NAME.value()) : opName.value(true);
                    LOG.warn("Ignoring unsupported operation: " + unsupportedOps);
                }
            }
        }
    	Transformer.prepareSave(opsDoc);
        doc.getPackage().setSuccessfulAppliedOperations(operations.length());
        return 1;
    }

    private void mergeChildOutlineAttributes(HashSet<String> attrsNeedsDefault, OpAttrs parentListLevelAttrs, OpAttrs _childAttrs, JSONObject _newChildAttrs, int listLevel, JSONObject attrs,  String type)
        throws JSONException {

        final OpAttrs parentAttrs = parentListLevelAttrs.getMap(OCKey.LIST_STYLES.value(), true).getMap("outline",  true).getMap("l" + Integer.toString(listLevel), true).getMap(type, true);
        final OpAttrs childAttrs = _childAttrs.getMap(type, true);
        final JSONObject newChildAttrs = new JSONObject();

        final Iterator<String> paraKeyIter = attrs.keys();
        while(paraKeyIter.hasNext()) {
            final String paraKey = paraKeyIter.next();
            if(!childAttrs.containsKey(paraKey)) {
                if(parentAttrs.containsKey(paraKey)) {
                    final Object o = parentAttrs.get(paraKey);
                    if(o instanceof Map) {
                        final HashMap<String, Object> destAttr = new HashMap<String, Object>();
                        StyleManager.deepCopy((Map)o, destAttr);
                        newChildAttrs.put(paraKey, destAttr);
                    }
                    else {
                        newChildAttrs.put(paraKey, o);
                    }
                }
                else {
                    attrsNeedsDefault.add(paraKey);
                }
            }
        }
        if(!newChildAttrs.isEmpty()) {
            _newChildAttrs.put(type, newChildAttrs);
        }
    }

    public void insertParagraph(JSONArray start, String target, JSONObject attrs) throws Exception {

        boolean createDefaultTextListItem = true;
        if(attrs!=null) {
            final JSONObject paragraphAttrs = attrs.optJSONObject(OCKey.PARAGRAPH.value());
            if(paragraphAttrs!=null) {
                final JSONObject bullet = paragraphAttrs.optJSONObject(OCKey.BULLET.value());
                if(bullet!=null) {
                    createDefaultTextListItem = !"none".equals(bullet.opt(OCKey.TYPE.value()));
                }
            }
        }

        if(createDefaultTextListItem) {
            final ParagraphComponent paragraphComponent = (ParagraphComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
                .insertChildComponent(start.getInt(start.length()-1), null, ComponentType.PARAGRAPH);

            // now apply the correct listItem
            final SlideComponent slideComponent = (SlideComponent)content.getRootComponent(opsDoc, target).getComponent(start, 1);
            final TextListStyle defaultTextListStyle = doc.getDefaultTextListStyle(opsDoc, paragraphComponent.getPresentationType(), slideComponent.getPage(), false);
            final TextList textList = new TextList(styleManager, null);
            textList.setStyleName(defaultTextListStyle.getName());
            paragraphComponent.getParagraph().setTextListItem(new TextListItem(textList));

            if(attrs!=null) {
                paragraphComponent.applyAttrsFromJSON(attrs);
            }
        }
        else {  // no list item needed ... insert paragraph child in one step
            content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
                .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.PARAGRAPH);
        }
    }

    public void splitParagraph(JSONArray start, String target)
        throws JSONException {

        ((IParagraph)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1))
            .splitParagraph(start.getInt(start.length()-1));
    }

    public void mergeParagraph(JSONArray start, String target) {

        ((IParagraph)content.getRootComponent(opsDoc, target).getComponent(start, start.length()))
            .mergeParagraph();
    }

    public void insertText(JSONArray start, String target, JSONObject attrs, String text)
        throws Exception {

        if(text.length()>0) {
            final IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1);
            ((IParagraph)component).insertText(start.getInt(start.length()-1), text, attrs);
        }
    }

    public void insertTab(JSONArray start, String target, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.TAB);
    }

    public void insertHardBreak(JSONArray start, String target, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.HARDBREAK);
    }

    public void setAttributes(JSONObject attrs, String target, JSONArray start, JSONArray end) throws Exception {

        if(attrs==null) {
            return;
        }
        int startIndex = start.getInt(start.length()-1);
        int endIndex = startIndex;

        if(end!=null) {
            if(end.length()!=start.length())
                return;
            endIndex = end.getInt(end.length()-1);
        }
        IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, target).getComponent(start, start.length());
        component.splitStart(startIndex, SplitMode.DELETE);
        while(component!=null&&component.getComponentNumber()<=endIndex) {
            if(component.getNextComponentNumber()>=endIndex+1) {
                component.splitEnd(endIndex, SplitMode.DELETE);
            }
            component.applyAttrsFromJSON(attrs);
            component = component.getNextComponent();
        }
    }

    public IComponent<OdfOperationDoc> insertDrawing(JSONArray start, String target, String type, JSONObject attrs)
        throws Exception {

        ComponentType childComponentType = ComponentType.AC_IMAGE;
        if(type.equals("shape")) {
            if(attrs==null||!attrs.has(OCKey.GEOMETRY.value())) {
                childComponentType = ComponentType.AC_FRAME;
            }
            else {
                childComponentType = ComponentType.AC_SHAPE;
            }
        }
        else if(type.equals("connector")) {
            childComponentType = ComponentType.AC_CONNECTOR;
        }
        else if(type.equals("group")) {
            childComponentType = ComponentType.AC_GROUP;
        }
        else if(type.equals("image")) {
            childComponentType = ComponentType.AC_IMAGE;
        }
        else if(type.equals("table")) {
            childComponentType = ComponentType.TABLE;
        }
        return content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, childComponentType);
    }

    public void insertTable(JSONArray start, String target, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.TABLE);
    }

    public void splitTable(JSONArray start, String target) throws Exception {

        final ITable splitTableComponent = (ITable)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-2)
            .insertChildComponent(start.getInt(start.length()-2) + 1, null, ComponentType.TABLE);

        ((ITable)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1))
            .splitTable(start.getInt(start.length()-1), splitTableComponent);
    }

    public void mergeTable(JSONArray start, String target) {
        ((ITable)content.getRootComponent(opsDoc, target).getComponent(start, start.length())).mergeTable();
    }

    public void insertRows(JSONArray start, String target, int count, boolean insertDefaultCells, int referenceRow, JSONObject attrs) throws Exception {

        ((ITable)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1))
            .insertRows(start.getInt(start.length()-1), count, insertDefaultCells, referenceRow, attrs);
    }

    public void insertCells(JSONArray start, String target, int count, JSONObject attrs) throws Exception {

        ((RowComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1))
            .insertCells(start.getInt(start.length()-1), count, attrs);
    }

    public void insertColumn(JSONArray start, String target, JSONArray tableGrid, int gridPosition, String insertMode) throws Exception {

        ((ITable)content.getRootComponent(opsDoc, target).getComponent(start, start.length()))
            .insertColumn(tableGrid, gridPosition, insertMode);
    }

    public void deleteColumns(JSONArray position, String target, int gridStart, int gridEnd) {

        ((ITable)content.getRootComponent(opsDoc, target).getComponent(position, position.length()))
            .deleteColumns(gridStart, gridEnd);
    }

    public void insertSlide(JSONArray position, String target, JSONObject attrs) throws Exception {
    	final DrawingPage drawingPage = new DrawingPage(new AttributesImpl());
    	drawingPage.setMasterPageName(target);
    	drawingPage.setName(getUniquePageName());
    	final int slideNumber = position.getInt(0);
    	content.getPresentation().getContent().add(slideNumber, drawingPage);
    	if(attrs!=null&&!attrs.isEmpty()) {
            final DLNode<Object> drawingPageNode = new DLNode<Object>(drawingPage);
    	    final SlideComponent slideComponent = new SlideComponent(content.getRootComponent(opsDoc, target), drawingPageNode, slideNumber, true);
    	    slideComponent.applyAttrsFromJSON(attrs);
    	}
    }

    private String getUniquePageName() {
        int i = content.getPresentation().getContent().size();
        while(true) {
            final String newPageName = "p" + Integer.valueOf(++i).toString();
            if(getPageByName(newPageName)==null) {
                return newPageName;
            }
        }
    }

    // returns the page with the given name, returns null if name is not available
    private DrawingPage getPageByName(String name) {
        final DLList<Object> presContent = content.getPresentation().getContent();
        for(Object o:presContent) {
            if(o instanceof DrawingPage) {
                if(name.equals(((DrawingPage)o).getName())) {
                    return (DrawingPage)o;
                }
            }
        }
        return null;
    }

    public void insertMasterSlide(String id, int slideNumber, JSONObject attrs) throws Exception {
        final MasterPage masterPage = new MasterPage(new AttributesImpl());
        masterPage.setName(id);
        final DLNode<Object> masterPageNode = new DLNode<Object>(masterPage);
        final DLList<Object> masterStylesContent = styleManager.getMasterStyles().getContent();
        final Iterator<DLNode<Object>> masterStylesContentIter = masterStylesContent.getNodeIterator();
        int f = 0;
        DLNode<Object> refNode = null;
        while(masterStylesContentIter.hasNext()) {
            final DLNode<Object> styleContentEntry = masterStylesContentIter.next();
            if(styleContentEntry.getData() instanceof MasterPage) {
                if(f==slideNumber) {
                    refNode = styleContentEntry;
                    break;
                }
                f++;
            }
        }
        masterStylesContent.addNode(refNode, masterPageNode, true);
        styles.getTargetNodes().put(id, new MasterPageWrapper(id, masterPage));
        if(attrs!=null&&!attrs.isEmpty()) {
            final SlideComponent slideComponent = new SlideComponent(content.getRootComponent(opsDoc, id), masterPageNode, slideNumber, false);
            slideComponent.applyAttrsFromJSON(attrs);
        }
        styleManager.insertStyleSheet("presentation", id + "-background", null, attrs, null, false);
    }

    public void moveSlide(JSONArray start, JSONArray end) throws JSONException {
        final DLList<Object> slideList = content.getPresentation().getContent();
        final Object o = slideList.remove(start.getInt(0));
        slideList.add(end.getInt(0), o);
    }

    public void setDocumentAttributes(JSONObject attrs)
        throws JSONException {

        final Iterator<Object> masterStyleContentIter = styleManager.getMasterStyles().getContent().iterator();
        MasterPage masterPage = null;
        while(masterStyleContentIter.hasNext()) {
            final Object o = masterStyleContentIter.next();
            if(o instanceof MasterPage) {
                masterPage = (MasterPage)o;
                break;
            }
        }
        if(masterPage!=null) {
            final StyleBase pageLayout = styleManager.getStyle(masterPage.getPageLayoutName(), StyleFamily.PAGE_LAYOUT, false);
            if(pageLayout!=null) {
                pageLayout.applyAttrs(styleManager, attrs);
            }
        }
    }

    public void insertField(JSONArray start, String target, String type, String representation, JSONObject attrs) throws Exception {

        final TextField textField = (TextField)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.FIELD).getObject();

        textField.setType(type);
        textField.setRepresentation(representation);
    }

    public void updateField(JSONArray start, String target, String type, String representation, JSONObject attrs) throws Exception {

        final TextFieldComponent tfComponent = (TextFieldComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length());
        if(type!=null) {
            ((TextField)tfComponent.getObject()).setType(type);
        }
        if(representation!=null) {
            ((TextField)tfComponent.getObject()).setRepresentation(representation);
        }
    }

    public void group(JSONArray start, String target, JSONArray drawings, JSONObject attrs, JSONArray childAttrs) throws Exception {

        final List<Object> drawingChildObjects = new ArrayList<Object>();
        for(int i = drawings.length()-1; i >= 0; i--) {
            final IComponent<OdfOperationDoc> child = content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1).getChildComponent(drawings.getInt(i));
            if(child.getObject() instanceof GroupShape) {
                ((GroupShape)child.getObject()).prepareSaveTopLevelGroupShape(styleManager);
            }
            drawingChildObjects.add(0, child.getObject());
            child.splitStart(child.getComponentNumber(), SplitMode.DELETE);
            child.delete(1);
        }
        final IComponent<OdfOperationDoc> groupComponent = insertDrawing(start, target, "group", attrs);
        final GroupShape groupShape = (GroupShape)groupComponent.getObject();
        groupShape.getContent().addAll(drawingChildObjects);
        groupShape.initializeTopLevelGroupShape(styleManager);
        if(attrs!=null) {
            //
        }
    }

    public void ungroup(JSONArray start, String target, JSONArray drawings) throws Exception {

        final int groupPosition = start.getInt(start.length()-1);
        final IComponent<OdfOperationDoc> parentComponent = content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1);
        final DLList<Object> parentContent = ((INodeAccessor)parentComponent.getObject()).getContent();
        final ShapeGroupComponent groupComponent = (ShapeGroupComponent)parentComponent.getChildComponent(groupPosition);
        final GroupShape group = (GroupShape)groupComponent.getObject();
        group.convertConnectorToPath(opsDoc, groupComponent.isContentAutoStyle());
        group.prepareSaveTopLevelGroupShape(styleManager);

        final DLList<Object> groupContent = group.getContent();

        groupComponent.splitStart(groupComponent.getComponentNumber(), SplitMode.DELETE);
        groupComponent.delete(1);

        IComponent<OdfOperationDoc> referenceComponent = parentComponent.getChildComponent(groupPosition);
        DLNode<Object> referenceNode = referenceComponent!=null ? referenceComponent.getNode() : null;

        for(int i = 0; i < groupContent.size(); i++) {
            if(drawings!=null&&i<drawings.length()) {
                referenceComponent = parentComponent.getChildComponent(drawings.getInt(i));
                referenceNode = referenceComponent!=null ? referenceComponent.getNode() : null;
            }
            final Object newChild = groupContent.get(i);
            final DLNode<Object> newChildNode = new DLNode<Object>(newChild);
            parentContent.addNode(referenceNode, newChildNode, true);
        }
    }

    public void move(JSONArray start, String target, JSONArray end, JSONArray to)
        throws JSONException {

        OdfComponent.move(content.getRootComponent(opsDoc, target), start, end, to);
    }

    public void changeMaster(JSONArray start, String target) throws JSONException {
        ((DrawingPage)content.getPresentation().getContent().get(start.getInt(0))).setMasterPageName(target);
    }

    public void changeOrInsertStyleSheet(boolean insert, String type, String styleId, String styleName, JSONObject attrs, String parent, int uiPriority, boolean defaultStyle)
        throws JSONException {

        if(type.equals("table")) {
            if(insert) {
                styleManager.insertStyleSheet("table-template", styleId, styleName, attrs, parent, defaultStyle);
            }
            else {
                final StyleBase templateStyle = styleManager.getStyle(styleId, StyleFamily.TABLE_TEMPLATE, false);
                if(templateStyle!=null) {
                    templateStyle.applyAttrs(styleManager, attrs);
                }
            }
        }
    }

    public void deleteStyleSheet(String type, String id) {
        if(type.equals("table")) {
            styleManager.removeStyle(StyleFamily.TABLE_TEMPLATE, id, false, false);
        }
    }

    private void insertComment(JSONArray start, String target, JSONArray pos, String text, String date, String author, String initials, Integer parentIndex)
        throws JSONException {

        final SlideComponent slideComponent = (SlideComponent)content.getRootComponent(opsDoc, target).getComponent(start, 1);
        final Authors authors = doc.getAuthors(true);
        final Page page = slideComponent.getPage();
        final DLList<Annotation> annotations = page.getAnnotations(true);
        final Annotation annotation = new Annotation("", true);
        annotation.setCreator(author);
        annotation.setInitials(initials);
        annotations.add(start.getInt(1), annotation);
        annotation.getTransformer().setX(pos.getInt(0));
        annotation.getTransformer().setY(pos.getInt(1));
        TextContentHelper.setSimpleText(annotation, text);
        annotation.setDate(date);
        authors.addAuthor(annotation);
    }

    private void changeComment(JSONArray start, String target, JSONArray pos, String text) throws JSONException {

        final SlideComponent slideComponent = (SlideComponent)content.getRootComponent(opsDoc, target).getComponent(start, 1);
        final Page page = slideComponent.getPage();
        final DLList<Annotation> annotations = page.getAnnotations(true);
        final Annotation annotation = annotations.get(start.getInt(1));
        if(pos!=null) {
            annotation.getTransformer().setX(pos.getInt(0));
            annotation.getTransformer().setY(pos.getInt(1));
        }
        if(text!=null) {
            TextContentHelper.setSimpleText(annotation, text);
        }
    }

    private void deleteComment(JSONArray start, String target)
        throws JSONException {

        final SlideComponent slideComponent = (SlideComponent)content.getRootComponent(opsDoc, target).getComponent(start, 1);
        final Page page = slideComponent.getPage();
        final DLList<Annotation> annotations = page.getAnnotations(true);
        annotations.remove(start.getInt(1));
    }
}
