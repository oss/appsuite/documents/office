/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom.components;

import org.json.JSONObject;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.odp.dom.DrawingPage;
import com.openexchange.office.filter.odf.odp.dom.MasterPage;
import com.openexchange.office.filter.odf.odp.dom.MasterPageWrapper;

public class RootComponent extends OdfComponent {

	public RootComponent(OdfOperationDoc operationDocument, INodeAccessor nodeAccessor) {
		super(operationDocument, new DLNode<Object>(nodeAccessor), 0);
		contentAutoStyle = !(nodeAccessor instanceof MasterPageWrapper);
	}

    @Override
    public String simpleName() {
        return "Root";
    }

    @Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {

        final int nextComponentNumber = previousChildComponent != null ? previousChildComponent.getNextComponentNumber() : 0;
        DLNode<Object> nextNode = previousChildContext != null ? previousChildContext.getNode().getNext() : ((INodeAccessor)getObject()).getContent().getFirstNode();
		while(nextNode!=null) {
			if(nextNode.getData() instanceof MasterPage) {
			    return new SlideComponent(this, nextNode, nextComponentNumber, false);
			}
			else if(nextNode.getData() instanceof DrawingPage) {
			    return new SlideComponent(this, nextNode, nextComponentNumber, true);
			}
			nextNode = nextNode.getNext();
		}
		return null;
	}

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> contextNode, int number, IComponent<OdfOperationDoc> child, ComponentType type, JSONObject attrs) {
        throw new UnsupportedOperationException();
    }

	@Override
	public void applyAttrsFromJSON(JSONObject attrs) {

		// TODO Auto-generated method stub
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {

		// TODO Auto-generated method stub
		return;
	}
}
