/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom.components;

import java.util.Iterator;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.doc.OdfPresentationDocument;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.IParagraph;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.components.TextComponent;
import com.openexchange.office.filter.odf.components.TextSpan_Base;
import com.openexchange.office.filter.odf.listlevel.ListLevelStyleBullet;
import com.openexchange.office.filter.odf.listlevel.ListLevelStyleEntry;
import com.openexchange.office.filter.odf.listlevel.ListLevelStyleImage;
import com.openexchange.office.filter.odf.listlevel.ListLevelStyleNumber;
import com.openexchange.office.filter.odf.odp.dom.Page;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;
import com.openexchange.office.filter.odf.odt.dom.Text;
import com.openexchange.office.filter.odf.odt.dom.TextField;
import com.openexchange.office.filter.odf.odt.dom.TextLineBreak;
import com.openexchange.office.filter.odf.odt.dom.TextList;
import com.openexchange.office.filter.odf.odt.dom.TextListItem;
import com.openexchange.office.filter.odf.odt.dom.TextSpan;
import com.openexchange.office.filter.odf.odt.dom.TextTab;
import com.openexchange.office.filter.odf.properties.TextProperties;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.TextListStyle;

public class ParagraphComponent extends OdfComponent implements IParagraph {

	final Paragraph paragraph;

	public ParagraphComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> paragraphNode, int componentNumber) {
		super(parentContext, paragraphNode, componentNumber);

		paragraph = (Paragraph)getObject();
	}

    @Override
    public String simpleName() {
        return "Para";
    }

    public Paragraph getParagraph() {
        return paragraph;
    }

    /*
	 * returns the object type the paragraph parent
	 *
	 * it can be "title", "body" or "other"
	 */
	public String getPresentationType() {
        if(getParentComponent() instanceof FrameComponent) {
            final String presentationClass = ((FrameComponent)getParentComponent()).getDrawFrame().getPresentationClass();
            if("title".equals(presentationClass)) {
                return "title";
            }
            if("outline".equals(presentationClass)) {
                return "body";
            }
            if("subtitle".equals(presentationClass)) {
                return "subtitle";
            }
        }
        return "other";
	}

	@Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {

        DLNode<Object> nextNode = previousChildContext != null ? previousChildContext.getNode().getNext() : ((Paragraph)getObject()).getContent().getFirstNode();
        while(nextNode!=null) {
			final Object child = nextNode.getData();
			if(child instanceof TextSpan) {
				final TextSpanContext textSpanContext =  new TextSpanContext(this, nextNode, contentAutoStyle);
				final IComponent<OdfOperationDoc> childComponent = textSpanContext.getNextChildComponent(null, previousChildComponent);
				if(childComponent!=null) {
					return childComponent;
				}
			}
			nextNode = nextNode.getNext();
		}
		return null;
	}

	@Override
	public void applyAttrsFromJSON(JSONObject attrs) {

		if(attrs!=null) {
		    try {
		        // applying standard paragraph attributes ...
                final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
                paragraph.setStyleName(styleManager.createStyle(StyleFamily.PARAGRAPH, paragraph.getStyleName(), isContentAutoStyle(), attrs));

                final JSONObject paragraphAttrs = attrs.optJSONObject(OCKey.PARAGRAPH.value());
                if(paragraphAttrs!=null) {
                    final Object listLevel = paragraphAttrs.opt(OCKey.LEVEL.value());
                    if(listLevel!=null) {
                        if(listLevel instanceof Integer) {
                            paragraph.setListLevel(styleManager, ((Integer)listLevel).intValue());
                        }
                        else if(listLevel==JSONObject.NULL) {
                            paragraph.setListLevel(styleManager, 0);
                        }
                    }
                    final Object bullet = paragraphAttrs.opt(OCKey.BULLET.value());
                    if(bullet instanceof JSONObject&&("none".equals(((JSONObject)bullet).opt(OCKey.TYPE.value())))) {
                        paragraph.setTextListItem(null);
                    }
                    else {

                        final Object bulletSize = paragraphAttrs.opt(OCKey.BULLET_SIZE.value());
                        final Object bulletColor = paragraphAttrs.opt(OCKey.BULLET_COLOR.value());
                        final Object bulletFont = paragraphAttrs.opt(OCKey.BULLET_FONT.value());

                        if(bullet!=null||bulletSize!=null||bulletColor!=null||bulletFont!=null) {

                            int level = paragraph.getListLevel();
                            if(level==-1) {
                                paragraph.setListLevel(styleManager, 0);
                                level = 0;
                            }

                            // a textListItem is needed
                            TextListStyle textListStyleClone = null;

                            // first try to clone the current textListStyle...
                            if(paragraph.getTextListItem()!=null) {
                                final String textListStyleId = paragraph.getTextListItem().getParentTextList().getStyleName(true);
                                if(textListStyleId!=null&&!textListStyleId.isEmpty()) {
                                    final StyleBase currentTextListStyle = styleManager.getStyle(textListStyleId, StyleFamily.LIST_STYLE, isContentAutoStyle());
                                    if(currentTextListStyle!=null) {
                                        textListStyleClone = ((TextListStyle)currentTextListStyle).clone();
                                    }
                                }
                            }
                            if(textListStyleClone==null) {
                                // if there is no current textListStyle, the default is cloned
                                textListStyleClone = ((OdfPresentationDocument)operationDocument.getDocument()).getDefaultTextListStyle(operationDocument, getPresentationType(), getPage(), true);
                            }
                            if(bullet!=null) {
                                if(paragraph.getTextListItem()==null) {
                                    paragraph.setTextListItem(new TextListItem(new TextList(styleManager, null)));
                                }
                                for(int l=1; l<=10; l++) {

                                    // ... apply bulletSize, type, color and font ...
                                    ListLevelStyleEntry currentListLevelEntry = textListStyleClone.getListLevelEntry(l);
                                    if(bullet instanceof JSONObject) {
                                        Object bulletType = ((JSONObject)bullet).opt(OCKey.TYPE.value());
                                        if("character".equals(bulletType)) {
                                            if(!(currentListLevelEntry instanceof ListLevelStyleBullet)) {
                                                currentListLevelEntry = new ListLevelStyleBullet(new AttributesImpl());
                                                currentListLevelEntry.setListLevel(l);
                                            }
                                        }
                                        else if("bitmap".equals(bulletType)) {
                                            if(!(currentListLevelEntry instanceof ListLevelStyleImage)) {
                                                currentListLevelEntry = new ListLevelStyleImage(new AttributesImpl());
                                                currentListLevelEntry.setListLevel(l);
                                            }
                                        }
                                        else if("numbering".equals(bulletType)) {
                                            if(!(currentListLevelEntry instanceof ListLevelStyleNumber)) {
                                                currentListLevelEntry = new ListLevelStyleNumber(new AttributesImpl());
                                                currentListLevelEntry.setListLevel(l);
                                            }
                                        }
                                    }

                                    // default needed ?
                                    if(currentListLevelEntry==null||bullet==JSONObject.NULL||bulletSize==JSONObject.NULL||bulletColor==JSONObject.NULL||bulletFont==JSONObject.NULL) {
                                        final TextListStyle textListStyleDefault = ((OdfPresentationDocument)operationDocument.getDocument()).getDefaultTextListStyle(operationDocument, getPresentationType(), getPage(), true);
                                        final ListLevelStyleEntry defaultListLevelEntry = textListStyleDefault.getListLevelEntry(l);
                                        if(defaultListLevelEntry!=null) {
                                            if(currentListLevelEntry==null) {
                                                currentListLevelEntry = defaultListLevelEntry.clone();
                                            }
                                            else {

                                                // apply defaults ...
                                                if(bullet==JSONObject.NULL) {
                                                    final TextProperties currentTextProperties = currentListLevelEntry.getTextProperties(false);
                                                    final String oldColor = currentTextProperties!=null ? currentTextProperties.getAttribute("fo:color") : null;
                                                    final String oldFontFamily = currentTextProperties!=null ? currentTextProperties.getAttribute("fo:font-family") : null;
                                                    final String oldFontName = currentTextProperties!=null ? currentTextProperties.getAttribute("style:font-name") : null;
                                                    final String oldFontSize = currentTextProperties!=null ? currentTextProperties.getAttribute("fo:font-size") : null;
                                                    currentListLevelEntry = defaultListLevelEntry.clone();
                                                    if(oldColor!=null) {
                                                        currentListLevelEntry.getTextProperties(true).getAttributes().setValue(Namespaces.FO, "color", "fo:color", oldColor);
                                                    }
                                                    if(oldFontFamily!=null) {
                                                        currentListLevelEntry.getTextProperties(true).getAttributes().setValue(Namespaces.FO, "font-family", "fo:font-family", oldFontFamily);
                                                    }
                                                    if(oldFontName!=null) {
                                                        currentListLevelEntry.getTextProperties(true).getAttributes().setValue(Namespaces.STYLE, "font-name", "style:font-name", oldFontFamily);
                                                    }
                                                    if(oldFontSize!=null) {
                                                        currentListLevelEntry.getTextProperties(true).getAttributes().setValue(Namespaces.FO, "font-size", "fo:font-size", oldFontSize);
                                                    }
                                                }
                                                final TextProperties defaultTextProperties = defaultListLevelEntry.getTextProperties(false);
                                                if(defaultTextProperties!=null) {
                                                    if(bulletSize==JSONObject.NULL) {
                                                        currentListLevelEntry.getTextProperties(true).getAttributes().setValue(Namespaces.FO, "font-size", "fo:font-size", defaultTextProperties.getAttribute("fo:font-size"));
                                                    }
                                                    if(bulletColor==JSONObject.NULL) {
                                                        currentListLevelEntry.getTextProperties(true).getAttributes().setValue(Namespaces.FO, "color", "fo:color", defaultTextProperties.getAttribute("fo:color"));
                                                    }
                                                    if(bulletFont==JSONObject.NULL) {
                                                        currentListLevelEntry.getTextProperties(true).getAttributes().setValue(Namespaces.FO, "font-family", "fo:font-family", defaultTextProperties.getAttribute("fo:font-family"));
                                                        currentListLevelEntry.getTextProperties(true).getAttributes().setValue(Namespaces.STYLE, "font-name", "fo:font-name", defaultTextProperties.getAttribute("style:font-name"));
                                                    }
                                                }
                                            }
                                        }
                                        else if(currentListLevelEntry==null) {
                                            currentListLevelEntry = new ListLevelStyleBullet(l);
                                        }
                                    }

                                    // change currentListLevelEntry
                                    if(bullet!=null||bulletSize!=null||bulletColor!=null||bulletFont!=null) {
                                        final JSONObject tempParagraphAttrs = new JSONObject();
                                        final JSONObject bulletAttrs = paragraphAttrs.optJSONObject(OCKey.BULLET.value());
                                        if(bulletAttrs!=null) {
                                            tempParagraphAttrs.put(OCKey.BULLET.value(), bulletAttrs);
                                        }
                                        final JSONObject bulletSizeAttrs = paragraphAttrs.optJSONObject(OCKey.BULLET_SIZE.value());
                                        if(bulletSizeAttrs!=null) {
                                            tempParagraphAttrs.put(OCKey.BULLET_SIZE.value(), bulletSizeAttrs);
                                        }
                                        final JSONObject bulletColorAttrs = paragraphAttrs.optJSONObject(OCKey.BULLET_COLOR.value());
                                        if(bulletColorAttrs!=null) {
                                            tempParagraphAttrs.put(OCKey.BULLET_COLOR.value(), bulletColorAttrs);
                                        }
                                        final JSONObject bulletFontAttrs = paragraphAttrs.optJSONObject(OCKey.BULLET_FONT.value());
                                        if(bulletFontAttrs!=null) {
                                            tempParagraphAttrs.put(OCKey.BULLET_FONT.value(), bulletFontAttrs);
                                        }
                                        currentListLevelEntry.applyPresentationAttrs(styleManager, tempParagraphAttrs);
                                    }
                                    // apply the current ListLevelEntry to our clone
                                    textListStyleClone.setListLevelEntry(currentListLevelEntry);
                                }

                                // check if there is a listLevelStyle (auto style) already available
                                String newTextListStyleId = null;
                                final String existingStyleId = styleManager.getExistingStyleIdForStyleBase(textListStyleClone);
                                if(existingStyleId!=null) {
                                    newTextListStyleId = existingStyleId;
                                }
                                else {
                                    // otherwise we will add a new list style
                                    textListStyleClone.setName(styleManager.getUniqueStyleName(StyleFamily.LIST_STYLE, isContentAutoStyle()));
                                    styleManager.addStyle(textListStyleClone);
                                    newTextListStyleId = textListStyleClone.getName();
                                }
                                paragraph.getTextListItem().setParentTextList(new TextList(styleManager, paragraph.getTextListItem().getParentTextList().getParentTextListItem()));
                                paragraph.getTextListItem().getParentTextList().setStyleName(newTextListStyleId);
                            }
                        }
                    }
                }
		    }
            catch(Exception e) {
                throw new RuntimeException(e);
            }
		}
	}

	private Page getPage() {
	    IComponent<OdfOperationDoc> parentComponent = getParentComponent();
	    while(parentComponent!=null) {
	        if(parentComponent instanceof SlideComponent) {
	            return ((SlideComponent)parentComponent).getPage();
	        }
	        parentComponent = parentComponent.getParentComponent();
	    }
	    return null;
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {
		final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
		if(paragraph.getStyleName()!=null&&!paragraph.getStyleName().isEmpty()) {
			styleManager.createAutoStyleAttributes(attrs, paragraph.getStyleName(), StyleFamily.PARAGRAPH, isContentAutoStyle());
		}
		final OpAttrs paragraphAttrs  = attrs.getMap(OCKey.PARAGRAPH.value(), true);
		TextListItem textListItem = paragraph.getTextListItem();
        if(textListItem!=null&&!textListItem.isHeader()) {
            final TextList textList = textListItem.getParentTextList();
            if(textList!=null) {
                final String styleName = textList.getStyleName(true);
                if(styleName!=null&&!styleName.isEmpty()) {
                    final StyleBase listStyle = styleManager.getStyle(styleName, StyleFamily.LIST_STYLE, isContentAutoStyle());
                    if(listStyle instanceof TextListStyle) {
                        // TODO: direct access to ListLevelStyleEntry ... only 10 are available
                        final Iterator<IElementWriter> listStyleIter = ((TextListStyle)listStyle).getContent().iterator();
                        while(listStyleIter.hasNext()) {
                            final IElementWriter n = listStyleIter.next();
                            if(n instanceof ListLevelStyleEntry) {
                                if(((ListLevelStyleEntry)n).getListLevel(0)-1==textListItem.getListLevel()) {
                                    ((ListLevelStyleEntry)n).createPresentationAttrs(styleManager, "other", isContentAutoStyle(), paragraphAttrs);
                                    ListLevelStyleEntry.finalizeParagraphAttrs(paragraphAttrs);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
			paragraphAttrs.put(OCKey.LEVEL.value(), textListItem.getListLevel());
			final Integer startValue = paragraph.getStartValue();
			if(startValue!=null) {
				paragraphAttrs.put(OCKey.LIST_START_VALUE.value(), startValue);
			}
			final Integer outlineLevel = paragraph.getOutlineLevel();
			if(outlineLevel!=null) {
				paragraphAttrs.put(OCKey.OUTLINE_LEVEL.value(), outlineLevel);
			}
		}
        else {
            final OpAttrs bulletAttrs = paragraphAttrs.getMap(OCKey.BULLET.value(), true);
            bulletAttrs.put(OCKey.TYPE.value(), "none");
            if(this.getParentComponent() instanceof FrameComponent) {
                final String presentationStyleName = ((FrameComponent)getParentComponent()).getDrawFrame().getPresentationStyleName();
                if(presentationStyleName!=null&&!presentationStyleName.isEmpty()) {
                    final int level = paragraph.getListLevel();
                    if(level>=0) {
                        paragraphAttrs.put(OCKey.LEVEL.value(), level);
                    }
                }
            }
        }
		if(paragraphAttrs.isEmpty()) {
			attrs.remove(OCKey.PARAGRAPH.value());
		}
	}

	@Override
	public void insertText(int textPosition, String text, JSONObject attrs) throws Exception {

    	if(text.length()>0) {
            Text t = null;
            IComponent<OdfOperationDoc> childComponent = getNextChildComponent(null, null);
            IComponent<OdfOperationDoc> cRet = null;

            if(childComponent!=null) {
            	if(textPosition>0) {
            		childComponent = childComponent.getComponent(textPosition-1);
            	}
                // check if the character could be inserted into an existing text:
                if(childComponent instanceof TextComponent) {
                    t = (Text)childComponent.getObject();
                    final StringBuffer s = new StringBuffer(t.getText());
                    s.insert(textPosition-((TextComponent)childComponent).getComponentNumber(), text);
                    t.setText(s.toString());
                    cRet = childComponent;
                }
                else {
                	t = new Text(text);
                	final TextSpanContext spanContext = (TextSpanContext)childComponent.getParentContext();
                    ((TextSpan)spanContext.getObject()).getContent().addNode(childComponent.getNode(), new DLNode<Object>(t), textPosition==0);
                    cRet = childComponent;

                    if(textPosition>0) {
                        cRet = childComponent.getNextComponent();
                    }
                    else {
                        cRet = getNextChildComponent(null, null);
                    }
                }
            }
            else {

            	// the paragraph is empty, we have to create R and its text
                final TextSpan newRun = new TextSpan();
                paragraph.getContent().add(newRun);
/*
                if(paragraph.getPPr()!=null) {
                    final RPr rPr = TextUtils.cloneParaRPrToRPr(paragraph.getPPr().getRPr());
                    if(rPr!=null) {
                        rPr.setParent(newRun);
                        newRun.setRPr(rPr);
                    }
                }
*/
                t = new Text(text);
                newRun.getContent().add(t);
                cRet = getNextChildComponent(null, null);
            }
            if(attrs!=null) {
            	cRet.splitStart(textPosition, SplitMode.DELETE);
            	cRet.splitEnd(textPosition+text.length()-1, SplitMode.DELETE);
            	cRet.applyAttrsFromJSON(attrs);
            }
        }
	}

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(int textPosition, JSONObject attrs, ComponentType childType) throws Exception {

        Object newChild = null;
        switch(childType) {
            case TAB : {
                newChild = new TextTab(null);
                break;
            }
            case HARDBREAK : {
            	newChild = new TextLineBreak();
            	break;
            }
            case FIELD : {
                newChild = new TextField();
                break;
            }
            default : {
                throw new UnsupportedOperationException();
            }
        }
        IComponent<OdfOperationDoc> childComponent = getNextChildComponent(null, null);
        if(childComponent!=null) {
            if(textPosition>0) {
                childComponent = childComponent.getComponent(textPosition-1);
                childComponent.splitEnd(textPosition-1, SplitMode.DELETE);
            }
    		// the new child can be added into an existing textRun
            final TextSpan textSpan = ((TextSpan_Base)childComponent).getTextSpan();
            textSpan.getContent().addNode(childComponent.getNode(), new DLNode<Object>(newChild), textPosition==0);
        }
        else {
            final TextSpan newTextSpan = new TextSpan();
            paragraph.getContent().add(newTextSpan);
            newTextSpan.getContent().add(newChild);
        }
        if(textPosition>0) {
            childComponent = childComponent.getNextComponent();
        }
        else {
            childComponent = getNextChildComponent(null, null);
        }
        if(attrs!=null) {
            childComponent.splitStart(textPosition, SplitMode.DELETE);
            childComponent.splitEnd(textPosition, SplitMode.DELETE);
            childComponent.applyAttrsFromJSON(attrs);
        }
        return childComponent;
    }

	@Override
    public void splitParagraph(int textPosition) {
        final Paragraph destParagraph = new Paragraph(
        	paragraph.getTextListItem()!=null
        	? new TextListItem(paragraph.getTextListItem().getParentTextList())
        	: null);

        // taking care of paragraphs sharing the same textListItem
        if(destParagraph.getTextListItem()!=null) {
        	DLNode<Object> nextParaNode = getNode().getNext();
        	while(nextParaNode!=null&&nextParaNode.getData() instanceof Paragraph) {
	        	final Paragraph nextPara = (Paragraph)getNode().getNext().getData();
	        	if(nextPara.getTextListItem()!=paragraph.getTextListItem()) {
	        		break;
	        	}
	        	nextPara.setTextListItem(destParagraph.getTextListItem());
    	        nextParaNode = nextParaNode.getNext();
        	}
        }

        final DLNode<Object> destParagraphNode = new DLNode<Object>(destParagraph);
        ((INodeAccessor)getParentContext().getObject()).getContent().addNode(getNode(), destParagraphNode, false);
        destParagraph.setStyleName(paragraph.getStyleName());

        IComponent<OdfOperationDoc> component = getNextChildComponent(null, null);
    	while(component!=null&&component.getNextComponentNumber()<=textPosition) {
    		component = component.getNextComponent();
    	}
        if(component!=null) {
        	component.splitStart(textPosition, SplitMode.DELETE);

        	// moving text spans into the new paragraph
        	paragraph.getContent().moveNodes(component.getParentContext().getNode(), -1, destParagraph.getContent(), null, true);
        }
    }

	@Override
	public void mergeParagraph() {
    	final IComponent<OdfOperationDoc> nextParagraphComponent = getNextComponent();
    	if(nextParagraphComponent instanceof ParagraphComponent) {
        	final DLList<Object> sourceContent = ((Paragraph)nextParagraphComponent.getObject()).getContent();
        	sourceContent.moveNodes(paragraph.getContent());
            ((INodeAccessor)getParentContext().getObject()).getContent().removeNode(nextParagraphComponent.getNode());
    	}
	}
}
