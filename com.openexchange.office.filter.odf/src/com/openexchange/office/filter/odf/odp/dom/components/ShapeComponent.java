/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom.components;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.draw.IDrawingType;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;

public class ShapeComponent extends OdfComponent implements IDrawingType {

	public ShapeComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> shapeNode, int componentNumber) {
		super(parentContext, shapeNode, componentNumber);
	}

    @Override
    public String simpleName() {
        return "Shape";
    }

    @Override
    public DrawingType getType() {
		return ((Shape)getObject()).getType();
	}

	@Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {
        final int nextComponentNumber = previousChildComponent != null ? previousChildComponent.getNextComponentNumber() : 0;
        DLNode<Object> nextNode = previousChildContext != null ? previousChildContext.getNode().getNext() : ((Shape)getNode().getData()).getContent().getFirstNode();
		while(nextNode!=null) {
			final Object child = nextNode.getData();
			if(child instanceof Paragraph) {
				return new ParagraphComponent(this, nextNode, nextComponentNumber);
			}
			nextNode = nextNode.getNext();
		}
		return null;
	}

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> contextNode, int number, IComponent<OdfOperationDoc> child, ComponentType type, JSONObject attrs) {
		final Shape shape = (Shape)getObject();
    	DLList<Object> DLList = shape.getContent();
        DLNode<Object> referenceNode = child != null && child.getComponentNumber()== number ? child.getNode() : null;

        switch(type) {
            case PARAGRAPH : {
                // if this is a normal shape, then paragraphs can be inserted
            	final DLNode<Object> newParagraphNode = new DLNode<Object>(new Paragraph(null));
                DLList.addNode(referenceNode, newParagraphNode, true);
                return new ParagraphComponent(parentContext, newParagraphNode, number);
            }
            default : {
                throw new UnsupportedOperationException();
            }
        }
    }

	@Override
	public void applyAttrsFromJSON(JSONObject attrs) {
		try {
            ((Shape)getObject()).applyAttrsFromJSON(operationDocument, attrs, isContentAutoStyle());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {

	    ((Shape)getObject()).createAttrs(operationDocument, attrs, isContentAutoStyle());
	}
}
