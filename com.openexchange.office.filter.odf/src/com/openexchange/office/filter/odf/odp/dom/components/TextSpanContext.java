/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom.components;

import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.components.TextComponent;
import com.openexchange.office.filter.odf.components.TextFieldComponent;
import com.openexchange.office.filter.odf.components.TextLineBreakComponent;
import com.openexchange.office.filter.odf.components.TextTabComponent;
import com.openexchange.office.filter.odf.odt.dom.Text;
import com.openexchange.office.filter.odf.odt.dom.TextField;
import com.openexchange.office.filter.odf.odt.dom.TextLineBreak;
import com.openexchange.office.filter.odf.odt.dom.TextSpan;
import com.openexchange.office.filter.odf.odt.dom.TextTab;

public class TextSpanContext extends ComponentContext<OdfOperationDoc> {

	public TextSpanContext(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> textSpanNode, boolean contentStyle) {
		super(parentContext, textSpanNode);
	}

	@Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {
        final int nextComponentNumber = previousChildComponent != null ? previousChildComponent.getNextComponentNumber() : 0;
        DLNode<Object> nextNode = previousChildContext != null ? previousChildContext.getNode().getNext() : ((TextSpan)getNode().getData()).getContent().getFirstNode();
		while(nextNode!=null) {
			final Object child = nextNode.getData();
			if(child instanceof Text) {
				if(!((Text)child).getText().isEmpty()) {
					return new TextComponent(this, nextNode, nextComponentNumber);
				}
			}
			else if(child instanceof TextTab) {
				return new TextTabComponent(this, nextNode, nextComponentNumber);
			}
			else if(child instanceof TextField) {
			    return new TextFieldComponent(this, nextNode, nextComponentNumber);
			}
			else if(child instanceof TextLineBreak) {
				return new TextLineBreakComponent(this, nextNode, nextComponentNumber);
			}
			nextNode = nextNode.getNext();
		}
		return null;
	}
}
