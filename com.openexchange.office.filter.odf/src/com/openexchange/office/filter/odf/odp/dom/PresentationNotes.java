/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class PresentationNotes extends Page {

    public PresentationNotes(AttributesImpl attributes) {
        super(attributes);
    }

    @Override
    public String getName() {
        return attributes.getValue("style:name");
    }

    @Override
    public void setName(String name) {
        attributes.setValue(Namespaces.STYLE, "name", "style:name", name);
    }

    @Override
    public void writeObject(SerializationHandler output)
        throws SAXException {

        SaxContextHandler.startElement(output, Namespaces.PRESENTATION, "notes", "presentation:notes");
        attributes.write(output);
        for(Object child:getContent()) {
            if(child instanceof IElementWriter) {
                ((IElementWriter)child).writeObject(output);
            }
        }
        writeComments(output);
        SaxContextHandler.endElement(output, Namespaces.PRESENTATION, "notes", "presentation:notes");
    }

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, boolean contentAutoStyle, OpAttrs attrs) {
        super.createAttrs(operationDocument, contentAutoStyle, attrs);
    }

    @Override
    public void applyAttrsFromJSON(OdfOperationDoc operationDocument, boolean contentAutoStyle, JSONObject attrs) throws JSONException, SAXException {
        super.applyAttrsFromJSON(operationDocument, contentAutoStyle, attrs);
    }
}
