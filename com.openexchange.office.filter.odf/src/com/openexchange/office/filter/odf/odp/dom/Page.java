/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.odt.dom.Annotation;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

public abstract class Page implements IElementWriter, INodeAccessor {

    protected AttributesImpl attributes;

    private DLList<Object> content;
    private DLList<Annotation> annotations;
    PresentationNotes presentationNotes;

    public Page(AttributesImpl attributes) {
	    this.attributes = attributes;
	}

	@Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
		return content;
	}

	public DLList<Annotation> getAnnotations(boolean forceCreate) {
	    if(annotations==null && forceCreate) {
	        annotations = new DLList<Annotation>();
	    }
	    return annotations;
	}

	abstract public String getName();

	abstract public void setName(String name);

	final public String getStyle() {
	    return attributes.getValue("draw:style-name");
	}

	final public void setStyle(String styleName) {
	    attributes.setValue(Namespaces.DRAW, "style-name", "draw:style-name", styleName);
	}

    @Override
	public abstract void writeObject(SerializationHandler output) throws SAXException;

    protected void writeComments(SerializationHandler output)
        throws SAXException {

        if(annotations!=null) {
            DLNode<Annotation> annotationNode = annotations.getFirstNode();
            while(annotationNode!=null) {
                annotationNode.getData().writeObject(output);
                annotationNode = annotationNode.getNext();
            }
        }
    }

    @SuppressWarnings("unused")
    public void applyAttrsFromJSON(OdfOperationDoc operationDocument, boolean contentAutoStyle, JSONObject attrs) throws JSONException, SAXException {
        setStyle(operationDocument.getDocument().getStyleManager().createStyle(StyleFamily.DRAWING_PAGE, getStyle(), contentAutoStyle, attrs));
    }

    public void createAttrs(OdfOperationDoc operationDocument, boolean contentAutoStyle, OpAttrs attrs) {
        final String style = getStyle();
        if(style!=null&&!style.isEmpty()) {
            final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
            final StyleBase styleBase = styleManager.getStyle(style, StyleFamily.DRAWING_PAGE, contentAutoStyle);
            if(styleBase!=null) {
                styleBase.createAttrs(styleManager, attrs);
            }
        }
    }


}
