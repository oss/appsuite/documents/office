/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.Decl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class Presentation extends ElementNSImpl implements IElementWriter, INodeAccessor {

	private static final long serialVersionUID = 1L;

	private DLList<Object> pages;

	HashMap<String, Decl> footerDecls;
    HashMap<String, Decl> headerDecls;
    HashMap<String, Decl> dateTimeDecls;
	
	public Presentation(OdfFileDom ownerDocument, Attributes attributes) {
		super(ownerDocument, Namespaces.OFFICE, "office:text");
	}

    @Override
    public DLList<Object> getContent() {
        if (pages == null) {
            pages = new DLList<Object>();
        }
        return pages;
    }

    public HashMap<String, Decl> getFooterDecls(boolean forceCreate) {
        if(footerDecls==null&&forceCreate) {
            footerDecls = new HashMap<String, Decl>();
        }
        return footerDecls;
    }

    public HashMap<String, Decl> getHeaderDecls(boolean forceCreate) {
        if(headerDecls==null&&forceCreate) {
            headerDecls = new HashMap<String, Decl>();
        }
        return headerDecls;
    }

    public HashMap<String, Decl> getDateTimeDecls(boolean forceCreate) {
        if(dateTimeDecls==null&&forceCreate) {
            dateTimeDecls = new HashMap<String, Decl>();
        }
        return dateTimeDecls;
    }

    /*
     * tries to get an existing decl key whose value equals the value parameter.
     * null if no decl can be found
     */
    public static Decl getDecl(HashMap<String, Decl> map, String value) {
        final Iterator<Entry<String, Decl>> iter = map.entrySet().iterator();
        while(iter.hasNext()) {
            final Entry<String, Decl> entry = iter.next();
            if(value.equals(entry.getValue().getValue())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public static String getUniqueDeclKey(HashMap<String, Decl> map, String prefix) {
        Integer k = 1;
        while(true) {
            final String key = prefix + k.toString();
            if(map.get(key)==null) {
                return key;
            }
            k++;
        }
    }

    @Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.OFFICE, "presentation", "office:presentation");
		if(footerDecls!=null) {
		    final Iterator<Decl> footerDeclIter = footerDecls.values().iterator();
		    while(footerDeclIter.hasNext()) {
		        footerDeclIter.next().writeObject(output);
		    }
		}
        if(headerDecls!=null) {
            final Iterator<Decl> headerDeclIter = headerDecls.values().iterator();
            while(headerDeclIter.hasNext()) {
                headerDeclIter.next().writeObject(output);
            }
        }
        if(dateTimeDecls!=null) {
            final Iterator<Decl> dateTimeDeclIter = dateTimeDecls.values().iterator();
            while(dateTimeDeclIter.hasNext()) {
                dateTimeDeclIter.next().writeObject(output);
            }
        }
		final Iterator<Object> pageIter = getContent().iterator();
		while(pageIter.hasNext()) {
		    ((IElementWriter)pageIter.next()).writeObject(output);
		}
		SaxContextHandler.endElement(output, Namespaces.OFFICE, "presentation", "office:presentation");
	}
}
