/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import org.odftoolkit.odfdom.doc.OdfPresentationDocument;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.odf.DOMBuilder;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.AutomaticStylesHandler;
import com.openexchange.office.filter.odf.styles.FontFaceDeclsHandler;

/**
 * @author sven.jacobi@open-xchange.com
 */
public class PresentationContentHandler extends DOMBuilder {

    // the empty XML file to which nodes will be added
    private final PresentationContent content;
    private OdfPresentationDocument presentationDocument = null;

    public PresentationContentHandler(Node rootNode, XMLReader xmlReader) {
    	super(rootNode, xmlReader, (PresentationContent)rootNode);

        // Initialize starting DOM node
        content = (PresentationContent)rootNode;
        presentationDocument = content.getDocument();
        presentationDocument.setContentDom(content);
    }

    public PresentationContent getContentDom() {
    	return content;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("office:automatic-styles")) {
    		return new AutomaticStylesHandler(this, content.getDocument().getStyleManager(), true);
    	}
    	else if(qName.equals("office:presentation")) {
    	    final Presentation presentation = new Presentation(getFileDom(), attributes);
    	    appendChild(presentation);
    	    content.setPresentation(presentation);
    	    return new PresentationHandler(this, presentation);
    	}
    	else if(qName.equals("office:font-face-decls")) {
    		return new FontFaceDeclsHandler(this, content.getDocument().getStyleManager(), true);
    	}
    	// if we come to this postion, the element is inserted normally into the dom...
    	return super.startElement(attributes, uri, localName, qName);
    }

    @Override
    public void endElement(String localName, String qName) {
    	if(qName.equals("office:automatic-styles")||qName.equals("office:font-face-decls")) {
    		// do nothing since we do not want this element to be inserted into the dom,
    		// this is done elsewhere
    		return;
    	}
    	super.endElement(localName, qName);
    }
}
