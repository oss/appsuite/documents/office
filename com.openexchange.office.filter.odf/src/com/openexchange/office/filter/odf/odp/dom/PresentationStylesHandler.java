/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import org.odftoolkit.odfdom.dom.OdfSchemaDocument;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.odf.DOMBuilder;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.AutomaticStylesHandler;
import com.openexchange.office.filter.odf.styles.FontFaceDeclsHandler;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StylesHandler;

public class PresentationStylesHandler extends DOMBuilder {

    // the empty XML file to which nodes will be added
    private final PresentationStyles presentationStyles;
    private final StyleManager styleManager;
    private OdfSchemaDocument mSchemaDoc = null;

    public PresentationStylesHandler(Node rootNode, XMLReader xmlReader) {
    	super(rootNode, xmlReader, (PresentationStyles)rootNode);

    	// Initialize starting DOM node
    	presentationStyles = (PresentationStyles)rootNode;
    	styleManager = presentationStyles.getDocument().getStyleManager();
        mSchemaDoc = presentationStyles.getDocument();
        mSchemaDoc.setStylesDom(presentationStyles);
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("office:font-face-decls")) {
    		return new FontFaceDeclsHandler(this, styleManager, false);
    	}
    	else if(qName.equals("office:styles")) {
    		return new StylesHandler(this, styleManager);
    	}
    	else if(qName.equals("office:automatic-styles")) {
    		return new AutomaticStylesHandler(this, styleManager, false);
    	}
    	else if(qName.equals("office:master-styles")) {
    		return new PresentationMasterStylesHandler(this, styleManager);
    	}
    	return super.startElement(attributes, uri, localName, qName);
    }
}
