/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.doc.OdfPresentationDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Decl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.NumberStyleBase;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class DrawingPage extends Page {

    public DrawingPage(AttributesImpl attributes) {
        super(attributes);
    }

    @Override
    public String getName() {
        return attributes.getValue("draw:name");
    }

    @Override
    public void setName(String name) {
        attributes.setValue(Namespaces.DRAW, "name", "draw:name", name);
    }

    public String getMasterPageName() {
        return attributes.getValue("draw:master-page-name");
    }

    public void setMasterPageName(String masterPageName) {
        attributes.setValue(Namespaces.DRAW, "master-page-name", "draw:master-page-name", masterPageName);
    }

    public String getPresentationPageLayoutName() {
        return attributes.getValue("presentation:presentation-page-layout-name");
    }

    public void setPresentationPageLayoutName(String presentationPageLayoutName) {
        attributes.setValue(Namespaces.PRESENTATION, "presentation-page-layout-name", "presentation:presentation-page-layout-name", presentationPageLayoutName);
    }

    @Override
    public void writeObject(SerializationHandler output)
        throws SAXException {

        SaxContextHandler.startElement(output, Namespaces.DRAW, "page", "draw:page");
        attributes.write(output);
        for(Object child:getContent()) {
            if(child instanceof IElementWriter) {
                ((IElementWriter)child).writeObject(output);
            }
        }
        if(presentationNotes != null) {
            presentationNotes.writeObject(output);
        }
        writeComments(output);
        SaxContextHandler.endElement(output, Namespaces.DRAW, "page", "draw:page");
    }

    @Override
    public void applyAttrsFromJSON(OdfOperationDoc operationDocument, boolean contentAutoStyle, JSONObject attrs) throws JSONException, SAXException {
        super.applyAttrsFromJSON(operationDocument, contentAutoStyle, attrs);

        final JSONObject slideAttrs = attrs.optJSONObject(OCKey.SLIDE.value());
        if(slideAttrs!=null) {
            final Presentation presentation = ((PresentationContent)((OdfPresentationDocument)operationDocument.getDocument()).getContentDom()).getPresentation();
            final Object footerText = slideAttrs.opt("footerText");
            if(footerText!=null) {
                if(footerText instanceof String) {
                    final HashMap<String, Decl> footerDecls = presentation.getFooterDecls(true);
                    Decl decl = Presentation.getDecl(footerDecls, (String)footerText);
                    if(decl==null) {
                        decl = new Decl("presentation:footer-decl", "footer-decl", Namespaces.PRESENTATION, new AttributesImpl());
                        decl.setValue((String)footerText);
                        final String k = Presentation.getUniqueDeclKey(footerDecls, "ftr");
                        decl.setName(k);
                        footerDecls.put(k, decl);
                    }
                    attributes.setValue(Namespaces.PRESENTATION, "use-footer-name", "presentation:use-footer-name", decl.getName());
                }
                else if(footerText==JSONObject.NULL) {
                    attributes.remove("presentation:use-footer-name");
                }
            }
            final Object dateText = slideAttrs.opt("dateText");
            final Object dateField = slideAttrs.opt("dateField");
            if(dateText instanceof String||dateField instanceof String) {
                final HashMap<String, Decl> dateTimeDecls = presentation.getDateTimeDecls(true);
                Decl dateTextDecl = null;
                if(dateText instanceof String) {
                    dateTextDecl = Presentation.getDecl(dateTimeDecls, (String)dateText);
                    if(dateTextDecl==null) {
                        dateTextDecl = new Decl("presentation:date-time-decl", "date-time-decl", Namespaces.PRESENTATION, new AttributesImpl());
                        dateTextDecl.setValue((String)dateText);
                        final String k = Presentation.getUniqueDeclKey(dateTimeDecls, "dt");
                        dateTextDecl.setName(k);
                        dateTimeDecls.put(k, dateTextDecl);
                    }
                    dateTextDecl.getAttributes().remove("style:data-style-name");
                    dateTextDecl.getAttributes().setValue(Namespaces.PRESENTATION, "source", "presentation:source", "fixed");
                }
                else {
                    final String dateId = operationDocument.getDocument().getStyleManager().applyDataStyle((String)dateField, null, -1, false, true, true);
                    final Iterator<Entry<String, Decl>> dateTimeDeclIter = dateTimeDecls.entrySet().iterator();
                    while(dateTimeDeclIter.hasNext()) {
                        final Entry<String, Decl> e = dateTimeDeclIter.next();
                        final Decl d = e.getValue();
                        if("current-date".equals(d.getAttributes().getValue("presentation:source"))) {
                            if(dateId.equals(d.getAttributes().getValue("style:data-style-name"))) {
                                dateTextDecl = d;
                            }
                        }
                    }
                    if(dateTextDecl==null) {
                        dateTextDecl = new Decl("presentation:date-time-decl", "date-time-decl", Namespaces.PRESENTATION, new AttributesImpl());
                        dateTextDecl.getAttributes().setValue(Namespaces.STYLE, "data-style-name", "style:data-style-name", dateId);
                        final String k = Presentation.getUniqueDeclKey(dateTimeDecls, "dt");
                        dateTextDecl.setName(k);
                        dateTimeDecls.put(k, dateTextDecl);
                    }
                    dateTextDecl.setValue(null);
                    dateTextDecl.getAttributes().setValue(Namespaces.PRESENTATION, "source", "presentation:source", "current-date");
                }
                attributes.setValue(Namespaces.PRESENTATION, "use-date-time-name", "presentation:use-date-time-name", dateTextDecl.getName());
            }
            else {
                final String useDateTimeDecl = attributes.getValue("presentation:use-date-time-name");
                if(useDateTimeDecl!=null) {
                    final Decl dateTimeDecl = presentation.getDateTimeDecls(true).get(useDateTimeDecl);
                    if(dateTimeDecl==null) {
                        attributes.remove("presentation:use-date-time-name");
                    }
                    else {
                        final String presentationSource = attributes.getValue("presentation:source");
                        if(presentationSource==null||(presentationSource.equals("fixed")&&dateText==JSONObject.NULL)||(presentationSource.equals("current-date")&&dateField==JSONObject.NULL)) {
                            attributes.remove("presentation:use-date-time-name");
                        }
                    }
                }
            }
        }
    }

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, boolean contentAutoStyle, OpAttrs attrs) {
        super.createAttrs(operationDocument, contentAutoStyle, attrs);

        try {
            final Presentation presentation = ((PresentationContent)((OdfPresentationDocument)operationDocument.getDocument()).getContentDom()).getPresentation();
            final OpAttrs slideAttrs = attrs.getMap(OCKey.SLIDE.value(), true);
            final String useFooterName = attributes.getValue("presentation:use-footer-name");
            if(useFooterName!=null) {
                final HashMap<String, Decl> footerDecls = presentation.getFooterDecls(false);
                if(footerDecls!=null) {
                    final Decl footerDecl = footerDecls.get(useFooterName);
                    if(footerDecl!=null) {
                        final String footerValue = footerDecl.getValue();
                        if(footerValue!=null&&!footerValue.isEmpty()) {
                            slideAttrs.put("footerText", footerValue);
                        }
                    }
                }
            }
            final String useDateTimeName = attributes.getValue("presentation:use-date-time-name");
            if(useDateTimeName!=null) {
                final HashMap<String, Decl> dateTimeDecls = presentation.getDateTimeDecls(false);
                if(dateTimeDecls!=null) {
                    final Decl dateTimeDecl = dateTimeDecls.get(useDateTimeName);
                    if(dateTimeDecl!=null) {
                        final String source = dateTimeDecl.getAttributes().getValue("presentation:source");
                        if(source!=null) {
                            if(source.equals("fixed")) {
                                final String dateTimeValue = dateTimeDecl.getValue();
                                if(dateTimeValue!=null&&!dateTimeValue.isEmpty()) {
                                    slideAttrs.put("dateText", dateTimeValue);
                                }
                            }
                            else if(source.equals("current-date")) {
                                final String dataStyleName = dateTimeDecl.getAttributes().getValue("style:data-style-name");
                                if(dataStyleName!=null) {
                                    final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
                                    final StyleBase dataStyle = styleManager.getStyle(dataStyleName, StyleFamily.DATA_STYLE, contentAutoStyle);
                                    if(dataStyle instanceof NumberStyleBase) {
                                        final String formatCode = ((NumberStyleBase)dataStyle).getFormat(styleManager, null, contentAutoStyle);
                                        if(formatCode!=null&&!formatCode.isEmpty()) {
                                            slideAttrs.put("dateField", formatCode);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(slideAttrs.isEmpty()) {
                attrs.remove(OCKey.SLIDE.value());
            }
        }
        catch(SAXException e) {
            // ohoh
        }
    }
}
