/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.IContentDom;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawFrameHandler;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.draw.ShapeHelper;
import com.openexchange.office.filter.odf.odt.dom.Annotation;
import com.openexchange.office.filter.odf.odt.dom.AnnotationHandler;

public class PageHandler extends SaxContextHandler {

    final Page page;
    final boolean contentStyle;
    Annotation annotation;

    public PageHandler(SaxContextHandler parentContext, Page page) {
    	super(parentContext);
    	this.page = page;

    	contentStyle = getFileDom() instanceof IContentDom;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        if(qName.equals("draw:frame")) {
            final DrawFrame drawFrame = new DrawFrame(getFileDom().getDocument(), new AttributesImpl(attributes), null, true, contentStyle);
            if(page instanceof MasterPage) {
                final String presentationClass = drawFrame.getPresentationClass();
                if("title".equals(presentationClass)) {
                    ((MasterPage)page).setTitleObject(drawFrame);
                }
                else if("outline".equals(presentationClass)) {
                    ((MasterPage)page).setOutlineObject(drawFrame);
                }
            }
            page.getContent().add(drawFrame);
            return new DrawFrameHandler(this, drawFrame);
        }
        else if(qName.equals("presentation:notes")) {
            if(!(page instanceof PresentationNotes)) {  // prevent PresentationNotes in PresentationNotes which is not allowed
                final PresentationNotes notesPage = new PresentationNotes(new AttributesImpl(attributes));
                page.presentationNotes = notesPage;
                return new PageHandler(this, page.presentationNotes);
            }
        }
        final Shape shape = ShapeHelper.getShape(attributes, uri, localName, qName, null, true, contentStyle);
        if(shape!=null) {
            page.getContent().add(shape);
            return shape.getContextHandler(this);
        }
        if(localName.equals("annotation")) {
            annotation = new Annotation("", attributes, true);
            page.getAnnotations(true).add(annotation);
            return new AnnotationHandler(this, annotation);
        }
        final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
        page.getContent().add(element);
        return new UnknownContentHandler(this, element);
    }

    @Override
    public void endElement(String localName, String qName) {
        super.endElement(localName, qName);

        if(annotation!=null) {
            getFileDom().getDocument().getAuthors(true).addAuthor(annotation);
            annotation = null;
        }
    }
}

