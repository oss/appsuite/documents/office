/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import java.util.HashMap;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Decl;
import com.openexchange.office.filter.odf.DeclHandler;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;

public class PresentationHandler extends SaxContextHandler {

	final Presentation presentation;

    public PresentationHandler(SaxContextHandler parentContext, Presentation presentation) {
    	super(parentContext);

    	this.presentation = presentation;
    }

    @Override
    public SaxContextHandler startElement(Attributes _attributes, String uri, String localName, String qName) {
        final AttributesImpl attributes = new AttributesImpl(_attributes);
    	if(qName.equals("draw:page")) {
    	    final Page page = new DrawingPage(attributes);
    	    presentation.getContent().add(page);
    	    return new PageHandler(this, page);
    	}

    	final String declName = attributes.getValue("presentation:name");
    	if(declName!=null&&!declName.isEmpty()) {
    	    HashMap<String, Decl> decls = null;
    	    if(qName.equals("presentation:date-time-decl")) {
    	        decls = presentation.getDateTimeDecls(true);
        	}
        	else if (qName.equals("presentation:footer-decl")) {
        	    decls = presentation.getFooterDecls(true);
        	}
        	else if (qName.equals("presentation:header-decl")) {
        	    decls = presentation.getHeaderDecls(true);
        	}
    	    if(decls!=null) {
    	        final Decl decl = new Decl(qName, localName, uri, attributes);
    	        decls.put(declName, decl);
    	        return new DeclHandler(this, decl);
    	    }
    	}
        final ElementNSWriter element = new ElementNSWriter(getFileDom(), _attributes, uri, qName);
        presentation.getContent().add(element);
        return new UnknownContentHandler(this, element);
    }

	@Override
	public void endElement(String localName, String qName) {
		super.endElement(localName, qName);
	}

	@Override
	public void endContext(String qName, String characters) {
		super.endContext(qName, characters);
	}
}
