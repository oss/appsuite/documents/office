/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.odp.dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.doc.OdfPresentationDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Authors;
import com.openexchange.office.filter.odf.IAttributesAccessor;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.Settings;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.components.TextComponent;
import com.openexchange.office.filter.odf.components.TextFieldComponent;
import com.openexchange.office.filter.odf.components.TextLineBreakComponent;
import com.openexchange.office.filter.odf.components.TextTabComponent;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.draw.IDrawing;
import com.openexchange.office.filter.odf.draw.IDrawingType;
import com.openexchange.office.filter.odf.odp.dom.components.CellComponent;
import com.openexchange.office.filter.odf.odp.dom.components.FrameComponent;
import com.openexchange.office.filter.odf.odp.dom.components.ParagraphComponent;
import com.openexchange.office.filter.odf.odp.dom.components.RowComponent;
import com.openexchange.office.filter.odf.odp.dom.components.ShapeComponent;
import com.openexchange.office.filter.odf.odp.dom.components.ShapeGroupComponent;
import com.openexchange.office.filter.odf.odp.dom.components.SlideComponent;
import com.openexchange.office.filter.odf.odt.dom.Annotation;
import com.openexchange.office.filter.odf.odt.dom.Text;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;
import com.openexchange.office.filter.odf.odt.dom.TextField;
import com.openexchange.office.filter.odf.properties.TextProperties;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleGraphic;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StylePresentation;
import com.openexchange.office.filter.odf.styles.TableTemplate;
import com.openexchange.office.filter.odf.table.Table;

public class JsonOperationProducer {

    final JSONArray operationQueue;

    private final OdfOperationDoc opsDoc;
    private final OdfPresentationDocument doc;
    private final PresentationStyles styles;
    private final PresentationContent content;
    private final Presentation presentation;
    private final StyleManager styleManager;
    private final Settings settings;

    public JsonOperationProducer(OdfOperationDoc opsDoc)
    	throws SAXException {

    	operationQueue = new JSONArray();
    	this.opsDoc = opsDoc;
        doc = (OdfPresentationDocument)opsDoc.getDocument();
        styles = (PresentationStyles)doc.getStylesDom();
        content = (PresentationContent)doc.getContentDom();
        presentation = content.getPresentation();
        settings = doc.getSettingsDom();
        styleManager = doc.getStyleManager();
        styleManager.setTabsRelativeToIndent(settings.hasTabsRelativeToIndent());
    }

    public JSONObject getDocumentOperations() throws Exception {

    	createDocumentAttributes();
    	createTableTemplateStyleOperations();
    	createMasterSlides();
    	createSlides();

    	final JSONObject operations = new JSONObject(1);
        operations.put("operations", operationQueue);
        return operations;
    }

    public void createDocumentAttributes() throws JSONException {
        final OpAttrs attrs = new OpAttrs();
        final Iterator<Object> masterStyleContentIter = styleManager.getMasterStyles().getContent().iterator();
        MasterPage masterPage = null;
        while(masterStyleContentIter.hasNext()) {
            final Object o = masterStyleContentIter.next();
            if(o instanceof MasterPage) {
                masterPage = (MasterPage)o;
                break;
            }
        }
        if(masterPage!=null) {
            final StyleBase pageLayout = styleManager.getStyle(masterPage.getPageLayoutName(), StyleFamily.PAGE_LAYOUT, false);
            if(pageLayout!=null) {
                pageLayout.createAttrs(styleManager, attrs);
            }
        }
        final StyleBase graphicStyle = styleManager.getStyle("standard", StyleFamily.GRAPHIC, false);
        if(graphicStyle!=null) {
            OpAttrs listStyles = new OpAttrs();
            styleManager.createPresentationAttributes(graphicStyle, OCKey.DEFAULT_TEXT_LIST_STYLES.value(), 10, false, listStyles);
            listStyles = listStyles.getMap(OCKey.LIST_STYLES.value(), false);
            if(listStyles!=null) {
                final OpAttrs defaultTextListStyles = listStyles.getMap(OCKey.DEFAULT_TEXT_LIST_STYLES.value(), false);
                if(defaultTextListStyles!=null) {
                    attrs.put(OCKey.DEFAULT_TEXT_LIST_STYLES.value(), defaultTextListStyles);
                }
            }
        }
        final StyleBase defaultGraphicStyle = styleManager.getStyle("_default", StyleFamily.GRAPHIC, false);
        if(defaultGraphicStyle!=null) {
            final TextProperties textProperties = ((StyleGraphic)defaultGraphicStyle).getTextProperties();
            final String language = getLanguage(textProperties.getAttribute("fo:language"), textProperties.getAttribute("fo:country"));
            if(language!=null) {
                attrs.getMap(OCKey.CHARACTER.value(), true).put(OCKey.LANGUAGE.value(), language);
            }
            final String languageAsian = getLanguage(textProperties.getAttribute("style:language-asian"), textProperties.getAttribute("style:country-asian"));
            if(languageAsian!=null) {
                attrs.getMap(OCKey.CHARACTER.value(), true).put(OCKey.LANGUAGE_EA.value(), languageAsian);
            }
            final String languageComplex = getLanguage(textProperties.getAttribute("style:language-complex"), textProperties.getAttribute("style:country-complex"));
            if(languageComplex!=null) {
                attrs.getMap(OCKey.CHARACTER.value(), true).put(OCKey.LANGUAGE_BIDI.value(), languageComplex);
            }
        }
        final Authors authors = doc.getAuthors(false);
        if(authors!=null) {
            final JSONArray json = authors.getJSON();
            if(json!=null) {
                attrs.put(OCKey.AUTHORS.value(), json);
            }
        }
        final JSONObject jsonInsertSlideOperation = new JSONObject(3);
        jsonInsertSlideOperation.put(OCKey.NAME.value(), OCValue.SET_DOCUMENT_ATTRIBUTES.value());
        jsonInsertSlideOperation.put(OCKey.ATTRS.value(), attrs);
        Tools.writeFilterVersion(jsonInsertSlideOperation.getJSONObject(OCKey.ATTRS.value()));
        operationQueue.put(jsonInsertSlideOperation);
    }

    private String getLanguage(String language, String country) {
        String r = null;
        if(language!=null) {
            r = country!=null ? language + "-" + country : language;
        }
        return r;
    }

    public void createTableTemplateStyleOperations()
        throws JSONException {

        final HashMap<String, DLNode<Object>> familyStyles = styleManager.getFamilyStyles(StyleFamily.TABLE_TEMPLATE);
        if(familyStyles!=null) {
            final Iterator<Entry<String, DLNode<Object>>> tableTemplateIter = familyStyles.entrySet().iterator();
            while(tableTemplateIter.hasNext()) {
                final Entry<String, DLNode<Object>> entry = tableTemplateIter.next();
                if(entry.getValue().getData() instanceof TableTemplate) {
                    operationQueue.put(TableTemplate.createInsertStyleSheetOperation(styleManager, entry.getKey(), (TableTemplate)entry.getValue().getData()));
                }
            }
        }
    }

    public void createMasterSlides() throws Exception {
        final Iterator<Object> masterStyleContentIter = styleManager.getMasterStyles().getContent().iterator();
        while(masterStyleContentIter.hasNext()) {
            final Object o = masterStyleContentIter.next();
            if(o instanceof MasterPage) {
                final String masterPageId = ((MasterPage)o).getName();
                final SlideComponent slideComponent = new SlideComponent(opsDoc, new DLNode<Object>(o), 0, false);
                final JSONObject jsonInsertMasterSlideOperation = new JSONObject(3);
                jsonInsertMasterSlideOperation.put(OCKey.NAME.value(), OCValue.INSERT_MASTER_SLIDE.value());
                jsonInsertMasterSlideOperation.put(OCKey.ID.value(), masterPageId);
                final OpAttrs attrs = new OpAttrs();
                createMasterStyles(slideComponent, attrs);
                slideComponent.createJSONAttrs(attrs);
                if(!attrs.isEmpty()) {
                    jsonInsertMasterSlideOperation.put(OCKey.ATTRS.value(), attrs);
                }
                operationQueue.put(jsonInsertMasterSlideOperation);
                createSlideOperations(slideComponent, 0, masterPageId);
            }
        }
    }

    public void createSlides() throws Exception {
        OdfComponent slideComponent = (OdfComponent)content.getRootComponent(opsDoc, null).getNextChildComponent(null, null);
        while(slideComponent instanceof SlideComponent) {
            final JSONObject jsonInsertSlideOperation = new JSONObject(4);
            jsonInsertSlideOperation.put(OCKey.NAME.value(), OCValue.INSERT_SLIDE.value());
            jsonInsertSlideOperation.put(OCKey.START.value(), slideComponent.getComponentNumber());
            final String masterPageName = ((DrawingPage)slideComponent.getObject()).getMasterPageName();
            jsonInsertSlideOperation.put(OCKey.TARGET.value(), masterPageName);
            final OpAttrs attrs = new OpAttrs();
            slideComponent.createJSONAttrs(attrs);
            if(!attrs.isEmpty()) {
                jsonInsertSlideOperation.put(OCKey.ATTRS.value(), attrs);
            }
            operationQueue.put(jsonInsertSlideOperation);
            createSlideOperations((SlideComponent)slideComponent, slideComponent.getComponentNumber(), null);
            slideComponent = (OdfComponent)slideComponent.getNextComponent();
        }
    }

    public void createSlideOperations(SlideComponent slideComponent, int slide, String target) throws Exception {
        final ArrayList<Integer> position = new ArrayList<Integer>();
        position.add(slide);
        OdfComponent component = (OdfComponent)slideComponent.getNextChildComponent(null, null);
        while(component!=null) {
            final List<Integer> childPosition = new ArrayList<Integer>(position);
            childPosition.add(component.getComponentNumber());
            if(component instanceof ShapeComponent) {
                createShapeOperations((ShapeComponent)component, childPosition, target);
            }
            else if(component instanceof ShapeGroupComponent) {
                createShapeGroupOperations((ShapeGroupComponent)component, childPosition, target);
            }
            else if(component instanceof FrameComponent) {
                createFrameOperations((FrameComponent)component, childPosition, target);
            }
            component = (OdfComponent)component.getNextComponent();
        }
        createSlideCommentOperations(slideComponent.getPage(), slide, target);
    }

    private void createSlideCommentOperations(Page page, int slide, String target)
        throws JSONException {

        final DLList<Annotation> annotations = page.getAnnotations(false);
        if(annotations!=null) {
            DLNode<Annotation> node = annotations.getFirstNode();
            int i = 0;
            while(node!=null) {
                final Annotation annotation = node.getData();
                final JSONObject jsonInsertCommentOperation = new JSONObject();
                jsonInsertCommentOperation.put(OCKey.NAME.value(), OCValue.INSERT_COMMENT.value());
                final JSONArray start = new JSONArray(2);
                start.put(slide);
                start.put(i);
                jsonInsertCommentOperation.put(OCKey.START.value(), start);
                if(target!=null) {
                    jsonInsertCommentOperation.put(OCKey.TARGET.value(), target);
                }
                final JSONArray pos = new JSONArray(2);
                pos.put((int)annotation.getTransformer().getX());
                pos.put((int)annotation.getTransformer().getY());
                jsonInsertCommentOperation.put(OCKey.POS.value(), pos);
                jsonInsertCommentOperation.put(OCKey.AUTHOR_ID.value(), annotation.getAuthorId());
                jsonInsertCommentOperation.put(OCKey.TEXT.value(), TextContentHelper.getSimpleText(annotation));
                if(annotation.getDate() != null) {
                    jsonInsertCommentOperation.put(OCKey.DATE.value(), annotation.getDate());
                }
                operationQueue.put(jsonInsertCommentOperation);
                node = node.getNext();
                i++;
            }
        }
   }

    public void createFrameOperations(FrameComponent frameComponent, List<Integer> position, String target) throws Exception {
        final IDrawing iDrawing = frameComponent.getDrawFrame().getDrawing();
        if(iDrawing instanceof Table) {
            String templateName = ((Table)iDrawing).getTemplateName(styleManager);
            if(templateName!=null&&!templateName.isEmpty()) {
                final StyleBase templateStyle = styleManager.getStyle(templateName, StyleFamily.TABLE_TEMPLATE, false);
                if(templateStyle == null) {
                    final String defaultCellStyle1 = styleManager.getStyle(templateName + "1", StyleFamily.TABLE_CELL, false) != null ? templateName + "1" : null;
                    if(defaultCellStyle1 != null) {
                        final String defaultCellStyle2 = styleManager.getStyle(templateName + "2", StyleFamily.TABLE_CELL, false) != null ? templateName + "2" : defaultCellStyle1;
                        final String defaultCellStyle3 = styleManager.getStyle(templateName + "3", StyleFamily.TABLE_CELL, false) != null ? templateName + "3" : defaultCellStyle1;
                        final TableTemplate tableTemplate = new TableTemplate(templateName, new AttributesImpl());
                        tableTemplate.addTableTemplateEntry("first-row", defaultCellStyle3);
                        tableTemplate.addTableTemplateEntry("last-row", defaultCellStyle3);
                        tableTemplate.addTableTemplateEntry("first-column", defaultCellStyle3);
                        tableTemplate.addTableTemplateEntry("last-column", defaultCellStyle3);
                        tableTemplate.addTableTemplateEntry("body", defaultCellStyle1);
                        tableTemplate.addTableTemplateEntry("odd-rows", defaultCellStyle2);
                        tableTemplate.addTableTemplateEntry("odd-columns", defaultCellStyle2);
                        styleManager.addStyle(tableTemplate);
                        try {
                            operationQueue.put(TableTemplate.createInsertStyleSheetOperation(styleManager, templateName, tableTemplate));
                        } catch (JSONException e) {
                            // ohoh
                        }
                    }
                }
            }
        }
        final OpAttrs attrs = new OpAttrs();
        frameComponent.createJSONAttrs(attrs);
        addInsertDrawingOperation(position, ((IDrawingType)frameComponent).getType(), attrs, target);
        OdfComponent child = (OdfComponent)frameComponent.getNextChildComponent(null, null);
        while(child!=null) {
            final List<Integer> childPosition = new ArrayList<Integer>(position);
            childPosition.add(child.getComponentNumber());
            if(child instanceof ParagraphComponent) {
                createParagraphOperations((ParagraphComponent)child, childPosition, target);
            }
            else if(child instanceof ShapeComponent) {
                createShapeOperations((ShapeComponent)child, childPosition, target);
            }
            else if(child instanceof RowComponent) {
                createRowOperations((RowComponent)child, childPosition, target);
            }
            child = (OdfComponent)child.getNextComponent();
        }
    }

    public void createRowOperations(RowComponent rowComponent, List<Integer> rowPosition, String target) throws Exception {

        OpAttrs attrs = new OpAttrs();
        rowComponent.createJSONAttrs(attrs);
        addInsertRowsOperation(rowPosition, 1, target, attrs);
        OdfComponent component= (OdfComponent)rowComponent.getNextChildComponent(null, null);
        while(component!=null) {
            if(component instanceof CellComponent) {
                final List<Integer> cellPosition = new ArrayList<Integer>(rowPosition);
                cellPosition.add(component.getComponentNumber());
                createCellOperations((CellComponent)component, cellPosition, target);
            }
            component = (OdfComponent)component.getNextComponent();
        }
    }

    public void createCellOperations(CellComponent cellComponent, List<Integer> cellPosition, String target)
        throws JSONException, SAXException {

        final OpAttrs attrs = new OpAttrs();
        cellComponent.createJSONAttrs(attrs);
        addInsertCellsOperation(cellPosition, target, attrs);

        OdfComponent component = (OdfComponent)cellComponent.getNextChildComponent(null, null);
        while (component!=null) {
            if(component instanceof ParagraphComponent) {
                final List<Integer> paragraphPosition = new ArrayList<Integer>(cellPosition);
                paragraphPosition.add(component.getComponentNumber());
                createParagraphOperations((ParagraphComponent)component, paragraphPosition, target);
            }
            component = (OdfComponent)component.getNextComponent();
        }
    }

    public void createShapeOperations(ShapeComponent shapeComponent, List<Integer> position, String target)
        throws JSONException, SAXException {

        final OpAttrs attrs = new OpAttrs();
        shapeComponent.createJSONAttrs(attrs);
        addInsertDrawingOperation(position, ((IDrawingType)shapeComponent).getType(), attrs, target);
        OdfComponent child = (OdfComponent)shapeComponent.getNextChildComponent(null, null);
        while(child!=null) {
            final List<Integer> childPosition = new ArrayList<Integer>(position);
            childPosition.add(child.getComponentNumber());
            if(child instanceof ParagraphComponent) {
                createParagraphOperations((ParagraphComponent)child, childPosition, target);
            }
            child = (OdfComponent)child.getNextComponent();
        }
    }

    public void createShapeGroupOperations(ShapeGroupComponent shapeComponent, List<Integer> position, String target) throws Exception {

        final OpAttrs attrs = new OpAttrs();
        shapeComponent.createJSONAttrs(attrs);
        addInsertDrawingOperation(position, ((IDrawingType)shapeComponent).getType(), attrs, target);
        OdfComponent child = (OdfComponent)shapeComponent.getNextChildComponent(null, null);
        while(child!=null) {
            final List<Integer> childPosition = new ArrayList<Integer>(position);
            childPosition.add(child.getComponentNumber());
            if(child instanceof ShapeComponent) {
                createShapeOperations((ShapeComponent)child, childPosition, target);
            }
            else if(child instanceof ShapeGroupComponent) {
                createShapeGroupOperations((ShapeGroupComponent)child, childPosition, target);
            }
            else if(child instanceof FrameComponent) {
                createFrameOperations((FrameComponent)child, childPosition, target);
            }
            child = (OdfComponent)child.getNextComponent();
        }
    }

    public void createParagraphOperations(ParagraphComponent paragraphComponent, List<Integer> paragraphPosition, String target)
        throws JSONException, SAXException {

        final JSONObject jsonInsertParagraphOperation = new JSONObject(4);
        jsonInsertParagraphOperation.put(OCKey.NAME.value(), OCValue.INSERT_PARAGRAPH.value());
        jsonInsertParagraphOperation.put(OCKey.START.value(), paragraphPosition);
        if(target!=null) {
            jsonInsertParagraphOperation.put(OCKey.TARGET.value(), target);
        }
        final OpAttrs paragraphAttrs = new OpAttrs();
        paragraphComponent.createJSONAttrs(paragraphAttrs);
        if(!paragraphAttrs.isEmpty()) {
            jsonInsertParagraphOperation.put(OCKey.ATTRS.value(), paragraphAttrs);
        }
        operationQueue.put(jsonInsertParagraphOperation);

        OpAttrs currentAttrs = new OpAttrs();
        OdfComponent paragraphChild = (OdfComponent)paragraphComponent.getNextChildComponent(null, null);
        while(paragraphChild!=null) {
            OpAttrs attrs = new OpAttrs();
            paragraphChild.createJSONAttrs(attrs);
            if(!opsDoc.isCreateFastLoadOperations()) {
                attrs = OpAttrs.createDifference(currentAttrs, attrs);
            }
            final List<Integer> textPosition = new ArrayList<Integer>(paragraphPosition);
            textPosition.add(paragraphChild.getComponentNumber());
            if(paragraphChild instanceof TextComponent) {
                createTextInsertOperation(textPosition, target, ((Text)((TextComponent)paragraphChild).getObject()).getText(), attrs);
            }
            else if(paragraphChild instanceof TextTabComponent) {
                addInsertTabOperation(textPosition, target, attrs);
            }
            else if(paragraphChild instanceof TextFieldComponent) {
                addInsertFieldOperation(textPosition, (TextField)paragraphChild.getObject(), target, attrs);
            }
            else if(paragraphChild instanceof TextLineBreakComponent) {
                addInsertHardBreakOperation(textPosition, target, attrs);
            }
            paragraphChild = (OdfComponent)paragraphChild.getNextComponent();
        }
    }

    public void createTextInsertOperation(List<Integer> position, String target, String text, OpAttrs attrs)
        throws JSONException {

        final JSONObject jsonInsertTextOperation = new JSONObject(4);
        jsonInsertTextOperation.put(OCKey.NAME.value(), OCValue.INSERT_TEXT.value());
        jsonInsertTextOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertTextOperation.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            jsonInsertTextOperation.put(OCKey.ATTRS.value(), attrs);
        }
        jsonInsertTextOperation.put(OCKey.TEXT.value(), text);
        operationQueue.put(jsonInsertTextOperation);
    }

    public void addInsertDrawingOperation(List<Integer> start, DrawingType type, OpAttrs attrs, String target)
        throws JSONException {

        final JSONObject insertDrawingObject = new JSONObject(5);
        insertDrawingObject.put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        insertDrawingObject.put(OCKey.START.value(), start);
        insertDrawingObject.put(OCKey.TYPE.value(), type.toString());
        if(!attrs.isEmpty()) {
            insertDrawingObject.put(OCKey.ATTRS.value(), attrs);
        }
        if(target!=null) {
            insertDrawingObject.put(OCKey.TARGET.value(), target);
        }
        operationQueue.put(insertDrawingObject);
    }

    public void addInsertCellsOperation(final List<Integer> start, String target, final OpAttrs attrs)
        throws JSONException {

        final JSONObject insertCellsObject = new JSONObject(3);
        insertCellsObject.put(OCKey.NAME.value(), OCValue.INSERT_CELLS.value());
        insertCellsObject.put(OCKey.START.value(), start);
        if(target!=null) {
            insertCellsObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
            insertCellsObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertCellsObject);
    }

    public void addInsertRowsOperation(final List<Integer> start, int count, String target, final OpAttrs attrs)
        throws JSONException {

        final JSONObject insertRowObject = new JSONObject(4);
        insertRowObject.put(OCKey.NAME.value(), OCValue.INSERT_ROWS.value());
        insertRowObject.put(OCKey.START.value(), start);
        if(count!=1) {
            insertRowObject.put(OCKey.COUNT.value(), count);
        }
        if(target!=null) {
            insertRowObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null&&!attrs.isEmpty()) {
            insertRowObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertRowObject);
    }

    public void addInsertTabOperation(final List<Integer> start, String target, OpAttrs attrs)
        throws JSONException {

        final JSONObject insertTabObject = new JSONObject(3);
        insertTabObject.put(OCKey.NAME.value(), OCValue.INSERT_TAB.value());
        insertTabObject.put(OCKey.START.value(), start);
        if(target!=null) {
            insertTabObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            insertTabObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertTabObject);
    }

    public void addInsertFieldOperation(List<Integer> start, TextField field, String target, OpAttrs attrs)
        throws JSONException {

        final JSONObject insertFieldObject = new JSONObject(5);
        insertFieldObject.put(OCKey.NAME.value(), OCValue.INSERT_FIELD.value());
        insertFieldObject.put(OCKey.START.value(), start);
        insertFieldObject.put(OCKey.TYPE.value(), field.getType());
        insertFieldObject.put(OCKey.REPRESENTATION.value(), field.getRepresentation());
        if(target!=null) {
            insertFieldObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            insertFieldObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertFieldObject);
    }

    public void addInsertHardBreakOperation(List<Integer> start, String target, OpAttrs attrs)
        throws JSONException {

        final JSONObject insertHardBreakObject = new JSONObject(3);
        insertHardBreakObject.put(OCKey.NAME.value(), OCValue.INSERT_HARD_BREAK.value());
        insertHardBreakObject.put(OCKey.START.value(), start);
        if(target!=null) {
            insertHardBreakObject.put(OCKey.TARGET.value(), target);
        }
        if(attrs!=null && !attrs.isEmpty()) {
            insertHardBreakObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(insertHardBreakObject);
    }

    public void createMasterStyles(SlideComponent slideComponent, OpAttrs attrs) {
        final StyleBase graphicStyle = styleManager.getStyle("standard", StyleFamily.GRAPHIC, false);
        if(graphicStyle!=null) {
            styleManager.createPresentationAttributes(graphicStyle, OCKey.OTHER.value(), 10, false, attrs);
        }
        OdfComponent component = (OdfComponent)slideComponent.getNextChildComponent(null, null);
        while(component!=null) {
            if(component.getObject() instanceof IAttributesAccessor) {
                final String presentationClass = ((IAttributesAccessor)component.getObject()).getAttributes().getValue("presentation:class");
                if(presentationClass!=null) {
                    final String presentationStyleName = ((IAttributesAccessor)component.getObject()).getAttributes().getValue("presentation:style-name");
                    if(presentationStyleName!=null&&!presentationStyleName.isEmpty()) {
                        final StyleBase styleBase = styleManager.getStyle(presentationStyleName, StyleFamily.PRESENTATION, false);
                        if(styleBase instanceof StylePresentation) {
                            final StylePresentation stylePresentation = (StylePresentation)styleBase;
                            if(presentationClass.equals("title")) {
                                styleManager.createPresentationAttributes(stylePresentation, OCKey.TITLE.value(), 1, false, attrs);
                            }
                            else if(presentationClass.equals("outline")) {
                                styleManager.createPresentationAttributes(stylePresentation, OCKey.BODY.value(), 9, false, attrs);
                            }
                        }
                    }
                }
            }
            component = (OdfComponent)component.getNextComponent();
        }
    }
}
