/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf;

import java.util.Iterator;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;

public class Decl implements IElementWriter {

    private final AttributesImpl attributes;
    private final String qName;
    private final String localName;
    private final String namespace;
    private String value;
    private DLList<IElementWriter> content;

	public Decl(String qName, String localName, String namespace, AttributesImpl attributes) {
	    this.attributes = attributes;
	    this.qName = qName;
	    this.localName = localName;
	    this.namespace = namespace;
	}

	public String getValue() {
	    return value;
	}

	public void setValue(String value) {
	    this.value = value;
	}

	public String getName() {
	    return attributes.getValue("presentation:name");
	}

	public void setName(String name) {
	    attributes.setValue(Namespaces.PRESENTATION, "name", "presentation:name", name);
	}

	public AttributesImpl getAttributes() {
	    return attributes;
	}

	public DLList<IElementWriter> getContent(boolean forceCreate) {
	    if(content==null&&forceCreate) {
	        content = new DLList<IElementWriter>();
	    }
		return content;
	}

    @Override
    public void writeObject(SerializationHandler output) throws SAXException {
        SaxContextHandler.startElement(output, namespace, localName, qName);
        attributes.write(output);
        if(value!=null&&!value.isEmpty()) {
            output.characters(value);
        }
        if(content!=null) {
            final Iterator<IElementWriter> childIter = content.iterator();
            while(childIter.hasNext()) {
                childIter.next().writeObject(output);
            }
        }
        SaxContextHandler.endElement(output, namespace, localName, qName);
    }
    
}
