/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;

public class Path extends Shape {

    public Path(OdfOperationDoc operationDoc, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
        super(operationDoc, Namespaces.DRAW, "path", "draw:path", parentGroup, rootShape, contentStyle);
    }

    public Path(AttributesImpl attributes, String uri, String localName, String qName, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
        super(attributes, uri, localName, qName, parentGroup, rootShape, contentStyle);
    }

    public void setContent(DLList<Object> content) {
        childs = content;
    }

    @Override
    public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle)
        throws JSONException {

        super.applyAttrsFromJSON(operationDocument, attrs, contentAutoStyle);

        final JSONObject geometryAttrs = attrs.optJSONObject(OCKey.GEOMETRY.value());
        if(geometryAttrs!=null) {
            final JSONArray pathList = geometryAttrs.optJSONArray(OCKey.PATH_LIST.value());
            if(pathList!=null&&pathList.length()==1) {
                SVGPathParser.pathListToSvg(pathList, getAttributes());
            }
        }
    }

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
        super.createAttrs(operationDocument, attrs, contentAutoStyle);

        attrs.getMap(OCKey.GEOMETRY.value(), true).put(OCKey.PATH_LIST.value(), SVGPathParser.getPathList(attributes.getValue("svg:d"), new ViewBox(attributes.getValue("svg:viewBox")), true));
    }

    // in LO/OO the style:mirror does not work with path objects :-( so the whole path of the shape has to be flipped
    public void flipH() {
        final ViewBox viewBox = new ViewBox(attributes.getValue("svg:viewBox"));
        if(viewBox.isValid()) {
            final List<OpAttrs> pathList = SVGPathParser.getPathList(attributes.getValue("svg:d"), viewBox, true);
            SVGPathParser.flipH(pathList, viewBox);
            try {
                SVGPathParser.pathListToSvg(new JSONArray(pathList), attributes);
            } catch (JSONException e) {
                //
            }
        }
    }
}
