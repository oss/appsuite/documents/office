/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.doc.OdfPresentationDocument;
import org.odftoolkit.odfdom.pkg.OdfPackageDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class DrawFrame extends Shape {

    private Boolean isPresentationStyle = null;
    private final OdfPackageDocument packageDocument;

    public DrawFrame(OdfPackageDocument packageDocument, AttributesImpl attributes, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
        super(attributes, Namespaces.DRAW, "frame", "draw:frame", parentGroup, rootShape, contentStyle);
        this.packageDocument = packageDocument;
	}

	public DrawFrame(OdfOperationDoc operationDoc, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
        super(operationDoc, Namespaces.DRAW, "frame", "draw:frame", parentGroup, rootShape, contentStyle);
        this.packageDocument = operationDoc.getDocument();

        if(this.packageDocument.getDocumentType().equals("text")) {
            this.attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "as-char");
        }
	}

	public IDrawing getDrawing() {

	    IDrawing image1 = null;
        IDrawing image2 = null;
	    IDrawing chart = null;
	    IDrawing def = null;

	    if(!childs.isEmpty()) {
		    final Iterator<Object> childIter = childs.iterator();
		    while(childIter.hasNext()) {
		        final Object o = childIter.next();
		        if(o instanceof IDrawing) {
		            if(!((IDrawing)o).preferRepresentation()) {
		                return (IDrawing)o;
		            }
		            final DrawingType drawingType = ((IDrawing)o).getType();
		            if (drawingType == DrawingType.IMAGE) {
		                if (o instanceof DrawImage) {
		                    if (((DrawImage)o).getFormat().equals("svg")) {
		                        image2 = (IDrawing)o;
		                    }
		                    else {
	                            image1 = (IDrawing)o;
		                    }
		                }
		                else {
		                    def = (IDrawing)o;
		                }
		            }
		            else if (drawingType == DrawingType.CHART) {
		                chart = (IDrawing)o;
		            }
		            else {
		                def = (IDrawing)o;
		            }
		        }
		    }
		}
	    // DOCS-4609, only when loading Spreadsheet documents Charts are preferred, in other cases the Chart representation is taken  
	    if("spreadsheet".equals(packageDocument.getDocumentType())) {
			return chart != null ? chart : image1 != null ? image1 : image2 != null ? image2 : def;
	    }
		return image1 != null ? image1 : image2 != null ? image2 : chart != null ? chart : def;
	}

	@Override
	public DLList<Object> getContent() {
		final IDrawing iDrawing = getDrawing();
		if(iDrawing!=null) {
		    return iDrawing.getContent();
		}
		return childs;
	}

	public void addContent(IElementWriter element) {
        childs.add(element);
	}

	@Override
    public DrawingType getType() {
		final IDrawing drawing = getDrawing();
		if(drawing!=null) {
			return drawing.getType();
		}
		return DrawingType.UNDEFINED;
	}

	@Override
    public AttributesImpl getAttributes() {
		return attributes;
	}

	@Override
    public String getStyleName() {
	    if(isPresentationStyle()) {
	        return getPresentationStyleName();
	    }
		return attributes.getValue("draw:style-name");
	}

	@Override
    public void setStyleName(String name) {
	    if(isPresentationStyle()) {
	        setPresentationStyleName(name);
	    }
	    else {
	        attributes.setValue(Namespaces.DRAW, "style-name", "draw:style-name", name);
	    }
	}

	public String getTextStyleName() {
	    return attributes.getValue("draw:text-style-name");
	}

	public void setTextStyleName(String name) {
	    attributes.setValue(Namespaces.DRAW, "text-style-name", "draw:text-style-name", name);
	}

	public String getPresentationStyleName() {
	    return attributes.getValue("presentation:style-name");
	}

	public void setPresentationStyleName(String name) {

	    attributes.setValue(Namespaces.PRESENTATION, "style-name", "presentation:style-name", name);
	}

	public String getPresentationClass() {
	    return attributes.getValue("presentation:class");
	}

	public void setPresentationClass(String presentationClass) {
	    attributes.setValue(Namespaces.PRESENTATION, "class", "presentation:class", presentationClass);
	}

	public boolean isPresentationStyle() {
	    if(isPresentationStyle==null) {
    	    final String presentationStyleName = getPresentationStyleName();
    	    isPresentationStyle =  presentationStyleName != null && !presentationStyleName.isEmpty();
	    }
	    return isPresentationStyle.booleanValue();
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.DRAW, "frame", "draw:frame");
		final IDrawing drawing = getDrawing();
		if(drawing instanceof DrawTextBox&&attributes.containsKey("presentation:class")) {
		    if(!((DrawTextBox)drawing).isSaveContent()||drawing.getContent().isEmpty()) {
		        attributes.setBooleanValue(Namespaces.PRESENTATION, "placeholder", "presentation:placeholder", Boolean.TRUE);
		    }
		    else {
	            attributes.remove("presentation:placeholder");
	        }
		}
		attributes.write(output);
		for(Object child:childs) {
			((IElementWriter)child).writeObject(output);
		}
		SaxContextHandler.endElement(output, Namespaces.DRAW, "frame", "draw:frame");
	}

	@Override
	public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle)
		throws JSONException {

	    super.applyAttrsFromJSON(operationDocument, attrs, contentAutoStyle);

	    final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
        final String presentationStyleName = getPresentationStyleName();

        final JSONObject presentationProps = attrs.optJSONObject(OCKey.PRESENTATION.value());
        if(presentationProps!=null) {
            final Object phType = presentationProps.opt(OCKey.PH_TYPE.value());
            if(phType==JSONObject.NULL) {
                attributes.remove("presentation:class");
                attributes.remove("presentation:placeholder");
                attributes.remove("presentation:user-transformed");
            }
            else if(phType instanceof String) {
                String presentationClass = null;
                switch((String)phType) {
                    case "chart" : presentationClass = "chart"; break;
                    case "pic" : presentationClass = "graphic"; break;
                    case "obj" : presentationClass = "object"; break;
                    case "dgm" : presentationClass = "orgchart"; break;
                    case "body" : presentationClass = "outline"; break;
                    case "subTitle" : presentationClass = "subtitle"; break;
                    case "tbl" : presentationClass = "table"; break;
//                  case "body" : presentationClass = "text"; break;
                    case "title": presentationClass = "title"; break;
                    case "dt" :  presentationClass = "date-time"; break;
                    case "ftr" : presentationClass = "footer"; break;
                    case "hdr" : presentationClass = "header"; break;
                    case "sldNum" : presentationClass = "page-number"; break;
                }
                if(presentationClass!=null) {
                    attributes.setValue(Namespaces.PRESENTATION, "class", "presentation:class", presentationClass);
                    // Fix bug DOCS-2144 Color Part
                    isPresentationStyle = true;
                }
            }
            final Object userTransformed = presentationProps.opt("userTransformed");
            if(userTransformed==JSONObject.NULL) {
                attributes.remove("presentation:user-transformed");
            }
            else if(userTransformed instanceof Boolean) {
                attributes.setBooleanValue(Namespaces.PRESENTATION, "user-transformed", "presentation:user-transformed", (Boolean)userTransformed);
            }
            setPresentationStyleName(styleManager.createStyle(StyleFamily.PRESENTATION, getPresentationStyleName(), contentAutoStyle, attrs));
        }
        else if(isPresentationStyle()) {
            setPresentationStyleName(styleManager.createStyle(StyleFamily.PRESENTATION, presentationStyleName, contentAutoStyle, attrs));
        }
        else {
            setStyleName(styleManager.createStyle(StyleFamily.GRAPHIC, getStyleName(), contentAutoStyle, attrs));
        }
		final IDrawing iDrawing = getDrawing();
		if(iDrawing!=null) {
			iDrawing.applyAttrsFromJSON(operationDocument, attrs, contentAutoStyle);
		}
	}

	@Override
	public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
	    super.createAttrs(operationDocument, attrs, contentAutoStyle);

	    final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
		final Map<String, AttributeImpl> frameAttributes = getAttributes().getUnmodifiableMap();
        final String textStyleName = getTextStyleName();
        if(textStyleName!=null&&!textStyleName.isEmpty()) {
            styleManager.createAutoStyleAttributes(attrs, textStyleName, StyleFamily.PARAGRAPH, contentAutoStyle);
        }
        if(operationDocument.getDocument() instanceof OdfPresentationDocument) {
            final String presentationStyleName = getPresentationStyleName();
            if(presentationStyleName!=null&&!presentationStyleName.isEmpty()) {
                boolean createAutoStyles = false;
                if(contentAutoStyle) {
                    final String presentationClass = getPresentationClass();
                    if(("title").equals(presentationClass)||("outline").equals(presentationClass)) {
                        createAutoStyles = true;
                    }
                }
                if(createAutoStyles) {
                    styleManager.createAutoStyleAttributes(attrs, presentationStyleName, StyleFamily.PRESENTATION, contentAutoStyle);
                }
                else {
                    styleManager.createPresentationStyle(attrs, presentationStyleName, contentAutoStyle);
                }
            }
            else {
                final String styleName = getStyleName();
                if(styleName!=null&&!styleName.isEmpty()) {
                    styleManager.createAutoStyleAttributes(attrs, styleName, StyleFamily.GRAPHIC, contentAutoStyle);
                }
            }

            attrs.remove(OCKey.STYLE_ID.value());

            final AttributeImpl presentationClassAttr = frameAttributes.get("presentation:class");
            if(presentationClassAttr!=null) {
                final String presentationClass = presentationClassAttr.getValue();
                if(presentationClass!=null) {
                    String phType = null;
                    switch(presentationClass) {
                        case "chart": phType = "chart"; break;
                        case "graphic": phType = "pic"; break;
                        case "object": phType = "obj"; break;
                        case "orgchart": phType = "dgm"; break;
                        case "outline": phType = "body"; break;
                        case "subtitle": phType = "subTitle"; break;
                        case "table": phType = "tbl"; break;
                        case "text":  phType = "body"; break;
                        case "title": phType = "title"; break;
                        // following classes are only usable with shapes on master pages
                        case "date-time": phType = "dt"; break;
                        case "footer": phType = "ftr"; break;
                        case "header": phType = "hdr"; break;
                        case "page-number": phType = "sldNum"; break;
                        // following classes are allowed on notes and handout pages only
                        case "page":
                        case "handout":
                        case "notes":
                            //
                        break;
                    }
                    if(phType!=null) {
                        attrs.getMap(OCKey.PRESENTATION.value(), true).put(OCKey.PH_TYPE.value(), phType);
                    }
                }
                final AttributeImpl userDefined = frameAttributes.get("presentation:user-transformed");
                if(userDefined!=null) {
                    attrs.getMap(OCKey.PRESENTATION.value(), true).put("userTransformed", AttributesImpl.getBoolean(userDefined.getValue(), Boolean.FALSE));
                }
            }
        }
        else {
            final String styleName = getStyleName();
            if(styleName!=null&&!styleName.isEmpty()) {
                styleManager.createAutoStyleAttributes(attrs, styleName, StyleFamily.GRAPHIC, contentAutoStyle);
            }
        }
        final IDrawing iDrawing = getDrawing();
        if(iDrawing!=null) {
        	iDrawing.createAttrs(operationDocument, attrs, contentAutoStyle);

        	if(iDrawing.getType() == DrawingType.IMAGE) {
	        	// DOCS-4616, there exist a problem in the ODF spec, the draw:fill attribute has no default value and this attribute is sometimes missing.
	    		// If this attribute is missing then LO interprets this depending to the object that uses this attribute, e.g. normal shapes and placeholder
	    		// objects are using a solid fill whereas images uses a fill none. Missing draw:fill attributes are defaulted by us to a SOLID fill, here
	    		// in case when loading a image we have to change this to a fill:none.
	    		final HashMap<String, AttributeImpl> xmlAttributes = styleManager.getXmlAttributes(new HashSet<String>(Arrays.asList("draw:fill", "draw:stroke", "fo:border")), getStyleName(), StyleFamily.GRAPHIC, contentAutoStyle, StyleManager.AttributeStyleFilter.GRAPHIC);
	    		if(xmlAttributes.get("draw:fill") == null) {
	    			attrs.getMap(OCKey.FILL.value(), true).put(OCKey.TYPE.value(), "none");
	    		}
                if(xmlAttributes.get("draw:stroke") == null && xmlAttributes.get("fo:border") == null) {
                    attrs.getMap(OCKey.LINE.value(), true).put(OCKey.TYPE.value(), "none");
                }
        	}
        }
	}
}
