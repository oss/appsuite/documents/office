/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

/*
 * in contrast to normal shapes the line is using "svg:x1, svg:y1, svg:y2, svg:x2" instead of "svg:x, svg:y, svg:width and svg:height" :(
 */

public class ConnectorShape extends Shape {

    final private ViewBox vb;

    public ConnectorShape(AttributesImpl attributes, String uri, String localName, String qName, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
	    super(attributes, uri, localName, qName, parentGroup, rootShape, contentStyle);

	    vb = SVGPathParser.getViewBox(attributes.getValue("svg:d"));
	    if(vb!=null) {
	        transformer.setX(vb.getX());
	        transformer.setY(vb.getY());
	        transformer.setWidth(vb.getWidth());
	        transformer.setHeight(vb.getHeight());
	        transformer.setFlipH(null);
            transformer.setFlipV(null);
	    }
    }

    @Override
    public DrawingType getType() {
        return DrawingType.SHAPE;
    }

	@Override
	public boolean isLineMode() {
	    return true;
	}

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
        super.createAttrs(operationDocument, attrs, contentAutoStyle);

        // wrong behaviour for svg:viewBox and svg:d in draw:connector
        // https://bugs.documentfoundation.org/show_bug.cgi?id=83360
        // -> so we have to adjust the viewBox
        final List<OpAttrs> pathList = SVGPathParser.getPathList(attributes.getValue("svg:d"), vb, false);
        if(!pathList.isEmpty()) {
            final OpAttrs geometryAttrs = attrs.getMap(OCKey.GEOMETRY.value(), true);
            pathList.get(0).put(OCKey.FILL_MODE.value(), "none");
            pathList.get(0).put(OCKey.LINE_SELECTION.value(), false);
            geometryAttrs.put(OCKey.PATH_LIST.value(), pathList);
        }
        attrs.getMap(OCKey.FILL.value(), true).put(OCKey.TYPE.value(), "none");
        attrs.getMap(OCKey.SHAPE.value(), true).put(OCKey.WORD_WRAP.value(), Boolean.TRUE);
        attrs.getMap(OCKey.SHAPE.value(), true).put(OCKey.AUTO_RESIZE_HEIGHT.value(), Boolean.FALSE);
    }

    public Path convertConnectorToPath(OdfOperationDoc operationDocument, boolean contentAutoStyle) throws JSONException {
        final Path path = new Path(attributes, Namespaces.DRAW, "path", "draw:path", parentGroup, rootShape, contentAutoStyle);
        final Transformer pathTrans = path.getTransformer();
        pathTrans.setLineMode(false);
        pathTrans.setX(transformer.getX());
        pathTrans.setY(transformer.getY());
        pathTrans.setWidth(transformer.getWidth());
        pathTrans.setHeight(transformer.getHeight());
        pathTrans.setInitilized(true);
        attributes.remove("svg:x1");
        attributes.remove("svg:y1");
        attributes.remove("svg:x2");
        attributes.remove("svg:y2");
        attributes.remove("draw:lines");
        attributes.remove("draw:line-skew");
        attributes.remove("draw:start-shape");
        attributes.remove("draw:start-glue-point");
        attributes.remove("draw:end-shape");
        attributes.remove("draw:end-glue-point");

        final List<OpAttrs> pathList = SVGPathParser.getPathList(attributes.getValue("svg:d"), vb, false);
        if(pathList!=null) {
            SVGPathParser.pathListToSvg(new JSONArray(pathList), attributes);
        }

        final JSONObject attrs = new JSONObject();
        final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
        final StyleBase styleBase = styleManager.getStyleBaseClone(StyleFamily.GRAPHIC, getStyleName(), contentAutoStyle);
        styleManager.applyAttrs(styleBase, contentAutoStyle, attrs);
        final JSONObject _attrs = new JSONObject(1);
        final JSONObject _fillAttrs = new JSONObject(1);
        _attrs.put(OCKey.FILL.value(), _fillAttrs);
        _fillAttrs.put(OCKey.TYPE.value(), "none");
        final JSONObject _shapeAttrs = new JSONObject(1);
        _attrs.put(OCKey.SHAPE.value(), _shapeAttrs);
        _shapeAttrs.put(OCKey.AUTO_RESIZE_HEIGHT.value(), Boolean.FALSE);
        _shapeAttrs.put(OCKey.WORD_WRAP.value(), Boolean.TRUE);
        styleBase.applyAttrs(styleManager, _attrs);
        path.setStyleName(styleManager.getStyleIdForStyleBase(styleBase));
        path.setContent(getContent());
        path.prepareSaveShape(styleManager);
        return path;
    }
}
