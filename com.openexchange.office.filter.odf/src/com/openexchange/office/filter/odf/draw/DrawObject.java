/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.util.Iterator;
import java.util.Map.Entry;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.ods.dom.chart.ChartContent;

public class DrawObject implements IDrawing {

	private final DrawFrame drawFrame;
	private final AttributesImpl attributes;
    private final DLList<Object> childs = new DLList<Object>();
    private ChartContent chartContent = null;

	public DrawObject(DrawFrame drawFrame) {
		this.drawFrame = drawFrame;
		this.attributes = new AttributesImpl();
	}

	public DrawObject(DrawFrame drawFrame, Attributes attributes) {
		this.drawFrame = drawFrame;
		this.attributes = new AttributesImpl(attributes);
	}

	public void setChart(ChartContent chartContent) {
	    this.chartContent = chartContent;
	}

	public ChartContent getChart() {
	    return chartContent;
	}

	@Override
	public DLList<Object> getContent() {
		return childs;
	}

    @Override
    public AttributesImpl getAttributes() {
        return attributes;
    }

    @Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.DRAW, "object", "draw:object");
		attributes.write(output);
		final Iterator<Object> childIter = childs.iterator();
		while(childIter.hasNext()) {
		    ((IElementWriter)childIter.next()).writeObject(output);
		}
		SaxContextHandler.endElement(output, Namespaces.DRAW, "object", "draw:object");
	}

	@Override
	public DrawingType getType() {
	    return chartContent!=null ? DrawingType.CHART : DrawingType.UNDEFINED;
	}

	@Override
	public boolean preferRepresentation() {
	    return true;
	}

	@Override
	public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle)
		throws JSONException {

        if (attrs.has(OCKey.CHART.value()) && chartContent!=null) {
            chartContent.applyDrawingAttributes(attrs);
        }
	}

	@Override
	public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
	    if(chartContent!=null) {
	        final JSONObject chartAttrs = new JSONObject();
	        chartContent.getChart().createAttributes(chartAttrs);
	        mergeAttributes(attrs, chartAttrs);
	    }
		final String hRef = attributes.getValue("xlink:href");
		if(hRef!=null) {
        }
		final String id = attributes.getValue("xml:id");
		if(id!=null) {
			attrs.getMap(OCKey.DRAWING.value(), true).put("imageXmlId", id);
        }
	}

	private static void mergeAttributes(OpAttrs target, JSONObject attrs) {
        for (Entry<String, Object> entry : attrs.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            if (value instanceof JSONObject) {
                if (!target.containsKey(key)) {
                    target.put(key, value);
                } else {
                    mergeAttributes(target.getMap(key, true), (JSONObject) value);
                }
            } else {
                target.put(key, value);
            }
        }
    }
}
