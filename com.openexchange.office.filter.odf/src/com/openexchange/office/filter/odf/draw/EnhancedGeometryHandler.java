/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import org.xml.sax.Attributes;
import com.google.common.collect.ImmutableMap;
import com.openexchange.office.filter.core.EnhancedGeometryType;
import com.openexchange.office.filter.core.STShapeType;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementToBase64Handler;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.draw.EnhancedGeometry.MT;

public class EnhancedGeometryHandler extends ElementToBase64Handler {

    final CustomShape customShape;

    public EnhancedGeometryHandler(SaxContextHandler parentContext, CustomShape customShape, Attributes attributes, String uri, String localName, String qName) {
    	super(parentContext, attributes, uri, localName, qName);

    	this.customShape = customShape;

    	final EnhancedGeometry enhancedGeometry = customShape.getEnhancedGeometry(true);
    	final String shapeType = attributes.getValue("draw:type");
    	if(shapeType!=null) {
        	enhancedGeometry.setShapeType(shapeType);
        	STShapeType presetShape = EnhancedGeometryType.getPresetShape(shapeType);
            if(presetShape==null) {
                if("mso-spt100".equals(shapeType)) {
                    final String enhancedPath = attributes.getValue("draw:enhanced-path");
                    if(enhancedPath!=null) {
                        if(enhancedPath.equals("V 0 0 21600 21600 ?f5 ?f7 ?f1 ?f3 L 10800 10800 Z N")) {
                            presetShape = STShapeType.PIE;
                        }
                        else if(enhancedPath.startsWith("M ?f0 0 L 21600 10800 ?f0 21800 ?f0 ?f2")) {
                            presetShape = STShapeType.STRIPED_RIGHT_ARROW;
                        }
                    }
                }
                if(presetShape==null) {
                    final String enhancedPath = attributes.getValue("draw:enhanced-path");
                    if(enhancedPath!=null&&!enhancedPath.contains("$")&&!enhancedPath.contains("?")) {
                        enhancedGeometry.setPathList(SVGPathParser.getPathList(enhancedPath, new ViewBox(attributes.getValue("svg:viewBox")), false));
                    }
                }
            }
        	enhancedGeometry.setPresetShape(presetShape);

        	final String modifiers = attributes.getValue("draw:modifiers");
            if(modifiers!=null) {
                final EnhancedGeometryType enhancedGeometryType = EnhancedGeometryType.fromValue(shapeType);
                if(enhancedGeometryType!=null) {
                    final String[] tokens = modifiers.split("\\s+");
                    if(enhancedGeometryType.isOOXMLType()) {
                        int i = 1;
                        final String adjNames[] = OOXMLAdjustmentNames.get(presetShape);
                        for(String token:tokens) {
                            Double number = 0.0;
                            try {
                                number = Double.parseDouble(token);
                            }
                            catch(NumberFormatException e) {
                                // ohoh
                            }
                            if(adjNames!=null&&adjNames.length>=i) {
                                enhancedGeometry.getModifiers(true).put(adjNames[i-1], number);
                            }
                            else {
                                enhancedGeometry.getModifiers(true).put("adj" + Integer.valueOf(i++).toString(), number);
                            }
                        }
                    }
                    else {
                        final MT modifierTransformation = EnhancedGeometry.OOShapeToOOXModifierTransformation.get("mso-spt100".equals(shapeType) ? presetShape : shapeType);
                        if(modifierTransformation!=null) {
                            enhancedGeometry.setModifierTransformer(modifierTransformation);
                            modifierTransformation.transform(tokens, enhancedGeometry.getModifiers(true));
                        }
                    }

                }
            }
            customShape.getTransformer().setFlipH(AttributesImpl.getBoolean(attributes.getValue("draw:mirror-horizontal"), null));
            customShape.getTransformer().setFlipV(AttributesImpl.getBoolean(attributes.getValue("draw:mirror-vertical"), null));
    	}
    	enhancedGeometry.setModified(false);
	}

	@Override
	final protected void setBase64ElementData(String data) {
	    customShape.getEnhancedGeometry(true).setShapeData(data);
	}

	static final protected ImmutableMap<STShapeType, String[]> OOXMLAdjustmentNames = ImmutableMap.<STShapeType, String[]> builder()
        .put(STShapeType.BEVEL, new String[] {"adj"})
        .put(STShapeType.BRACE_PAIR, new String[] {"adj"})
        .put(STShapeType.BRACKET_PAIR, new String[] {"adj"})
        .put(STShapeType.CAN, new String[] {"adj"})
        .put(STShapeType.CHEVRON, new String[] {"adj"})
        .put(STShapeType.CUBE, new String[] {"adj"})
        .put(STShapeType.DECAGON, new String[] {"vf"})
        .put(STShapeType.DIAG_STRIPE, new String[] {"adj"})
        .put(STShapeType.DONUT, new String[] {"adj"})
        .put(STShapeType.FOLDED_CORNER, new String[] {"adj"})
        .put(STShapeType.HEPTAGON, new String[] {"hf", "vf"})
        .put(STShapeType.HEXAGON, new String[] {"adj", "vf"})
        .put(STShapeType.HOME_PLATE, new String[] {"adj"})
        .put(STShapeType.HORIZONTAL_SCROLL, new String[] {"adj"})
        .put(STShapeType.LEFT_BRACKET, new String[] {"adj"})
        .put(STShapeType.MOON, new String[] {"adj"})
        .put(STShapeType.NO_SMOKING, new String[] {"adj"})
        .put(STShapeType.OCTAGON, new String[] {"adj"})
        .put(STShapeType.PARALLELOGRAM, new String[] {"adj"})
        .put(STShapeType.PENTAGON, new String[] {"hf", "vf"})
        .put(STShapeType.PLAQUE, new String[] {"adj"})
        .put(STShapeType.PLUS, new String[] {"adj"})
        .put(STShapeType.RIGHT_BRACKET, new String[] {"adj"})
        .put(STShapeType.ROUND_1_RECT, new String[] {"adj"})
        .put(STShapeType.ROUND_RECT, new String[] {"adj"})
        .put(STShapeType.SMILEY_FACE, new String[] {"adj"})
        .put(STShapeType.SNIP_1_RECT, new String[] {"adj"})
        .put(STShapeType.STAR_10, new String[] {"adj", "hf"})
        .put(STShapeType.STAR_12, new String[] {"adj"})
        .put(STShapeType.STAR_16, new String[] {"adj"})
        .put(STShapeType.STAR_24, new String[] {"adj"})
        .put(STShapeType.STAR_32, new String[] {"adj"})
        .put(STShapeType.STAR_4, new String[] {"adj"})
        .put(STShapeType.STAR_5, new String[] {"adj", "hf", "vf"})
        .put(STShapeType.STAR_6, new String[] {"adj", "hf"})
        .put(STShapeType.STAR_7, new String[] {"adj", "hf", "vf"})
        .put(STShapeType.STAR_8, new String[] {"adj"})
        .put(STShapeType.SUN, new String[] {"adj"})
        .put(STShapeType.SWOOSH_ARROW, new String[] {"adj"})
        .put(STShapeType.TEARDROP, new String[] {"adj"})
        .put(STShapeType.TRAPEZOID, new String[] {"adj"})
        .put(STShapeType.TRIANGLE, new String[] {"adj"})
        .put(STShapeType.VERTICAL_SCROLL, new String[] {"adj"})
        .build();
}
