/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Iterator;
import org.apache.commons.lang3.BooleanUtils;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class GroupShape extends Shape {

    private Rectangle2D childRect = null;

    public GroupShape(OdfOperationDoc operationDoc, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
        super(operationDoc, Namespaces.DRAW, "g", "draw:g", parentGroup, rootShape, contentStyle);
    }

    public GroupShape(AttributesImpl attributes, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
	    super(attributes, Namespaces.DRAW, "g", "draw:g", parentGroup, rootShape, contentStyle);
	}

    @Override
    public Rectangle2D getChildRect() {
        if(childRect==null) {
            boolean first = true;
            Rectangle2D rect2D = new Rectangle2D.Double();
            for(Object child:getContent()) {
                if(child instanceof Shape) {
                    final Rectangle2D c = ((Shape)child).getChildRect();
                    if(first) {
                        rect2D = c;
                        first = false;
                    }
                    else {
                        rect2D.add(new Point2D.Double(c.getMinX(), c.getMinY()));
                        rect2D.add(new Point2D.Double(c.getMaxX(), c.getMaxY()));
                    }
                }
            }
            childRect = rect2D;
        }
        return childRect;
    }

    @Override
    public DrawingType getType() {
        return DrawingType.GROUP;
    }

    @Override
    public SaxContextHandler getContextHandler(SaxContextHandler parentHandler) {
        return new ShapeGroupHandler(parentHandler, this);
    }

    @Override
    public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle)
        throws JSONException {

        super.applyAttrsFromJSON(operationDocument, attrs, contentAutoStyle);
    }

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
        super.createAttrs(operationDocument, attrs, contentAutoStyle);

        final OpAttrs drawingAttrs = attrs.getMap(OCKey.DRAWING.value(), true);
        drawingAttrs.put(OCKey.CHILD_WIDTH.value(), transformer.getWidth());
        drawingAttrs.put(OCKey.CHILD_HEIGHT.value(), transformer.getHeight());
        drawingAttrs.put(OCKey.CHILD_LEFT.value(), transformer.getX());
        drawingAttrs.put(OCKey.CHILD_TOP.value(), transformer.getY());
    }

    public void convertConnectorToPath(OdfOperationDoc operationDocument, boolean contentAutoStyle)
        throws JSONException {

        convertConnectorToPath(getContent(), operationDocument, contentAutoStyle);
    }

    private void convertConnectorToPath(DLList<Object> c, OdfOperationDoc operationDocument, boolean contentAutoStyle)
        throws JSONException {

        final Iterator<DLNode<Object>> childNodeIter = c.getNodeIterator();
        while(childNodeIter.hasNext()) {
            final DLNode<Object> childNode = childNodeIter.next();
            final Object child = childNode.getData();
            if(child instanceof GroupShape) {
                ((GroupShape)child).convertConnectorToPath(((GroupShape)child).getContent(), operationDocument, contentAutoStyle);
            }
            else if(child instanceof ConnectorShape) {
                final Path path = ((ConnectorShape)child).convertConnectorToPath(operationDocument, contentAutoStyle);
                childNode.setData(path);
            }
        }
    }

    // this method has to be called with the top level group ...
    public void initializeTopLevelGroupShape(StyleManager styleManager) {
        // the groupshape will get its attributes (rotation and flipH + flipV) from the first normal child shape
        final Shape firstChild = getFirstChild();
        if(firstChild!=null) {
            firstChild.initializeShape(styleManager);
            final Transformer firstChildTransformer = firstChild.getTransformer();
            transformer.setRect(getChildRect());
            transformer.setFlipH(firstChildTransformer.getFlipH());
            transformer.setFlipV(firstChildTransformer.getFlipV());
            transformer.setRotation(firstChildTransformer.getRotation());
            applyGroupChildAttributes(styleManager, transformer);
            transformer.setInitilized(true);
            if(transformer.getRotation()!=null||transformer.getFlipH()!=null||transformer.getFlipV()!=null) {
                // with the current group child rect we only have the proper center
                applyGroupRotationAndFlipping(transformer.getRotation()!=null ? transformer.getRotation() : 0.0d, BooleanUtils.toBoolean(transformer.getFlipH()), BooleanUtils.toBoolean(transformer.getFlipV()), transformer.getCenterX(), transformer.getCenterY());
            }
        }
    }

    private void applyGroupChildAttributes(StyleManager styleManager, Transformer topLevelGroupTransformer) {
        final DLList<Object> groupContent = getContent();
        for(Object o:groupContent) {
            if(o instanceof GroupShape) {
                ((GroupShape)o).applyGroupChildAttributes(styleManager, topLevelGroupTransformer);
            }
            else if(o instanceof Shape) {
                ((Shape)o).initializeShape(styleManager);
            }
        }
        transformer.setRect(getChildRect());
        transformer.setInitilized(true);
    }

    private void applyGroupRotationAndFlipping(double topLevelGroupRotation, boolean flipH, boolean flipV, double topLevelGroupCenterX, double topLevelGroupCenterY) {
        Rectangle2D rect2D = new Rectangle2D.Double();
        for(Object o:getContent()) {
            if(o instanceof Shape) {
                if(o instanceof GroupShape) {
                    ((GroupShape)o).applyGroupRotationAndFlipping(topLevelGroupRotation, flipH, flipV, topLevelGroupCenterX, topLevelGroupCenterY);
                }
                else {

                    final Transformer childTransformer = ((Shape)o).getTransformer();
                    childTransformer.setRotation(childTransformer.getRotation()!=null?childTransformer.getRotation() - topLevelGroupRotation : -topLevelGroupRotation);
                    final double[] pts = Tools.rotatePoint(childTransformer.getCenterX(), childTransformer.getCenterY(), topLevelGroupCenterX, topLevelGroupCenterY, -topLevelGroupRotation);
                    childTransformer.setX(pts[0] - childTransformer.getWidth() * 0.5d);
                    childTransformer.setY(pts[1] - childTransformer.getHeight() * 0.5d);
                    if(flipH) {
                        childTransformer.setX(((topLevelGroupCenterX-childTransformer.getCenterX())+topLevelGroupCenterX)-childTransformer.getWidth() * 0.5d);
                        childTransformer.setFlipH(Boolean.logicalXor(flipH, BooleanUtils.toBoolean(childTransformer.getFlipH())));
                        childTransformer.setRotation(childTransformer.getRotation()!=null ? 360.0d - childTransformer.getRotation() : null);
                    }
                    if(flipV) {
                        childTransformer.setY(((topLevelGroupCenterY-childTransformer.getCenterY())+topLevelGroupCenterY)-childTransformer.getHeight() * 0.5d);
                        childTransformer.setFlipV(Boolean.logicalXor(flipV, BooleanUtils.toBoolean(childTransformer.getFlipV())));
                        childTransformer.setRotation(childTransformer.getRotation()!=null ? 180.0d - childTransformer.getRotation() : 180.0d);
                    }
                }
                final Rectangle2D childRect = ((Shape)o).getChildRect();
                if(rect2D.isEmpty()) {
                    rect2D = childRect;
                }
                else {
                    rect2D = rect2D.createUnion(childRect);
                }
            }
        }
        double maxXDiff = Math.max(topLevelGroupCenterX - rect2D.getX(), rect2D.getMaxX() - topLevelGroupCenterX);
        double maxYDiff = Math.max(topLevelGroupCenterY - rect2D.getY(), rect2D.getMaxY() - topLevelGroupCenterY);
        rect2D = rect2D.createUnion(new Rectangle2D.Double(topLevelGroupCenterX - maxXDiff, topLevelGroupCenterY - maxYDiff, maxXDiff * 2, maxYDiff * 2));

        transformer.setX(rect2D.getX());
        transformer.setY(rect2D.getY());
        transformer.setWidth(rect2D.getWidth());
        transformer.setHeight(rect2D.getHeight());
    }

    private Shape getFirstChild() {
        final DLList<Object> groupContent = getContent();
        if(groupContent.isEmpty()) {
            return null;
        }
        final Object firstGroupChild = groupContent.get(0);
        if(firstGroupChild instanceof GroupShape) {
            return ((GroupShape)firstGroupChild).getFirstChild();
        }
        else if(firstGroupChild instanceof Shape) {
            return (Shape)firstGroupChild;
        }
        return null;
    }

    public void prepareSaveTopLevelGroupShape(StyleManager styleManager) {
        if(transformer.isInitialized()) {
            prepareSaveTransformGroupShapeChilds(styleManager, transformer, getChildRect());
            transformer.setRotation(null);
            transformer.setFlipH(null);
            transformer.setFlipV(null);
            transformer.setInitilized(false);
        }
    }

    private void prepareSaveTransformGroupShapeChilds(StyleManager styleManager, Transformer topLevelGroupTransformer, Rectangle2D childRect) {
        for(Object child:getContent()) {
            if(child instanceof Shape) {
                final Transformer childTransformer = ((Shape)child).getTransformer();
                if(childTransformer.isInitialized()) {
                    Double scaleX = null;
                    if(childRect.getWidth()!=topLevelGroupTransformer.getWidth()) {
                        scaleX = topLevelGroupTransformer.getWidth() / childRect.getWidth();
                        childTransformer.setWidth(childTransformer.getWidth() * scaleX);
                        childTransformer.setX(((childTransformer.getX()-childRect.getX()) * scaleX) + childRect.getX());
                    }
                    Double scaleY = null;
                    if(childRect.getHeight()!=topLevelGroupTransformer.getHeight()) {
                        scaleY = topLevelGroupTransformer.getHeight() / childRect.getHeight();
                        childTransformer.setHeight(childTransformer.getHeight() * scaleY);
                        childTransformer.setY(((childTransformer.getY()-childRect.getY()) * scaleY) + childRect.getY());
                    }
                    Double translateX = null;
                    if(childRect.getX()!=topLevelGroupTransformer.getX()) {
                        translateX = topLevelGroupTransformer.getX() - childRect.getX();
                        childTransformer.setX(childTransformer.getX() + translateX);
                    }
                    Double translateY = null;
                    if(childRect.getY()!=topLevelGroupTransformer.getY()) {
                        translateY = topLevelGroupTransformer.getY() - childRect.getY();
                        childTransformer.setY(childTransformer.getY() + translateY);
                    }
                    if(child instanceof GroupShape) {
                        ((GroupShape)child).prepareSaveTransformGroupShapeChilds(styleManager, topLevelGroupTransformer, childRect);
                    }
                    else {
                        Double rotation = topLevelGroupTransformer.getRotation();
                        if(rotation!=null) {
                            ((Shape)child).rotate(rotation, topLevelGroupTransformer.getCenterX(), topLevelGroupTransformer.getCenterY());
                            rotation = null;
                        }
                        final Boolean flipH = topLevelGroupTransformer.getFlipH();
                        final Boolean flipV = topLevelGroupTransformer.getFlipV();
                        if(flipH!=null&&flipH.booleanValue()) {
                            childTransformer.setX((topLevelGroupTransformer.getCenterX()-childTransformer.getCenterX())+topLevelGroupTransformer.getCenterX()-childTransformer.getWidth() * 0.5d);
                            childTransformer.setFlipH(childTransformer.getFlipH()!=null?!childTransformer.getFlipH().booleanValue():Boolean.TRUE);
                        }
                        if(flipV!=null&&flipV.booleanValue()) {
                            childTransformer.setY((topLevelGroupTransformer.getCenterY()-childTransformer.getCenterY())+topLevelGroupTransformer.getCenterY()-childTransformer.getHeight() * 0.5d);
                            childTransformer.setFlipV(childTransformer.getFlipV()!=null?!childTransformer.getFlipV().booleanValue():Boolean.TRUE);
                        }
                        ((Shape)child).prepareSaveShape(styleManager);
                    }
                    childTransformer.setInitilized(false);
                }
            }
        }
    }
}
