/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import com.google.common.collect.ImmutableMap;
import com.openexchange.office.filter.core.EnhancedGeometryType;
import com.openexchange.office.filter.core.STShapeType;
import com.openexchange.office.filter.odf.ElementToBase64Handler;
import com.openexchange.office.filter.odf.OpAttrs;

public class EnhancedGeometry implements IElementWriter {

    private final CustomShape customShape;
    private boolean modified;
    private STShapeType presetShape;        // the shape we use for rendering (its an ooxml shape)
    private String shapeType;               // the odf shape type that is reflected in shapeData and modifiers, this shape type is used for the round trip
    private String b64ShapeData;
    private TreeMap<String, Number> modifiers;
    private List<OpAttrs> pathList;
    private MT mt;

    public EnhancedGeometry(CustomShape customShape) {
        this.customShape = customShape;
    }

    public STShapeType getPresetShape() {
        return presetShape;
    }

    public void setPresetShape(STShapeType presetShape) {
        this.presetShape = presetShape;
    }

    public String getShapeType() {
        return shapeType;
    }

    public void setShapeType(String shapeType) {
        this.shapeType = shapeType;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    // take care not to change values of this treeMap without setting the
    // modified flag, otherwise a changed treeMap might not be saved
    public TreeMap<String, Number> getModifiers(boolean forceCreate) {
        if(modifiers==null&&forceCreate) {
            modifiers = new TreeMap<String, Number>();
        }
        return modifiers;
    }

    public String getShapeData() {
        return b64ShapeData;
    }

    public void setShapeData(String b64ShapeData) {
        this.b64ShapeData = b64ShapeData;
    }

    public void setPathList(List<OpAttrs> pathList) {
        this.pathList = pathList;
    }

    public List<OpAttrs> getPathList() {
        return pathList;
    }

    public MT getModifierTransformer() {
        return mt;
    }

    public void setModifierTransformer(MT mt) {
        this.mt = mt;
    }

    public boolean isConnector() {
        return presetShape!=null&&(presetShape==STShapeType.STRAIGHT_CONNECTOR_1
         ||presetShape==STShapeType.BENT_CONNECTOR_2
         ||presetShape==STShapeType.BENT_CONNECTOR_3
         ||presetShape==STShapeType.BENT_CONNECTOR_4
         ||presetShape==STShapeType.BENT_CONNECTOR_5
         ||presetShape==STShapeType.CURVED_CONNECTOR_2
         ||presetShape==STShapeType.CURVED_CONNECTOR_3
         ||presetShape==STShapeType.CURVED_CONNECTOR_4
         ||presetShape==STShapeType.CURVED_CONNECTOR_5);
    }

    @Override
    public void writeObject(SerializationHandler output) {
        try {
            output.flushPending();
        } catch (SAXException e) {
            //
        }
        boolean newHostData = false;
        if(b64ShapeData==null) {
            if(presetShape!=null) {
                final String ooShapeType = EnhancedGeometryType.OOShapeTypes.inverse().get(presetShape);
                if(ooShapeType!=null) {
                    final EnhancedGeometryType enhancedGeometryType = EnhancedGeometryType.fromValue(ooShapeType);
                    if(enhancedGeometryType!=null) {
                        b64ShapeData = enhancedGeometryType.getShapeGeometry();
                        newHostData = true;
                    }
                }
            }
            if(b64ShapeData==null) {
                return;
            }
        }
        if(modified) {
            // this document part is loaded without "namespaceAware", so Namespaces are not
            // supported, therefore the qualified name has to be used for the localName
            final Document doc = ElementToBase64Handler.getDOMFromBase64Data(b64ShapeData);
            if(doc!=null) {
                final Element enhancedGeometry = doc.getDocumentElement();
                final Boolean flipH = customShape.getTransformer().getFlipH();
                if(flipH==null) {
                    enhancedGeometry.removeAttribute("draw:mirror-horizontal");
                }
                else {
                    enhancedGeometry.setAttribute("draw:mirror-horizontal", flipH.toString());
                }
                final Boolean flipV = customShape.getTransformer().getFlipV();
                if(flipV==null) {
                    enhancedGeometry.removeAttribute("draw:mirror-vertical");
                }
                else {
                    enhancedGeometry.setAttribute("draw:mirror-vertical", flipV.toString());
                }
                if(modifiers==null||modifiers.isEmpty()) {
                    if(!newHostData) {
                        enhancedGeometry.removeAttribute("draw:modifiers");
                    }
                }
                else {
                    if(mt==null) {
                        final String customShapeType = enhancedGeometry.getAttribute("draw:type");
                        mt = EnhancedGeometry.OOShapeToOOXModifierTransformation.get("mso-spt100".equals(customShapeType) ? presetShape : customShapeType);
                    }
                    if(mt!=null) {
                        enhancedGeometry.setAttribute("draw:modifiers", mt.transformOOXMLToODF(modifiers));
                    }
                    else {
                        final StringBuffer modifiersBuf = new StringBuffer();
                        final Iterator<Number> avListIter = modifiers.values().iterator();
                        while(avListIter.hasNext()) {
                            modifiersBuf.append(Double.valueOf(avListIter.next().doubleValue()).toString());
                            if(avListIter.hasNext()) {
                                modifiersBuf.append(' ');
                            }
                        }
                        enhancedGeometry.setAttribute("draw:modifiers", modifiersBuf.toString());
                    }
                }
                try {
                    output.asDOMSerializer().serialize(doc);
                } catch (IOException e) {
                    //
                }
            }
        }
        else {
            // shape is not modified, we can write shape data directly
            ElementToBase64Handler.writeString(output, b64ShapeData);
        }
    }

    protected abstract static class MT {

        public void transform(String[] tokens, TreeMap<String, Number> modifier) {
            int i = 0;
            for(String token:tokens) {
                Double number = 0.0;
                try {
                    number = Double.parseDouble(token);
                }
                catch(NumberFormatException e) {
                    // ohoh
                }
                putVal(modifier, i, getVal(i, number));
                i++;
            }
        }

        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            final Iterator<Entry<String, Number>> avListIter = modifiers.entrySet().iterator();
            int i = 0;
            while(avListIter.hasNext()) {
                final Entry<String, Number> entry = avListIter.next();
                modifiersBuf.append(getODFVal(i++, entry.getValue().doubleValue()));
                if(avListIter.hasNext()) {
                    modifiersBuf.append(' ');
                }
            }
            return modifiersBuf.toString();
        }

        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj" + Integer.valueOf(index+1).toString(), val);
        }

        public abstract String getODFVal(int index, double val);

        public abstract long getVal(int index, double number);
    }

    protected static class MT21600 extends MT {

        private final boolean centered;

        public MT21600(boolean centered) {
            this.centered = centered;
        }

        @Override
        public long getVal(int index, double number) {
            long val = Double.valueOf((number * 100000d) / 21600d).longValue();
            if(centered) {
                val -= 50000;
            }
            return val;
        }

        @Override
        public String getODFVal(int index, double val) {
            long v = Double.valueOf(((val * 21600d) / 100000d)).longValue();
            if(centered) {
                v += 10800;
            }
            return Long.valueOf(v).toString();
        }
    }

    protected static class MT10800Adj extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((number * 50000d) / 10800d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj", val);
        }

        @Override
        public String getODFVal(int index, double val) {
            return Long.valueOf(Double.valueOf((val * 10800) / 50000d).longValue()).toString();
        }
    }

    protected static class MT10800Adj1 extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((number * 50000d) / 10800d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj1", val);
        }

        @Override
        public String getODFVal(int index, double val) {
            return Long.valueOf(Double.valueOf((val * 10800d) / 50000d).longValue()).toString();
        }
    }

    protected static class MT21600Adj extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((number * 100000d) / 21600d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj", val);
        }

        @Override
        public String getODFVal( int index, double val) {
            return Long.valueOf(Double.valueOf((val * 21600d) / 100000d).longValue()).toString();
        }
    }

    protected static class MTBlockArc extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==1) {
                return Double.valueOf((number * 25000) / 10800d).longValue();
            }
            long val = Double.valueOf(number * 60000).longValue();
            if(val < 0) {
                val += 21600000;
            }
            return val;
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==1) {
                modifier.put("adj3", val);
            }
            else {
                modifier.put("adj1", val);
                val = 10800000 - val;
                if(val < 0) {
                    val += 21600000;
                }
                modifier.put("adj2", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj1").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj3").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(2, modifiers.get("adj2").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            if(index==1) {
                return Long.valueOf(Double.valueOf((val * 10800d) / 25000d).longValue()).toString();
            }
            else if(index==0) {
                return Long.valueOf(Double.valueOf(val / 60000d).longValue()).toString();
            }
            val = 10800000 - val;
            if(val < 0) {
                val += 21600000;
            }
            return Long.valueOf(Double.valueOf(val / 60000d).longValue()).toString();
        }
    }

    protected static class MTPie extends MT {

        @Override
        public long getVal(int index, double number) {
            long val = Double.valueOf(number * 60000).longValue();
            if(val<0) {
                val += 21600000;
            }
            return val;
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==1) {
                modifier.put("adj1", val);
            }
            else {
                modifier.put("adj2", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj2").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj1").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            return Long.valueOf(Double.valueOf(val / 60000d).longValue()).toString();
        }
    }

    protected static class MTFolderAdj extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf(((21600d - number) * 50000d) / 10800d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj", val);
        }

        @Override
        public String getODFVal(int index, double val) {
            return Double.valueOf(Double.valueOf(21600d - ((val * 10800d) / 50000d)).longValue()).toString();
        }
    }

    protected static class MTSmileyAdj extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((((number - 14510) * 9306d) / 4010d) - 4653d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj", val);
        }

        @Override
        public String getODFVal(int index, double val) {
            return Double.valueOf(Double.valueOf((((val + 4553d) * 4010d) / 9306d) + 14510d).longValue()).toString();
        }
    }

    protected static class MTSunAdj extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((((number - 2700) / 7425d) * 34375d) + 12500d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj", val);
        }

        @Override
        public String getODFVal(int index, double val) {
            return Double.valueOf(Double.valueOf((((val - 12500d) / 34375d) * 7425d) + 2700d).longValue()).toString();
        }
    }

    protected static class MTMoonAdj extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((number / 18900d) * 87500d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj", val);
        }

        @Override
        public String getODFVal(int index, double val) {
            return Double.valueOf(Double.valueOf((val / 87500d) * 18900d).longValue()).toString();
        }
    }

    protected static class MTForbiddenAdj extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((number / 7200d) * 50000d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj", val);
        }

        @Override
        public String getODFVal(int index, double val) {
            return Double.valueOf(Double.valueOf((val / 50000d) * 7200d).longValue()).toString();
        }
    }

    protected static class MTBrace extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==1) {
                return Double.valueOf((number * 100000d) / 21600d).longValue();
            }
            return 8333;
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==1) {
                modifier.put("adj2", val);
            }
            else {
                modifier.put("adj1", val);
            }
        }

        @Override
        public String getODFVal(int index, double val) {
            if(index==1) {
                return Long.valueOf(Double.valueOf((val * 21600d) / 100000d).longValue()).toString();
            }
            return "1800";
        }
    }

    protected static class MTLeftArrow extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==1) {
                return Double.valueOf(((10800 - number) / 10800d) * 100000d).longValue();
            }
            return Double.valueOf(100000 - (((21600 - number) / 21600d) * 100000d)).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==1) {
                modifier.put("adj1", val);
            }
            else {
                modifier.put("adj2", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj2").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj1").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            if(index==1) {
                return Long.valueOf(Double.valueOf(((100000 - val) * 10800d) / 100000d).longValue()).toString();
            }
            return Long.valueOf(Double.valueOf(21600 - (((100000 - val) * 21600d) / 100000d)).longValue()).toString();
        }
    }

    protected static class MTRightArrow extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==1) {
                return Double.valueOf(((10800 - number) / 10800d) * 100000d).longValue();
            }
            return Double.valueOf(((21600 - number) / 21600d) * 100000d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==1) {
                modifier.put("adj1", val);
            }
            else {
                modifier.put("adj2", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj2").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj1").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            if(index==1) {
                return Long.valueOf(Double.valueOf(((100000 - val) * 10800d) / 100000d).longValue()).toString();
            }
            return Long.valueOf(Double.valueOf(((100000 - val) * 21600d) / 100000d).longValue()).toString();
        }
    }

    protected static class MTLeftRightArrow extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==0) {
                return Double.valueOf((number / 10800d) * 100000d).longValue();
            }
            return Double.valueOf(((10800 - number) / 10800d) * 100000d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==0) {
                modifier.put("adj2", val);
            }
            else {
                modifier.put("adj1", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj2").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj1").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            if(index==0) {
                long v = Double.valueOf((val * 10800d) / 100000d).longValue();
                if(v>10800) {
                    v=10800;
                }
                return Long.valueOf(v).toString();
            }
            return Long.valueOf(Double.valueOf(((100000 - val) * 10800d) / 100000d).longValue()).toString();
        }
    }

    protected static class MTUpDownArrow extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==1) {
                return Double.valueOf((number / 10800d) * 100000d).longValue();
            }
            return Double.valueOf(((10800 - number) / 10800d) * 100000d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==1) {
                modifier.put("adj2", val);
            }
            else {
                modifier.put("adj1", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj1").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj2").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            if(index==1) {
                long v = Double.valueOf((val * 10800d) / 100000d).longValue();
                if(v>10800) {
                    v=10800;
                }
                return Long.valueOf(v).toString();
            }
            return Long.valueOf(Double.valueOf(((100000 - val) * 10800d) / 100000d).longValue()).toString();
        }
    }

    protected static class MTQuadArrow extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==2) {
                return Double.valueOf((number / 6500d) * 27500d).longValue();
            }
            return 22500;
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==0) {
                modifier.put("adj1", val);
            }
            else if (index==1) {
                modifier.put("adj2", val);
            }
            else {
                modifier.put("adj3", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append("6500 8600");
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(2, modifiers.get("adj3").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            if(index==2) {
                return Long.valueOf(Double.valueOf((val * 6500d) / 27500d).longValue()).toString();
            }
            return "0";
        }
    }

    protected static class MTStripedRightArrow extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==1) {
                return Double.valueOf(((10800 - number) / 10800d) * 100000d).longValue();
            }
            return 50000;
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==0) {
                modifier.put("adj2", val);
            }
            else if (index==1) {
                modifier.put("adj1", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append("13200");
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(0, modifiers.get("adj1").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append('0');
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            return Long.valueOf(Double.valueOf(((100000 - val) * 10800d) / 100000d).longValue()).toString();
        }
    }

    protected static class MTNotchedRightArrow extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==1) {
                return Double.valueOf(((10800 - number) / 10800d) * 100000d).longValue();
            }
            return 50000;
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==0) {
                modifier.put("adj2", val);
            }
            else if (index==1) {
                modifier.put("adj1", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj2").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj1").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            if(index==2) {
                return Long.valueOf(Double.valueOf(((100000 - val) * 10800d) / 100000d).longValue()).toString();
            }
            return "16200";
        }
    }

    protected static class MTCircularArrow extends MT {

        @Override
        public long getVal(int index, double number) {
            if(index==1||index==0) {
                long val = Double.valueOf(number * 60000).longValue();
                if(val < 0) {
                    val += 21600000;
                }
                return val;
            }
            return 0;
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==1) {
                modifier.put("adj3", val);
            }
            else if(index==0) {
                modifier.put("adj4", val);
            }
            else {
                modifier.put("adj1", 12500);
                modifier.put("adj2", 1142319);
                modifier.put("adj5", 12500);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj4").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj3").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append("5500");
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            if(index==0||index==1) {
                return Long.valueOf(Double.valueOf(val / 60000d).longValue()).toString();
            }
            return "0";
        }
    }

    protected static class MTStar4 extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf(((10800 - number) / 10800d) * 50000d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj", val);
        }

        @Override
        public String getODFVal(int index, double val) {
            return Long.valueOf(Double.valueOf(((10800 - val) * 10800d) / 50000d).longValue()).toString();
        }
    }

    protected static class MTVerticalScroll extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((number / 5400d) * 25000d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            modifier.put("adj", val);
        }

        @Override
        public String getODFVal(int index, double val) {
            return Long.valueOf(Double.valueOf((val * 5400d) / 25000d).longValue()).toString();
        }
    }

    protected static class MTLineCallout extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((number / 21600d) * 100000d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==0) {
                modifier.put("adj4", val);
            }
            else if(index==1) {
                modifier.put("adj3", val);
            }
            else if(index==2) {
                modifier.put("adj2", val);
            }
            else if(index==3) {
                modifier.put("adj1", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj4").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj3").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(2, modifiers.get("adj2").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(3, modifiers.get("adj1").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            return Long.valueOf(Double.valueOf((val * 21600d) / 100000d).longValue()).toString();
        }
    }

    protected static class MTLineCallout2 extends MT {

        @Override
        public long getVal(int index, double number) {
            return Double.valueOf((number / 21600d) * 100000d).longValue();
        }

        @Override
        public void putVal(TreeMap<String, Number> modifier, int index, long val) {
            if(index==0) {
                modifier.put("adj6", val);
            }
            else if(index==1) {
                modifier.put("adj5", val);
            }
            else if(index==2) {
                modifier.put("adj4", val);
            }
            else if(index==3) {
                modifier.put("adj3", val);
            }
            else if(index==4) {
                modifier.put("adj2", val);
            }
            else if(index==5) {
                modifier.put("adj1", val);
            }
        }

        @Override
        public String transformOOXMLToODF(TreeMap<String, Number> modifiers) {
            final StringBuffer modifiersBuf = new StringBuffer();
            modifiersBuf.append(getODFVal(0, modifiers.get("adj6").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(1, modifiers.get("adj5").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(2, modifiers.get("adj4").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(3, modifiers.get("adj3").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(4, modifiers.get("adj2").doubleValue()));
            modifiersBuf.append(' ');
            modifiersBuf.append(getODFVal(5, modifiers.get("adj1").doubleValue()));
            return modifiersBuf.toString();
        }

        @Override
        public String getODFVal(int index, double val) {
            return Long.valueOf(Double.valueOf((val * 21600d) / 100000d).longValue()).toString();
        }
    }

    static final public ImmutableMap<String, MT> OOShapeToOOXModifierTransformation = ImmutableMap.<String, MT> builder()
        .put("round-rectangle", new MT10800Adj())
        .put("rectangular-callout", new MT21600(true))
        .put("round-rectangular-callout", new MT21600(true))
        .put("round-callout", new MT21600(true))
        .put("cloud-callout", new MT21600(true))
        .put("isosceles-triangle", new MT21600(false))
        .put("parallelogram", new MT21600(false))
        .put("hexagon", new MT10800Adj())
        .put("octagon", new MT10800Adj())
        .put("cross", new MT10800Adj())
        .put("ring", new MT10800Adj())
        .put("block-arc", new MTBlockArc())
        .put("pie", new MTPie())
        .put("can", new MT10800Adj())
        .put("cube", new MT21600Adj())
        .put("paper", new MTFolderAdj())
        .put("frame", new MT10800Adj1())
        .put("smiley", new MTSmileyAdj())
        .put("sun", new MTSunAdj())
        .put("moon", new MTMoonAdj())
        .put("forbidden", new MTForbiddenAdj())
        .put("bracket-pair", new MT10800Adj())
        .put("brace-pair", new MT10800Adj())
        .put("left-brace", new MTBrace())
        .put("right-brace", new MTBrace())
        .put("quad-bevel", new MT10800Adj())
        .put("left-arrow", new MTLeftArrow())
        .put("right-arrow", new MTRightArrow())
        .put("up-arrow", new MTLeftArrow())
        .put("down-arrow", new MTRightArrow())
        .put("left-right-arrow", new MTLeftRightArrow())
        .put("up-down-arrow", new MTUpDownArrow())
        .put("quad-arrow", new MTQuadArrow())
        .put("stripedRightArrow", new MTStripedRightArrow())
        .put("notched-right-arrow", new MTNotchedRightArrow())
        .put("circular-arrow", new MTCircularArrow())
        .put("star4", new MTStar4())
        .put("star8", new MTStar4())
        .put("star24", new MTStar4())
        .put("vertical-scroll", new MTVerticalScroll())
        .put("horizontal-scroll", new MTVerticalScroll())
        .put("mso-spt21", new MT10800Adj())
        .put("line-callout-1", new MTLineCallout())
        .put("line-callout-2", new MTLineCallout2())
        .put("line-callout-3", new MTLineCallout())
        .build();
}
