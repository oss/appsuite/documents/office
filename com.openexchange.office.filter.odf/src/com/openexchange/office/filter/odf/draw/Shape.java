/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.awt.geom.Rectangle2D;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;
import com.openexchange.office.filter.odf.properties.GraphicProperties;
import com.openexchange.office.filter.odf.styles.IGraphicProperties;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleGraphic;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class Shape implements IDrawing, INodeAccessor {

	protected final String uri;
	protected final String localName;
	protected final String qName;
	protected final GroupShape parentGroup;
    protected final boolean rootShape;
    protected final boolean contentStyle;
	protected final AttributesImpl attributes;
	protected final Transformer transformer;
	protected DLList<Object> childs = new DLList<Object>();

	// new shape via sax parser ..
	public Shape(AttributesImpl attributes, String uri, String localName, String qName, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
		this.attributes = attributes;
		this.transformer = new Transformer(this.attributes, isLineMode());
		this.uri = uri;
		this.localName = localName;
		this.qName = qName;
		this.parentGroup = parentGroup;
		this.rootShape = rootShape;
		this.contentStyle = contentStyle;
	}

	// new shape via ... insertDrawing operation
	public Shape(OdfOperationDoc operationDoc, String uri, String localName, String qName, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
		this.attributes = new AttributesImpl();
        this.transformer = new Transformer(isLineMode());
		this.uri = uri;
		this.localName = localName;
		this.qName = qName;
		this.parentGroup = parentGroup;
		this.rootShape = rootShape;
		this.contentStyle = contentStyle;

		// saving newly inserted shapes.
		operationDoc.getDocument().getModifiedShapes().add(this);

		if(rootShape&&operationDoc.getDocument().getDocumentType().equals("text")) {
		    this.attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "as-char");
		}
	}

    public Transformer getTransformer() {
	    return transformer;
	}

    /*
     *
     * if this shape is child a groupShape then the Transformer of the top most group shape is returned.
     * if this shape is not a child then null is returned
     *
     */
    public Transformer getTopLevelGroupTransformer() {
        if(parentGroup==null) {
            return null;
        }
        Shape parent = parentGroup;
        while(parent.parentGroup!=null) {
            parent = parent.parentGroup;
        }
        return parent.getTransformer();
    }

	public boolean isLineMode() {
	    return false;
	}

	@Override
	public DLList<Object> getContent() {
		return childs;
	}

	@Override
    public DrawingType getType() {
		return DrawingType.SHAPE;
	}

	@Override
    public AttributesImpl getAttributes() {
		return attributes;
	}

	@Override
	public boolean preferRepresentation() {
	    return false;
	}

	public String getStyleName() {
		return attributes.getValue("draw:style-name");
	}

	public void setStyleName(String style) {
		attributes.setValue(Namespaces.DRAW, "style-name", "draw:style-name", style);
	}

    public boolean isContentStyle() {
        return contentStyle;
    }

	public String getId() {
	    return attributes.getValue("draw:id");
	}

	public void setId(String id) {
	    if(id==null||id.isEmpty()) {
	        attributes.remove("draw:id");
	        attributes.remove("xml:id");
	    }
	    else {
	        attributes.setValue(Namespaces.DRAW, "id", "draw:id", id);
	        attributes.setValue(Namespaces.XML, "id", "xml:id", id);
	    }
	}

	public void rotate(double rotation, Double refX, Double refY) {
        final Double currentRotation = transformer.getRotation();
        boolean fH = transformer.getFlipH()!=null ? transformer.getFlipH().booleanValue() : false;
        boolean fV = transformer.getFlipV()!=null ? transformer.getFlipV().booleanValue() : false;
        if(Boolean.logicalXor(fH, fV)) {
            transformer.setRotation(currentRotation==null ? -rotation : currentRotation - rotation);
            if(refX!=null&&refY!=null) {
                final Rectangle2D rect2D = transformer.getRect2D();
                final double[] pts = Tools.rotatePoint(rect2D.getCenterX(), rect2D.getCenterY(), refX, refY, -rotation);
                transformer.setX(pts[0]-rect2D.getWidth()/2);
                transformer.setY(pts[1]-rect2D.getHeight()/2);
            }
        }
        else {
            transformer.setRotation(currentRotation==null ? rotation : currentRotation + rotation);
            if(refX!=null&&refY!=null) {
                final Rectangle2D rect2D = transformer.getRect2D();
                final double[] pts = Tools.rotatePoint(rect2D.getCenterX(), rect2D.getCenterY(), refX, refY, rotation);
                transformer.setX(pts[0]-rect2D.getWidth()/2);
                transformer.setY(pts[1]-rect2D.getHeight()/2);
            }
        }
	}

	public Rectangle2D getChildRect() {
        return transformer.getRect2D();
    }

    public GroupShape getParentGroup() {
        return parentGroup;
    }

    @Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		output.startElement(uri, localName, qName);
		attributes.write(output);
		TextContentHelper.write(output, getContent());
		output.endElement(uri, localName, qName);
	}

    public SaxContextHandler getContextHandler(SaxContextHandler parentHandler) {
        if(getType()==DrawingType.SHAPE||getType()==DrawingType.CONNECTOR) {
            return new ShapeHandler(parentHandler, this);
        }
        return new ShapeUnknownChildsHandler(parentHandler, this);
    }

	@Override
	public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle)
		throws JSONException {

        operationDocument.getDocument().getModifiedShapes().add(this);

        if(!transformer.isInitialized()) {
            Transformer.initialize(operationDocument, this);
        }
        final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
	    final StyleBase styleBase = styleManager.getStyleBaseClone(attributes.containsKey("presentation:style-name") ? StyleFamily.PRESENTATION: StyleFamily.GRAPHIC, getStyleName(), contentAutoStyle);
	    styleManager.applyAttrs(styleBase, contentAutoStyle, attrs);
	    if(this instanceof ConnectorShape) {
	        final JSONObject _attrs = new JSONObject(1);
	        final JSONObject _fillAttrs = new JSONObject(1);
	        _attrs.put(OCKey.FILL.value(), _fillAttrs);
	        _fillAttrs.put(OCKey.TYPE.value(), "none");
	        styleBase.applyAttrs(styleManager, _attrs);
	    }
	    // apply correct default values
	    final String docType = operationDocument.getDocument().getDocumentType();
	    if(docType.equals("presentation")||docType.equals("spreadsheet")) {
	        if(getStyleName()==null) {
	            boolean defaultAutoResizeHeight = true;
	            boolean defaultWordWrap = true;

	            final JSONObject shapeAttrs = attrs.optJSONObject(OCKey.SHAPE.value());
	            if(shapeAttrs!=null) {
	                defaultAutoResizeHeight = !shapeAttrs.has(OCKey.AUTO_RESIZE_HEIGHT.value());
                    defaultWordWrap = !shapeAttrs.has(OCKey.WORD_WRAP.value());
	            }
	            if(defaultAutoResizeHeight||defaultWordWrap) {
                    final JSONObject _attrs = new JSONObject(1);
                    final JSONObject _shapeAttrs = new JSONObject(2);
                    _attrs.put(OCKey.SHAPE.value(), _shapeAttrs);
                    if(defaultAutoResizeHeight) {
                        _shapeAttrs.put(OCKey.AUTO_RESIZE_HEIGHT.value(), Boolean.FALSE);
                    }
                    if(defaultWordWrap) {
                        _shapeAttrs.put(OCKey.WORD_WRAP.value() , Boolean.TRUE);
                    }
                    styleBase.applyAttrs(styleManager, _attrs);
	            }
	        }
	    }
	    setStyleName(styleManager.getStyleIdForStyleBase(styleBase));

		final JSONObject drawingProps = attrs.optJSONObject(OCKey.DRAWING.value());
        if(drawingProps!=null) {
            if (drawingProps.has(OCKey.NAME.value())) {
                final String name = drawingProps.optString(OCKey.NAME.value());
                if (name!=null && !name.isEmpty()) {
                	attributes.setValue(Namespaces.DRAW, "name", "draw:name", name);
                }
            }
            final Object id = drawingProps.optString(OCKey.ID.value(), null);
            if(id!=null) {
                if(id==JSONObject.NULL) {
                    setId(null);
                }
                else {
                    setId((String)id);
                }
            }
            if(rootShape && operationDocument.getDocument() instanceof OdfTextDocument) {
                if (drawingProps.has(OCKey.ANCHOR_HOR_OFFSET.value())) {
                    transformer.setX(drawingProps.optInt(OCKey.ANCHOR_HOR_OFFSET.value()));
                }
                if (drawingProps.has(OCKey.ANCHOR_VERT_OFFSET.value())) {
                    transformer.setY(drawingProps.optInt(OCKey.ANCHOR_VERT_OFFSET.value()));
                }
                applyTextRootShapeProps(drawingProps);
            }
            else {
                if (drawingProps.has(OCKey.LEFT.value())) {
                    transformer.setX(drawingProps.optInt(OCKey.LEFT.value()));
                }
                if (drawingProps.has(OCKey.TOP.value())) {
                    transformer.setY(drawingProps.optInt(OCKey.TOP.value()));
                }
            }
            if (drawingProps.has(OCKey.WIDTH.value())) {
                transformer.setWidth(drawingProps.optInt(OCKey.WIDTH.value()));
            }
            if (drawingProps.has(OCKey.HEIGHT.value())) {
                transformer.setHeight(drawingProps.optInt(OCKey.HEIGHT.value()));
            }
            final Object rotation = drawingProps.opt(OCKey.ROTATION.value());
            if(rotation!=null) {
                transformer.setRotation(rotation instanceof Number ? ((Number)rotation).doubleValue() : null);
            }
            final Object flipH = drawingProps.opt(OCKey.FLIP_H.value());
            if(flipH!=null) {
                getTransformer().setFlipH(flipH instanceof Boolean ? (Boolean)flipH : null);
            }
            final Object flipV = drawingProps.opt(OCKey.FLIP_V.value());
            if(flipV!=null) {
                getTransformer().setFlipV(flipV instanceof Boolean ? (Boolean)flipV : null);
            }
        }
        final JSONObject shapeProps = attrs.optJSONObject(OCKey.SHAPE.value());
        if(shapeProps!=null) {
            if (shapeProps.has(OCKey.AUTO_RESIZE_HEIGHT.value()) && shapeProps.optBoolean(OCKey.AUTO_RESIZE_HEIGHT.value()) == true) {
            	attributes.remove("svg:height");
            }
        }
	}

	public void applyTextRootShapeProps(JSONObject drawingProps)
	    throws JSONException {

	    if (drawingProps.hasAndNotNull(OCKey.INLINE.value()) && drawingProps.optBoolean(OCKey.INLINE.value())) {
            attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "as-char");
        } else if (drawingProps.has(OCKey.ANCHOR_HOR_BASE.value()) && drawingProps.has(OCKey.ANCHOR_VERT_BASE.value())) {
            String anchorHorBase = drawingProps.optString(OCKey.ANCHOR_HOR_BASE.value());
            String anchorVertBase = drawingProps.optString(OCKey.ANCHOR_VERT_BASE.value());
            if (anchorHorBase != null && anchorVertBase != null) {
                if (anchorHorBase.equals("page") && anchorVertBase.equals("page")) {
                    attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "paragraph");
                    final int anchorPageNumber = drawingProps.optInt("anchorPageNumber", -1);
                    if(anchorPageNumber!=-1) {
                        attributes.setIntValue(Namespaces.TEXT, "anchor-page-number", "text:anchor-page-number", Integer.valueOf(anchorPageNumber));
                    }
                } else if (anchorHorBase.equals("column") && anchorVertBase.equals("margin")) {
                    attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "frame");

                } else if (anchorHorBase.equals("column") && anchorVertBase.equals("paragraph")) {
                    attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "paragraph");
                    //apply related default wrapping, if not part of the attributes:
                    if (!drawingProps.has(OCKey.TEXT_WRAP_MODE.value()) && !drawingProps.has(OCKey.TEXT_WRAP_SIDE.value())) {
                        drawingProps.put(OCKey.TEXT_WRAP_MODE.value(), "topAndBottom");
                    }
                } else if (anchorHorBase.equals("character") && anchorVertBase.equals("paragraph")) {
                    attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "char");
                } else { // the default is OCKey.INLINE.shortName() a
                    attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "as-char");
                }
            }
        } else {
            if (drawingProps.has(OCKey.ANCHOR_HOR_BASE.value())) {
                String anchorHorBase = drawingProps.optString(OCKey.ANCHOR_HOR_BASE.value());
                if (anchorHorBase != null) {
                    if (anchorHorBase.equals("page")) {
                        attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "page");
                        final int anchorPageNumber = drawingProps.optInt("anchorPageNumber", -1);
                        if(anchorPageNumber!=-1) {
                            attributes.setIntValue(Namespaces.TEXT, "anchor-page-number", "text:anchor-page-number", Integer.valueOf(anchorPageNumber));
                        }
                    } else if (anchorHorBase.equals("column")) {
                        attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "paragraph");

                    } else if (anchorHorBase.equals("character")) {
                        attributes.setValue(Namespaces.TEXT, "anchor-type", "text:anchor-type", "char");
                    }
                }
            }
        }
        if (drawingProps.has(OCKey.ANCHOR_LAYER_ORDER.value())) {
            int anchorLayerOrder = drawingProps.optInt(OCKey.ANCHOR_LAYER_ORDER.value(), 0);
            attributes.setIntValue(Namespaces.DRAW, "z-index", "draw:z-index", anchorLayerOrder);
        }
	}

	@Override
	public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
        if(!transformer.isInitialized()) {
            Transformer.initialize(operationDocument, this);
        }
	    final String docType = operationDocument.getDocument().getDocumentType();
	    if(getStyleName()!=null&&!getStyleName().isEmpty()) {
			operationDocument.getDocument().getStyleManager().
			    collectAllGraphicPropertiesFromStyle(attrs, getStyleName(), StyleFamily.GRAPHIC, contentAutoStyle, true);
		}
		final OpAttrs drawingAttrs = attrs.getMap(OCKey.DRAWING.value(), true);
		final boolean isTextRootShape = rootShape && docType.equals("text");
        final String id = getId();
        if(id!=null) {
            drawingAttrs.put(OCKey.ID.value(), id);
        }
		double x = transformer.getX();
		double y = transformer.getY();
		double width = transformer.getWidth();
		double height = transformer.getHeight();
		Double rotation = transformer.getRotation();
		boolean flipH = transformer.getFlipH()!=null ? transformer.getFlipH() : false;
		boolean flipV = transformer.getFlipV()!=null ? transformer.getFlipV() : false;

		final Transformer topLevelTransformer = getTopLevelGroupTransformer();
		if(topLevelTransformer!=null) {
		    final double groupCenterX = topLevelTransformer.getCenterX();
		    final double groupCenterY = topLevelTransformer.getCenterY();
		    final Double groupRotation = topLevelTransformer.getRotation();
/*
		    if(groupRotation!=null) {
		        boolean fH = transformer.getFlipH()!=null ? transformer.getFlipH().booleanValue() : false;
		        boolean fV = transformer.getFlipV()!=null ? transformer.getFlipV().booleanValue() : false;
		        if(Boolean.logicalXor(fH, fV)) {
		            rotation = rotation==null ? -groupRotation : rotation + groupRotation;
	                final double[] pts = Tools.rotatePoint(x + width * 0.5, y + height * 0.5, groupCenterX, groupCenterY, -groupRotation);
	                x = pts[0];
	                y = pts[1];
		        }
		        else {
		            rotation = rotation==null ? groupRotation : rotation - groupRotation;
	                final double[] pts = Tools.rotatePoint(x + width * 0.5, y + height * 0.5, groupCenterX, groupCenterY, -groupRotation);
                    x = pts[0];
                    y = pts[1];
		        }
		        x = x - width * 0.5;
	            y = y - height * 0.5;
		    }
*/
		}

		drawingAttrs.put(OCKey.WIDTH.value(), width);
        drawingAttrs.put(OCKey.HEIGHT.value(), height);
        drawingAttrs.put(isTextRootShape ? OCKey.ANCHOR_HOR_OFFSET.value() : OCKey.LEFT.value(), x);
        drawingAttrs.put(isTextRootShape ? OCKey.ANCHOR_VERT_OFFSET.value() : OCKey.TOP.value(), y);

        if(rotation!=null) {
            drawingAttrs.put(OCKey.ROTATION.value(), rotation);
        }
        if(flipH) {
            attrs.getMap(OCKey.DRAWING.value(), true).put(OCKey.FLIP_H.value(), flipH);
        }
        if(flipV) {
            attrs.getMap(OCKey.DRAWING.value(), true).put(OCKey.FLIP_V.value(), flipV);
        }
        if(isTextRootShape) {
            createTextRootShapeProps(drawingAttrs);
        }
        if(!drawingAttrs.containsKey(OCKey.HEIGHT.value())) {
            attrs.getMap(OCKey.SHAPE.value(), true).put(OCKey.AUTO_RESIZE_HEIGHT.value(), true);
        }
	}

	public void createTextRootShapeProps(OpAttrs drawingAttrs) {
	    final Integer zIndex = attributes.getIntValue("draw:z-index");
        if(zIndex!=null) {
            drawingAttrs.put(OCKey.ANCHOR_LAYER_ORDER.value(), zIndex);
        }
        final String anchorType = attributes.getValue("text:anchor-type");
        if(anchorType!=null) {
            String anchorVertBase = null;
            String anchorHorBase = null;
            if (anchorType.equals("page")) {
                // OX API: true: image as character, false: floating mode
                drawingAttrs.put(OCKey.INLINE.value(), Boolean.FALSE);
                //page anchor requires page relation
                drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "page");
                drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "page");
                final Integer anchorPageNumber = attributes.getIntValue("text:anchor-page-number");
                if(anchorPageNumber!=null) {
                    drawingAttrs.put("anchorPageNumber", anchorPageNumber);
                }
            } else if (anchorType.equals("frame")) {
                // OX API: true: image as character, false: floating mode
                drawingAttrs.put(OCKey.INLINE.value(), Boolean.FALSE);
                anchorHorBase = "column";
                anchorVertBase= "margin";
            } else if (anchorType.equals("paragraph")) {
                // OX API: true: image as character, false: floating mode
                drawingAttrs.put(OCKey.INLINE.value(), Boolean.FALSE);
                anchorHorBase = "column";
                anchorVertBase = "paragraph";
            } else if (anchorType.equals("char")) {
                // OX API: true: image as character, true: floating mode
                drawingAttrs.put(OCKey.INLINE.value(), Boolean.FALSE);
                anchorHorBase = "character";
                anchorVertBase = "paragraph";
            } else if (anchorType.equals("as-char")) {
                // OX API: true: image as character, false: floating mode
                drawingAttrs.put(OCKey.INLINE.value(), Boolean.TRUE);
            }
            if(anchorVertBase != null && !drawingAttrs.containsKey(OCKey.ANCHOR_VERT_BASE.value())) {
                drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), anchorVertBase);
            }
            if(anchorHorBase != null && !drawingAttrs.containsKey(OCKey.ANCHOR_HOR_BASE.value())) {
                drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), anchorHorBase);
            }
        }
	}

	public static Shape createShape(OdfOperationDoc operationDoc, JSONObject attrs, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
	    if(attrs!=null) {
	        final JSONObject geometryAttrs = attrs.optJSONObject(OCKey.GEOMETRY.value());
	        if(geometryAttrs!=null) {
	            final String presetShapeType = geometryAttrs.optString(OCKey.PRESET_SHAPE.value(), null);
                if("line".equals(presetShapeType)) {
                    return new LineShape(operationDoc, parentGroup, rootShape, contentStyle);
                }
	            final String hostData = geometryAttrs.optString(OCKey.HOST_DATA.value(), null);
	            if(presetShapeType!=null||hostData!=null) {
	                return new CustomShape(operationDoc, parentGroup, rootShape, contentStyle);
	            }
                final Object avList = geometryAttrs.opt(OCKey.AV_LIST.value());
                final Object gdList = geometryAttrs.opt(OCKey.GD_LIST.value());
                final Object textRect = geometryAttrs.opt(OCKey.TEXT_RECT.value());
                final Object pathList = geometryAttrs.opt(OCKey.PATH_LIST.value());
                if(pathList instanceof JSONArray&&avList==null&&gdList==null&&textRect==null) {
                    return new Path(operationDoc, parentGroup, rootShape, contentStyle);
                }
	        }
	    }
	    return new Shape(operationDoc, Namespaces.DRAW, "rect", "draw:rect", parentGroup, rootShape, contentStyle);
	}

    public void initializeShape(StyleManager styleManager) {
        if(transformer.isInitialized()) {
            return;
        }
        if(this instanceof CustomShape) {
            // Flip already set in EnhancedGeometryHandler
        }
        else {
            // what to do with "StylePresentation ?"
            GraphicProperties graphicProperties = null;
            final StyleBase styleBase = styleManager.getStyle(getStyleName(), StyleFamily.GRAPHIC, isContentStyle());
            if(styleBase instanceof StyleGraphic) {
                graphicProperties = ((StyleGraphic)styleBase).getGraphicProperties();
            }
            if(graphicProperties!=null) {
                final String mirror = graphicProperties.getAttribute("style:mirror");
                if(mirror!=null) {
                    if(mirror.contains("horizontal") && !mirror.contains("-on-")) {
                        transformer.setFlipH(Boolean.logicalXor(BooleanUtils.toBoolean(transformer.getFlipH()), Boolean.TRUE));
                    }
                    if(mirror.contains("vertical")) {
                        transformer.setFlipV(Boolean.logicalXor(BooleanUtils.toBoolean(transformer.getFlipV()), Boolean.TRUE));
                    }
                }
            }
        }
        Double rotation = transformer.getRotation();
        if(rotation!=null) {
            if(Boolean.logicalXor(BooleanUtils.toBoolean(transformer.getFlipH()), BooleanUtils.toBoolean(transformer.getFlipV()))) {
                transformer.setRotation(-transformer.getRotation());
            }
        }
        transformer.setInitilized(true);
    }

    public void prepareSaveShape(StyleManager styleManager) {
        if(transformer.isInitialized()) {
            attributes.remove("draw:transform");
            if(transformer.isLineMode()) {
                // dollar, paragraph sign, percent= line special case
                final double width = transformer.getWidth();
                final double height = transformer.getHeight();

                double x1 = transformer.getX();
                double y1 = transformer.getY();
                double x2 = width + transformer.getX();
                double y2 = height + transformer.getY();
                if(BooleanUtils.toBoolean(transformer.getFlipH())) {
                    transformer.setFlipH(null);
                    x1 += width;
                    x2 -= width;
                }
                if(BooleanUtils.toBoolean(transformer.getFlipV())) {
                    transformer.setFlipV(null);
                    y1 += height;
                    y2 -= height;
                }
                final Double rotation = transformer.getRotation();
                if(rotation!=null&&rotation!=0.0) {
                    final double[] pts1 = Tools.rotatePoint(x1, y1, transformer.getCenterX(), transformer.getCenterY(), rotation);
                    final double[] pts2 = Tools.rotatePoint(x2, y2, transformer.getCenterX(), transformer.getCenterY(), rotation);
                    x1 = pts1[0];
                    y1 = pts1[1];
                    x2 = pts2[0];
                    y2 = pts2[1];
                }
                attributes.setValue(Namespaces.SVG, "x1", "svg:x1", (x1 / 100.0 + "mm"));
                attributes.setValue(Namespaces.SVG, "y1", "svg:y1", (y1 / 100.0 + "mm"));
                attributes.setValue(Namespaces.SVG, "x2", "svg:x2", (x2 / 100.0 + "mm"));
                attributes.setValue(Namespaces.SVG, "y2", "svg:y2", (y2 / 100.0 + "mm"));
            }
            else {
/*
                if(Boolean.logicalXor(BooleanUtils.toBoolean(transformer.getFlipH()), BooleanUtils.toBoolean(transformer.getFlipV()))) {
                    transformer.setRotation(-transformer.getRotation());
                }
*/
                if(transformer.getRotation()!=null&&transformer.getRotation()!=0.0d) {
                    final boolean isTextDocument = styleManager.getDocumentType().equals("text");
                    if(isTextDocument) {
                        // https://bugs.documentfoundation.org/show_bug.cgi?id=115782 ... wrong orientation up to LO601
                        attributes.setValue(Namespaces.SVG, "x", "svg:x", (transformer.getX() / 100.0 + "mm"));
                        attributes.setValue(Namespaces.SVG, "y", "svg:y", (transformer.getY() / 100.0 + "mm"));
                    }
                    else {
                        attributes.remove("svg:x");
                        attributes.remove("svg:y");
                    }
                    final double dxCenter = transformer.getWidth() * 0.5d;
                    final double dyCenter = transformer.getHeight() * 0.5d;
                    final double alpha = transformer.getRotation() * Math.PI / 180.0d;
                    final double x2 = dxCenter * Math.cos(alpha) - dyCenter * Math.sin(alpha);
                    final double y2 = dyCenter * Math.cos(alpha) + dxCenter * Math.sin(alpha);
                    double translateX = -(x2 - dxCenter);
                    double translateY = -(y2 - dyCenter);
                    translateX += transformer.getX();
                    translateY += transformer.getY();
                    final StringBuffer buffer = new StringBuffer();
                    if(isTextDocument) {
                        buffer.append("translate (");
                        buffer.append(-((dxCenter + transformer.getX()) / 100.0) + "mm ");
                        buffer.append(-((dyCenter + transformer.getY()) / 100.0) + "mm) ");
                        translateX = dxCenter + transformer.getX();
                        translateY = dyCenter + transformer.getY();
                    }
                    buffer.append("rotate (");
                    buffer.append(Double.valueOf(-alpha).toString());
                    buffer.append(") translate (");
                    buffer.append(Double.valueOf(translateX / 100d).toString());
                    buffer.append("mm ");
                    buffer.append(Double.valueOf(translateY / 100d).toString());
                    buffer.append("mm)");
                    attributes.setValue(Namespaces.DRAW, "transform", "draw:transform", buffer.toString());
                }
                else {
                    attributes.setValue(Namespaces.SVG, "x", "svg:x", (transformer.getX() / 100.0 + "mm"));
                    attributes.setValue(Namespaces.SVG, "y", "svg:y", (transformer.getY() / 100.0 + "mm"));
                }
                attributes.setValue(Namespaces.SVG, "width", "svg:width", (transformer.getWidth() / 100.0 + "mm"));
                attributes.setValue(Namespaces.SVG, "height", "svg:height", (transformer.getHeight() / 100.0 + "mm"));

                if(this instanceof CustomShape) {
                    ((CustomShape)this).getEnhancedGeometry(true).setModified(true);
                }
                else if(this instanceof Path) {
                    if(transformer.getFlipH()!=null && transformer.getFlipH().booleanValue()) {
                        ((Path)this).flipH();
                    }
                }
                else {

                    final StyleBase styleBase = styleManager.getStyleBaseClone(attributes.containsKey("presentation:style-name") ? StyleFamily.PRESENTATION: StyleFamily.GRAPHIC, getStyleName(), isContentStyle());
                    if(styleBase instanceof IGraphicProperties) {
                        final GraphicProperties graphicProperties = ((IGraphicProperties) styleBase).getGraphicProperties();
                        boolean styleFlipH = false;
                        final String mirror = graphicProperties.getAttribute("style:mirror");
                        if(mirror!=null) {
                            if(mirror.contains("horizontal") && !mirror.contains("-on-")) {
                                styleFlipH = true;
                            }
                        }
                        final boolean transFlipH = transformer.getFlipH()!=null && transformer.getFlipH().booleanValue();
                        if(styleFlipH!=transFlipH) {
                            if(transFlipH) {
                                graphicProperties.getAttributes().setValue(Namespaces.STYLE, "mirror", "style:mirror", "horizontal");
                            }
                            else  {
                                graphicProperties.getAttributes().remove("style:mirror");
                            }
                            setStyleName(styleManager.getStyleIdForStyleBase(styleBase));
                        }
                    }
                }
            }
            transformer.setInitilized(false);
        }
    }
}
