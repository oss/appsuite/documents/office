/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;

public class ShapeHelper {

    public static Shape getShape(Attributes attributes, String uri, String localName, String qName, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
	    switch(qName) {
            case "draw:g": {
                return new GroupShape(new AttributesImpl(attributes), parentGroup, rootShape, contentStyle);
            }
            case "draw:custom-shape": {
                return new CustomShape(new AttributesImpl(attributes), parentGroup, rootShape, contentStyle);
            }
            case "draw:circle":
            case "draw:ellipse": {
                return new Ellipse(new AttributesImpl(attributes), uri, localName, qName, parentGroup, rootShape, contentStyle);
            }
            case "draw:measure":
            case "draw:caption":
            case "draw:rect":
            case "draw:regular-polygon": {
                return new Shape(new AttributesImpl(attributes), uri, localName, qName, parentGroup, rootShape, contentStyle);
            }
            case "draw:path": {
                return new Path(new AttributesImpl(attributes), uri, localName, qName, parentGroup, rootShape, contentStyle);
            }
            case "draw:polygon":
            case "draw:polyline": {
                return new Polygon(new AttributesImpl(attributes), uri, localName, qName, parentGroup, rootShape, contentStyle);
            }

            case "draw:connector": {
                return new ConnectorShape(new AttributesImpl(attributes), uri, localName, qName, parentGroup, rootShape, contentStyle);
            }
            case "draw:line": {
                return new LineShape(new AttributesImpl(attributes), uri, localName, qName, parentGroup, rootShape, contentStyle);
            }

            case "draw:page-thumbnail":
            case "draw:control":
            case "dr3d:scene": {
                return new UndefinedShape(new AttributesImpl(attributes), uri, localName, qName, parentGroup, rootShape, contentStyle);
            }
            default: {
                return null;
            }
	    }
	}
}
