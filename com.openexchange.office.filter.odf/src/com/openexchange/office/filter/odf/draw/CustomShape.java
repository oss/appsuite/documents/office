/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.STShapeType;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class CustomShape extends Shape {

    private EnhancedGeometry enhancedGeometry;

    public CustomShape(OdfOperationDoc odfOperationDoc, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
        super(odfOperationDoc, Namespaces.DRAW, "custom-shape", "draw:custom-shape", parentGroup, rootShape, contentStyle);
    }

    public CustomShape(AttributesImpl attributes, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
        super(attributes, Namespaces.DRAW,"custom-shape", "draw:custom-shape", parentGroup, rootShape, contentStyle);
    }

    @Override
    public DrawingType getType() {
        return enhancedGeometry!=null&&enhancedGeometry.isConnector() ? DrawingType.CONNECTOR : DrawingType.SHAPE;
    }

    public EnhancedGeometry getEnhancedGeometry(boolean forceCreate) {
        if(enhancedGeometry==null&&forceCreate) {
            enhancedGeometry = new EnhancedGeometry(this);
            getContent().add(enhancedGeometry);
        }
        return enhancedGeometry;
    }

    @Override
    public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle)
        throws JSONException {

        super.applyAttrsFromJSON(operationDocument, attrs, contentAutoStyle);

        final JSONObject geometryAttrs = attrs.optJSONObject(OCKey.GEOMETRY.value());
        if(geometryAttrs!=null) {
            final Object hostData = geometryAttrs.opt(OCKey.HOST_DATA.value());
            if(hostData instanceof String) {
                getEnhancedGeometry(true).setShapeData((String)hostData);
            }
            else if(hostData==JSONObject.NULL) {
                getEnhancedGeometry(true).setShapeData(null);
            }
            final Object presetShape = geometryAttrs.opt(OCKey.PRESET_SHAPE.value());
            if(presetShape instanceof String) {
                final EnhancedGeometry enhancedGeometry = getEnhancedGeometry(true);
                enhancedGeometry.setPresetShape(STShapeType.fromValue((String)presetShape));
                // there is no working OO counterpart for the ooxml up arrow, so we use a flipped down arrow here
                if(enhancedGeometry.getPresetShape()==STShapeType.UP_ARROW) {
                    enhancedGeometry.setPresetShape(STShapeType.DOWN_ARROW);
                    final Boolean flipV = transformer.getFlipV();
                    transformer.setFlipV(flipV==null||!flipV.booleanValue() ? Boolean.TRUE : null);
                }
            }
            final Object avList = geometryAttrs.opt(OCKey.AV_LIST.value());
            if(avList!=null) {
                final EnhancedGeometry geometry = getEnhancedGeometry(true);
                final TreeMap<String, Number> modifiers = geometry.getModifiers(true);
                modifiers.clear();
                if(avList instanceof JSONObject) {
                    final Iterator<Entry<String, Object>> avListIter = ((JSONObject)avList).entrySet().iterator();
                    while(avListIter.hasNext()) {
                        final Entry<String, Object> avListEntry = avListIter.next();
                        if(avListEntry.getValue() instanceof Number) {
                            modifiers.put(avListEntry.getKey(), (Number)avListEntry.getValue());
                        }
                    }
                }
                geometry.setModified(true);
            }
        }

        final JSONObject drawingProps = attrs.optJSONObject(OCKey.DRAWING.value());
        if(drawingProps!=null) {
            final Object flipH = drawingProps.opt(OCKey.FLIP_H.value());
            final Object flipV = drawingProps.opt(OCKey.FLIP_V.value());
            if(flipH!=null||flipV!=null) {
                getEnhancedGeometry(true).setModified(true);
            }
        }

        final JSONObject connector = attrs.optJSONObject(OCKey.CONNECTOR.value());
        if (connector != null) {
            final Object startId = connector.opt(OCKey.START_ID.value());
            if (startId!=null) {
                if (startId==JSONObject.NULL||((String)startId).isEmpty()) {
                    attributes.remove("loext:start-id");
                } else {
                    attributes.setValue(Namespaces.LOEXT, "start-id", "loext:start-id", (String)startId);
                }
            }
            final Object startIndex = connector.opt(OCKey.START_INDEX.value());
            if (startIndex!=null) {
                if (startIndex==JSONObject.NULL) {
                    attributes.remove("loext:start-index");
                } else {
                    attributes.setIntValue(Namespaces.LOEXT, "start-index", "loext:start-index", ((Number)startIndex).intValue());
                }
            }
            final Object endId = connector.opt(OCKey.END_ID.value());
            if (endId!=null) {
                if (endId==JSONObject.NULL||((String) endId).isEmpty()) {
                    attributes.remove("loext:end-id");
                } else {
                    attributes.setValue(Namespaces.LOEXT, "end-id", "loext:end-id", (String)endId);
                }
            }
            final Object endIndex = connector.opt(OCKey.END_INDEX.value());
            if (endIndex!=null) {
                if (endIndex == JSONObject.NULL) {
                    attributes.remove("loext:end-index");
                } else {
                    attributes.setIntValue(Namespaces.LOEXT, "end-index", "loext:end-index", ((Number)endIndex).intValue());
                }
            }
        }
    }

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
        super.createAttrs(operationDocument, attrs, contentAutoStyle);

        if(enhancedGeometry!=null) {
            final Map<String, Object> geometryAttrs = attrs.getMap(OCKey.GEOMETRY.value(), true);
            final STShapeType presetShape = enhancedGeometry.getPresetShape();
            if(presetShape!=null) {
                geometryAttrs.put(OCKey.PRESET_SHAPE.value(), presetShape.value());
            }
            final String shapeData = enhancedGeometry.getShapeData();
            if(shapeData!=null) {
                geometryAttrs.put(OCKey.HOST_DATA.value(), shapeData);
            }
            final TreeMap<String, Number> modifiers = enhancedGeometry.getModifiers(false);
            if(modifiers!=null) {
                geometryAttrs.put(OCKey.AV_LIST.value(), modifiers);
            }
            final List<OpAttrs> pathList = enhancedGeometry.getPathList();
            if(pathList!=null) {
                geometryAttrs.put(OCKey.PATH_LIST.value(), pathList);
            }
        }

        final String startShape = attributes.getValue("loext:start-id");
        final String endShape = attributes.getValue("loext:end-id");
        if((startShape!=null&&!startShape.isEmpty())||(endShape!=null&&!endShape.isEmpty())) {

            final Integer startIndex = attributes.getIntValue("loext:start-index", null);
            final Integer endIndex = attributes.getIntValue("loext:end-index", null);

            final OpAttrs connector = new OpAttrs(2);
            if (startShape!=null&&!startShape.isEmpty()) {
                connector.put(OCKey.START_ID.value(), startShape);
                if(startIndex!=null) {
                    connector.put(OCKey.START_INDEX.value(), startIndex);
                }
            }
            if (endShape!=null) {
                connector.put(OCKey.END_ID.value(), endShape);
                if(endIndex!=null) {
                    connector.put(OCKey.END_INDEX.value(), endIndex);
                }
            }
            attrs.put(OCKey.CONNECTOR.value(), connector);
        }
    }

    @Override
    public SaxContextHandler getContextHandler(SaxContextHandler parentHandler) {
        return new CustomShapeHandler(parentHandler, this);
    }
}
