/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;

public class Ellipse extends Shape {

    public Ellipse(AttributesImpl attributes, String uri, String localName, String qName, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
        super(attributes, uri, localName, qName, parentGroup, rootShape, contentStyle);
    }

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
        super.createAttrs(operationDocument, attrs, contentAutoStyle);

        final String kind = attributes.getValue("draw:kind");
        final String predefinedShape;
        if("section".equals(kind)) {
            predefinedShape = "pie";
        }
        else if("cut".equals(kind)) {
            predefinedShape = "chord";
        }
        else if("arc".equals(kind)) {
            predefinedShape = "arc";
            attrs.getMap(OCKey.FILL.value(), true).put(OCKey.TYPE.value(), "none");     // arc in LO is always without fill
        }
        else {  // "full"
            predefinedShape = "ellipse";
        }
        final OpAttrs geometryAttrs = attrs.getMap(OCKey.GEOMETRY.value(), true);
        geometryAttrs.put(OCKey.PRESET_SHAPE.value(), predefinedShape);

        // the direction of start and end angle is different. in LO the direction is counter clockwise, whereas the direction in ooxml is clockwise
        final Double endAngle = attributes.getDoubleValue("draw:start-angle");          // -> start and end angle has to be swapped
        final Double startAngle = attributes.getDoubleValue("draw:end-angle");
        if(startAngle!=null||endAngle!=null) {
            final OpAttrs avList = new OpAttrs();
            geometryAttrs.put(OCKey.AV_LIST.value(), avList);
            if(startAngle!=null) {  // -> adj1
                avList.put("adj1", Double.valueOf((360.0 - startAngle.doubleValue()) * 60000.0).intValue());
            }
            if(endAngle!=null) {    // -> adj2
                avList.put("adj2", Double.valueOf((360.0 - endAngle.doubleValue()) * 60000.0).intValue());
            }
        }
    }
}
