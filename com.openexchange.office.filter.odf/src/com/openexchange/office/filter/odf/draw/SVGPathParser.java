/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;

public class SVGPathParser {

    public static void pathListToSvg(JSONArray pathList, AttributesImpl attributes)
        throws JSONException {

        final JSONObject path = pathList.optJSONObject(0);
        if(path!=null) {
            final StringBuffer stringBuffer = new StringBuffer();
            final Object width = path.opt(OCKey.WIDTH.value());
            final Object height = path.opt(OCKey.HEIGHT.value());
            if(width instanceof Number && height instanceof Number) {
                stringBuffer.append("0 0 ");
                stringBuffer.append(Long.valueOf(((Number)width).longValue()).toString());
                stringBuffer.append(' ');
                stringBuffer.append(Long.valueOf(((Number)height).longValue()).toString());
                attributes.setValue(Namespaces.SVG, "viewBox", "svg:viewBox", stringBuffer.toString());
                stringBuffer.setLength(0);
            }
            final JSONArray commands = path.optJSONArray(OCKey.COMMANDS.value());
            for(int i=0; i<commands.length(); i++) {
                final JSONObject command = commands.getJSONObject(i);
                final String c = command.getString(OCKey.C.value());
                switch(c) {
                    case "moveTo" : {
                        final Object x = command.opt(OCKey.X.value());
                        final Object y = command.opt(OCKey.Y.value());
                        if(x instanceof Number && y instanceof Number) {
                            stringBuffer.append('M');
                            stringBuffer.append(Long.valueOf(((Number)x).longValue()).toString());
                            stringBuffer.append(' ');
                            stringBuffer.append(Long.valueOf(((Number)y).longValue()).toString());
                        }
                        break;
                    }
                    case "lineTo" : {
                        final Object x = command.opt(OCKey.X.value());
                        final Object y = command.opt(OCKey.Y.value());
                        if(x instanceof Number && y instanceof Number) {
                            stringBuffer.append('L');
                            stringBuffer.append(Long.valueOf(((Number)x).longValue()).toString());
                            stringBuffer.append(' ');
                            stringBuffer.append(Long.valueOf(((Number)y).longValue()).toString());
                        }
                        break;
                    }
                    case "cubicBezierTo" : {
                        final JSONArray pts = command.optJSONArray(OCKey.PTS.value());
                        if(pts!=null) {
                            for(int p=0; (p+2)<pts.length(); p+=3) {
                                final JSONObject c1 = pts.getJSONObject(p);
                                final JSONObject c2 = pts.getJSONObject(p+1);
                                final JSONObject po = pts.getJSONObject(p+2);
                                stringBuffer.append('C');
                                stringBuffer.append(Long.valueOf(((Number)c1.opt(OCKey.X.value())).longValue()).toString());
                                stringBuffer.append(' ');
                                stringBuffer.append(Long.valueOf(((Number)c1.opt(OCKey.Y.value())).longValue()).toString());
                                stringBuffer.append(' ');
                                stringBuffer.append(Long.valueOf(((Number)c2.opt(OCKey.X.value())).longValue()).toString());
                                stringBuffer.append(' ');
                                stringBuffer.append(Long.valueOf(((Number)c2.opt(OCKey.Y.value())).longValue()).toString());
                                stringBuffer.append(' ');
                                stringBuffer.append(Long.valueOf(((Number)po.opt(OCKey.X.value())).longValue()).toString());
                                stringBuffer.append(' ');
                                stringBuffer.append(Long.valueOf(((Number)po.opt(OCKey.Y.value())).longValue()).toString());
                            }
                        }
                        break;
                    }
                    case "close" : {
                        stringBuffer.append('z');
                        break;
                    }
                    case "arcTo" : {
                        break;
                    }
                    case "quadBezierTo" : {
                        break;
                    }
                }
            }
            attributes.setValue(Namespaces.SVG, "d", "svg:d", stringBuffer.toString());
        }
    }

    public static List<OpAttrs> getPathList(String svgPath, ViewBox viewBox, boolean noFillIfNotClosed) {

        final List<OpAttrs> pathList = new ArrayList<OpAttrs>(1);
        if(svgPath==null) {
            return pathList;
        }
        final String svgD = svgPath.trim();
        if(svgD!=null&&svgD.length()>0) {
            if(viewBox.isValid()) {
                final int width = viewBox.getWidth();
                final int height = viewBox.getHeight();

                final OpAttrs path = new OpAttrs();
                path.put(OCKey.WIDTH.value(), width);
                path.put(OCKey.HEIGHT.value(), height);

                final List<OpAttrs> commands = new ArrayList<OpAttrs>();
                path.put(OCKey.COMMANDS.value(), commands);

                boolean closed = false;

                final StringPosValue stringPosValue = new StringPosValue(svgD, 0);
                try {

                    double offX = viewBox.getX();
                    double offY = viewBox.getY();
                    double nLastX = 0.0d;
                    double nLastY = 0.0d;
                    Double nLastControlX = null;
                    Double nLastControlY = null;

                    boolean hasStartPoint = false;

                    while(stringPosValue.hasMore()) {

                        boolean bRelative = false;
                        char aCurrChar = stringPosValue.currentChar();

                        switch(aCurrChar) {

                            case 'z' :
                            case 'Z' : {

                                // consume CurrChar and whitespace
                                stringPosValue.incPos();
                                stringPosValue.skipSpaces();

                                final OpAttrs geometryCommand = new OpAttrs();
                                geometryCommand.put(OCKey.C.value(), "close");
                                commands.add(geometryCommand);
                                hasStartPoint = false;
                                nLastControlX = null;
                                closed = true;
                                break;
                            }

                            case 'm' :
                            case 'M' :
                            case 'l' :
                            case 'L' : {

                                if('m' == aCurrChar || 'l' == aCurrChar) {
                                    bRelative = true;
                                }

                                // consume CurrChar and whitespace
                                stringPosValue.incPos();
                                stringPosValue.skipSpaces();

                                while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                    double nX = stringPosValue.importDoubleAndSpaces();
                                    double nY = stringPosValue.importDoubleAndSpaces();

                                    if(bRelative) {
                                        nX += nLastX;
                                        nY += nLastY;
                                    }
                                    else {
                                        nX -= offX;
                                        nY -= offY;
                                    }

                                    // set last position
                                    nLastX = nX;
                                    nLastY = nY;

                                    // add point
                                    addPoint(commands, aCurrChar=='m'||aCurrChar=='M', Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());
                                    hasStartPoint = true;
                                    nLastControlX = null;
                                }
                                break;
                            }

                            case 'h' : bRelative = true;
                                //$FALL-THROUGH$
                            case 'H' : {

                                stringPosValue.incPos();
                                stringPosValue.skipSpaces();

                                while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                    double nY = nLastY;
                                    double nX = stringPosValue.importDoubleAndSpaces();

                                    if(bRelative) {
                                        nX += nLastX;
                                    }
                                    else {
                                        nX -= offX;
                                    }
                                    // set last position
                                    nLastX = nX;

                                    // add point
                                    addPoint(commands, !hasStartPoint, Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());
                                    hasStartPoint = true;
                                    nLastControlX = null;
                                }
                                break;
                            }

                            case 'v' : bRelative = true;
                                //$FALL-THROUGH$
                            case 'V' : {

                                stringPosValue.incPos();
                                stringPosValue.skipSpaces();

                                while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                    double nX = nLastX;
                                    double nY = stringPosValue.importDoubleAndSpaces();

                                    if(bRelative) {
                                        nY += nLastY;
                                    }
                                    else {
                                        nY -= offY;
                                    }

                                    // set last position
                                    nLastY = nY;

                                    // add point
                                    addPoint(commands, !hasStartPoint, Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());
                                    hasStartPoint = true;
                                    nLastControlX = null;
                                }
                                break;
                            }

                            case 's' : bRelative = true;
                                //$FALL-THROUGH$
                            case 'S' : {

                                stringPosValue.incPos();
                                stringPosValue.skipSpaces();

                                while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                    double nX2 = stringPosValue.importDoubleAndSpaces();
                                    double nY2 = stringPosValue.importDoubleAndSpaces();
                                    double nX = stringPosValue.importDoubleAndSpaces();
                                    double nY = stringPosValue.importDoubleAndSpaces();

                                    if(bRelative) {
                                        nX2 += nLastX;
                                        nY2 += nLastY;
                                        nX += nLastX;
                                        nY += nLastY;
                                    }
                                    else {
                                        nX2 -= offX;
                                        nY2 -= offY;
                                        nX -= offX;
                                        nY -= offY;
                                    }

                                    // ensure existence of start point
                                    if(!hasStartPoint) {
                                        addPoint(commands, true, Double.valueOf(nLastX).intValue(), Double.valueOf(nLastY).intValue());
                                    }

                                    // get first control point. It's the reflection of the PrevControlPoint
                                    // of the last point. If not existent, use current point (see SVG)
                                    double prevControlX = nLastX;
                                    double prevControlY = nLastY;

                                    if(nLastControlX!=null) {
                                        prevControlX = (2.0 * nLastX) - nLastControlX;
                                        prevControlY = (2.0 * nLastY) - nLastControlY;
                                    }

                                    // append curved edge
                                    final OpAttrs geometryCommand = new OpAttrs();
                                    geometryCommand.put(OCKey.C.value(), "cubicBezierTo");
                                    final ArrayList<OpAttrs> pts = new ArrayList<OpAttrs>();
                                    final OpAttrs c1 = new OpAttrs();
                                    final OpAttrs c2 = new OpAttrs();
                                    final OpAttrs p = new OpAttrs();
                                    c1.put(OCKey.X.value(), Double.valueOf(prevControlX).intValue());
                                    c1.put(OCKey.Y.value(), Double.valueOf(prevControlY).intValue());
                                    c2.put(OCKey.X.value(), Double.valueOf(nX2).intValue());
                                    c2.put(OCKey.Y.value(), Double.valueOf(nY2).intValue());
                                    p.put(OCKey.X.value(), Double.valueOf(nX).intValue());
                                    p.put(OCKey.Y.value(), Double.valueOf(nY).intValue());
                                    pts.add(c1);
                                    pts.add(c2);
                                    pts.add(p);
                                    geometryCommand.put(OCKey.PTS.value(), pts);
                                    commands.add(geometryCommand);

                                    // set last position
                                    nLastX = nX;
                                    nLastY = nY;
                                    nLastControlX = nX2;
                                    nLastControlY = nY2;
                                    hasStartPoint = true;
                                }
                                break;
                            }

                            case 'c' : bRelative = true;
                                //$FALL-THROUGH$
                            case 'C' : {

                                stringPosValue.incPos();
                                stringPosValue.skipSpaces();

                                while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                    double nX1 = stringPosValue.importDoubleAndSpaces();
                                    double nY1 = stringPosValue.importDoubleAndSpaces();
                                    double nX2 = stringPosValue.importDoubleAndSpaces();
                                    double nY2 = stringPosValue.importDoubleAndSpaces();
                                    double nX = stringPosValue.importDoubleAndSpaces();
                                    double nY = stringPosValue.importDoubleAndSpaces();

                                    if(bRelative) {
                                        nX1 += nLastX;
                                        nY1 += nLastY;
                                        nX2 += nLastX;
                                        nY2 += nLastY;
                                        nX += nLastX;
                                        nY += nLastY;
                                    }
                                    else {
                                        nX1 -= offX;
                                        nY1 -= offY;
                                        nX2 -= offX;
                                        nY2 -= offY;
                                        nX -= offX;
                                        nY -= offY;
                                    }

                                    // ensure existence of start point
                                    if(!hasStartPoint) {
                                        addPoint(commands, true, Double.valueOf(nLastX).intValue(), Double.valueOf(nLastY).intValue());
                                    }

                                    final OpAttrs geometryCommand = new OpAttrs();
                                    geometryCommand.put(OCKey.C.value(), "cubicBezierTo");
                                    final ArrayList<OpAttrs> pts = new ArrayList<OpAttrs>(3);
                                    final OpAttrs c1 = new OpAttrs();
                                    final OpAttrs c2 = new OpAttrs();
                                    final OpAttrs p = new OpAttrs();
                                    c1.put(OCKey.X.value(), Double.valueOf(nX1).intValue());
                                    c1.put(OCKey.Y.value(), Double.valueOf(nY1).intValue());
                                    c2.put(OCKey.X.value(), Double.valueOf(nX2).intValue());
                                    c2.put(OCKey.Y.value(), Double.valueOf(nY2).intValue());
                                    p.put(OCKey.X.value(), Double.valueOf(nX).intValue());
                                    p.put(OCKey.Y.value(), Double.valueOf(nY).intValue());
                                    pts.add(c1);
                                    pts.add(c2);
                                    pts.add(p);
                                    geometryCommand.put(OCKey.PTS.value(), pts);
                                    commands.add(geometryCommand);

                                    // set last position
                                    nLastX = nX;
                                    nLastY = nY;
                                    nLastControlX = nX2;
                                    nLastControlY = nY2;
                                    hasStartPoint = true;
                                }
                                break;
                            }

                            // #100617# quadratic beziers are imported as cubic ones
                            case 'q' : bRelative = true;
                                //$FALL-THROUGH$
                            case 'Q' : {

                                stringPosValue.incPos();
                                stringPosValue.skipSpaces();

                                while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                    double nX1 = stringPosValue.importDoubleAndSpaces();
                                    double nY1 = stringPosValue.importDoubleAndSpaces();
                                    double nX = stringPosValue.importDoubleAndSpaces();
                                    double nY = stringPosValue.importDoubleAndSpaces();

                                    if(bRelative) {
                                        nX1 += nLastX;
                                        nY1 += nLastY;
                                        nX += nLastX;
                                        nY += nLastY;
                                    }
                                    else {
                                        nX1 -= offX;
                                        nY1 -= offY;
                                        nX -= offX;
                                        nY -= offY;
                                    }

                                    // calculate the cubic bezier coefficients from the quadratic ones
                                    final double nX1Prime = ((nX1 * 2.0 + nLastX) / 3.0);
                                    final double nY1Prime = ((nY1 * 2.0 + nLastY) / 3.0);
                                    final double nX2Prime = ((nX1 * 2.0 + nX) / 3.0);
                                    final double nY2Prime = ((nY1 * 2.0 + nY) / 3.0);

                                    // ensure existence of start point
                                    if(!hasStartPoint) {
                                        addPoint(commands, true, Double.valueOf(nLastX).intValue(), Double.valueOf(nLastY).intValue());
                                    }

                                    final OpAttrs geometryCommand = new OpAttrs();
                                    geometryCommand.put(OCKey.C.value(), "cubicBezierTo");
                                    final ArrayList<OpAttrs> pts = new ArrayList<OpAttrs>(3);
                                    final OpAttrs c1 = new OpAttrs();
                                    final OpAttrs c2 = new OpAttrs();
                                    final OpAttrs p = new OpAttrs();
                                    c1.put(OCKey.X.value(), Double.valueOf(nX1Prime).intValue());
                                    c1.put(OCKey.Y.value(), Double.valueOf(nY1Prime).intValue());
                                    c2.put(OCKey.X.value(), Double.valueOf(nX2Prime).intValue());
                                    c2.put(OCKey.Y.value(), Double.valueOf(nY2Prime).intValue());
                                    p.put(OCKey.X.value(), Double.valueOf(nX).intValue());
                                    p.put(OCKey.Y.value(), Double.valueOf(nY).intValue());
                                    pts.add(c1);
                                    pts.add(c2);
                                    pts.add(p);
                                    geometryCommand.put(OCKey.PTS.value(), pts);
                                    commands.add(geometryCommand);

                                    // set last position
                                    nLastX = nX;
                                    nLastY = nY;
                                    nLastControlX = nX2Prime;
                                    nLastControlY = nY2Prime;
                                    hasStartPoint = true;
                                }
                                break;
                            }

                            // #100617# relative quadratic beziers are imported as cubic
                            case 't' : bRelative = true;
                                //$FALL-THROUGH$
                            case 'T' : {

                                stringPosValue.incPos();
                                stringPosValue.skipSpaces();

                                while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                    double nX = stringPosValue.importDoubleAndSpaces();
                                    double nY = stringPosValue.importDoubleAndSpaces();

                                    if(bRelative) {
                                        nX += nLastX;
                                        nY += nLastY;
                                    }
                                    else {
                                        nX -= offX;
                                        nY -= offY;
                                    }

                                    // ensure existence of start point
                                    if(!hasStartPoint) {
                                        addPoint(commands, true, Double.valueOf(nLastX).intValue(), Double.valueOf(nLastY).intValue());
                                    }

                                    double prevControlX = nLastX;
                                    double prevControlY = nLastY;

                                    if(nLastControlX!=null) {
                                        prevControlX = (2.0 * nLastX) - nLastControlX;
                                        prevControlY = (2.0 * nLastY) - nLastControlY;
                                    }

                                    if((prevControlX!=nLastX)&&(prevControlY!=nLastY)) {
                                        // there is a prev control point, and we have the already mirrored one
                                        // in aPrevControl. We also need the quadratic control point for this
                                        // new quadratic segment to calculate the 2nd cubic control point
                                        final double quadControlPointX = ((3.0 * prevControlX) - nLastX) / 2.0;
                                        final double quadControlPointY = ((3.0 * prevControlY) - nLastY) / 2.0;

                                        // calculate the cubic bezier coefficients from the quadratic ones.
                                        final double nX2Prime = ((quadControlPointX * 2.0 + nX) / 3.0);
                                        final double nY2Prime = ((quadControlPointY * 2.0 + nY) / 3.0);

                                        // append curved edge, use mirrored cubic control point directly
                                        final OpAttrs geometryCommand = new OpAttrs();
                                        geometryCommand.put(OCKey.C.value(), "cubicBezierTo");
                                        final ArrayList<OpAttrs> pts = new ArrayList<OpAttrs>(3);
                                        final OpAttrs c1 = new OpAttrs();
                                        final OpAttrs c2 = new OpAttrs();
                                        final OpAttrs p = new OpAttrs();
                                        c1.put(OCKey.X.value(), Double.valueOf(prevControlX).intValue());
                                        c1.put(OCKey.Y.value(), Double.valueOf(prevControlY).intValue());
                                        c2.put(OCKey.X.value(), Double.valueOf(nX2Prime).intValue());
                                        c2.put(OCKey.Y.value(), Double.valueOf(nY2Prime).intValue());
                                        p.put(OCKey.X.value(), Double.valueOf(nX).intValue());
                                        p.put(OCKey.Y.value(), Double.valueOf(nY).intValue());
                                        pts.add(c1);
                                        pts.add(c2);
                                        pts.add(p);
                                        geometryCommand.put(OCKey.PTS.value(), pts);
                                        commands.add(geometryCommand);

                                        nLastControlX = nX2Prime;
                                        nLastControlY = nY2Prime;
                                    }
                                    else {
                                        // when no previous control, SVG says to use current point -> straight line. Just add end point
                                        addPoint(commands, false, Double.valueOf(nX).intValue(), Double.valueOf(nX).intValue());
                                        nLastControlX = null;
                                    }

                                    // set last position
                                    nLastX = nX;
                                    nLastY = nY;
                                    hasStartPoint = true;
                                }
                                break;
                            }

                            case 'a' :
                            case 'A' : {
                                break;
                            }

                            default: {
                                stringPosValue.incPos();
                                break;
                            }
                        }
                    }
                }
                catch(NumberFormatException e) {
                    // ohoh
                }
                if(!closed&&noFillIfNotClosed) {
                    path.put(OCKey.FILL_MODE.value(), "none");
                }
                pathList.add(path);
            }
        }
        return pathList;
    }

    public static void flipH(List<OpAttrs> pathList, ViewBox viewBox) {

        if(!pathList.isEmpty()&&viewBox.isValid()) {
            final OpAttrs path = pathList.get(0);
            final Object o = path.get(OCKey.COMMANDS.value());
            if(o instanceof List) {
                final List<OpAttrs> commands = (List<OpAttrs>)o;
                for(OpAttrs command:commands) {
                    final String c = command.optString(OCKey.C.value());
                    if(c!=null) {
                        switch(c) {
                            case "moveTo": {
                                flipH(command, viewBox.getHeight());
                                break;
                            }
                            case "lineTo": {
                                flipH(command, viewBox.getHeight());
                                break;
                            }
                            case "cubicBezierTo": {
                                final List<OpAttrs> pts = (List)command.get(OCKey.PTS.value());
                                if(pts!=null) {
                                    for(int p=0; (p+2)<pts.size(); p+=3) {
                                        flipH(pts.get(p), viewBox.getHeight());
                                        flipH(pts.get(p+1), viewBox.getHeight());
                                        flipH(pts.get(p+2), viewBox.getHeight());
                                    }
                                }
                                break;
                            }
                            case "close" : {
                                break;
                            }
                            case "arcTo" : {
                                break;
                            }
                            case "quadBezierTo" : {
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private static void flipH(OpAttrs p, int height) {
        final int y = ((Number)p.get(OCKey.Y.value())).intValue();
        p.put(OCKey.Y.value(), height - y);
    }

    public static ViewBox getViewBox(String svgPath) {

        if(svgPath==null) {
            return null;
        }
        final Rect rect = new Rect();
        final String svgD = svgPath.trim();
        if(svgD!=null&&svgD.length()>0) {

            final StringPosValue stringPosValue = new StringPosValue(svgD, 0);
            try {
                double nLastX = 0.0d;
                double nLastY = 0.0d;
                Double nLastControlX = null;
                Double nLastControlY = null;

                boolean hasStartPoint = false;

                while(stringPosValue.hasMore()) {

                    boolean bRelative = false;
                    char aCurrChar = stringPosValue.currentChar();

                    switch(aCurrChar) {

                        case 'z' :
                        case 'Z' : {

                            // consume CurrChar and whitespace
                            stringPosValue.incPos();
                            stringPosValue.skipSpaces();

                            hasStartPoint = false;
                            nLastControlX = null;
                            break;
                        }

                        case 'm' :
                        case 'M' :
                        case 'l' :
                        case 'L' : {

                            if('m' == aCurrChar || 'l' == aCurrChar) {
                                bRelative = true;
                            }

                            // consume CurrChar and whitespace
                            stringPosValue.incPos();
                            stringPosValue.skipSpaces();

                            while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                double nX = stringPosValue.importDoubleAndSpaces();
                                double nY = stringPosValue.importDoubleAndSpaces();

                                if(bRelative) {
                                    nX += nLastX;
                                    nY += nLastY;
                                }

                                // set last position
                                nLastX = nX;
                                nLastY = nY;

                                // add point
                                rect.add(Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());
                                hasStartPoint = true;
                                nLastControlX = null;
                            }
                            break;
                        }

                        case 'h' : bRelative = true;
                            //$FALL-THROUGH$
                        case 'H' : {

                            stringPosValue.incPos();
                            stringPosValue.skipSpaces();

                            while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                double nY = nLastY;
                                double nX = stringPosValue.importDoubleAndSpaces();

                                if(bRelative) {
                                    nX += nLastX;
                                }
                                // set last position
                                nLastX = nX;

                                // add point
                                rect.add(Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());
                                hasStartPoint = true;
                                nLastControlX = null;
                            }
                            break;
                        }

                        case 'v' : bRelative = true;
                            //$FALL-THROUGH$
                        case 'V' : {

                            stringPosValue.incPos();
                            stringPosValue.skipSpaces();

                            while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                double nX = nLastX;
                                double nY = stringPosValue.importDoubleAndSpaces();

                                if(bRelative) {
                                    nY += nLastY;
                                }
                                // set last position
                                nLastY = nY;

                                // add point
                                rect.add(Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());
                                hasStartPoint = true;
                                nLastControlX = null;
                            }
                            break;
                        }

                        case 's' : bRelative = true;
                            //$FALL-THROUGH$
                        case 'S' : {

                            stringPosValue.incPos();
                            stringPosValue.skipSpaces();

                            while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                double nX2 = stringPosValue.importDoubleAndSpaces();
                                double nY2 = stringPosValue.importDoubleAndSpaces();
                                double nX = stringPosValue.importDoubleAndSpaces();
                                double nY = stringPosValue.importDoubleAndSpaces();

                                if(bRelative) {
                                    nX2 += nLastX;
                                    nY2 += nLastY;
                                    nX += nLastX;
                                    nY += nLastY;
                                }

                                // ensure existence of start point
                                if(!hasStartPoint) {
                                    rect.add(Double.valueOf(nLastX).intValue(), Double.valueOf(nLastY).intValue());
                                }

                                // get first control point. It's the reflection of the PrevControlPoint
                                // of the last point. If not existent, use current point (see SVG)
                                double prevControlX = nLastX;
                                double prevControlY = nLastY;

                                if(nLastControlX!=null) {
                                    prevControlX = (2.0 * nLastX) - nLastControlX;
                                    prevControlY = (2.0 * nLastY) - nLastControlY;
                                }

                                // append curved edge
                                rect.add(Double.valueOf(prevControlX).intValue(), Double.valueOf(prevControlY).intValue());
                                rect.add(Double.valueOf(nX2).intValue(),  Double.valueOf(nY2).intValue());
                                rect.add(Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());

                                // set last position
                                nLastX = nX;
                                nLastY = nY;
                                nLastControlX = nX2;
                                nLastControlY = nY2;
                                hasStartPoint = true;
                            }
                            break;
                        }

                        case 'c' : bRelative = true;
                            //$FALL-THROUGH$
                        case 'C' : {

                            stringPosValue.incPos();
                            stringPosValue.skipSpaces();

                            while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                double nX1 = stringPosValue.importDoubleAndSpaces();
                                double nY1 = stringPosValue.importDoubleAndSpaces();
                                double nX2 = stringPosValue.importDoubleAndSpaces();
                                double nY2 = stringPosValue.importDoubleAndSpaces();
                                double nX = stringPosValue.importDoubleAndSpaces();
                                double nY = stringPosValue.importDoubleAndSpaces();

                                if(bRelative) {
                                    nX1 += nLastX;
                                    nY1 += nLastY;
                                    nX2 += nLastX;
                                    nY2 += nLastY;
                                    nX += nLastX;
                                    nY += nLastY;
                                }

                                // ensure existence of start point
                                if(!hasStartPoint) {
                                    rect.add(Double.valueOf(nLastX).intValue(), Double.valueOf(nLastY).intValue());
                                }

                                rect.add(Double.valueOf(nX1).intValue(), Double.valueOf(nY1).intValue());
                                rect.add(Double.valueOf(nX2).intValue(), Double.valueOf(nY2).intValue());
                                rect.add(Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());

                                // set last position
                                nLastX = nX;
                                nLastY = nY;
                                nLastControlX = nX2;
                                nLastControlY = nY2;
                                hasStartPoint = true;
                            }
                            break;
                        }

                        // #100617# quadratic beziers are imported as cubic ones
                        case 'q' : bRelative = true;
                            //$FALL-THROUGH$
                        case 'Q' : {

                            stringPosValue.incPos();
                            stringPosValue.skipSpaces();

                            while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                double nX1 = stringPosValue.importDoubleAndSpaces();
                                double nY1 = stringPosValue.importDoubleAndSpaces();
                                double nX = stringPosValue.importDoubleAndSpaces();
                                double nY = stringPosValue.importDoubleAndSpaces();

                                if(bRelative) {
                                    nX1 += nLastX;
                                    nY1 += nLastY;
                                    nX += nLastX;
                                    nY += nLastY;
                                }

                                // calculate the cubic bezier coefficients from the quadratic ones
                                final double nX1Prime = ((nX1 * 2.0 + nLastX) / 3.0);
                                final double nY1Prime = ((nY1 * 2.0 + nLastY) / 3.0);
                                final double nX2Prime = ((nX1 * 2.0 + nX) / 3.0);
                                final double nY2Prime = ((nY1 * 2.0 + nY) / 3.0);

                                // ensure existence of start point
                                if(!hasStartPoint) {
                                    rect.add(Double.valueOf(nLastX).intValue(), Double.valueOf(nLastY).intValue());
                                }

                                rect.add(Double.valueOf(nX1Prime).intValue(), Double.valueOf(nY1Prime).intValue());
                                rect.add(Double.valueOf(nX2Prime).intValue(), Double.valueOf(nY2Prime).intValue());
                                rect.add(Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());

                                // set last position
                                nLastX = nX;
                                nLastY = nY;
                                nLastControlX = nX2Prime;
                                nLastControlY = nY2Prime;
                                hasStartPoint = true;
                            }
                            break;
                        }

                        // #100617# relative quadratic beziers are imported as cubic
                        case 't' : bRelative = true;
                            //$FALL-THROUGH$
                        case 'T' : {

                            stringPosValue.incPos();
                            stringPosValue.skipSpaces();

                            while(stringPosValue.hasMore() && stringPosValue.isOnNumberChar(true)) {

                                double nX = stringPosValue.importDoubleAndSpaces();
                                double nY = stringPosValue.importDoubleAndSpaces();

                                if(bRelative) {
                                    nX += nLastX;
                                    nY += nLastY;
                                }

                                // ensure existence of start point
                                if(!hasStartPoint) {
                                    rect.add(Double.valueOf(nLastX).intValue(), Double.valueOf(nLastY).intValue());
                                }

                                double prevControlX = nLastX;
                                double prevControlY = nLastY;

                                if(nLastControlX!=null) {
                                    prevControlX = (2.0 * nLastX) - nLastControlX;
                                    prevControlY = (2.0 * nLastY) - nLastControlY;
                                }

                                if((prevControlX!=nLastX)&&(prevControlY!=nLastY)) {
                                    // there is a prev control point, and we have the already mirrored one
                                    // in aPrevControl. We also need the quadratic control point for this
                                    // new quadratic segment to calculate the 2nd cubic control point
                                    final double quadControlPointX = ((3.0 * prevControlX) - nLastX) / 2.0;
                                    final double quadControlPointY = ((3.0 * prevControlY) - nLastY) / 2.0;

                                    // calculate the cubic bezier coefficients from the quadratic ones.
                                    final double nX2Prime = ((quadControlPointX * 2.0 + nX) / 3.0);
                                    final double nY2Prime = ((quadControlPointY * 2.0 + nY) / 3.0);

                                    // append curved edge, use mirrored cubic control point directly
                                    rect.add(Double.valueOf(prevControlX).intValue(), Double.valueOf(prevControlY).intValue());
                                    rect.add(Double.valueOf(nX2Prime).intValue(), Double.valueOf(nY2Prime).intValue());
                                    rect.add(Double.valueOf(nX).intValue(), Double.valueOf(nY).intValue());

                                    nLastControlX = nX2Prime;
                                    nLastControlY = nY2Prime;
                                }
                                else {
                                    // when no previous control, SVG says to use current point -> straight line. Just add end point
                                    rect.add(Double.valueOf(nX).intValue(), Double.valueOf(nX).intValue());
                                    nLastControlX = null;
                                }

                                // set last position
                                nLastX = nX;
                                nLastY = nY;
                                hasStartPoint = true;
                            }
                            break;
                        }

                        case 'a' :
                        case 'A' : {
                            break;
                        }

                        default: {
                            stringPosValue.incPos();
                            break;
                        }
                    }
                }
            }
            catch(NumberFormatException e) {
                // ohoh
            }
        }
        return rect.getViewBox();
    }

    private static class Rect {

        int x1 = Integer.MAX_VALUE;
        int y1 = Integer.MAX_VALUE;
        int x2 = Integer.MIN_VALUE;
        int y2 = Integer.MIN_VALUE;

        public Rect() {}

        public void add(int x, int y) {
            addX(x);
            addY(y);
        }

        public void addX(int x) {
            if(x1>x) {
                x1 = x;
            }
            if(x2<x) {
                x2 = x;
            }
        }

        public void addY(int y) {
            if(y1>y) {
                y1 = y;
            }
            if(y2<y) {
                y2 = y;
            }
        }

        public ViewBox getViewBox() {
            return new ViewBox(x1, y1, x2-x1, y2-y1);
        }
    }

    private static class StringPosValue {

        public final String str;
        public final int len;
        public int pos;

        public StringPosValue(String str, int pos) {
            this.str = str;
            this.len = str.length();
            this.pos = pos;
        }

        public boolean hasMore() {
            return pos < len;
        }

        public void incPos() {
            pos++;
        }

        public char currentChar() {
            return str.charAt(pos);
        }

        public void skipSpaces() {
            while(pos<len) {
                if(str.charAt(pos)!=' ') {
                    break;
                }
                pos++;
            }
        }

        private static boolean isOnNumberChar(char aChar, boolean bSignAllowed, boolean bDotAllowed) {
            return ( ('0' <= aChar && '9' >= aChar)
                || (bSignAllowed && '+' == aChar)
                || (bSignAllowed && '-' == aChar)
                || (bDotAllowed && '.' == aChar));
        }

        public boolean isOnNumberChar(boolean bSignAllowed) {
            return isOnNumberChar(str.charAt(pos), bSignAllowed, true);
        }

        public double importDoubleAndSpaces() {
            double n = getDoubleChar();
            skipSpacesAndCommas();
            return n;
        }

        private void skipSpacesAndCommas() {
            while(hasMore() && (' ' == str.charAt(pos) || ',' == str.charAt(pos))) {
                pos++;
            }
        }

        private double getDoubleChar() throws NumberFormatException {

            char aChar = pos < len ? str.charAt(pos) : 0;
            final StringBuffer sNumberString = new StringBuffer();

            // sign
            if('+' == aChar || '-' == aChar) {
                sNumberString.append(str.charAt(pos));
                aChar = str.charAt(++pos);
            }

            // numbers before point
            while('0' <= aChar && '9' >= aChar) {
                sNumberString.append(str.charAt(pos++));
                aChar = pos < len ? str.charAt(pos) : 0;
            }

            // point
            if('.' == aChar) {
                sNumberString.append(str.charAt(pos++));
                aChar = pos < len ? str.charAt(pos) : 0;
            }

            // numbers after point
            while ('0' <= aChar && '9' >= aChar) {
                sNumberString.append(str.charAt(pos++));
                aChar = pos < len ? str.charAt(pos) : 0;
            }

            // 'e'
            if('e' == aChar || 'E' == aChar) {
                sNumberString.append(str.charAt(pos++));
                aChar = pos < len ? str.charAt(pos) : 0;

                // sign for 'e'
                if('+' == aChar || '-' == aChar) {
                    sNumberString.append(str.charAt(pos++));
                    aChar = pos < len ? str.charAt(pos) : 0;
                }

                // number for 'e'
                while('0' <= aChar && '9' >= aChar) {
                    sNumberString.append(str.charAt(pos++));
                    aChar = pos < len ? str.charAt(pos) : 0;
                }
            }
            if(sNumberString.length()>0) {
                return Double.parseDouble(sNumberString.toString());
            }
            throw new NumberFormatException();
        }
    }

    private static void addPoint(List<OpAttrs> commands, boolean moveTo, int x, int y) {
        final OpAttrs geometryCommand = new OpAttrs();
        geometryCommand.put(OCKey.C.value(), moveTo ? "moveTo" : "lineTo");
        geometryCommand.put(OCKey.X.value(), x);
        geometryCommand.put(OCKey.Y.value(), y);
        commands.add(geometryCommand);
    }
}
