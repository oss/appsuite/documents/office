/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xerces.impl.dv.util.Base64;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.odftoolkit.odfdom.pkg.manifest.OdfFileEntry;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Length;
import com.openexchange.office.filter.odf.Length.Unit;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.GraphicProperties;
import com.openexchange.office.filter.odf.styles.IGraphicProperties;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.imagemgr.Resource;

public class DrawImage implements IDrawing {

	private final DrawFrame drawFrame;
	private final AttributesImpl attributes;
    private final DLList<Object> childs = new DLList<Object>();

	public DrawImage(DrawFrame drawFrame) {
		this.drawFrame = drawFrame;
		this.attributes = new AttributesImpl();
	}

	public DrawImage(DrawFrame drawFrame, Attributes attributes) {
		this.drawFrame = drawFrame;
		this.attributes = new AttributesImpl(attributes);
	}

	@Override
	public DLList<Object> getContent() {
		return childs;
	}

    @Override
    public AttributesImpl getAttributes() {
        return attributes;
    }

    @Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.DRAW, "image", "draw:image");
		attributes.write(output);
		final Iterator<Object> childIter = childs.iterator();
		while(childIter.hasNext()) {
		    ((IElementWriter)childIter.next()).writeObject(output);
		}
		SaxContextHandler.endElement(output, Namespaces.DRAW, "image", "draw:image");
	}

	@Override
	public DrawingType getType() {
		return DrawingType.IMAGE;
	}

	@Override
	public boolean preferRepresentation() {
	    return true;
	}

	public String getFormat() {
	    final String href = attributes.getValue("xlink:href");
	    return href!=null&&href.length()>3 ? href.substring(href.length()-3) : "";
	}

	@Override
	public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle) {
		final JSONObject imageProps = attrs.optJSONObject(OCKey.IMAGE.value());
		if(imageProps==null) {
			return;
		}
		final String href = imageProps.optString(OCKey.IMAGE_URL.value(), null);
		if(href!=null) {
    		if(transferResource(href, operationDocument.getDocument().getStyleManager())) {
                attributes.setValue(Namespaces.XLINK, "type", "xlink:type", "simple");
                final Integer height = drawFrame.getAttributes().getIntValue("svg:height");
                final Integer width = drawFrame.getAttributes().getIntValue("svg:widtht");
                if(height==null||width==null) {
                    try {
                        final BufferedImage image = ImageIO.read(operationDocument.getDocument().getPackage().getInputStream(href));
                        if(image!=null) {
                            drawFrame.getAttributes().setValue(Namespaces.SVG, "width", "svg:width", Length.mapToUnit(String.valueOf(image.getWidth(null)) + "px", Unit.CENTIMETER));
                            drawFrame.getAttributes().setValue(Namespaces.SVG, "height", "svg:height", Length.mapToUnit(String.valueOf(image.getHeight(null)) + "px", Unit.CENTIMETER));
                        }
                    }
                    catch(IOException ohoh) {
                        //
                    }
                }
    		}
            attributes.setValue(Namespaces.XLINK, "href", "xlink:href", href);
		}
	    else if (imageProps.has(OCKey.IMAGE_DATA.value())) {
            String imageData = imageProps.optString(OCKey.IMAGE_DATA.value());
            if (imageData != null && !imageData.isEmpty()) {
                //expected header is
                //		"data:image/png;base64,
                String[] header = imageData.split("base64,");
                String suffix = "png";
                String mediaTypeString = "image/png";
                mediaTypeString = header[0].substring(header[0].indexOf(":") + 1, header[0].indexOf(";"));
                suffix = header[0].substring(header[0].indexOf("/") + 1, header[0].indexOf(";"));
                String fileName = "img_" + new Random().nextInt() + "." + suffix;
                operationDocument.getDocument().getPackage().insert(Base64.decode(header[1]), "Pictures/" + fileName, mediaTypeString);
                attributes.setValue(Namespaces.XLINK, "href", "xlink:href", "Pictures/" + fileName);
            }
        }
		final Object cropLeft = imageProps.opt(OCKey.CROP_LEFT.value());
		final Object cropTop = imageProps.opt(OCKey.CROP_TOP.value());
		final Object cropRight = imageProps.opt(OCKey.CROP_RIGHT.value());
		final Object cropBottom = imageProps.opt(OCKey.CROP_BOTTOM.value());

		if(cropLeft!=null||cropTop!=null||cropRight!=null||cropBottom!=null) {
		    applyCropAttrs(operationDocument, contentAutoStyle, cropLeft, cropTop, cropRight, cropBottom);
		}
        if (imageProps.has("imageXmlId")) {
            String xmlId = imageProps.optString("imageXmlId");
            if (xmlId != null && !xmlId.isEmpty()) {
            	attributes.setValue(Namespaces.XML, "id", "xml:id", xmlId);
            }
        }
	}

	@Override
	public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
		final String hRef = attributes.getValue("xlink:href");
		if(hRef!=null) {
			final Map<String, Object> imageProps = attrs.getMap(OCKey.IMAGE.value(), true);
            imageProps.put(OCKey.IMAGE_URL.value(), hRef);
            if (imageProps.containsKey(OCKey.CROP_LEFT.value())||imageProps.containsKey(OCKey.CROP_TOP.value())||imageProps.containsKey(OCKey.CROP_RIGHT.value())||imageProps.containsKey(OCKey.CROP_BOTTOM.value())) {
            	createCropAttrs(operationDocument, imageProps);
            }
        }
		final String id = attributes.getValue("xml:id");
		if(id!=null) {
			attrs.getMap(OCKey.DRAWING.value(), true).put("imageXmlId", id);
        }
		// fix for 58490, in odf the default for the "auto-grow-height" attribute in graphic styles is true,
		// but images in frames are not supporting this attribute.
		attrs.getMap(OCKey.SHAPE.value(), true).remove(OCKey.AUTO_RESIZE_HEIGHT.value());
	}

	public static boolean transferResource(String href, StyleManager styleManager) {
        if(href!=null&&!href.isEmpty()) {
            if (href.contains(Resource.RESOURCE_UID_PREFIX)) {
                int uidStart = href.indexOf(Resource.RESOURCE_UID_PREFIX) + Resource.RESOURCE_UID_PREFIX.length();
                if(uidStart!=3&&uidStart<href.length()&&uidStart<href.indexOf('.')) {
                    String uidString = null;
                    if (href.contains(".")) {
                        uidString = href.substring(uidStart, href.indexOf('.'));
                    } else {
                        uidString = href.substring(uidStart);
                    }
                    if(uidString!=null) {
                        final String hash = uidString;
                        final Map<String, byte[]> resourceMap = styleManager.getResourceMap();
                        if(resourceMap!=null) {
                            final byte[] fileBytes = resourceMap.get(hash);
                            if(fileBytes!=null) {
                                try {
                                    styleManager.getPackage().insert(fileBytes, href, OdfFileEntry.getMediaTypeString(href));
                                    return true;
                                }
                                catch(Exception ohoh) {
                                    //
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
	}

	private void createCropAttrs(OdfOperationDoc operationDocument, Map<String, Object> imageProps) {
	    final Pair<Double, Double> size = getLogicalBitmapSize(operationDocument);
	    if(size!=null) {
            double width = size.getLeft();
            double height = size.getRight();

        	// 2nd half of absolute fo:clip to relative crop (OX API) mapping
            if (imageProps.containsKey(OCKey.CROP_RIGHT.value())) {
                Number cropRight = (Number) imageProps.get(OCKey.CROP_RIGHT.value());
                if (cropRight != null) {
                    if (cropRight.doubleValue() != 0.0) {
                        imageProps.put(OCKey.CROP_RIGHT.value(), cropRight.doubleValue() * 100.0 / width);
                    } else {
                        // do not set explicitly with 0
                        imageProps.remove(OCKey.CROP_RIGHT.value());
                    }
                }
            }
            if (imageProps.containsKey(OCKey.CROP_LEFT.value())) {
                Number cropLeft = (Number) imageProps.get(OCKey.CROP_LEFT.value());
                if (cropLeft != null) {
                    if (cropLeft.doubleValue() != 0.0) {
                        imageProps.put(OCKey.CROP_LEFT.value(), cropLeft.doubleValue() * 100.0 / width);
                    } else {
                        // do not set explicitly with 0
                        imageProps.remove(OCKey.CROP_LEFT.value());
                    }
                }
            }
            // 2nd half of absolute fo:clip to relative crop (OX API) mapping
            if (imageProps.containsKey(OCKey.CROP_TOP.value())) {
                Number cropTop = (Number) imageProps.get(OCKey.CROP_TOP.value());
                if (cropTop != null) {
                    if (cropTop.doubleValue() != 0.0) {
                        imageProps.put(OCKey.CROP_TOP.value(), cropTop.doubleValue() * 100.0 / height);
                    } else {
                        // do not set explicitly with 0
                        imageProps.remove(OCKey.CROP_TOP.value());
                    }
                }
            }
            if (imageProps.containsKey(OCKey.CROP_BOTTOM.value())) {
                Number cropBottom = (Number) imageProps.get(OCKey.CROP_BOTTOM.value());
                if (cropBottom != null) {
                    if (cropBottom.doubleValue() != 0.0) {
                        imageProps.put(OCKey.CROP_BOTTOM.value(), cropBottom.doubleValue() * 100.0 / height);
                    } else {
                        // do not set explicitly with 0
                        imageProps.remove(OCKey.CROP_BOTTOM.value());

                    }
                }
            }
	    }
    }

	private void applyCropAttrs(OdfOperationDoc operationDocument, boolean contentAutoStyle, Object cropLeft, Object cropTop, Object cropRight, Object cropBottom) {
	    final Pair<Double, Double> size = getLogicalBitmapSize(operationDocument);
	    if(size!=null) {
	        final double width = size.getLeft().doubleValue();
	        final double height = size.getRight().doubleValue();
	        final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
	        final StyleBase graphicStyleBase;
	        if(drawFrame.isPresentationStyle()) {
	            graphicStyleBase = styleManager.getStyleBaseClone(StyleFamily.PRESENTATION, drawFrame.getPresentationStyleName(), contentAutoStyle);
	        }
	        else {
                graphicStyleBase = styleManager.getStyleBaseClone(StyleFamily.GRAPHIC, drawFrame.getStyleName(), contentAutoStyle);
	        }
	        final GraphicProperties graphicProperties = ((IGraphicProperties)graphicStyleBase).getGraphicProperties();

	        // foClip == top, right, bottom, left :-)
	        int[] foClip = graphicProperties.getFoClip();
	        if(foClip==null) {
	            foClip = new int[4];
	            foClip[0] = foClip[1] = foClip[2] = foClip[3] = 0;
	        }
	        if(cropLeft!=null) {
	            if(cropLeft instanceof Number) {
	                foClip[3] = Double.valueOf((((Number)cropLeft).doubleValue() / 100.0) * width).intValue();
	            }
	            else {
	                foClip[3] = 0;
	            }
	        }
	        if(cropTop!=null) {
	            if(cropTop instanceof Number) {
                    foClip[0] = Double.valueOf((((Number)cropTop).doubleValue() / 100.0) * height).intValue();
	            }
	            else {
	                foClip[0] = 0;
	            }
	        }
	        if(cropRight!=null) {
	            if(cropRight instanceof Number) {
                    foClip[1] = Double.valueOf((((Number)cropRight).doubleValue() / 100.0) * width).intValue();
	            }
	            else {
	                foClip[1] = 0;
	            }
	        }
	        if(cropBottom!=null) {
	            if(cropBottom instanceof Number) {
                    foClip[2] = Double.valueOf((((Number)cropBottom).doubleValue() / 100.0) * height).intValue();
	            }
	            else {
	                foClip[2] = 0;
	            }
	        }
	        graphicProperties.setFoClip(foClip);

	        if(drawFrame.isPresentationStyle()) {
	            drawFrame.setPresentationStyleName(styleManager.getStyleIdForStyleBase(graphicStyleBase));
	        }
	        else {
                drawFrame.setStyleName(styleManager.getStyleIdForStyleBase(graphicStyleBase));
	        }
	    }
	}

	private Pair<Double, Double> getLogicalBitmapSize(OdfOperationDoc operationDocument) {

	    Pair<Double, Double> originalSize = null;

	    final Double originalWidth = getAttributes().getDoubleValue("loext:original-width");
        final Double originalHeight = getAttributes().getDoubleValue("loext:original-height");
        if(originalWidth!=null&&originalHeight!=null) {
            originalSize = Pair.of(originalWidth, originalHeight);
        }
        else {

            final OdfPackage pkg = operationDocument.getDocument().getPackage();
            final String hRef = attributes.getValue("xlink:href");
            if(hRef!=null&&!hRef.isEmpty()) {
                final InputStream is = pkg.getInputStream(hRef);
                if (is!=null) {
                    try {
                        originalSize = Tools.getOriginalSizeFromImage(is);
                        is.close();
                    } catch (Exception e) {
                        //
                    }
                }
            }
        }

        if(originalSize!=null) {
            getAttributes().setValue(Namespaces.LOEXT, "original-width", "loext:original-width", originalSize.getLeft().toString());
            getAttributes().setValue(Namespaces.LOEXT, "original-height", "loext:original-height", originalSize.getRight().toString());
        }
        return originalSize;
	}
}
