/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.util.Map;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;

public class DrawTextBox implements IDrawing, INodeAccessor {

    private final DrawFrame drawFrame;
	private final AttributesImpl attributes;
	private final DLList<Object> childs = new DLList<Object>();

	/* for title and body master placeholder objects the filter is replacing
	 * the empty textbox with the proper translation, then this flag is used
	 * to indicate that the content is not to be saved */
	private boolean saveContent = true;

	public DrawTextBox(DrawFrame drawFrame) {
        this.drawFrame = drawFrame;
		attributes = new AttributesImpl();
	}

	DrawTextBox(DrawFrame drawFrame, Attributes attributes) {
	    this.drawFrame = drawFrame;
		this.attributes = new AttributesImpl(attributes);
	}

	@Override
    public DLList<Object> getContent() {
		return childs;
	}

	public void disableSaveContent() {
	    saveContent = false;
	}

	public boolean isSaveContent() {
	    return saveContent;
	}

    @Override
    public AttributesImpl getAttributes() {
        return attributes;
    }

    @Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.DRAW, "text-box", "draw:text-box");
		if(saveContent) {
    		attributes.write(output);
    		TextContentHelper.write(output, getContent());
		}
		SaxContextHandler.endElement(output, Namespaces.DRAW, "text-box", "draw:text-box");
	}

	@Override
	public DrawingType getType() {
		return DrawingType.SHAPE;
	}

	@Override
	public boolean preferRepresentation() {    // (docs-4405) normally the textBox should be taken because it is more high level than a image, but
	    return getContent().isEmpty();         // sometimes LO is writing empty text boxes beside images in this case the image makes more sense
	}

	@Override
	public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle) {
	    if(!contentAutoStyle) {
            final JSONObject presentationProps = attrs.optJSONObject(OCKey.PRESENTATION.value());
            if(presentationProps!=null) {
                final Object phType = presentationProps.opt(OCKey.PH_TYPE.value());
                if("outline".equals(phType)||"body".equals(phType)) {
                    disableSaveContent();
                }
            }
	    }
	}

	@Override
	public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
	    // a draw:text-box does not support word wrapping even if this attribute is set via graphic styles
	    attrs.getMap(OCKey.SHAPE.value(), true).put(OCKey.WORD_WRAP.value(), true);
        final String minHeight = attributes.getValue("fo:min-height");
        if(minHeight!=null) {
            attrs.getMap(OCKey.DRAWING.value(), true).put(OCKey.MIN_FRAME_HEIGHT.value(), AttributesImpl.normalizeLength(minHeight));
        }
		final Map<String, Object> drawingProps = attrs.getMap(OCKey.DRAWING.value(), false);
		if(drawingProps!=null) {
		    final Object h = drawingProps.get(OCKey.HEIGHT.value());
		    if(h==null||((h instanceof Number)&&((Number)h).intValue()==1)) {
			    attrs.getMap(OCKey.SHAPE.value(), true).put(OCKey.AUTO_RESIZE_HEIGHT.value(), true);
			}
		}
		return;
	}
}
