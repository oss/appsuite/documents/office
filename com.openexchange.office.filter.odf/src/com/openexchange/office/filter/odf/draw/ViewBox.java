/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import org.apache.commons.lang3.StringUtils;

public class ViewBox {

    private int x;
    private int y;
    private int width;
    private int height;

    public ViewBox(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public ViewBox(String viewBox) {
        x = 0;
        y = 0;
        width = 0;
        height = 0;

        final String[] viewBoxParams = StringUtils.split(viewBox);
        if(viewBoxParams!=null&&viewBoxParams.length==4) {
            try {
                x = Integer.parseInt(viewBoxParams[0]);
                y = Integer.parseInt(viewBoxParams[1]);
                width = Integer.parseInt(viewBoxParams[2]);
                height = Integer.parseInt(viewBoxParams[3]);
            }
            catch(NumberFormatException e) {
                // ohoh
            }
        }
    }

    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append(Integer.valueOf(x).toString())
              .append(' ')
              .append(Integer.valueOf(y).toString())
              .append(' ')
              .append(Integer.valueOf(width).toString())
              .append(' ')
              .append(Integer.valueOf(height).toString());
        return buffer.toString();
    }

    public boolean isValid() {
        return width>0&&height>0;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
