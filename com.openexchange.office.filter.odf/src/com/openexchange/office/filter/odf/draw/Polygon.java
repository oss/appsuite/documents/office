/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;

public class Polygon extends Shape {

    public Polygon(AttributesImpl attributes, String uri, String localName, String qName, GroupShape parentGroup, boolean rootShape, boolean contentStyle) {
        super(attributes, uri, localName, qName, parentGroup, rootShape, contentStyle);
    }

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
        super.createAttrs(operationDocument, attrs, contentAutoStyle);

        final OpAttrs geometryAttrs = attrs.getMap(OCKey.GEOMETRY.value(), true);
        final List<OpAttrs> pathList = new ArrayList<OpAttrs>(1);
        geometryAttrs.put(OCKey.PATH_LIST.value(), pathList);

        final String drawPoints = attributes.getValue("draw:points");
        final String[] tokens = StringUtils.split(drawPoints);
        if(tokens!=null&&tokens.length>0) {
            final ViewBox viewBox = new ViewBox(attributes.getValue("svg:viewBox"));
            if(viewBox.isValid()) {
                final OpAttrs path = new OpAttrs();
                path.put(OCKey.WIDTH.value(), viewBox.getWidth());
                path.put(OCKey.HEIGHT.value(), viewBox.getHeight());
                if("polyline".equals(localName)) {
                    path.put(OCKey.FILL_MODE.value(), "none");
                }
                pathList.add(path);

                final boolean closed = "draw:polygon".equals(qName);
                int tokenCount = tokens.length;
                if(closed) {
                    tokenCount++;
                }
                final List<OpAttrs> commands = new ArrayList<OpAttrs>(tokenCount);
                path.put(OCKey.COMMANDS.value(), commands);
                String c = "moveTo";
                for(String token:tokens) {
                    final OpAttrs geometryCommand = new OpAttrs();
                    final String[] points = StringUtils.split(token, ',');
                    if(points!=null&&points.length==2) {
                        int x = 0;
                        int y = 0;
                        try {
                            x = Integer.parseInt(points[0]) - viewBox.getX();
                            y = Integer.parseInt(points[1]) - viewBox.getY();
                        }
                        catch(NumberFormatException e) {
                            // ohoh
                        }
                        geometryCommand.put(OCKey.X.value(), x);
                        geometryCommand.put(OCKey.Y.value(), y);
                        geometryCommand.put(OCKey.C.value(), c);
                    }
                    commands.add(geometryCommand);
                    c = "lineTo";
                }
                if(closed) {
                    final OpAttrs geometryCommand = new OpAttrs();
                    geometryCommand.put(OCKey.C.value(), "close");
                    commands.add(geometryCommand);
                }
            }
        }
    }
}
