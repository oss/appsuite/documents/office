/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.odftoolkit.odfdom.doc.OdfChartDocument;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.core.PresentationObjectTitles;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.odp.dom.PresentationStyles;
import com.openexchange.office.filter.odf.ods.dom.chart.ChartContent;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;
import com.openexchange.office.filter.odf.odt.dom.Text;
import com.openexchange.office.filter.odf.odt.dom.TextList;
import com.openexchange.office.filter.odf.odt.dom.TextListItem;
import com.openexchange.office.filter.odf.odt.dom.TextSpan;
import com.openexchange.office.filter.odf.table.Table;
import com.openexchange.office.filter.odf.table.TableHandler;

public class DrawFrameHandler extends SaxContextHandler {

	final DrawFrame drawFrame;

	public DrawFrameHandler(SaxContextHandler parentContext, DrawFrame drawFrame) {
    	super(parentContext);

    	this.drawFrame = drawFrame;
	}

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("draw:image")) {
    		final DrawImage drawImage = new DrawImage(drawFrame, attributes);
    		drawFrame.addContent(drawImage);
    		return new DrawImageHandler(this, drawImage);
    	}
    	else if(qName.equals("draw:object")) {
    	    final DrawObject drawObject = new DrawObject(drawFrame, attributes);
    	    drawFrame.addContent(drawObject);
            String documentPath = attributes.getValue("xlink:href");
            if (StringUtils.isNotEmpty(documentPath)) {
                String media = getFileDom().getDocument().getPackage().getMediaTypeString(documentPath);
                if ("application/vnd.oasis.opendocument.chart".equals(media)) {
                    try {
                        final OdfChartDocument subDoc = (OdfChartDocument) getFileDom().getDocument().loadSubDocument(documentPath);
                        subDoc.getStylesDom();
                        final ChartContent chart = (ChartContent) subDoc.getContentDom();
                        drawObject.setChart(chart);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            return new DrawObjectHandler(this, drawObject);
        }
    	else if(qName.equals("draw:text-box")) {
    		final DrawTextBox drawTextBox = new DrawTextBox(drawFrame, attributes);
    		drawFrame.addContent(drawTextBox);
    		// replacing presentation placeholder on master page with translated text content
    		if(drawFrame.getAttributes().getBooleanValue("presentation:placeholder", false) && getFileDom() instanceof PresentationStyles) {
    		    final String presentationClass = drawFrame.getAttributes().getValue("presentation:class");
    		    if("title".equals(presentationClass)||"outline".equals(presentationClass)) {
    		        drawTextBox.disableSaveContent();
                    final Map<String, Object> configuration = getFileDom().getDocument().getPackage().getRunTimeConfiguration();
                    final Object o = configuration.get(DocumentProperties.PROP_USER_LANGUAGE);
                    final String userLanguage = o instanceof String ? (String)o : "en";
    		        if("title".equals(presentationClass)) {
    		            final Paragraph paragraph = new Paragraph(null);
    		            drawTextBox.getContent().add(paragraph);
    		            final TextSpan textSpan = new TextSpan();
    		            paragraph.getContent().add(textSpan);
    		            textSpan.getContent().add(new Text(PresentationObjectTitles.getTranslation("title", userLanguage, "Title")));
    		        }
    		        else {
    		            for(int i=1; i<=5; i++) {
                            final Paragraph paragraph = new Paragraph(new TextListItem(new TextList(getFileDom().getDocument().getStyleManager(), null)));
                            paragraph.setListLevel(getFileDom().getDocument().getStyleManager(), i-1);
                            drawTextBox.getContent().add(paragraph);
                            final TextSpan textSpan = new TextSpan();
                            paragraph.getContent().add(textSpan);
                            textSpan.getContent().add(new Text(PresentationObjectTitles.getTranslation("level" + Integer.valueOf(i).toString(), userLanguage, "")));
    		            }
    		        }
    		        // dummy content handler
                    return new UnknownContentHandler(this, new ElementNSWriter(getFileDom(), attributes, uri, qName));
    		    }
    		}
    		return new DrawTextBoxHandler(this, drawTextBox);
    	}
    	else if(qName.equals("table:table")) {
    	    final Table table = new Table(((OdfDocument)getFileDom().getDocument()).getDocumentType(), attributes);
    	    drawFrame.addContent(table);
    	    return new TableHandler(this, table);
    	}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	drawFrame.addContent(element);
		return new UnknownContentHandler(this, element);
    }
}
