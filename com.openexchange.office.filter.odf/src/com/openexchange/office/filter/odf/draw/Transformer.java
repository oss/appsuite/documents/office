/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.draw;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.odftoolkit.odfdom.doc.OdfDocument;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class Transformer {

    private double x;
    private double y;
    private double width;
    private double height;
    private Double rotation;
    private boolean isLineMode;

    // flipping is saved in the styles, this is a very good idea :(
    // so when loading we do not have enough information to make proper
    // translations. If createAttributes or applyAttributes is called at
    // the object, then the initialized method should be called once.

    private boolean initialized;
    private Boolean flipH;
    private Boolean flipV;

    // before saving the transformation attributes needs to be created
    public Transformer(boolean isLineMode) {
        this.isLineMode = isLineMode;
        initialized = true;
    }

    // if not modified then the attributes of the shape can be written without modification
    public Transformer(AttributesImpl attributes, boolean isLineMode) {
        this.isLineMode = isLineMode;
        initialized = false;

        if(isLineMode) {
            init(attributes.getValue("svg:x1"), attributes.getValue("svg:y1"), attributes.getValue("svg:x2"), attributes.getValue("svg:y2"), attributes.getValue("draw:transform"), true);
        }
        else {
            init(attributes.getValue("svg:x"), attributes.getValue("svg:y"), attributes.getValue("svg:width"), attributes.getValue("svg:height"), attributes.getValue("draw:transform"), false);
        }
    }

    public void init(String sX, String sY, String sWidth, String sHeight, String sTransformation, boolean sIsLineMode) {
        x = sX != null ? AttributesImpl.normalizeLength(sX, false) : 0.0d;
        y = sY != null ? AttributesImpl.normalizeLength(sY, false) : 0.0d;
        width = sWidth != null ? AttributesImpl.normalizeLength(sWidth, false) : 1.0d;
        height = sHeight != null ? AttributesImpl.normalizeLength(sHeight, false) : 1.0d;
        rotation = null;
        if(isLineMode = sIsLineMode) {
            width -= x;
            height -= y;
        }
        if(sTransformation!=null) {
            double centerX = width * 0.5d + x;
            double centerY = height * 0.5d + y;
            final List<Pair<String, List<Double>>> transformations = getTransformations(sTransformation);
            for(Pair<String, List<Double>> entry:transformations) {
                final List<Double> values = entry.getValue();
                switch(entry.getKey()) {
                    case "rotate" : {
                        if(!values.isEmpty()) {
                            final double alpha = -values.get(0);
                            rotation = (alpha * 180.0d) / Math.PI;
                            if(rotation!=0.0d) {
                                final double x2 = centerX * Math.cos(alpha) - centerY * Math.sin(alpha);
                                final double y2 = centerY * Math.cos(alpha) + centerX * Math.sin(alpha);
                                centerX = x2;
                                centerY = y2;
                            }
                        }
                        break;
                    }
                    case "skewX" : {
                        break;
                    }
                    case "skewY" : {
                        break;
                    }
                    case "matrix" : {
                        break;
                    }
                    case "scale" : {
                        break;
                    }
                    case "translate" : {
                        if(values.size()>=1) {
                            centerX += values.get(0);
                        }
                        if(values.size()>=2) {
                            centerY += values.get(1);
                        }
                        break;
                    }
                }
            }
            x = centerX - width * 0.5d;
            y = centerY - height * 0.5d;
        }
        if(isLineMode) {
            if(width<0) {
                x += width;
                width = -width;
                flipH = Boolean.TRUE;
            }
            if(height<0) {
                y += height;
                height = -height;
                flipV = Boolean.TRUE;
            }
        }
    }

    private List<Pair<String, List<Double>>> getTransformations(String sTransformation) {
        final List<Pair<String, List<Double>>> transformations = new ArrayList<Pair<String, List<Double>>>();
        int index = 0;
        while(index>=0) {
            index = getNextTransformationEntry(transformations, index, sTransformation);
        }
        return transformations;
    }

    private int getNextTransformationEntry(List<Pair<String, List<Double>>> transformations, int index, String sTransformation) {
        if(index<0) {
            return -1;
        }
        final int o = sTransformation.indexOf('(', index + 1);
        if(o<0) {
            return -1;
        }
        final int c = sTransformation.indexOf(')', o + 2);
        if(c<0) {
            return -1;
        }
        final String command = sTransformation.substring(index, o).trim();
        if(!command.isEmpty()) {
            final Pair<String, List<Double>> pair = new ImmutablePair<String, List<Double>>(command, new ArrayList<Double>());
            final String[] parameters = sTransformation.substring(o+1, c).split("\\s+", -1);
            for(String p:parameters) {
                Double v = 0.0d;
                try {
                    if(command.equals("translate")) {
                        v = AttributesImpl.normalizeLength(p, false).doubleValue();
                    }
                    else {
                        v = Double.parseDouble(p);
                    }
                }
                catch(NumberFormatException e) {
                    //
                }
                pair.getValue().add(v);
            }
            transformations.add(pair);
        }
        if(c+1==sTransformation.length()) {
            return -1;
        }
        return c + 1;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public Rectangle2D getRect2D() {
        return new Rectangle2D.Double(x, y, width, height);
    }

    public Double getRotation() {
        return rotation;
    }

    public Boolean getFlipH() {
        return flipH;
    }

    public Boolean getFlipV() {
        return flipV;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setRect(Rectangle2D rect)  {
        this.x = rect.getX();
        this.y = rect.getY();
        this.width = rect.getWidth();
        this.height = rect.getHeight();
    }

    public double getCenterX() {
        return x + width * 0.5;
    }

    public double getCenterY() {
        return y + height * 0.5;
    }

    public void setRotation(Double sRotation) {
        rotation = sRotation;
    }

    public void setFlipH(Boolean flip) {
        this.flipH = flip;
    }

    public void setFlipV(Boolean flip) {
        this.flipV = flip;
    }

    public void setLineMode(boolean isLineMode) {
        this.isLineMode = isLineMode;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitilized(boolean inititalized) {
        this.initialized = inititalized;
    }

    public boolean isLineMode() {
        return isLineMode;
    }

    public static void initialize(OdfOperationDoc operationDocument, Shape shape) {
        if(!shape.getTransformer().isInitialized()) {
            final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
            GroupShape groupShape = shape.getParentGroup();
            if(groupShape==null&&shape instanceof GroupShape) {
                groupShape = (GroupShape)shape;
            }
            if(groupShape!=null) {
                while(groupShape.getParentGroup()!=null) {
                    groupShape = groupShape.getParentGroup();
                }
                groupShape.initializeTopLevelGroupShape(styleManager);
            }
            else {
                shape.initializeShape(styleManager);
            }
        }
    }

    public static void prepareSave(OdfOperationDoc operationDocument)
        throws JSONException {

        final OdfDocument doc = operationDocument.getDocument();
        final StyleManager styleManager = doc.getStyleManager();
        final Set<Shape> modifiedShapes = doc.getModifiedShapes();

        final Iterator<Shape> shapeIter = modifiedShapes.iterator();
        while(shapeIter.hasNext()) {
            final Shape shape = shapeIter.next();
            final Transformer transformer = shape.getTransformer();
            if(transformer.isInitialized()) {
                GroupShape groupShape = shape.getParentGroup();
                if(groupShape==null&&shape instanceof GroupShape) {
                    groupShape = (GroupShape)shape;
                }
                if(groupShape!=null) {
                    groupShape.convertConnectorToPath(operationDocument, groupShape.isContentStyle());
                    while(groupShape.getParentGroup()!=null) {
                        groupShape = groupShape.getParentGroup();
                    }
                    groupShape.prepareSaveTopLevelGroupShape(styleManager);
                }
                else {
                    shape.prepareSaveShape(styleManager);
                }
                transformer.setInitilized(false);
            }
        }
        modifiedShapes.clear();
    }
}
