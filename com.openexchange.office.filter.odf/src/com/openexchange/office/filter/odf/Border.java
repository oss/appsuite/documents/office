/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;

public class Border implements Cloneable {

    // 100thmm
	private Integer width;
    // rgb color value
    private String color;
    // fo:style
    private String style;

    public Border() {
    	width = null;
    	color = null;
    	style = null;
    }

    public boolean isBorder() {
    	return style!=null;
    }

    @Override
	public Border clone() throws CloneNotSupportedException {
		return (Border)super.clone();
	}

    public HashMap<String, String> getColorMap() {
    	if(color==null) {
    		return null;
    	}
    	final HashMap<String, String> colorMap = new HashMap<String, String>(2);
        colorMap.put(OCKey.TYPE.value(), "rgb");
        colorMap.put(OCKey.VALUE.value(), color);
    	return colorMap;
    }

    public void setColor(JSONObject color)
    	throws JSONException {

    	if(color==null) {
    	    return;
    	}
    	else if(color==JSONObject.NULL) {
    		this.color = null;
    	}
    	else {
            String type = color.getString(OCKey.TYPE.value());
            if (type.equals("auto")) {
                this.color = "000000";
            } else if (type.equals("rgb")) {
                this.color = color.getString(OCKey.VALUE.value());
            } else if (color.has(OCKey.FALLBACK_VALUE.value())) {
            	this.color = color.getString(OCKey.FALLBACK_VALUE.value());
            }
            else {
            	this.color = null;
            }
    	}
    }

    // returns border width in 100th/mm or zero
    public Integer getWidth() {
    	return width;
    }

    public void setWidth(Integer width) {
    	this.width = width;
    }

    // returns the style (as specified for the operations) or null if no border
    public String getStyle() {
    	if(style==null) {
    		return null;
    	}
    	switch(style) {
            case "dotted" : return "dotted";
            case "dashed" : return "dashed";
            case "groove" :
            case "ridge" :
            case "inset" :
            case "outset" :
            case "solid" : return "single";
            case "double" : return "double";
            default:
            case "none" :
            case "hidden" : return "none";
    	}
    }

    public void setStyle(String style) {
    	if(style==null) {
    		this.style = "none";
    	}
    	else switch(style) {
    		default:
    		case "none": this.style = "none"; break;
    		case "single": this.style = "solid"; break;
    		case "double": this.style = "double"; break;
    		case "dotted": this.style = "dotted"; break;
    		case "dashDot":
    		case "dashDotDot":
    		case "dashed": this.style = "dashed"; break;
    	}
    }

    public void applyDrawStroke(AttributesImpl attrs) {
        final String stroke = attrs.getValue("draw:stroke");
        if(stroke!=null) {
            if(stroke.equals("none")) {
                style = "none";
            }
            else if(stroke.equals("solid")) {
                style = "solid";
            }
            else if(stroke.equals("dash")) {
                style = "dashed";
            }
        }
        final String w = attrs.getValue("svg:stroke-width");
        if(w!=null) {
            setWidth(AttributesImpl.normalizeLength(w));
        }
        final String c = attrs.getValue("svg:stroke-color");
        if(c!=null&&c.length()==7) {
            color = c.substring(1);
        }
    }

    public void applyFoBorder(String foBorder) {
        if(foBorder==null||foBorder.isEmpty()) {
            return;
        }
        final String[] tokens = foBorder.split("\\s+");
        for(String token:tokens) {
            switch(token) {
                // WIDTH
                case "thin" : {             // A thin border    1px
                    width = Integer.valueOf(35);
                    continue;
                }
                case "medium" : {           // A medium border  3px
                    width = Integer.valueOf(106);
                    continue;
                }
                case "thick" : {            // A thick border   5px
                    width = Integer.valueOf(176);
                    continue;
                }

                // STYLE
                case "none" :               // No border; the computed border width is zero.
                case "hidden" :             // Same as 'none', except in terms of border conflict resolution for table elements.
                case "dotted" :             // A series of dots.
                case "dashed" :             // A series of short line segments.
                case "solid" :              // A single line segment.
                case "double" :             // Two solid lines. The sum of the two lines and the space between them equals the value of 'border-width'.
                case "groove" :             // The border looks as though it were carved into the canvas.
                case "ridge" :              // The opposite of 'groove': the border looks as though it were coming out of the canvas.
                case "inset" :              // The border makes the box look as though it were embedded in the canvas.
                case "outset" : {           // The opposite of 'inset': the border makes the box look as though it were coming out of the canvas.
                    style = token;
                    continue;
                }

                // COLOR
                case "inherit" :
                case "transparent" : {      // The border is transparent (though it may have width).
                    color = null;
                    continue;
                }
                case "aqua" : {
                    color = "00FFFF";
                    continue;
                }
                case "black" : {
                    color = "000000";
                    continue;
                }
                case "blue" : {
                    color = "0000FF";
                    continue;
                }
                case "fuchsia" : {
                    color = "FF00FF";
                    continue;
                }
                case "gray" : {
                    color = "808080";
                    continue;
                }                    
                case "green" : {
                    color = "008000";
                    continue;
                }
                case "lime" : {
                    color = "00FF00";
                    continue;
                }
                case "maroon" : {
                    color = "800000";
                    continue;
                }
                case "navy" : {
                    color = "000080";
                    continue;
                }
                case"olive" : {
                    color = "808000";
                    continue;
                }
                case "purple" : {
                    color = "800080";
                    continue;
                }
                case "red" : {
                    color = "FF0000";
                    continue;
                }
                case "silver" :  {
                    color = "C0C0C0";
                    continue;
                }
                case "teal" : {
                    color = "008080";
                    continue;
                }
                case "white": {
                    color = "FFFFFF";
                    continue;
                }
                case "yellow" : {
                    color = "FFFF00";
                    continue;
                }
            }

            // check for a color
            if(token.startsWith("#")) {
                if(token.length()==7) {
                    color = token.substring(1).toUpperCase();
                }
                continue;
            }

            try {
                width = AttributesImpl.normalizeLength(token);
            }
            catch(NumberFormatException e) {
                //
            }
        }
    }

    public void applyJsonBorder(JSONObject borderAttrs)
    	throws JSONException {

    	setStyle(borderAttrs.optString(OCKey.STYLE.value(), null));
        final Object w = borderAttrs.opt(OCKey.WIDTH.value());
        setWidth(w instanceof Number ? ((Number)w).intValue() : null);
        setColor(borderAttrs.optJSONObject(OCKey.COLOR.value()));
    }

    public void applyJsonLine(JSONObject lineAttrs)
    	throws JSONException {

    	if(lineAttrs==null||lineAttrs==JSONObject.NULL) {
    		setStyle("none");
    	}
    	else {
    		final Object type = lineAttrs.opt(OCKey.TYPE.value());
    		if(!(type instanceof String)||((String)type).equals("none")) {
    			setStyle("none");
    		}
    		else {
    			final Object s = lineAttrs.opt(OCKey.STYLE.value());
    			if(!(s instanceof String)) {
    				setStyle("single");
    			}
    			else switch((String)s) {
    				case "dashDotDot":    			
    				case "dotted": setStyle("dotted"); break;
    				case "dashDot":
    				case "dashed": setStyle("dashed"); break;
    				default:
    				case "solid": setStyle("single"); break;
    			
    			}
    	        final Object w = lineAttrs.opt(OCKey.WIDTH.value());
    	        if(w!=null) {
    	            setWidth(w instanceof Number ? ((Number)w).intValue() : null);
    	        }
    	        setColor(lineAttrs.optJSONObject(OCKey.COLOR.value()));
	        }
    	}
    }

    public Map<String, Object> getBorderMap(Integer defaultPadding) {
        if(!isBorder()) {
            return null;
        }
        final Map<String, Object> borderMap = new HashMap<String, Object>();
        final String s = getStyle();
        borderMap.put(OCKey.STYLE.value(), s);
        if(!s.equals("none")) {
            if(defaultPadding!=null) {
                borderMap.put(OCKey.SPACE.value(), defaultPadding);
            }
        }
        final HashMap<String, String> colorMap = getColorMap();
        if(colorMap!=null) {
            borderMap.put(OCKey.COLOR.value(), colorMap);
        }
        final Integer w = getWidth();
        if(w!=null) {
            borderMap.put(OCKey.WIDTH.value(), w);
        }
        return borderMap;
    }
    
    /* returns the border in fo:border style as it is stored in the odf format */
    @Override
    public String toString() {
    	if(style==null) {
    		return "";
    	}
    	if(style.equals("none") || style.equals("hidden")) {
    		return style;
    	}
    	final StringBuffer border = new StringBuffer();
    	if(width!=null) {
    		border.append(Double.valueOf(width.doubleValue() / 100).toString());
    		border.append("mm ");
    	}
    	border.append(style);
    	if(color==null||color.length()!=6) {
    		border.append(" transparent");
    	}
    	else {
    		border.append(" #");
    		border.append(color);
    	}
    	return border.toString();
    }

    public static void createDefaultBorderMapAttrsFromDrawStroke(Map<String, Object> attrs, Integer defaultPadding, AttributesImpl sourceAttrs) {
        final Map<String, Object> defaultBorderMap = createBordermapFromDrawStroke(sourceAttrs, defaultPadding);
        if(defaultBorderMap!=null) {
            attrs.put(OCKey.BORDER_LEFT.value(), defaultBorderMap);
            attrs.put(OCKey.BORDER_TOP.value(), defaultBorderMap);
            attrs.put(OCKey.BORDER_RIGHT.value(), defaultBorderMap);
            attrs.put(OCKey.BORDER_BOTTOM.value(), defaultBorderMap);
        }
    }

    public static Map<String, Object> createBordermapFromDrawStroke(AttributesImpl sourceAttrs, Integer defaultPadding) {
        if(sourceAttrs==null) {
            return null;
        }
        final Border border = new Border();
        border.applyDrawStroke(sourceAttrs);
        return border.getBorderMap(defaultPadding);
    }

    public static void createDefaultBorderMapAttrsFromFoBorder(Map<String, Object> attrs, Integer defaultPadding, AttributesImpl sourceAttrs) {
    	final Map<String, Object> defaultBorderMap = createBordermapFromFoBorder(sourceAttrs.getValue("fo:border"), defaultPadding);
        if(defaultBorderMap!=null) {
        	attrs.put(OCKey.BORDER_LEFT.value(), defaultBorderMap);
        	attrs.put(OCKey.BORDER_TOP.value(), defaultBorderMap);
        	attrs.put(OCKey.BORDER_RIGHT.value(), defaultBorderMap);
        	attrs.put(OCKey.BORDER_BOTTOM.value(), defaultBorderMap);
        }
    }

    public static Map<String, Object> createBordermapFromFoBorder(String value, Integer defaultPadding) {
    	if(value==null||value.isEmpty()) {
    		return null;
    	}
    	final Border border = new Border();
    	border.applyFoBorder(value);
    	return border.getBorderMap(defaultPadding);
    }

    public static void applyFoBorderToSingleBorder(AttributesImpl attributes) {
		final AttributeImpl attribute = attributes.remove("fo:border");
		if(attribute!=null) {
			if(attributes.getValue("fo:border-left")==null) {
				attributes.setValue(new AttributeImpl(Namespaces.FO, "fo:border-left", "border-left", attribute.getValue()));
			}
			if(attributes.getValue("fo:border-top")==null) {
				attributes.setValue(new AttributeImpl(Namespaces.FO, "fo:border-top", "border-top", attribute.getValue()));
			}
			if(attributes.getValue("fo:border-right")==null) {
				attributes.setValue(new AttributeImpl(Namespaces.FO, "fo:border-right", "border-right", attribute.getValue()));
			}
			if(attributes.getValue("fo:border-bottom")==null) {
				attributes.setValue(new AttributeImpl(Namespaces.FO, "fo:border-bottom", "border-bottom", attribute.getValue()));
			}
		}
	}
}
