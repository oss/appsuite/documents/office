/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf;

import java.util.Iterator;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;

public class MetaItem implements IElementWriter, INodeAccessor {

	private DLList<Object> content;
	private final AttributesImpl attributes;
	private final String uri;
	private final String localName;
	private final String qName;
	private String value;

	public MetaItem(String uri, String localName, String qName, AttributesImpl attributes) {
		this.uri = uri;
		this.localName = localName;
		this.qName = qName;
		this.attributes = attributes;
	}

	public MetaItem(String uri, String localName, String qName, String value) {
		this.uri = uri;
		this.localName = localName;
		this.qName = qName;
		this.value = value;
		this.attributes = new AttributesImpl();
	}

	public String getQName() {
		return qName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, uri, localName, qName);
		attributes.write(output);
		if(value!=null) {
			output.characters(value);
		}
		if(content!=null) {
			final Iterator<Object> contentIter = content.iterator();
			while(contentIter.hasNext()) {
				((IElementWriter)contentIter.next()).writeObject(output);
			}
		}
		SaxContextHandler.endElement(output, uri, localName, qName);
	}

	@Override
	public DLList<Object> getContent() {
		if(content==null) {
			content = new DLList<Object>();
		}
		return content;
	}
}
