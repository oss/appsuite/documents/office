/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf;

import org.xml.sax.Attributes;

public class MetaItemHandler extends SaxContextHandler {

	final private MetaItem metaItem;

	public MetaItemHandler(SaxContextHandler parentContextHandler, MetaItem metaItem) {
   		super(parentContextHandler);
   		this.metaItem = metaItem;
    }

    @Override
	public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
        metaItem.getContent().add(element);
        return new UnknownContentHandler(this, element);
    }

	@Override
	public void endContext(String qName, String characters) {
		metaItem.setValue(characters);
	}
}
