/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Length;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class TextProperties extends StylePropertiesBase {

	public TextProperties(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

    static public boolean hasData(TextProperties o) {
        if(o==null) {
            return false;
        }
        return !o.getAttributes().isEmpty();
    }

	@Override
	public String getQName() {
		return "style:text-properties";
	}

	@Override
	public String getLocalName() {
		return "text-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs)
		throws JSONException {

		final JSONObject characterAttributes = attrs.optJSONObject(OCKey.CHARACTER.value());
		if(characterAttributes!=null) {

	    	Object language = null;
	    	Object noProof = null;

			final Iterator<Entry<String, Object>> characterIter = characterAttributes.entrySet().iterator();
			while(characterIter.hasNext()) {
				final Entry<String, Object> characterEntry = characterIter.next();
				final Object value = characterEntry.getValue();
				switch(OCKey.fromValue(characterEntry.getKey())) {
					case COLOR : {
						if (value==JSONObject.NULL) {
							attributes.remove("fo:color");
						}
						else {
	                        final JSONObject color = (JSONObject)value;
	                        if (color.hasAndNotNull(OCKey.TYPE.value())) {
	                            String type = color.optString(OCKey.TYPE.value(), "");
	                            if(!type.equals("AUTO")) {
	                            	attributes.setValue(Namespaces.FO, "color", "fo:color", PropertyHelper.getColor(color, null));
	                            	attributes.remove("style:use-window-font-color");
	                            } else {
	                            	attributes.setValue(Namespaces.STYLE, "use-window-font-color", "style:use-window-font-color", "true");
	                            }
	                        } else {
	                            attributes.setValue(Namespaces.STYLE, "use-window-font-color", "style:use-window-font-color", "true");
	                        }
						}
						break;
					}
					case BOLD : {
		                if(value==JSONObject.NULL) {
		                    attributes.remove("fo:font-weight");
		                }
		                else {
	                        attributes.setValue(Namespaces.FO, "font-weight", "fo:font-weight", ((Boolean)value).booleanValue() ? "bold" : "normal");
		                }
		                break;
					}
					case BOLD_ASIAN : {
						if(value==JSONObject.NULL) {
							attributes.remove("style:font-weight-asian");
						} else {
							attributes.setValue(Namespaces.STYLE, "font-weight-asian", "style:font-weight-asian", ((Boolean)value).booleanValue() ? "bold" : "normal");
						}
						break;
					}
					case BOLD_COMPLEX : {
						if(value==JSONObject.NULL) {
							attributes.remove("style:font-weight-complex");
						} else {
							attributes.setValue(Namespaces.STYLE, "font-weight-complex", "style:font-weight-complex", ((Boolean)value).booleanValue() ? "bold" : "normal");
						}
						break;
					}
					case CAPS : {
					    attributes.remove("fo:font-variant");
					    attributes.remove("fo'text-transform");
					    if(value instanceof String) {
					        String caps = null;
					        if(((String)value).equals("none")) {
					            caps = "none";
					        }
					        else if(((String)value).equals("small")) {
					            caps = "small";
					        }
					        else if((((String)value)).equals("all")) {
					            caps = "all";
					        }
					        if(caps!=null) {
					            attributes.setValue(Namespaces.FO, "font-transform", "fo:font-transform", caps);
					        }
					    }
					    break;
					}
					case UNDERLINE : {
		                if(value==JSONObject.NULL) {
		                    attributes.remove("style:text-underline-style");
		                    attributes.remove("style:text-underline-width");
		                    attributes.remove("style:text-underline-color");
		                    attributes.remove("style:text-underline-type");
		                } else {
							attributes.setValue(Namespaces.STYLE, "text-underline-style", "style:text-underline-style", ((Boolean)value).booleanValue() ? "solid" : "none");
		                }
		                break;
					}
					case ITALIC : {
		                if(value==JSONObject.NULL) {
		                    attributes.remove("fo:font-style");
		                } else {
							attributes.setValue(Namespaces.FO, "font-style", "fo:font-style", ((Boolean)value).booleanValue() ? "italic" : "normal");
		                }
		                break;
					}
					case ITALIC_ASIAN : {
		                if(value==JSONObject.NULL) {
		                    attributes.remove("style:font-style-asian");
		                } else {
							attributes.setValue(Namespaces.STYLE, "font-style-asian", "style:font-style-asian", ((Boolean)value).booleanValue() ? "italic" : "normal");
		                }
		                break;
	                }
					case ITALIC_COMPLEX : {
		                if(value==JSONObject.NULL) {
		                    attributes.remove("style:font-style-complex");
		                } else {
							attributes.setValue(Namespaces.STYLE, "font-style-complex", "style:font-style-complex", ((Boolean)value).booleanValue() ? "italic" : "normal");
		                }
		                break;
	                }
					case FILL_COLOR : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:background-color");
		                } else {
		                    attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", PropertyHelper.getColor((JSONObject)value, "transparent"));
		                }
		                break;
					}
					case FONT_SIZE : {
						if(value==JSONObject.NULL) {
							attributes.remove("fo:font-size");
						} else {
							attributes.setValue(Namespaces.FO, "font-size", "fo:font-size", value.toString() + "pt");
						}
						break;
					}
					case FONT_SIZE_ASIAN : {
						if(value==JSONObject.NULL) {
							attributes.remove("style:font-size-asian");
						} else {
							attributes.setValue(Namespaces.STYLE, "font-size-asian", "style:font-size-asian", value.toString() + "pt");
						}
						break;
					}
					case FONT_SIZE_COMPLEX : {
						if(value==JSONObject.NULL) {
							attributes.remove("style:font-size-complex");
						} else {
							attributes.setValue(Namespaces.STYLE, "font-size-complex", "style:font-size-complex", value.toString() + "pt");
						}
						break;
					}
					case FONT_NAME : {
		                if (value==JSONObject.NULL) {
		                	attributes.remove("style:font-name");
		                } else {
		                	attributes.setValue(Namespaces.STYLE, "font-name", "style:font-name", (String)value);
		                	styleManager.addFontToDocument((String)value);
		                }
		                break;
					}
					case FONT_NAME_ASIAN : {
		                if (value==JSONObject.NULL) {
		                	attributes.remove("style:font-name-asian");
		                } else {
		                	attributes.setValue(Namespaces.STYLE, "font-name-asian", "style:font-name-asian", (String)value);
		                	styleManager.addFontToDocument((String)value);
		                }
		                break;
					}
					case FONT_NAME_COMPLEX : {
		                if (value==JSONObject.NULL) {
		                	attributes.remove("style:font-name-complex");
		                } else {
		                	attributes.setValue(Namespaces.STYLE, "font-name-complex", "style:font-name-complex", (String)value);
		                	styleManager.addFontToDocument((String)value);
		                }
		                break;
					}
					case VERT_ALIGN : {
		                if (value == null || value.equals(JSONObject.NULL)) {
		                	attributes.remove("style:text-position");
		                } else {
		                	final String alignment = (String)value;
		                    if (alignment.equals("sub")) {
		                    	attributes.setValue(Namespaces.STYLE, "text-position", "style:text-position", "sub");
		                    } else if (alignment.equals("super")) {
		                    	attributes.setValue(Namespaces.STYLE, "text-position", "style:text-position", "super");
		                    } else { //baseline
		                    	attributes.setValue(Namespaces.STYLE, "text-position", "style:text-position", "0% 100%");
		                    }
		                }
		                break;
					}
					case STRIKE : {
		                if(value==JSONObject.NULL) {
		                    attributes.remove("style:text-line-through-color");
		                    attributes.remove("style:text-line-through-mode");
		                    attributes.remove("style:text-line-through-style");
		                    attributes.remove("style:text-line-through-text");
		                    attributes.remove("style:text-line-through-text-style");
		                    attributes.remove("style:text-line-through-type");
		                    attributes.remove("style:text-line-through-width");
		                }
		                else {
		                    String strikeType = (String)value;
		                    if(strikeType.equals("none")) {
		                        attributes.setValue(Namespaces.STYLE, "text-line-through-style", "style:text-line-through-style", "none");
		                    }
		                    else {
		                        attributes.setValue(Namespaces.STYLE, "text-line-through-style", "style:text-line-through-style", "solid");
		                    	attributes.setValue(Namespaces.STYLE, "text-line-through-type", "style:text-line-through-type", strikeType.equals("single") ? "single" : "double");
		                        attributes.setValue(Namespaces.STYLE, "text-line-through-mode", "style:text-line-through-mode", "continuous");
		                        attributes.setValue(Namespaces.STYLE, "text-underline-mode", "style:text-underline-mode", "continuous");
		                        attributes.setValue(Namespaces.STYLE, "text-overline-mode", "style:text-overline-mode", "continuous");
		                    }
		                }
		                break;
					}
					case LANGUAGE : {
						language = value;
						break;
					}
					case NO_PROOF : {
						noProof = value;
						break;
					}
					case LETTER_SPACING : {
		                if (value==JSONObject.NULL) {
		                	attributes.remove("fo:letter-spacing");
		                }
		                else {
		                	attributes.setValue(Namespaces.FO, "letter-spacing", "fo:letter-spacing", "normal".equals("normal") ? "normal" : ((Number)value).longValue() / 100 + "mm");
		                }
		                break;
					}
				}
			}

	        if(noProof!=null || language!=null) {
	        	Object newLanguage = language;
	        	if((noProof instanceof Boolean && ((Boolean)noProof).booleanValue()) || ((language instanceof String) && ((String)language).equals("none"))) {
	    			attributes.setValue(Namespaces.FO, "language", "fo:language", "zxx");
	    			attributes.setValue(Namespaces.STYLE, "language-asian", "style:language-asian", "zxx");
	    			attributes.setValue(Namespaces.STYLE, "language-complex", "style:language-complex", "zxx");
	    			attributes.setValue(Namespaces.FO, "country", "fo:country", "none");
	    			attributes.setValue(Namespaces.STYLE, "country-asian", "style:country-asian", "none");
	    			attributes.setValue(Namespaces.STYLE, "country-complex", "style:country-complex", "none");
	        	}
	        	else if (newLanguage == null || newLanguage.equals(JSONObject.NULL)) {
	        		attributes.remove("fo:country");
	        		attributes.remove("fo:language");
	        		attributes.remove("style:country-asian");
	        		attributes.remove("style:language-asian");
	        		attributes.remove("style:country-complex");
	        		attributes.remove("style:language-complex");
	            }
	        	else {
	                String locale = (String)newLanguage;
	                if (!locale.isEmpty()) {
	                    int delimiterPos = locale.indexOf('-');
	                    if (delimiterPos > -1) {
	                    	attributes.setValue(Namespaces.FO, "language", "fo:language", locale.substring(0, delimiterPos));
	                    	attributes.setValue(Namespaces.FO, "country", "fo:country", locale.substring(delimiterPos + 1, locale.length()));
	                    } else {
	                    	attributes.setValue(Namespaces.FO, "language", "fo:language", locale);
	                    }
	                }
	            }
	        }
		}
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		final Map<String, Object> characterAttrs = attrs.getMap(OCKey.CHARACTER.value(), true);
		final Iterator<Entry<String, AttributeImpl>> propIter = attributes.getUnmodifiableMap().entrySet().iterator();
		String fontName = null;
		String fontFamily = null;
        while(propIter.hasNext()) {
        	final Entry<String, AttributeImpl> propEntry = propIter.next();
        	final String propName = propEntry.getKey();
        	final String propValue = propEntry.getValue().getValue();
        	switch(propName) {
        		case "fo:letter-spacing": {
	        	    if (propValue.equals("normal")) {
	        	    	characterAttrs.put(OCKey.LETTER_SPACING.value(), propValue);
	        	    } else {
	        	        characterAttrs.put(OCKey.LETTER_SPACING.value(), AttributesImpl.normalizeLength(propValue));
	        	    }
	        	    break;
        		}
        		case "fo:font-size": {
	        	    if (propValue.contains("%")) {
	        	    	//
	        	    } else {
	        	        characterAttrs.put(OCKey.FONT_SIZE.value(), new Length(propValue).getPoint());
	        	    }
	        	    break;
        		}
        		case "style:font-size-asian": {
	        	    if (propValue.contains("%")) {
	        	    	//
	        	    } else {
	        	        characterAttrs.put(OCKey.FONT_SIZE_ASIAN.value(), new Length(propValue).getPoint());
	        	    }
	        	    break;
        		}
        		case "style:font-size-complex": {
	        	    if (propValue.contains("%")) {
	        	    	//
	        	    } else {
	        	        characterAttrs.put(OCKey.FONT_SIZE_COMPLEX.value(), new Length(propValue).getPoint());
	        	    }
	        	    break;
        		}
        		case "style:font-name": {
        		    fontName = propValue;
        			break;
        		}
        		case "fo:font-family": {
        		    fontFamily = propValue;
        		    break;
        		}
        		case "style:font-name-asian": {
        			characterAttrs.put(OCKey.FONT_NAME_ASIAN.value(), propValue);
        			break;
        		}
        		case "style:font-name-complex": {
        			characterAttrs.put(OCKey.FONT_NAME_COMPLEX.value(), propValue);
        			break;
        		}
        		case "style:text-position": {
	        	    if (propValue.contains("sub")) {
	        	        characterAttrs.put(OCKey.VERT_ALIGN.value(), "sub");
	        	    } else if (propValue.contains("super")) {
	        	        characterAttrs.put(OCKey.VERT_ALIGN.value(), "super");
	        	    } else if (propValue.equals("0%") || propValue.equals("0% 100%")) {
	        	        characterAttrs.put(OCKey.VERT_ALIGN.value(), "baseline");
	        	    }
	        	    break;
        		}
        		case "fo:language": {
	        	    if (!propValue.equals("none")) {
	        			final String country = attributes.getValue("fo:country");
	        			String language = propValue;
	        	        if (country!=null && !country.isEmpty()&&!country.equals("none")) {
	        	        	language = propValue + '-' + country;
	        	        }
	        	        characterAttrs.put(OCKey.LANGUAGE.value(), language);
	        	        styleManager.getUsedLanguages().add(language);
	        	    }
	        	    break;
        		}
        		case "fo:font-weight": {
    		    	characterAttrs.put(OCKey.BOLD.value(), !propValue.equals("normal"));
        		    break;
        		}
        		case"style:font-weight-asian": {
    		    	characterAttrs.put(OCKey.BOLD_ASIAN.value(), !propValue.equals("normal"));
        		    break;
        		}
        		case "style:font-weight-complex": {
        	    	characterAttrs.put(OCKey.BOLD_COMPLEX.value(), !propValue.equals("normal"));
	        	    break;
        		}
        		case "style:text-underline-style": {
    		    	characterAttrs.put(OCKey.UNDERLINE.value(), !propValue.equals("none"));
        		    break;
        		}
        		case "fo:font-style": {
        	    	characterAttrs.put(OCKey.ITALIC.value(), !propValue.equals("normal"));
	        	    break;
        		}
        		case "style:font-style-asian": {
        	    	characterAttrs.put(OCKey.ITALIC_ASIAN.value(), !propValue.equals("normal"));
	        	    break;
        		}
        		case "style:font-style-complex": {
        	    	characterAttrs.put(OCKey.ITALIC_COMPLEX.value(), !propValue.equals("normal"));
	        	    break;
        		}
        		case "fo:color": {
        		    if (!characterAttrs.containsKey(OCKey.COLOR.value())) {
        		    	final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
        		    	if(color!=null) {
        		    		characterAttrs.put(OCKey.COLOR.value(), color);
        		    	}
        		    }
        		    break;
        		}
        		case "style:use-window-font-color": {
	        	    if (propValue.equals("true")) {
	                    final Map<String, Object> color = PropertyHelper.createColorMap("auto");
	                    if(color!=null) {
	                        characterAttrs.put(OCKey.COLOR.value(), color);
	                    }
	        	    }
	        	    break;
        		}
        		case "fo:background-color": {
        			final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
        			if(color!=null) {
        				characterAttrs.put(OCKey.FILL_COLOR.value(), color);
        			}
	        	    break;
        		}
        		case "style:width": {
        			final Integer width = attributes.getLength100thmm(propValue, true);
        			if(width!=null) {
	        	    	characterAttrs.put(OCKey.WIDTH.value(), width);
	        	    }
	        	    break;
        		}
        		case "style:writing-mode": {
	        	    characterAttrs.put("writingMode", propValue);
	        	    break;
        		}
        		case "style:text-line-through-style": {
	        	    if (propValue.equals("none")) {
	        	    	characterAttrs.put(OCKey.STRIKE.value(), "none");
	        	    } else {

	        	    	final String lineType = attributes.getValue("style:text-line-through-type");
	        	    	if(lineType!=null) {
        	            	characterAttrs.put(OCKey.STRIKE.value(), lineType.equals("double") ? "double" : "single");
	        	        }
	        	    	else {
	        	        	characterAttrs.put(OCKey.STRIKE.value(), "single");
	        	        }
	        	    }
	        	    break;
        		}
        		case "fo:text-transform": {
        		    String caps = null;
        		    if(propValue.equals("none")) {
        		        caps = "none";
        		    }
        		    else if(propValue.equals("lowercase")) {
        		        caps = "small";
        		    }
        		    else if(propValue.equals("uppercase")) {
        		        caps = "all";
        		    }
        		    else if(propValue.equals("capitalize")) {
        		        caps = "all";
        		    }
        		    if(caps!=null) {
        		        characterAttrs.put(OCKey.CAPS.value(), caps);
        		    }
        		    break;
        		}
        		case "fo:font-variant": {
        		    characterAttrs.put(OCKey.CAPS.value(), propValue.equals("small-caps") ? "small" : "normal");
        		    break;
        		}
        	}
        	if(fontName!=null) {
                characterAttrs.put(OCKey.FONT_NAME.value(), fontName);
        	}
        	else if(fontFamily!=null) {
                characterAttrs.put(OCKey.FONT_NAME.value(), fontFamily);
        	}
        }
        if(characterAttrs.isEmpty()) {
        	attrs.remove(OCKey.CHARACTER.value());
        }
	}

	public String getColorFromElement() {
        //contains e.g. fo:color
        String ret = "";
        final String color = attributes.getValue("fo:color");
        if(color != null) {
            if(color.equalsIgnoreCase("#0000ff")) {
                ret = "[BLUE]";
            } else if(color.equalsIgnoreCase("#00FF00")) {
                ret = "[GREEN]";
            } else if(color.equalsIgnoreCase("#FF0000")) {
                ret = "[RED]";
            } else if(color.equalsIgnoreCase("#FFFFFF")) {
                ret = "[WHITE]";
            } else if(color.equalsIgnoreCase("#FF00FF")) {
                ret = "[MAGENTA]";
            } else if(color.equalsIgnoreCase("#FFFF00")) {
                ret = "[YELLOW]";
            } else if(color.equalsIgnoreCase("#00FFFF")) {
                ret = "[CYAN]";
            } else if(color.equalsIgnoreCase("#000000")) {
                ret = "[BLACK]";
            }
        }
        return ret;
    }

	@Override
	public TextProperties clone() {
		return (TextProperties)_clone();
	}

    public void applyHardDefaultAttributes(Set<String> charKeyset) {

        final Iterator<String> keyIter = charKeyset.iterator();
        while(keyIter.hasNext()) {
            switch(OCKey.fromValue(keyIter.next())) {
                case COLOR : {
                    attributes.setValue(Namespaces.FO, "color", "fo:color", "#000000");
                    attributes.remove("style:use-window-font-color");
                    break;
                }
                case BOLD : {
                    attributes.setValue(Namespaces.FO, "font-weight", "fo:font-weight", "normal");
                    break;
                }
                case BOLD_ASIAN : {
                    attributes.setValue(Namespaces.STYLE, "font-weight-asian", "style:font-weight-asian", "normal");
                    break;
                }
                case BOLD_COMPLEX : {
                    attributes.setValue(Namespaces.STYLE, "font-weight-complex", "style:font-weight-complex", "normal");
                    break;
                }
                case CAPS : {
                    attributes.remove("fo:font-variant");
                    attributes.remove("fo'text-transform");
                    attributes.setValue(Namespaces.FO, "font-transform", "fo:font-transform", "none");
                    break;
                }
                case UNDERLINE : {
                    attributes.setValue(Namespaces.STYLE, "text-underline-style", "style:text-underline-style", "none");
                    break;
                }
                case ITALIC : {
                    attributes.setValue(Namespaces.FO, "font-style", "fo:font-style", "normal");
                    break;
                }
                case ITALIC_ASIAN : {
                    attributes.setValue(Namespaces.STYLE, "font-style-asian", "style:font-style-asian", "normal");
                    break;
                }
                case ITALIC_COMPLEX : {
                    attributes.setValue(Namespaces.STYLE, "font-style-complex", "style:font-style-complex", "normal");
                    break;
                }
                case FILL_COLOR : {
                    attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", "transparent");
                    break;
                }
                case FONT_SIZE : {  // TODO: what is the font size default ?
                    attributes.setValue(Namespaces.FO, "font-size", "fo:font-size","12pt");
                    break;
                }
                case FONT_SIZE_ASIAN : {
                    attributes.setValue(Namespaces.STYLE, "font-size-asian", "style:font-size-asian", "12pt");
                    break;
                }
                case FONT_SIZE_COMPLEX : {
                    attributes.setValue(Namespaces.STYLE, "font-size-complex", "style:font-size-complex", "12pt");
                    break;
                }
                case FONT_NAME : {  // TODO: font name default ?
                    attributes.remove("style:font-name");
                    break;
                }
                case FONT_NAME_ASIAN : {
                    attributes.remove("style:font-name-asian");
                    break;
                }
                case FONT_NAME_COMPLEX : {
                    attributes.remove("style:font-name-complex");
                    break;
                }
                case VERT_ALIGN : { // TODO:
                    attributes.remove("style:text-position");
                    break;
                }
                case STRIKE : {
                    attributes.setValue(Namespaces.STYLE, "text-line-through-style", "style:text-line-through-style", "none");
                    break;
                }
                case LANGUAGE : {
                    break;
                }
                case NO_PROOF : {
                    break;
                }
                case LETTER_SPACING : {
                    attributes.setValue(Namespaces.FO, "letter-spacing", "fo:letter-spacing", "normal");
                    break;
                }
            }
        }
    }
}
