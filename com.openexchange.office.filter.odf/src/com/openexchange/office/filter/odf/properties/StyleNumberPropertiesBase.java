/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.StyleManager;

public abstract class StyleNumberPropertiesBase extends StylePropertiesBase {

	public StyleNumberPropertiesBase(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		attributes.write(output);
		if(!text.isEmpty()) {
			output.characters(text);
		}
		final DLList<IElementWriter> content = getContent();
		if(content!=null) {
			final Iterator<IElementWriter> childIter = content.iterator();
			while(childIter.hasNext()) {
				childIter.next().writeObject(output);
			}
		}
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());

	}

	@Override
	public final void applyAttrs(StyleManager styleManager, JSONObject attrs) {
	    //
	}

	@Override
	public final void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
	    //
	}
}
