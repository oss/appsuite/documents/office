/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;

public class PropertyHelper {

	public static void setLineHeight(JSONObject lineHeight, StylePropertiesBase stylePropertiesBase)
		throws JSONException {

        final AttributesImpl attributes = stylePropertiesBase.getAttributes();
        final String type = lineHeight.getString(OCKey.TYPE.value());
        if (type.equals("percent")) {
        	final String value = lineHeight.optString(OCKey.VALUE.value());
        	if(value!=null&&!value.isEmpty()) {
        		final String lineHeightValue = value.concat("%");
                attributes.setValue(Namespaces.FO, "line-height", "fo:line-height", lineHeightValue);
                attributes.remove("style:line-height-at-least");
                attributes.remove("style:line-spacing");
            }
        }
        else if(type.equals("fixed")) {
        	final double value = lineHeight.optDouble(OCKey.VALUE.value());
        	if(value!=Double.NaN) {
        		final String lineHeightValue = value / 100.0 + "mm";
	            attributes.setValue(Namespaces.FO, "line-height", "fo:line-height", lineHeightValue);
	            attributes.remove("style:line-height-at-least");
	            attributes.remove("style:line-spacing");
        	}
        }
        else if(type.equals("atLeast")) {
        	final double value = lineHeight.optDouble(OCKey.VALUE.value());
            if(value!=Double.NaN) {
	            final String lineHeightValue = value / 100.0 + "mm";
	            attributes.setValue(Namespaces.STYLE, "line-height-at-least", "style:line-height-at-least", lineHeightValue);
	            attributes.remove("fo:line-height");
	            attributes.remove("style:line-spacing");
        	}
        }
        else if(type.equals("leading")) {
        	final double value = lineHeight.optDouble(OCKey.VALUE.value());
            if(value!=Double.NaN) {
	            final String lineHeightValue = value / 100.0 + "mm";
	            attributes.setValue(Namespaces.FO, "line-heigh", "fo:line-heigh", lineHeightValue);
	            attributes.remove("fo:line-height");
	            attributes.remove("style:line-height-at-least");
        	}
        }
        else if(type.equals("normal")) {
            attributes.remove("style:line-height-at-least");
            attributes.remove("style:line-spacing");
        }
    }

    public static String getColor(JSONObject color, String autoColor)
    	throws JSONException {

    	String colorValue = "";
        if (color != null) {
            String type = color.getString(OCKey.TYPE.value());
            if (type.equals("auto")) {
                colorValue = autoColor;
            } else if (type.equals("rgb")) {
                colorValue = '#' + color.getString(OCKey.VALUE.value());
            } else if (color.has(OCKey.FALLBACK_VALUE.value())) {
                // {"color":{"type":"scheme",OCKey.VALUE.shortName():"accent1","transformations":[{"type":"shade",OCKey.VALUE.shortName():74902}],"fallbackValue":"376092"}
                colorValue = '#' + color.getString(OCKey.FALLBACK_VALUE.value());
            }
        }
        return colorValue;
    }

    public static Map<String, Object> createColorMap(String rgbValue) {
    	final Map<String, Object> color = new HashMap<String, Object>();
        if (rgbValue.contains("#")) {
            color.put(OCKey.TYPE.value(), "rgb");
            rgbValue = rgbValue.subSequence(rgbValue.indexOf('#') + 1, rgbValue.length()).toString();
            color.put(OCKey.VALUE.value(), rgbValue);
            return color;
        } else if (rgbValue.equals("transparent") || rgbValue.equals("auto")) {
            color.put(OCKey.TYPE.value(), "auto");
            return color;
        }
        return null;
    }

    // applies the opacity as alpha transformation to the given colormap, existing transparency is overwritten
    public static void applyOpacityToColorMap(Map<String, Object> color, double opacity) {
        try {
            Object transformation = color.get(OCKey.TRANSFORMATIONS.value());
            if(transformation==null) {
                transformation = new JSONArray(1);
                color.put(OCKey.TRANSFORMATIONS.value(), transformation);
            }
            JSONObject o = null;
            for(int i = 0; i < ((JSONArray)transformation).length(); i++) {
                final JSONObject a = ((JSONArray)transformation).getJSONObject(i);
                if (a.get(OCKey.TYPE.value()).equals("alpha")) {
                    o = a;
                    break;
                }
            }
            if(o==null) {
                o = new JSONObject();
                o.put(OCKey.TYPE.value(), "alpha");
                ((JSONArray)transformation).put(o);
            }
            o.put(OCKey.VALUE.value(), opacity * 1000);
        }
        catch(JSONException e) {
            // ohoh
        }
    }

    public static Map<String, Object> createLineHeightMap(String lineHeightValue) {
    	final Map<String, Object> lineHeight = new HashMap<String, Object>(2);
        if (lineHeightValue.equals("normal")) {
            lineHeight.put(OCKey.TYPE.value(), "percent");
            lineHeight.put(OCKey.VALUE.value(), "100");
        } else if (lineHeightValue.contains("%")) {
            lineHeight.put(OCKey.TYPE.value(), "percent");
            lineHeight.put(OCKey.VALUE.value(), Integer.parseInt(lineHeightValue.subSequence(0, lineHeightValue.indexOf('%')).toString()));
        } else {
            lineHeight.put(OCKey.TYPE.value(), "fixed");
            lineHeight.put(OCKey.VALUE.value(), AttributesImpl.normalizeLength(lineHeightValue));
        }
        return lineHeight;
    }

    public static String mapFoTextAlign(String propValue) {
        if (propValue.equals("start") || propValue.equals("left")) {
            propValue = "left";
        } else if (propValue.equals("end") || propValue.equals("right")) {
            propValue = "right";
        }
        return propValue;
    }

    public static void createDefaultMarginAttrs(Map<String, Object> attrs, AttributesImpl sourceAttrs) {
        final Integer defaultMargin = sourceAttrs.getLength100thmm("fo:margin", true);
        if(defaultMargin!=null) {
        	attrs.put(OCKey.MARGIN_LEFT.value(), defaultMargin);
        	attrs.put(OCKey.INDENT_LEFT.value(), defaultMargin);
        	attrs.put(OCKey.MARGIN_TOP.value(), defaultMargin);
        	attrs.put(OCKey.MARGIN_RIGHT.value(), defaultMargin);
        	attrs.put(OCKey.INDENT_RIGHT.value(), defaultMargin);
        	attrs.put(OCKey.MARGIN_BOTTOM.value(), defaultMargin);
        }
    }

    public static Integer createDefaultPaddingAttrs(Map<String, Object> attrs, AttributesImpl sourceAttrs) {
	    final Integer defaultPadding = sourceAttrs.getLength100thmm("fo:padding", true);
	    if(defaultPadding!=null) {
	    	attrs.put(OCKey.PADDING_LEFT.value(), defaultPadding);
	    	attrs.put(OCKey.PADDING_TOP.value(), defaultPadding);
	    	attrs.put(OCKey.PADDING_RIGHT.value(), defaultPadding);
	    	attrs.put(OCKey.PADDING_BOTTOM.value(), defaultPadding);
	    }
	    return defaultPadding;
    }

    private static Map<Integer, String> languageToLocaleMap = null;
    private static Map<String, Integer> localeToLanguageMap = null;

    static private void fillLocaleMaps() {
        if(localeToLanguageMap == null) {
            localeToLanguageMap = new HashMap<String, Integer>();
            languageToLocaleMap = new HashMap<Integer, String>();

        class StringAndInt{
            public String     locale;
            public int         msValue;
            public StringAndInt(String s, int v) {
                locale = s;
                msValue = v;
            }
        }

        StringAndInt mapping[] = {
            new StringAndInt("af-ZA", 0x436),
            new StringAndInt("sq-AL", 0x41c),
            new StringAndInt("gsw-FR", 0x484),
            new StringAndInt("am-ET", 0x45e),
            new StringAndInt("ar-DZ", 0x1401),
            new StringAndInt("ar-BH", 0x3c01),
            new StringAndInt("ar-EG", 0xc01),
            new StringAndInt("ar-IQ", 0x801),
            new StringAndInt("ar-JO", 0x2c01),
            new StringAndInt("ar-KW", 0x3401),
            new StringAndInt("ar-LB", 0x3001),
            new StringAndInt("ar-LY", 0x1001),
            new StringAndInt("ar-MA", 0x1801),
            new StringAndInt("ar-OM", 0x2001),
            new StringAndInt("ar-QA", 0x4001),
            new StringAndInt("ar-SA", 0x401),
            new StringAndInt("ar-SY", 0x2801),
            new StringAndInt("ar-TN", 0x1c01),
            new StringAndInt("ar-AE", 0x3801),
            new StringAndInt("ar-YE", 0x2401),
            new StringAndInt("ar", 0x1),
            new StringAndInt("hy-AM", 0x42b),
            new StringAndInt("as-IN", 0x44d),
            new StringAndInt("az", 0x2c),
            new StringAndInt("az-cyrillic", 0x82c),
            new StringAndInt("az-AZ", 0x42c),
            new StringAndInt("ba-RU", 0x46d),
            new StringAndInt("eu", 0x42d),
            new StringAndInt("be-BY", 0x423),
            new StringAndInt("bn-IN", 0x445),
            new StringAndInt("bn-BD", 0x845),
            new StringAndInt("bs-BA", 0x141a),
//            new StringAndInt("", 0x201a),
            new StringAndInt("br-FR", 0x47e),
            new StringAndInt("bg-BG", 0x402),
            new StringAndInt("my-MM", 0x455),
            new StringAndInt("ca-ES", 0x403),
            new StringAndInt("chr-US", 0x45c),
            new StringAndInt("zh", 0x4),
            new StringAndInt("zh-HK", 0xc04),
            new StringAndInt("zh-MO", 0x1404),
            new StringAndInt("zh-CN", 0x804),
            new StringAndInt("zh-SG", 0x1004),
            new StringAndInt("zh-TW", 0x404),
            new StringAndInt("co-FR", 0x483),
            new StringAndInt("hr-HR", 0x41a),
            new StringAndInt("hr-BA", 0x101a),
            new StringAndInt("cs-CZ", 0x405),
            new StringAndInt("da-DK", 0x406),
            new StringAndInt("gbz-AF", 0x48c),
            new StringAndInt("dv-MV", 0x465),
            new StringAndInt("nl-NL", 0x413),
            new StringAndInt("nl-BE", 0x813),
            new StringAndInt("bin-NG", 0x466),
            new StringAndInt("en", 0x9),
            new StringAndInt("en-AU", 0xc09),
            new StringAndInt("en-BZ", 0x2809),
            new StringAndInt("en-CA", 0x1009),
            new StringAndInt("en-BS", 0x2409),
            new StringAndInt("en-IE", 0x1809),
            new StringAndInt("en-HK", 0x3c09),
            new StringAndInt("en-IN", 0x4009),
            new StringAndInt("en-ID", 0x3809),
            new StringAndInt("en-JM", 0x2009),
            new StringAndInt("en-MY", 0x4409),
            new StringAndInt("en-NZ", 0x1409),
            new StringAndInt("en-PH", 0x3409),
            new StringAndInt("en-ZA", 0x1c09),
            new StringAndInt("en-SG", 0x4809),
            new StringAndInt("en-TT", 0x2c09),
            new StringAndInt("en-GB", 0x809),
            new StringAndInt("en-US", 0x409),
            new StringAndInt("en-ZW", 0x3009),
            new StringAndInt("et-EE", 0x425),
            new StringAndInt("fo-FO", 0x438),
            new StringAndInt("fa-IR", 0x429),
            new StringAndInt("fil-PH", 0x464),
            new StringAndInt("fi-FI", 0x40b),
            new StringAndInt("fr-FR", 0x40c),
            new StringAndInt("fr-BE", 0x80c),
            new StringAndInt("fr-CM", 0x2c0c),
            new StringAndInt("fr-CA", 0xc0c),
            new StringAndInt("fr-CI", 0x300c),
            new StringAndInt("fr-HT", 0x3c0c),
            new StringAndInt("fr-LU", 0x140c),
            new StringAndInt("fr-ML", 0x340c),
            new StringAndInt("fr-MC", 0x180c),
            new StringAndInt("fr-MA", 0x380c),
            new StringAndInt("fr", 0xe40c),
            new StringAndInt("fr-RE", 0x200c),
            new StringAndInt("fr-SN", 0x280c),
            new StringAndInt("fr-CH", 0x100c),
            new StringAndInt("fr", 0x1c0c),
            new StringAndInt("fr-CD", 0x240c),
            new StringAndInt("fy-NL", 0x462),
            new StringAndInt("ff-NG", 0x467),
            new StringAndInt("ga-IE", 0x83c),
            new StringAndInt("gd-GB", 0x43c),
            new StringAndInt("gl-ES", 0x456),
            new StringAndInt("ka-GE", 0x437),
            new StringAndInt("de-DE", 0x407),
            new StringAndInt("de-AT", 0xc07),
            new StringAndInt("de-LI", 0x1407),
            new StringAndInt("de-LU", 0x1007),
            new StringAndInt("de-CH", 0x807),
            new StringAndInt("el-GR", 0x408),
            new StringAndInt("gug-PY", 0x474),
            new StringAndInt("gu-IN", 0x447),
            new StringAndInt("ha-NG", 0x468),
            new StringAndInt("haw-US", 0x475),
            new StringAndInt("he-IL", 0x40d),
            new StringAndInt("hi-IN", 0x439),
            new StringAndInt("hu-HU", 0x40e),
//            new StringAndInt("", 0x469),
            new StringAndInt("is-IS", 0x40f),
            new StringAndInt("ig-NG", 0x470),
            new StringAndInt("id-ID", 0x421),
//            new StringAndInt("", 0x45d),
            new StringAndInt("iu-CA", 0x85d),
            new StringAndInt("it-IT", 0x410),
            new StringAndInt("it-CH", 0x810),
            new StringAndInt("ja-JP", 0x411),
            new StringAndInt("kl-GL", 0x46f),
            new StringAndInt("kn-IN", 0x44b),
            new StringAndInt("kr-NG", 0x471),
            new StringAndInt("ks", 0x460),
            new StringAndInt("ks-IN", 0x860),
            new StringAndInt("kk-KZ", 0x43f),
            new StringAndInt("km-KH", 0x453),
            new StringAndInt("qut-GT", 0x486),
            new StringAndInt("rw-RW", 0x487),
            new StringAndInt("ky-KG", 0x440),
            new StringAndInt("kok-IN", 0x457),
            new StringAndInt("ko-KR", 0x412),
            new StringAndInt("ko-KR", 0x812),
            new StringAndInt("lo-LA", 0x454),
            new StringAndInt("la-VA", 0x476),
            new StringAndInt("lv-LV", 0x426),
            new StringAndInt("lt-LT", 0x427),
            new StringAndInt("lt-LT", 0x827),
            new StringAndInt("lb-LU", 0x46e),
            new StringAndInt("mk-MK", 0x42f),
            new StringAndInt("ms", 0x3e),
            new StringAndInt("ml-IN", 0x44c),
            new StringAndInt("ms-BN", 0x83e),
            new StringAndInt("ms-MY", 0x43e),
            new StringAndInt("mt-MT", 0x43a),
            new StringAndInt("mni-IN", 0x458),
            new StringAndInt("mi-NZ", 0x481),
            new StringAndInt("arn-CL", 0x47a),
            new StringAndInt("mr-IN", 0x44e),
            new StringAndInt("moh-CA", 0x47c),
            new StringAndInt("mn-MN", 0x450),
            new StringAndInt("mn-MN", 0x850),
            new StringAndInt("ne-NP", 0x461),
            new StringAndInt("ne-IN", 0x861),
            new StringAndInt("no-NO", 0x14),
            new StringAndInt("nb-NO", 0x414),
            new StringAndInt("nn-NO", 0x814),
            new StringAndInt("oc-FR", 0x482),
            new StringAndInt("or-IN", 0x448),
            new StringAndInt("om-ET", 0x472),
            new StringAndInt("pap-AN", 0x479),
            new StringAndInt("ps-AF", 0x463),
            new StringAndInt("pl-PL", 0x415),
            new StringAndInt("pt-PT", 0x816),
            new StringAndInt("pt-BR", 0x416),
            new StringAndInt("pa-IN", 0x446),
            new StringAndInt("lah-PK", 0x846),
            new StringAndInt("qu-BO", 0x46b),
            new StringAndInt("qu-EC", 0x86b),
            new StringAndInt("qu-PE", 0xc6b),
            new StringAndInt("rm-CH", 0x417),
            new StringAndInt("ro-RO", 0x418),
            new StringAndInt("ro-MD", 0x818),
            new StringAndInt("ru-RU", 0x419),
            new StringAndInt("mo-MD", 0x819),
            new StringAndInt("se-NO", 0x43b),
            new StringAndInt("smn-FI", 0x243b),
            new StringAndInt("smj-NO", 0x103b),
            new StringAndInt("smj-SE", 0x143b),
            new StringAndInt("se-FI", 0xc3b),
            new StringAndInt("se-SE", 0x83b),
            new StringAndInt("sms-FI", 0x203b),
            new StringAndInt("sma-NO", 0x183b),
            new StringAndInt("sma-SE", 0x1c3b),
            new StringAndInt("sa-IN", 0x44f),
            new StringAndInt("nso-ZA", 0x46c),
            new StringAndInt("sr", 0x1a),
            new StringAndInt("sr-YU", 0xc1a),
            new StringAndInt("sr-BA", 0x1c1a),
            new StringAndInt("sh-YU", 0x81a),
            new StringAndInt("sh-BA", 0x181a),
            new StringAndInt("sh", 0x7c1a),
            new StringAndInt("st-ZA", 0x430),
            new StringAndInt("sd-IN", 0x459),
            new StringAndInt("sd-PK", 0x859),
            new StringAndInt("si-LK", 0x45b),
            new StringAndInt("sk-SK", 0x41b),
            new StringAndInt("sl-SI", 0x424),
            new StringAndInt("so-SO", 0x477),
            new StringAndInt("hsb-DE", 0x42e),
            new StringAndInt("dsb-DE", 0x82e),
            new StringAndInt("es-ES", 0x40a),
            new StringAndInt("es-AR", 0x2c0a),
            new StringAndInt("es-BO", 0x400a),
            new StringAndInt("es-CL", 0x340a),
            new StringAndInt("es-CO", 0x240a),
            new StringAndInt("es-CR", 0x140a),
            new StringAndInt("es-DO", 0x1c0a),
            new StringAndInt("es-EC", 0x300a),
            new StringAndInt("es-SV", 0x440a),
            new StringAndInt("es-GT", 0x100a),
            new StringAndInt("es-HN", 0x480a),
            new StringAndInt("es", 0xe40a),
            new StringAndInt("es-MX", 0x80a),
            new StringAndInt("es-ES", 0xc0a),
            new StringAndInt("es-NI", 0x4c0a),
            new StringAndInt("es-PA", 0x180a),
            new StringAndInt("es-PY", 0x3c0a),
            new StringAndInt("es-PE", 0x280a),
            new StringAndInt("es-PR", 0x500a),
            new StringAndInt("es-US", 0x540a),
            new StringAndInt("es-UY", 0x380a),
            new StringAndInt("es-VE", 0x200a),
            new StringAndInt("sw-KE", 0x441),
            new StringAndInt("sv-SE", 0x41d),
            new StringAndInt("sv-FI", 0x81d),
            new StringAndInt("syr-TR", 0x45a),
            new StringAndInt("tg-TJ", 0x428),
//            new StringAndInt("", 0x45f),
//            new StringAndInt("", 0x85f),
            new StringAndInt("ta-IN", 0x449),
            new StringAndInt("tt-RU", 0x444),
            new StringAndInt("te-IN", 0x44a),
            new StringAndInt("th-TH", 0x41e),
            new StringAndInt("bo-CN", 0x451),
            new StringAndInt("dz-BT", 0x851),
            new StringAndInt("ti-ER", 0x873),
            new StringAndInt("ti-ET", 0x473),
            new StringAndInt("ts-ZA", 0x431),
            new StringAndInt("tn-ZA", 0x432),
            new StringAndInt("tr-TR", 0x41f),
            new StringAndInt("tk-TM", 0x442),
            new StringAndInt("ug-CN", 0x480),
            new StringAndInt("uk-UA", 0x422),
            new StringAndInt("ur", 0x20),
            new StringAndInt("ur-IN", 0x820),
            new StringAndInt("ur-PK", 0x420),
//            new StringAndInt("", 0x843),
            new StringAndInt("uz-UZ", 0x443),
            new StringAndInt("ve-ZA", 0x433),
            new StringAndInt("vi-VN", 0x42a),
            new StringAndInt("cy-GB", 0x452),
            new StringAndInt("wo-SN", 0x488),
            new StringAndInt("xh-ZA", 0x434),
            new StringAndInt("sah-RU", 0x485),
            new StringAndInt("ii-CN", 0x478),
            new StringAndInt("yi-IL", 0x43d),
            new StringAndInt("yo-NG", 0x46a),
            new StringAndInt("zu-ZA", 0x435),
                null};
            int index = 0;
            while( mapping[index] != null) {
                localeToLanguageMap.put( mapping[index].locale, mapping[index].msValue );
                languageToLocaleMap.put( mapping[index].msValue, mapping[index].locale );
                ++index;
            }
        }
    }
    /**
    * converts a language code from a string and interpretes the number as hex value and returns the
    * a locale or null if either the format is wrong or no locale is known
    */
    static public String getLocaleFromLangCode( String languageCode ) {
        if(languageToLocaleMap == null) {
            fillLocaleMaps();
        }
        String ret = "";
        try{
        int languageValue = Integer.parseInt(languageCode, 16);
            ret = languageToLocaleMap.get(languageValue);
        } catch( NumberFormatException e ) {
            //no handling required
        }
        return ret;
    }
    /**
    * converts locale information to a hex value string but without a hex marker like '0x'
    * returns null if no value can be found
    */

    static public String getMSLangCode(String language, String country) {
        if(localeToLanguageMap == null) {
            fillLocaleMaps();
        }
        String cmpString = new String(language);
        if(country!=null&&!country.isEmpty()){
            cmpString += "-" + country;
        }
        Integer entry = localeToLanguageMap.get(cmpString);
        if(entry != null) {
            return Integer.toHexString(entry.intValue());
        }
        return "";
    }
}
