/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class TableRowProperties extends StylePropertiesBase {

	private DLList<IElementWriter> content;

	public TableRowProperties(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	@Override
	public DLList<IElementWriter> getContent() {
		if(content==null) {
			content = new DLList<IElementWriter>();
		}
		return content;
	}

	@Override
	public String getQName() {
		return "style:table-row-properties";
	}

	@Override
	public String getLocalName() {
		return "table-row-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs)
		throws JSONException {

		final JSONObject rowAttrs = attrs!=null ? attrs.optJSONObject(OCKey.ROW.value()) : null;
		if(rowAttrs!=null) {
	    	final Iterator<Entry<String, Object>> rowAttrsIter = rowAttrs.entrySet().iterator();
	    	while(rowAttrsIter.hasNext()) {
	    		final Entry<String, Object> rowAttrsEntry = rowAttrsIter.next();
				final Object rowAttrsValue = rowAttrsEntry.getValue();
				if(rowAttrsValue!=null) {
	        		switch(OCKey.fromValue(rowAttrsEntry.getKey())) {
	        			case HEIGHT : {
	        				if(rowAttrsValue==JSONObject.NULL) {
	        					attributes.remove("style:row-height");
	        				}
	        				else {
	        					attributes.setLength100thmm(Namespaces.STYLE, "row-height", "style:row-height", ((Number)rowAttrsValue).intValue());
	        				}
	        				break;
	        			}
						case FILL_COLOR : {
							if(rowAttrsValue==JSONObject.NULL) {
								attributes.remove("fo:background-color");
							}
							else {
								attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", PropertyHelper.getColor((JSONObject)rowAttrsValue, "transparent"));
							}
							break;
						}
						case CUSTOM_HEIGHT : {
							if(rowAttrsValue==JSONObject.NULL) {
								attributes.remove("style:use-optimal-row-height");
							}
							else if(rowAttrsValue instanceof Boolean) {
								attributes.setValue(Namespaces.STYLE, "use-optimal-row-height", "style:use-optimal-row-height", Boolean.toString((!(Boolean)rowAttrsValue)));
							}
							break;
						}
	        		}
				}
	    	}
		}
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		final Map<String, Object> rowAttrs = attrs.getMap(OCKey.ROW.value(), true);
		final String backgroundColor = attributes.getValue("fo:background-color");
		if(backgroundColor!=null) {
            final Map<String, Object> color = PropertyHelper.createColorMap(backgroundColor);
            if(color!=null) {
                rowAttrs.put(OCKey.FILL_COLOR.value(), color);
            }
		}
		final Integer minRowHeight = attributes.getLength100thmm("style:min-row-height", false);
		if(minRowHeight!=null) {
			rowAttrs.put(OCKey.HEIGHT.value(), minRowHeight);
		}
		final Integer rowHeight = attributes.getLength100thmm("style:row-height", false);
		if(rowHeight!=null) {
			rowAttrs.put(OCKey.HEIGHT.value(), rowHeight);
		}
		final String useOptimalRowHeight = attributes.getValue("style:use-optimal-row-height");
		if(useOptimalRowHeight!=null) {
			rowAttrs.put(OCKey.CUSTOM_HEIGHT.value(), !Boolean.parseBoolean(useOptimalRowHeight));
		}
		if(rowAttrs.isEmpty()) {
			attrs.remove(OCKey.ROW.value());
		}
    }

	@Override
	public TableRowProperties clone() {
		return (TableRowProperties)_clone();
	}
}
