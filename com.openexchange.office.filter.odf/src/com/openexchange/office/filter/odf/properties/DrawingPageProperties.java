/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Border;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.DrawFillImage;
import com.openexchange.office.filter.odf.styles.Gradient;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class DrawingPageProperties extends StylePropertiesBase {

	private DLList<IElementWriter> content;

	public DrawingPageProperties(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	@Override
	public DLList<IElementWriter> getContent() {
		if(content==null) {
			content = new DLList<IElementWriter>();
		}
		return content;
	}

	@Override
	public String getQName() {
		return "style:drawing-page-properties";
	}

	@Override
	public String getLocalName() {
		return "drawing-page-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}

    @Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs)
	    throws JSONException {

	    final JSONObject fillAttributes = attrs.optJSONObject(OCKey.FILL.value());
        if(fillAttributes!=null) {
            final Object type = fillAttributes.opt(OCKey.TYPE.value());
            if(type!=null) {
                if(type==JSONObject.NULL) {
                    attributes.remove("draw:fill");
                    attributes.remove("draw:fill-color");
                    attributes.remove("fo:background-color");
                    attributes.remove("draw:fill-image-name");
                    attributes.remove("draw:fill-gradient-name");
                    attributes.remove("presentation:background-visible");
                    attributes.remove("style:repeat");
                }
                else if(type instanceof String) {
                    String drawFill = "solid";
                    if("none".equals(type)) {
                        drawFill = "none";
                    }
                    else if("bitmap".equals(type)) {
                        drawFill = "bitmap";
                    }
                    else if("gradient".equals(type)) {
                        drawFill = "gradient";
                    }
                    attributes.setValue(Namespaces.DRAW, "fill", "draw:fill", drawFill);
                }
            }
            final JSONObject color = fillAttributes.optJSONObject(OCKey.COLOR.value());
            if(color!=null) {
                if (color==JSONObject.NULL) {
                    attributes.remove("fo:color");
                    attributes.remove("fo:background-color");
                }
                else {
                    final String colorType = color.optString(OCKey.TYPE.value(), null);
                    if(colorType!=null) {
                        if(colorType.equals("auto")) {
                            attributes.setValue(Namespaces.DRAW, "fill-color", "draw:fill-color", "#ffffff");
                            attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", "#ffffff");
                        }
                        else {
                            attributes.setValue(Namespaces.DRAW, "fill-color", "draw:fill-color", PropertyHelper.getColor(color, null));
                            attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", PropertyHelper.getColor(color, null));
                        }
                    }
                }
            }
            final JSONObject bitmapAttrs = fillAttributes.optJSONObject(OCKey.BITMAP.value());
            if(bitmapAttrs!=null) {
                attributes.setValue(Namespaces.DRAW,  "fill-image-name",  "draw:fill-image-name", DrawFillImage.getFillImage(styleManager, attributes.getValue("draw:fill-image-name"), bitmapAttrs));
                DrawFillImage.applyTilingAttrs(bitmapAttrs, getAttributes());
            }
            final JSONObject gradientAttrs = fillAttributes.optJSONObject(OCKey.GRADIENT.value());
            if(gradientAttrs!=null) {
                attributes.setValue(Namespaces.DRAW, "fill-gradient-name", "draw:fill-gradient-name", Gradient.getGradient(styleManager, attributes.getValue("draw:fill-gradient-name"), fillAttributes));
            }
        }
	    final JSONObject slideAttrs = attrs.optJSONObject(OCKey.SLIDE.value());
	    if(slideAttrs!=null) {
	        final Iterator<Entry<String, Object>> slideAttrsIter = slideAttrs.entrySet().iterator();
	        while(slideAttrsIter.hasNext()) {
	            final Entry<String, Object> entry = slideAttrsIter.next();
	            final Object o = entry.getValue();
	            switch(OCKey.fromValue(entry.getKey())) {
	                case FOLLOW_MASTER_SHAPES : {
	                    if(o instanceof Boolean) {
	                        getAttributes().setBooleanValue(Namespaces.PRESENTATION, "background-objects-visible", "presentation:background-objects-visible", (Boolean)o);
	                    }
	                    else if (o==JSONObject.NULL){
	                        getAttributes().remove("presentation:background-objects-visible");
	                    }
	                    break;
	                }
                    case IS_SLIDE_NUM : {
                        if(o instanceof Boolean) {
                            getAttributes().setBooleanValue(Namespaces.PRESENTATION, "display-page-number", "presentation:display-page-number", (Boolean)o);
                        }
                        else if(o== JSONObject.NULL) {
                            getAttributes().remove("presentation:display-page-number");
                        }
                        break;
                    }
	                case IS_HEADER : {
                        if(o instanceof Boolean) {
                            getAttributes().setBooleanValue(Namespaces.PRESENTATION, "display-header", "presentation:display-header", (Boolean)o);
                        }
                        else if(o== JSONObject.NULL) {
                            getAttributes().remove("presentation:display-header");
                        }
                        break;
                    }
                    case IS_FOOTER : {
                        if(o instanceof Boolean) {
                            getAttributes().setBooleanValue(Namespaces.PRESENTATION, "display-footer", "presentation:display-footer", (Boolean)o);
                        }
                        else if(o== JSONObject.NULL) {
                            getAttributes().remove("presentation:display-footer");
                        }
                        break;
                    }
                    case IS_DATE : {
                        if(o instanceof Boolean) {
                            getAttributes().setBooleanValue(Namespaces.PRESENTATION, "display-date-time", "presentation:display-date-time", (Boolean)o);
                        }
                        else if(o== JSONObject.NULL) {
                            getAttributes().remove("presentation:display-date-time");
                        }
                        break;
                    }
                    case HIDDEN : {
                        if(o instanceof Boolean&&((Boolean)o).booleanValue()) {
                            getAttributes().setValue(Namespaces.PRESENTATION, "visibility", "presentation:visibility", "hidden");
                        }
                        else {
                            getAttributes().remove("presentation:visibility");
                        }
                        break;
                    }
	            }
	        }
	    }
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
	    final OpAttrs pageAttrs = attrs.getMap(OCKey.PAGE.value(), true);
        final OpAttrs fillAttrs = attrs.getMap(OCKey.FILL.value(), true);
        final OpAttrs slideAttrs = attrs.getMap(OCKey.SLIDE.value(), true);

        boolean disableMasterBackground = false;
		PropertyHelper.createDefaultMarginAttrs(pageAttrs, attributes);
        final Integer defaultPadding = PropertyHelper.createDefaultPaddingAttrs(pageAttrs, attributes);
        Border.createDefaultBorderMapAttrsFromFoBorder(pageAttrs, defaultPadding, attributes);
        final Iterator<Entry<String, AttributeImpl>> propIter = attributes.getUnmodifiableMap().entrySet().iterator();
        while(propIter.hasNext()) {
        	final Entry<String, AttributeImpl> propEntry = propIter.next();
        	final String propName = propEntry.getKey();
        	final String propValue = propEntry.getValue().getValue();
        	switch(propName) {
	    		case "fo:margin-left": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	                if(margin!=null) {
	                	pageAttrs.put(OCKey.MARGIN_LEFT.value(), margin);
	                	pageAttrs.put(OCKey.INDENT_LEFT.value(), margin);
	                }
	    			break;
	    		}
	    		case "fo:margin-top": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	                if(margin!=null) {
	                	pageAttrs.put(OCKey.MARGIN_TOP.value(), margin);
	                }
	    			break;
	    		}
	    		case "fo:margin-right": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	    			if(margin!=null) {
	    				pageAttrs.put(OCKey.MARGIN_RIGHT.value(), margin);
	    				pageAttrs.put(OCKey.INDENT_RIGHT.value(), margin);
	    			}
	    			break;
	    		}
	    		case "fo:margin-bottom": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	    			if(margin!=null) {
	    				pageAttrs.put(OCKey.MARGIN_BOTTOM.value(), margin);
	    			}
	    			break;
	    		}
	    		case "fo:padding-left": {
	    			final Integer padding = AttributesImpl.normalizeLength(propValue, true);
	    			if(padding!=null) {
	    				pageAttrs.put(OCKey.PADDING_LEFT.value(), padding);
	    			}
	    			break;
	    		}
	    		case "fo:padding-top": {
	    			final Integer padding = AttributesImpl.normalizeLength(propValue, true);
	    			if(padding!=null) {
	    				pageAttrs.put(OCKey.PADDING_TOP.value(), padding);
	    			}
	    			break;
	    		}
	    		case "fo:padding-right": {
	    			final Integer padding = AttributesImpl.normalizeLength(propValue, true);
	    			if(padding!=null) {
	    				pageAttrs.put(OCKey.PADDING_RIGHT.value(), padding);
	    			}
	    			break;
	    		}
	    		case "fo:padding-bottom": {
	    			final Integer padding = AttributesImpl.normalizeLength(propValue, true);
	    			if(padding!=null) {
	    				pageAttrs.put(OCKey.PADDING_BOTTOM.value(), padding);
	    			}
	    			break;
	    		}
	    		case "fo:border-left": {
	    			final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
	                if(border!=null) {
	                	pageAttrs.put(OCKey.BORDER_LEFT.value(), border);
	                }
	    			break;
	    		}
	    		case "fo:border-top": {
	    			final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
	                if(border!=null) {
	                	pageAttrs.put(OCKey.BORDER_TOP.value(), border);
	                }
	    			break;
	    		}
	    		case "fo:border-right": {
	    			final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
	                if(border!=null) {
	                	pageAttrs.put(OCKey.BORDER_RIGHT.value(), border);
	                }
	    			break;
	    		}
	    		case "fo:border-bottom": {
	    			final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
	                if(border!=null) {
	                	pageAttrs.put(OCKey.BORDER_BOTTOM.value(), border);
	                }
	    			break;
	    		}
	    		case "fo:page-width": {
    				pageAttrs.put(OCKey.WIDTH.value(), AttributesImpl.normalizeLength(propValue));
	    			break;
	    		}
	    		case "fo:page-height": {
    				pageAttrs.put(OCKey.HEIGHT.value(), AttributesImpl.normalizeLength(propValue));
	    			break;
	    		}
	    		case "style:print-orientation": {
	    			pageAttrs.put(OCKey.PRINT_ORIENTATION.value(), propValue);
	       			break;
	       	  	}
	    		case "style:num-format": {
	                pageAttrs.put(OCKey.NUMBER_FORMAT.value(), propValue);
	                break;
	            }
                case "draw:fill": {
                    if(propValue.equals("none")) {
                        fillAttrs.put(OCKey.TYPE.value(), "none");
                    }
                    else if(propValue.equals("bitmap")) {
                        fillAttrs.put(OCKey.TYPE.value(), "bitmap");
                    }
                    else if(propValue.equals("gradient")) {
                        fillAttrs.put(OCKey.TYPE.value(), "gradient");
                    }
                    else {
                        fillAttrs.put(OCKey.TYPE.value(), "solid");
                    }
                    break;
                }
                case "draw:fill-image-name": {
                    final StyleBase styleBase = styleManager.getStyle(propValue, StyleFamily.FILL_IMAGE, contentAutoStyle);
                    if(styleBase!=null) {
                        styleBase.createAttrs(styleManager, attrs);
                        final Object imageUrl = fillAttrs.opt(OCKey.BITMAP.value(), OCKey.IMAGE_URL.value());
                        if(imageUrl instanceof String) {
                            String repeat = attributes.getValue("style:repeat");
                            if(repeat==null) {
                                repeat = "repeat";
                            }
                            DrawFillImage.createTilingAttrs(styleManager, (String)imageUrl, repeat, attributes, fillAttrs);
                        }
                    }
                    if(!attributes.containsKey("draw:fill")) {
                        fillAttrs.put(OCKey.TYPE.value(), "bitmap");
                    }
                    break;
                }
                case "draw:fill-gradient-name": {
                    StyleBase styleBase = styleManager.getStyle(propValue, StyleFamily.GRADIENT, contentAutoStyle);
                    if(styleBase==null) {
                        styleBase = new Gradient(null);
                    }
                    styleBase.createAttrs(styleManager, attrs);
                    if(!attributes.containsKey("draw:fill")) {
                        fillAttrs.put(OCKey.TYPE.value(), "gradient");
                    }
                    break;
                }
                case "fo:background-color": {
                    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
                    if(color!=null) {
                        fillAttrs.put(OCKey.COLOR.value(), color);
                    }
                    if(!attributes.containsKey("draw:fill")) {
                        fillAttrs.put(OCKey.TYPE.value(), "solid");
                    }
                    break;
                }
                case "draw:fill-color": {
                    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
                    if(color!=null) {
                        fillAttrs.put(OCKey.COLOR.value(), color);
                    }
                    if(!attributes.containsKey("draw:fill")) {
                        fillAttrs.put(OCKey.TYPE.value(), "solid");
                    }
                    break;
                }
                case "presentation:background-visible": {
                    if(!AttributesImpl.getBoolean(propValue, Boolean.TRUE).booleanValue()) {
                        disableMasterBackground = true;
                    }
                    break;
                }
                case "presentation:background-objects-visible": {
                    if(!AttributesImpl.getBoolean(propValue, Boolean.TRUE).booleanValue()) {
                        slideAttrs.put(OCKey.FOLLOW_MASTER_SHAPES.value(), false);
                    }
                    break;
                }
                case "presentation:display-header": {
                    slideAttrs.put(OCKey.IS_HEADER.value(), AttributesImpl.getBoolean(propValue, Boolean.FALSE));
                    break;
                }
                case "presentation:display-footer": {
                    slideAttrs.put(OCKey.IS_FOOTER.value(), AttributesImpl.getBoolean(propValue, Boolean.FALSE));
                    break;
                }
                case "presentation:display-page-number": {
                    slideAttrs.put(OCKey.IS_SLIDE_NUM.value(), AttributesImpl.getBoolean(propValue, Boolean.FALSE));
                    break;
                }
                case "presentation:display-date-time": {
                    slideAttrs.put(OCKey.IS_DATE.value(), AttributesImpl.getBoolean(propValue, Boolean.FALSE));
                    break;
                }
                case "presentation:visibility": {
                    if(propValue.equals("hidden")) {
                        slideAttrs.put(OCKey.HIDDEN.value(), Boolean.TRUE);
                    }
                    break;
                }
        	}
        }
        if(disableMasterBackground) {
            final Object masterBackground = fillAttrs.get(OCKey.TYPE.value());
            if(!(masterBackground instanceof String)) {
                fillAttrs.put(OCKey.TYPE.value(), "solid");
            }
        }
        if(slideAttrs.isEmpty()) {
            attrs.remove(OCKey.SLIDE.value());
        }
        if(pageAttrs.isEmpty()) {
            attrs.remove(OCKey.PAGE.value());
        }
        if(fillAttrs.isEmpty()) {
            attrs.remove(OCKey.FILL.value());
        }
	}

	@Override
	public DrawingPageProperties clone() {
		return (DrawingPageProperties)_clone();
	}
}
