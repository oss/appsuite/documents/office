/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class ChartProperties extends StylePropertiesBase {

    private final static HashMap<String, ArrayList<HandleInfo>> handles;

    private static void putHandle(String key, Object value, String targetMap, String targetKey, Object targetVal) {
        ArrayList<HandleInfo> list = handles.get(key);
        if (list == null) {
            list = new ArrayList<>();
            handles.put(key, list);
        }
        list.add(new HandleInfo(value.toString(), targetMap, targetKey, targetVal));
    }

    static {
        handles = new HashMap<>();

        putHandle("chart:percentage", true, OCKey.CHART.value(), OCKey.STACKING.value(), "percentStacked");
        putHandle("chart:stacked", true, OCKey.CHART.value(), OCKey.STACKING.value(), "stacked");
        putHandle("chart:interpolation", "cubic-spline", OCKey.CHART.value(), OCKey.CURVED.value(), true);

        putHandle("chart:display-label", true, OCKey.AXIS.value(), OCKey.LABEL.value(), true);

        putHandle("chart:data-label-number", "value", OCKey.SERIES.value(), OCKey.DATA_LABEL.value(), "value");
        putHandle("chart:data-label-number", "percentage", OCKey.SERIES.value(), OCKey.DATA_LABEL.value(), "percent");
        putHandle("chart:data-label-number", "value-and-percentage", OCKey.SERIES.value(), OCKey.DATA_LABEL.value(), "value;percent");
    }

    private DLList<IElementWriter> content;

    public ChartProperties(AttributesImpl attributesImpl) {
        super(attributesImpl);
    }

    @Override
    public DLList<IElementWriter> getContent() {
        if (content == null) {
            content = new DLList<IElementWriter>();
        }
        return content;
    }

    @Override
    public String getQName() {
        return "style:chart-properties";
    }

    @Override
    public String getLocalName() {
        return "chart-properties";
    }

    @Override
    public String getNamespace() {
        return Namespaces.STYLE;
    }

    @Override
    public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
        // do nothing
    }

    @Override
    public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
        Map<String, AttributeImpl> map = attributes.getUnmodifiableMap();

        for (String key : map.keySet()) {
            if (key.equals("chart:angle-offset")) {
                attrs.getMap(OCKey.CHART.value(), true).put(OCKey.ROTATION.value(), attributes.getIntValue("chart:angle-offset"));
            } else if (key.equals("chart:visible") && !attributes.getBooleanValue("chart:visible", true)) {
                attrs.getMap(OCKey.LINE.value(), true).put(OCKey.TYPE.value(), "none");
                attrs.remove(OCKey.AXIS.value());
            } else if (key.equals("chart:label-position")) {
                String position = getDataLabelPosition(attributes.getValue("chart:label-position"));
                if (position != null) {
                    try {
                        OpAttrs target = attrs.getMap(OCKey.SERIES.value(), true);
                        if (target != null) {
                            if (target.get(OCKey.DATA_LABEL.value()) instanceof JSONObject) {
                                ((JSONObject)target.get(OCKey.DATA_LABEL.value())).put(OCKey.DATA_LABEL_POS.value(), position);
                            } else {
                                JSONObject dataLabelJSON = new JSONObject();
                                dataLabelJSON.put(OCKey.DATA_LABEL_POS.value(), position);
                                target.put(OCKey.DATA_LABEL.value(), dataLabelJSON);
                            }
                        }

                    } catch (JSONException e) {
                        // do nothing
                    }   
                }
            } else {
                if (key.equals("chart:display-label") && attrs.containsKey(OCKey.LINE.value())) {
                    OpAttrs lineAttrs = attrs.getMap(OCKey.LINE.value(), false);
                    if (lineAttrs != null && lineAttrs.containsKey(OCKey.TYPE.value()) && "none".equals(lineAttrs.get(OCKey.TYPE.value()))) {
                        continue;
                    }
                }
                ArrayList<HandleInfo> handleList = handles.get(key);
                if (handleList != null) {
                    for (HandleInfo handle : handleList) {
                        if (StringUtils.equals(attributes.getValue(key), handle.value)) {
                            OpAttrs target = attrs.getMap(handle.targetMap, true);
                            if (key.equals("chart:data-label-number")) {
                                try {
                                    JSONArray showOptions = new JSONArray();
                                    showOptions.put(handle.targetVal);

                                    if (target.get(handle.targetKey) instanceof JSONObject) {
                                        ((JSONObject)target.get(handle.targetKey)).put(OCKey.DATA_LABEL_TEXT.value(), showOptions);
                                    } else {
                                        JSONObject dataLabelJSON = new JSONObject();
                                        dataLabelJSON.put(OCKey.DATA_LABEL_TEXT.value(), showOptions);
                                        target.put(handle.targetKey, dataLabelJSON);   
                                    }

                                } catch (JSONException e) {
                                    target.put(handle.targetKey, handle.targetVal);   
                                }  
                            } else {
                                target.put(handle.targetKey, handle.targetVal);   
                            }
                        }
                    }
                }
            }
            
        }
    }
    
    
    private String getDataLabelPosition(String dataLabelPos) {
        String position = null;
        if (dataLabelPos != null) {
            
            switch(dataLabelPos.toLowerCase()) {
                case "bottom":
                    position = OCKey.DATA_LABEL_POS_BOTTOM.value();
                    break;
                case "center":
                    position = OCKey.DATA_LABEL_POS_CENTER.value();
                    break;
                case "near-origin":
                    position = OCKey.DATA_LABEL_POS_INSIDE_BASE.value();
                    break;
                case "inside":
                    position = OCKey.DATA_LABEL_POS_INSIDE.value();
                    break;
                case "left":
                    position = OCKey.DATA_LABEL_POS_LEFT.value();
                    break;
                case "outside":
                    position = OCKey.DATA_LABEL_POS_OUTSIDE.value();
                    break;
                case "right":
                    position = OCKey.DATA_LABEL_POS_RIGHT.value();
                    break;
                case "above":
                    position = OCKey.DATA_LABEL_POS_ABOVE.value();
                    break;  
                case "best-fit":
                    position = OCKey.DATA_LABEL_POS_BEST_FIT.value();
                    break; 
            }
        }
        return position;
    }

    @Override
    public ChartProperties clone() {
        return (ChartProperties) _clone();
    }

    private static class HandleInfo {

        final String value;
        final String targetMap;
        final String targetKey;
        final Object targetVal;

        HandleInfo(String value, String targetMap, String targetKey, Object targetVal) {
            this.value = value;
            this.targetMap = targetMap;
            this.targetKey = targetKey;
            this.targetVal = targetVal;
        }

    }
}
