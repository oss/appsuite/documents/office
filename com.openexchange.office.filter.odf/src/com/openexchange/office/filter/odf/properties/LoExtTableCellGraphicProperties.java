/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Border;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StrokeDashStyle;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class LoExtTableCellGraphicProperties extends GraphicProperties {

	public LoExtTableCellGraphicProperties(AttributesImpl attributesImpl, StyleBase parentStyle, boolean defaultProperties) {
		super(attributesImpl, parentStyle, defaultProperties);
	}

	@Override
	public String getQName() {
		return "loext:graphic-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.LOEXT;
	}

    @Override
    public void applyAttrs(StyleManager styleManager, JSONObject attrs)
        throws JSONException {

        final JSONObject cellAttributes = attrs.optJSONObject(OCKey.CELL.value());
        if(cellAttributes!=null) {
            final Object fillColor = cellAttributes.opt(OCKey.FILL_COLOR.value());
            if(fillColor!=null) {
                if(fillColor==JSONObject.NULL) {
                    attributes.remove("draw:fill");
                }
                else if(fillColor instanceof JSONObject) {
                    attributes.setValue(Namespaces.DRAW, "fill-color", "draw:fill-color", PropertyHelper.getColor((JSONObject)fillColor, null));
                }
            }

            // DOCS-3091
            final Object vertAlign = cellAttributes.opt(OCKey.ALIGN_VERT.value());
            if(vertAlign!=null) {
                if(vertAlign==JSONObject.NULL) {
                    attributes.remove("draw:textarea-vertical-align");
                }
                else {
                    final String vAlign;
                    if("center".equals(vertAlign)) {
                        vAlign = "middle";
                    }
                    else if("bottom".equals(vertAlign)) {
                        vAlign = "bottom";
                    }
                    else {
                        vAlign = "top";
                    }
                    attributes.setValue(Namespaces.STYLE, "textarea-vertical-align", "draw:textarea-vertical-align", vAlign);
                }
            }

            // to be removed if fill & line family is supported
            final Object fillType = cellAttributes.opt(OCKey.FILL_TYPE.value());
            if(fillType!=null) {
                if(fillType==JSONObject.NULL) {
                    attributes.remove("draw:fill");
                }
                else {
                    String newFillType = "solid";
                    if("none".equals(fillType)) {
                        newFillType = "none";
                    }
                    else if("bitmap".equals(fillType)) {
                        newFillType = "bitmap";
                    }
                    else if("gradient".equals(fillType)) {
                        newFillType = "gradient";
                    }
                    attributes.setValue(Namespaces.DRAW, "fill", "draw:fill", newFillType);
                }
            }
            final Object paddingBottom = cellAttributes.opt(OCKey.PADDING_BOTTOM.value());
            if(paddingBottom!=null) {
                if (paddingBottom==JSONObject.NULL) {
                    attributes.remove("fo:padding-bottom");
                } else if (paddingBottom instanceof Number) {
                    attributes.setValue(Namespaces.FO, "padding-bottom", "fo:padding-bottom", (((Number)paddingBottom).intValue() / 100) + "mm");
                }
            }
            final Object paddingLeft = cellAttributes.opt(OCKey.PADDING_LEFT.value());
            if(paddingLeft!=null) {
                if (paddingLeft==JSONObject.NULL) {
                    attributes.remove("fo:padding-left");
                } else if(paddingLeft instanceof Number) {
                    attributes.setValue(Namespaces.FO, "padding-left", "fo:padding-left", (((Number)paddingLeft).intValue() / 100) + "mm");
                }
            }
            final Object paddingRight = cellAttributes.opt(OCKey.PADDING_RIGHT.value());
            if(paddingRight!=null) {
                if (paddingRight==JSONObject.NULL) {
                    attributes.remove("fo:padding-right");
                } else if(paddingRight instanceof Number) {
                    attributes.setValue(Namespaces.FO, "padding-right", "fo:padding-right", (((Number)paddingRight).intValue() / 100) + "mm");
                }
            }
            final Object paddingTop = cellAttributes.opt(OCKey.PADDING_TOP.value());
            if(paddingTop!=null) {
                if (paddingTop==JSONObject.NULL) {
                    attributes.remove("fo:padding-top");
                } else if(paddingTop instanceof Number){
                    attributes.setValue(Namespaces.FO, "padding-top", "fo:padding-top", (((Number)paddingTop).intValue() / 100) + "mm");
                }
            }
        }

        // TODO: line attributes ..

        final JSONObject lineAttributes = attrs.optJSONObject(OCKey.LINE.value());
        if(lineAttributes!=null) {
            if(getIsFrameOrGraphic()) {
                final Border border = new Border();
                border.applyFoBorder(attributes.getValue("fo:border"));
                border.applyJsonLine(lineAttributes);
                final String foBorder = border.toString();
                if(foBorder!=null) {
                    attributes.setValue(Namespaces.FO, "border", "fo:border", foBorder);
                }
            }
            else {
                final Object type = lineAttributes.opt(OCKey.TYPE.value());
                if(type!=null) {
                    if(type==JSONObject.NULL) {
                        attributes.remove("draw:stroke");
                    }
                    else if (type instanceof String) {
                        if(((String)type).equals("none")) {
                            attributes.setValue(Namespaces.DRAW, "stroke", "draw:stroke", "none");
                        }
                        else if(((String)type).equals("solid")) {
                            final Object lineStyle = lineAttributes.opt(OCKey.STYLE.value());
                            if(lineStyle!=null) {
                                if(lineStyle==JSONObject.NULL||((String)lineStyle).equals("solid")) {
                                    attributes.setValue(Namespaces.DRAW, "stroke", "draw:stroke", "solid");
                                }
                                else {
                                    attributes.setValue(Namespaces.DRAW, "stroke", "draw:stroke", "dash");
                                    attributes.setValue(Namespaces.DRAW, "stroke-dash", "draw:stroke-dash", StrokeDashStyle.getStrokeDash(styleManager, (String)lineStyle));
                                }
                            }
                            else {
                                final String stroke = attributes.getValue("draw:stroke");
                                if(stroke==null||stroke.isEmpty()) {
                                    attributes.setValue(Namespaces.DRAW, "stroke", "draw:stroke", "solid");
                                }
                            }
                        }
                    }
                }
                final JSONObject color = lineAttributes.optJSONObject(OCKey.COLOR.value());
                if(color!=null) {
                    if (color==JSONObject.NULL) {
                        attributes.remove("svg:stroke-color");
                    }
                    else {
                        final String colorType = color.optString(OCKey.TYPE.value(), null);
                        if(colorType!=null) {
                            if(colorType.equals("auto")) {
                                attributes.setValue(Namespaces.SVG, "stroke-color", "svg:stroke-color", "#000000");
                            }
                            else {
                                attributes.setValue(Namespaces.SVG, "stroke-color", "svg:stroke-color", PropertyHelper.getColor(color, null));
                            }
                        }
                    }
                }
                final Object strokeWidth = lineAttributes.opt(OCKey.WIDTH.value());
                if(strokeWidth!=null) {
                    if(strokeWidth==JSONObject.NULL) {
                        attributes.remove("svg:stroke-width");
                    }
                    else if(strokeWidth instanceof Number) {
                        attributes.setValue(Namespaces.SVG, "stroke-width", "svg:stroke-width", (((Number)strokeWidth).doubleValue() / 100) + "mm");
                    }
                }
            }
        }
    }

    @Override
    public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
        final Map<String, Object> cellAttrs = attrs.getMap(OCKey.CELL.value(), true);
        final Integer defaultPadding = PropertyHelper.createDefaultPaddingAttrs(cellAttrs, attributes);
        Border.createDefaultBorderMapAttrsFromFoBorder(cellAttrs, defaultPadding, attributes);

        boolean hasDrawStroke = false;

        final Iterator<Entry<String, AttributeImpl>> propIter = attributes.getUnmodifiableMap().entrySet().iterator();
        while(propIter.hasNext()) {
            final Entry<String, AttributeImpl> propEntry = propIter.next();
            final String propName = propEntry.getKey();
            final String propValue = propEntry.getValue().getValue();
            switch(propName) {
                case "fo:padding-left": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if(padding!=null) {
                        cellAttrs.put(OCKey.PADDING_LEFT.value(), padding);
                    }
                    break;
                }
                case "fo:padding-top": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if(padding!=null) {
                        cellAttrs.put(OCKey.PADDING_TOP.value(), padding);
                    }
                    break;
                }
                case "fo:padding-right": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if(padding!=null) {
                        cellAttrs.put(OCKey.PADDING_RIGHT.value(), padding);
                    }
                    break;
                }
                case "fo:padding-bottom": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if(padding!=null) {
                        cellAttrs.put(OCKey.PADDING_BOTTOM.value(), padding);
                    }
                    break;
                }
                case "fo:border-left": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if(border!=null) {
                        cellAttrs.put(OCKey.BORDER_LEFT.value(), border);
                    }
                    break;
                }
                case "fo:border-top": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if(border!=null) {
                        cellAttrs.put(OCKey.BORDER_TOP.value(), border);
                    }
                    break;
                }
                case "fo:border-right": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if(border!=null) {
                        cellAttrs.put(OCKey.BORDER_RIGHT.value(), border);
                    }
                    break;
                }
                case "fo:border-bottom": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if(border!=null) {
                        cellAttrs.put(OCKey.BORDER_BOTTOM.value(), border);
                    }
                    break;
                }
                case "draw:fill": {
// we do not support fill properties
                    if(propValue.equals("none")) {
                        cellAttrs.put(OCKey.FILL_TYPE.value(), "none");
                    }
                    else if(propValue.equals("bitmap")) {
                        cellAttrs.put(OCKey.FILL_TYPE.value(), "bitmap");
                    }
                    else if(propValue.equals("gradient")) {
                        cellAttrs.put(OCKey.FILL_TYPE.value(), "gradient");
                    }
                    else {
                        cellAttrs.put(OCKey.FILL_TYPE.value(), "solid");
                    }
                    break;
                }
                case "fo:background-color": {
                    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
                    if(color!=null) {
                        cellAttrs.put(OCKey.FILL_COLOR.value(), color);
                    }
                    break;
                }
                case "draw:fill-color": {
                    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
                    if(color!=null) {
                        cellAttrs.put(OCKey.FILL_COLOR.value(), color);
                    }
                    break;
                }
                case "draw:stroke":
                    // PASSTRHOUGH INTENDED!!
                case "svg:stroke-width":
                    // PASSTRHOUGH INTENDED!!
                case "svg:stroke-color": {
                    hasDrawStroke = true;
                    break;
                }

                // DOCS-3091
                case "draw:textarea-vertical-align": {
                    final String vAlign;
                    if("middle".equals(propValue)) {
                        vAlign = "center";
                    }
                    else if("bottom".equals(propValue)) {
                        vAlign = "bottom";
                    }
                    else {
                        vAlign = "top";
                    }
                    cellAttrs.put(OCKey.ALIGN_VERT.value(), vAlign);
                    break;
                }
            }

        }
        if(hasDrawStroke) {
            Border.createDefaultBorderMapAttrsFromDrawStroke(cellAttrs, defaultPadding, attributes);
        }
        if(cellAttrs.isEmpty()) {
            attrs.remove(OCKey.CELL.value());
        }
    }

    @Override
    public LoExtTableCellGraphicProperties clone() {
        return (LoExtTableCellGraphicProperties)super.clone();
    }
}
