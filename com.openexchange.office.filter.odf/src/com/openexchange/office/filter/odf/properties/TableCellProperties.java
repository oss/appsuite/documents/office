/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Border;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class TableCellProperties extends StylePropertiesBase {

    private DLList<IElementWriter> content;

    public TableCellProperties(AttributesImpl attributesImpl) {
        super(attributesImpl);
    }

    @Override
    public DLList<IElementWriter> getContent() {
        if(content==null) {
            content = new DLList<IElementWriter>();
        }
        return content;
    }

    @Override
    public String getQName() {
        return "style:table-cell-properties";
    }

    @Override
    public String getLocalName() {
        return "table-cell-properties";
    }

    @Override
    public String getNamespace() {
        return Namespaces.STYLE;
    }

    @Override
    public void applyAttrs(StyleManager styleManager, JSONObject attrs)
        throws JSONException {

        _applyAttrs(attrs.optJSONObject(OCKey.CELL.value()));
    }

    public void _applyAttrs(JSONObject cellAttributes)
        throws JSONException {

        if(cellAttributes!=null) {
            boolean createCellProtectAttribute = false;
            Boolean unlocked = null;
            Boolean hidden = null;
            final Iterator<Entry<String, Object>> characterIter = cellAttributes.entrySet().iterator();
            while(characterIter.hasNext()) {
                final Entry<String, Object> characterEntry = characterIter.next();
                final Object value = characterEntry.getValue();
                switch(OCKey.fromValue(characterEntry.getKey())) {
                    case FILL_COLOR : {
                        if(value==JSONObject.NULL) {
                            attributes.remove("fo:background-color");
                        }
                        else {
                            attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", PropertyHelper.getColor((JSONObject)value, "transparent"));
                        }
                        break;
                    }
                    case ALIGN_VERT : {
                        String vAlign = value.toString();
                        switch(vAlign) {
                            case "middle": // PASSTHROUGH INTENDED
                            case "bottom":
                            case "top":
                            case "justify": {
                                break;
                            }
                            default: {
                                vAlign = null;
                                break;
                            }
                        }
                        attributes.setValue(Namespaces.STYLE, "vertical-align", "style:vertical-align", vAlign);
                        break;
                    }
                    case PADDING_BOTTOM : {
                        if (value==JSONObject.NULL) {
                            attributes.remove("fo:padding-bottom");
                        } else {
                            attributes.setValue(Namespaces.FO, "padding-bottom", "fo:padding-bottom", (((Number)value).intValue() / 100) + "mm");
                        }
                        break;
                    }
                    case PADDING_LEFT : {
                        if (value==JSONObject.NULL) {
                            attributes.remove("fo:padding-left");
                        } else {
                            attributes.setValue(Namespaces.FO, "padding-left", "fo:padding-left", (((Number)value).intValue() / 100) + "mm");
                        }
                        break;
                    }
                    case PADDING_RIGHT : {
                        if (value==JSONObject.NULL) {
                            attributes.remove("fo:padding-right");
                        } else {
                            attributes.setValue(Namespaces.FO, "padding-right", "fo:padding-right", (((Number)value).intValue() / 100) + "mm");
                        }
                        break;
                    }
                    case PADDING_TOP : {
                        if (value==JSONObject.NULL) {
                            attributes.remove("fo:padding-top");
                        } else {
                            attributes.setValue(Namespaces.FO, "padding-top", "fo:padding-top", (((Number)value).intValue() / 100) + "mm");
                        }
                        break;
                    }
                    case BORDER_LEFT : {
                        Border.applyFoBorderToSingleBorder(attributes);
                        if(value==JSONObject.NULL) {
                            attributes.remove("fo:border-left");
                        }
                        else {
                            final Border border = new Border();
                            border.applyJsonBorder((JSONObject)value);
                            final String foBorder = border.toString();
                            if(foBorder!=null) {
                                attributes.setValue(Namespaces.FO, "border-left", "fo:border-left", foBorder);
                            }
                        }
                        break;
                    }
                    case BORDER_TOP : {
                        Border.applyFoBorderToSingleBorder(attributes);
                        if(value==JSONObject.NULL) {
                            attributes.remove("fo:border-top");
                        }
                        else {
                            final Border border = new Border();
                            border.applyJsonBorder((JSONObject)value);
                            final String foBorder = border.toString();
                            if(foBorder!=null) {
                                attributes.setValue(Namespaces.FO, "border-top", "fo:border-top", foBorder);
                            }
                        }
                        break;
                    }
                    case BORDER_RIGHT : {
                        Border.applyFoBorderToSingleBorder(attributes);
                        if(value==JSONObject.NULL) {
                            attributes.remove("fo:border-right");
                        }
                        else {
                            final Border border = new Border();
                            border.applyJsonBorder((JSONObject)value);
                            final String foBorder = border.toString();
                            if(foBorder!=null) {
                                attributes.setValue(Namespaces.FO, "border-right", "fo:border-right", foBorder);
                            }
                        }
                        break;
                    }
                    case BORDER_BOTTOM : {
                        Border.applyFoBorderToSingleBorder(attributes);
                        if(value==JSONObject.NULL) {
                            attributes.remove("fo:border-bottom");
                        }
                        else {
                            final Border border = new Border();
                            border.applyJsonBorder((JSONObject)value);
                            final String foBorder = border.toString();
                            if(foBorder!=null) {
                                attributes.setValue(Namespaces.FO, "border-bottom", "fo:border-bottom", foBorder);
                            }
                        }
                        break;
                    }
                    case WRAP_TEXT : {
                        if(value==JSONObject.NULL) {
                            attributes.remove("fo:wrap-option");
                        }
                        else {
                            attributes.setValue(Namespaces.FO, "wrap-option", "fo:wrap-option", ((Boolean)value).booleanValue() ? "wrap" : "no-wrap");
                        }
                        break;
                    }
                    case UNLOCKED : {
                        if(unlocked==JSONObject.NULL) {
                            unlocked = null;
                        }
                        else if (value instanceof Boolean) {
                            unlocked = (Boolean)value;
                        }
                        createCellProtectAttribute = true;
                        break;
                    }
                    case HIDDEN : {
                        if(hidden==JSONObject.NULL) {
                            hidden = null;
                        }
                        else if(value instanceof Boolean) {
                            hidden = (Boolean)value;
                        }
                        createCellProtectAttribute = true;
                        break;
                    }
                }
            }
            if(createCellProtectAttribute) {
                String newProtectValue = null;
                if(unlocked!=null) {
                    if(unlocked.booleanValue()) {
                        if(hidden!=null&&hidden.booleanValue()) {
                            newProtectValue = "formula-hidden";
                        }
                        else {
                            newProtectValue = "none";
                        }
                    }
                    else {
                        if(hidden!=null&&hidden.booleanValue()) {
                            newProtectValue = "hidden-and-protected";
                        }
                        else {
                            newProtectValue = "protected";
                        }
                    }
                }
                else if(hidden!=null&&hidden.booleanValue()) {
                    newProtectValue = "formula-hidden";
                }
                if(newProtectValue==null) {
                    attributes.remove("style:cell-protect");
                }
                else {
                    attributes.setValue(Namespaces.STYLE, "cell-protect", "style:cell-protect", newProtectValue);
                }
            }
        }
    }

    @Override
    public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
        final Map<String, Object> cellAttrs = attrs.getMap(OCKey.CELL.value(), true);
        final Integer defaultPadding = PropertyHelper.createDefaultPaddingAttrs(cellAttrs, attributes);
        Border.createDefaultBorderMapAttrsFromFoBorder(cellAttrs, defaultPadding, attributes);
        final Iterator<Entry<String, AttributeImpl>> propIter = attributes.getUnmodifiableMap().entrySet().iterator();
        final String styleCellProtect = getAttribute("style:cell-protect");
        if(styleCellProtect!=null&&!styleCellProtect.isEmpty()) {
            final String[] propValues = StringUtils.split(styleCellProtect);
            for(int i=0; i<propValues.length; i++) {
                final String val = propValues[i];
                if(val.equals("none")) {
                    cellAttrs.put(OCKey.UNLOCKED.value(), Boolean.TRUE);
                }
                else if(val.equals("hidden-and-protected")||val.equals("formula-hidden")) {
                    cellAttrs.put(OCKey.HIDDEN.value(), Boolean.TRUE);
                }
            }
        }
        while(propIter.hasNext()) {
            final Entry<String, AttributeImpl> propEntry = propIter.next();
            final String propName = propEntry.getKey();
            final String propValue = propEntry.getValue().getValue();
            switch(propName) {
                case "fo:padding-left": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if(padding!=null) {
                        cellAttrs.put(OCKey.PADDING_LEFT.value(), padding);
                    }
                    break;
                }
                case "fo:padding-top": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if(padding!=null) {
                        cellAttrs.put(OCKey.PADDING_TOP.value(), padding);
                    }
                    break;
                }
                case "fo:padding-right": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if(padding!=null) {
                        cellAttrs.put(OCKey.PADDING_RIGHT.value(), padding);
                    }
                    break;
                }
                case "fo:padding-bottom": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if(padding!=null) {
                        cellAttrs.put(OCKey.PADDING_BOTTOM.value(), padding);
                    }
                    break;
                }
                case "fo:border-left": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if(border!=null) {
                        cellAttrs.put(OCKey.BORDER_LEFT.value(), border);
                    }
                    break;
                }
                case "fo:border-top": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if(border!=null) {
                        cellAttrs.put(OCKey.BORDER_TOP.value(), border);
                    }
                    break;
                }
                case "fo:border-right": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if(border!=null) {
                        cellAttrs.put(OCKey.BORDER_RIGHT.value(), border);
                    }
                    break;
                }
                case "fo:border-bottom": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if(border!=null) {
                        cellAttrs.put(OCKey.BORDER_BOTTOM.value(), border);
                    }
                    break;
                }
                case "fo:background-color": {
                    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
                    if(color!=null) {
                        cellAttrs.put(OCKey.FILL_COLOR.value(), color);
                    }
                    break;
                }
                case "style:vertical-align": {
                    final String vAlign;
                    if("middle".equals(propValue)) {
                        vAlign = "middle";
                    }
                    else if("bottom".equals(propValue)) {
                        vAlign = "bottom";
                    }
                    else if("justify".equals(propValue)) {
                        vAlign = "justify";
                    }
                    else {
                        vAlign = "top";
                    }
                    cellAttrs.put(OCKey.ALIGN_VERT.value(), vAlign);
                    break;
                }
                case "fo:wrap-option": {
                    cellAttrs.put(OCKey.WRAP_TEXT.value(), "no-wrap".equals(propValue) ? Boolean.FALSE : Boolean.TRUE);
                    break;
                }
            }
        }
    }

    @Override
    public TableCellProperties clone() {
        return (TableCellProperties)_clone();
    }
}
