/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class TableColumnProperties extends StylePropertiesBase {

	public TableColumnProperties(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	@Override
	public String getQName() {
		return "style:table-column-properties";
	}

	@Override
	public String getLocalName() {
		return "table-column-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
        final JSONObject columnAttrs = attrs!=null ? attrs.optJSONObject(OCKey.COLUMN.value()) : null;
        if(columnAttrs!=null) {
        	final Iterator<Entry<String, Object>> columnAttrsIter = columnAttrs.entrySet().iterator();
        	while(columnAttrsIter.hasNext()) {
        		final Entry<String, Object> columnAttrsEntry = columnAttrsIter.next();
				final Object columnAttrsValue = columnAttrsEntry.getValue();
				if(columnAttrsValue!=null) {
	        		switch(OCKey.fromValue(columnAttrsEntry.getKey())) {
	        			case WIDTH : {
	        				if(columnAttrsValue==JSONObject.NULL) {
	        					attributes.remove("style:column-width");
	        				}
	        				else {
	        					attributes.setLength100thmm(Namespaces.STYLE, "column-width", "style:column-width", ((Number)columnAttrsValue).intValue());
	        				}
	        				break;
	        			}
	        		}
				}
        	}
        }
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		final Map<String, Object> columnAttrs = attrs.getMap(OCKey.COLUMN.value(), true);
		final Integer width = attributes.getLength100thmm("style:column-width", false);
		if(width!=null) {
            columnAttrs.put(OCKey.WIDTH.value(), width);
		}
		final String optimalColumnWidth = attributes.getValue("style:use-optimal-column-width");
		if(optimalColumnWidth!=null) {
			columnAttrs.put(OCKey.CUSTOM_WIDTH.value(), !Boolean.parseBoolean(optimalColumnWidth));
		}
		if(columnAttrs.isEmpty()) {
			attrs.remove(OCKey.COLUMN.value());
		}
	}

	@Override
	public TableColumnProperties clone() {
		return (TableColumnProperties)_clone();
	}
}
