/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class SectionProperties extends StylePropertiesBase {

	private DLList<IElementWriter> content;

	public SectionProperties(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	@Override
	public DLList<IElementWriter> getContent() {
		if(content==null) {
			content = new DLList<IElementWriter>();
		}
		return content;
	}

	@Override
	public String getQName() {
		return "style:section-properties";
	}

	@Override
	public String getLocalName() {
		return "section-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SectionProperties clone() {
		return (SectionProperties)_clone();
	}
}
