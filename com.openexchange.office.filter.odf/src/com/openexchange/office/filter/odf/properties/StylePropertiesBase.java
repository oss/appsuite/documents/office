/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.StyleManager;

public abstract class StylePropertiesBase implements IElementWriter, Cloneable {

	protected AttributesImpl attributes;
	protected String text = "";
	protected boolean deepContent = false;

	protected StylePropertiesBase(AttributesImpl attributesImpl) {
		this.attributes = attributesImpl;
	}

	public StylePropertiesBase setAttributes(Attributes attributes) {
		this.attributes.setAttributes(attributes);
		return this;
	}

	public void setTextContent(String text) {
		this.text = text;
	}

	public String getTextContent() {
		return text;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		if(!attributes.isEmpty()||(getContent()!=null&&!getContent().isEmpty())||!getTextContent().isEmpty()) {
			SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
			attributes.write(output);
			if(!text.isEmpty()) {
				output.characters(text);
			}
			final DLList<IElementWriter> content = getContent();
			if(content!=null) {
				final Iterator<IElementWriter> childIter = content.iterator();
				while(childIter.hasNext()) {
					childIter.next().writeObject(output);
				}
			}
			SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
		}
	}

	protected DLList<IElementWriter> getContent() {
		return null;
	}

	public String getAttribute(String qName) {
		return attributes.getValue(qName);
	}

	public Boolean getBoolean(String qName, Boolean defaultValue) {
		return attributes.getBooleanValue(qName, defaultValue);
	}

	public Integer getInteger(String qName, Integer defaultValue) {
		final Integer ret = attributes.getIntValue(qName);
		return ret!=null ? ret : defaultValue;
	}

	public AttributesImpl getAttributes() {
		return attributes;
	}

	public void mergeAttrs(StylePropertiesBase propertiesBase) {
	    //
	}

	public abstract void applyAttrs(StyleManager styleManager, JSONObject attrs)
		throws JSONException;

	public abstract void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs);

	@Override
	public int hashCode() {
        final int prime = 31;
	    int hash = attributes.hashCode() + getQName().hashCode();
	    hash ^= text.hashCode();
	    final DLList<IElementWriter> content = getContent();
	    if(content!=null&&!content.isEmpty()) {
    	    if(deepContent) {
                final Iterator<IElementWriter> iter = content.iterator();
                while(iter.hasNext()) {
                    hash = prime * hash + iter.next().hashCode();
                }
    	    }
	    }
        return hash;
	}

	@Override
	abstract protected Object clone();

	protected Object _clone() {
		try {
			final StylePropertiesBase clone = (StylePropertiesBase)super.clone();
			clone.attributes = attributes.clone();
			return clone;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final StylePropertiesBase other = (StylePropertiesBase)obj;
		if (!attributes.equals(other.attributes))
			return false;
		if (!text.equals(other.text)) {
		    return false;
		}
		final DLList<IElementWriter> content = getContent();
        final DLList<IElementWriter> otherContent = other.getContent();
        if(content==null) {
            if(otherContent!=null&&!otherContent.isEmpty()) {
                return false;
            }
            return true;
        }
        if(content.isEmpty()&&(otherContent==null||otherContent.isEmpty())) {
            return true;
        }
        if(otherContent==null) {
            return false;
        }
        if(deepContent) {
	        if(content==otherContent) {
	            return true;
	        }
	        DLNode<IElementWriter> node = getContent().getFirstNode();
	        DLNode<IElementWriter> otherNode = otherContent.getFirstNode();
	        while(node!=null&&otherNode!=null) {
	            if(!node.getData().equals(otherNode.getData())) {
	                return false;
	            }
	            node = node.getNext();
	            otherNode = otherNode.getNext();
	        }
	        if(node!=null||otherNode!=null) {
	            return false;
	        }
		}
		else if(content!=otherContent) {
	        return false;
		}
		return true;
	}

	abstract public String getQName();

	abstract public String getLocalName();

	abstract public String getNamespace();
}
