/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class PageLayoutProperties extends StylePropertiesBase {

	public PageLayoutProperties(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	@Override
	public String getQName() {
		return "style:page-layout-properties";
	}

	@Override
	public String getLocalName() {
		return "page-layout-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		final JSONObject pageAttributes = attrs.optJSONObject(OCKey.PAGE.value());
		if(pageAttributes!=null) {
			final Iterator<Entry<String, Object>> paragraphIter = pageAttributes.entrySet().iterator();
			while(paragraphIter.hasNext()) {
				final Entry<String, Object> paragraphEntry = paragraphIter.next();
				final Object value = paragraphEntry.getValue();
				switch(OCKey.fromValue(paragraphEntry.getKey())) {
					case MARGIN_BOTTOM : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:margin-bottom");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "margin-bottom", "fo:margin-bottom", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
					case MARGIN_LEFT : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:margin-left");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "margin-left", "fo:margin-left", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
					case MARGIN_RIGHT : { 
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:margin-right");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "margin-right", "fo:margin-right", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
					case MARGIN_TOP : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:margin-top");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "margin-top", "fo:margin-top", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
					case WIDTH : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:page-width");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "page-width", "fo:page-width", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
					case HEIGHT : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:page-height");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "page-height", "fo:page-height", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
				}
			}
		}
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		final Map<String, Object> pageAttrs = attrs.getMap(OCKey.PAGE.value(), true);
		final Iterator<Entry<String, AttributeImpl>> propIter = attributes.getUnmodifiableMap().entrySet().iterator();
        while(propIter.hasNext()) {
        	final Entry<String, AttributeImpl> propEntry = propIter.next();
        	final String propName = propEntry.getKey();
        	final String propValue = propEntry.getValue().getValue();
        	switch(propName) {
	    		case "fo:margin-left": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	                if(margin!=null) {
	                	pageAttrs.put(OCKey.MARGIN_LEFT.value(), margin);
	                }
	    			break;
	    		}
	    		case "fo:margin-top": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	                if(margin!=null) {
	                	pageAttrs.put(OCKey.MARGIN_TOP.value(), margin);
	                }
	    			break;
	    		}
	    		case "fo:margin-right": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	    			if(margin!=null) {
	    				pageAttrs.put(OCKey.MARGIN_RIGHT.value(), margin);
	    			}
	    			break;
	    		}
	    		case "fo:margin-bottom": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	    			if(margin!=null) {
	    				pageAttrs.put(OCKey.MARGIN_BOTTOM.value(), margin);
	    			}
	    			break;
	    		}
        		case "fo:page-width": {
        			final Integer pageWidth = AttributesImpl.normalizeLength(propValue, true);
        			if(pageWidth!=null) {
        				pageAttrs.put(OCKey.WIDTH.value(), pageWidth);
        			}
        			break;
	            }
        		case "fo:page-height": {
        			final Integer pageHeight = AttributesImpl.normalizeLength(propValue, true);
        			if(pageHeight!=null) {
        				pageAttrs.put(OCKey.HEIGHT.value(), pageHeight);
        			}
        			break;
	            }
        	}
        }
        if(pageAttrs.isEmpty()) {
        	attrs.remove(OCKey.PAGE.value());
        }
	}

	@Override
	public PageLayoutProperties clone() {
		return (PageLayoutProperties)_clone();
	}
}
