/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;

public class TabStopsHandler extends SaxContextHandler {

	final TabStops tabStops;

	public TabStopsHandler(SaxContextHandler parentContext, TabStops tabStops) {
		super(parentContext);

		this.tabStops = tabStops;
	}

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("style:tab-stop")) {
    		final TabStop tabStop = new TabStop(new AttributesImpl(attributes));
    		tabStops.getTabStopList().add(tabStop);
    		return new StylePropertiesBaseHandler(this, tabStop);
    	}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	final DLList<IElementWriter> content = tabStops.getContent();
    	if(content!=null) {
        	content.add(element);
    	}
		return new UnknownContentHandler(this, element);
    }

	@Override
	public void endContext(String qName, String characters) {
		super.endContext(qName, characters);

		tabStops.setTextContent(characters);
	}
}
