/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class TableProperties extends StylePropertiesBase {

	private DLList<IElementWriter> content;

	public TableProperties(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	@Override
	public DLList<IElementWriter> getContent() {
		if(content==null) {
			content = new DLList<IElementWriter>();
		}
		return content;
	}

	@Override
	public String getQName() {
		return "style:table-properties";
	}

	@Override
	public String getLocalName() {
		return "table-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs)
		throws JSONException {

		final JSONObject tableAttributes = attrs!=null ? attrs.optJSONObject(OCKey.TABLE.value()) : null;
		if(tableAttributes!=null) {
			final Iterator<Entry<String, Object>> characterIter = tableAttributes.entrySet().iterator();
			while(characterIter.hasNext()) {
				final Entry<String, Object> characterEntry = characterIter.next();
				final Object value = characterEntry.getValue();
				switch(OCKey.fromValue(characterEntry.getKey())) {
					case VISIBLE : {
						if(value==JSONObject.NULL) {
							attributes.remove("table:display");
						}
						else {
							attributes.setValue(Namespaces.TABLE, "displays", "table:display", ((Boolean)value).toString());
						}
						break;
					}
	        		case PAGE_BREAK_BEFORE : {
						if(value==JSONObject.NULL) {
							attributes.remove("fo:break-before");
						}
						else {
							attributes.setValue(Namespaces.FO, "break-before", "fo:break-before", ((Boolean)value).toString());
						}
						break;
	        		}
	        		case PAGE_BREAK_AFTER : {
						if(value==JSONObject.NULL) {
							attributes.remove("fo:break-after");
						}
						else {
							attributes.setValue(Namespaces.FO, "break-after", "fo:break-after", ((Boolean)value).toString());
						}
						break;
	        		}
					case WIDTH : {
						if(value==JSONObject.NULL) {
							attributes.remove("style:width");
						}
						else {
						    // LO requires a table alignment attribute, otherwise the table:width is ignored.
						    if(!attributes.containsKey("table:align")) {
						        attributes.setValue(Namespaces.TABLE, "align", "table:align", "left");
						    }
						    if(value instanceof Number) {
    							attributes.setValue(Namespaces.STYLE, "width", "style:width", (((Number)value).intValue() / 100) + "mm");
    						}
    						else if(value instanceof String) {
    							if(value.equals("auto")) {			// auto ... and now ? auto, how big should it be ?
    								attributes.setValue(Namespaces.STYLE, "widht", "style:width", "17cm");
    							}
    						}
						}
						break;
					}
					case FILL_COLOR : {
						if(value==JSONObject.NULL) {
							attributes.remove("fo:background-color");
						}
						else {
							attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", PropertyHelper.getColor((JSONObject)value, "transparent"));
						}
						break;
					}
					case MARGIN_BOTTOM : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:margin-bottom");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "margin-bottom", "fo:margin-bottom", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
					case MARGIN_LEFT : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:margin-left");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "margin-left", "fo:margin-left", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
					case MARGIN_RIGHT : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:margin-right");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "margin-right", "fo:margin-right", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
					case MARGIN_TOP : {
		                if(value==JSONObject.NULL) {
		                	attributes.remove("fo:margin-top");
		                }
		                else {
		                    attributes.setValue(Namespaces.FO, "margin-top", "fo:margin-top", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
					}
				}
			}
		}
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		final Map<String, Object> tableAttrs = attrs.getMap(OCKey.TABLE.value(), true);
		PropertyHelper.createDefaultMarginAttrs(tableAttrs, attributes);
        final Iterator<Entry<String, AttributeImpl>> propIter = attributes.getUnmodifiableMap().entrySet().iterator();
        while(propIter.hasNext()) {
        	final Entry<String, AttributeImpl> propEntry = propIter.next();
        	final String propName = propEntry.getKey();
        	final String propValue = propEntry.getValue().getValue();
        	switch(propName) {
        		case "fo:margin-left": {
        			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
                    if(margin!=null) {
                    	tableAttrs.put(OCKey.MARGIN_LEFT.value(), margin);
                    }
        			break;
        		}
        		case "fo:margin-top": {
        			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
                    if(margin!=null) {
                    	tableAttrs.put(OCKey.MARGIN_TOP.value(), margin);
                    }
        			break;
        		}
        		case "fo:margin-right": {
        			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
        			if(margin!=null) {
        				tableAttrs.put(OCKey.MARGIN_RIGHT.value(), margin);
        			}
        			break;
        		}
        		case "fo:margin-bottom": {
        			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
        			if(margin!=null) {
        				tableAttrs.put(OCKey.MARGIN_BOTTOM.value(), margin);
        			}
        			break;
        		}
        		case "style:width": {
        			final Integer width = AttributesImpl.normalizeLength(propValue, true);
        		    if (width!=null) {
        		    	tableAttrs.put(OCKey.WIDTH.value(), width);
        		    }
        		    break;
        		}
        		case "fo:background-color": {
                    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
                    if(color!=null) {
                        tableAttrs.put(OCKey.FILL_COLOR.value(), color);
                    }
	        	    break;
        		}
        		case "table:display": {
	        	    tableAttrs.put(OCKey.VISIBLE.value(), Boolean.parseBoolean(propValue));
	        	    break;
        		}
        		case "fo:break-before": {
	        	    tableAttrs.put(OCKey.PAGE_BREAK_BEFORE.value(), propValue.equals("page"));
	        	    break;
        		}
        		case "fo:break-after": {
	        	    tableAttrs.put(OCKey.PAGE_BREAK_AFTER.value(), propValue.equals("page"));
	        	    break;
        		}
        	}
        }
        if(tableAttrs.isEmpty()) {
        	attrs.remove(OCKey.TABLE.value());
        }
	}

	@Override
	public TableProperties clone() {
		return (TableProperties)_clone();
	}
}
