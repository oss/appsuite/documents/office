/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;

final public class NumberNumber extends StyleNumberPropertiesBase {

    public NumberNumber(AttributesImpl attributesImpl) {
        super(attributesImpl);
    }

    @Override
    public String getQName() {
        return "number:number";
    }

    @Override
    public String getLocalName() {
        return "number";
    }

    @Override
    public String getNamespace() {
        return Namespaces.NUMBER;
    }

    public String getNumberFormat() {
        String result = "";

        final int minInt = getInteger("number:min-integer-digits", 0);
        Integer decimalPos = attributes.getIntValue("number:decimal-places");
        String decimalReplacement = getAttribute("number:decimal-replacement");
        Boolean isGroup = getBoolean("number:grouping", null);
        int i;

        if(minInt==1&&decimalPos==null&&decimalReplacement==null&&isGroup==null) {
            return "general";
        }
        if(decimalPos==null) {
            decimalPos = Integer.valueOf(0);
        }
        if(isGroup==null) {
            isGroup = Boolean.valueOf(false);
        }
        if( minInt == 0){
            result = "#";
        }
        for (i = 0; i < minInt; i++) {
            if ((i + 1 != minInt) && ((i + 1) % 3) == 0 && isGroup) {
                result = ",0" + result;
            } else {
                result = "0" + result;
            }
        }
        while (isGroup && (result.indexOf(',') == -1)) {
            if (((i + 1) % 3) == 0 && isGroup) {
                result = "#,#" + result;
            } else {
                result = "#" + result;
            }
            i++;
        }

        if(decimalReplacement != null){
            result += '.' + decimalReplacement;
        } else if (decimalPos > 0) {
            result += ".";
            for (i = 0; i < decimalPos; i++) {
                result += "0";
            }
        }
        return result;
    }

    @Override
    public NumberNumber clone() {
        return (NumberNumber)_clone();
    }
}
