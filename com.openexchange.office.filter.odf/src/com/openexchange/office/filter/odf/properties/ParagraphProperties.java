/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Border;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class ParagraphProperties extends StylePropertiesBase {

    private DLList<IElementWriter> content;
    private TabStops               tabStops;

    public ParagraphProperties(AttributesImpl attributesImpl) {
        super(attributesImpl);
    }

    @Override
    public ParagraphProperties setAttributes(Attributes attributes) {
        super.setAttributes(attributes);
        return this;
    }

    @Override
    public DLList<IElementWriter> getContent() {
        if (content == null) {
            content = new DLList<IElementWriter>();
        }
        return content;
    }

    public TabStops getTabStops(boolean forceCreate) {
        if (tabStops == null && forceCreate) {
            tabStops = new TabStops(new AttributesImpl());
        }
        return tabStops;
    }

    public void setTabStops(TabStops tabStops) {
        this.tabStops = tabStops;
    }

    @Override
    public String getQName() {
        return "style:paragraph-properties";
    }

    @Override
    public String getLocalName() {
        return "paragraph-properties";
    }

    @Override
    public String getNamespace() {
        return Namespaces.STYLE;
    }

    @Override
    public void applyAttrs(StyleManager styleManager, JSONObject attrs) throws JSONException {

        // TODO: addBorderProperties(key, attrs, propertiesElement, null);

        final JSONObject paragraphAttributes = attrs.optJSONObject(OCKey.PARAGRAPH.value());
        if (paragraphAttributes != null) {
            final Iterator<Entry<String, Object>> paragraphIter = paragraphAttributes.entrySet().iterator();
            while (paragraphIter.hasNext()) {
                final Entry<String, Object> paragraphEntry = paragraphIter.next();
                final Object value = paragraphEntry.getValue();
                switch (OCKey.fromValue(paragraphEntry.getKey())) {
                    case COLOR: {
                        break;
                    }
                    case BORDER_LEFT: {
                        Border.applyFoBorderToSingleBorder(attributes);
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:border-left");
                        } else {
                            final Border border = new Border();
                            border.applyJsonBorder((JSONObject) value);
                            final String foBorder = border.toString();
                            if (foBorder != null) {
                                attributes.setValue(Namespaces.FO, "border-left", "fo:border-left", foBorder);
                            }
                        }
                        break;
                    }
                    case BORDER_TOP: {
                        Border.applyFoBorderToSingleBorder(attributes);
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:border-top");
                        } else {
                            final Border border = new Border();
                            border.applyJsonBorder((JSONObject) value);
                            final String foBorder = border.toString();
                            if (foBorder != null) {
                                attributes.setValue(Namespaces.FO, "border-top", "fo:border-top", foBorder);
                            }
                        }
                        break;
                    }
                    case BORDER_RIGHT: {
                        Border.applyFoBorderToSingleBorder(attributes);
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:border-right");
                        } else {
                            final Border border = new Border();
                            border.applyJsonBorder((JSONObject) value);
                            final String foBorder = border.toString();
                            if (foBorder != null) {
                                attributes.setValue(Namespaces.FO, "border-right", "fo:border-right", foBorder);
                            }
                        }
                        break;
                    }
                    case BORDER_BOTTOM: {
                        Border.applyFoBorderToSingleBorder(attributes);
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:border-bottom");
                        } else {
                            final Border border = new Border();
                            border.applyJsonBorder((JSONObject) value);
                            final String foBorder = border.toString();
                            if (foBorder != null) {
                                attributes.setValue(Namespaces.FO, "border-bottom", "fo:border-bottom", foBorder);
                            }
                        }
                        break;
                    }
                    case MARGIN_BOTTOM: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:margin-bottom");
                        } else {
                            attributes.setValue(Namespaces.FO, "margin-bottom", "fo:margin-bottom", (((Number) value).intValue() / 100F) + "mm");
                        }
                        break;
                    }
                    case INDENT_LEFT:
                        // PASSTHROUGH INTENDED !!
                    case MARGIN_LEFT: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:margin-left");
                        } else {
                            attributes.setValue(Namespaces.FO, "margin-left", "fo:margin-left", (((Number) value).intValue() / 100F) + "mm");
                        }
                        break;
                    }
                    case INDENT_RIGHT:
                        // PASSTHROUGH INTENDED !!
                    case MARGIN_RIGHT: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:margin-right");
                        } else {
                            attributes.setValue(Namespaces.FO, "margin-right", "fo:margin-right", (((Number) value).intValue() / 100F) + "mm");
                        }
                        break;
                    }
                    case MARGIN_TOP: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:margin-top");
                        } else {
                            attributes.setValue(Namespaces.FO, "margin-top", "fo:margin-top", (((Number) value).intValue() / 100F) + "mm");
                        }
                        break;
                    }
                    case PADDING_BOTTOM: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:padding-bottom");
                        } else {
                            attributes.setValue(Namespaces.FO, "padding-bottom", "fo:padding-bottom", (((Number) value).intValue() / 100F) + "mm");
                        }
                        break;
                    }
                    case PADDING_LEFT: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:padding-left");
                        } else {
                            attributes.setValue(Namespaces.FO, "padding-left", "fo:padding-left", (((Number) value).intValue() / 100F) + "mm");
                        }
                        break;
                    }
                    case PADDING_RIGHT: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:padding-right");
                        } else {
                            attributes.setValue(Namespaces.FO, "padding-right", "fo:padding-right", (((Number) value).intValue() / 100F) + "mm");
                        }
                        break;
                    }
                    case PADDING_TOP: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:padding-top");
                        } else {
                            attributes.setValue(Namespaces.FO, "padding-top", "fo:padding-top", (((Number) value).intValue() / 100F) + "mm");
                        }
                        break;
                    }
                    case FILL_COLOR: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:background-color");
                        } else {
                            attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", PropertyHelper.getColor((JSONObject) value, "transparent"));
                        }
                        break;
                    }
                    case LINE_HEIGHT: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:line-height");
                            attributes.remove("style:line-height-at-least");
                            attributes.remove("style:line-spacing");
                        } else {
                            PropertyHelper.setLineHeight((JSONObject) value, this);
                        }
                        break;
                    }
                    case ALIGNMENT: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:text-align");
                        } else {
                            attributes.setValue(Namespaces.FO, "text-align", "fo:text-align", (String) value);
                        }
                        break;
                    }
                    case INDENT_FIRST_LINE: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:text-indent");
                        } else {
                            attributes.setValue(Namespaces.FO, "text-indent", "fo:text-indent", (((Number) value).intValue() / 100F) + "mm");
                        }
                        break;
                    }
                    case PAGE_BREAK_BEFORE: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:break-before");
                        } else if (((Boolean) value).booleanValue()) {
                            attributes.setValue(Namespaces.FO, "break-before", "fo:break-before", "page");
                        }
                        // there can not be before and after break at the same paragraph
                        attributes.remove("fo:break-after");
                        break;
                    }
                    case PAGE_BREAK_AFTER: {
                        if (value == JSONObject.NULL) {
                            attributes.remove("fo:break-after");
                        } else if (((Boolean) value).booleanValue()) {
                            attributes.setValue(Namespaces.FO, "break-after", "fo:break-after", "page");
                        }
                        // there can not be before and after break at the same paragraph
                        attributes.remove("fo:break-before");
                        break;
                    }
                    case TAB_STOPS: {
                        setTabStops(null);
                        if (value instanceof JSONArray) {
                            final JSONArray tabArray = (JSONArray) value;
                            final TabStops newTabStops = getTabStops(true);
                            for (int i = 0; i < tabArray.length(); i++) {
                                final JSONObject tab = tabArray.getJSONObject(i);
                                final TabStop tabStop = new TabStop(new AttributesImpl());
                                newTabStops.getTabStopList().add(tabStop);
                                if (tab.has(OCKey.POS.value())) {
                                    tabStop.getAttributes().setValue(Namespaces.STYLE, "position", "style:position", (tab.getInt(OCKey.POS.value()) / 1000F) + "cm");
                                }
                                if (tab.has(OCKey.VALUE.value())) {
                                    String tabValue = tab.getString(OCKey.VALUE.value());
                                    if (tabValue.equals("decimal")) {
                                        tabValue = "char";
                                    } else if (tabValue.equals("bar")) {
                                        tabValue = "left";
                                    } else if (tabValue.equals("clear")) {
                                        continue; // clear unsupported
                                    }
                                    tabStop.getAttributes().setValue(Namespaces.STYLE, "type", "style:type", tabValue);
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        final JSONObject cellAttributes = attrs.optJSONObject(OCKey.CELL.value());
        if (cellAttributes != null) {
            Object alignHor = cellAttributes.opt(OCKey.ALIGN_HOR.value());
            if (null != alignHor) {
                String textAlign = alignHor.toString();
                switch (textAlign) {
                    case "left": {
                        textAlign = "start";
                        break;
                    }
                    case "right": {
                        textAlign = "end";
                        break;
                    }
                    case "center": // PATHTHROUGH INTENDED
                    case "justify": {
                        break;
                    }
                    default: {
                        textAlign = null;
                        break;
                    }
                }
                attributes.setValue(Namespaces.FO, "text-align", "fo:text-align", textAlign);
            }
        }
    }

    @Override
    public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
        final Map<String, Object> paragraphAttrs = attrs.getMap(OCKey.PARAGRAPH.value(), true);
        PropertyHelper.createDefaultMarginAttrs(paragraphAttrs, attributes);
        final Integer defaultPadding = PropertyHelper.createDefaultPaddingAttrs(paragraphAttrs, attributes);
        Border.createDefaultBorderMapAttrsFromFoBorder(paragraphAttrs, defaultPadding, attributes);
        final Iterator<Entry<String, AttributeImpl>> propIter = attributes.getUnmodifiableMap().entrySet().iterator();
        while (propIter.hasNext()) {
            final Entry<String, AttributeImpl> propEntry = propIter.next();
            final String propName = propEntry.getKey();
            final String propValue = propEntry.getValue().getValue();
            switch (propName) {
                case "fo:margin-left": {
                    final Integer margin = AttributesImpl.normalizeLength(propValue, true);
                    if (margin != null) {
//                        paragraphAttrs.put(OCKey.MARGIN_LEFT.value(), margin);
                        paragraphAttrs.put(OCKey.INDENT_LEFT.value(), margin);
                    }
                    break;
                }
                case "fo:margin-top": {
                    final Integer margin = AttributesImpl.normalizeLength(propValue, true);
                    if (margin != null) {
                        paragraphAttrs.put(OCKey.MARGIN_TOP.value(), margin);
                    }
                    break;
                }
                case "fo:margin-right": {
                    final Integer margin = AttributesImpl.normalizeLength(propValue, true);
                    if (margin != null) {
//                        paragraphAttrs.put(OCKey.MARGIN_RIGHT.value(), margin);
                        paragraphAttrs.put(OCKey.INDENT_RIGHT.value(), margin);
                    }
                    break;
                }
                case "fo:margin-bottom": {
                    final Integer margin = AttributesImpl.normalizeLength(propValue, true);
                    if (margin != null) {
                        paragraphAttrs.put(OCKey.MARGIN_BOTTOM.value(), margin);
                    }
                    break;
                }
                case "fo:padding-left": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if (padding != null) {
                        paragraphAttrs.put(OCKey.PADDING_LEFT.value(), padding);
                    }
                    break;
                }
                case "fo:padding-top": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if (padding != null) {
                        paragraphAttrs.put(OCKey.PADDING_TOP.value(), padding);
                    }
                    break;
                }
                case "fo:padding-right": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if (padding != null) {
                        paragraphAttrs.put(OCKey.PADDING_RIGHT.value(), padding);
                    }
                    break;
                }
                case "fo:padding-bottom": {
                    final Integer padding = AttributesImpl.normalizeLength(propValue, true);
                    if (padding != null) {
                        paragraphAttrs.put(OCKey.PADDING_BOTTOM.value(), padding);
                    }
                    break;
                }
                case "fo:border-left": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if (border != null) {
                        paragraphAttrs.put(OCKey.BORDER_LEFT.value(), border);
                    }
                    break;
                }
                case "fo:border-top": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if (border != null) {
                        paragraphAttrs.put(OCKey.BORDER_TOP.value(), border);
                    }
                    break;
                }
                case "fo:border-right": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if (border != null) {
                        paragraphAttrs.put(OCKey.BORDER_RIGHT.value(), border);
                    }
                    break;
                }
                case "fo:border-bottom": {
                    final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
                    if (border != null) {
                        paragraphAttrs.put(OCKey.BORDER_BOTTOM.value(), border);
                    }
                    break;
                }
                case "fo:background-color": {
                    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
                    if (color != null) {
                        paragraphAttrs.put(OCKey.FILL_COLOR.value(), color);
                    }
                    break;
                }
                case "fo:line-height": {
                    if (!propValue.isEmpty()) {
                        paragraphAttrs.put(OCKey.LINE_HEIGHT.value(), PropertyHelper.createLineHeightMap(propValue));
                    }
                    break;
                }
                case "style:line-spacing": {
                    if (!propValue.isEmpty()) {
                        final Map<String, Object> lineSpacing = new HashMap<String, Object>(2);
                        lineSpacing.put(OCKey.TYPE.value(), "leading");
                        lineSpacing.put(OCKey.VALUE.value(), AttributesImpl.normalizeLength(propValue));
                        paragraphAttrs.put(OCKey.LINE_HEIGHT.value(), lineSpacing);
                    }
                    break;
                }
                case "style:line-height-at-least": {
                    if (!propValue.isEmpty()) {
                        final Map<String, Object> atLeast = new HashMap<String, Object>(2);
                        atLeast.put(OCKey.TYPE.value(), "atLeast");
                        atLeast.put(OCKey.VALUE.value(), AttributesImpl.normalizeLength(propValue));
                        paragraphAttrs.put(OCKey.LINE_HEIGHT.value(), atLeast);
                    }
                    break;
                }
                case "fo:text-align": {
                    paragraphAttrs.put(OCKey.ALIGNMENT.value(), PropertyHelper.mapFoTextAlign(propValue));
                    break;
                }
                case "fo:text-indent": {
                    final Integer firstLineIndent = AttributesImpl.normalizeLength(propValue, true);
                    if (firstLineIndent != null) {
                        paragraphAttrs.put(OCKey.INDENT_FIRST_LINE.value(), firstLineIndent);
                    }
                }
                case "style:tab-stop-distance": {
                    break;
                }
                case "fo:break-before": {
                    if (propValue.equals("page")) {
                        paragraphAttrs.put(OCKey.PAGE_BREAK_BEFORE.value(), Boolean.TRUE);
                    }
                    break;
                }
                case "fo:break-after": {
                    if (propValue.equals("page")) {
                        paragraphAttrs.put(OCKey.PAGE_BREAK_AFTER.value(), Boolean.TRUE);
                    }
                    break;
                }
            }
        }
        if (tabStops != null) {
            int tabIndent = 0;
            final boolean hasTabsRelativeToIndent = styleManager.hasTabsRelativeToIndent();
            if (hasTabsRelativeToIndent) {
                Object leftMargin = paragraphAttrs.get(OCKey.MARGIN_LEFT.value());
                if (leftMargin instanceof Number) {
                    tabIndent = ((Number) leftMargin).intValue();
                }
            }
            final List<TabStop> tabStopList = tabStops.getTabStopList();
            final JSONArray tabArray = new JSONArray(tabStopList.size());
            for (TabStop tabStop : tabStopList) {
                try {
                    boolean hasPosition = false;

                    final JSONObject newTab = new JSONObject();
                    final String leaderText = tabStop.getAttribute("style:leader-text");
                    if (leaderText != null) {
                        String fillChar = null;
                        if (leaderText.equals(".")) {
                            fillChar = "dot";
                        } else if (leaderText.equals("-")) {
                            fillChar = "hyphen";
                        } else if (leaderText.equals("_")) {
                            fillChar = "underscore";
                        } else if (leaderText.equals(" ")) {
                            fillChar = "none";
                        }
                        if (fillChar != null) {
                            newTab.put(OCKey.FILL_CHAR.value(), fillChar);
                        }
                    }
                    final String tabPos = tabStop.getAttribute("style:position");
                    if (tabPos != null) {
                        final Integer pos = AttributesImpl.normalizeLength(tabPos, true);
                        if (pos != null) {
                            newTab.put(OCKey.POS.value(), pos + tabIndent);
                        }
                        hasPosition = true;
                    }
                    final String tabType = tabStop.getAttribute("style:type");
                    if (tabType != null) {
                        if (tabType.equals("char")) {
                            newTab.put(OCKey.VALUE.value(), "decimal");
                        } else if (!tabType.isEmpty()) {
                            newTab.put(OCKey.VALUE.value(), tabType);
                        }
                    }
                    if (hasPosition) {
                        tabArray.put(newTab);
                    }
                } catch (JSONException e) {
                    //
                }
            }
            paragraphAttrs.put(OCKey.TAB_STOPS.value(), tabArray);
        }
        if (styleManager.getDocumentType().equals("presentation")) {
            final Object marginTop = paragraphAttrs.remove(OCKey.MARGIN_TOP.value());
            if (marginTop instanceof Number) {
                final OpAttrs lineSpacing = new OpAttrs(2);
                lineSpacing.put(OCKey.TYPE.value(), "fixed");
                lineSpacing.put(OCKey.VALUE.value(), marginTop);
                paragraphAttrs.put(OCKey.SPACING_BEFORE.value(), lineSpacing);
            }
            final Object marginBottom = paragraphAttrs.remove(OCKey.MARGIN_BOTTOM.value());
            if (marginBottom instanceof Number) {
                final OpAttrs lineSpacing = new OpAttrs(2);
                lineSpacing.put(OCKey.TYPE.value(), "fixed");
                lineSpacing.put(OCKey.VALUE.value(), marginBottom);
                paragraphAttrs.put(OCKey.SPACING_AFTER.value(), lineSpacing);
            }
        }
        if (paragraphAttrs.isEmpty()) {
            attrs.remove(OCKey.PARAGRAPH.value());
        }
    }

    @Override
    public void writeObject(SerializationHandler output) throws SAXException {

        if ((tabStops != null && !tabStops.isEmpty()) || !attributes.isEmpty() || (content != null && !content.isEmpty()) || !getTextContent().isEmpty()) {
            output.startElement(getNamespace(), getLocalName(), getQName());
            attributes.write(output);
            if (!text.isEmpty()) {
                output.characters(text);
            }
            if (tabStops != null) {
                tabStops.writeObject(output);
            }
            if (content != null) {
                final Iterator<IElementWriter> childIter = content.iterator();
                while (childIter.hasNext()) {
                    childIter.next().writeObject(output);
                }
            }
            output.endElement(getNamespace(), getLocalName(), getQName());
        }
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        if (tabStops != null) {
            hash = hash * 31 + tabStops.hashCode();
        }
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        final TabStops otherTabStops = ((ParagraphProperties) obj).getTabStops(false);
        if (tabStops != null) {
            if (otherTabStops == null) {
                return false;
            }
            if (!tabStops.equals(otherTabStops)) {
                return false;
            }
        } else if (otherTabStops != null) {
            return false;
        }
        return true;
    }

    @Override
    public ParagraphProperties clone() {
        final ParagraphProperties clone = (ParagraphProperties) _clone();
        if (tabStops != null) {
            clone.setTabStops(tabStops.clone());
        }
        return clone;
    }

    public void applyHardDefaultAttributes(Set<String> paraKeyset) {
        final Iterator<String> keyIter = paraKeyset.iterator();
        while (keyIter.hasNext()) {
            switch (OCKey.fromValue(keyIter.next())) {
                case COLOR: {
                    break;
                }
                case BORDER_LEFT: {
                    attributes.setValue(Namespaces.FO, "border-left", "fo:border-left", "none");
                    break;
                }
                case BORDER_TOP: {
                    attributes.setValue(Namespaces.FO, "border-top", "fo:border-top", "none");
                    break;
                }
                case BORDER_RIGHT: {
                    attributes.setValue(Namespaces.FO, "border-right", "fo:border-right", "none");
                    break;
                }
                case BORDER_BOTTOM: {
                    attributes.setValue(Namespaces.FO, "border-bottom", "fo:border-bottom", "none");
                    break;
                }
                case MARGIN_BOTTOM: {
                    attributes.setValue(Namespaces.FO, "margin-bottom", "fo:margin-bottom", "0mm");
                    break;
                }
                case INDENT_LEFT:
                    // PASSTHROUGH INTENDED !!
                case MARGIN_LEFT: {
                    attributes.setValue(Namespaces.FO, "margin-left", "fo:margin-left", "0mm");
                    break;
                }
                case INDENT_RIGHT:
                    // PASSTHROUGH INTENDED !!
                case MARGIN_RIGHT: {
                    attributes.setValue(Namespaces.FO, "margin-right", "fo:margin-right", "0mm");
                    break;
                }
                case MARGIN_TOP: {
                    attributes.setValue(Namespaces.FO, "margin-top", "fo:margin-top", "0mm");
                    break;
                }
                case PADDING_BOTTOM: {
                    attributes.setValue(Namespaces.FO, "padding-bottom", "fo:padding-bottom", "0mm");
                    break;
                }
                case PADDING_LEFT: {
                    attributes.setValue(Namespaces.FO, "padding-left", "fo:padding-left", "0mm");
                    break;
                }
                case PADDING_RIGHT: {
                    attributes.setValue(Namespaces.FO, "padding-right", "fo:padding-right", "0mm");
                    break;
                }
                case PADDING_TOP: {
                    attributes.setValue(Namespaces.FO, "padding-top", "fo:padding-top", "0mm");
                    break;
                }
                case FILL_COLOR: {
                    attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", "transparent");
                    break;
                }
                case LINE_HEIGHT: {
                    attributes.setValue(Namespaces.FO, "line-height", "fo:line-height", "100%");
                    attributes.remove("style:line-height-at-least");
                    attributes.remove("style:line-spacing");
                    break;
                }
                case ALIGNMENT: {
                    attributes.setValue(Namespaces.FO, "text-align", "fo:text-align", "left");
                    break;
                }
                case INDENT_FIRST_LINE: {
                    attributes.setValue(Namespaces.FO, "text-indent", "fo:text-indent", "0mm");
                    break;
                }
                case PAGE_BREAK_BEFORE: {
                    break;
                }
                case PAGE_BREAK_AFTER: {
                    break;
                }
                case TAB_STOPS: {
                    break;
                }
            }
        }
    }
}
