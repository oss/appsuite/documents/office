/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Border;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.DrawFillImage;
import com.openexchange.office.filter.odf.styles.Gradient;
import com.openexchange.office.filter.odf.styles.Marker;
import com.openexchange.office.filter.odf.styles.StrokeDashStyle;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.TextListStyle;

public class GraphicProperties extends StylePropertiesBase {

    private Boolean                isFrameOrGraphic = null;
    private final StyleBase        parentStyle;
    private TextListStyle          textListStyle = null;

	public GraphicProperties(AttributesImpl attributesImpl, StyleBase parentStyle, boolean defaultProperties) {
		super(attributesImpl);
		this.parentStyle = parentStyle;

		if(defaultProperties) {
		    if(!attributesImpl.containsKey("draw:stroke")) {
		        attributesImpl.setValue(Namespaces.DRAW, "stroke", "draw:stroke", "solid");
		    }
		    if(!attributesImpl.containsKey("draw:fill")) {
		        attributesImpl.setValue(Namespaces.DRAW, "fill", "draw:fill", "solid");
		    }
            if(!attributesImpl.containsKey("draw:auto-grow-height")) {
                attributesImpl.setValue(Namespaces.DRAW, "auto-grow-height", "draw:auto-grow-height", "true");
            }
            if(!attributesImpl.containsKey("fo:wrap-option")) {
                attributesImpl.setValue(Namespaces.FO, "wrap-option", "fo:wrap-option", "no-wrap");
            }
		}
	}

    public GraphicProperties(AttributesImpl attributesImpl, StyleBase parentStyle) {
        super(attributesImpl);
        this.parentStyle = parentStyle;

    }

	public Boolean getIsFrameOrGraphic() {
		return isFrameOrGraphic;
	}

	public void setIsFrameOrGraphic(boolean isFrameOrGraphic) {
		this.isFrameOrGraphic = isFrameOrGraphic;
	}

	@Override
	public String getQName() {
		return "style:graphic-properties";
	}

	@Override
	public String getLocalName() {
		return "graphic-properties";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}

	public TextListStyle getTextListStyle() {
	    return textListStyle;
	}

	public void setTextListStyle(TextListStyle textListStyle) {
	    this.textListStyle = textListStyle;
	}

	@Override
	public int hashCode() {
	    int hash = super.hashCode();
	    if(textListStyle!=null) {
	        hash = hash * 31 + textListStyle.hashCode();
	    }
	    return hash;
	}

    @Override
    public boolean equals(Object obj) {
        if(!super.equals(obj)) {
            return false;
        }
        GraphicProperties other = (GraphicProperties)obj;
        if(textListStyle!=null) {
            return textListStyle.equals(other.textListStyle);
        }
        return other.textListStyle==null;
    }

    @Override
    public GraphicProperties clone() {
        final GraphicProperties clone = (GraphicProperties)_clone();
        if(textListStyle!=null) {
            clone.textListStyle = textListStyle.clone();
        }
        return clone;
    }

    @Override
    public void writeObject(SerializationHandler output)
        throws SAXException {

        if(!attributes.isEmpty()||(getContent()!=null&&!getContent().isEmpty())||!getTextContent().isEmpty()) {
            SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
            attributes.write(output);
            if(!text.isEmpty()) {
                output.characters(text);
            }
            if(textListStyle!=null) {
                textListStyle.writeObject(output);
            }
            SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
        }
    }

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs)
		throws JSONException {

		final JSONObject fillAttributes = attrs.optJSONObject(OCKey.FILL.value());
		if(fillAttributes!=null) {
			final Object type = fillAttributes.opt(OCKey.TYPE.value());
			if(type!=null) {
				if(type==JSONObject.NULL) {
					attributes.remove("draw:fill");
				}
				else if(type instanceof String) {
				    String drawFill = "solid";
					if("none".equals(type)) {
					    drawFill = "none";
					}
					else if("bitmap".equals(type)) {
					    drawFill = "bitmap";
					}
					else if("gradient".equals(type)) {
					    drawFill = "gradient";
					}
                    attributes.setValue(Namespaces.DRAW, "fill", "draw:fill", drawFill);
				}
			}
			final JSONObject color = fillAttributes.optJSONObject(OCKey.COLOR.value());
			if(color!=null) {
				if (color==JSONObject.NULL) {
					attributes.remove("fo:color");
					attributes.remove("fo:background-color");
				}
				else {
					final String colorType = color.optString(OCKey.TYPE.value(), null);
					if(colorType!=null) {
	                    if(colorType.equals("auto")) {
							attributes.setValue(Namespaces.DRAW, "fill-color", "draw:fill-color", "#ffffff");
							attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", "#ffffff");
	                    }
	                    else {
	                    	attributes.setValue(Namespaces.DRAW, "fill-color", "draw:fill-color", PropertyHelper.getColor(color, null));
	                    	attributes.setValue(Namespaces.FO, "background-color", "fo:background-color", PropertyHelper.getColor(color, null));
	                    }
					}
					final JSONArray transformations = color.optJSONArray(OCKey.TRANSFORMATIONS.value());
					if(transformations==JSONArray.EMPTY_ARRAY) {
					    attributes.remove("draw:opcacity");
				    }
				    else if(transformations!=null) {
				        for(int i = 0; i < transformations.length(); i++) {
				            final JSONObject o = transformations.getJSONObject(i);
				            final String t = o.getString(OCKey.TYPE.value());
				            if("alpha".equals(t)) {
				                final int alpha = o.optInt(OCKey.VALUE.value(), 0) / 1000;
				                attributes.setValue(Namespaces.DRAW, "opacity", "draw:opacity", Integer.valueOf(alpha).toString() + "%");
				            }
				        }
			        }
				}
			}
            final JSONObject bitmapAttrs = fillAttributes.optJSONObject(OCKey.BITMAP.value());
            if(bitmapAttrs!=null&&bitmapAttrs.has(OCKey.IMAGE_URL.value())) {
                attributes.setValue(Namespaces.DRAW,  "fill-image-name",  "draw:fill-image-name", DrawFillImage.getFillImage(styleManager, attributes.getValue("draw:fill-image-name"), bitmapAttrs));
                DrawFillImage.applyTilingAttrs(bitmapAttrs, getAttributes());
            }
	        final JSONObject gradientAttrs = fillAttributes.optJSONObject(OCKey.GRADIENT.value());
	        if(gradientAttrs!=null) {
	            attributes.setValue(Namespaces.DRAW, "fill-gradient-name", "draw:fill-gradient-name", Gradient.getGradient(styleManager, attributes.getValue("draw:fill-gradient-name"), fillAttributes));
	        }
		}
		final JSONObject lineAttributes = attrs.optJSONObject(OCKey.LINE.value());
		if(lineAttributes!=null) {
			if(isFrameOrGraphic) {
				final Border border = new Border();
				border.applyFoBorder(attributes.getValue("fo:border"));
				border.applyJsonLine(lineAttributes);
				final String foBorder = border.toString();
				if(foBorder!=null) {
					attributes.setValue(Namespaces.FO, "border", "fo:border", foBorder);
				}
			}
			else {
				final Object type = lineAttributes.opt(OCKey.TYPE.value());
				if(type!=null) {
					if(type==JSONObject.NULL) {
						attributes.remove("draw:stroke");
					}
					else if (type instanceof String) {
						if(((String)type).equals("none")) {
							attributes.setValue(Namespaces.DRAW, "stroke", "draw:stroke", "none");
						}
						else if(((String)type).equals("solid")) {
							final Object lineStyle = lineAttributes.opt(OCKey.STYLE.value());
							if(lineStyle!=null) {
								if(lineStyle==JSONObject.NULL||((String)lineStyle).equals("solid")) {
									attributes.setValue(Namespaces.DRAW, "stroke", "draw:stroke", "solid");
								}
								else {
									attributes.setValue(Namespaces.DRAW, "stroke", "draw:stroke", "dash");
                                    attributes.setValue(Namespaces.DRAW, "stroke-dash", "draw:stroke-dash", StrokeDashStyle.getStrokeDash(styleManager, (String)lineStyle));
								}
							}
							else {
							    final String drawStroke = attributes.getValue("draw:stroke");
							    if(!"dash".equals(drawStroke)) {
							        attributes.setValue(Namespaces.DRAW, "stroke", "draw:stroke", "solid");
							    }
							}
						}
					}
				}
				final JSONObject color = lineAttributes.optJSONObject(OCKey.COLOR.value());
				if(color!=null) {
					if (color==JSONObject.NULL) {
						attributes.remove("svg:stroke-color");
					}
					else {
						final String colorType = color.optString(OCKey.TYPE.value(), null);
						if(colorType!=null) {
		                    if(colorType.equals("auto")) {
								attributes.setValue(Namespaces.SVG, "stroke-color", "svg:stroke-color", "#000000");
		                    }
		                    else {
		                    	attributes.setValue(Namespaces.SVG, "stroke-color", "svg:stroke-color", PropertyHelper.getColor(color, null));
		                    }
						}
					}
				}
				final Object strokeWidth = lineAttributes.opt(OCKey.WIDTH.value());
				if(strokeWidth!=null) {
					if(strokeWidth==JSONObject.NULL) {
						attributes.remove("svg:stroke-width");
					}
					else if(strokeWidth instanceof Number) {
	                    attributes.setValue(Namespaces.SVG, "stroke-width", "svg:stroke-width", (((Number)strokeWidth).doubleValue() / 100) + "mm");
					}
				}
			}

			Marker.applyLineEndAttributes(styleManager, lineAttributes, attributes, true);
            Marker.applyLineEndAttributes(styleManager, lineAttributes, attributes, false);
		}
		final JSONObject drawingAttributes = attrs.optJSONObject(OCKey.DRAWING.value());
		if(drawingAttributes!=null) {
			final Iterator<Entry<String, Object>> drawingIter = drawingAttributes.entrySet().iterator();
			while(drawingIter.hasNext()) {
				final Entry<String, Object> drawingEntry = drawingIter.next();
				final Object value = drawingEntry.getValue();
				switch(OCKey.fromValue(drawingEntry.getKey())) {
					case MARGIN_BOTTOM : {
			            if(value==JSONObject.NULL) {
			            	attributes.remove("fo:margin-bottom");
			            }
			            else {
			                attributes.setValue(Namespaces.FO, "margin-bottom", "fo:margin-bottom", (((Number)value).doubleValue() / 1000) + "cm");
			            }
			            break;
					}
					case MARGIN_LEFT : {
			            if(value==JSONObject.NULL) {
			            	attributes.remove("fo:margin-left");
			            }
			            else {
			                attributes.setValue(Namespaces.FO, "margin-left", "fo:margin-left", (((Number)value).doubleValue() / 1000) + "cm");
			            }
			            break;
					}
					case MARGIN_RIGHT : {
			            if(value==JSONObject.NULL) {
			            	attributes.remove("fo:margin-right");
			            }
			            else {
			                attributes.setValue(Namespaces.FO, "margin-right", "fo:margin-right", (((Number)value).doubleValue() / 1000) + "cm");
			            }
			            break;
					}
					case MARGIN_TOP : {
			            if(value==JSONObject.NULL) {
			            	attributes.remove("fo:margin-top");
			            }
			            else {
			                attributes.setValue(Namespaces.FO, "margin-top", "fo:margin-top", (((Number)value).doubleValue() / 1000) + "cm");
			            }
			            break;
					}
		    		case ANCHOR_HOR_ALIGN : {
		    			if(value==JSONObject.NULL) {
		    				attributes.remove("style:horizontal-pos");
		    			}
		    			else if(value instanceof String) {
		    				String align = null;
		    				if(value.equals("center")) {
		    					align = "center";
		    				}
		    				else if(value.equals("offset")) {
		    					align = "from-left";
		    				}
		    				else if(value.equals("left")) {
		    					align = "left";
		    				}
		    				else if(value.equals("right")) {
		    					align = "right";
		    				}
		    				else if(value.equals("inside")) {
		    					align = "inside";
		    				}
		    				else if(value.equals("outside")) {
		    					align = "outside";
		    				}
		    				if(align!=null) {
		    					attributes.setValue(Namespaces.STYLE, "horizontal-pos", "style:horizontal-pos", align);
		    				}
		    			}
		    			break;
		    		}
		    		case ANCHOR_VERT_ALIGN : {
		    			if(value==JSONObject.NULL) {
		    				attributes.remove("style:vertical-pos");
		    			}
		    			else if(value instanceof String) {
		    				String align = null;
		    				if(value.equals("center")) {
		    					align = "center";
		    				}
		    				else if(value.equals("offset")) {
		    					align = "from-top";
		    				}
		    				else if(value.equals("top")) {
		    					align = "top";
		    				}
		    				else if(value.equals("bottom")) {
		    					align = "bottom";
		    				}
		    				else if(value.equals("inside")) {
		    					align = "inside";
		    				}
		    				else if(value.equals("outside")) {
		    					align = "outside";
		    				}
		    				if(align!=null) {
		    					attributes.setValue(Namespaces.STYLE, "vertical-pos", "style:vertical-pos", align);
		    				}
		    			}
		                break;
		    		}
		    		case ANCHOR_HOR_BASE : {
		    			if(value==JSONObject.NULL) {
		    				attributes.remove("style:horizontal-rel");
		    			}
		    			else if(value instanceof String) {
		    				String base = null;
			                if(value.equals("character")) {
			                	base = "char";
			                }
			                else if(value.equals("margin")) {
			                	base = "page-content";
			                }
			                else if(value.equals("leftMargin")) {
			                	base = "page-start-margin";
			                }
			                else if(value.equals("rightMargin")) {
			                	base = "page-end-margin";
			                }
			                else if(value.equals("column")) {
			                	base = "paragraph";
			                }
			                else if(value.equals("rightMargin")) {
			                	base = "paragraph-end-margin";
			                }
			                else if(value.equals("leftMargin")) {
			                	base = "paragraph-start-margin";
			                }
			                if(base!=null) {
			                	attributes.setValue(Namespaces.STYLE, "horizontal-rel", "style:horizontal-rel", base);
			                }
			                break;
		    			}
		    		}
		    		case ANCHOR_VERT_BASE : {
		    			if(value==JSONObject.NULL) {
		    				attributes.remove("style:vertical-rel");
		    			}
		    			else if(value instanceof String) {
		    				String base = null;
		    				if(value.equals("line")) {
		    					base = "line";
		    				}
		    				else if(value.equals("paragraph")) {
		    					base = "paragraph";
		    				}
		    				else if(value.equals("page")) {
		    					base = "page";
		    				}
		    				else if(value.equals("margin")) {
		    					base = "page-content";
		    				}
		    				else if(value.equals("topMargin")) {
		    					base = "page-content";
		    				}
		    				else if(value.equals("bottomMargin")) {
		    					base = "page-content";
		    				}
		    				if(base!=null) {
		    					attributes.setValue(Namespaces.STYLE, "vertical-rel", "style:vertical-rel", base);
		    				}
		    			}
		    			break;
		    		}
		    		case ANCHOR_HOR_OFFSET :
	    			// PATHTHROUGH INTENDED
		    		case LEFT : {
		    			if(value instanceof Number) {
		    				attributes.setValue(Namespaces.SVG, "x", "svg:x", (((Number)value).intValue() / 100) + "mm");
		    			}
		    			break;
		    		}
		    		case ANCHOR_VERT_OFFSET :
		    		//PATHTHROUGH INTENDED
		    		case TOP : {
		    			if(value instanceof Number) {
		    				attributes.setValue(Namespaces.SVG, "y", "svg:y", (((Number)value).intValue() / 100) + "mm");
		                }
		                break;
		    		}
		    		case TEXT_WRAP_MODE : {
                        if(value==null||value.equals(JSONObject.NULL)) {
                            attributes.remove("style:wrap");
                            attributes.remove("style:wrap-contour");
                        } else {
                        	String textWrapMode = (String) value;
                            if (textWrapMode.equals("topAndBottom")) {
                                attributes.setValue(Namespaces.STYLE, "wrap", "style:wrap", "none");
                                attributes.setValue(Namespaces.STYLE, "wrap-contour", "style:wrap-contour", "false");
                            } else {
                                String textWrapSide = drawingAttributes.optString(OCKey.TEXT_WRAP_SIDE.value());
                                if(textWrapSide!=null) {
                                    if (textWrapMode.equals("square")) {
                                        if (textWrapSide.equals("largest")) {
                                            attributes.setValue(Namespaces.STYLE, "wrap", "style:wrap", "biggest");
                                        } else if (textWrapSide.equals("left")) {
                                            attributes.setValue(Namespaces.STYLE, "wrap", "style:wrap", "left");
                                        } else if (textWrapSide.equals("both")) {
                                        	attributes.setValue(Namespaces.STYLE, "wrap", "style:wrap", "parallel");
                                        } else if (textWrapSide.equals("right")) {
                                        	attributes.setValue(Namespaces.STYLE, "wrap", "style:wrap", "right");
                                        }
                                        attributes.setValue(Namespaces.STYLE, "wrap-contour", "style:wrap-contour", "false");
                                    } else if (textWrapMode.equals("through")) {
                                    	attributes.setValue(Namespaces.STYLE, "wrap", "style:wrap", "run-through");
                                    	attributes.setValue(Namespaces.STYLE, "wrap-contour", "style:wrap-contour", "false");
                                    }
                                }
                            }
                        }
		    			break;
		    		}
		    		case ANCHOR_BEHIND_DOC : {
		    		    if(value==JSONObject.NULL) {
		    		        attributes.remove("style:run-through");
		    		    }
		    		    else if (value instanceof Boolean) {
		    		        attributes.setValue(Namespaces.STYLE, "run-through", "style:run-through", ((Boolean)value).booleanValue() ? "background" : "foreground");
		    		    }
		    		}
		    		case NO_RESIZE : {
		    		    attributes.applyToken(Namespaces.STYLE, "protect", "style:protect", Pair.of("size", value));
		    		    break;
		    		}
		    		case NO_MOVE : {
                        attributes.applyToken(Namespaces.STYLE, "protect", "style:protect", Pair.of("position", value));
		    		    break;
		    		}
                    case MIN_FRAME_HEIGHT : {
                        if(value==JSONObject.NULL) {
                            attributes.remove("fo:min-height");
                        }
                        else {
                            attributes.setValue(Namespaces.FO, "min-height", "fo:min-height", (((Number)value).doubleValue() / 1000) + "cm");
                        }
                        break;
                    }
                    case ASPECT_LOCKED : {
                        if(value==JSONObject.NULL) {
                            attributes.remove("loext:aspect-locked");
                        }
                        else {
                            attributes.setBooleanValue(Namespaces.LOEXT, "aspect-locked", "loext:aspect-locked", (Boolean)value);
                        }
                        break;
                    }
				}
			}
		}
		final JSONObject shapeAttributes = attrs.optJSONObject(OCKey.SHAPE.value());
		if(shapeAttributes!=null) {
			final Iterator<Entry<String, Object>> shapeIter = shapeAttributes.entrySet().iterator();
			while(shapeIter.hasNext()) {
				final Entry<String, Object> shapeEntry = shapeIter.next();
				final Object value = shapeEntry.getValue();
				switch(OCKey.fromValue(shapeEntry.getKey())) {
					case PADDING_BOTTOM : {
	                    if (value==JSONObject.NULL) {
	                        attributes.remove("fo:padding-bottom");
	                    } else {
	                        attributes.setValue(Namespaces.FO, "padding-bottom", "fo:padding-bottom", (((Number)value).intValue() / 100) + "mm");
	                    }
	                    break;
					}
					case PADDING_LEFT : {
	                    if (value==JSONObject.NULL) {
	                        attributes.remove("fo:padding-left");
	                    } else {
	                        attributes.setValue(Namespaces.FO, "padding-left", "fo:padding-left", (((Number)value).intValue() / 100) + "mm");
	                    }
	                    break;
					}
					case PADDING_RIGHT : {
	                    if (value==JSONObject.NULL) {
	                        attributes.remove("fo:padding-right");
	                    } else {
	                        attributes.setValue(Namespaces.FO, "padding-right", "fo:padding-right", (((Number)value).intValue() / 100) + "mm");
	                    }
	                    break;
					}
					case PADDING_TOP : {
	                    if (value==JSONObject.NULL) {
	                        attributes.remove("fo:padding-top");
	                    } else {
	                        attributes.setValue(Namespaces.FO, "padding-top", "fo:padding-top", (((Number)value).intValue() / 100) + "mm");
	                    }
	                    break;
					}
                    case ANCHOR : {
                        if(value==JSONObject.NULL) {
                            attributes.remove("draw:textarea-vertical-align");
                        }
                        else {
                            String anchor = "top";
                            if("centered".equals(value)) {
                                anchor = "middle";
                            }
                            else if("bottom".equals(value)) {
                                anchor = "bottom";
                            }
                            else if("justified".equals(value)) {
                                anchor = "justify";
                            }
                            attributes.setValue(Namespaces.DRAW, "textarea-vertical-align", "draw:textarea-vertical-align", anchor);
                        }
                        break;
                    }
                    case AUTO_RESIZE_HEIGHT : {
                        if(value==JSONObject.NULL) {
                            attributes.remove("draw:auto-grow-height");
                        }
                        else {
                            attributes.setValue(Namespaces.DRAW, "auto-grow-height", "draw:auto-grow-height", ((Boolean)value).toString());
                        }
                        break;
                    }
                    case WORD_WRAP : {
                        attributes.setValue(Namespaces.FO, "wrap-option", "fo:wrap-option", (value instanceof Boolean && ((Boolean)value).booleanValue())||value==JSONObject.NULL ? "wrap" : "no-wrap");
                        break;
                    }
				}
			}
		}
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		final OpAttrs drawingAttrs = attrs.getMap(OCKey.DRAWING.value(), true);
		final OpAttrs shapeAttrs = attrs.getMap(OCKey.SHAPE.value(), true);
		final OpAttrs imageAttrs = attrs.getMap(OCKey.IMAGE.value(), true);
		final OpAttrs lineAttrs = attrs.getMap(OCKey.LINE.value(), true);
		final OpAttrs fillAttrs = attrs.getMap(OCKey.FILL.value(),  true);
		PropertyHelper.createDefaultMarginAttrs(drawingAttrs, attributes);
        final Integer defaultPadding = PropertyHelper.createDefaultPaddingAttrs(shapeAttrs, attributes);
        Border.createDefaultBorderMapAttrsFromFoBorder(drawingAttrs, defaultPadding, attributes);
        final Iterator<Entry<String, AttributeImpl>> propIter = attributes.getUnmodifiableMap().entrySet().iterator();
        while(propIter.hasNext()) {
        	final Entry<String, AttributeImpl> propEntry = propIter.next();
        	final String propName = propEntry.getKey();
        	final String propValue = propEntry.getValue().getValue();
        	switch(propName) {
	    		case "fo:margin-left": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	                if(margin!=null) {
	                	drawingAttrs.put(OCKey.MARGIN_LEFT.value(), margin);
	                	drawingAttrs.put(OCKey.INDENT_LEFT.value(), margin);
	                }
	    			break;
	    		}
	    		case "fo:margin-top": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	                if(margin!=null) {
	                	drawingAttrs.put(OCKey.MARGIN_TOP.value(), margin);
	                }
	    			break;
	    		}
	    		case "fo:margin-right": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	    			if(margin!=null) {
	    				drawingAttrs.put(OCKey.MARGIN_RIGHT.value(), margin);
	    				drawingAttrs.put(OCKey.INDENT_RIGHT.value(), margin);
	    			}
	    			break;
	    		}
	    		case "fo:margin-bottom": {
	    			final Integer margin = AttributesImpl.normalizeLength(propValue, true);
	    			if(margin!=null) {
	    				drawingAttrs.put(OCKey.MARGIN_BOTTOM.value(), margin);
	    			}
	    			break;
	    		}
	    		case "fo:padding-left": {
	    			final Integer padding = AttributesImpl.normalizeLength(propValue, true);
	    			if(padding!=null) {
	    				shapeAttrs.put(OCKey.PADDING_LEFT.value(), padding);
	    			}
	    			break;
	    		}
	    		case "fo:padding-top": {
	    			final Integer padding = AttributesImpl.normalizeLength(propValue, true);
	    			if(padding!=null) {
	    				shapeAttrs.put(OCKey.PADDING_TOP.value(), padding);
	    			}
	    			break;
	    		}
	    		case "fo:padding-right": {
	    			final Integer padding = AttributesImpl.normalizeLength(propValue, true);
	    			if(padding!=null) {
	    				shapeAttrs.put(OCKey.PADDING_RIGHT.value(), padding);
	    			}
	    			break;
	    		}
	    		case "fo:padding-bottom": {
	    			final Integer padding = AttributesImpl.normalizeLength(propValue, true);
	    			if(padding!=null) {
	    				shapeAttrs.put(OCKey.PADDING_BOTTOM.value(), padding);
	    			}
	    			break;
	    		}
	    		case "fo:border-left": {
	    			final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
	                if(border!=null) {
	                	drawingAttrs.put(OCKey.BORDER_LEFT.value(), border);
	                }
	    			break;
	    		}
	    		case "fo:border-top": {
	    			final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
	                if(border!=null) {
	                	drawingAttrs.put(OCKey.BORDER_TOP.value(), border);
	                }
	    			break;
	    		}
	    		case "fo:border-right": {
	    			final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
	                if(border!=null) {
	                	drawingAttrs.put(OCKey.BORDER_RIGHT.value(), border);
	                }
	    			break;
	    		}
	    		case "fo:border-bottom": {
	    			final Map<String, Object> border = Border.createBordermapFromFoBorder(propValue, defaultPadding);
	                if(border!=null) {
	                	drawingAttrs.put(OCKey.BORDER_BOTTOM.value(), border);
	                }
	    			break;
	    		}
	    		case "draw:fill": {
	    			if(propValue.equals("none")) {
	    				fillAttrs.put(OCKey.TYPE.value(), "none");
	    			}
	    			else if(propValue.equals("bitmap")) {
	    				fillAttrs.put(OCKey.TYPE.value(), "bitmap");
	    			}
	    			else if(propValue.equals("gradient")) {
	    				fillAttrs.put(OCKey.TYPE.value(), "gradient");
	    			}
	    			else {
	    				fillAttrs.put(OCKey.TYPE.value(), "solid");
	    			}
	    			break;
	    		}
	    		case "draw:fill-image-name": {
                    final StyleBase styleBase = styleManager.getStyle(propValue, StyleFamily.FILL_IMAGE, contentAutoStyle);
                    if(styleBase!=null) {
                        styleBase.createAttrs(styleManager, attrs);
                        final Object imageUrl = fillAttrs.opt(OCKey.BITMAP.value(), OCKey.IMAGE_URL.value());
                        if(imageUrl instanceof String) {
                            String repeat = attributes.getValue("style:repeat");
                            if(repeat==null) {
                                repeat = "repeat";
                            }
                            DrawFillImage.createTilingAttrs(styleManager, (String)imageUrl, repeat, attributes, fillAttrs);
                        }
                    }
                    break;
	    		}
                case "draw:fill-gradient-name": {
                    StyleBase styleBase = styleManager.getStyle(propValue, StyleFamily.GRADIENT, contentAutoStyle);
                    if(styleBase==null) {
                        styleBase = new Gradient(null);
                    }
                    styleBase.createAttrs(styleManager, attrs);
                    break;
                }
	    		case "fo:background-color": {
	    		    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
	    		    if(color!=null) {
	    		        fillAttrs.put(OCKey.COLOR.value(), color);
	    		    }
	                break;
	    		}
	    		case "draw:fill-color": {
	    		    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
	    		    if(color!=null) {
	    		        fillAttrs.put(OCKey.COLOR.value(), color);
	    		        // check if we have a opacity value for bitmap fillings
	    		        final OpAttrs bitmapAttrs  = fillAttrs.getMap(OCKey.BITMAP.value(), false);
	    		        if(bitmapAttrs!=null) {
	    		            final Object transparency = bitmapAttrs.get(OCKey.TRANSPARENCY.value());
	    		            if(transparency instanceof Number) {
	    		                PropertyHelper.applyOpacityToColorMap(color, (100.0 - (((Number)transparency).doubleValue()) * 100.0));
	    		            }
	    		        }
	    		    }
	                break;
	    		}
                case "draw:opacity": {
                    // we always will create a corresponding bitmap transparency even if there is no bitmap fill.
                    final int opacity = AttributesImpl.getPercentage(propValue, 100).intValue();
                    fillAttrs.getMap(OCKey.BITMAP.value(), true).put(OCKey.TRANSPARENCY.value(), (100 - opacity) / 100.0);
                    // if we already have a fill color then we need to create the corresponding transformation
                    final OpAttrs color = fillAttrs.getMap(OCKey.COLOR.value(), false);
                    if(color!=null) {
                        PropertyHelper.applyOpacityToColorMap(color, opacity);
                    }
                    break;
                }
	    		case "draw:textarea-vertical-align": {
	    		    String anchor = "top";
	    		    if(propValue.equals("middle")) {
	    		        anchor = "centered";
	    		    }
	    		    else if(propValue.equals("bottom")) {
	    		        anchor = "bottom";
	    		    }
	    		    else if(propValue.equals("justify")) {
	    		        anchor = "justified";
	    		    }
	    		    shapeAttrs.put(OCKey.ANCHOR.value(), anchor);
	    		    break;
	    		}
	    		case "style:horizontal-pos": {
	                if (propValue.equals("center")) {
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), "center");
	                } else if (propValue.equals("from-left")) {
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), "offset");
	                } else if (propValue.equals("left")) {
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), "left");
	                } else if (propValue.equals("right")) {
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), "right");
	                } else if (propValue.equals("inside")) {
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), "inside");
	                } else if (propValue.equals("outside")) {
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_ALIGN.value(), "outside");
	                }
	                break;
	    		}
	    		case "style:vertical-pos": {
	                if (propValue.equals("center")) {
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_ALIGN.value(), "center");
	                } else if (propValue.equals("from-top")) {
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_ALIGN.value(), "offset");
	                } else if (propValue.equals("top")) {
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_ALIGN.value(), "top");
	                } else if (propValue.equals("bottom")) {
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_ALIGN.value(), "bottom");
	                } else if (propValue.equals("inside")) {
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_ALIGN.value(), "inside");
	                } else if (propValue.equals("outside")) {
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_ALIGN.value(), "outside");
	                }
	                break;
	    		}
	    		case "style:horizontal-rel": {
	                if(propValue.equals("char"))
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "character");
	                else if(propValue.equals("page-content"))
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "margin");
	                else if(propValue.equals("page-start-margin"))
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "leftMargin");
	                else if(propValue.equals("page-end-margin"))
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "rightMargin");
	                else if(propValue.equals("frame"))
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "column");
	                else if(propValue.equals("frame-content") || propValue.equals("frame-end-margin") || propValue.equals("frame-start-margin"))
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "column");
	                else if(propValue.equals("paragraph") || propValue.equals("paragraph-content"))
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "column");
	                else if(propValue.equals("paragraph-end-margin"))
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "rightMargin");
	                else if(propValue.equals("paragraph-start-margin"))
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_BASE.value(), "leftMargin");
	                break;
	    		}
	    		case "style:vertical-rel": {
	                if(propValue.equals("char"))
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "line");
	                else if(propValue.equals("frame"))
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "paragraph");
	                else if(propValue.equals("frame-content"))
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "paragraph");
	                else if(propValue.equals("line"))
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "line");
	                else if(propValue.equals("page"))
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "page");
	                else if(propValue.equals("page-content"))
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "margin");
	                else if(propValue.equals("paragraph"))
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "paragraph");
	                else if(propValue.equals("paragraph-content"))
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "paragraph");
	                else if(propValue.equals("text"))
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_BASE.value(), "line");
	                break;
	    		}
	    		case "svg:x": {
	    			final int x = AttributesImpl.normalizeLength(propValue);
	                if (x != 0) {
	                	drawingAttrs.put(OCKey.ANCHOR_HOR_OFFSET.value(), x);
	                	drawingAttrs.put(OCKey.LEFT.value(), x);
	                }
	                break;
	    		}
	    		case "svg:y": {
	    			final int y = AttributesImpl.normalizeLength(propValue);
	                if (y != 0) {
	                	drawingAttrs.put(OCKey.ANCHOR_VERT_OFFSET.value(), y);
	                    drawingAttrs.put(OCKey.TOP.value(), y);
	                }
	                break;
	    		}
	    		case "fo:clip": {
	    		    final int[] cInt =getFoClip();
	    		    if(cInt!=null) {
                        imageAttrs.put(OCKey.CROP_TOP.value(), cInt[0]);
                        imageAttrs.put(OCKey.CROP_RIGHT.value(), cInt[1]);
                        imageAttrs.put(OCKey.CROP_BOTTOM.value(), cInt[2]);
                        imageAttrs.put(OCKey.CROP_LEFT.value(), cInt[3]);
	    		    }
                    break;
	    		}
	    		case "style:wrap": {
	                if (propValue.equals("biggest")) {
	                	drawingAttrs.put(OCKey.TEXT_WRAP_MODE.value(), "square");
	                	drawingAttrs.put(OCKey.TEXT_WRAP_SIDE.value(), "largest");
	                } else if (propValue.equals("left")) {
	                	drawingAttrs.put(OCKey.TEXT_WRAP_MODE.value(), "square");
	                	drawingAttrs.put(OCKey.TEXT_WRAP_SIDE.value(), "left");
	                } else if (propValue.equals("none")) {
	                	drawingAttrs.put(OCKey.TEXT_WRAP_MODE.value(), "topAndBottom");
	                } else if (propValue.equals("parallel")) {
	                	drawingAttrs.put(OCKey.TEXT_WRAP_MODE.value(), "square");
	                	drawingAttrs.put(OCKey.TEXT_WRAP_SIDE.value(), "both");
	                } else if (propValue.equals("right")) {
	                	drawingAttrs.put(OCKey.TEXT_WRAP_MODE.value(), "square");
	                	drawingAttrs.put(OCKey.TEXT_WRAP_SIDE.value(), "right");
	                } else if (propValue.equals("run-through")) {
	                	drawingAttrs.put(OCKey.TEXT_WRAP_MODE.value(), "through");
	                	drawingAttrs.put(OCKey.TEXT_WRAP_SIDE.value(), "both");
	                }
	                break;
	    		}
	    		case "style:run-through": {
	    		    if(propValue.equals("background")) {
	    		        drawingAttrs.put(OCKey.ANCHOR_BEHIND_DOC.value(), true);
	    		    }
	    		    else if(propValue.equals("foreground")) {
                        drawingAttrs.put(OCKey.ANCHOR_BEHIND_DOC.value(), false);
	    		    }
	    		    break;
	    		}
	    		case "draw:stroke": {
	                if(propValue.equals("none")) {
	                    lineAttrs.put(OCKey.TYPE.value(), "none");
	                }
	                else if(propValue.equals("solid")) {
	                	lineAttrs.put(OCKey.TYPE.value(), "solid");
	                	lineAttrs.put(OCKey.STYLE.value(), "solid");
	                }
	                else if(propValue.equals("dash")) {
	                	lineAttrs.put(OCKey.TYPE.value(), "solid");
	                	lineAttrs.put(OCKey.STYLE.value(), StrokeDashStyle.getStrokeDashEnumFromStyleName(styleManager, attributes.getUnmodifiableMap().get("draw:stroke-dash")));
	                }
	                break;
	    		}
	    		case "svg:stroke-color": {
                    final Map<String, Object> color = PropertyHelper.createColorMap(propValue);
                    if(color!=null) {
                        lineAttrs.put(OCKey.COLOR.value(), color);
                    }
	                break;
	    		}
	    		case "svg:stroke-width": {
	    			lineAttrs.put(OCKey.WIDTH.value(), AttributesImpl.normalizeLength(propValue));
	    			break;
	    		}
	    		case "draw:marker-start": {
	    		    Marker.createLineEndAttributes(styleManager, propValue, lineAttrs, true);
	    		    break;
	    		}
	    		case "draw:marker-end": {
                    Marker.createLineEndAttributes(styleManager, propValue, lineAttrs, false);
	    		    break;
	    		}
	    		case "style:protect": {
    		       final String[] tokens = propValue.split("\\s+");
    		       for(String token:tokens) {
    		           if(token.equals("size")) {
    		               drawingAttrs.put(OCKey.NO_RESIZE.value(), true);
    		           }
    		           else if(token.equals("position")) {
    		               drawingAttrs.put(OCKey.NO_MOVE.value(), true);
    		           }
    		       }
    		       break;
	    		}
	    		case "fo:min-height": {
	    		    drawingAttrs.put(OCKey.MIN_FRAME_HEIGHT.value(), AttributesImpl.normalizeLength(propValue));
	    		    break;
	    		}
                // it seems that normal shapes are using the word-wrap attribute whereas auto-grow-width is used by text frames
                // !! old lo version are writing the attribute "fo:wrap-option" wrongly (wrap and no-wrap was switched).
                case "fo:wrap-option": {
                    shapeAttrs.put(OCKey.WORD_WRAP.value(), "no-wrap".equals(propValue) ? Boolean.FALSE : Boolean.TRUE);
                    break;
                }
                case "draw:auto-grow-width": {
                    shapeAttrs.put(OCKey.WORD_WRAP.value(), !AttributesImpl.getBoolean(propValue, false));
                    break;
	    		}
	    		case "draw:auto-grow-height": {
	    		    shapeAttrs.put(OCKey.AUTO_RESIZE_HEIGHT.value(), AttributesImpl.getBoolean(propValue, false));
	    		    break;
	    		}
	    		case "loext:aspect-locked": {
	    		    drawingAttrs.put(OCKey.ASPECT_LOCKED.value(), AttributesImpl.getBoolean(propValue, false));
	    		    break;
	    		}
        	}
        }
        if(getIsFrameOrGraphic()!=null&&getIsFrameOrGraphic().booleanValue()) {
            if(!fillAttrs.containsKey(OCKey.TYPE.value())) {
                fillAttrs.put(OCKey.TYPE.value(), "none");
            }
        }
        //border handling: text frames have a border attribute, shapes _can_ have stroke
        if(drawingAttrs.containsKey(OCKey.BORDER.value()) || drawingAttrs.containsKey(OCKey.BORDER_TOP.value())) {
            // convert to "line"
        	@SuppressWarnings("unchecked")
			final HashMap<String, Object> currentBorder = drawingAttrs.containsKey(OCKey.BORDER.value()) ? (HashMap<String, Object>)drawingAttrs.get(OCKey.BORDER.value()) : (HashMap<String, Object>)drawingAttrs.get(OCKey.BORDER_TOP.value());
            if(currentBorder.containsKey(OCKey.STYLE.value()) ) {
                final String style = (String)currentBorder.get(OCKey.STYLE.value());
                if(style.equals("none")) {
                    lineAttrs.put(OCKey.TYPE.value(), "none");
                } else {
                    if(style.equals("dashed")){
                        lineAttrs.put(OCKey.STYLE.value(), "dashed");
                    } else if(style.equals("dotted")) {
                        lineAttrs.put(OCKey.STYLE.value(), "dotted");
                    } else {
                        lineAttrs.put(OCKey.STYLE.value(), "solid");
                    }
                    lineAttrs.put(OCKey.TYPE.value(), "solid");
                }
            }
            if(currentBorder.containsKey(OCKey.WIDTH.value()) ) {
                lineAttrs.put(OCKey.WIDTH.value(), currentBorder.get(OCKey.WIDTH.value()));
            }
            if( currentBorder.containsKey(OCKey.COLOR.value()) ) {
                lineAttrs.put(OCKey.COLOR.value(), currentBorder.get(OCKey.COLOR.value()));
            }
        }
       	drawingAttrs.remove(OCKey.BORDER_TOP.value());
    	drawingAttrs.remove(OCKey.BORDER_BOTTOM.value());
    	drawingAttrs.remove(OCKey.BORDER_LEFT.value());
    	drawingAttrs.remove(OCKey.BORDER_RIGHT.value());
    	drawingAttrs.remove(OCKey.BORDER.value());
    	attrs.removeEmpty(OCKey.DRAWING.value());
    	attrs.removeEmpty(OCKey.SHAPE.value());
    	attrs.removeEmpty(OCKey.IMAGE.value());
    	attrs.removeEmpty(OCKey.LINE.value());
    	attrs.removeEmpty(OCKey.FILL.value());
	}

    public final boolean isAutoStyle() {
        return parentStyle.isAutoStyle();
    }

    public final boolean isContentStyle() {
        return parentStyle.isContentStyle();
    }

    public int[] getFoClip() {
        final String propValue = attributes.getValue("fo:clip");
        if(propValue==null) {
            return null;
        }
        final int start = propValue.indexOf("rect(");
        final int end = propValue.indexOf(")");
        final String clip = (start > -1 && end > -1) ? propValue.substring(start + 5, end) : propValue;
        String clips[] = clip.split(", ");
        if (clips.length!=4) {
            clips = clip.split(" ");
        }
        if(clips.length!=4) {
            return null;
        }
        final int[] cInt = new int[4];
        for(int i=0; i<4; i++) {
            cInt[i] = AttributesImpl.normalizeLength(clips[i]);
        }
        return cInt;
    }

    public void setFoClip(int[] foClip) {
        if(foClip==null||foClip.length!=4||(foClip[0]==0&&foClip[1]==0&&foClip[2]==0&&foClip[3]==0)) {
            getAttributes().remove("fo:clip");
        }
        else {
            final StringBuffer buffer = new StringBuffer();
            buffer.append("rect(");
            for(int i=0; i<4; i++) {
                if(i!=0) {
                    buffer.append(", ");           // xsl fo:clip is only with space separation, but ...
                }
                buffer.append(Double.valueOf(foClip[i] / 1000.0).toString() + "cm");
            }
            buffer.append(")");
            getAttributes().setValue(Namespaces.FO, "clip", "fo:clip", buffer.toString());
        }
    }
}
