/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.properties;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.StyleManager;

final public class TabStops extends StylePropertiesBase {

	List<TabStop> tabStopList = new ArrayList<TabStop>();

	public TabStops(AttributesImpl attributesImpl) {
		super(attributesImpl);
	}

	public List<TabStop> getTabStopList() {
		return tabStopList;
	}

	public void setTabStopList(List<TabStop> tabStopList) {
		this.tabStopList = tabStopList;
	}

	@Override
	public String getQName() {
		return "style:tab-stops";
	}

	@Override
	public String getLocalName() {
		return "tab-stops";
	}

	@Override
	public String getNamespace() {
		return Namespaces.STYLE;
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, boolean contentAutoStyle, OpAttrs attrs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		if(!isEmpty()) {
			SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
			attributes.write(output);
			if(!text.isEmpty()) {
				output.characters(text);
			}
			for(TabStop tabStop:tabStopList) {
				tabStop.writeObject(output);
			}
			final DLList<IElementWriter> content = getContent();
			if(content!=null) {
				final Iterator<IElementWriter> childIter = content.iterator();
				while(childIter.hasNext()) {
					childIter.next().writeObject(output);
				}
			}
			SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
		}
	}

	public boolean isEmpty() {
		return attributes.isEmpty()&&(tabStopList==null||tabStopList.isEmpty())&&(getContent()==null||getContent().isEmpty())&&getTextContent().isEmpty();
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + attributes.hashCode();
		for(TabStop tabStop:tabStopList) {
			hash = hash * 31 + tabStop.hashCode();
		}
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if(!super.equals(obj)) {
			return false;
		}
		final List<TabStop> other = ((TabStops)obj).getTabStopList();
		if(tabStopList.size()!=other.size()) {
			return false;
		}
		for(int i=0; i<tabStopList.size(); i++) {
			if(!tabStopList.get(i).equals(other.get(i))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public TabStops clone() {
		final TabStops clone = (TabStops)_clone();
		clone.setTabStopList(new ArrayList<TabStop>());
		for(TabStop tabStop:tabStopList) {
			clone.getTabStopList().add(tabStop.clone());
		}
		return clone;
	}
}
