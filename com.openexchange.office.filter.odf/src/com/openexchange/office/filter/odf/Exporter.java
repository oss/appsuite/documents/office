/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import java.io.InputStream;

import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.springframework.stereotype.Service;

import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;
import com.openexchange.session.Session;

/**
 * {@link Exporter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
@Service
@RegisteredService
public class Exporter implements IExporter, OsgiBundleContextAware {

	private OsgiBundleContextAndActivator bundleCtx;

    /**
     * Initializes a new {@link Exporter}.
     */
    public Exporter() {
        //
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.openexchange.office.IExporter#createDocument(java.io.InputStream,
     * org.json.JSONObject)
     */
    @Override
    public InputStream createDocument(Session session, InputStream documentStm, String mergeOperations, IResourceManager resourceManager,  DocumentProperties documentProperties, boolean createFinalDocument) {
        InputStream document = null;
        try(OdfOperationDoc odtOperationDocument = new OdfOperationDoc(session, resourceManager, documentProperties)) {
        	bundleCtx.prepareObject(odtOperationDocument);
            odtOperationDocument.setCreateFinalDocument(createFinalDocument);
            odtOperationDocument.loadDocument(documentStm);
            odtOperationDocument.updateDocumentProperties();
            if(mergeOperations!=null) {
                odtOperationDocument.applyOperations(mergeOperations);
            }
            document = odtOperationDocument.save();
        }
        catch(Throwable e) {
            OdfOperationDoc.rethrowFilterException(e, (OdfPackage)null);
        }
        return document;
    }

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}
}
