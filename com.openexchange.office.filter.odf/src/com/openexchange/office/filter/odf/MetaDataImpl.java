/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * 
 * @author sven.jacobiATopen-xchange.com
 * 
 */

package com.openexchange.office.filter.odf;

import java.util.Iterator;

import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;

public class MetaDataImpl extends ElementNSImpl implements IElementWriter {

	private static final long serialVersionUID = 1L;

	private DLList<MetaItem> content;

	public MetaDataImpl(OdfFileDom ownerDocument) {
		super(ownerDocument, Namespaces.OFFICE, "office:meta");
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

        if(content!=null) {
    		SaxContextHandler.startElement(output, Namespaces.OFFICE, "meta", "office:meta");
    		final Iterator<MetaItem> contentIter = content.iterator();
    		while(contentIter.hasNext()) {
    			contentIter.next().writeObject(output);
    		}
    		SaxContextHandler.endElement(output, Namespaces.OFFICE, "meta", "office:meta");
        }
	}

	public MetaItem getItem(String qName) {
		final Iterator<MetaItem> contentIter = getContent().iterator();
		while(contentIter.hasNext()) {
			final MetaItem i = contentIter.next();
			if(qName.equals(i.getQName())) {
				return i;
			}
		}
		return null;
	}

	public void removeItem(String qName) {
		final Iterator<MetaItem> contentIter = getContent().iterator();
		while(contentIter.hasNext()) {
			final MetaItem i = contentIter.next();
			if(qName.equals(i.getQName())) {
				contentIter.remove();
				break;
			}
		}
	}

	public void setItem(MetaItem metaItem) {
		removeItem(metaItem.getQName());
		getContent().add(metaItem);
	}

	public DLList<MetaItem> getContent() {
        if(content==null) {
            content = new DLList<MetaItem>();
        }
        return content;
    }
}
