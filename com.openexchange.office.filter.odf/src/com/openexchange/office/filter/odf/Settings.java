/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import java.util.HashMap;
import java.util.List;
import org.apache.xerces.dom.ElementNSImpl;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.dom.OdfSchemaDocument;
import org.odftoolkit.odfdom.dom.OdfSettingsNamespace;
import org.odftoolkit.odfdom.pkg.NamespaceName;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * The DOM repesentation of the ODF Settings.xml file of an ODF document.
 */
public class Settings extends OdfFileDom {

	private static final long serialVersionUID = 766167617530147885L;

	private OfficeSettings officeSettings;

	/**
	 * Creates the DOM representation of an XML file of an Odf document.
	 *
	 * @param odfDocument   the document the XML files belongs to
	 * @param packagePath   the internal package path to the XML file
	 */
	public Settings(OdfSchemaDocument odfDocument, String packagePath) throws SAXException {
		super(odfDocument, packagePath);
	}

	/** Might be used to initialize specific XML Namespace prefixes/URIs for this XML file*/
	@Override
	protected void initialize() throws SAXException {
		for (NamespaceName name : OdfSettingsNamespace.values()) {
			mUriByPrefix.put(name.getPrefix(), name.getUri());
			mPrefixByUri.put(name.getUri(), name.getPrefix());
		}
		final XMLReader xmlReader = mPackage.getXMLReader();
		super.initialize(new SettingsHandler(this, xmlReader), xmlReader);
		ElementNSImpl rootElement = this.getRootElement();
		if(rootElement == null){		
			rootElement = new ElementNS(this, Namespaces.OFFICE, "office:document-settings");
			this.appendChild(rootElement);
			rootElement.appendChild(new OfficeSettings(this));
		}
	}

	/**
	 * Retrieves the Odf Document
	 *
	 * @return The <code>OdfDocument</code>
	 */
	@Override
	public OdfSchemaDocument getDocument() {
		return (OdfSchemaDocument) mPackageDocument;
	}

	public ConfigItemSet getConfigItemSet(String name, boolean forceCreate) {
		if(name==null) {
			return null;
		}
		final OfficeSettings os = getOfficeSettings(forceCreate);
		if(os==null) {
			return null;
		}
		return os.getConfigItem(name, forceCreate);
	}

	public OfficeSettings getOfficeSettings(boolean forceCreate) {
		if(officeSettings==null&&forceCreate) {
			officeSettings = new OfficeSettings(this);
			getRootElement().appendChild(officeSettings);
		}
		return officeSettings;
	}

	public ConfigItemMapEntry getGlobalViewSettings(boolean forceCreate) {
    	final ConfigItemSet configItemSet = getConfigItemSet("ooo:view-settings", forceCreate);
    	if(configItemSet==null) {
    		return null;
    	}
    	final HashMap<String, IElementWriter> configItems = configItemSet.getItems();
        IElementWriter views = configItems.get("Views");
        if(!(views instanceof ConfigItemMapIndexed)) {
        	if(!forceCreate) {
        		return null;
        	}
        	views = new ConfigItemMapIndexed("Views");
        	configItems.put("Views", views);
        }
		final List<ConfigItemMapEntry> viewEntries = ((ConfigItemMapIndexed)views).getEntries();
		if(viewEntries.isEmpty()) {
			if(!forceCreate) {
				return null;
			}
			viewEntries.add(new ConfigItemMapEntry());
		}
		return viewEntries.get(0);
	}

	public ConfigItemMapNamed getViewTables(ConfigItemMapEntry globalViewSettings, boolean forceCreate) {
		IElementWriter tables = globalViewSettings.getItems().get("Tables");
		if(!(tables instanceof ConfigItemMapNamed)) {
			if(!forceCreate) {
				return null;
			}
			tables = new ConfigItemMapNamed("Tables");
			globalViewSettings.getItems().put("Tables", tables);
		}
		return (ConfigItemMapNamed)tables;
	}

	public ConfigItemMapEntry getViewSettings(ConfigItemMapEntry globalViewSettings, String sheetName, boolean forceCreate) {
		if(sheetName==null||sheetName.isEmpty()) {
			return null;
		}
		final ConfigItemMapNamed tables = getViewTables(globalViewSettings, forceCreate);
		if(tables==null) {
			return null;
		}
		ConfigItemMapEntry sheetViewSettings = tables.getItems().get(sheetName);
		if(sheetViewSettings==null&&forceCreate) {
			sheetViewSettings = new ConfigItemMapEntry(sheetName);
			tables.getItems().put(sheetName, sheetViewSettings);
		}
		return sheetViewSettings;
	}

	public String getActiveSheet() {
		final ConfigItemSet configItemSet = getConfigItemSet("ooo:view-settings", false);
		if(configItemSet!=null) {
			final IElementWriter views = configItemSet.getItems().get("Views");
			if(views instanceof ConfigItemMapIndexed) {
				final List<ConfigItemMapEntry> itemMapEntries = ((ConfigItemMapIndexed)views).getEntries();
				if(!itemMapEntries.isEmpty()) {
					final ConfigItemMapEntry itemMapEntry = itemMapEntries.get(0);
					final IElementWriter activeSheetItem = itemMapEntry.getItems().get("ActiveTable");
					if(activeSheetItem instanceof ConfigItem) {
						return ((ConfigItem)activeSheetItem).getValue();
					}
				}
			}
		}
		return null;
	}

	public void setActiveSheet(String activeSheet) {
		final ConfigItemSet configItemSet = getConfigItemSet("ooo:view-settings", false);
		if(configItemSet!=null) {
			final IElementWriter views = configItemSet.getItems().get("Views");
			if(views instanceof ConfigItemMapIndexed) {
				final List<ConfigItemMapEntry> itemMapEntries = ((ConfigItemMapIndexed)views).getEntries();
				if(!itemMapEntries.isEmpty()) {
					final ConfigItemMapEntry itemMapEntry = itemMapEntries.get(0);
					final IElementWriter activeSheetItem = itemMapEntry.getItems().get("ActiveTable");
					if(activeSheetItem instanceof ConfigItem) {
						((ConfigItem)activeSheetItem).setValue(activeSheet);
					}
				}
			}
		}
	}

	public boolean hasTabsRelativeToIndent() {
    	final ConfigItemSet configItemSet = getConfigItemSet("ooo:configuration-settings", false);
    	if(configItemSet!=null) {
    		final IElementWriter tabsRelativeToIndent = configItemSet.getItems().get("TabsRelativeToIndent");
    		if(tabsRelativeToIndent instanceof ConfigItem) {
    			if(((ConfigItem)tabsRelativeToIndent).getType().equals("boolean")) {
    				return (((ConfigItem)tabsRelativeToIndent).getValue()).equals("true");
    			}
    		}
    	}
		return false;
	}

    public static Integer getConfigValueIntDefault(ConfigItemMapEntry global, ConfigItemMapEntry local, String name, Integer defaultValue) {
        final Integer val = getConfigValueInt(global, local, name);
        if(val!=null) {
            return val;
        }
        return defaultValue;
    }

    public static Integer getConfigValueInt(ConfigItemMapEntry global, ConfigItemMapEntry local, String name) {
        Integer val = null;
        final String stringVal = getConfigValue(global, local, name);
        if(stringVal!=null) {
            try {
                val = Integer.valueOf(stringVal);
            }
            catch(NumberFormatException e) {
                return null;
            }
        }
        return val;
    }

    public static String getConfigValue(ConfigItemMapEntry global, ConfigItemMapEntry local, String name) {
        String val = null;
        if(local!=null) {
            final IElementWriter item = local.getItems().get(name);
            if(item instanceof ConfigItem) {
                val = ((ConfigItem)item).getValue();
            }
        }
        if(val==null&&global!=null) {
            final IElementWriter item = global.getItems().get(name);
            if(item instanceof ConfigItem) {
                val = ((ConfigItem)item).getValue();
            }
        }
        return val;
    }
}
