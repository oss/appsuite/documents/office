/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import java.io.IOException;
import org.apache.xerces.dom.ElementNSImpl;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.odf.styles.StyleManager;

/**
 * @author sven.jacobi@open-xchange.com
 */
public class DOMBuilder extends SaxContextHandler {

    // the context node
    private Node currentNode;
    private OdfFileDom odfFileDom;

    // instantiate DOMBuilder and create a new contextHandler
    public DOMBuilder(Node rootNode, XMLReader xmlReader, OdfFileDom dom) {
    	super(xmlReader, dom);

        // Initialize starting DOM node
        currentNode = rootNode;
        odfFileDom = dom;
        initialize();
    }

    // instantiate DOMBuilder and use existing contextHandler, child elements are inserted at given rootNode
    public DOMBuilder(Node rootNode, SaxContextHandler contextHandler) {
    	super(contextHandler);

        // Initialize starting DOM node
        currentNode = rootNode;
        odfFileDom = (OdfFileDom)rootNode.getOwnerDocument();
        initialize();
    }

    private void initialize() {
        final StyleManager styleManager = odfFileDom.getDocument().getStyleManager();
        if(styleManager!=null) {
            styleManager.initializeStyleOptimizations(this);
        }
    }

    public Node getCurrentNode() {
    	return currentNode;
    }

    @Override
    public void startDocument() {
        //
    }

    @Override
    public void endDocument() {
        //
    }

    /* important, the corresponding ElementNS is created and inserted, its allowed to avoid this by not calling this method */
    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        appendChild(new ElementNS(odfFileDom, attributes, uri, qName));
        return this;
    }

    @Override
    public void endContext(String qName, String characters) {
        if (!characters.isEmpty() && currentNode!=null && qName.equals(currentNode.getPrefix() + ":" + currentNode.getLocalName())) {
            flushText(characters);
        }
    }

    @Override
    public void endElement(String localName, String qName) {
        if (currentNode!=null && qName.equals(currentNode.getPrefix() + ":" + currentNode.getLocalName())) {
            currentNode = currentNode.getParentNode();
        }
    }

    public void appendChild(Node element) {
    	currentNode = currentNode.appendChild(element);
    }

    /**
     * Consumers shall collapse white space characters that occur in <ul> <li>a
     * <text:p> or <text:h> element (so called paragraph elements), and </li>
     * <li>in their descendant elements, if the OpenDocument schema permits the
     * inclusion of character data for the element itself and all its ancestor
     * elements up to the paragraph element. </li></ul> Collapsing white space
     * characters is defined by the following algorithm: 1)The following
     * [UNICODE] characters are replaced by a " " (U+0020, SPACE) character:
     * \ue570HORIZONTAL TABULATION (U+0009) \ue570CARRIAGE RETURN (U+000D)
     * \ue570LINE FEED (U+000A) 2)The character data of the paragraph element
     * and of all descendant elements for which the OpenDocument schema permits
     * the inclusion of character data for the element itself and all its
     * ancestor elements up to the paragraph element, is concatenated in
     * document order. 3)Leading " " (U+0020, SPACE) characters at the start of
     * the resulting text and trailing SPACE characters at the end of the
     * resulting text are removed. 4)Sequences of " " (U+0020, SPACE) characters
     * are replaced by a single " " (U+0020, SPACE) character.
     */
    private void flushText(String newString) {
        final Text text = odfFileDom.createTextNode(newString);
        // if the text is within a text aware component
        if (currentNode instanceof ElementNSImpl) {
            if (isSpaceElement(currentNode)) {
                currentNode.getParentNode().appendChild(text);
            } else {
                currentNode.appendChild(text);
            }
        }
    }

    static boolean isSpaceElement(Node node) {
        return isSpaceElement(node.getNamespaceURI(), node.getLocalName());
    }

    static boolean isSpaceElement(String uri, String localName) {
        return uri != null && uri.equals(Namespaces.TEXT) && localName.equals("s");
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws IOException, SAXException {
        return super.resolveEntity(publicId, systemId);
    }
}
