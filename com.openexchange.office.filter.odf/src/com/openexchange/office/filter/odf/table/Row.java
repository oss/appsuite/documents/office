/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.table;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class Row implements IElementWriter, INodeAccessor, Cloneable {

	private String defaultCellStyleName;
	private String styleName;
	private String visibility;
	private String id;
	private boolean headerRow;
	private boolean softPageBreak = false;

	private DLList<Object> content = new DLList<Object>();

	public Row() {
	}

	public Row(Attributes attributes, boolean headerRow) {
		this.headerRow = headerRow;
		defaultCellStyleName = attributes.getValue("table:default-cell-style-name");
		styleName = attributes.getValue("table:style-name");
		visibility = attributes.getValue("table:visibility");
		id = attributes.getValue("xml:id");
	}

	public boolean isHeaderRow() {
		return headerRow;
	}

	public void setSoftPageBreak() {
		softPageBreak = true;
	}

	public String getStyleName() {
		return styleName;
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	@Override
	public DLList<Object> getContent() {
		return content;
	}

	public String getDefaultCellStyleName() {
	    return defaultCellStyleName;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.TABLE, "table-row", "table:table-row");
		SaxContextHandler.addAttribute(output, Namespaces.TABLE, "default-cell-style-name", "table:default-cell-style-name", defaultCellStyleName);
		SaxContextHandler.addAttribute(output, Namespaces.TABLE, "style-name", "table:style-name", styleName);
		SaxContextHandler.addAttribute(output, Namespaces.TABLE, "visibility", "table:visibility", visibility);
		SaxContextHandler.addAttribute(output, Namespaces.XML, "id", "xml:id", id);
		for(Object child:getContent()) {
			((Cell)child).writeObject(output);
		}
		SaxContextHandler.endElement(output, Namespaces.TABLE, "table-row", "table:table-row");
		if(softPageBreak) {
			SaxContextHandler.startElement(output, Namespaces.TEXT, "soft-page-break", "text:soft-page-break");
			SaxContextHandler.endElement(output, Namespaces.TEXT, "soft-page-break", "text:soft-page-break");
		}
	}

	@Override
	public Row clone() {
		final Row newRow = new Row();
		newRow.headerRow = headerRow;
		newRow.defaultCellStyleName = defaultCellStyleName;
		newRow.styleName = styleName;
		newRow.visibility = visibility;
		newRow.id = id;
		return newRow;
	}
}
