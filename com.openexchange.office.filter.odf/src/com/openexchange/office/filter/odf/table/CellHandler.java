/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.table;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;

public class CellHandler extends SaxContextHandler {

	final Row row;
	final Cell cell;
	int repeated;

    public CellHandler(SaxContextHandler parentContext, Table table, Row row, Attributes attributes, int maxColumns) {
    	super(parentContext);

    	this.row = row;
    	this.cell = new Cell(new AttributesImpl(attributes));

    	final DLList<Object> rowContent = row.getContent();
    	rowContent.add(cell);
    	repeated = 1;
        try {
        	final String val = attributes.getValue("table:number-cells-repeated");
        	if(val!=null) {
        		repeated = Integer.valueOf(Integer.parseInt(val));
        	}
        } catch (NumberFormatException e) {
            //
        }
        if((rowContent.size() + repeated) - 1 > maxColumns) {
        	table.setTableSizeExceeded(true);
        }
    }

	@Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	final SaxContextHandler textHandler = TextContentHelper.startElement(this, cell, attributes, uri, localName, qName);
    	if(textHandler!=null) {
    		return textHandler;
    	}
		final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
		cell.getContent().add(element);
		return new UnknownContentHandler(this, element);
    }

    @Override
	public void endContext(String qName, String characters) {
		super.endContext(qName, characters);

		final DLList<Object> cells = row.getContent();
        while(repeated-->1) {
        	final Cell clone = cell.clone();
        	terminateWithParagraph(cell);
        	cells.add(clone);
        }
		terminateWithParagraph(cell);
    }

    private static void terminateWithParagraph(Cell cell) {
    	final DLList<Object> cellContent = cell.getContent();
    	if(cellContent.isEmpty()||!(cellContent.getLast() instanceof Paragraph)) {
    		cellContent.add(new Paragraph(null));
    	}
    }
}
