/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.draw.IDrawing;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.TableTemplate;

public class Table implements IDrawing, INodeAccessor {

	private AttributesImpl attributes;
	private DLList<Object> elements = new DLList<Object>();

	private final List<Column> columns = new ArrayList<Column>();
	private Rows rows = null;
	private boolean tableSizeExceeded = false;
	private final String documentType;

	public Table(String documentType) {
		this.attributes = new AttributesImpl();
		this.documentType = documentType;
		if(documentType.equals("presentation")) {
		    resetPresTable();
		}
	}

	public Table(String documentType, Attributes attributes) {
		this.attributes = new AttributesImpl(attributes);
		this.documentType = documentType;
	}

	public Rows getRows() {
		if(rows==null) {
			rows = new Rows();
			elements.add(rows);
		}
		return rows;
	}

	@Override
    public AttributesImpl getAttributes() {
	    return attributes;
	}

	public void setAttributes(AttributesImpl attributes) {
	    this.attributes = attributes;
	}

	public String getStyleName() {
		return attributes.getValue("table:style-name");
	}

	public void setStyleName(String styleName) {
		attributes.setValue(Namespaces.TABLE, "style-name", "table:style-name", styleName);
	}

	public String getTemplateName(StyleManager styleManager) {
	    String templateName = attributes.getValue("table:template-name");
	    if("default".equals(templateName)) {
            final StyleBase templateStyle = styleManager.getStyle(templateName, StyleFamily.TABLE_TEMPLATE, false);
            if(templateStyle==null) {
                return "gray";
            }
	    }
	    return templateName;
	}

	public void setTemplateName(String templateName) {
        attributes.setValue(Namespaces.TABLE, "template-name", "table:template-name", templateName);
	}

	@Override
	public DLList<Object> getContent() {
		return getRows().getContent();
	}

	public List<Column> getColumns() {
		return columns;
	}

	public DLList<Object> getElements() {
		return elements;
	}

	public boolean getTableSizeExceeded() {
		return tableSizeExceeded;
	}

	public void setTableSizeExceeded(boolean tableSizeExceeded) {
		this.tableSizeExceeded = tableSizeExceeded;
	}

	private int maxRowGrid = 0;
	public int getMaxRowGrid() {
		return maxRowGrid;
	}
	public void setMaxRowGrid(int maxRowGrid) {
		this.maxRowGrid = maxRowGrid;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.TABLE, "table", "table:table");
		attributes.write(output);
		for(Column column:columns) {
			column.writeObject(output);
		}
		for(Object child:getElements()) {
			if(child instanceof IElementWriter) {
				((IElementWriter)child).writeObject(output);
			}
		}
		SaxContextHandler.endElement(output, Namespaces.TABLE, "table", "table:table");
	}

    @Override
    public DrawingType getType() {
        return DrawingType.TABLE;
    }

    @Override
    public boolean preferRepresentation() {
        return false;
    }

    @Override
    public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle) throws JSONException {
        setStyleName(operationDocument.getDocument().getStyleManager().createStyle(StyleFamily.TABLE, getStyleName(), contentAutoStyle, attrs));

        final JSONObject tableProperties = attrs.optJSONObject(OCKey.TABLE.value());
        if(tableProperties!=null) {
            final JSONArray tableGrid = tableProperties.optJSONArray(OCKey.TABLE_GRID.value());
            if(tableGrid!=null) {
                Integer tableWidth = null;
                // presentation documents are containing the table width in drawing properties whereas text documents are containing it in table properties.
                if(documentType.equals("presentation")) {
                    final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
                    if(drawingAttrs!=null) {
                        tableWidth = drawingAttrs.optInt(OCKey.WIDTH.value());
                    }
                }
                else if(documentType.equals("text")) {
                    tableWidth = tableProperties.optInt(OCKey.WIDTH.value());
                }
                applyTableGrid(operationDocument, tableWidth, tableGrid, contentAutoStyle);
            }
        }
        if(documentType.equals("presentation")) {
            Object styleId = attrs.opt(OCKey.STYLE_ID.value());
            if(styleId!=null) {
                setTemplateName(styleId instanceof String ? (String)styleId : null);
            }
            if(tableProperties!=null) {
                final Object tableExclude = tableProperties.opt(OCKey.EXCLUDE.value());
                if(tableExclude!=null) {
                    resetPresTable();
                    if(tableExclude instanceof JSONArray) {
                        for(Object o:((JSONArray)tableExclude).asList()) {
                            if(o.equals("bandsVert")) {
                                attributes.setBooleanValue(Namespaces.TABLE, "use-banding-columns-styles", "table:use-banding-columns-styles", false);
                            }
                            else if(o.equals("bandsHor")) {
                                attributes.setBooleanValue(Namespaces.TABLE, "use-banding-rows-styles", "table:use-banding-rows-styles", false);
                            }
                            else if(o.equals("firstCol")) {
                                attributes.setBooleanValue(Namespaces.TABLE, "use-first-column-styles", "table:use-first-column-styles", false);
                            }
                            else if(o.equals("firstRow")) {
                                attributes.setBooleanValue(Namespaces.TABLE, "use-first-row-styles", "table:use-first-row-styles", false);
                            }
                            else if(o.equals("lastCol")) {
                                attributes.setBooleanValue(Namespaces.TABLE, "use-last-column-styles", "table:use-last-column-styles", false);
                            }
                            else if(o.equals("lastRow")) {
                                attributes.setBooleanValue(Namespaces.TABLE, "use-last-row-styles", "table:use-last-row-styles", false);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {
        final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
        if(documentType.equals("text")) {
            if(getStyleName()!=null&&!getStyleName().isEmpty()) {
                styleManager.createAutoStyleAttributes(attrs, getStyleName(), StyleFamily.TABLE, contentAutoStyle);
            }
            final Map<String, Object> tableProperties = attrs.getMap(OCKey.TABLE.value(), true);
            if(!columns.isEmpty()) {
                int calculatedTableWidth = 0;
                final List<Integer> tableGrid = new ArrayList<Integer>(columns.size());
                for(Column column:columns) {
                    Integer width = null;
                    final String styleName = column.getStyleName();
                    if(styleName!=null&&!styleName.isEmpty()) {
                        final OpAttrs hardColumnAttrs = new OpAttrs();
                        operationDocument.getDocument().getStyleManager().
                            createAutoStyleAttributes(hardColumnAttrs, styleName, StyleFamily.TABLE_COLUMN, contentAutoStyle);
                        final Map<String, Object> columnAttrs = hardColumnAttrs.getMap(OCKey.COLUMN.value(), false);
                        if(columnAttrs!=null) {
                            final Object o = columnAttrs.get(OCKey.WIDTH.value());
                            if(o instanceof Number) {
                                width = Integer.valueOf(((Number)o).intValue());
                                tableGrid.add(width);
                                calculatedTableWidth += width;
                            }
                        }
                    }
                }
                // ensure that we have collected proper values for the grid,
                // otherwise it is best to provide better defaults
                if(tableGrid.size()!=columns.size()) {
                    tableGrid.clear();
                    for(int i=0; i < columns.size(); i++) {
                        tableGrid.add(100);
                    }
                    if(!tableProperties.containsKey(OCKey.WIDTH.value())) {
                        tableProperties.put(OCKey.WIDTH.value(), "auto");
                    }
                }
                else {
                    if(!tableProperties.containsKey(OCKey.WIDTH.value())) {
                        tableProperties.put(OCKey.WIDTH.value(), calculatedTableWidth);
                    }
                }
                tableProperties.put(OCKey.TABLE_GRID.value(), tableGrid);
            }
            if(!tableProperties.isEmpty()) {
                attrs.put(OCKey.TABLE.value(), tableProperties);
            }
        }
        else if(documentType.equals("presentation")) {
            String templateName = getTemplateName(styleManager);
            if(templateName!=null&&!templateName.isEmpty()) {
                attrs.put(OCKey.STYLE_ID.value(), templateName);
            }
            else if(getStyleName()!=null&&!getStyleName().isEmpty()) {
                styleManager.createAutoStyleAttributes(attrs, getStyleName(), StyleFamily.TABLE, contentAutoStyle);
            }
            final Map<String, Object> tableProperties = attrs.getMap(OCKey.TABLE.value(), true);
            final JSONArray tableExclude = new JSONArray();
            boolean useStyle = attributes.getBooleanValue("table:use-banding-columns-styles", false);
            if(!useStyle) {
                tableExclude.put("bandsVert");
            }
            useStyle = attributes.getBooleanValue("table:use-banding-rows-styles", false);
            if(!useStyle) {
                tableExclude.put("bandsHor");
            }
            useStyle = attributes.getBooleanValue("table:use-first-column-styles", false);
            if(!useStyle) {
                tableExclude.put("firstCol");
            }
            useStyle = attributes.getBooleanValue("table:use-first-row-styles", false);
            if(!useStyle) {
                tableExclude.put("firstRow");
            }
            useStyle = attributes.getBooleanValue("table:use-last-column-styles", false);
            if(!useStyle) {
                tableExclude.put("lastCol");
            }
            useStyle = attributes.getBooleanValue("table:use-last-row-styles", false);
            if(!useStyle) {
                tableExclude.put("lastRow");
            }
            if(!tableExclude.isEmpty()) {
                tableProperties.put(OCKey.EXCLUDE.value(), tableExclude);
            }
            if(!columns.isEmpty()) {
                final List<Integer> tableGrid = new ArrayList<Integer>(columns.size());
                for(Column column:columns) {
                    Integer width = null;
                    final String styleName = column.getStyleName();
                    if(styleName!=null&&!styleName.isEmpty()) {
                        final OpAttrs hardColumnAttrs = new OpAttrs();
                        operationDocument.getDocument().getStyleManager().
                            createAutoStyleAttributes(hardColumnAttrs, styleName, StyleFamily.TABLE_COLUMN, contentAutoStyle);
                        final Map<String, Object> columnAttrs = hardColumnAttrs.getMap(OCKey.COLUMN.value(), false);
                        if(columnAttrs!=null) {
                            final Object o = columnAttrs.get(OCKey.WIDTH.value());
                            if(o instanceof Number) {
                                width = Integer.valueOf(((Number)o).intValue());
                                tableGrid.add(width);
                            }
                        }
                    }
                }
                // ensure that we have collected proper values for the grid,
                // otherwise it is best to provide better defaults
                if(tableGrid.size()!=columns.size()) {
                    tableGrid.clear();
                    for(int i=0; i < columns.size(); i++) {
                        tableGrid.add(100);
                    }
                }
                tableProperties.put(OCKey.TABLE_GRID.value(), tableGrid);
            }
            if(!tableProperties.isEmpty()) {
                attrs.put(OCKey.TABLE.value(), tableProperties);
            }
        }
    }

    private void resetPresTable() {
        attributes.setBooleanValue(Namespaces.TABLE, "use-banding-columns-styles", "table:use-banding-columns-styles", true);
        attributes.setBooleanValue(Namespaces.TABLE, "use-banding-rows-styles", "table:use-banding-rows-styles", true);
        attributes.setBooleanValue(Namespaces.TABLE, "use-first-column-styles", "table:use-first-column-styles", true);
        attributes.setBooleanValue(Namespaces.TABLE, "use-first-row-styles", "table:use-first-row-styles", true);
        attributes.setBooleanValue(Namespaces.TABLE, "use-last-column-styles", "table:use-last-column-styles", true);
        attributes.setBooleanValue(Namespaces.TABLE, "use-last-row-styles", "table:use-last-row-styles", true);
    }

    /*
     * In LO/OO the relative column width is not working, so we have to
     * convert them into absolute grid positions. The table width is required
     * to do so. If a tableWidth is given, then this width will be used to
     * adjust the grid otherwise the table width will be gathered together.
     *
     * https://bugs.documentfoundation.org/show_bug.cgi?id=109305
     */
    public void applyTableGrid(OdfOperationDoc operationDocument, Integer tableWidth, JSONArray tableGrid, boolean contentAutoStyle)
        throws JSONException {

        // TODO... get the table width in case of null...
        if(tableWidth!=null&&!tableGrid.isEmpty()) {
            int totalGrid = 0;
            for(int i= 0; i < tableGrid.length(); i++) {
                totalGrid += tableGrid.getInt(i);
            }

            int currentTotal = 0;
            for(int i=0; i < tableGrid.length(); i++) {
                int remainingWidth = tableWidth - currentTotal;
                int currentGrid = remainingWidth;
                if(i!=tableGrid.length()-1) {
                    currentGrid = Double.valueOf(Double.valueOf(tableGrid.getInt(i)).doubleValue() * tableWidth.doubleValue() / Double.valueOf(totalGrid).doubleValue()).intValue();
                }
                if(currentGrid > remainingWidth) {
                    currentGrid = remainingWidth;
                }
                currentTotal += currentGrid;
                tableGrid.put(i, Integer.valueOf(currentGrid));
            }
        }
        columns.clear();
        for(int i = 0; i < tableGrid.length(); i++) {
            final Column column = new Column();
            columns.add(column);
            final JSONObject attrs = new JSONObject();
            final JSONObject columnAttrs = new JSONObject();
            attrs.put(OCKey.COLUMN.value(), columnAttrs);
            columnAttrs.put(OCKey.WIDTH.value(), tableGrid.get(i));
            column.setStyleName(operationDocument.getDocument().getStyleManager().createStyle(StyleFamily.TABLE_COLUMN, column.getStyleName(), contentAutoStyle, attrs));
        }
    }
}
