/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.table;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class Rows implements IElementWriter, INodeAccessor {

	private DLList<Object> content = new DLList<Object>();

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		boolean isHeaderRow = false;

		// only one header row allowed
		boolean wasHeaderRow = false;

		for(Object o:content) {
		    if(o instanceof Row) {
    			final Row row = (Row)o;
    			if(!wasHeaderRow) {
    				if(row.isHeaderRow()!=isHeaderRow) {
    					if(row.isHeaderRow()) {
    						writeStartHeaderRow(output);
    						isHeaderRow = true;
    					}
    					else {
    						writeEndHeaderRow(output);
    						isHeaderRow = false;
    						wasHeaderRow= true;
    					}
    				}
    			}
    			row.writeObject(output);
		    }
		}
		if(isHeaderRow) {
			writeEndHeaderRow(output);
		}
	}

	@Override
	public DLList<Object> getContent() {
		return content;
	}

	private void writeStartHeaderRow(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.TABLE, "table", "table:table-header-rows");
	}

	private void writeEndHeaderRow(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.endElement(output, Namespaces.TABLE, "table", "table:table-header-rows");
	}
}
