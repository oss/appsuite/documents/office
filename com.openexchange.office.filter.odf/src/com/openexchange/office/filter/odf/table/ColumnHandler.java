/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.table;

import java.util.List;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class ColumnHandler extends SaxContextHandler {

	public ColumnHandler(SaxContextHandler parentContext, Table table, Attributes attributes, int maxColumns) {
    	super(parentContext);

		int repeated = 1;
        try {
        	final String val = attributes.getValue("table:number-columns-repeated");
        	if(val!=null) {
        		repeated = Integer.valueOf(Integer.parseInt(val));
        	}
        } catch (NumberFormatException e) {
            //
        }
        final List<Column> columns = table.getColumns();
        if((columns.size()+repeated)-1 > maxColumns) {
        	table.setTableSizeExceeded(true);
        }
        while(--repeated>=0) {
        	columns.add(new Column(attributes));
        }
	}
}
