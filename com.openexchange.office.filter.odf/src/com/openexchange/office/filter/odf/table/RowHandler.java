/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.table;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class RowHandler extends SaxContextHandler {

	final Table table;
	final Row row;
	final int maxColumns;
	int repeated;

	public RowHandler(SaxContextHandler parentContext, Table table, Attributes attributes, boolean headerRow, int maxColumns, int maxRows, int maxCells) {
		super(parentContext);

		this.table = table;
		this.row = new Row(attributes, headerRow);
		this.maxColumns = maxColumns;

		repeated = 1;
        try {
        	final String val = attributes.getValue("table:number-rows-repeated");
        	if(val!=null) {
        		repeated = Integer.valueOf(Integer.parseInt(val));
        	}
        } catch (NumberFormatException e) {
            //
        }
        final DLList<Object> rows = table.getRows().getContent();
        if((rows.size() + repeated) - 1 > maxRows) {
        	table.setTableSizeExceeded(true);
        }
        if(((rows.size() + repeated) - 1) * table.getColumns().size() > maxCells) {
        	table.setTableSizeExceeded(true);
        }
        rows.add(row);
	}

	@Override
	public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
		if(qName.equals("table:table-cell")) {
    		return new CellHandler(this, table, row, attributes, maxColumns);
    	}
		return this;
	}

	@Override
	public void endContext(String qName, String characters) {
		super.endContext(qName, characters);

		int gridSpan = 0;
		for(Object o:row.getContent()) {
			gridSpan += ((Cell)o).getColumnSpan();
		}
		if(table.getMaxRowGrid() < gridSpan) {
			table.setMaxRowGrid(gridSpan);
		}
		final DLList<Object> rows = table.getContent();
        while(repeated-->1) {
			rows.add(row.clone());
        }
	}
}
