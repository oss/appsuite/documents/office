/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.table;

import java.util.List;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;

public class TableHandler extends SaxContextHandler {

	final Table table;
	final int maxRows;
	final int maxCells;
	final int maxColumns;
	boolean headerRow = false;

	public TableHandler(SaxContextHandler parentContext, Table table) {
    	super(parentContext);

    	this.table = table;
    	final Object oMaxRows = parentContext.getFileDom().getDocument().getPackage().getRunTimeConfiguration().get("maxTableRows");
    	maxRows = oMaxRows instanceof Integer ? ((Integer)oMaxRows).intValue() : 1500;
    	final Object oMaxCells = parentContext.getFileDom().getDocument().getPackage().getRunTimeConfiguration().get("maxTableCells");
    	maxCells = oMaxRows instanceof Integer ? ((Integer)oMaxCells).intValue() : 1500;
    	final Object oMaxColumns = parentContext.getFileDom().getDocument().getPackage().getRunTimeConfiguration().get("maxTableColumns");
    	maxColumns = oMaxColumns instanceof Integer ? ((Integer)oMaxColumns).intValue() : 15;
	}

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {

    	// just ignoring following elements, they should not be used in odt documents
    	if(qName.equals("table:table-column")) {
    		return new ColumnHandler(this, table, attributes, maxColumns);
    	}
    	else if(qName.equals("table:table-row")) {
    		return new RowHandler(this, table, attributes, headerRow, maxColumns, maxRows, maxCells);
    	}
    	else if(qName.equals("text:soft-page-break")) {
    		final DLNode<Object> lastNode = table.getRows().getContent().getLastNode();
    		if(lastNode!=null) {
    			((Row)lastNode.getData()).setSoftPageBreak();
    		}
    	}
    	else if(qName.equals("table:table-header-rows")) {
    		headerRow = true;
    		return this;
    	}
    	else if(qName.equals("table:table-column-group")||qName.equals("table:table-columns")||qName.equals("table:table-header-columns")) {
    		return this;
    	}
    	else if(qName.equals("table:table-row-group")||qName.equals("table:table-rows")) {
    		return this;
    	}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	table.getElements().add(element);
		return new UnknownContentHandler(this, element);
    }

	@Override
	public void endElement(String localName, String qName) {
		super.endElement(localName, qName);

		if(qName.equals("table:table-header-rows")) {
			headerRow = false;
		}
	}

	@Override
	public void endContext(String qName, String characters) {
		if(qName.equals("table:table")) {
			// now we ensure that our column list matches used cells,
			// so we definitely can use this column list to create and
			// update the tabel grid
			final List<Column> columnList = table.getColumns();
			final int columns = columnList.size();
			int columnsNeeded = table.getMaxRowGrid() - columns;
			if(columnsNeeded > 0) {
				final Column lastColumn = columns > 0 ? columnList.get(columns - 1) :  new Column();
				while(columnsNeeded-- > 0) {
					columnList.add(lastColumn.clone());
				}
			}
		}
	}
}
