/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.table;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;

public class Cell implements INodeAccessor, IElementWriter, Cloneable {

	private final AttributesImpl attributes;
	private int columnsSpanned = 1;
	private int rowsSpanned = 1;

	private DLList<Object> content = new DLList<Object>();

	public Cell() {
		this.attributes = new AttributesImpl();
		init();
	}

	public Cell(AttributesImpl attributesImpl) {
		this.attributes = attributesImpl;
		init();
	}

	private void init() {
	    AttributeImpl attribute = attributes.remove("table:number-columns-spanned");
	    if(attribute!=null) {
	        columnsSpanned = attribute.getIntValue(1);
	    }
	    attribute = attributes.remove("table:number-rows-spanned");
	    if(attribute!=null) {
	        rowsSpanned = attribute.getIntValue(1);
	    }
	}

	public int getColumnSpan() {
	    return columnsSpanned;
	}

	public void setColumnSpan(Integer n) {
	    columnsSpanned = n;
	}

	public int getRowSpan() {
	    return rowsSpanned;
	}

	public void setRowSpan(Integer n) {
	    rowsSpanned = n;
	}

	public String getStyleName() {
		return attributes.getValue("table:style-name");
	}

	public void setStyleName(String styleName) {
		attributes.setValue(Namespaces.TABLE, "style-name", "table:style-name", styleName);
	}

	@Override
	public DLList<Object> getContent() {
		return content;
	}

	@Override
    public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.TABLE, "table-cell", "table:table-cell");
		attributes.write(output);
		if(columnsSpanned>1) {
		    SaxContextHandler.addAttribute(output, Namespaces.TABLE, "number-columns-spanned", "table:number-columns-spanned", columnsSpanned);
		}
		if(rowsSpanned>1) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "number-rows-spanned", "table:number-rows-spanned", rowsSpanned);
		}
		TextContentHelper.write(output, getContent());
		SaxContextHandler.endElement(output, Namespaces.TABLE, "table-cell", "table:table-cell");

		final int columnSpan = getColumnSpan();
		for(int i = 1; i < columnSpan; i++) {
			SaxContextHandler.startElement(output, Namespaces.TABLE, "covered-table-cell", "table:covered-table-cell");
			SaxContextHandler.endElement(output, Namespaces.TABLE, "covered-table-cell", "table:covered-table-cell");
		}
	}

	@Override
	public Cell clone() {
		final Cell clone = new Cell();
		final String cellStyle = getStyleName();
		if(cellStyle!=null) {
			clone.setStyleName(cellStyle);
		}
		final int columnSpan = getColumnSpan();
		if(columnSpan!=1) {
			clone.setColumnSpan(columnSpan);
		}
		final int rowSpan = getRowSpan();
		if(rowSpan!=1) {
			clone.setRowSpan(rowSpan);
		}
		return clone;
	}
}
