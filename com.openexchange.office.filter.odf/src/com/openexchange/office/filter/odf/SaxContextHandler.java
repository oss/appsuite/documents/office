/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.odftoolkit.odfdom.pkg.OdfName;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class SaxContextHandler extends DefaultHandler {

	private class ContextStackEntry {

		final private SaxContextHandler saxContextHandler;
	    final private StringBuilder stringBuilder;
	    final private String qName;

		ContextStackEntry(SaxContextHandler context, String qN, StringBuilder builder) {
			saxContextHandler = context;
			qName = qN;
			stringBuilder = builder;
		}
		SaxContextHandler getContextHandler() {
			return saxContextHandler;
		}
	    public String getQName() {
	    	return qName;
	    }
		public StringBuilder getStringBuilder() {
			return stringBuilder;
		}
	}

	final private XMLReader xmlReader;
	final private OdfFileDom fileDom;
    final private List<IStartElementListener> startElementListener;
	final private Stack<ContextStackEntry> contextStack;

	// initializes the root context
	public SaxContextHandler(XMLReader reader, OdfFileDom fDom) {
		xmlReader = reader;
		fileDom = fDom;
		startElementListener = new ArrayList<IStartElementListener>();
		contextStack = new Stack<ContextStackEntry>();
		contextStack.push(new ContextStackEntry(this, "", new StringBuilder()));
	}

	public SaxContextHandler(SaxContextHandler parentContext) {
		xmlReader = parentContext.xmlReader;
		fileDom = parentContext.fileDom;
		startElementListener = parentContext.startElementListener;
		contextStack = parentContext.contextStack;
	}

	public OdfFileDom getFileDom() {
		return fileDom;
	}

	public void addStartElementListener(IStartElementListener listener) {
	    startElementListener.add(listener);
	}

	public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
		return this;
	}

	public void endElement(String localName, String qName) {
	}

	public void characters(String characters) {
	}

	public void endContext(String qName, String characters) {

	}

	@Override
    public final void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		final SaxContextHandler newContextHandler = startElement(attributes, uri, localName, qName);
		contextStack.push(new ContextStackEntry(newContextHandler, qName, new StringBuilder()));
		xmlReader.setContentHandler(newContextHandler);
		for(IStartElementListener listener:startElementListener) {
		    listener.startElement(attributes, uri, localName, qName);
		}
	}

    @Override
    public final void endElement(String uri, String localName, String qName) {
    	final ContextStackEntry oldContextStack = contextStack.pop();
        endContext(oldContextStack.getQName(), oldContextStack.getStringBuilder().toString());
        final ContentHandler oldContentHandler = contextStack.peek().getContextHandler();
    	xmlReader.setContentHandler(oldContentHandler);
    	if(oldContentHandler instanceof SaxContextHandler) {
    		((SaxContextHandler)oldContentHandler).endElement(localName, qName);
    	}
    }

    public SaxContextHandler getContextHandler() {
        return contextStack.peek().getContextHandler();
    }

    @Override
    public final void characters(char[] ch, int startPosition, int length) {
    	final StringBuilder builder = contextStack.peek().getStringBuilder();
    	final String s = new String(ch, startPosition, length);
    	builder.append(s);
    	characters(s);
    }

    public static void addOdfAttribute(SerializationHandler output, OdfName attributeName, String type, String value) {
    	final String namespaceUri = "";
    	final String localName = attributeName.getLocalName();
    	final String rawName = attributeName.getQName();
    	try {
			output.addAttribute(namespaceUri, localName, rawName, type, value);
		} catch (SAXException e) {
		    //
		}
    }

    public static void addAttribute(SerializationHandler output, String namespaceUri, String localName, String qName, String value)
    	throws SAXException {

    	if(value!=null) {
			output.addAttribute(namespaceUri, localName, qName, "", value);
    	}
    }

    public static void addAttribute(SerializationHandler output, String namespaceUri, String localName, String qName, Integer value)
    	throws SAXException {

    	if(value!=null) {
			output.addAttribute(namespaceUri, localName, qName, "", value.toString());
    	}
    }

    public static void addAttributes(SerializationHandler output, NamedNodeMap attrs, HashSet<String> ignorables) {
		for(int i=0; i<attrs.getLength(); i++) {
			final Node attr = attrs.item(i);
            final String localName = attr.getLocalName();
            if(ignorables!=null) {
            	if(ignorables.contains(localName)) {
            		continue;
            	}
            }
            final String attrName = attr.getNodeName();
            final String attrValue = attr.getNodeValue();
            // Determine the Attr's type.
            final String type = ((Attr) attr).getSchemaTypeInfo().getTypeName();
			try {
				output.addAttribute(
				        "",
				        localName,
				        attrName,
				        type,
				        attrValue);
			} catch (SAXException e) {
			    //
			}
		}
    }

    public static void startElement(SerializationHandler output, String namespaceUri, String localName, String qName)
    	throws SAXException {

    	output.startElement(namespaceUri, localName, qName);
    }

    public static void endElement(SerializationHandler output, String namespaceUri, String localName, String qName)
    	throws SAXException {

    	output.endElement(namespaceUri, localName, qName);
    }

    public static void startElement(SerializationHandler output, OdfName odfName) {
		try {
			output.startElement(odfName.getUri(), odfName.getLocalName(), odfName.getQName());
		} catch (SAXException e) {
		    //
		}
    }

    public static void endElement(SerializationHandler output, OdfName odfName) {
		try {
			output.endElement(odfName.getUri(), odfName.getLocalName(), odfName.getQName());
		} catch (SAXException e) {
		    //
		}
    }

    public static void serializeElement(SerializationHandler output, Element e) {
		try {
			final String uri = e.getNamespaceURI();
			final String local = e.getLocalName();
			final String qname = e.getPrefix()!=null?e.getPrefix()+":"+e.getLocalName():e.getLocalName();
			output.startElement(uri, local, qname);
			if(e.hasAttributes()) {
				addAttributes(output, e.getAttributes(), null);
			}
			serializeChildElements(output, e);
			output.endElement(uri, local, qname);
		} catch (SAXException e1) {
		    //
		}
    }

    public static void serializeChildElements(SerializationHandler output, Element e) {
    	try {
			final NodeList childs = e.getChildNodes();
			if(childs!=null) {
				for(int i=0; i<childs.getLength(); i++) {
					final Object child = childs.item(i);
					if(child instanceof Element) {
						serializeElement(output, (Element)child);
					}
					else if(child instanceof org.w3c.dom.Text) {
						output.characters(((org.w3c.dom.Text)child).getNodeValue());
					}
				}
			}
    	} catch (SAXException e1) {
    	    //
    	}
    }
}
