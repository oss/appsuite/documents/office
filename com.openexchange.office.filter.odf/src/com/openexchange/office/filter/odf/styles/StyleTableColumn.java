/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.TableColumnProperties;

final public class StyleTableColumn extends StyleBase {

	private TableColumnProperties tableColumnProperties = new TableColumnProperties(new AttributesImpl());

	public StyleTableColumn(String name, boolean automaticStyle, boolean contentStyle) {
		super(StyleFamily.TABLE_COLUMN, name, automaticStyle, contentStyle);
	}

	public StyleTableColumn(String name, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, defaultStyle, automaticStyle, contentStyle);
	}

	@Override
    public StyleFamily getFamily() {
		return StyleFamily.TABLE_COLUMN;
	}

	public TableColumnProperties getTableColumnProperties() {
		return tableColumnProperties;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		tableColumnProperties.writeObject(output);
		writeMapStyleList(output);
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		if(styleBase instanceof StyleTableColumn) {
			getTableColumnProperties().mergeAttrs(((StyleTableColumn)styleBase).getTableColumnProperties());
		}
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {

	    tableColumnProperties.applyAttrs(styleManager, attrs);
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {

		tableColumnProperties.createAttrs(styleManager, isContentStyle(), attrs);
	}

	@Override
	protected int _hashCode() {
		return tableColumnProperties.hashCode();
	}

	@Override
	protected boolean _equals(StyleBase e) {
		final StyleTableColumn other = (StyleTableColumn)e;
		if(!tableColumnProperties.equals(other.tableColumnProperties)) {
			return false;
		}
		return true;
	}

	@Override
	public StyleTableColumn clone() {
		final StyleTableColumn clone = (StyleTableColumn)_clone();
		clone.tableColumnProperties = tableColumnProperties.clone();
		return clone;
	}
}
