/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;

final public class TextOutlineStyle extends TextListStyle {

	public TextOutlineStyle(String name) {
		super(name, false, false);
	}

	public TextOutlineStyle(String name, AttributesImpl attributesImpl) {
		super(name, attributesImpl, false, false);
	}

	@Override
    public StyleFamily getFamily() {
		return StyleFamily.OUTLINE_STYLE;
	}

	@Override
	public String getQName() {
		return "text:outline-style";
	}

	@Override
	public String getLocalName() {
		return "outline-style";
	}

	@Override
	public String getNamespace() {
		return Namespaces.TEXT;
	}
}
