/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.core.spreadsheet.FormatCode;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.properties.NumberNumber;
import com.openexchange.office.filter.odf.properties.NumberText;
import com.openexchange.office.filter.odf.properties.TextProperties;

final public class NumberPercentageStyle extends NumberStyleBase {

	public NumberPercentageStyle(String name, boolean automaticStyle, boolean contentStyle) {
		super(null, name, automaticStyle, contentStyle);
	}

	public NumberPercentageStyle(String name, AttributesImpl attributesImpl, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, false, automaticStyle, contentStyle);
	}

	@Override
	public String getQName() {
		return "number:percentage-style";
	}

	@Override
	public String getLocalName() {
		return "percentage-style";
	}

	@Override
	public String getNamespace() {
		return Namespaces.NUMBER;
	}

	@Override
	public String getFormat(StyleManager styleManager, Map<String, String> additionalStyleProperties, boolean contentAutoStyle) {
        String result = "";
		final Iterator<IElementWriter> propertiesIter = getContent().iterator();
		while(propertiesIter.hasNext()) {
			final IElementWriter properties = propertiesIter.next();
            if (properties instanceof NumberNumber) {
                result += ((NumberNumber)properties).getNumberFormat();
            }
            else if (properties instanceof NumberText) {
                String textcontent = ((NumberText)properties).getTextContent();
                if (textcontent == null || textcontent.length() == 0) {
                    textcontent = " ";
                }
                result += textcontent;
            }
            else if (properties instanceof TextProperties) {
            	result += ((TextProperties)properties).getColorFromElement();
            }
        }
		if(!getMapStyleList().isEmpty()) {
            result = getMapStyleList().get(0).getMapping(styleManager, contentAutoStyle, getFamily()) + ";" + result;
		}
        return result;
	}

	@Override
	public void setFormat(FormatCode _formatCode, Map<String, String> additionalStyleProperties) {

	    final String formatCode = _formatCode.toString();
        boolean hasNumber = false; // only one number token can be created, next one will be part of a text token
        String currentTextToken = "";
        for(int pos = 0; pos < formatCode.length(); ++pos){
            char c = formatCode.charAt(pos);
            if(c == '"'){
                //add all characters until the next quotation to the current text token
                currentTextToken += c;
                while(pos < formatCode.length() - 1  ){
                    c = formatCode.charAt(pos + 1);
                    if(c == '\\'){
                        currentTextToken += c;
                        if((pos > formatCode.length() - 2))
                            break; //invalid!
                        currentTextToken += c;
                        currentTextToken += formatCode.charAt(pos + 2);
                        ++pos;
                    } else if(c == '"' ){
                        currentTextToken += c;
                        ++pos;
                        break;
                    } else {
                        currentTextToken += c;
                    }
                    ++pos;
                }
            } else if(c == '['){
                if(!currentTextToken.isEmpty()){
                	getContent().add(new NumberText(currentTextToken));
                    currentTextToken = "";
                }
                int closePos = formatCode.indexOf(']', pos);
                if(closePos < 0 || closePos < pos + 2){ // minimum two characters inside, closing bracket not quoted
                    break; // invalid
                }
                final String bracketToken = formatCode.substring(pos, closePos + 1);
                final String color = getColorElement(bracketToken.substring(1, bracketToken.length() - 1));
                if(getContent().isEmpty() && color != null) {
                	final TextProperties textProperties = new TextProperties(new AttributesImpl());
                	textProperties.getAttributes().setValue(Namespaces.FO, "color", "fo:color", color);
                	getContent().add(textProperties);
                } else {
                    break; // invalid
                }
                pos = closePos;

            } else if(!hasNumber && (c == '#' || c == '0' || c == '.' )){
                if(!currentTextToken.isEmpty()){
                	getContent().add(new NumberText(currentTextToken));
                    currentTextToken = "";
                }
                final int numPos = pos;
                while(++pos < formatCode.length()){
                    // TODO: hashes can only be at the beginning interrupted by comma
                    c = formatCode.charAt(pos);
                    if((c != '#' && c != '.' && c != ',' && c != '0') || (pos == (formatCode.length() - 1))) {
                        String numberToken = formatCode.substring(numPos, pos + 1);
                        if(numberToken.charAt(numberToken.length() - 1) == '.'){
                            //add replacement chars
                            int spacePos = formatCode.indexOf(' ', pos-1);
                            int bracketPos = formatCode.indexOf('[', pos-1);
                            if(spacePos < 0) {
                                spacePos = bracketPos;
                            } else if(bracketPos < 0){
                                bracketPos = spacePos;
                            }
                            spacePos = Math.min(spacePos, bracketPos);
                            if(spacePos < 0){ // if not found add the rest of the format TODO: Are there other delimiters than space and bracket? ?
                                spacePos = formatCode.length();
                            }
                            if(spacePos > 0){
                                numberToken += formatCode.substring(pos, spacePos );
                                pos = spacePos;
                            }
                        }
                        if(numberToken.endsWith("%")) {
                            emitNumber(numberToken.substring(0, numberToken.length() - 1));
                            getContent().add(new NumberText("%"));
                        }
                        else {
                        	emitNumber(numberToken);
                        }
                        break;
                    }
                }
            } else if( c == '\\'){
                //add this and the next character to the text token
                currentTextToken += c;
                if(pos > formatCode.length() - 2){
                    break;//invalid
                }
                currentTextToken += formatCode.charAt(pos + 1);
                ++pos;
            } else {
                currentTextToken += c;
            }
        }
        if(!currentTextToken.isEmpty()){
        	getContent().add(new NumberText(currentTextToken));
        }
	}

    protected void emitNumber(String numberToken){
        final NumberNumber number = new NumberNumber(new AttributesImpl());
        /* Process part before the decimal point (if any) */
        int nDigits = 0;
        char ch;
        int pos;
        for (pos = 0; pos < numberToken.length()
                && (ch = numberToken.charAt(pos)) != '.'; pos++) {
            if (ch == ',') {
            	number.getAttributes().setBooleanValue(Namespaces.NUMBER, "grouping", "number:grouping", true);
            } else if (ch == '0') {
                nDigits++;
            }
        }
        number.getAttributes().setIntValue(Namespaces.NUMBER, "min-integer-digits", "number:min-integer-digits", nDigits);

        /* Number of decimal places is the length after the decimal */
        if (pos < numberToken.length()) {
        	number.getAttributes().setIntValue(Namespaces.NUMBER, "decimal-places", "number:decimal-places", numberToken.length() - (pos + 1));
            if(pos < numberToken.length() - 1 && numberToken.charAt(pos + 1) != '0'){
            	number.getAttributes().setValue(Namespaces.NUMBER, "decimal-replacement", "number:decimal-replacement", numberToken.substring(pos + 1));
            }
        }
        else {
        	number.getAttributes().setIntValue(Namespaces.NUMBER, "decimal-places", "number:decimal-places", 0);
        }
        getContent().add(number);
    }

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		//
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		// TODO Auto-generated method stub

	}
}
