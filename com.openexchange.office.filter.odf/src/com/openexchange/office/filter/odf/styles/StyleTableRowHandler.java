/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.properties.MapProperties;
import com.openexchange.office.filter.odf.properties.StylePropertiesBaseHandler;

public class StyleTableRowHandler extends SaxContextHandler {

	final StyleManager styleManager;
	final StyleTableRow tableRowStyle;

	public StyleTableRowHandler(SaxContextHandler parentContext, StyleTableRow tableRowStyle, StyleManager styleManager) {
		super(parentContext);

		this.styleManager = styleManager;
		this.tableRowStyle = tableRowStyle;
	}

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	switch(qName) {
    		case "style:table-row-properties": {
    			return new StylePropertiesBaseHandler(this, tableRowStyle.getTableRowProperties().setAttributes(attributes));
    		}
    		case "style:map": {
    			if(!tableRowStyle.isDefaultStyle()) {
	    			final MapProperties mapProperties = new MapProperties(new AttributesImpl(attributes));
	    			tableRowStyle.getMapStyleList().add(mapProperties);
	    			return new StylePropertiesBaseHandler(this, mapProperties);
    			}
    		}
    	}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	return new UnknownContentHandler(this, element);
    }

    @Override
	public void endContext(String qName, String characters) {
		super.endContext(qName, characters);

		styleManager.addStyle(tableRowStyle);
	}
}
