/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;

public class StyleHeaderFooter implements IElementWriter, INodeAccessor {

	private final AttributesImpl attributes;
	private String type;
	private String id;

	private DLList<Object> content;

	public StyleHeaderFooter(String id, String type) {
		this.attributes = new AttributesImpl();
		this.type = type;
		this.id = id;
	}

	public StyleHeaderFooter(String id, String type, Attributes attributes) {
		this.attributes = new AttributesImpl(attributes);
		this.type = type;
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	@Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
		return content;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		String name = "";
		switch(type) {
			case "header_default" : {
				name = "header";
				break;
			}
			case "header_first" : {
				name = "header-first";
				break;
			}
			case "header_even" : {
				name = "header-left";
				break;
			}
			case "footer_default" : {
				name = "footer";
				break;
			}
			case "footer_first" : {
				name = "footer-first";
				break;
			}
			case "footer_even" : {
				name = "footer-left";
				break;
			}
		}
		SaxContextHandler.startElement(output, Namespaces.STYLE, name, "style:" + name);
		attributes.write(output);
		TextContentHelper.write(output, getContent());
		SaxContextHandler.endElement(output, Namespaces.STYLE, name, "style:" + name);
	}
}
