/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;

final public class Marker extends StyleBase {

	public Marker(String name) {
		super(null, name, false, false);
	}

	public Marker(String name, AttributesImpl attributesImpl) {
		super(name, attributesImpl, false, false, false);
	}

	@Override
    public StyleFamily getFamily() {
		return StyleFamily.MARKER;
	}

    @Override
    public String getQName() {
        return "draw:marker";
    }

    @Override
    public String getLocalName() {
        return "marker";
    }

    @Override
    public String getNamespace() {
        return Namespaces.DRAW;
    }

    @Override
    protected void writeNameAttribute(SerializationHandler output) throws SAXException {
        output.addAttribute(Namespaces.DRAW, "name", "draw:name", "", getName());
    }

    @Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

    @Override
    public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
        // TODO Auto-generated method stub

    }

    @Override
    public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mergeAttrs(StyleBase style) {
        //
    }

    @Override
    public Marker clone() {
        return (Marker)_clone();
    }

    @Override
    protected int _hashCode() {
        return 0;
    }

    @Override
    protected boolean _equals(StyleBase e) {
        return true;
    }

    public static void applyLineEndAttributes(StyleManager styleManager, JSONObject lineAttributes, AttributesImpl attributes, boolean start) {

        final Object lineEnd = lineAttributes.opt(start ? OCKey.HEAD_END_TYPE.value() : OCKey.TAIL_END_TYPE.value());
        final Object lineEndLength = lineAttributes.opt(start ? OCKey.HEAD_END_LENGTH.value() : OCKey.TAIL_END_LENGTH.value());
        final Object lineEndWidth = lineAttributes.opt(start ? OCKey.HEAD_END_WIDTH.value() : OCKey.TAIL_END_WIDTH.value());

        if(lineEnd!=null||lineEndLength!=null||lineEndWidth!=null) {
            final String qName = start ? "draw:marker-start" : "draw:marker-end";
            final String mName = attributes.getValue(qName);
            final StyleBase currentMarker = mName!=null ? styleManager.getStyle(mName, StyleFamily.MARKER, false) : null;

            int currentMarkerSize = -1;
            String currentDisplayName = "msArrowEnd";

            if(currentMarker!=null) {
                final String displayName = currentMarker.getDisplayName();
                if(displayName!=null) {
                    switch(displayName) {
                        case "msArrowDiamondEnd 1" :
                        case "msArrowEnd 1" :
                        case "msArrowOpenEnd 1" :
                        case "msArrowOvalEnd 1" :
                        case "msArrowStealthEnd 1" : currentMarkerSize = 0; break;
                        case "msArrowDiamondEnd 2" :
                        case "msArrowEnd 2" :
                        case "msArrowOpenEnd 2" :
                        case "msArrowOvalEnd 2" :
                        case "msArrowStealthEnd 2" : currentMarkerSize = 1; break;
                        case "msArrowDiamondEnd 3" :
                        case "msArrowEnd 3" :
                        case "msArrowOpenEnd 3" :
                        case "msArrowOvalEnd 3" :
                        case "msArrowStealthEnd 3" : currentMarkerSize = 2; break;
                        case "msArrowDiamondEnd 4" :
                        case "msArrowEnd 4" :
                        case "msArrowOpenEnd 4" :
                        case "msArrowOvalEnd 4" :
                        case "msArrowStealthEnd 4" : currentMarkerSize = 3; break;
                        case "msArrowDiamondEnd 5" :
                        case "msArrowEnd 5" :
                        case "msArrowOpenEnd 5" :
                        case "msArrowOvalEnd 5" :
                        case "msArrowStealthEnd 5" : currentMarkerSize = 4; break;
                        case "msArrowDiamondEnd 6" :
                        case "msArrowEnd 6" :
                        case "msArrowOpenEnd 6" :
                        case "msArrowOvalEnd 6" :
                        case "msArrowStealthEnd 6" : currentMarkerSize = 5; break;
                        case "msArrowDiamondEnd 7" :
                        case "msArrowEnd 7" :
                        case "msArrowOpenEnd 7" :
                        case "msArrowOvalEnd 7" :
                        case "msArrowStealthEnd 7" : currentMarkerSize = 6; break;
                        case "msArrowDiamondEnd 8" :
                        case "msArrowEnd 8" :
                        case "msArrowOpenEnd 8" :
                        case "msArrowOvalEnd 8" :
                        case "msArrowStealthEnd 8" : currentMarkerSize = 7; break;
                        case "msArrowDiamondEnd 9" :
                        case "msArrowEnd 9" :
                        case "msArrowOpenEnd 9" :
                        case "msArrowOvalEnd 9" :
                        case "msArrowStealthEnd 9" : currentMarkerSize = 8; break;
                    }
                    if(currentMarkerSize!=-1) {
                        currentDisplayName = displayName.substring(0, displayName.length()-2);
                    }
                }
            }
            if(currentMarkerSize==-1) {
                currentMarkerSize = 4;
            }
           if(lineEnd instanceof String) {
                switch((String)lineEnd) {
                    case "none" : currentDisplayName = null; break;
                    case "triangle" : currentDisplayName = "msArrowEnd"; break;
                    case "stealth" : currentDisplayName = "msArrowStealthEnd"; break;
                    case "diamond" : currentDisplayName = "msArrowDiamondEnd"; break;
                    case "oval" : currentDisplayName = "msArrowOvalEnd"; break;
                    case "arrow" : currentDisplayName = "msArrowOpenEnd"; break;
                }
            }
            else if(lineEnd==JSONObject.NULL) {
                currentDisplayName = null;
            }
            if(currentDisplayName!=null) {
                if(lineEndLength!=null) {
                    int newLength = 1;
                    if("small".equals(lineEndLength)) {
                        newLength = 0;
                    }
                    else if("large".equals(lineEndLength)) {
                        newLength = 2;
                    }
                    currentMarkerSize = ((currentMarkerSize / 3) * 3) + newLength;
                }
                if(lineEndWidth!=null) {
                    int newWidth = 1;
                    final boolean openEnd = "msArrowOpenEnd".equals(currentDisplayName);
                    String gWidth = openEnd ? "0.316cm" : "0.21cm";
                    if("small".equals(lineEndWidth)) {
                        newWidth = 0;
                        gWidth = openEnd ? "0.245cm" : "0.139cm";
                    }
                    else if("large".equals(lineEndWidth)) {
                        newWidth = 2;
                        gWidth = openEnd ? "0.42cm" : "0.349cm";
                    }
                    currentMarkerSize = currentMarkerSize % 3 + 3 * newWidth;
                    if(start) {
                        attributes.setValue(Namespaces.DRAW, "marker-start-width", "draw:marker-start-width", gWidth);
                    }
                    else {
                        attributes.setValue(Namespaces.DRAW, "marker-end-width", "draw:marker-end-width", gWidth);
                    }
                }
                final String newDisplayName = currentDisplayName + " " + ((Integer)(currentMarkerSize + 1)).toString();
                StyleBase destMarker = styleManager.getStyleByDisplayName(newDisplayName, StyleFamily.MARKER, false);
                if(destMarker==null) {
                    destMarker = new Marker(styleManager.getUniqueStyleName(StyleFamily.MARKER, false));
                    destMarker.setDisplayName(newDisplayName);
                    String viewBox = null;
                    String d = null;

                    switch(newDisplayName) {
                        case "msArrowDiamondEnd 1" : viewBox="0 0 200 200"; d="M100 0l100 100-100 100-100-100z"; break;
                        case "msArrowDiamondEnd 2" : viewBox="0 0 200 300"; d="M100 0l100 150-100 150-100-150z"; break;
                        case "msArrowDiamondEnd 3" : viewBox="0 0 200 500"; d="M100 0l100 250-100 250-100-250z"; break;
                        case "msArrowDiamondEnd 4" : viewBox="0 0 300 200"; d="M150 0l150 100-150 100-150-100z"; break;
                        case "msArrowDiamondEnd 5" : viewBox="0 0 300 300"; d="M150 0l150 150-150 150-150-150z"; break;
                        case "msArrowDiamondEnd 6" : viewBox="0 0 300 500"; d="M150 0l150 250-150 250-150-250z"; break;
                        case "msArrowDiamondEnd 7" : viewBox="0 0 500 200"; d="M250 0l250 100-250 100-250-100z"; break;
                        case "msArrowDiamondEnd 8" : viewBox="0 0 500 300"; d="M250 0l250 150-250 150-250-150z"; break;
                        case "msArrowDiamondEnd 9" : viewBox="0 0 500 500"; d="M250 0l250 250-250 250-250-250z"; break;
                        case "msArrowEnd 1" : viewBox="0 0 200 200"; d="M100 0l100 200h-200z"; break;
                        case "msArrowEnd 2" : viewBox="0 0 200 300"; d="M100 0l100 300h-200z"; break;
                        case "msArrowEnd 3" : viewBox="0 0 200 500"; d="M100 0l100 500h-200z"; break;
                        case "msArrowEnd 4" : viewBox="0 0 300 200"; d="M150 0l150 200h-300z"; break;
                        case "msArrowEnd 5" : viewBox="0 0 300 300"; d="M150 0l150 300h-300z"; break;
                        case "msArrowEnd 6" : viewBox="0 0 300 500"; d="M150 0l150 500h-300z"; break;
                        case "msArrowEnd 7" : viewBox="0 0 500 200"; d="M250 0l250 200h-500z"; break;
                        case "msArrowEnd 8" : viewBox="0 0 500 300"; d="M250 0l250 300h-500z"; break;
                        case "msArrowEnd 9" : viewBox="0 0 500 500"; d="M250 0l250 500h-500z"; break;
                        case "msArrowOpenEnd 1" : viewBox="0 0 350 350"; d="M175 0l175 318-53 32-122-224-123 224-52-32z"; break;
                        case "msArrowOpenEnd 2" : viewBox="0 0 350 450"; d="M175 0l175 409-53 41-122-288-123 288-52-41z"; break;
                        case "msArrowOpenEnd 3" : viewBox="0 0 350 600"; d="M175 0l175 546-53 54-122-384-123 384-52-54z"; break;
                        case "msArrowOpenEnd 4" : viewBox="0 0 450 350"; d="M225 0l225 318-68 32-157-224-158 224-67-32z"; break;
                        case "msArrowOpenEnd 5" : viewBox="0 0 450 450"; d="M225 0l225 409-68 41-157-288-158 288-67-41z"; break;
                        case "msArrowOpenEnd 6" : viewBox="0 0 450 600"; d="M225 0l225 546-68 54-157-384-158 384-67-54z"; break;
                        case "msArrowOpenEnd 7" : viewBox="0 0 600 350"; d="M300 0l300 318-90 32-210-224-210 224-90-32z"; break;
                        case "msArrowOpenEnd 8" : viewBox="0 0 600 450"; d="M300 0l300 409-90 41-210-288-210 288-90-41z"; break;
                        case "msArrowOpenEnd 9" : viewBox="0 0 600 600"; d="M300 0l300 546-90 54-210-384-210 384-90-54z"; break;
                        case "msArrowOvalEnd 1" : viewBox="0 0 200 200"; d="M100 0l50 14 36 36 14 50-14 50-36 36-50 14-50-14-36-36-14-50 14-50 36-36z"; break;
                        case "msArrowOvalEnd 2" : viewBox="0 0 200 300"; d="M100 0l50 21 36 54 14 75-14 75-36 54-50 21-50-21-36-54-14-75 14-75 36-54z"; break;
                        case "msArrowOvalEnd 3" : viewBox="0 0 200 500"; d="M100 0l50 35 36 90 14 125-14 125-36 90-50 35-50-35-36-90-14-125 14-125 36-90z"; break;
                        case "msArrowOvalEnd 4" : viewBox="0 0 300 200"; d="M150 0l75 14 54 36 21 50-21 50-54 36-75 14-75-14-54-36-21-50 21-50 54-36z"; break;
                        case "msArrowOvalEnd 5" : viewBox="0 0 300 300"; d="M150 0l75 21 54 54 21 75-21 75-54 54-75 21-75-21-54-54-21-75 21-75 54-54z"; break;
                        case "msArrowOvalEnd 6" : viewBox="0 0 300 500"; d="M150 0l75 35 54 90 21 125-21 125-54 90-75 35-75-35-54-90-21-125 21-125 54-90z"; break;
                        case "msArrowOvalEnd 7" : viewBox="0 0 500 200"; d="M250 0l125 14 90 36 35 50-35 50-90 36-125 14-125-14-90-36-35-50 35-50 90-36z"; break;
                        case "msArrowOvalEnd 8" : viewBox="0 0 500 300"; d="M250 0l125 21 90 54 35 75-35 75-90 54-125 21-125-21-90-54-35-75 35-75 90-54z"; break;
                        case "msArrowOvalEnd 9" : viewBox="0 0 500 500"; d="M250 0l125 35 90 90 35 125-35 125-90 90-125 35-125-35-90-90-35-125 35-125 90-90z"; break;
                        case "msArrowStealthEnd 1" : viewBox="0 0 200 200"; d="M100 0l100 200-100-80-100 80z"; break;
                        case "msArrowStealthEnd 2" : viewBox="0 0 200 300"; d="M100 0l100 300-100-120-100 120z"; break;
                        case "msArrowStealthEnd 3" : viewBox="0 0 200 500"; d="M100 0l100 500-100-200-100 200z"; break;
                        case "msArrowStealthEnd 4" : viewBox="0 0 300 200"; d="M150 0l150 200-150-80-150 80z"; break;
                        case "msArrowStealthEnd 5" : viewBox="0 0 300 300"; d="M150 0l150 300-150-120-150 120z"; break;
                        case "msArrowStealthEnd 6" : viewBox="0 0 300 500"; d="M150 0l150 500-150-200-150 200z"; break;
                        case "msArrowStealthEnd 7" : viewBox="0 0 500 200"; d="M250 0l250 200-250-80-250 80z"; break;
                        case "msArrowStealthEnd 8" : viewBox="0 0 500 300"; d="M250 0l250 300-250-120-250 120z"; break;
                        case "msArrowStealthEnd 9" : viewBox="0 0 500 500"; d="M250 0l250 500-250-200-250 200z"; break;
                    }
                    if(viewBox!=null) {
                        destMarker.getAttributes().setValue(Namespaces.SVG, "viewBox", "svg:viewBox", viewBox);
                        destMarker.getAttributes().setValue(Namespaces.SVG, "d", "svg:d", d);
                    }
                    styleManager.addStyle(destMarker);
                }
                attributes.setValue(Namespaces.DRAW, start ? "marker-start" : "marker-end", qName, destMarker.getName());
            }
            else {
                attributes.remove(qName);
            }
        }
    }

    public static void createLineEndAttributes(StyleManager styleManager, String name, OpAttrs lineAttrs, boolean start) {
        String lineType = "arrow";
        String length = null;
        String width = null;

        final StyleBase marker = styleManager.getStyle(name, StyleFamily.MARKER, false);
        if(marker!=null) {
            final String displayName = marker.getDisplayName();
            if(displayName!=null) {
                if(displayName.startsWith("msArrowStealthEnd")) {
                    lineType = "stealth";
                }
                else if(displayName.startsWith("msArrowOvalEnd")) {
                    lineType = "oval";
                }
                else if(displayName.startsWith("msArrowEnd")) {
                    lineType = "triangle";
                }
                else if(displayName.startsWith("msArrowDiamondEnd")) {
                    lineType = "diamond";
                }
                if(displayName.length()>2) {
                    final String number = displayName.substring(displayName.length()-1, displayName.length());
                    try {
                        int n = Integer.parseInt(number) - 1;
                        if(n<=8) {
                            final int l = n / 3;
                            if(l==0) {
                                length = "small";
                            }
                            else if(l==2) {
                                length = "large";
                            }
                            final int w = n % 3;
                            if(w==0) {
                                width = "small";
                            }
                            else if(w==2) {
                                width = "large";
                            }
                        }
                    }
                    catch(NumberFormatException e) {
                        //
                    }
                }
            }
            if(start) {
                lineAttrs.put(OCKey.HEAD_END_TYPE.value(), lineType);
                if(length!=null) {
                    lineAttrs.put(OCKey.HEAD_END_LENGTH.value(), length);
                }
                if(width!=null) {
                    lineAttrs.put(OCKey.HEAD_END_WIDTH.value(), width);
                }
            }
            else {
                lineAttrs.put(OCKey.TAIL_END_TYPE.value(), lineType);
                if(length!=null) {
                    lineAttrs.put(OCKey.TAIL_END_LENGTH.value(), length);
                }
                if(width!=null) {
                    lineAttrs.put(OCKey.TAIL_END_WIDTH.value(), width);
                }
            }
        }
    }
}
