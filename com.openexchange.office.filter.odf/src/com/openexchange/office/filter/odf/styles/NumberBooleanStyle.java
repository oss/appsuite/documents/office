/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Map;
import org.json.JSONObject;
import com.openexchange.office.filter.core.spreadsheet.FormatCode;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;

final public class NumberBooleanStyle extends NumberStyleBase {

	public NumberBooleanStyle(String name, boolean automaticStyle, boolean contentStyle) {
		super(null, name, automaticStyle, contentStyle);
	}

	public NumberBooleanStyle(String name, AttributesImpl attributesImpl, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, false, automaticStyle, contentStyle);
	}

	@Override
	public String getQName() {
		return "number:boolean-style";
	}

	@Override
	public String getLocalName() {
		return "boolean-style";
	}

	@Override
	public String getNamespace() {
		return Namespaces.NUMBER;
	}

	@Override
	public String getFormat(StyleManager styleManager, Map<String, String> additionalStyleProperties, boolean contentAutoStyle) {
		return "BOOLEAN";
	}

	@Override
	public void setFormat(FormatCode formatCode, Map<String, String> additionalStyleProperties) {
	    //
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
	    //
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		// TODO Auto-generated method stub
	}
}
