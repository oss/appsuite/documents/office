/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.HashSet;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.ParagraphProperties;
import com.openexchange.office.filter.odf.properties.TextProperties;

final public class StyleParagraph extends StyleBase implements IParagraphProperties {

	private ParagraphProperties paragraphProperties = new ParagraphProperties(new AttributesImpl());
	private TextProperties textProperties = new TextProperties(new AttributesImpl());

	// the outline level for this style, if null then parent styles have to be checked also,
	// 0 indicates that this style does not have an outline level
	private Integer defaultOutlineLevel = null;

	public StyleParagraph(String name, boolean automaticStyle, boolean contentStyle) {
		super(StyleFamily.PARAGRAPH, name, automaticStyle, contentStyle);
	}

	public StyleParagraph(String name, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, defaultStyle, automaticStyle, contentStyle);
	}

	@Override
    public StyleFamily getFamily() {
		return StyleFamily.PARAGRAPH;
	}

	public int getDefaultOutlineLevel(StyleManager styleManager) {
	    if(defaultOutlineLevel!=null) {
	        return defaultOutlineLevel.intValue();
	    }
        return getDefaultOutlineLevel(styleManager, this, new HashSet<String>());
	}

	private int getDefaultOutlineLevel(StyleManager styleManager, StyleParagraph style, HashSet<String> checkedStyles) {
	    if(defaultOutlineLevel==null) {
    	    final String outlineLevel = style.getAttribute("style:default-outline-level");
            if(outlineLevel!=null) {
                defaultOutlineLevel = AttributesImpl.getInteger(outlineLevel, Integer.valueOf(0));
            }
            else {
                // get the value from parent
                StyleParagraph parentStyle = null;
                final String parentStyleName = style.getParent();
                if(parentStyleName!=null&&!parentStyleName.isEmpty()) {
                    parentStyle = (StyleParagraph)styleManager.getStyle(parentStyleName, style.getFamily(), style.isContentStyle());
                }
                checkedStyles.add(style.getName());
                if(parentStyle!=null&&!checkedStyles.contains(parentStyleName)) {
                    defaultOutlineLevel = getDefaultOutlineLevel(styleManager, parentStyle, checkedStyles);
                }
                else {
                    defaultOutlineLevel = Integer.valueOf(0);
                }
            }
	    }
        return defaultOutlineLevel.intValue();
	}

	@Override
    public ParagraphProperties getParagraphProperties() {
		return paragraphProperties;
	}

	public void setParagraphPropertis(ParagraphProperties paragraphProperties) {
		this.paragraphProperties = paragraphProperties;
	}

	public TextProperties getTextProperties() {
		return textProperties;
	}

	public void setTextPropertis(TextProperties textProperties) {
		this.textProperties = textProperties;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		paragraphProperties.writeObject(output);
		textProperties.writeObject(output);
		writeMapStyleList(output);
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		if(styleBase instanceof IParagraphProperties) {
			getParagraphProperties().mergeAttrs(((IParagraphProperties)styleBase).getParagraphProperties());
		}
		if(styleBase instanceof ITextProperties) {
			getTextProperties().mergeAttrs(((ITextProperties)styleBase).getTextProperties());
		}
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs)
		throws JSONException {

	    final String masterPageName = attrs.optString("masterPageName", null);
	    if(masterPageName!=null) {
	        attributes.setValue(Namespaces.STYLE, "master-page-name", "style:master-page-name", masterPageName);
	    }
		paragraphProperties.applyAttrs(styleManager, attrs);
		textProperties.applyAttrs(styleManager, attrs);
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		final String listStyleName = this.attributes.getValue("style:list-style-name");
		if(listStyleName!=null) {
			attrs.getMap(OCKey.PARAGRAPH.value(), true).put(OCKey.LIST_STYLE_ID.value(), listStyleName);
		}
		paragraphProperties.createAttrs(styleManager, isContentStyle(), attrs);
		textProperties.createAttrs(styleManager, isContentStyle(), attrs);
	}

	@Override
	protected int _hashCode() {

		int result = 0;

		result += paragraphProperties.hashCode();
		result += textProperties.hashCode();
		return result;
	}

	@Override
	protected boolean _equals(StyleBase e) {
		final StyleParagraph other = (StyleParagraph)e;
		if(!paragraphProperties.equals(other.paragraphProperties)) {
			return false;
		}
		if(!textProperties.equals(other.textProperties)) {
			return false;
		}
		return true;
	}

	@Override
	public StyleParagraph clone() {
		final StyleParagraph clone = (StyleParagraph)_clone();
		clone.paragraphProperties = paragraphProperties.clone();
		clone.textProperties = textProperties.clone();
		return clone;
	}
}
