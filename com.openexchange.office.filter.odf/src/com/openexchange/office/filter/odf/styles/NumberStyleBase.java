/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Iterator;
import java.util.Map;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.spreadsheet.FormatCode;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.NumberDay;
import com.openexchange.office.filter.odf.properties.NumberDayOfWeek;
import com.openexchange.office.filter.odf.properties.NumberEra;
import com.openexchange.office.filter.odf.properties.NumberHours;
import com.openexchange.office.filter.odf.properties.NumberMinutes;
import com.openexchange.office.filter.odf.properties.NumberMonth;
import com.openexchange.office.filter.odf.properties.NumberQuarter;
import com.openexchange.office.filter.odf.properties.NumberSeconds;
import com.openexchange.office.filter.odf.properties.NumberText;
import com.openexchange.office.filter.odf.properties.NumberWeekOfYear;
import com.openexchange.office.filter.odf.properties.NumberYear;
import com.openexchange.office.filter.odf.properties.StyleNumberPropertiesBase;

abstract public class NumberStyleBase extends StyleBase {

	private DLList<IElementWriter> content;

	public NumberStyleBase(StyleFamily family, String name, boolean automaticStyle, boolean contentStyle) {
		super(family, name, automaticStyle, contentStyle);
	}

	protected NumberStyleBase(String name, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle,	boolean contentStyle) {
		super(name, attributesImpl, defaultStyle, automaticStyle, contentStyle);
	}

	final public DLList<IElementWriter> getContent() {
		if(content==null) {
			content = new DLList<IElementWriter>();
		}
		return content;
	}

	@Override
	final public StyleFamily getFamily() {
		return StyleFamily.DATA_STYLE;
	}

	@Override
    public AttributesImpl getAttributes() {
		return attributes;
	}

	public abstract String getFormat(StyleManager styleManager, Map<String, String> additionalStyleProperties, boolean contentAutoStyle);

	public abstract void setFormat(FormatCode formatCode, Map<String, String> additionalStyleProperties);

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		final Iterator<IElementWriter> iter = getContent().iterator();
		while(iter.hasNext()) {
			iter.next().writeObject(output);
		}
		writeMapStyleList(output);
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

	@Override
	final protected int _hashCode() {
		int hash = 0;
		if(content!=null&&!content.isEmpty()) {
			final Iterator<IElementWriter> contentIter = content.iterator();
			while(contentIter.hasNext()) {
				hash = hash * 31 + contentIter.next().hashCode();
			}
		}
		return hash;
	}

	@Override
	final protected boolean _equals(StyleBase e) {
		final NumberStyleBase other = (NumberStyleBase)e;
		if(content!=null&&!content.isEmpty()) {
			if(other.content==null||other.content.isEmpty()) {
				return false;
			}
			if(content.size()!=other.content.size()) {
				return false;
			}
			final Iterator<IElementWriter> sourceIter = content.iterator();
			final Iterator<IElementWriter> destIter = other.content.iterator();
			while(sourceIter.hasNext()) {
				if(!sourceIter.next().equals(destIter.next())) {
					return false;
				}
			}
		}
		else if(other.content!=null&&!other.content.isEmpty()) {
			return false;
		}
		return true;
	}

	@Override
	public NumberStyleBase clone() {
		return (NumberStyleBase)_clone();
	}

    static protected String getColorElement(String colorToken) {
        String ret = null;
        if(colorToken.equalsIgnoreCase("RED")) {
            ret = "#ff0000";
        } else if(colorToken.equalsIgnoreCase("BLUE")) {
            ret = "#0000ff";
        } else if(colorToken.equalsIgnoreCase("GREEN")) {
            ret = "#00ff00";
        } else if(colorToken.equalsIgnoreCase("WHITE")) {
            ret = "#ffffff";
        } else if(colorToken.equalsIgnoreCase("MAGENTA")) {
            ret = "#ff00ff";
        } else if(colorToken.equalsIgnoreCase("YELLOW")) {
            ret = "#ffff00";
        } else if(colorToken.equalsIgnoreCase("CYAN")) {
            ret = "#00ffff";
        } else if(colorToken.equalsIgnoreCase("BLACK")) {
            ret = "#000000";
        }
        return ret;
    }

    static public void processDateTimeEntry(NumberStyleBase numberStyleBase, FormatCode.DateTimeEntry entry) {
        int count = entry.getDateTimeLenght();
        StyleNumberPropertiesBase element = null;
        switch(entry.getDateTimeSymbol()) {
            case 'D': {
                if(count > 2) {
                    element = new NumberDayOfWeek(new AttributesImpl());
                    element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count > 3));
                } else {
                    element = new NumberDay(new AttributesImpl());
                    element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count > 1));
                }
                break;
            }
            case 'G': {
                element = new NumberEra(new AttributesImpl());
                element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count > 3));
                break;
            }
            case 'h': {
                element = new NumberHours(new AttributesImpl());
                element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count > 1));
                break;
            }
            case 'M': {
                element = new NumberMonth(new AttributesImpl());
                element.getAttributes().setBooleanValue(Namespaces.NUMBER, "textual", "number:textual", count > 2);
                element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count % 2 == 0));
                break;
            }
            case 'm': {
                element = new NumberMinutes(new AttributesImpl());
                element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count > 1));
                break;
            }
            case 'N': {
                element = new NumberDayOfWeek(new AttributesImpl());
                element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count > 3));
                if(count > 3) {
                    numberStyleBase.getContent().add(new NumberText(", "));
                }
                break;
            }
            case 'Q': {
                element = new NumberQuarter(new AttributesImpl());
                element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count > 1));
                break;
            }
            case 'S':
            case 's': {
                element = new NumberSeconds(new AttributesImpl());
                element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count > 1));
                break;
            }
            case 'W': {
                element = new NumberWeekOfYear(new AttributesImpl());
                break;
            }
            case 'Y': {
                element = new NumberYear(new AttributesImpl());
                element.getAttributes().setValue(Namespaces.NUMBER, "style", "number:style", isLongIf(count > 3));
            }
        }
        if(element!=null) {
            numberStyleBase.getContent().add(element);
            if(entry.isElapsedTime()) {
                numberStyleBase.getAttributes().setBooleanValue(Namespaces.NUMBER, "truncate-on-overflow", "number:truncate-on-overflow", false);
            }
        }
    }

    /**
    * Add long or short style to an element.
    * @param isLong true if this is number:style="long"; false if number:style="short"
    * @return the string "long" or "short"
    */
   private static String isLongIf(boolean isLong) {
       return ((isLong) ? "long" : "short");
   }

   public void appendNumberText(String text) {
       NumberText numberText = null;
       if(getContent().getLast() instanceof NumberText) {
           numberText = (NumberText)getContent().getLast();
           numberText.setTextContent(numberText.getTextContent() + text);
       }
       else {
           getContent().add(new NumberText(text));
       }
   }
}
