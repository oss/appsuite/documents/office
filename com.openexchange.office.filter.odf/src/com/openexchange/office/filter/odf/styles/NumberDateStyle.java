/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.core.spreadsheet.FormatCode;
import com.openexchange.office.filter.core.spreadsheet.FormatCode.Entry;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.properties.NumberAmPm;
import com.openexchange.office.filter.odf.properties.NumberDay;
import com.openexchange.office.filter.odf.properties.NumberDayOfWeek;
import com.openexchange.office.filter.odf.properties.NumberEra;
import com.openexchange.office.filter.odf.properties.NumberHours;
import com.openexchange.office.filter.odf.properties.NumberMinutes;
import com.openexchange.office.filter.odf.properties.NumberMonth;
import com.openexchange.office.filter.odf.properties.NumberQuarter;
import com.openexchange.office.filter.odf.properties.NumberSeconds;
import com.openexchange.office.filter.odf.properties.NumberText;
import com.openexchange.office.filter.odf.properties.NumberWeekOfYear;
import com.openexchange.office.filter.odf.properties.NumberYear;

final public class NumberDateStyle extends NumberStyleBase {

	public NumberDateStyle(String name, boolean automaticStyle, boolean contentStyle) {
		super(null, name, automaticStyle, contentStyle);
	}

	public NumberDateStyle(String name, AttributesImpl attributesImpl, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, false, automaticStyle, contentStyle);
	}

	@Override
	public String getQName() {
		return "number:date-style";
	}

	@Override
	public String getLocalName() {
		return "date-style";
	}

	@Override
	public String getNamespace() {
		return Namespaces.NUMBER;
	}

	@Override
	public String getFormat(StyleManager styleManager, Map<String, String> additionalStyleProperties, boolean contentAutoStyle) {
	    boolean setBrackets = !attributes.getBooleanValue("number:truncate-on-overflow", true);
	    if(additionalStyleProperties!=null) {
    	    final String autoOrder = getAttribute("number:automatic-order");
    	    if(autoOrder!=null&&!autoOrder.isEmpty()) {
    	        additionalStyleProperties.put("number:automatic-order", autoOrder);
    	    }
    	    final String sourceFormat = getAttribute("number:format-source");
    	    if(sourceFormat!=null&&!sourceFormat.isEmpty()) {
    	        additionalStyleProperties.put("number:format-source", sourceFormat);
    	    }
	    }
		String result = "";
		final Iterator<IElementWriter> propertiesIter = getContent().iterator();
		while(propertiesIter.hasNext()) {
			final IElementWriter properties = propertiesIter.next();
			if (properties instanceof NumberDay) {
				final String numberstyle = ((NumberDay)properties).getAttribute("number:style");
				if((numberstyle != null) && numberstyle.equals("long")) {
					result += "DD";
				}
				else {
					result += "D";
				}
			}
			else if (properties instanceof NumberMonth) {
				final String numberstyle = ((NumberMonth)properties).getAttribute("number:style");
				if(((NumberMonth)properties).getBoolean("number:textual", false)) {
					if ((numberstyle != null) && numberstyle.equals("long")) {
						result += "MMMM";
					} else {
						result += "MMM";
					}
				}
				else {
					if((numberstyle != null) && numberstyle.equals("long")) {
						result += "MM";
					}
					else {
						result += "M";
					}
				}
			}
			else if (properties instanceof NumberYear) {
				final String numberstyle = ((NumberYear)properties).getAttribute("number:style");
				if((numberstyle != null) && numberstyle.equals("long")) {
					result += "YYYY";
				}
				else {
					result += "YY";
				}
			}
			else if (properties instanceof NumberText) {
				final String content = ((NumberText)properties).getTextContent();
				if((content == null) || (content.equals(""))) {
					result += " ";
				}
				else {
					result += content;
				}
			}
			else if (properties instanceof NumberEra) {
				final String numberstyle = ((NumberEra)properties).getAttribute("number:style");
				if((numberstyle != null) && numberstyle.equals("long")) {
					result += "GG";
				}
				else {
					result += "G";
				}
			}
			else if (properties instanceof NumberHours) {
                final String numberstyle = ((NumberHours)properties).getAttribute("number:style");
                if(setBrackets)
                    result += "[";
                if ((numberstyle != null) && numberstyle.equals("long")) {
                    result += "hh";
                } else {
                    result += "h";
                }
                if( setBrackets)
                {
                    result += "]";
                    setBrackets = false;
                }
			}
			else if (properties instanceof NumberMinutes) {
                if(setBrackets) {
                    result += "[";
                }
                String numberstyle = ((NumberMinutes)properties).getAttribute("number:style");
                if ((numberstyle != null) && numberstyle.equals("long")) {
                    result += "mm";
                } else {
                    result += "m";
                }
                if( setBrackets)
                {
                    result += "]";
                    setBrackets = false;
                }
			}
			else if (properties instanceof NumberSeconds) {
                if(setBrackets) {
                    result += "[";
                }
                String numberstyle = ((NumberSeconds)properties).getAttribute("number:style");
                if ((numberstyle != null) && numberstyle.equals("long")) {
                    result += "ss";
                } else {
                    result += "s";
                }
                Integer decimals = ((NumberSeconds)properties).getInteger("number:decimal-places", null);
                if(decimals != null && decimals.intValue() > 0){
                    result += '.';
                    for( int i = 0; i < decimals.intValue(); i++){
                        result += '0';
                    }
                }
                if( setBrackets)
                {
                    result += "]";
                    setBrackets = false;
                }
			}
			else if (properties instanceof NumberQuarter) {
				final String numberstyle = ((NumberQuarter)properties).getAttribute("number:style");
				if((numberstyle != null) && numberstyle.equals("long")) {
					result += "QQ";
				}
				else {
					result += "Q";
				}
			}
			else if (properties instanceof NumberDayOfWeek) {
				final String numberstyle = ((NumberDayOfWeek)properties).getAttribute("number:style");
				if ((numberstyle != null) && numberstyle.equals("long")) {
					result += "NNN";
				}
				else {
					result += "NN";
				}
			}
			else if (properties instanceof NumberAmPm) {
				result += "AM/PM";
            }
			else if (properties instanceof NumberWeekOfYear) {
                result += "WW";
			}
		}
		return result;
	}

	@Override
    public void setFormat(FormatCode formatCode, Map<String, String> additionalStyleProperties) {
	    final List<Entry> entries = formatCode.getEntries();
        for(int i = 0; i < entries.size(); i++) {
            final Entry formatCodeEntry = entries.get(i);
            if(formatCodeEntry instanceof FormatCode.TextEntry) {
                appendNumberText(formatCodeEntry.toString());
            }
            else if(formatCodeEntry instanceof FormatCode.DecimalPointPlaceholder) {
                getContent().add(new NumberText(formatCodeEntry.toString()));
            }
            else if(formatCodeEntry instanceof FormatCode.AmPmPlaceholder) {
                getContent().add(new NumberAmPm(new AttributesImpl()));
            }
            else if(formatCodeEntry instanceof FormatCode.DateTimeEntry) {
                processDateTimeEntry(this, (FormatCode.DateTimeEntry)formatCodeEntry);
            }
            else if(formatCodeEntry instanceof FormatCode.SquareBracketEntry) {
                final FormatCode.SquareBracketEntry squareBracketEntry = (FormatCode.SquareBracketEntry)formatCodeEntry;
                if(squareBracketEntry.isTimeSymbol()) {
                    final FormatCode.DateTimeEntry dateTimeEntry = new FormatCode.DateTimeEntry(squareBracketEntry.getContent().charAt(0), squareBracketEntry.getContent().length());
                    dateTimeEntry.setIsElapsedTime(true);
                    processDateTimeEntry(this, dateTimeEntry);
                }
            }
        }
    }

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		//
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		// TODO Auto-generated method stub

	}
}
