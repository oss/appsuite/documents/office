/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;

public class StylesHandler extends SaxContextHandler {

    private final StyleManager styleManager;

    public StylesHandler(SaxContextHandler parentContext, StyleManager styleManager) {
    	super(parentContext);
    	this.styleManager = styleManager;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {

    	final AttributesImpl attributesImpl = new AttributesImpl(attributes);
    	String name = removeNameFromAttributes(attributesImpl);
    	if(name==null) {
    		// default styles does not have a name, we will check this...
    		if(qName.equals("style:default-style")) {
    			name = "_default";
    		}
    	}
    	if(name!=null) {
	    	switch(qName) {
	    		case "style:default-style" : {
	    			final SaxContextHandler styleHandler = StylesHandler.getStyleHandler(this, name, styleManager, attributesImpl, true, false, false);
	    			if(styleHandler!=null) {
	    				return styleHandler;
	    			}
	    			break;
	    		}
	    		case "style:style" : {
	    			final SaxContextHandler styleHandler = StylesHandler.getStyleHandler(this, name, styleManager, attributesImpl, false, false, false);
	    			if(styleHandler!=null) {
	    				return styleHandler;
	    			}
	    			break;
	    		}
	    		case "number:number-style": {
		    		return new NumberNumberStyleHandler(this, new NumberNumberStyle(name, attributesImpl, false, false), styleManager);
	    		}
	    		case "number:boolean-style": {
		    		return new NumberBooleanStyleHandler(this, new NumberBooleanStyle(name, attributesImpl, false, false), styleManager);
	    		}
	    		case "number:currency-style" : {
		    		return new NumberCurrencyStyleHandler(this, new NumberCurrencyStyle(name, attributesImpl, false, false), styleManager);
	    		}
	    		case "number:date-style" : {
		    		return new NumberDateStyleHandler(this, new NumberDateStyle(name, attributesImpl, false, false), styleManager);
	    		}
	    		case "number:percentage-style" : {
		    		return new NumberPercentageStyleHandler(this, new NumberPercentageStyle(name, attributesImpl, false, false), styleManager);
	    		}
	    		case "number:text-style" : {
		    		return new NumberTextStyleHandler(this, new NumberTextStyle(name, attributesImpl, false, false), styleManager);
	    		}
	    		case "number:time-style" : {
		    		return new NumberTimeStyleHandler(this, new NumberTimeStyle(name, attributesImpl, false, false), styleManager);
	    		}
	    		case "text:outline-style" : {
	    		    return new TextOutlineStyleHandler(this, new TextOutlineStyle(name, attributesImpl), styleManager);
	    		}
	    		case "text:list-style" : {
		    		return new TextListStyleHandler(this, new TextListStyle(name, attributesImpl, false, false), styleManager);
	    		}
	            case "table:table-template" : {
	                return new TableTemplateHandler(this, new TableTemplate(name, attributesImpl), styleManager);
	            }
                case "draw:gradient" : {
                    return new GradientHandler(this, new Gradient(name, attributesImpl), styleManager);
                }
                case "draw:fill-image" : {
                    return new DrawFillImageHandler(this, new DrawFillImage(name, attributesImpl), styleManager);
                }
                case "draw:stroke-dash" : {
                    return new StrokeDashStyleHandler(this, new StrokeDashStyle(name, attributesImpl), styleManager);
                }
	            case "draw:marker" : {
	                return new MarkerHandler(this, new Marker(name, attributesImpl), styleManager);
	            }
	    	}
    	}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	styleManager.getStyles().getContent().add(element);
		return new UnknownContentHandler(this, element);
    }

    public static String removeNameFromAttributes(AttributesImpl attributesImpl) {
    	AttributeImpl attributeImpl = attributesImpl.remove("style:name");
		if(attributeImpl==null) {
			// WTF is the style name for the "draw:stroke-dash" a "draw:name" and not a "style:name" as each other style is using
			attributeImpl = attributesImpl.remove("draw:name");
			if(attributeImpl==null) {
			    // the spec specifies a "table:name" but only "text:style-name" is used in LO :-(
			    attributeImpl = attributesImpl.remove("text:style-name");
			}
		}
		if(attributeImpl!=null) {
			final String name = attributeImpl.getValue();
			if(name!=null&&!name.isEmpty()) {
				return name;
			}
		}
		return null;
    }

    public static SaxContextHandler getStyleHandler(SaxContextHandler context, String name, StyleManager styleManager, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle, boolean contentStyle) {
    	final String family = attributesImpl.getValue("style:family");
		if(family==null) {
			return null;
		}
		switch(family) {
			case "text" : {
				return new StyleTextHandler(context, new StyleText(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "paragraph" : {
				return new StyleParagraphHandler(context, new StyleParagraph(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "section" : {
				return new StyleSectionHandler(context, new StyleSection(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "ruby" : {
				return new StyleRubyHandler(context, new StyleRuby(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "table" : {
				return new StyleTableHandler(context, new StyleTable(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "table-column" : {
				return new StyleTableColumnHandler(context, new StyleTableColumn(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "table-row" : {
				return new StyleTableRowHandler(context, new StyleTableRow(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "table-cell" : {
				return new StyleTableCellHandler(context, new StyleTableCell(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "graphic" : {
				return new StyleGraphicHandler(context, new StyleGraphic(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "presentation" : {
				return new StylePresentationHandler(context, new StylePresentation(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "drawing-page" : {
				return new StyleDrawingPageHandler(context, new StyleDrawingPage(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
			case "chart" : {
                return new StyleChartHandler(context, new StyleChart(name, attributesImpl, defaultStyle, automaticStyle, contentStyle), styleManager);
			}
		}
    	return null;
    }
}
