/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.ChartProperties;
import com.openexchange.office.filter.odf.properties.GraphicProperties;
import com.openexchange.office.filter.odf.properties.ParagraphProperties;
import com.openexchange.office.filter.odf.properties.TextProperties;

final public class StyleChart extends StyleBase implements IParagraphProperties, ITextProperties, IGraphicProperties {

    private ChartProperties     chartProperties     = new ChartProperties(new AttributesImpl());
    private ParagraphProperties paragraphProperties = new ParagraphProperties(new AttributesImpl());
    private TextProperties      textProperties      = new TextProperties(new AttributesImpl());
    private GraphicProperties   graphicProperties;

    public StyleChart(String name, boolean automaticStyle, boolean contentStyle) {
        super(StyleFamily.CHART, name, automaticStyle, contentStyle);

        graphicProperties = new GraphicProperties(new AttributesImpl(), this);
    }

    public StyleChart(String name, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle, boolean contentStyle) {
        super(name, attributesImpl, defaultStyle, automaticStyle, contentStyle);

        graphicProperties = new GraphicProperties(new AttributesImpl(), this);
    }

    @Override
    public StyleFamily getFamily() {
        return StyleFamily.CHART;
    }

    public ChartProperties getChartProperties() {
        return chartProperties;
    }

    @Override
    public GraphicProperties getGraphicProperties() {
        return graphicProperties;
    }

    @Override
    public ParagraphProperties getParagraphProperties() {
        return paragraphProperties;
    }

    @Override
    public TextProperties getTextProperties() {
        return textProperties;
    }

    @Override
    public void writeObject(SerializationHandler output)
        throws SAXException {

        SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
        writeAttributes(output);
        chartProperties.writeObject(output);
        graphicProperties.writeObject(output);
        paragraphProperties.writeObject(output);
        textProperties.writeObject(output);
        writeMapStyleList(output);
        SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
    }

    @Override
    public void mergeAttrs(StyleBase styleBase) {
        if(styleBase instanceof IParagraphProperties) {
            getParagraphProperties().mergeAttrs(((IParagraphProperties)styleBase).getParagraphProperties());
        }
        if(styleBase instanceof ITextProperties) {
            getTextProperties().mergeAttrs(((ITextProperties)styleBase).getTextProperties());
        }
        if(styleBase instanceof IGraphicProperties) {
            getGraphicProperties().mergeAttrs(((IGraphicProperties)styleBase).getGraphicProperties());
        }
        if(styleBase instanceof StyleChart) {
            getChartProperties().mergeAttrs(((StyleChart)styleBase).getChartProperties());
        }
    }

    @Override
    public void applyAttrs(StyleManager styleManager, JSONObject attrs)
        throws JSONException {

        styleManager.updateFrameStyleProperty(this);

        paragraphProperties.applyAttrs(styleManager, attrs);
        textProperties.applyAttrs(styleManager, attrs);
        graphicProperties.applyAttrs(styleManager, attrs);
        chartProperties.applyAttrs(styleManager, attrs);
    }

    @Override
    public void createAttrs(StyleManager styleManager, OpAttrs attrs) {

        styleManager.updateFrameStyleProperty(this);

        chartProperties.createAttrs(styleManager, isContentStyle(), attrs);
        graphicProperties.createAttrs(styleManager, isContentStyle(), attrs);
        paragraphProperties.createAttrs(styleManager, isContentStyle(), attrs);
        textProperties.createAttrs(styleManager, isContentStyle(), attrs);
    }

    @Override
    protected int _hashCode() {

        int result = 0;

        result += chartProperties.hashCode();
        result += graphicProperties.hashCode();
        result += paragraphProperties.hashCode();
        result += textProperties.hashCode();
        return result;
    }

    @Override
    protected boolean _equals(StyleBase e) {
        final StyleChart other = (StyleChart)e;
        if(!chartProperties.equals(other.chartProperties)) {
            return false;
        }
        if(!graphicProperties.equals(other.graphicProperties)) {
            return false;
        }
        if(!paragraphProperties.equals(other.paragraphProperties)) {
            return false;
        }
        if(!textProperties.equals(other.textProperties)) {
            return false;
        }
        return true;
    }

    @Override
    public StyleChart clone() {
        final StyleChart clone = (StyleChart)_clone();
        clone.chartProperties = chartProperties.clone();
        clone.graphicProperties = graphicProperties.clone();
        clone.paragraphProperties = paragraphProperties.clone();
        clone.textProperties = textProperties.clone();
        return clone;
    }
}
