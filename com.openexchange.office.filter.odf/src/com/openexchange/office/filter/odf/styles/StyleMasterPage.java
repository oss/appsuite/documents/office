/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;

final public class StyleMasterPage extends StyleBase implements INodeAccessor {

	private DLList<Object> content;
	private DLList<StyleHeaderFooter> styleHeaderFooters;

	public StyleMasterPage(String name, String pageLayoutName) {
		super(null, name, false, false);

		if(pageLayoutName!=null&&!pageLayoutName.isEmpty()) {
			attributes.setValue(Namespaces.STYLE, "page-layout-name", "style:page-layout-name", pageLayoutName);
		}
	}

	public StyleMasterPage(String name, AttributesImpl attributesImpl) {
		super(name, attributesImpl, false, false, false);
	}

	@Override
	public StyleFamily getFamily() {
		return StyleFamily.MASTER_PAGE;
	}

	@Override
	public String getQName() {
		return "style:master-page";
	}

	@Override
	public String getLocalName() {
		return "master-page";
	}

	public DLList<StyleHeaderFooter> getStyleHeaderFooters() {
		if(styleHeaderFooters==null) {
			styleHeaderFooters = new DLList<StyleHeaderFooter>();
		}
		return styleHeaderFooters;
	}

	public String getStyleName() {
		return attributes.getValue("draw:style-name");
	}

	@Override
    public String getDisplayName() {
		return attributes.getValue("style:display-name");
	}

	public String getNextStyleName() {
		return attributes.getValue("style:next-style-name");
	}

	public String getPageLayoutName() {
		return attributes.getValue("style:page-layout-name");
	}

	public void setPageLayoutName(String pageLayoutName) {
		attributes.setValue(Namespaces.STYLE, "page-layout-name", "style:page-layout-name", pageLayoutName);
	}

	@Override
	public boolean isMasterStyle() {
		return true;
	}

	@Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
		return content;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.STYLE, getLocalName(), getQName());
		writeAttributes(output);
		for(Object child:getContent()) {
			((IElementWriter)child).writeObject(output);
		}
		if(styleHeaderFooters!=null) {
			for(StyleHeaderFooter styleHeaderFooter:styleHeaderFooters) {
				styleHeaderFooter.writeObject(output);
			}
		}
		SaxContextHandler.endElement(output, Namespaces.STYLE, getLocalName(), getQName());
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		//
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected int _hashCode() {
		int hash = content==null||content.isEmpty() ? 0 : content.hashCode();
		hash += styleHeaderFooters==null||styleHeaderFooters.isEmpty() ? 0 : styleHeaderFooters.hashCode();
		return hash;
	}

	@Override
	protected boolean _equals(StyleBase e) {
		final StyleMasterPage other = (StyleMasterPage)e;
		if(content==null||content.isEmpty()) {
			if(other.content!=null&&!other.content.isEmpty()) {
				return false;
			}
		}
		else if(!content.equals(other.content)) {
			return false;
		}
		if(styleHeaderFooters==null||styleHeaderFooters.isEmpty()) {
			if(other.styleHeaderFooters!=null&&!other.styleHeaderFooters.isEmpty()) {
				return false;
			}
		}
		else if(!styleHeaderFooters.equals(other.styleHeaderFooters)) {
			return false;
		}
		return true;
	}

	@Override
	public StyleMasterPage clone() {
		return (StyleMasterPage)_clone();
	}
}
