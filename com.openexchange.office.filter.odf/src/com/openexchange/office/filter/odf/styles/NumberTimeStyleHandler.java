/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.properties.MapProperties;
import com.openexchange.office.filter.odf.properties.NumberAmPm;
import com.openexchange.office.filter.odf.properties.NumberHours;
import com.openexchange.office.filter.odf.properties.NumberMinutes;
import com.openexchange.office.filter.odf.properties.NumberSeconds;
import com.openexchange.office.filter.odf.properties.NumberText;
import com.openexchange.office.filter.odf.properties.StylePropertiesBaseHandler;
import com.openexchange.office.filter.odf.properties.TextProperties;

public class NumberTimeStyleHandler extends SaxContextHandler {

	final StyleManager styleManager;
	final NumberTimeStyle numberTimeStyle;

	public NumberTimeStyleHandler(SaxContextHandler parentContext, NumberTimeStyle numberTimeStyle, StyleManager styleManager) {
		super(parentContext);

		this.styleManager = styleManager;
		this.numberTimeStyle = numberTimeStyle;
	}

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	switch(qName) {
			case "number:am-pm": {
				final NumberAmPm numberAmPm = new NumberAmPm(new AttributesImpl(attributes));
				numberTimeStyle.getContent().add(numberAmPm);
				return new StylePropertiesBaseHandler(this, numberAmPm);
			}
			case "number:hours": {
				final NumberHours numberHours = new NumberHours(new AttributesImpl(attributes));
				numberTimeStyle.getContent().add(numberHours);
				return new StylePropertiesBaseHandler(this, numberHours);
			}
			case "number:minutes": {
				final NumberMinutes numberMinutes = new NumberMinutes(new AttributesImpl(attributes));
				numberTimeStyle.getContent().add(numberMinutes);
				return new StylePropertiesBaseHandler(this, numberMinutes);
			}
			case "number:seconds": {
				final NumberSeconds numberSeconds = new NumberSeconds(new AttributesImpl(attributes));
				numberTimeStyle.getContent().add(numberSeconds);
				return new StylePropertiesBaseHandler(this, numberSeconds);
			}
			case "number:text": {
				final NumberText numberText = new NumberText(new AttributesImpl(attributes));
				numberTimeStyle.getContent().add(numberText);
				return new StylePropertiesBaseHandler(this, numberText);
			}
			case "style:text-properties": {
				final TextProperties textProperties = new TextProperties(new AttributesImpl(attributes));
				numberTimeStyle.getContent().add(textProperties);
				return new StylePropertiesBaseHandler(this, textProperties);
			}
			case "style:map": {
				if(!numberTimeStyle.isDefaultStyle()) {
	    			final MapProperties mapProperties = new MapProperties(new AttributesImpl(attributes));
	    			numberTimeStyle.getMapStyleList().add(mapProperties);
	    			return new StylePropertiesBaseHandler(this, mapProperties);
				}
			}
    	}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	numberTimeStyle.getContent().add(element);
		return new UnknownContentHandler(this, element);
    }

    @Override
	public void endContext(String qName, String characters) {
		super.endContext(qName, characters);

		styleManager.addStyle(numberTimeStyle);
	}
}
