/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;

public class StyleMasterPageHandler extends SaxContextHandler {

	final StyleManager styleManager;
	final StyleMasterPage styleMasterPage;

	public StyleMasterPageHandler(SaxContextHandler parentContext, StyleMasterPage styleMasterPage, StyleManager styleManager) {
		super(parentContext);

		this.styleManager = styleManager;
		this.styleMasterPage = styleMasterPage;
	}

	@Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
		StyleHeaderFooter styleHeaderFooter = null;
		if(qName.equals("style:header")) {
			styleHeaderFooter = new StyleHeaderFooter("header_default_Standard", "header_default", attributes);
		}
		else if(qName.equals("style:header-left")) {
			styleHeaderFooter = new StyleHeaderFooter("header_even_Standard", "header_even", attributes);
		}
		else if(qName.equals("style:header-first")) {
			styleHeaderFooter = new StyleHeaderFooter("header_first_Standard", "header_first", attributes);
		}
		else if(qName.equals("style:footer")) {
			styleHeaderFooter = new StyleHeaderFooter("footer_default_Standard", "footer_default", attributes);
		}
		else if(qName.equals("style:footer-left")) {
			styleHeaderFooter = new StyleHeaderFooter("footer_even_Standard", "footer_even", attributes);
		}
		else if(qName.equals("style:footer-first")) {
			styleHeaderFooter = new StyleHeaderFooter("footer_first_Standard", "footer_first", attributes);
		}
		if(styleHeaderFooter!=null) {
			styleMasterPage.getStyleHeaderFooters().add(styleHeaderFooter);
			return new StyleHeaderFooterHandler(this, styleHeaderFooter);
		}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	styleMasterPage.getContent().add(element);
		return new UnknownContentHandler(this, element);
    }

    @Override
	public void endContext(String qName, String characters) {
		super.endContext(qName, characters);

		styleManager.addStyle(styleMasterPage);
	}
}
