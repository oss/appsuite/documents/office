/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Map;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.PropertyHelper;

final public class Gradient extends StyleBase {

	public Gradient(String name) {
		super(null, name, false, false);
	}

	public Gradient(String name, AttributesImpl attributesImpl) {
		super(name, attributesImpl, false, false, false);
	}

	@Override
    public StyleFamily getFamily() {
		return StyleFamily.GRADIENT;
	}

    @Override
    public String getQName() {
        return "draw:gradient";
    }

    @Override
    public String getLocalName() {
        return "gradient";
    }

    @Override
    public String getNamespace() {
        return Namespaces.DRAW;
    }

    @Override
    protected void writeNameAttribute(SerializationHandler output) throws SAXException {
        output.addAttribute(Namespaces.DRAW, "name", "draw:name", "", getName());
    }

    @Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

    @Override
    public void applyAttrs(StyleManager styleManager, JSONObject fillAttributes)
        throws JSONException {

        String gradientType = null;
        Object o = null;

        final JSONObject gradientAttributes = fillAttributes.optJSONObject(OCKey.GRADIENT.value());
        if(gradientAttributes!=null) {
            o = gradientAttributes.opt(OCKey.TYPE.value());
            if(o!=null) {
                if(o instanceof String) {
                    gradientType = "linear";

                    if("path".equals(o)) {
                        attributes.setValue(Namespaces.DRAW, "style", "draw:style", "path");
                        o = gradientAttributes.opt(OCKey.PATH_TYPE.value());
                        if(o!=null) {
                            if("circle".equals(o)) {
                                gradientType = "ellipsoid";
                            }
                            else if("rect".equals(o)) {
                                gradientType = "rectangular";
                            }
                            else if("shape".equals(o)) {
                                gradientType = "rectangular";
                            }
                        }
                    }
                    attributes.setValue(Namespaces.DRAW, "style", "draw:style", gradientType);
                }
                else {
                    attributes.remove("draw:style");
                }
            }
            o = gradientAttributes.opt(OCKey.ROTATION.value());
            if(o!=null) {
                final int angleDegrees = 270 - ((o instanceof Number) ? ((Number)o).intValue() : 0);
                attributes.setValue(Namespaces.DRAW, "angle", "draw:angle", Integer.valueOf(((angleDegrees < 0) ? (angleDegrees + 360) : angleDegrees) * 10).toString());
            }
            o = gradientAttributes.opt(OCKey.PATH_FILL_LEFT.value());
            if(o!=null) {
                if(o instanceof Number) {
                    attributes.setValue(Namespaces.DRAW, "cx", "draw:cx", Integer.valueOf(Math.min(Math.abs(Double.valueOf(((Number)o).doubleValue() * 100d).intValue()), 100)).toString() + "%");
                }
                else {
                    attributes.remove("draw:cx");
                }
            }
            o = gradientAttributes.opt(OCKey.PATH_FILL_TOP.value());
            if(o!=null) {
                if(o instanceof Number) {
                    attributes.setValue(Namespaces.DRAW, "cy", "draw:cy", Integer.valueOf(Math.min(Math.abs(Double.valueOf(((Number)o).doubleValue() * 100d).intValue()), 100)).toString() + "%");
                }
                else {
                    attributes.remove("draw:cy");
                }
            }
        }

        final boolean exchangeColors = "linear".equals(gradientType) || "radial".equals(gradientType) || "ellipsoid".equals(gradientType);
        final String color1AttrName = exchangeColors ? OCKey.COLOR2.value() : OCKey.COLOR.value();
        final String color2AttrName = exchangeColors ? OCKey.COLOR.value() : OCKey.COLOR2.value();

        o = fillAttributes.opt(color1AttrName);
        if(o!=null) {
            if(o instanceof JSONObject) {
                attributes.setValue(Namespaces.DRAW, "start-color", "draw:start-color", PropertyHelper.getColor((JSONObject)o, null));
            }
            else {
                attributes.remove("draw:start-color");
            }
        }

        o = fillAttributes.opt(color2AttrName);
        if(o!=null) {
            if(o instanceof JSONObject) {
                attributes.setValue(Namespaces.DRAW, "end-color", "draw:end-color", PropertyHelper.getColor((JSONObject)o, null));
            }
            else {
                attributes.remove("draw:end-color");
            }
        }

    }

    @Override
    public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
        final OpAttrs fillAttrs = attrs.getMap(OCKey.FILL.value(), true);
        final Integer angleObj = attributes.getIntValue("draw:angle");
        final int angle = (null == angleObj) ? 90 : ((360 - ((angleObj.intValue() / 10) + 90)) % 360);

        fillAttrs.getMap(OCKey.GRADIENT.value(), true).put(OCKey.ROTATION.value(), Integer.valueOf((angle < 0) ? (angle + 360) : angle));

        final String style = attributes.getValue("draw:style");
        if(style!=null) {
            String type = "linear";
            String pathType = null;
            if(style.equals("axial")) {
                type = "path";
            }
            else if(style.equals("ellipsoid")) {
                type = "path";
                pathType = "circle";
            }
            else if(style.equals("radial")) {
                type = "path";
                pathType = "circle";
            }
            else if(style.equals("rectangular")) {
                type = "path";
                pathType = "rect";
            }
            else if(style.equals("square")) {
                type = "path";
                pathType = "rect";
            }
            fillAttrs.getMap(OCKey.GRADIENT.value(), true).put(OCKey.TYPE.value(), type);
            if(pathType!=null) {
                fillAttrs.getMap(OCKey.GRADIENT.value(), true).put(OCKey.PATH_TYPE.value(), pathType);
            }
        }
        final String cx = attributes.getValue("draw:cx");
        if(cx!=null) {
            final double left = Math.max(Math.min(AttributesImpl.getPercentage(cx, 0).intValue(), 100), 0) / 100d;
            final OpAttrs gradientAttrs = fillAttrs.getMap(OCKey.GRADIENT.value(), true);
            gradientAttrs.put(OCKey.PATH_FILL_LEFT.value(), left);
            gradientAttrs.put(OCKey.PATH_FILL_RIGHT.value(), 1-left);
        }
        final String cy = attributes.getValue("draw:cy");
        if(cy!=null) {
            final double top = Math.max(Math.min(AttributesImpl.getPercentage(cy, 0).intValue(), 100), 0) / 100d;
            final OpAttrs gradientAttrs = fillAttrs.getMap(OCKey.GRADIENT.value(), true);
            gradientAttrs.put(OCKey.PATH_FILL_TOP.value(), top);
            gradientAttrs.put(OCKey.PATH_FILL_BOTTOM.value(), 1-top);
        }

        String startColor = attributes.getValue("draw:start-color");
        String endColor = attributes.getValue("draw:end-color");

        if(startColor==null) {
            startColor = "#000000";
        }

        if(endColor==null) {
            endColor = "#ffffff";
        }

        // Colors
        final boolean exchangeColors = "linear".equals(style) || "radial".equals(style) || "ellipsoid".equals(style);
        final Map<String, Object> colorMap1 = PropertyHelper.createColorMap(exchangeColors ? endColor : startColor);
        final Map<String, Object> colorMap2 = PropertyHelper.createColorMap(exchangeColors ? startColor : endColor);

        if(colorMap1!=null) {
            fillAttrs.put(OCKey.COLOR.value(), colorMap1);
        }

        if(colorMap2!=null) {
            fillAttrs.put(OCKey.COLOR2.value(), colorMap2);
        }
    }

    @Override
    public void mergeAttrs(StyleBase style) {
        //
    }

    @Override
    public Gradient clone() {
        return (Gradient)_clone();
    }

    @Override
    protected int _hashCode() {
        return 0;
    }

    @Override
    protected boolean _equals(StyleBase e) {
        return true;
    }

    public static String getGradient(StyleManager styleManager, String currentGradient, JSONObject gradientAttrs)
        throws JSONException {

        Gradient gradient = null;
        if(currentGradient!=null) {
            final StyleBase current = styleManager.getStyle(currentGradient, StyleFamily.GRADIENT, false);
            if(current!=null) {
                gradient = (Gradient)current.clone();
                gradient.setName(styleManager.getUniqueStyleName(StyleFamily.GRADIENT, false));
            }
        }
        if(gradient==null) {
            gradient = new Gradient(styleManager.getUniqueStyleName(StyleFamily.GRADIENT, false));
        }
        gradient.applyAttrs(styleManager, gradientAttrs);
        final String existingGradient = styleManager.getExistingStyleIdForStyleBase(gradient);
        if(existingGradient!=null) {
            return existingGradient;
        }
        styleManager.addStyle(gradient);
        return gradient.getName();
    }
}
