/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;

final public class TableTemplate extends StyleBase {

    private HashMap<String, AttributesImpl> templateMap = new HashMap<String, AttributesImpl>();

    public TableTemplate(String styleName, AttributesImpl attributesImpl) {
	    super(styleName, attributesImpl, false, false, false);
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
        output.addAttribute(Namespaces.TEXT, "style-name", "text:style-name", "", getName());
        attributes.write(output);
        final Iterator<Entry<String, AttributesImpl>> templateMapIter = templateMap.entrySet().iterator();
        while(templateMapIter.hasNext()) {
            final Entry<String, AttributesImpl> templateMapEntry = templateMapIter.next();
            SaxContextHandler.startElement(output, Namespaces.TABLE, templateMapEntry.getKey(), "table:" + templateMapEntry.getKey());
            templateMapEntry.getValue().write(output);
            SaxContextHandler.endElement(output, Namespaces.TABLE, templateMapEntry.getKey(), "table:" + templateMapEntry.getKey());
        }
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

	public HashMap<String, AttributesImpl> getTableTemplates() {
	    return templateMap;
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
	    //
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs)
	    throws JSONException {

	    final String parent;
	    if(attrs.has(OCKey.WHOLE_TABLE.value())) {
	        parent = OCKey.WHOLE_TABLE.value();
	        insertTableTemplateStyle(styleManager, OCKey.WHOLE_TABLE.value(), OCKey.BODY.value(), null, attrs);
	    }
	    else {
	        parent = null;
	    }
	    final Iterator<Entry<String, Object>> iter = attrs.entrySet().iterator();
	    while(iter.hasNext()) {
	        final Entry<String, Object> entry = iter.next();
	        final String conditionalStyle;
	        switch(OCKey.fromValue(entry.getKey())) {
                case FIRST_COL : {
                    conditionalStyle = "first-column";
                    break;
                }
                case LAST_COL : {
                    conditionalStyle = "last-column";
                    break;
                }
                case FIRST_ROW : {
                    conditionalStyle = "first-row";
                    break;
                }
                case LAST_ROW : {
                    conditionalStyle = "last-row";
                    break;
                }
                case BAND2_HORZ : {
                    conditionalStyle = "even-rows";
                    break;
                }
                case BAND1_HORZ : {
                    conditionalStyle = "odd-rows";
                    break;
                }
                case BAND2_VERT : {
                    conditionalStyle = "even-columns";
                    break;
                }
                case BAND1_VERT : {
                    conditionalStyle = "odd-columns";
                    break;
                }
                default: {
                    conditionalStyle = null;
                    break;
                }
	        }
	        if(conditionalStyle!=null) {
	            insertTableTemplateStyle(styleManager, entry.getKey(), conditionalStyle, parent, attrs);
	        }
	    }
	}

	private void insertTableTemplateStyle(StyleManager styleManager, String sourceConditionalStyle, String destConditionalStyle, String parentConditionalStyle, JSONObject attrs)
	    throws JSONException {

        final String cellStyleName = styleManager.getUniqueStyleName(StyleFamily.TABLE_CELL, false);
        addTableTemplateEntry(destConditionalStyle, cellStyleName);
        if(parentConditionalStyle!=null) {
            final StyleBase styleBase = styleManager.createStyleSheet("cell", cellStyleName, null, attrs.optJSONObject(parentConditionalStyle) , "default", false);
            styleBase.applyAttrs(styleManager, attrs.optJSONObject(sourceConditionalStyle));
            styleManager.addStyle(styleBase);
        }
        else {
            final JSONObject sourceAttrs = attrs.optJSONObject(sourceConditionalStyle);
            final StyleBase styleBase = styleManager.createStyleSheet("cell", cellStyleName, null, sourceAttrs, "default", false);
            if(sourceAttrs!=null) {
                final JSONObject cellAttrs = sourceAttrs.optJSONObject(OCKey.CELL.value());
                if(cellAttrs==null||!cellAttrs.has(OCKey.FILL_COLOR.value())) {
                    // applying default which is no fill
                    ((StyleTableCell)styleBase).getGraphicProperties().getAttributes().setValue(Namespaces.DRAW, "fill", "draw:fill", "none");
                }
            }
            styleManager.addStyle(styleBase);
        }
	}

	public void addTableTemplateEntry(String destConditionalStyle, String cellStyleName) {
	    AttributesImpl conditionalStyleAttributes = templateMap.get(destConditionalStyle);
        if(conditionalStyleAttributes==null) {
            conditionalStyleAttributes = new AttributesImpl();
            templateMap.put(destConditionalStyle, conditionalStyleAttributes);
        }
        conditionalStyleAttributes.setValue(Namespaces.TABLE, "style-name", "table:style-name", cellStyleName);
	}

    public static JSONObject createInsertStyleSheetOperation(StyleManager styleManager, String templateName, TableTemplate tableTemplate) throws JSONException {
        final JSONObject insertStylesheetObject = new JSONObject();
        insertStylesheetObject.put(OCKey.NAME.value(), OCValue.INSERT_STYLE_SHEET.value());
        insertStylesheetObject.put(OCKey.TYPE.value(), "table");
        insertStylesheetObject.put(OCKey.STYLE_ID.value(), templateName);
        insertStylesheetObject.put(OCKey.STYLE_NAME.value(), templateName);
        final JSONObject attrs = new JSONObject();
        final Iterator<Entry<String, AttributesImpl>> tableStyleEntryIter = tableTemplate.getTableTemplates().entrySet().iterator();
        while(tableStyleEntryIter.hasNext()) {
            String conditionalStyle = null;
            final Entry<String, AttributesImpl> tableStyleEntry = tableStyleEntryIter.next();
            switch(tableStyleEntry.getKey()) {
                case "first-column": {
                    conditionalStyle = OCKey.FIRST_COL.value();
                    break;
                }
                case "last-column": {
                    conditionalStyle = OCKey.LAST_COL.value();
                    break;
                }
                case "first-row": {
                    conditionalStyle = OCKey.FIRST_ROW.value();
                    break;
                }
                case "last-row": {
                    conditionalStyle = OCKey.LAST_ROW.value();
                    break;
                }
                case "even-rows": {
                    conditionalStyle = OCKey.BAND2_HORZ.value();
                    break;
                }
                case "odd-rows": {
                    conditionalStyle = OCKey.BAND1_HORZ.value();
                    break;
                }
                case "even-columns": {
                    conditionalStyle = OCKey.BAND2_VERT.value();
                    break;
                }
                case "odd-columns": {
                    conditionalStyle = OCKey.BAND1_VERT.value();
                    break;
                }
                case "body": {
                    conditionalStyle = OCKey.WHOLE_TABLE.value();
                    break;
                }
            }
            if(conditionalStyle!=null) {
                final String tableStyleName = tableStyleEntry.getValue().getValue("table:style-name");
                if(tableStyleName!=null&&!tableStyleName.isEmpty()) {
                    final OpAttrs cellAttrs = new OpAttrs();
                    styleManager.createPresentationTemplateStyle(cellAttrs, tableStyleName);
                    if(!cellAttrs.isEmpty()) {
                        attrs.put(conditionalStyle, cellAttrs);
                    }
                }
            }
        }
        if(!attrs.isEmpty()) {
            insertStylesheetObject.put(OCKey.ATTRS.value(), attrs);
        }
        insertStylesheetObject.put(OCKey.HIDDEN.value(), false);
        return insertStylesheetObject;
    }

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
	    //
	}

	@Override
	protected int _hashCode() {
	    int hash = 0;
        final Iterator<Entry<String, AttributesImpl>> templateMapIter = templateMap.entrySet().iterator();
        while(templateMapIter.hasNext()) {
            final Entry<String, AttributesImpl> templateMapEntry = templateMapIter.next();
            hash ^= templateMapEntry.getKey().hashCode();
            hash ^= templateMapEntry.getValue().hashCode();
        }
	    return hash;
	}

	@Override
	protected boolean _equals(StyleBase e) {
		final TableTemplate other = (TableTemplate)e;
		if(templateMap.size()!=other.templateMap.size()) {
		    return false;
		}
		final Iterator<Entry<String, AttributesImpl>> templateMapIter = templateMap.entrySet().iterator();
        while(templateMapIter.hasNext()) {
            final Entry<String, AttributesImpl> templateMapEntry = templateMapIter.next();
            if(!other.templateMap.containsKey(templateMapEntry.getKey())) {
                return false;
            }
            if(!templateMapEntry.getValue().equals(other.templateMap.get(templateMapEntry.getKey()))) {
                return false;
            }
        }
		return true;
	}

	@Override
	public TableTemplate clone() {
		final TableTemplate clone = (TableTemplate)_clone();
		clone.templateMap = new HashMap<String, AttributesImpl>();
        final Iterator<Entry<String, AttributesImpl>> templateMapIter = templateMap.entrySet().iterator();
        while(templateMapIter.hasNext()) {
            final Entry<String, AttributesImpl> templateMapEntry = templateMapIter.next();
            clone.templateMap.put(templateMapEntry.getKey(), templateMapEntry.getValue().clone());
        }
		return clone;
	}

	@Override
	public String getNamespace() {
        return Namespaces.TABLE;
    }

	@Override
    public String getQName() {
        return "table:table-template";
    }

	@Override
    public String getLocalName() {
        return "table-template";
    }

    @Override
    public StyleFamily getFamily() {
        return StyleFamily.TABLE_TEMPLATE;
    }
}
