/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.dom.OdfDocumentNamespace;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import com.google.common.collect.ImmutableSet;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.spreadsheet.FormatCode;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.IStartElementListener;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.listlevel.ListLevelStyleEntry;
import com.openexchange.office.filter.odf.properties.FontFace;
import com.openexchange.office.filter.odf.properties.GraphicProperties;
import com.openexchange.office.filter.odf.properties.MapProperties;
import com.openexchange.office.filter.odf.properties.ParagraphProperties;
import com.openexchange.office.filter.odf.properties.StylePropertiesBase;
import com.openexchange.office.filter.odf.properties.TextProperties;
import com.openexchange.office.imagemgr.IResourceManager;

public class StyleManager {

    // common styles (styles.xml)
	private Styles             styles;
	private final FamilyStyles familyStyles = new FamilyStyles();

    // master styles are a common styles that contains formatting information and additional content that is displayed with the document content when the style is applied (styles.xml only)
	private MasterStyles       masterStyles;
	private final FamilyStyles familyMasterStyles = new FamilyStyles();

    // automatic styles (content.xml)
	private AutomaticStyles    automaticStylesMaster;
	private final FamilyStyles familyAutomaticStylesMaster = new FamilyStyles();

    // automatic styles (styles.xml)
	private AutomaticStyles    automaticStylesContent;
	private final FamilyStyles familyAutomaticStylesContent = new FamilyStyles();

	private FontFaceDecls      fontFaceDeclsMaster;
	private FontFaceDecls      fontFaceDeclsContent;

	private static Random      random;
	private boolean            hasTabsRelativeToIndent = false;
	private IResourceManager   resourceManager = null;
	private OdfPackage         odfPackage = null;
    private final String       documentType;

    public StyleManager(String documentType, OdfPackage odfPackage) {
        this.documentType = documentType;
        this.odfPackage = odfPackage;
    }

	public String getDocumentType() {
        return documentType;
	}

	public void setResourceManager(IResourceManager resourceManager) {
	     this.resourceManager = resourceManager;
	}

    public Map<String, byte[]> getResourceMap() {
        if(resourceManager!=null) {
            return resourceManager.getByteArrayTable();
        }
        return null;
    }

    public OdfPackage getPackage() {
        return odfPackage;
    }

    /*
	 * The FamilyStyles class is a wrapper for a two key hash map, the
	 * first key is the family and the second key is the name of the style
	 */
	protected class FamilyStyles {

		private final HashMap<StyleFamily, HashMap<String, DLNode<Object>>> stylesByFamily = new HashMap<StyleFamily, HashMap<String, DLNode<Object>>>();

		// adds the style, family and name is taken from the style
		public void addStyle(DLNode<Object> styleBaseNode) {
			final StyleBase styleBase = (StyleBase)styleBaseNode.getData();
			HashMap<String, DLNode<Object>> familyMap = stylesByFamily.get(styleBase.getFamily());
			if(familyMap==null) {
				familyMap = new HashMap<String, DLNode<Object>>();
				stylesByFamily.put(styleBase.getFamily(), familyMap);
			}
			familyMap.put(styleBase.getName(), styleBaseNode);
		}

		public DLNode<Object> removeStyle(StyleFamily family, String name) {
			return stylesByFamily.get(family).remove(name);
		}

		public StyleBase getStyle(StyleFamily family, String name) {
			if(name==null||name.isEmpty()) {
				return null;
			}
			final HashMap<String, DLNode<Object>> styles = stylesByFamily.get(family);
			if(styles==null) {
				return null;
			}
			final DLNode<Object> styleBaseNode = styles.get(name);
			return styleBaseNode!=null ? (StyleBase)styleBaseNode.getData() : null;
		}

		public HashMap<String, DLNode<Object>> getStylesByFamily(StyleFamily family) {
			return stylesByFamily.get(family);
		}
	}

	private final HashMap<StyleBase, String> allStyles = new HashMap<StyleBase, String>();

    public void addStyle(StyleBase styleBase) {
        final DLNode<Object> styleBaseNode = new DLNode<Object>(styleBase);
        if(styleBase.isMasterStyle()) {
            masterStyles.getContent().addNode(styleBaseNode);
            familyMasterStyles.addStyle(styleBaseNode);
        }
        else if(styleBase.isAutoStyle()) {
            if(styleBase.isContentStyle()) {
                automaticStylesContent.getContent().addNode(styleBaseNode);
                familyAutomaticStylesContent.addStyle(styleBaseNode);
            }
            else {
                automaticStylesMaster.getContent().addNode(styleBaseNode);
                familyAutomaticStylesMaster.addStyle(styleBaseNode);
            }
        }
        else {
            styles.getContent().addNode(styleBaseNode);
            familyStyles.addStyle(styleBaseNode);
        }
        allStyles.put(styleBase, styleBase.getName());
    }

	public StyleBase removeStyle(StyleFamily family, String name, boolean autoStyle, boolean masterStyle) {
		DLNode<Object> styleBaseNode = null;
		if(autoStyle) {
			if(masterStyle) {
				styleBaseNode = familyAutomaticStylesMaster.removeStyle(family, name);
				if(styleBaseNode!=null) {
					automaticStylesMaster.getContent().removeNode(styleBaseNode);
				}
			}
			else {
				styleBaseNode = familyAutomaticStylesContent.removeStyle(family, name);
				if(styleBaseNode!=null) {
					automaticStylesContent.getContent().removeNode(styleBaseNode);
				}
			}
		}
		else if(masterStyle) {
			styleBaseNode = familyMasterStyles.removeStyle(family, name);
			if(styleBaseNode!=null) {
				masterStyles.getContent().removeNode(styleBaseNode);
			}
		}
		else {
			styleBaseNode = familyStyles.removeStyle(family, name);
			if(styleBaseNode!=null) {
				styles.getContent().removeNode(styleBaseNode);
			}
		}
		if(styleBaseNode==null) {
			return null;
		}
		final StyleBase styleBase = (StyleBase)styleBaseNode.getData();
        final String styleBaseName = styleBase.getName();
		final String allStyleName = allStyles.get(styleBase);
        if (styleBaseName.equals(allStyleName)) {
			allStyles.remove(styleBase);
		}
		return styleBase;
	}

	public StyleBase getAutoStyle(String name, StyleFamily family, boolean content) {
	    if(content) {
	        final StyleBase style = familyAutomaticStylesContent.getStyle(family, name);
	        if(style!=null) {
	            return style;
	        }
	    }
	    return familyAutomaticStylesMaster.getStyle(family, name);
	}

	public String getUniqueStyleName(StyleFamily family, boolean content) {
		int i = getRandom();
		while(true) {
			final String name = family.getName().charAt(0) + Integer.toString(i);
			if(getStyle(name, family, content)==null) {
				return name;
			}
			i++;
		}
	}

	public static void setRandom(long randomSeed) {
	    random = new Random(randomSeed);
	}

	public static int getRandom() {
		if(random==null) {
	        random = new Random();
		}
		return Math.abs(random.nextInt());
	}

	public StyleBase getStyle(String name, StyleFamily family, boolean content) {
		StyleBase style = getAutoStyle(name, family, content);
		if(style==null) {
			style = familyStyles.getStyle(family, name);
		}
		return style;
	}

	public Styles getStyles() {
		return styles;
	}

	public MasterStyles getMasterStyles() {
		return masterStyles;
	}

	public AutomaticStyles getAutomaticStyles(boolean content) {
		if(content) {
			return automaticStylesContent;
		}
		return automaticStylesMaster;
	}

	public HashMap<String, DLNode<Object>> getFamilyStyles(StyleFamily family) {
	    return familyStyles.getStylesByFamily(family);
	}

	public FontFaceDecls getFontFaceDecls(boolean content) {
		if(content) {
			return fontFaceDeclsContent;
		}
		return fontFaceDeclsMaster;
	}

    public StyleBase getStyleByDisplayName(String displayName, StyleFamily family, boolean content) {
        final FamilyStyles autofamilyStyles = content ? familyAutomaticStylesContent : familyAutomaticStylesMaster;
        final HashMap<String, DLNode<Object>> autoStylesByFamily = autofamilyStyles.getStylesByFamily(family);
        if(autoStylesByFamily!=null) {
            for (DLNode<Object> node : autoStylesByFamily.values()) {
                StyleBase style = (StyleBase) node.getData();
                if (displayName.equals(style.getDisplayName())) {
                    return style;
                }
            }
        }
        final HashMap<String, DLNode<Object>> stylesByFamily = familyStyles.getStylesByFamily(family);
        if(stylesByFamily!=null) {
            for (DLNode<Object> node : stylesByFamily.values()) {
                StyleBase style = (StyleBase) node.getData();
                if (displayName.equals(style.getDisplayName())) {
                    return style;
                }
            }
        }
        return null;
    }

	// ------------------------------------------------------------------------------------------------------------------
	// document type can be "text", "spreadsheet" or "presentation"
	public void createInsertStyleOperations(INodeAccessor nodeAccessor, String documentType, JSONArray operationsQueue, Set<String> createdStyles)
		throws JSONException {

		final Iterator<Object> styleIter = nodeAccessor.getContent().iterator();
		while(styleIter.hasNext()) {
			final Object o = styleIter.next();
			if(o instanceof StyleBase) {
				final StyleBase style = (StyleBase)o;
				switch(style.getFamily()) {
					case PARAGRAPH : {
						if(documentType.equals("text")||documentType.equals("presentation")) {
							_createInsertStyleOperation(style, "paragraph", false, createdStyles, operationsQueue);
						}
						break;
					}
					case TEXT : {
						if(documentType.equals("text")||documentType.equals("presentation")) {
							_createInsertStyleOperation(style, "character", false, createdStyles, operationsQueue);
						}
						break;
					}
					case GRAPHIC : {
						if(documentType.equals("text")||documentType.equals("presentation")) {
							_createInsertStyleOperation(style, "drawing", false, createdStyles, operationsQueue);
						}
						break;
					}
					case TABLE : {
						if(documentType.equals("text")) {
							_createInsertTableStyleOperation(style, createdStyles, operationsQueue);
						}
						break;
					}
					case TABLE_CELL : {
						if(documentType.equals("spreadsheet")) {
							_createInsertStyleOperation(style, "cell", true, createdStyles, operationsQueue);
						}
						break;
					}
					case LIST_STYLE : {
						if(documentType.equals("text")) {
		                    if(usedStyles.contains(style.getName())) {
		                        _createInsertStyleOperation(style, "list-style", false, createdStyles, operationsQueue);
		                    }
						}
						break;
					}
					case OUTLINE_STYLE : {
					    if(documentType.equals("text")) {
					        _createInsertStyleOperation(style, "list-style", false, createdStyles, operationsQueue);
					    }

					}
				}
			}
		}
	}

	public void createInsertAutoListStylesOperations(boolean content, JSONArray operationsQueue)
		throws JSONException {

	    final HashMap<String, DLNode<Object>> stylesByFamily = (content ? familyAutomaticStylesContent : familyAutomaticStylesMaster).getStylesByFamily(StyleFamily.LIST_STYLE);
		if(stylesByFamily!=null) {
			final Iterator<DLNode<Object>> styleIter = stylesByFamily.values().iterator();
			final HashSet<String> createdStyles = new HashSet<String>();
			while(styleIter.hasNext()) {
				final StyleBase style = (StyleBase)styleIter.next().getData();
                if(usedStyles.contains(style.getName())) {
    				if(style.getFamily()==StyleFamily.LIST_STYLE) {
    					_createInsertStyleOperation(style, "list-style", false, createdStyles, operationsQueue);
    				}
                }
			}
		}
	}

	public void createPresentationAttributes(StyleBase presentationStyle, String listStyleTypeKey, int maxLevel, boolean contentStyle, OpAttrs attrs) {
	    _createPresentationAttributes(presentationStyle, listStyleTypeKey, maxLevel, new HashSet<String>(), contentStyle, attrs);
	    for(int i=1; i<=maxLevel; i++) {
	        final OpAttrs listLevel = TextListStyle.getCreatePresentationListStyleTypeMapLevel(listStyleTypeKey, i, true, attrs);
	        if(listLevel!=null) {
	            final OpAttrs paragraphAttrs = listLevel.getMap(OCKey.PARAGRAPH.value(), false);
	            if(paragraphAttrs!=null) {
	                ListLevelStyleEntry.finalizeParagraphAttrs(paragraphAttrs);
	            }
	        }
	    }
	}

	private void _createPresentationAttributes(StyleBase presentationStyle, String listStyleTypeKey, int maxLevel, HashSet<String> createdStyles, boolean contentStyle, OpAttrs attrs) {
        final String styleIdentifier = presentationStyle.getFamily() + presentationStyle.getName() + presentationStyle.isAutoStyle() + "1";
        if(createdStyles.contains(styleIdentifier)) {
            return;
        }
        createdStyles.add(styleIdentifier);
        final StyleBase parentStyle = getStyle(presentationStyle.getParent(), presentationStyle.getFamily(), contentStyle);
        if(parentStyle!=null) {
            _createPresentationAttributes(parentStyle, listStyleTypeKey, maxLevel, createdStyles, contentStyle, attrs);
        }
        createPresentationListStyleAttributes(presentationStyle, listStyleTypeKey, false, maxLevel, attrs);
        OpAttrs currentListStyleTypeMapLevel = TextListStyle.getCreatePresentationListStyleTypeMapLevel(listStyleTypeKey, 1, true, attrs);
        createPresentationParagraphAttributes(presentationStyle, currentListStyleTypeMapLevel, 1, false);
        final String presentationStyleName = presentationStyle.getName();
        if(presentationStyleName!=null&&!presentationStyleName.isEmpty()&&presentationStyleName.charAt(presentationStyleName.length()-1)=='1') {
            for(int level = 2; level <= maxLevel; level++) {
                final StyleBase styleBase = getStyle(presentationStyleName.substring(0, presentationStyleName.length()-1) + Integer.valueOf(level).toString(), StyleFamily.PRESENTATION, contentStyle);
                if(styleBase instanceof StylePresentation) {
                    OpAttrs newListStyleTypeMapLevel = TextListStyle.getCreatePresentationListStyleTypeMapLevel(listStyleTypeKey, level, true, attrs);
                    StyleManager.deepMerge(currentListStyleTypeMapLevel, newListStyleTypeMapLevel);
                    createPresentationParagraphAttributes(styleBase, newListStyleTypeMapLevel, level, false);
                    currentListStyleTypeMapLevel = newListStyleTypeMapLevel;
                }
            }
        }
    }

    private void createPresentationParagraphAttributes(StyleBase presentationStyle, OpAttrs levelAttrs, int level, boolean contentStyle) {
        if(presentationStyle instanceof IParagraphProperties) {
            ((IParagraphProperties)presentationStyle).getParagraphProperties().createAttrs(this, contentStyle, levelAttrs);
        }
        if(presentationStyle instanceof ITextProperties) {
            ((ITextProperties)presentationStyle).getTextProperties().createAttrs(this, contentStyle, levelAttrs);
        }
    }

    /*
     * maxLevel index starts from 1
     */
    private void createPresentationListStyleAttributes(StyleBase presentationStyle, String listStyleType, boolean contentStyle, int maxLevel, OpAttrs attrs) {
	    if(presentationStyle instanceof IGraphicProperties) {
    	    final GraphicProperties graphicProperties = ((IGraphicProperties)presentationStyle).getGraphicProperties();
    	    final TextListStyle textListStyle = graphicProperties.getTextListStyle();
    	    if(textListStyle!=null) {
    	        textListStyle.createPresentationListStyleDefinition(this, listStyleType, maxLevel, attrs);
    	    }
	    }
	}

	private void _createInsertStyleOperation(StyleBase style, String destType, boolean isDefaultType, Set<String> createdStyles, JSONArray operationsQueue)
		throws JSONException {

		// not creating styles twice... parents are inserted first
		final String styleIdentifier = style.getFamily() + style.getName() + style.isAutoStyle();
		if(createdStyles.contains(styleIdentifier)) {
			return;
		}
		createdStyles.add(styleIdentifier);

		final StyleBase parentStyle = familyStyles.getStyle(style.getFamily(), style.getParent());
		if(parentStyle!=null) {
			_createInsertStyleOperation(parentStyle, destType, isDefaultType, createdStyles, operationsQueue);
		}
		// ongoing with style ...
		final JSONObject op = new JSONObject();
		if(destType.equals("list-style")) {
			op.put(OCKey.NAME.value(), OCValue.INSERT_LIST_STYLE.value());
			op.put(OCKey.LIST_STYLE_ID.value(), style.getName());
		}
		else {
			if(style.isAutoStyle()) {
				op.put(OCKey.NAME.value(), OCValue.INSERT_AUTO_STYLE.value());
			}
			else {
				op.put(OCKey.NAME.value(), OCValue.INSERT_STYLE_SHEET.value());
			}
			if(!isDefaultType) {
			    op.put(OCKey.TYPE.value(), destType);
			}
			op.put(OCKey.STYLE_ID.value(), style.getName());

			if (style.getName() == "_default") {
                op.put(OCKey.HIDDEN.value(), true);
            }
		}
		final String displayName = style.getDisplayName();
		if(displayName!=null&&!displayName.isEmpty()) {
			op.put(OCKey.STYLE_NAME.value(), displayName);
		}
		if (style.isDefaultStyle()) {
            op.put(OCKey.DEFAULT.value(), style.isDefaultStyle());
        }
		final OpAttrs attrs = new OpAttrs();
		style.createAttrs(this, attrs);
        if(parentStyle!=null) {
        	if(style.isAutoStyle()) {
        		attrs.put(OCKey.STYLE_ID.value(), parentStyle.getName());
        	}
        	else {
        		op.put(OCKey.PARENT.value(), parentStyle.getName());
        	}
        }
        else if(!style.isDefaultStyle()) {
        	op.put(OCKey.PARENT.value(), "_default");
        }
		if(!attrs.isEmpty()) {
			if(destType.equals("list-style")) {
				op.put(OCKey.LIST_DEFINITION.value(), attrs);
			}
			else {
				op.put(OCKey.ATTRS.value(), attrs);
			}
		}
        operationsQueue.put(op);
	}

    private void _createInsertTableStyleOperation(StyleBase style, Set<String> createdStyles, JSONArray operationsQueue)
        throws JSONException {

        // not creating styles twice... parents are inserted first
        final String styleIdentifier = style.getFamily() + style.getName() + style.isAutoStyle();
        if(createdStyles.contains(styleIdentifier)) {
            return;
        }
        createdStyles.add(styleIdentifier);

        final StyleBase parentStyle = familyStyles.getStyle(style.getFamily(), style.getParent());
        if(parentStyle!=null) {
            _createInsertTableStyleOperation(parentStyle, createdStyles, operationsQueue);
        }
        // ongoing with style ...
        final JSONObject op = new JSONObject();
        if(style.isAutoStyle()) {
            op.put(OCKey.NAME.value(), OCValue.INSERT_AUTO_STYLE.value());
        }
        else {
            op.put(OCKey.NAME.value(), OCValue.INSERT_STYLE_SHEET.value());
        }
        op.put(OCKey.TYPE.value(), "table");
        op.put(OCKey.STYLE_ID.value(), style.getName());

        if (style.getName() == "_default") {
            op.put(OCKey.HIDDEN.value(), true);
        }
        final String displayName = style.getDisplayName();
        if(displayName!=null&&!displayName.isEmpty()) {
            op.put(OCKey.STYLE_NAME.value(), displayName);
        }
        if (style.isDefaultStyle()) {
            op.put(OCKey.DEFAULT.value(), style.isDefaultStyle());
        }
        final OpAttrs attrs = new OpAttrs();
        style.createAttrs(this, attrs);
        final String cellStyleName = getCellStyleFromTableStyle(style.getName(), style.isContentStyle(), false);
        if(cellStyleName!=null&&!cellStyleName.isEmpty()) {
            final StyleBase cellStyle = familyStyles.getStyle(StyleFamily.TABLE_CELL, cellStyleName);
            if(cellStyle!=null) {
                cellStyle.createAttrs(this, attrs);
            }
        }
        if(parentStyle!=null) {
            if(style.isAutoStyle()) {
                attrs.put(OCKey.STYLE_ID.value(), parentStyle.getName());
            }
            else {
                op.put(OCKey.PARENT.value(), parentStyle.getName());
            }
        }
        else if(!style.isDefaultStyle()) {
            op.put(OCKey.PARENT.value(), "_default");
        }
        if(!attrs.isEmpty()) {
            final OpAttrs a = new OpAttrs(1);
            a.put(OCKey.WHOLE_TABLE.value(), attrs);
            op.put(OCKey.ATTRS.value(), a);
        }
        operationsQueue.put(op);
    }

	private static StyleFamily styleTypeToFamily(String styleType) {
		switch(styleType) {
			case "page" 	     : return StyleFamily.PAGE_LAYOUT;    // text style
			case "drawing" 	     : return StyleFamily.GRAPHIC;        // text style
			case "character"     : return StyleFamily.TEXT;           // text style
			case "paragraph"     : return StyleFamily.PARAGRAPH;      // text style
			case "table"	     : return StyleFamily.TABLE;          // text style
			case "cell"		     : return StyleFamily.TABLE_CELL;     // spreadsheet style
			case "table-template": return StyleFamily.TABLE_TEMPLATE; // presentation style
			case "presentation"  : return StyleFamily.PRESENTATION;
			default: throw new RuntimeException("odf: unknown style type used");
		}
	}

	public void insertStyleSheet(String styleType, String styleId, String styleName, JSONObject attrs, String parent, boolean isDefaultStyle)
		throws JSONException {

	    addStyle(createStyleSheet(styleType, styleId, styleName, attrs, parent, isDefaultStyle));
	}

	public void changeStyleSheet(String styleType, String styleId, String displayName, JSONObject attrs, String parent)
	    throws JSONException {

	    final StyleBase styleBase = getStyle(styleId, styleTypeToFamily(styleType), false);
	    if(displayName!=null) {
	        styleBase.setDisplayName(displayName);
	    }
	    if(parent!=null) {
	        styleBase.setParent(parent);
	    }
	    if(attrs!=null) {
	        styleBase.applyAttrs(this, attrs);
	    }
	}

	public StyleBase createStyleSheet(String styleType, String styleId, String styleName, JSONObject attrs, String parent, boolean isDefaultStyle)
        throws JSONException {

        final StyleBase styleBase = createStyleBase(styleTypeToFamily(styleType), styleId, false, false);
        styleBase.setIsDefaultStyle(isDefaultStyle);
        if(styleName!=null&&!styleName.isEmpty()) {
            styleBase.setDisplayName(styleName);
        }
        if(parent!=null&&!parent.isEmpty()&&!parent.equals("_default")) {
            styleBase.setParent(parent);
        }
        if(attrs!=null) {
            styleBase.applyAttrs(this, attrs);
        }
        return styleBase;
    }

    public void insertAutoStyle(String styleType, String styleId, JSONObject attrs, boolean isDefaultStyle)
    	throws JSONException {

		final StyleBase styleBase = createStyleBase(styleTypeToFamily(styleType), styleId, true, true);
		styleBase.setIsDefaultStyle(isDefaultStyle);
		styleBase.applyAttrs(this, attrs);

        final String parent = attrs != null ? attrs.optString(OCKey.STYLE_ID.value()) : null;
        if (parent != null && !parent.isEmpty() && !parent.equals("_default")) {
            styleBase.setParent(parent);
        }
		addStyle(styleBase);
    }

    public void deleteAutoStyle(String styleType, String name) {
    	removeStyle(styleTypeToFamily(styleType), name, true, false);
    }

    public void changeAutoStyle(String type, String styleId, JSONObject attrs)
    	throws JSONException {

    	deleteAutoStyle(type, styleId);
    	insertAutoStyle(type, styleId, attrs, false);
    }

    /* adds the style to the manager and returns its id, ids of identical styles might be returned. given attrs are applyied before the styles is inserted */
    public String createStyle(StyleFamily family, String styleId, boolean contentAutoStyle, JSONObject attrs)
		throws JSONException {

        final StyleBase styleBaseClone = getStyleBaseClone(family, styleId, contentAutoStyle);
        applyAttrs(styleBaseClone, contentAutoStyle, attrs);
        return getStyleIdForStyleBase(styleBaseClone);
	}

    /* adds the style to the manager and returns its id, ids of identical styles might be returned */
    public String getStyleIdForStyleBase(StyleBase styleBase) {
        final String existingStyleId = getExistingStyleIdForStyleBase(styleBase);
        if(existingStyleId!=null) {
            return existingStyleId;
        }
        /* TODO: isEmpty implementieren
        if(currentStyleBase.isEmpty()) {
            return currentStyleBase.getParent();
        }
        */
        this.addStyle(styleBase);
        return styleBase.getName();
    }

    /* returns a style clone of the given parameters, the style is not inserted */
    public StyleBase getStyleBaseClone(StyleFamily family, String styleId, boolean contentAutoStyle) {
        if(styleId!=null&&!styleId.isEmpty()) {
            final StyleBase originalStyleBase = getStyle(styleId, family, contentAutoStyle);
            if(originalStyleBase!=null) {
                final StyleBase clonedStyle = originalStyleBase.clone();
                clonedStyle.setName(getUniqueStyleName(family, contentAutoStyle));
                return clonedStyle;
            }
        }
        return createStyleBase(family, getUniqueStyleName(family, contentAutoStyle), true, contentAutoStyle);
    }

    public void applyAttrs(StyleBase styleBase, boolean contentAutoStyle, JSONObject attrs)
        throws JSONException {

        // if a new style is applied, then we need to merge each parent auto style first
        final Object oStyleId = attrs.opt(OCKey.STYLE_ID.value());
        if(oStyleId!=null) {
            final List<StyleBase> parentAutoStyles = new ArrayList<StyleBase>();
            StyleBase p = styleBase;
            do {
                final StyleBase parentStyle = findParentAutoStyle(p, parentAutoStyles);
                if(parentStyle!=null) {
                    parentAutoStyles.add(parentStyle);
                }
                p = parentStyle;
            }
            while(p!=null);

            for(int i=0; i<parentAutoStyles.size(); i++) {
                styleBase.mergeAttrs(parentAutoStyles.get(i));
            }
            if(oStyleId instanceof String) {
                styleBase.setParent((String)oStyleId);
            }
            else if (oStyleId==JSONObject.NULL) {
                styleBase.setParent(null);
            }
        }
        styleBase.setIsAutoStyle(true);
        styleBase.setIsContentStyle(contentAutoStyle);

        styleBase.applyAttrs(this, attrs);
    }

	public enum AttributeStyleFilter {
		ALL,
		TEXT,
		PARAGRAPH,
		GRAPHIC
	}

	// searches for qNames in the given style and its parents, the first attribute that was found is returned.
	public HashMap<String, AttributeImpl> getXmlAttributes(HashSet<String> _qNames, String styleId, StyleFamily family, boolean contentStyle, AttributeStyleFilter styleFilter) {
	    final HashSet<String> qNames = new HashSet<String>(_qNames);
		final HashMap<String, AttributeImpl> retValue = new HashMap<String, AttributeImpl>(qNames.size());
	    if(styleId!=null&&!styleId.isEmpty()) {
			StyleBase style = getAutoStyle(styleId, family, contentStyle);
			if(style!=null) {
				final Stack<StyleBase> styleStack = new Stack<StyleBase>();
				while(style!=null) {
					StylePropertiesBase propertySet = null;
					if(styleFilter == AttributeStyleFilter.TEXT && style instanceof ITextProperties) {
						propertySet = ((ITextProperties)style).getTextProperties();
					}
					else if(styleFilter == AttributeStyleFilter.PARAGRAPH && style instanceof IParagraphProperties) {
						propertySet = ((IParagraphProperties)style).getParagraphProperties();
					}
					else if(styleFilter == AttributeStyleFilter.GRAPHIC && style instanceof IGraphicProperties) {
						propertySet = ((IGraphicProperties)style).getGraphicProperties();
					}
					if(propertySet!=null) {
					    final Map<String, AttributeImpl> attributes = propertySet.getAttributes().getUnmodifiableMap();					    
					    final Iterator<String> qNameIter = qNames.iterator();
					    while(qNameIter.hasNext()) {
					        final String qName = qNameIter.next();
	                        final AttributeImpl a = attributes.get(qName);
	                        if(a!=null) {
	                            retValue.put(qName, a);
	                            qNameIter.remove();
	                        }
					    }
					    if(qNames.isEmpty()) {
					        break;
					    }
					}
					styleStack.push(style);
				    style = findParentStyle(style, styleStack);
				}
			}
		}
		return retValue;
    }

	public class AttributeCache {

    	private final boolean useContentCache;
        private final HashMap<String, HashMap<String, Object>> masterAttributes;
        private final HashMap<String, HashMap<String, Object>> contentAttributes;

        private AttributeCache(boolean _useContentCache) {
            useContentCache = _useContentCache;
            masterAttributes = new HashMap<String, HashMap<String, Object>>();
            contentAttributes = new HashMap<String, HashMap<String, Object>>();
        }

        public void createAttributes(StyleManager styleManager, OpAttrs destAttrs, String styleId, StyleFamily family, boolean contentStyle, AttributeStyleFilter styleFilter, boolean addStyleId, boolean addDefaultStyle, boolean autoStylesOnly) {
    		if(styleId!=null&&!styleId.isEmpty()) {
    			StyleBase style = useContentCache ? getAutoStyle(styleId, family, contentStyle) : getStyle(styleId, family, contentStyle);
    			if(style!=null) {
    	            final HashMap<String, HashMap<String, Object>> cachedAutoStyles = useContentCache && contentStyle ? contentAttributes : masterAttributes;
    		        final OpAttrs attrs = new OpAttrs();
    				final Stack<StyleBase> styleStack = new Stack<StyleBase>();
    				HashMap<String, Object> cachedStyle = null;
    				while(style!=null) {
    					// only adding autoStyles to the stack which are not already cached
    					cachedStyle = cachedAutoStyles.get(style.getName());
    					if(cachedStyle!=null) {
    						break;
    					}
    					styleStack.push(style);
    					if(!style.isDefaultStyle()) {
    	                    style = autoStylesOnly ? findParentAutoStyle(style, styleStack) : findParentStyle(style, styleStack);
    					    if(addDefaultStyle && style == null) {
    					        style = getStyle("_default", family, false);
    					    }
    					}
    					else {
    					    style = null;
    					}
    				}
    				if(cachedStyle!=null) {
    					deepCopy(cachedStyle, attrs);	// we have cached attributes (containing also the correct style)
    				}
    				else if(addStyleId) {
    					// the last one in the stack has the correct style
    					styleId = styleStack.peek().getParent();
    					if(styleId != null && !styleId.isEmpty()) {
    						attrs.put(OCKey.STYLE_ID.value(), styleId);
    					}
    				}
    				while(!styleStack.isEmpty()) {	// copy attributes from styles that not have been cached already
    					style = styleStack.pop();
    					cachedStyle = null;
    					if(styleFilter == AttributeStyleFilter.ALL) {
    						style.createAttrs(styleManager, attrs);
    						cachedStyle = new HashMap<String, Object>();
    					}
    					else if(styleFilter == AttributeStyleFilter.TEXT && style instanceof ITextProperties) {
    						if(style instanceof ITextProperties) {
    							final TextProperties TextProperties = ((ITextProperties)style).getTextProperties();
    							TextProperties.createAttrs(styleManager, contentStyle, attrs);
    							cachedStyle = new HashMap<String, Object>();
    						}
    					}
    					else if(styleFilter == AttributeStyleFilter.PARAGRAPH && style instanceof IParagraphProperties) {
    						if(style instanceof IParagraphProperties) {
    							final ParagraphProperties paragraphProperties = ((IParagraphProperties)style).getParagraphProperties();
    							paragraphProperties.createAttrs(styleManager, contentStyle, attrs);
    							cachedStyle = new HashMap<String, Object>();
    						}
    					}
    					else if(styleFilter == AttributeStyleFilter.GRAPHIC && style instanceof IGraphicProperties) {
    						if(style instanceof IGraphicProperties) {
    							final GraphicProperties graphicProperties = ((IGraphicProperties)style).getGraphicProperties();
    							graphicProperties.createAttrs(styleManager, contentStyle, attrs);
    							cachedStyle = new HashMap<String, Object>();
    						}
    					}
    					if(cachedStyle != null) {
	    					deepCopy(attrs, cachedStyle);
	    					cachedAutoStyles.put(style.getName(), cachedStyle);
    					}
    				}
    	            deepCopy(attrs, destAttrs);
    			}
    			else if(addStyleId) {	// this must be the correct style because there exists no style with this id
    			    destAttrs.put(OCKey.STYLE_ID.value(), styleId);
    			}
    		}
        }
    }

    private final AttributeCache attributeCacheAutoStyles = new AttributeCache(true);

    public void createAutoStyleAttributes(OpAttrs destAttrs, String styleId, StyleFamily family, boolean contentStyle) {
		attributeCacheAutoStyles.createAttributes(this, destAttrs, styleId, family, contentStyle, AttributeStyleFilter.ALL, true, false, true);
	}

    private final AttributeCache attributeCacheParagraphStyles = new AttributeCache(true);

	public void collectAllParagraphPropertiesFromStyle(OpAttrs destAttrs, String styleId, StyleFamily family, boolean contentStyle) {
		attributeCacheParagraphStyles.createAttributes(this, destAttrs, styleId, family, contentStyle, AttributeStyleFilter.PARAGRAPH, false, false, false);
	}

    private final AttributeCache attributeCacheTextStyles = new AttributeCache(true);

	public void collectAllTextPropertiesFromStyle(OpAttrs destAttrs, String styleId, StyleFamily family, boolean contentStyle) {
		attributeCacheTextStyles.createAttributes(this, destAttrs, styleId, family, contentStyle, AttributeStyleFilter.TEXT, false, false, false);
	}

    private final AttributeCache attributeCacheGraphicStyles = new AttributeCache(true);

	public void collectAllGraphicPropertiesFromStyle(OpAttrs destAttrs, String styleId, StyleFamily family, boolean contentStyle, boolean applyDefaultStyle) {
		attributeCacheGraphicStyles.createAttributes(this, destAttrs, styleId, family, contentStyle, AttributeStyleFilter.GRAPHIC, false, applyDefaultStyle, false);
	}

    private final AttributeCache attributeCacheTemplateStyles = new AttributeCache(false);

    public void createPresentationTemplateStyle(OpAttrs destAttrs, String styleId) {
    	attributeCacheTemplateStyles.createAttributes(this, destAttrs, styleId, StyleFamily.TABLE_CELL, false, AttributeStyleFilter.ALL, false, false, false);
    }

    private final AttributeCache attributeCachePresentationStyles = new AttributeCache(false);

    public void createPresentationStyle(OpAttrs destAttrs, String styleId, boolean isContentStyle) {
    	attributeCachePresentationStyles.createAttributes(this, destAttrs, styleId, StyleFamily.PRESENTATION, isContentStyle, AttributeStyleFilter.ALL, false, false, false);
    }

    public String applyDataStyle(String formatCode, Map<String, String> additionalStyleProperties, int id, boolean volatileStyle, boolean automaticStyle, boolean contentStyle) {

        // use language dependent date format (id 14 is special)
        if(id==14) {
            final NumberDateStyle numberDateStyle = new NumberDateStyle(getUniqueStyleName(StyleFamily.DATA_STYLE, contentStyle), automaticStyle, contentStyle);
            if(volatileStyle) {
                numberDateStyle.setIsVolatile(true);
            }
            numberDateStyle.getAttributes().setValue(Namespaces.NUMBER, "automatic-order", "number:automatic-order", "true");
            numberDateStyle.getAttributes().setValue(Namespaces.NUMBER, "format-source", "number:format-source", "language");
            numberDateStyle.setFormat(new FormatCode("MM/DD/YYYY"), additionalStyleProperties);
            return getStyleIdForStyleBase(numberDateStyle);
        }

        if(formatCode==null||formatCode.isEmpty()) {
            switch(id) {
                case 1:  formatCode = "0"; break;
                case 2:  formatCode = "0.00"; break;
                case 3:  formatCode = "#,##0"; break;
                case 4:  formatCode = "#,##0.00"; break;
                case 5:  formatCode = "#,##0[$$-409]"; break;
                case 6:  formatCode = "#,##0[$$-409];[RED]-#,##0[$$-409]"; break;
                case 7:  formatCode = "#,##0.00[$$-409]"; break;
                case 8:  formatCode = "#,##0.00[$$-409];[RED]-#,##0.00[$$-409]"; break;
                case 9:  formatCode = "0%"; break;
                case 10: formatCode = "0.00%"; break;
                case 11: formatCode = "0.00E+00"; break;
                case 12: formatCode = "# ?/?"; break;
                case 13: formatCode = "# ??/??"; break;
                case 14:  formatCode = "MM/DD/YYYY"; break;
                case 15:  formatCode = "D-MMM-YY"; break;
                case 16:  formatCode = "D-MMM"; break;
                case 17:  formatCode = "MMM-YY"; break;
                case 18:  formatCode = "H:MM AM/PM"; break;
                case 19:  formatCode = "H:MM:SS AM/PM"; break;
                case 20:  formatCode = "H:MM"; break;
                case 21:  formatCode = "H:MM:SS"; break;
                case 22:  formatCode = "M/D/YYYY H:MM"; break;
                case 23:  // PASSTHROUGH INTENTED !!
                case 24:  // PASSTHROUGH INTENTED !!
                case 25:  // PASSTHROUGH INTENTED !!
                case 26:  formatCode = "0"; break;
                case 27:  // PASSTHROUGH INTENTED !!
                case 28:  // PASSTHROUGH INTENTED !!
                case 29:  // PASSTHROUGH INTENTED !!
                case 30:  // PASSTHROUGH INTENTED !!
                case 31:  formatCode = "M/D/YYYY"; break;
                case 32:  // PASSTHROUGH INTENTED !!
                case 33:  // PASSTHROUGH INTENTED !!
                case 34:  // PASSTHROUGH INTENTED !!
                case 35:  formatCode = "HH:MM:SS"; break;
                case 36:  formatCode = "M/D/YYYY"; break;
                case 37:  formatCode = "#,##0_);(#,##0)";  break;
                case 38:  formatCode = "#,##0_);[RED](#,##0)"; break;
                case 39:  formatCode = "#,##0.00_);(#,##0.00)"; break;
                case 40:  formatCode = "#,##0.00_);[RED](#,##0.00)"; break;
                case 41:  formatCode = "_-* #,##0 _\u20ac_-;-* #,##0 _\u20ac_-;_-* \"-\" _\u20ac_-;_-@_-"; break;
                case 42:  formatCode = "_-* #,##0 \"\u20ac\"_-;-* #,##0 \"\u20ac\"_-;_-* \"-\" \"\u20ac\"_-;_-@_-"; break;
                case 43:  formatCode = "_-* #,##0.00 _\u20ac_-;-* #,##0.00 _\u20ac_-;_-* \"-\"?? _\u20ac_-;_-@_-"; break;
                case 44:  formatCode = "_-* #,##0.00 \"\u20ac\"_-;-* #,##0.00 \"\u20ac\"_-;_-* \"-\"?? \"\u20ac\"_-;_-@_-"; break;
                case 45:  formatCode = "MM:SS"; break;
                case 46:  formatCode = "[h]:mm:ss"; break;
                case 47:  formatCode = "mm:ss.0"; break;
                case 48:  formatCode = "##0.0E+0"; break;
                case 49:  formatCode = "@"; break;
                case 50:  formatCode = "M/D/YYYY"; break;
                case 51:  // PASSTHROUGH INTENTED !!
                case 52:  // PASSTHROUGH INTENTED !!
                case 53:  // PASSTHROUGH INTENTED !!
                case 54:  // PASSTHROUGH INTENTED !!
                case 55:  // PASSTHROUGH INTENTED !!
                case 56:  // PASSTHROUGH INTENTED !!
                case 57:  // PASSTHROUGH INTENTED !!
                case 58:  formatCode = "M/D/YYYY"; break;
                case 59:  formatCode = "0"; break;
                case 60:  formatCode = "0.00"; break;
                case 61:  formatCode = "#,##0"; break;
                case 62:  formatCode = "#,##0.00"; break;
                case 63:  formatCode = "#,##0 \"\u20ac\";-#,##0 \"\u20ac\""; break;
                case 64:  formatCode = "#,##0 \"\u20ac\";[RED]-#,##0 \"\u20ac\""; break;
                case 65:  formatCode = "#,##0.00 \"\u20ac\";-#,##0.00 \"\u20ac\""; break;
                case 66:  formatCode = "#,##0.00 \"\u20ac\";[RED]-#,##0.00 \"\u20ac\""; break;
                case 67:  formatCode = "0%"; break;
                case 68:  formatCode = "0.00%"; break;
                case 69:  formatCode = "0.00E+00"; break;
                case 70:  formatCode = "# ?/?"; break;
                case 71:  // PASSTHROUGH INTENTED !!
                case 72:  formatCode = "M/D/YYYY"; break;
                case 73:  formatCode = "D-MMM-YY"; break;
                case 74:  formatCode = "D-MMM"; break;
                case 75:  formatCode = "MMM-YY"; break;
                case 76:  formatCode = "HH:MM"; break;
                case 77:  formatCode = "HH:MM:SS"; break;
                case 78:  formatCode = "M/D/YYYY H:MM"; break;
                case 79:  formatCode = "mm:ss"; break;
                case 80:  formatCode = "[h]:mm:ss"; break;
                case 81:  formatCode = "mm:ss.0"; break;
                default: {
                    formatCode = "0";
                }
            }
        }

        final String[] codeSections = formatCode.split(";", -1);
        final int sectionCount = codeSections.length > 4 ? 4 : codeSections.length;
        final String[] numberStyleBases = new String[sectionCount];
        for(int i = 0; i < sectionCount; i++) {
            final NumberStyleBase numberStyleBase = createNumberStyleBase(codeSections[i], additionalStyleProperties, volatileStyle, automaticStyle, contentStyle);
            if(i > 0) {
                String condition = "value()";
                // TODO: extract condition if available (e.g. '[>5]')
                condition += sectionCount == 2  ? ">=0" : i == 1 ? ">0" : "<0";
                final MapProperties mapProperties = new MapProperties(new AttributesImpl());
                mapProperties.getAttributes().setValue(Namespaces.STYLE, "apply-style-name", "style:apply-style-name", numberStyleBases[i - 1]);
                mapProperties.getAttributes().setValue(Namespaces.STYLE, "condition", "style:condition", condition);
                numberStyleBase.getMapStyleList().add(mapProperties);
            }
            numberStyleBases[i] = getStyleIdForStyleBase(numberStyleBase);
        }
        return numberStyleBases[sectionCount - 1];
    }

    final NumberStyleBase createNumberStyleBase(String codeSection, Map<String, String> additionalStyleProperties, boolean volatileStyle, boolean automaticStyle, boolean contentStyle) {
        final FormatCode formatCode = new FormatCode(codeSection);

        StyleFamily dataStyleFamily;
        if(formatCode.isText()) {
            dataStyleFamily = StyleFamily.TEXT_STYLE;
        } else if(formatCode.isBoolean()) {
            dataStyleFamily = StyleFamily.BOOLEAN_STYLE;
        } else if(formatCode.isCurrency()) {
            dataStyleFamily = StyleFamily.CURRENCY_STYLE;
        } else if(formatCode.isPercentage()) {
            dataStyleFamily = StyleFamily.PERCENTAGE_STYLE;
        } else if(formatCode.isDate()) {
            dataStyleFamily = StyleFamily.DATE_STYLE;
        } else if(formatCode.isTime()) {
            dataStyleFamily = StyleFamily.TIME_STYLE;
        } else {
            dataStyleFamily = StyleFamily.NUMBER_STYLE;
        }

        final NumberStyleBase numberStyleBase = (NumberStyleBase)createStyleBase(dataStyleFamily, getUniqueStyleName(dataStyleFamily, contentStyle), automaticStyle, contentStyle);
        if(volatileStyle) {
            numberStyleBase.setIsVolatile(true);
        }
        if(additionalStyleProperties!=null&&!additionalStyleProperties.isEmpty()) {

            numberStyleBase.getAttributes().setValue(Namespaces.NUMBER, "automatic-order", "number:automatic-order", null);
            numberStyleBase.getAttributes().setValue(Namespaces.NUMBER, "format-source", "number:format-source", null);

            for(Entry<String, String> entry:additionalStyleProperties.entrySet()) {
                final String qName = entry.getKey();
                final int index = qName.indexOf(':');
                if(index>0&&qName.length()>index+1) {
                    final String prefix = qName.substring(0, index);
                    final String localName = qName.substring(index + 1);
                    final String uri = OdfDocumentNamespace.getUri(prefix);
                    if(uri!=null) {
                        numberStyleBase.getAttributes().setValue(uri, localName, qName, entry.getValue());
                    }
                }
            }
        }
        numberStyleBase.setFormat(formatCode, additionalStyleProperties);
        return numberStyleBase;
    }

    public String getExistingStyleIdForStyleBase(StyleBase styleBase) {
    	return allStyles.get(styleBase);
    }

	private StyleBase findParentStyle(StyleBase style, Iterable<StyleBase> autoStyleStack) {
		final StyleBase parent = getStyle(style.getParent(), style.getFamily(), style.isContentStyle());
		if(parent!=null&&autoStyleStack!=null) {
			final Iterator<StyleBase> iter = autoStyleStack.iterator();
			while(iter.hasNext()) {
				if(parent.getName().equals(iter.next().getName())) {
					return null;
				}
			}
		}
		return parent;
	}

	/* returns the parent autostyle or zero. If autoStyleStack is given then the parent is checked
	   against the autoStyleStack to prevent recursion
	 */
	private StyleBase findParentAutoStyle(StyleBase style, Iterable<StyleBase> autoStyleStack) {
		final StyleBase parent = getAutoStyle(style.getParent(), style.getFamily(), style.isContentStyle());
		if(parent!=null&&autoStyleStack!=null) {
			final Iterator<StyleBase> iter = autoStyleStack.iterator();
			while(iter.hasNext()) {
				if(parent.getName().equals(iter.next().getName())) {
					return null;
				}
			}
		}
		return parent;
	}

	// is copying the content of each hashmap into dest.
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void deepCopy(Map<String, Object> source, Map<String, Object> dest) {
		final Iterator<Entry<String, Object>> sourceIter = source.entrySet().iterator();
		while(sourceIter.hasNext()) {
			final Entry<String, Object> sourceEntry = sourceIter.next();
			if(sourceEntry.getValue() instanceof Map) {
				Object o = dest.get(sourceEntry.getKey());
				if(!(o instanceof Map)) {
					o = new OpAttrs();
					dest.put(sourceEntry.getKey(), o);
				}
				deepCopy((Map)sourceEntry.getValue(), (Map)o);
			}
			else {
				dest.put(sourceEntry.getKey(), sourceEntry.getValue());
			}
		}
	}

	/*
	 * each source value that is not available within dest will be copied
	 */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void deepMerge(Map<String, Object> source, Map<String, Object> dest) {
        final Iterator<Entry<String, Object>> sourceIter = source.entrySet().iterator();
        while(sourceIter.hasNext()) {
            final Entry<String, Object> sourceEntry = sourceIter.next();
            if(sourceEntry.getValue() instanceof Map) {
                Object o = dest.get(sourceEntry.getKey());
                if(!(o instanceof Map)) {
                    o = new OpAttrs();
                    dest.put(sourceEntry.getKey(), o);
                }
                deepMerge((Map)sourceEntry.getValue(), (Map)o);
            }
            else if(!dest.containsKey(sourceEntry.getKey())) {
                dest.put(sourceEntry.getKey(), sourceEntry.getValue());
            }
        }
    }

	// Nice to know, if a frame uses borders instead of stroke depends on having "Graphics"
	// or "Frame" as parent style The style itself can be empty, it is just the name that matters.
	public void updateFrameStyleProperty(StyleBase styleBase) {
		StyleBase parent = styleBase;
		Stack<StyleBase> styleStack = null;
		while(parent!=null&&!parent.getName().equals("Graphics")&&!parent.getName().equals("Frame")) {
			if(!(parent instanceof IGraphicProperties)) {
				return;
			}
			final Boolean isFrameOrGraphic = ((IGraphicProperties)parent).getGraphicProperties().getIsFrameOrGraphic();
			if(isFrameOrGraphic!=null) {
				((IGraphicProperties)styleBase).getGraphicProperties().setIsFrameOrGraphic(isFrameOrGraphic);
				if(styleStack!=null) {
					final Iterator<StyleBase> styleStackIter = styleStack.iterator();
					while(styleStackIter.hasNext()) {
						((IGraphicProperties)styleStackIter.next()).getGraphicProperties().setIsFrameOrGraphic(isFrameOrGraphic);
					}
				}
				return;
			}
			if(styleStack==null) {
				styleStack = new Stack<StyleBase>();
			}
			styleStack.push(parent);

			parent = findParentStyle(parent, styleStack);
		}
		if(parent!=null&&parent instanceof IGraphicProperties) {
			((IGraphicProperties)parent).getGraphicProperties().setIsFrameOrGraphic(true);
		}
		if(styleStack!=null) {
			final Iterator<StyleBase> styleStackIter = styleStack.iterator();
			while(styleStackIter.hasNext()) {
				((IGraphicProperties)styleStackIter.next()).getGraphicProperties().setIsFrameOrGraphic(parent!=null);
			}
		}
	}

	private StyleBase createStyleBase(StyleFamily family, String styleId, boolean automaticStyle, boolean contentStyle) {
		switch(family) {
			case TEXT : {
				return new StyleText(styleId, automaticStyle, contentStyle);
			}
			case PARAGRAPH : {
				return new StyleParagraph(styleId, automaticStyle, contentStyle);
			}
			case SECTION : {
				return new StyleSection(styleId, automaticStyle, contentStyle);
			}
			case RUBY : {
				return new StyleRuby(styleId, automaticStyle, contentStyle);
			}
			case TABLE : {
				return new StyleTable(styleId, automaticStyle, contentStyle);
			}
			case TABLE_COLUMN : {
				return new StyleTableColumn(styleId, automaticStyle, contentStyle);
			}
			case TABLE_ROW : {
				return new StyleTableRow(styleId, automaticStyle, contentStyle);
			}
			case TABLE_CELL : {
				return new StyleTableCell(styleId, automaticStyle, contentStyle);
			}
			case GRAPHIC : {
				return new StyleGraphic(styleId, automaticStyle, contentStyle);
			}
			case PRESENTATION : {
				return new StylePresentation(styleId, automaticStyle, contentStyle);
			}
			case DRAWING_PAGE : {
				return new StyleDrawingPage(styleId, automaticStyle, contentStyle);
			}
			case CHART : {
				return new StyleChart(styleId, automaticStyle, contentStyle);
			}
			case PAGE_LAYOUT : {
				return new StylePageLayout(styleId, automaticStyle, contentStyle);
			}
			case NUMBER_STYLE : {
	    		return new NumberNumberStyle(styleId, automaticStyle, contentStyle);
			}
			case BOOLEAN_STYLE : {
	    		return new NumberBooleanStyle(styleId, automaticStyle, contentStyle);
			}
			case CURRENCY_STYLE : {
	    		return new NumberCurrencyStyle(styleId, automaticStyle, contentStyle);
			}
			case DATE_STYLE : {
	    		return new NumberDateStyle(styleId, automaticStyle, contentStyle);
			}
			case PERCENTAGE_STYLE : {
	    		return new NumberPercentageStyle(styleId, automaticStyle, contentStyle);
			}
			case TEXT_STYLE : {
	    		return new NumberTextStyle(styleId, automaticStyle, contentStyle);
			}
			case TIME_STYLE : {
	    		return new NumberTimeStyle(styleId, automaticStyle, contentStyle);
			}
			case LIST_STYLE : {
	    		return new TextListStyle(styleId, automaticStyle, contentStyle);
			}
			case STROKE_DASH : {
				return new StrokeDashStyle(styleId);
			}
			case FILL_IMAGE : {
			    return new DrawFillImage(styleId);
			}
			case TABLE_TEMPLATE : {
			    return new TableTemplate(styleId, new AttributesImpl());
			}
		}
		return null;
	}

	public boolean hasTabsRelativeToIndent() {
		return hasTabsRelativeToIndent;
	}

	public void setTabsRelativeToIndent(boolean hasTabsRelativeToIndent) {
		this.hasTabsRelativeToIndent = hasTabsRelativeToIndent;
	}

    public void addFontToDocument(String fontName) {
    	if(fontName==null||fontName.isEmpty()) {
    		return;
    	}
    	if(fontFaceDeclsMaster==null||fontFaceDeclsContent==null) {
    		return;
    	}
    	if(fontFaceDeclsMaster.getFontFaces().containsKey(fontName)) {
    		return;
    	}
    	switch(fontName) {
    		case "Andale Mono": {
	            insertFontDescription("Andale Mono", null, "Andale Mono", null, null, null);
	            break;
    		}
    		case "Arial":  {
	            insertFontDescription("Arial", null, "Arial", "swiss", "variable", null);
	            break;
    		}
    		case "Book Antiqua": {
	            insertFontDescription("Book Antiqua", null, "Book Antiqua", "roman", "variable", null);
	            break;
    		}
    		case "Calibri": {
	            insertFontDescription("Calibri", null, "Calibri", "swiss", "variable", "2 15 5 2 2 2 4 3 2 4");
	            break;
    		}
    		case "Cambria": {
	            insertFontDescription("Cambria", null, "Cambria", "roman", "variable", "2 4 5 3 5 4 6 3 2 4");
	            break;
    		}
    		case "Consolas": {
	            insertFontDescription("Consolas", null, "Consolas", "modern", "fixed", null);
	            break;
    		}
    		case "Courier New": {
	            insertFontDescription("Courier New", null, "Courier New", "modern", "fixed", null);
	            break;
    		}
    		case "Courier": {
	            insertFontDescription("Courier", null, "Courier", "modern", "fixed", null);
	            break;
    		}
    		case "Georgia": {
	            insertFontDescription("Georgia", null, "Georgia", "roman", "variable", null);
	            break;
    		}
    		case "Helvetica": {
	            insertFontDescription("Helvetica", null, "Helvetica", "swiss", null, null);
	            break;
    		}
    		case "Impact": {
	            insertFontDescription("Impact", null, "Impact", "swiss", "variable", null);
	            break;
    		}
    		case "Mangal": {
	            insertFontDescription("Mangal", null, "Mangal", "system", "variable", null);
	            break;
    		}
    		case "Mangal1": {
	            insertFontDescription("Mangal1", null, "Mangal", null, null, null);
	            break;
    		}
    		case "Microsoft YaHei": {
	            insertFontDescription("Microsoft YaHei", null, "Microsoft YaHei", "system", "variable", null);
	            break;
    		}
    		case "MS Gothic": {
	            insertFontDescription("MS Gothic", null, "MS Gothic", "modern", "fixed", "2 11 6 9 7 2 5 8 2 4");
	            break;
    		}
    		case "MS Mincho": {
	            insertFontDescription("MS Mincho", null, "MS Mincho", "modern", "fixed", "2 2 6 9 4 2 5 8 3 4");
	            break;
    		}
    		case "Palatino": {
	            insertFontDescription("Palatino", null, "Palatino", "roman", null, null);
	            break;
    		}
    		case "SimSun": {
	            insertFontDescription("SimSun", null, "SimSun", "system", "variable", null);
	            break;
    		}
    		case "Tahoma": {
	            insertFontDescription("Tahoma", null, "Tahoma", "swiss", "variable", null);
	            break;
    		}
    		case "Times New Roman": {
	            insertFontDescription("Times New Roman", null, "Times New Roman", "roman", "variable", "2 2 6 3 5 4 5 2 3 4");
	            break;
    		}
    		case "Times": {
	            insertFontDescription("Times", null, "Times", "roman", null, null);
	            break;
    		}
    		case "Verdana": {
	            insertFontDescription("Verdana", null, "Verdana", "swiss", "variable", null);
	            break;
	        }
    	}
    }

    public void insertFontDescription(String fontName, String[] altNames, String family, String familyGeneric, String pitch, String panose1) {
    	if(fontName!=null&&!fontName.isEmpty()) {
	        final FontFace fontFace = new FontFace(new AttributesImpl());
	        final AttributesImpl fontFaceAttributes = fontFace.getAttributes();
	        fontFaceAttributes.setValue(Namespaces.STYLE, "name", "style:name", fontName);
	        if(family!=null&&!family.isEmpty()) {
	        	fontFaceAttributes.setValue(Namespaces.SVG, "font-family", "svg:font-family", family);
	        }
	        if(familyGeneric!= null&&!familyGeneric.isEmpty()) {
	        	fontFaceAttributes.setValue(Namespaces.STYLE, "font-family-generic", "style:font-family-generic", familyGeneric);
	        }
	        if(pitch!=null&&!pitch.isEmpty()) {
	            fontFaceAttributes.setValue(Namespaces.STYLE, "font-pitch", "style:font-pitch", pitch);
	        }
	        if(panose1!=null&&!panose1.isEmpty()) {
	            if(panose1.contains("[")) {
	                panose1 = panose1.substring(1, panose1.length() - 1);
	            }
	            if(panose1.contains(",")) {
	                panose1 = panose1.replace(',', ' ');
	            }
	            fontFaceAttributes.setValue(Namespaces.SVG, "panose1", "svg:panose1", panose1);
	        }
	        fontFaceDeclsMaster.getFontFaces().put(fontName, fontFace);
	        fontFaceDeclsContent.getFontFaces().put(fontName, fontFace.clone());
    	}
    }

    public String getCellStyleFromTableStyle(String tableStyleName, boolean contentAutoStyle, boolean autoStyleRequired) {
        String tableCellStyle = null;
        while(tableStyleName!=null&&!tableStyleName.isEmpty()) {
            StyleBase styleBase = getStyle(tableStyleName + ".A1", StyleFamily.TABLE_CELL, contentAutoStyle);
            if(styleBase!=null) {
                tableCellStyle = styleBase.getName();
                if(!styleBase.isAutoStyle() && autoStyleRequired) {

                    // create a automatic style
                    styleBase = styleBase.clone();
                    styleBase.setIsContentStyle(contentAutoStyle);
                    styleBase.setIsAutoStyle(true);
                    styleBase.setName(getUniqueStyleName(StyleFamily.TABLE_CELL, true));

                    // check if existing styles can be reused otherwise add it
                    tableCellStyle = getStyleIdForStyleBase(styleBase);
                }
                break;
            }
            styleBase = getStyle(tableStyleName, StyleFamily.TABLE, contentAutoStyle);
            if(styleBase==null) {
                break;
            }
            tableStyleName = styleBase.getParent();
        }
        return tableCellStyle;
    }

    private HashSet<String> usedLanguages = new HashSet<String>();

    public Set<String> getUsedLanguages() {
        return usedLanguages;
    }

    private HashSet<String> usedLanguagesEA = new HashSet<String>();

    public Set<String> getUsedLanguagesEA() {
        return usedLanguagesEA;
    }

    private HashSet<String> usedLanguagesBidi = new HashSet<String>();

    public Set<String> getUsedLanguagesBidi() {
        return usedLanguagesBidi;
    }

    private final HashMap<String, String> listItemStyleIds = new HashMap<String, String>();

    public HashMap<String, String> getListItemStyleIds() {
        return listItemStyleIds;
    }

// ------------------------------------------------------------------------------------------------------------------
// Following methods are to be called from the corresponding SaxContextHandlers only to initialize the StyleManager -
// Styles have to be inserted into the DOM otherwise style changes arent't saved when writing the document.         -
// ------------------------------------------------------------------------------------------------------------------

	public void setStyles(Styles styles) {
		this.styles = styles;
	}

	public void setMasterStyles(MasterStyles masterStyles) {
		this.masterStyles = masterStyles;
	}

	public void setAutomaticStyles(AutomaticStyles automaticStyles, boolean content) {
		if(content) {
			this.automaticStylesContent = automaticStyles;
		}
		else {
			this.automaticStylesMaster = automaticStyles;
		}
	}

	public void setFontFaceDecls(FontFaceDecls fontFaceDecls, boolean content) {
		if(content) {
			fontFaceDeclsContent = fontFaceDecls;
		}
		else {
			fontFaceDeclsMaster = fontFaceDecls;
		}
	}

	// ------------------------------------------------------------------------------------------------------------------
	// Style optimizations ...
	// ------------------------------------------------------------------------------------------------------------------

	// the set of styles that are referenced via styleRefAttributes when loading
	final Set<String> usedStyles = new HashSet<String>();

	// all attributes of the odf file format that are referencing a style (styleRef). !If the attribute name is ending
	// with "names" then the value of the attribute is a space separated list of style names (styleRefs) e.g. "text:class-names"
    static final public ImmutableSet<String> styleRefAttributes = new ImmutableSet.Builder<String>()
        .add("chart:style-name")
        .add("db:default-cell-style-name")
        .add("db:default-row-style-name")
        .add("db:style-name")
        .add("draw:stroke-dash-names")                      // styleRefs
        .add("draw:class-names")                            // styleRefs
        .add("draw:fill-gradient-name")
        .add("draw:fill-hatch-name")
        .add("draw:fill-image-name")
        .add("draw:marker-end")
        .add("draw:marker-start")
        .add("draw:master-page-name")
        .add("draw:opacity-name")
        .add("draw:stroke-dash")
        .add("draw:style-name")
        .add("draw:text-style-name")
        .add("form:text-style-name")
        .add("presentation:class-names")                    // styleRefs
        .add("presentation:presentation-page-layout-name")
        .add("presentation:style-name")
        .add("style:apply-style-name")
        .add("style:data-style-name")
        .add("style:leader-text-style")
        .add("style:list-style-name")
        .add("style:master-page-name")
        .add("style:next-style-name")
        .add("style:page-layout-name")
        .add("style:parent-style-name")
        .add("style:percentage-data-style-name")
        .add("style:register-truth-ref-style-name")
        .add("style:style-name")
        .add("style:text-line-through-text-style")
        .add("table:default-cell-style-name")
        .add("table:paragraph-style-name")
        .add("table:style-name")
        .add("text:citation-body-style-name")
        .add("text:citation-style-name")
        .add("text:class-names")                            // styleRefs
        .add("text:cond-style-name")
        .add("text:default-style-name")
        .add("text:main-entry-style-name")
        .add("text:master-page-name")
        .add("text:style-name")
        .add("text:style-override")
        .add("text:visited-style-name")
        .build();

    final private IStartElementListener startElementListener = (attributes, uri, localName, qName) -> {
        for(int i = 0; i < attributes.getLength(); i++) {
            final String attr = attributes.getQName(i);
            if(styleRefAttributes.contains(attr)) {
                if(attr.endsWith("names")) { // taking care of the type "styleRefs"
                    final String[] styleNames = attributes.getValue(i).split(" ", -1);
                    for(String styleName:styleNames) {
                        if(!styleName.isEmpty()) {
                            usedStyles.add(styleName);
                        }
                    }
                }
                else {
                    usedStyles.add(attributes.getValue(i));
                }
            }
        }
    };

    public void removeUnusedStyles() {
        removeUnusedStyles(StyleFamily.TEXT, true, true);
        removeUnusedStyles(StyleFamily.TEXT, true, false);
        removeUnusedStyles(StyleFamily.PARAGRAPH, true, true);
        removeUnusedStyles(StyleFamily.PARAGRAPH, true, false);
    }

    private void removeUnusedStyles(StyleFamily styleFamily, boolean automaticStyle, boolean contentStyle) {
        final HashMap<String, DLNode<Object>> styleMap;
        final DLList<Object> contentAccessor;
        if (automaticStyle) {
            styleMap = contentStyle ? familyAutomaticStylesContent.getStylesByFamily(styleFamily) : familyAutomaticStylesMaster.getStylesByFamily(styleFamily);
            contentAccessor = contentStyle ? automaticStylesContent.getContent() : automaticStylesMaster.getContent();
        }
        else {
            styleMap = contentStyle ? familyStyles.getStylesByFamily(styleFamily) : familyMasterStyles.getStylesByFamily(styleFamily);
            contentAccessor = contentStyle ? styles.getContent() : masterStyles.getContent();
        }
        if(styleMap!=null) {
            styleMap.entrySet().removeIf(entry -> {
                if(!usedStyles.contains(entry.getKey())) {
                    final DLNode<Object> styleBaseNode = entry.getValue();
                    final StyleBase styleBase = (StyleBase)styleBaseNode.getData();
                    contentAccessor.removeNode(styleBaseNode);
                    final String allStyleName = allStyles.get(styleBase);
                    if (entry.getKey().equals(allStyleName)) {
                        allStyles.remove(styleBase);
                    }
                    return true;
                }
                return false;
            });
        }
    }

    public void initializeStyleOptimizations(SaxContextHandler contextHandler) {
	    contextHandler.addStartElementListener(startElementListener);
	}
}
