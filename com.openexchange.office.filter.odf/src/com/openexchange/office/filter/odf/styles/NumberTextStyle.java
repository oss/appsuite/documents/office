/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.core.spreadsheet.FormatCode;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.properties.NumberText;
import com.openexchange.office.filter.odf.properties.NumberTextContent;

final public class NumberTextStyle extends NumberStyleBase {

	public NumberTextStyle(String name, boolean automaticStyle, boolean contentStyle) {
		super(null, name, automaticStyle, contentStyle);
	}

	public NumberTextStyle(String name, AttributesImpl attributesImpl, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, false, automaticStyle, contentStyle);
	}

	@Override
	public String getQName() {
		return "number:text-style";
	}

	@Override
	public String getLocalName() {
		return "text-style";
	}

	@Override
	public String getNamespace() {
		return Namespaces.NUMBER;
	}

	@Override
	public String getFormat(StyleManager styleManager, Map<String, String> additionalStyleProperties, boolean contentAutoStyle) {
		String result = "";
		final Iterator<IElementWriter> propertiesIter = getContent().iterator();
		while(propertiesIter.hasNext()) {
			final IElementWriter properties = propertiesIter.next();
            if (properties instanceof NumberText) {
                String textcontent = ((NumberText)properties).getTextContent();
                if (textcontent == null || textcontent.length() == 0) {
                    textcontent = " ";
                }
                result += textcontent;
            }
            else if (properties instanceof NumberTextContent) {
                result += "@";
            }
		}
		if(!getMapStyleList().isEmpty()) {
			result = getMapStyleList().get(0).getMapping(styleManager, contentAutoStyle, getFamily()) + ";" + result;
		}
        return result;
	}

	@Override
	public void setFormat(FormatCode _formatCode, Map<String, String> additionalStyleProperties) {
	    String formatCode = _formatCode.toString();
        while(!formatCode.isEmpty()){
            int atPos = formatCode.indexOf("@");
            if(atPos==0) {
            	getContent().add(new NumberTextContent(new AttributesImpl()));
            } else {
            	getContent().add(new NumberText(atPos < 0 ? formatCode : formatCode.substring(0, atPos)));
                if(atPos < 0) {
                    break; // finished
                }
               	getContent().add(new NumberTextContent(new AttributesImpl()));
            }
            formatCode = formatCode.substring(atPos + 1);
        }
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		//
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		// TODO Auto-generated method stub

	}
}
