/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.HeaderFooterProperties;
import com.openexchange.office.filter.odf.properties.PageLayoutProperties;

final public class StylePageLayout extends StyleBase {

	private PageLayoutProperties pageLayoutProperties = new PageLayoutProperties(new AttributesImpl());
	private HeaderFooterProperties headerProperties = new HeaderFooterProperties(new AttributesImpl());
	private HeaderFooterProperties footerProperties = new HeaderFooterProperties(new AttributesImpl());

	public StylePageLayout(String name, boolean automaticStyle, boolean contentStyle) {
		super(null, name, automaticStyle, contentStyle);
	}

	public StylePageLayout(String name, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, defaultStyle, automaticStyle, contentStyle);
	}

	@Override
    public StyleFamily getFamily() {
		return StyleFamily.PAGE_LAYOUT;
	}

	@Override
    public String getQName() {
		return "style:page-layout";
	}

	@Override
    public String getLocalName() {
		return "page-layout";
	}
	
	public PageLayoutProperties getPageLayoutProperties() {
		return pageLayoutProperties;
	}

	public HeaderFooterProperties getHeaderProperties() {
		return headerProperties;
	}

	public HeaderFooterProperties getFooterProperties() {
		return footerProperties;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		pageLayoutProperties.writeObject(output);
		SaxContextHandler.startElement(output, getNamespace(), "header-style", "style:header-style");
		headerProperties.writeObject(output);
		SaxContextHandler.endElement(output, getNamespace(), "header-style", "style:header-style");
		SaxContextHandler.startElement(output, getNamespace(), "footer-style", "style:footer-style");
		footerProperties.writeObject(output);
		SaxContextHandler.endElement(output, getNamespace(), "footer-style", "style:footer-style");
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		if(styleBase instanceof StylePageLayout) {
			getPageLayoutProperties().mergeAttrs(((StylePageLayout)styleBase).getPageLayoutProperties());
			getHeaderProperties().mergeAttrs(((StylePageLayout)styleBase).getHeaderProperties());
			getFooterProperties().mergeAttrs(((StylePageLayout)styleBase).getFooterProperties());
		}
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		pageLayoutProperties.applyAttrs(styleManager, attrs);
		headerProperties.applyAttrs(styleManager, attrs);
		footerProperties.applyAttrs(styleManager, attrs);
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		pageLayoutProperties.createAttrs(styleManager, isContentStyle(), attrs);
		headerProperties.createAttrs(styleManager, isContentStyle(), attrs);
		footerProperties.createAttrs(styleManager, isContentStyle(), attrs);
	}

	@Override
	protected int _hashCode() {
		int hashCode = pageLayoutProperties.hashCode();
		hashCode = 31 * hashCode + headerProperties.hashCode();
		return 31 * hashCode + footerProperties.hashCode();
	}

	@Override
	protected boolean _equals(StyleBase e) {
		final StylePageLayout other = (StylePageLayout)e;
		if(!pageLayoutProperties.equals(other.pageLayoutProperties)) {
			return false;
		}
		if(!headerProperties.equals(other.headerProperties)) {
			return false;
		}
		if(!footerProperties.equals(other.footerProperties)) {
			return false;
		}
		return true;
	}

	@Override
	public StylePageLayout clone() {
		final StylePageLayout clone = (StylePageLayout)_clone();
		clone.pageLayoutProperties = pageLayoutProperties.clone();
		clone.headerProperties = headerProperties.clone();
		clone.footerProperties = footerProperties.clone();
		return clone;
	}
}
