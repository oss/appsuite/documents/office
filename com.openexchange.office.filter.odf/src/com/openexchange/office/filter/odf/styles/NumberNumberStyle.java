/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.core.spreadsheet.FormatCode;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.properties.NumberFraction;
import com.openexchange.office.filter.odf.properties.NumberNumber;
import com.openexchange.office.filter.odf.properties.NumberScientificNumber;
import com.openexchange.office.filter.odf.properties.NumberText;
import com.openexchange.office.filter.odf.properties.TextProperties;

final public class NumberNumberStyle extends NumberStyleBase {

	public NumberNumberStyle(String name, boolean automaticStyle, boolean contentStyle) {
		super(null, name, automaticStyle, contentStyle);
	}

	public NumberNumberStyle(String name, AttributesImpl attributesImpl, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, false, automaticStyle, contentStyle);
	}

	@Override
	public String getQName() {
		return "number:number-style";
	}

	@Override
	public String getLocalName() {
		return "number-style";
	}

	@Override
	public String getNamespace() {
		return Namespaces.NUMBER;
	}

	@Override
	public String getFormat(StyleManager styleManager, Map<String, String> additionalStyleProperties, boolean contentAutoStyle) {
		String result = "";
		final Iterator<IElementWriter> propertiesIter = getContent().iterator();
		while(propertiesIter.hasNext()) {
			final IElementWriter properties = propertiesIter.next();
			if (properties instanceof NumberNumber) {
				result += ((NumberNumber)properties).getNumberFormat();
			}
			else if (properties instanceof NumberText) {
				String textcontent = ((NumberText)properties).getTextContent();
				if (textcontent!=null&&!textcontent.isEmpty()) {
	                result += quoteTextContent(textcontent);
				}
            }
			else if(properties instanceof TextProperties) {
                result += ((TextProperties)properties).getColorFromElement();
            }
			else if(properties instanceof NumberFraction) {
				final NumberFraction f = (NumberFraction)properties;
                Integer digitCount = f.getInteger("number:min-integer-digits", null);
                if(digitCount!=null){
                    if(digitCount == 0 ){
                        result +=  '#'; // show optional integer part of the fraction
                    } else {
                        while(--digitCount >= 0 ) {
                            result +=  '0';
                        }
                    }
                    result += ' '; //space between integer part and fraction
                }
                Integer numeratorCount = f.getInteger("number:min-numerator-digits", null);
                if(numeratorCount != null ){
                    while(--numeratorCount >= 0 ) {
                        result +=  '?';
                    }
                } else {
                    result += '?';
                }

                result += '/';

                Integer denominatorValue = f.getInteger("number:denominator-value", null);
                if(denominatorValue != null) {
                    result += denominatorValue.toString();
                }
                else {
                    Integer denominatorCount = f.getInteger("number:min-denominator-digits", null);
                    if( denominatorCount != null ){
                        while(--denominatorCount >= 0 ) {
                            result += '?';
                        }
                    } else {
                        result += '?';
                    }
                }
            }
			else if(properties instanceof NumberScientificNumber){
                NumberScientificNumber s = (NumberScientificNumber)properties;
                Boolean isGroup = s.getBoolean("number:grouping", false);
                Integer digits = s.getInteger("number:min-integer-digits", null);
                int digitCount = digits == null ? 0 : digits.intValue();
                for(int digit = 0; digit < digitCount; ++ digit){
                    result += '0';
                }
                Integer places = s.getInteger("number:decimal-places", null);
                if(places != null){
                    result += '.';
                    int placeCount = places.intValue();
                    while(--placeCount >= 0){
                        result += '0';
                    }
                }
                result += 'E';
                if(isGroup != null && isGroup.booleanValue()){
                    //fill with #,##...
                    if(digitCount < 4){
                        String fill = "#,###";
                        result = fill.substring(0, 5 - digitCount) + result;
                    } else {
                        result = result.substring(0, digitCount - 3) + ',' + result.substring(digitCount - 3);
                    }
                }
                Integer exp = s.getInteger("number:min-exponent-digits", null);
                if(exp != null) {
                    result += '+';
                    int exponents = exp.intValue();
                    while(--exponents >= 0){
                        result += '0';
                    }
                }
            }
        }
		if(!getMapStyleList().isEmpty()) {
            result = getMapStyleList().get(0).getMapping(styleManager, contentAutoStyle, getFamily()) + ";" + result;
		}
		return result;
	}

	@Override
	public void setFormat(FormatCode _formatCode, Map<String, String> additionalStyleProperties) {

	    String formatCode = _formatCode.toString();
	    int openBracket = formatCode.indexOf("[");
        String color = "";
        while(openBracket >= 0){
            int closeBracket = formatCode.indexOf("]", openBracket);
            if(closeBracket > openBracket){
                String innerText = formatCode.substring(openBracket + 1, closeBracket);
                if(innerText.length() > 1) {
                    //detect color - if any
                    color = getColorElement(innerText);
                    if(color!=null) {
                    	final TextProperties textProperties = new TextProperties(new AttributesImpl());
                    	textProperties.getAttributes().setValue(Namespaces.FO, "color", "fo:color", color);
                    	getContent().add(textProperties);
                    }
                    formatCode = formatCode.substring(0, openBracket) + formatCode.substring(closeBracket + 1);
                }
            }
            openBracket = formatCode.indexOf("[");
        }

		/*
		 * If there is a numeric specification, then split the
		 * string into the part before the specifier, the specifier
		 * itself, and then part after the specifier. The parts
		 * before and after are just text (which may contain the
		 * currency symbol).
		 */
        if (!formatCode.isEmpty()) {
            Pattern p = Pattern.compile("[#\\d,.?/E+\\s]+");
            Matcher m = p.matcher(formatCode);

            int lastEnd = 0;
            while (m.find()) {
                String prefix = "";
                if(m.start() > lastEnd ){
                    prefix = formatCode.substring(lastEnd, m.start());
                }
                lastEnd = m.end();
                String sub = formatCode.substring(m.start(), m.end());
                if(sub.startsWith(" ")){
                    int pos = 1;
                    while(sub.length() > pos && sub.charAt(pos) == ' '){
                        ++pos;
                    }
                    prefix += sub.substring( 0, pos);
                    sub = sub.substring(pos);
                }
                if(!prefix.isEmpty()){
                	getContent().add(new NumberText(prefix));
                }
                String suffix = "";
                if(sub.endsWith(" ")){
                    int pos = sub.length() - 1;
                    while(sub.charAt(pos) == ' '){
                        --pos;
                    }
                    suffix = sub.substring( pos + 1 );
                    sub = sub.substring(0, pos + 1);
                }
                boolean denominator = false;
                int denominatorCount = 0;
                int nominatorCount = 0;
                boolean isDecimals = false;
                boolean isFraction = false;
                boolean isHash = false;
                boolean isGrouping = false;
                int digitCount = 0;
                int decimalsCount = 0;
                Integer denominatorValue = null;
                boolean isScientific = false;
                int exponentCount = 0;
                for(int pos = 0; pos < sub.length(); ++pos){
                    char c = sub.charAt(pos);
                    if(c == '?'){
                        isFraction = true;
                        if(denominator){
                            ++denominatorCount;
                        } else {
                            ++nominatorCount;
                        }
                    } else if( c == '/') {
                        denominator = true;
                    } else if (c == ',') {
                        isGrouping = true;
                    } else if (c == '.') {
                        isDecimals = true;
                    } else if (c == '0') {
                        if(isScientific) {
                            ++exponentCount;
                        } else if(isDecimals){
                            ++decimalsCount;
                        } else {
                            ++digitCount;
                        }
                    } else if (denominator && (c > '0' && c <= '9')) {
                        denominatorValue = Integer.valueOf(c - '0');
                        for(++pos;pos < sub.length(); ++pos) {
                            char c1 = sub.charAt(pos);
                            if(c1 >= '0' && c1 <= '9') {
                                denominatorValue *= 10;
                                denominatorValue += Integer.valueOf(c1 - '0');
                            }
                            else {
                                pos--;
                                break;
                            }
                        }
                    } else if (c == 'E') {
                        isScientific = true;
                    } else if (c == '#') {
                        isHash = true; // only required in fraction formats
                    }
                }

                if(isFraction){
                	final NumberFraction number = new NumberFraction(new AttributesImpl());
                    if (isHash || digitCount > 0) {
                    	number.getAttributes().setIntValue(Namespaces.NUMBER, "min-integer-digits", "number:min-integer-digits", isHash ? 0 : digitCount);
                    }
                    if(denominatorValue!=null) {
                        number.getAttributes().setIntValue(Namespaces.NUMBER, "denominator-value", "number:denominator-value", denominatorValue);
                    }
                    number.getAttributes().setIntValue(Namespaces.NUMBER, "min-numerator-digits", "number:min-numerator-digits", nominatorCount);
                    number.getAttributes().setIntValue(Namespaces.NUMBER, "min-denominator-digits", "number:min-denominator-digits", denominatorCount);
                    getContent().add(number);
                }
                else if(isScientific) {
                	final NumberScientificNumber number = new NumberScientificNumber(new AttributesImpl());
                    if(decimalsCount > 0){
                    	number.getAttributes().setIntValue(Namespaces.NUMBER, "decimal-places", "number:decimal-places", decimalsCount);
                    }
                    if (digitCount > 0) {
                    	number.getAttributes().setIntValue(Namespaces.NUMBER, "min-integer-digits", "number:min-integer-digits", digitCount);
                    }
                    if(isGrouping){
                    	number.getAttributes().setBooleanValue(Namespaces.NUMBER, "grouping", "number:grouping", true);
                    }
                    if(exponentCount > 0){
                    	number.getAttributes().setIntValue(Namespaces.NUMBER, "min-exponent-digits", "number:min-exponent-digits", exponentCount);
                    }
                    getContent().add(number);
                }
                else if(sub.length() > 0) {
                    final NumberNumber number = new NumberNumber(new AttributesImpl());
                    if(decimalsCount >= 0){
                    	number.getAttributes().setIntValue(Namespaces.NUMBER, "decimal-places", "number:decimal-places", decimalsCount);
                    }
                    if (digitCount > 0) {
                    	number.getAttributes().setIntValue(Namespaces.NUMBER, "min-integer-digits", "number:min-integer-digits", digitCount);
                    }
                    if(isGrouping){
                    	number.getAttributes().setBooleanValue(Namespaces.NUMBER, "grouping", "number:grouping", true);
                    }
                    getContent().add(number);
                }
                if(!suffix.isEmpty()){
                	getContent().add(new NumberText(suffix));
                }

            }
            if(lastEnd < formatCode.length()){
                String t = formatCode.substring(lastEnd);
                boolean general = false;
                if(t.equalsIgnoreCase("-general")) {
                    general = true;
                    getContent().add(new NumberText("-"));
                }
                else if(t.equalsIgnoreCase("general")) {
                    general = true;
                }
                if(general) {
                    final NumberNumber number = new NumberNumber(new AttributesImpl());
                    number.getAttributes().setIntValue(Namespaces.NUMBER, "min-integer-digits", "number:min-integer-digits", 1);
                    getContent().add(number);
                }
                else {
                    getContent().add(new NumberText(t));
                }
            }
        }
	}

	private String quoteTextContent(String text){
		final Pattern p = Pattern.compile("[MDYHMSE#0,.]+");
        final Matcher m = p.matcher(text);
        if(m.find()){
            text = "\"" + text +  "\"";
	    }
	    return text;
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		//
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		// TODO Auto-generated method stub

	}
}
