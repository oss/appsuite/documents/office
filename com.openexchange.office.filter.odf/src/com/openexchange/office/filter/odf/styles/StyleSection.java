/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.SectionProperties;

final public class StyleSection extends StyleBase {

	private SectionProperties sectionProperties = new SectionProperties(new AttributesImpl());

	public StyleSection(String name, boolean automaticStyle, boolean contentStyle) {
		super(StyleFamily.SECTION, name, automaticStyle, contentStyle);
	}

	public StyleSection(String name, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, defaultStyle, automaticStyle, contentStyle);
	}

	@Override
    public StyleFamily getFamily() {
		return StyleFamily.SECTION;
	}

	public SectionProperties getSectionProperties() {
		return sectionProperties;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		sectionProperties.writeObject(output);
		writeMapStyleList(output);
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		if(styleBase instanceof StyleSection) {
			getSectionProperties().mergeAttrs(((StyleSection)styleBase).getSectionProperties());
		}
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {

		sectionProperties.applyAttrs(styleManager, attrs);
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		sectionProperties.createAttrs(styleManager, isContentStyle(), attrs);
	}

	@Override
	protected int _hashCode() {
		return sectionProperties.hashCode();
	}

	@Override
	protected boolean _equals(StyleBase e) {
		final StyleSection other = (StyleSection)e;
		if(!sectionProperties.equals(other.sectionProperties)) {
			return false;
		}
		return true;
	}

	@Override
	public StyleSection clone() {
		final StyleSection clone = (StyleSection)_clone();
		clone.sectionProperties = sectionProperties.clone();
		return clone;
	}
}
