/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.properties.FontFace;
import com.openexchange.office.filter.odf.properties.StylePropertiesBaseHandler;

public class FontFaceDeclsHandler extends SaxContextHandler {

	final StyleManager styleManager;
	final boolean contentStyle;

	public FontFaceDeclsHandler(SaxContextHandler parentContext, StyleManager styleManager, boolean contentStyle) {
		super(parentContext);

		this.styleManager = styleManager;
		this.contentStyle = contentStyle;
	}

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("style:font-face")) {
    		final FontFace fontFace = new FontFace(new AttributesImpl(attributes));
    		final String name = fontFace.getAttribute("style:name");
    		if(name!=null&&!name.isEmpty()) {
    			styleManager.getFontFaceDecls(contentStyle).getFontFaces().put(name, fontFace);
    			return new StylePropertiesBaseHandler(this, fontFace);
    		}
    	}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	styleManager.getFontFaceDecls(contentStyle).getContent().add(element);
		return new UnknownContentHandler(this, element);
    }
}
