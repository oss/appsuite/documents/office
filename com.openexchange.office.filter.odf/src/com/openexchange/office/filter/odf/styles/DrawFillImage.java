/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.draw.DrawImage;

final public class DrawFillImage extends StyleBase {

    public DrawFillImage(String name) {
        super(null, name, false, false);
    }

    public DrawFillImage(String name, AttributesImpl attributesImpl) {
        super(name, attributesImpl, false, false, false);
    }

    @Override
    public StyleFamily getFamily() {
        return StyleFamily.FILL_IMAGE;
    }

    @Override
    public String getQName() {
        return "draw:fill-image";
    }

    @Override
    public String getLocalName() {
        return "fill-image";
    }

    @Override
    public String getNamespace() {
        return Namespaces.DRAW;
    }

    @Override
    protected void writeNameAttribute(SerializationHandler output) throws SAXException {
        output.addAttribute(Namespaces.DRAW, "name", "draw:name", "", getName());
    }

    @Override
    public void writeObject(SerializationHandler output)
        throws SAXException {

        SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
        writeAttributes(output);
        SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
    }

    @Override
    public void mergeAttrs(StyleBase style) {
        // TODO Auto-generated method stub
    }

    @Override
    public void applyAttrs(StyleManager styleManager, JSONObject bitmapAttrs) {
        final AttributesImpl drawFillImageAttributes = getAttributes();
        final String imageUrl = bitmapAttrs.optString(OCKey.IMAGE_URL.value(), null);
        if(imageUrl!=null) {
            DrawImage.transferResource(imageUrl, styleManager);
            drawFillImageAttributes.setValue(Namespaces.XLINK, "href", "xlink:href", imageUrl);
            drawFillImageAttributes.setValue(Namespaces.XLINK, "type", "xlink:type", "simple");
            drawFillImageAttributes.setValue(Namespaces.XLINK, "show", "xlink:show", "embed");
            drawFillImageAttributes.setValue(Namespaces.XLINK, "actuate", "xlink:actuate", "onLoad");
        }
    }

    @Override
    public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
        final String hRef = attributes.getValue("xlink:href");
        if(hRef!=null) {
            final OpAttrs fillProps = attrs.getMap(OCKey.FILL.value(), true);
            final OpAttrs bitmapProps = fillProps.getMap(OCKey.BITMAP.value(), true);
            bitmapProps.put(OCKey.IMAGE_URL.value(), hRef);
        }
        final String id = attributes.getValue("xml:id");
        if(id!=null) {
            attrs.getMap(OCKey.DRAWING.value(), true).put("imageXmlId", id);
        }
    }

    @Override
    protected int _hashCode() {
        return 0;
    }

    @Override
    protected boolean _equals(StyleBase e) {
        return true;
    }

    @Override
    public DrawFillImage clone() {
        final DrawFillImage clone = (DrawFillImage)_clone();
        return clone;
    }

    public static String getFillImage(StyleManager styleManager, String currentFillImage, JSONObject bitmapAttrs) {
        DrawFillImage drawFillImage = null;
        if(currentFillImage!=null) {
            final StyleBase current = styleManager.getStyle(currentFillImage, StyleFamily.FILL_IMAGE, false);
            if(current!=null) {
                drawFillImage = (DrawFillImage)current.clone();
                drawFillImage.setName(styleManager.getUniqueStyleName(StyleFamily.FILL_IMAGE, false));
            }
        }
        if(drawFillImage==null) {
            drawFillImage = new DrawFillImage(styleManager.getUniqueStyleName(StyleFamily.FILL_IMAGE, false));
        }
        drawFillImage.applyAttrs(styleManager, bitmapAttrs);
        final String existingStyleId = styleManager.getExistingStyleIdForStyleBase(drawFillImage);
        if(existingStyleId!=null) {
            return existingStyleId;
        }
        styleManager.addStyle(drawFillImage);
        return drawFillImage.getName();
    }

    public static void applyTilingAttrs(JSONObject sourceBitmapAttributes, AttributesImpl attrs) {
        final Object transparency = sourceBitmapAttributes.opt(OCKey.TRANSPARENCY.value());
        if(transparency==JSONObject.NULL) {
            attrs.remove("draw:opacity");
        }
        else if(transparency instanceof Number) {
            int opacity = Double.valueOf((1.0 - ((Number)transparency).doubleValue()) * 100.0 + 0.5).intValue();
            if(opacity > 100) {
                opacity = 100;
            }
            attrs.setValue(Namespaces.DRAW, "opacity", "draw:opacity", Integer.valueOf(opacity).toString() + "%");
        }
        final JSONObject tilingAttrs = sourceBitmapAttributes.optJSONObject(OCKey.TILING.value());
        final JSONObject stretchingAttrs = sourceBitmapAttributes.optJSONObject(OCKey.STRETCHING.value());
        final JSONObject stretchingAlignedAttrs = sourceBitmapAttributes.optJSONObject("stretchingAligned");
        final String repeat;
        if(tilingAttrs!=null) {
            repeat = "repeat";
            final String ref = DrawFillImage.getRefPoint(tilingAttrs.optString(OCKey.RECT_ALIGNMENT.value(), null));
            if(ref!=null) {
                attrs.setValue(Namespaces.DRAW, "fill-image-ref-point", "draw:fill-image-ref-point", ref);
            }
        }
        else if(stretchingAttrs!=null) {
            int left = 0;
            int top = 0;
            int right = 0;
            int bottom = 0;
            Integer fillImageWidth = null;
            Integer fillImageHeight = null;
            final Object l = stretchingAttrs.opt(OCKey.LEFT.value());
            if(l instanceof Number) {
                left = ((Number)l).intValue();
            }
            final Object t = stretchingAttrs.opt(OCKey.TOP.value());
            if(t instanceof Number) {
                top = ((Number)t).intValue();
            }
            final Object r = stretchingAttrs.opt(OCKey.RIGHT.value());
            if(r instanceof Number) {
                right = ((Number)r).intValue();
            }
            final Object b = stretchingAttrs.opt(OCKey.BOTTOM.value());
            if(b instanceof Number) {
                bottom = ((Number)b).intValue();
            }
            if(left==0&&top==0&&right==0&&bottom==0) {
                repeat = "stretch";
            }
            else {
                repeat = "no-repeat";
                fillImageWidth = 100 - (left + right);
                fillImageHeight = 100 - (top + bottom);
            }
            if(fillImageWidth!=null) {
                attrs.setValue(Namespaces.DRAW, "fill-image-width", "draw:fill-image-width", fillImageWidth.toString() + "%");
            }
            if(fillImageHeight!=null) {
                attrs.setValue(Namespaces.DRAW, "fill-image-height", "draw:fill-image-height", fillImageHeight.toString() + "%");
            }
        }
        else if(stretchingAlignedAttrs!=null) {
            repeat = "no-repeat";
            final String ref = DrawFillImage.getRefPoint(stretchingAlignedAttrs.optString(OCKey.RECT_ALIGNMENT.value(), null));
            if(ref!=null) {
                attrs.setValue(Namespaces.DRAW, "fill-image-ref-point", "draw:fill-image-ref-point", ref);
            }
            final Object width = stretchingAlignedAttrs.opt(OCKey.WIDTH.value());
            if(width instanceof Number) {
                attrs.setValue(Namespaces.DRAW, "fill-image-width", "draw:fill-image-width", ((Number)width).doubleValue() / 100.0 + "mm");
            }
            final Object scaleWidth = stretchingAlignedAttrs.opt("scaleX");
            if(scaleWidth instanceof Number) {
                attrs.setValue(Namespaces.DRAW, "fill-image-width", "draw:fill-image-width", ((Number)scaleWidth).doubleValue() + "%");
            }
            final Object height = stretchingAlignedAttrs.opt(OCKey.HEIGHT.value());
            if(height instanceof Number) {
                attrs.setValue(Namespaces.DRAW, "fill-image-height", "draw:fill-image-height", ((Number)height).doubleValue() / 100.0 + "mm");
            }
            final Object scaleHeight = stretchingAlignedAttrs.opt("scaleY");
            if(scaleHeight instanceof Number) {
                attrs.setValue(Namespaces.DRAW, "fill-image-height", "draw:fill-image-height", ((Number)scaleHeight).doubleValue() + "%");
            }
        }
        else {
            repeat = "no-repeat";
        }
        attrs.setValue(Namespaces.STYLE, "repeat", "style:repeat", repeat);
    }

    public static void createTilingAttrs(StyleManager styleManager, String imageUrl, String repeat, AttributesImpl sourceAttributes, OpAttrs fillAttrs) {

        // 58240 ... fill bitmaps aren't rotated at all in LO
        fillAttrs.getMap(OCKey.BITMAP.value(), true).put(OCKey.ROTATE_WITH_SHAPE.value(), false);

        final String imageWidth = sourceAttributes.getValue("draw:fill-image-width");
        Integer width = null;
        Integer height = null;
        boolean widthScale = false;
        boolean heightScale = false;
        if(imageWidth!=null&&!imageWidth.isEmpty()) {
            if(imageWidth.charAt(imageWidth.length()-1)=='%') {
                width = AttributesImpl.getPercentage(imageWidth, null);
                widthScale = true;
            }
            else {
                width = AttributesImpl.normalizeLength(imageWidth, false);
            }
        }
        final String imageHeight = sourceAttributes.getValue("draw:fill-image-height");
        if(imageHeight!=null&&!imageHeight.isEmpty()) {
            if(imageHeight.charAt(imageHeight.length()-1)=='%') {
                height = AttributesImpl.getPercentage(imageHeight, null);
                heightScale = true;
            }
            else {
                height = AttributesImpl.normalizeLength(imageHeight, false);
            }
        }
        if("repeat".equals(repeat)) {
            final OpAttrs tilingAttrs = fillAttrs.getMap(OCKey.BITMAP.value(), true).getMap(OCKey.TILING.value(), true);
            tilingAttrs.put(OCKey.RECT_ALIGNMENT.value(), getRectAlignment(sourceAttributes.getValue("draw:fill-image-ref-point")));

            // pixel count of the graphic needed
            Double scaleX = null;
            Double scaleY = null;
            final Pair<Double, Double> dpiFromImage = styleManager.getPackage().getDPIFromImage(imageUrl);
            final double dpiX = dpiFromImage!=null && dpiFromImage.getLeft()!=null && dpiFromImage.getLeft()!=0d ? dpiFromImage.getLeft() : 96d;
            final double dpiY = dpiFromImage!=null && dpiFromImage.getRight()!=null && dpiFromImage.getRight()!=0d ? dpiFromImage.getRight() : 96d;
            if(width==null) {
                scaleX = (100d * 96d) / dpiX;
            }
            else if(widthScale) {
                scaleX = (width * 96d) / dpiX;
            }
            if(height==null) {
                scaleY = (100d * 96d) / dpiY;
            }
            else if(heightScale) {
                scaleY = (height * 96d) / dpiY;
            }
            if(scaleX!=null) {
                tilingAttrs.put(OCKey.STRETCH_X.value(), scaleX);
            }
            if(scaleY!=null) {
                tilingAttrs.put(OCKey.STRETCH_Y.value(), scaleY);
            }
        }
        else if ("no-repeat".equals(repeat)) {
            final OpAttrs stretchingAttrs = fillAttrs.getMap(OCKey.BITMAP.value(), true).getMap("stretchingAligned", true);
            stretchingAttrs.put(OCKey.RECT_ALIGNMENT.value(), getRectAlignment(sourceAttributes.getValue("draw:fill-image-ref-point")));
            if(width!=null) {
                stretchingAttrs.put(widthScale ? "scaleX" : OCKey.WIDTH.value(), width);
            }
            if(height!=null) {
                stretchingAttrs.put(heightScale ? "scaleY" : OCKey.HEIGHT.value(), height);
            }
        }
        else if("stretch".equals(repeat)) {
            final OpAttrs stretchingAttrs = new OpAttrs(4);
            stretchingAttrs.put(OCKey.LEFT.value(), 0);
            stretchingAttrs.put(OCKey.RIGHT.value(), 0);
            stretchingAttrs.put(OCKey.TOP.value(), 0);
            stretchingAttrs.put(OCKey.BOTTOM.value(), 0);
            fillAttrs.getMap(OCKey.BITMAP.value(), true).put(OCKey.STRETCHING.value(), stretchingAttrs);
        }
    }

    private static String getRefPoint(String rectAlignment) {
        String ref = null;
        if(rectAlignment!=null) {
            switch(rectAlignment) {
                case "bottomLeft": ref = "bottom-left"; break;
                case "bottomRight": ref = "bottom-right"; break;
                case "topLeft": ref = "top-left"; break;
                case "topRight": ref = "top-right"; break;
                case "center": // PASSTROUGH INTENDED !!
                case "left":
                case "right":
                case "top":
                case "bottom": ref = rectAlignment; break;
                default: ref = "center"; break;
            }
        }
        return ref;
    }

    private static String getRectAlignment(String refPoint) {
        String ref = "center";
        if(refPoint!=null) {
            switch(refPoint) {
                case "bottom-left": ref = "bottomLeft"; break;
                case "bottom-right": ref = "bottomRight"; break;
                case "top-left": ref = "topLeft"; break;
                case "top-right": ref = "topRight"; break;
                case "center":  // PASSTROUGH INTENDED !!
                case "left":
                case "right":
                case "top":
                case "bottom": ref = refPoint; break;
                default: ref = "center"; break;
            }
        }
        return ref;
    }
}
