/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.HashMap;
import java.util.Map;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Border;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.LoExtTableCellGraphicProperties;
import com.openexchange.office.filter.odf.properties.ParagraphProperties;
import com.openexchange.office.filter.odf.properties.TableCellProperties;
import com.openexchange.office.filter.odf.properties.TextProperties;

final public class StyleTableCell extends StyleBase implements ITextProperties, IParagraphProperties, IGraphicProperties {

    private TableCellProperties             tableCellProperties = new TableCellProperties(new AttributesImpl());
    private ParagraphProperties             paragraphProperties = new ParagraphProperties(new AttributesImpl());
    private TextProperties                  textProperties      = new TextProperties(new AttributesImpl());
    private LoExtTableCellGraphicProperties loExtGraphicProperties;

    public StyleTableCell(String name, boolean automaticStyle, boolean contentStyle) {
        super(StyleFamily.TABLE_CELL, name, automaticStyle, contentStyle);

        loExtGraphicProperties = new LoExtTableCellGraphicProperties(new AttributesImpl(), this, false);
    }

    public StyleTableCell(String name, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle, boolean contentStyle) {
        super(name, attributesImpl, defaultStyle, automaticStyle, contentStyle);

        loExtGraphicProperties = new LoExtTableCellGraphicProperties(new AttributesImpl(), this, defaultStyle);
    }

    @Override
    public StyleFamily getFamily() {
        return StyleFamily.TABLE_CELL;
    }

    public TableCellProperties getTableCellProperties() {
        return tableCellProperties;
    }

    @Override
    public ParagraphProperties getParagraphProperties() {
        return paragraphProperties;
    }

    @Override
    public TextProperties getTextProperties() {
        return textProperties;
    }

    @Override
    public LoExtTableCellGraphicProperties getGraphicProperties() {
        return loExtGraphicProperties;
    }

    @Override
    public void writeObject(SerializationHandler output)
        throws SAXException {

        SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
        writeAttributes(output);
        tableCellProperties.writeObject(output);
        paragraphProperties.writeObject(output);
        textProperties.writeObject(output);
        loExtGraphicProperties.writeObject(output);
        writeMapStyleList(output);
        SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
    }

    @Override
    public void mergeAttrs(StyleBase styleBase) {
        if(styleBase instanceof IParagraphProperties) {
            getParagraphProperties().mergeAttrs(((IParagraphProperties)styleBase).getParagraphProperties());
        }
        if(styleBase instanceof ITextProperties) {
            getTextProperties().mergeAttrs(((ITextProperties)styleBase).getTextProperties());
        }
        if(styleBase instanceof IGraphicProperties) {
            getGraphicProperties().mergeAttrs(((IGraphicProperties)styleBase).getGraphicProperties());
        }
        if(styleBase instanceof StyleTableCell) {
            getTableCellProperties().mergeAttrs(((StyleTableCell)styleBase).getTableCellProperties());
        }
    }

    @Override
    public void applyAttrs(StyleManager styleManager, JSONObject attrs)
        throws JSONException {

        final String documentType = styleManager.getDocumentType();
        getParagraphProperties().applyAttrs(styleManager, attrs);
        getTextProperties().applyAttrs(styleManager, attrs);
        if(documentType.equals("text")) {
            // text is sending border attributes via table family
            getTableCellProperties()._applyAttrs(attrs.optJSONObject(OCKey.TABLE.value()));
            getTableCellProperties()._applyAttrs(attrs.optJSONObject(OCKey.CELL.value()));
        }
        else {
            getTableCellProperties().applyAttrs(styleManager, attrs);
        }
        getGraphicProperties().applyAttrs(styleManager, attrs);
        if(documentType.equals("presentation")) {
            final AttributesImpl cellAttributes = tableCellProperties.getAttributes();
            final AttributesImpl paragraphAttributes = paragraphProperties.getAttributes();
            final AttributeImpl borderTop = cellAttributes.remove("fo:border-top");
            if(borderTop!=null) {
                paragraphAttributes.setValue(divideInHalf(borderTop));
            }
            final AttributeImpl borderLeft = cellAttributes.remove("fo:border-left");
            if(borderLeft!=null) {
                paragraphAttributes.setValue(divideInHalf(borderLeft));
            }
            final AttributeImpl borderRight = cellAttributes.remove("fo:border-right");
            if(borderRight!=null) {
                paragraphAttributes.setValue(divideInHalf(borderRight));
            }
            final AttributeImpl borderBottom = cellAttributes.remove("fo:border-bottom");
            if(borderBottom!=null) {
                paragraphAttributes.setValue(divideInHalf(borderBottom));
            }
            // DOCS-3091 -> in presentations the vertical align is saved as graphic property, the cell property can be removed
            tableCellProperties.getAttributes().remove("style:vertical-align");
        }
        else {
            // DOCS-3091 -> only in presentations the vertical align is saved as graphic property
            getGraphicProperties().getAttributes().remove("draw:textarea-vertical-align");
        }
        final JSONObject cellAttrs = attrs.optJSONObject(OCKey.CELL.value());
        if(cellAttrs!=null) {
            final String formatCode = cellAttrs.optString(OCKey.FORMAT_CODE.value());
            if(!formatCode.isEmpty()) {
                attributes.setValue(Namespaces.STYLE, "data-style-name", "style:data-style-name",
                    styleManager.applyDataStyle(formatCode, null, cellAttrs.optInt(OCKey.FORMAT_ID.value(), -1), false, isAutoStyle(), isContentStyle()));
            }
            else {
                attributes.remove("style:date-style-name");
            }
        }
    }

    private static AttributeImpl divideInHalf(AttributeImpl source) {
        final Border border = new Border();
        border.applyFoBorder(source.getValue());
        final Integer width = border.getWidth();
        if(width!=null) {
            border.setWidth(Double.valueOf(((width+0.5)/2)).intValue());
        }
        return new AttributeImpl(source.getUri(), source.getQName(), source.getLocalName(), border.toString());
    }

    @Override
    public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
        Object alignment = null;
        paragraphProperties.createAttrs(styleManager, isContentStyle(), attrs);
        alignment = attrs.remove(OCKey.PARAGRAPH.value(), OCKey.ALIGNMENT.value());
        textProperties.createAttrs(styleManager, isContentStyle(), attrs);
        if(alignment!=null) {
            attrs.getMap(OCKey.CELL.value(), true).put(OCKey.ALIGN_HOR.value(), alignment);
        }
        tableCellProperties.createAttrs(styleManager, isContentStyle(), attrs);
        final String dataStyleName = attributes.getValue("style:data-style-name");
        if(dataStyleName!=null) {
            String formatCode = "";
            final StyleBase dataStyle = styleManager.getStyle(dataStyleName, StyleFamily.DATA_STYLE, isContentStyle());
            if(dataStyle instanceof NumberStyleBase) {
                formatCode = ((NumberStyleBase)dataStyle).getFormat(styleManager, null, isContentStyle());
            }
            attrs.getMap(OCKey.CELL.value(), true).put(OCKey.FORMAT_CODE.value(), formatCode);
        }
        if (styleManager.getDocumentType().equals("presentation")) {
            loExtGraphicProperties.createAttrs(styleManager, isContentStyle(), attrs);
            final OpAttrs paragraphAttrs = attrs.getMap(OCKey.PARAGRAPH.value(), false);
            if(paragraphAttrs!=null) {
                final Object borderTop = paragraphAttrs.remove(OCKey.BORDER_TOP.value());
                if(borderTop!=null) {
                    attrs.getMap(OCKey.CELL.value(), true).put(OCKey.BORDER_TOP.value(), doubleWidth(borderTop));
                }
                final Object borderLeft = paragraphAttrs.remove(OCKey.BORDER_LEFT.value());
                if(borderLeft!=null) {
                    attrs.getMap(OCKey.CELL.value(), true).put(OCKey.BORDER_LEFT.value(), doubleWidth(borderLeft));
                }
                final Object borderRight = paragraphAttrs.remove(OCKey.BORDER_RIGHT.value());
                if(borderRight!=null) {
                    attrs.getMap(OCKey.CELL.value(), true).put(OCKey.BORDER_RIGHT.value(), doubleWidth(borderRight));
                }
                final Object borderBottom = paragraphAttrs.remove(OCKey.BORDER_BOTTOM.value());
                if(borderBottom!=null) {
                    attrs.getMap(OCKey.CELL.value(), true).put(OCKey.BORDER_BOTTOM.value(), doubleWidth(borderBottom));
                }
                final Object borderInside = paragraphAttrs.remove(OCKey.BORDER_INSIDE.value());
                if(borderInside!=null) {
                    attrs.getMap(OCKey.CELL.value(), true).put(OCKey.BORDER_INSIDE_HOR.value(), doubleWidth(borderInside));
                    attrs.getMap(OCKey.CELL.value(), true).put(OCKey.BORDER_INSIDE_VERT.value(), doubleWidth(borderInside));
                }
            }
        }
    }

    private static Object doubleWidth(Object source)  {
        if(source instanceof Map) {
            final Object width = ((Map)source).get(OCKey.WIDTH.value());
            if(width instanceof Number) {
                final Map<String, Object> dest = new HashMap<String, Object>(((Map) source).size());
                StyleManager.deepCopy((Map)source, dest);
                dest.put(OCKey.WIDTH.value(), ((Number)width).intValue() * 2);
                return dest;
            }
        }
        return source;
    }

    @Override
    protected int _hashCode() {

        int result = 0;

        result += tableCellProperties.hashCode();
        result += paragraphProperties.hashCode();
        result += textProperties.hashCode();
        result += loExtGraphicProperties.hashCode();

        return result;
    }

    @Override
    protected boolean _equals(StyleBase e) {
        final StyleTableCell other = (StyleTableCell)e;
        if(!tableCellProperties.equals(other.tableCellProperties)) {
            return false;
        }
        if(!paragraphProperties.equals(other.paragraphProperties)) {
            return false;
        }
        if(!textProperties.equals(other.textProperties)) {
            return false;
        }
        if(!loExtGraphicProperties.equals(other.loExtGraphicProperties)) {
            return false;
        }
        return true;
    }

    @Override
    public StyleTableCell clone() {
        final StyleTableCell clone = (StyleTableCell)_clone();
        clone.tableCellProperties = tableCellProperties.clone();
        clone.paragraphProperties = paragraphProperties.clone();
        clone.textProperties = textProperties.clone();
        clone.loExtGraphicProperties = loExtGraphicProperties.clone();
        return clone;
    }
}
