/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Iterator;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Length;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.listlevel.ListLevelLabelAlignment;
import com.openexchange.office.filter.odf.listlevel.ListLevelProperties;
import com.openexchange.office.filter.odf.listlevel.ListLevelStyleBullet;
import com.openexchange.office.filter.odf.listlevel.ListLevelStyleEntry;
import com.openexchange.office.filter.odf.listlevel.ListLevelStyleImage;
import com.openexchange.office.filter.odf.listlevel.ListLevelStyleNumber;
import com.openexchange.office.filter.odf.properties.PropertyHelper;
import com.openexchange.office.filter.odf.properties.StylePropertiesBase;
import com.openexchange.office.filter.odf.properties.TextProperties;

public class TextListStyle extends StyleBase {

	private DLList<IElementWriter> content;

	public TextListStyle(String name, boolean automaticStyle, boolean contentStyle) {
		super(null, name, automaticStyle, contentStyle);
	}

	public TextListStyle(String name, AttributesImpl attributesImpl, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, false, automaticStyle, contentStyle);
	}

	@Override
    public StyleFamily getFamily() {
		return StyleFamily.LIST_STYLE;
	}

	@Override
	public String getQName() {
		return "text:list-style";
	}

	@Override
	public String getLocalName() {
		return "list-style";
	}

	@Override
	public String getNamespace() {
		return Namespaces.TEXT;
	}

	public DLList<IElementWriter> getContent() {
		if(content==null) {
			content = new DLList<IElementWriter>();
		}
		return content;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		final Iterator<IElementWriter> iter = getContent().iterator();
		while(iter.hasNext()) {
			iter.next().writeObject(output);
		}
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

	/*
	 * level starting from 1 ->
	 */
	public ListLevelStyleEntry getListLevelEntry(int level) {
	    for(IElementWriter e:getContent()) {
	        if(e instanceof ListLevelStyleEntry) {
	            if(((ListLevelStyleEntry)e).getListLevel(-1)==level) {
	                return (ListLevelStyleEntry)e;
	            }
	        }
	    }
	    return null;
	}

	// to be used only for styles that are not inserted yet ..
	public void setListLevelEntry(ListLevelStyleEntry newEntry) {
	    final int level = newEntry.getListLevel(-1);
	    if(level==-1) {
	        return;
	    }
	    final DLList<IElementWriter> c = getContent();
	    DLNode<IElementWriter> node = null;
        for(int i=0; i<c.size(); i++) {
            final DLNode<IElementWriter> n = c.getNode(i);
            final IElementWriter o = n.getData();
            if(o instanceof ListLevelStyleEntry) {
                if(((ListLevelStyleEntry)o).getListLevel(-1)==level) {
                    node = n;
                    break;
                }
            }
        }
        if(node!=null) {
            node.setData(newEntry);
        }
        else {
            c.add(newEntry);
        }
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		//
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject listDefinition) {
        for (int i = 0; i < 9; i++) {
        	addListDefinition(listDefinition.optJSONObject(OCKey.LIST_LEVEL.value() + i), i);
        }
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		final String consecutiveNumbering = attributes.getValue("text:consecutive-numbering");
		if(StringUtils.isNotEmpty(consecutiveNumbering)) {
			attrs.put("listUnifiedNumbering", Boolean.parseBoolean(consecutiveNumbering));
		}
		if(content!=null) {
			final Iterator<IElementWriter> listStyleIter = content.iterator();
			while(listStyleIter.hasNext()) {
				final IElementWriter listStyleEntry = listStyleIter.next();
				if(listStyleEntry instanceof StylePropertiesBase) {
					((StylePropertiesBase)listStyleEntry).createAttrs(styleManager, isContentStyle(), attrs);
				}
			}
		}
	}

	/*
	 * level from 1 to 10 is valid
	 */
	public static OpAttrs getCreatePresentationListStyleTypeMapLevel(String listStyleTypeKey, int level, boolean forceCreate, OpAttrs attrs) {
	    final OpAttrs listStyleTypeMap = getCreatePresentationListStyleTypeMap(listStyleTypeKey, forceCreate, attrs);
	    OpAttrs listStyleTypeMapLevel = null;
	    if(listStyleTypeMap!=null) {
	        final String levelString = "l" +  Integer.valueOf(level).toString();
	        listStyleTypeMapLevel = listStyleTypeMap.getMap(levelString, forceCreate);
	    }
	    return listStyleTypeMapLevel;
	}

	public static OpAttrs getCreatePresentationListStyleTypeMap(String listStyleTypeKey, boolean forceCreate, OpAttrs attrs) {
        final OpAttrs listStyles = attrs.getMap(OCKey.LIST_STYLES.value(), forceCreate);
        OpAttrs listStyleTypeMap = null;
        if(listStyles!=null) {
            listStyleTypeMap = listStyles.getMap(listStyleTypeKey, forceCreate);
        }
        return listStyleTypeMap;
	}

	/*
	 * maxLevel, index starts from 1
	 */
	public void createPresentationListStyleDefinition(StyleManager styleManager, String listStyleType, int maxLevel, OpAttrs attrs) {
        if(content!=null) {
    	    final Iterator<IElementWriter> listStyleIter = content.iterator();
            while(listStyleIter.hasNext()) {
                final IElementWriter listStyleEntry = listStyleIter.next();
                if(listStyleEntry instanceof ListLevelStyleEntry) {
                    final Integer listLevel = ((ListLevelStyleEntry)listStyleEntry).getListLevel(0);
                    if(listLevel>=1&&listLevel<=maxLevel) {
                        final String lName = "l" + Integer.toString(listLevel);
                        final OpAttrs listStyleTypeMapLevel = getCreatePresentationListStyleTypeMap(listStyleType, true, attrs).getMap(lName, true);
                        ((ListLevelStyleEntry)listStyleEntry).createPresentationAttrs(styleManager, listStyleType, false, listStyleTypeMapLevel.getMap(OCKey.PARAGRAPH.value(), true));
                    }
                }
            }
        }
	}

	public void applyPresentationListStyleAttributes(StyleManager styleManager, JSONObject attrs, int listLevel)
	    throws JSONException {

	    final DLList<IElementWriter> listLevelContainer = getContent();
	    DLNode<IElementWriter> refNode = null;
	    final Iterator<DLNode<IElementWriter>> listLevelNodeIter = listLevelContainer.getNodeIterator();
	    while(listLevelNodeIter.hasNext()) {
	        final DLNode<IElementWriter> e = listLevelNodeIter.next();
	        final IElementWriter writer = e.getData();
	        if(writer instanceof ListLevelStyleEntry) {
	            final Integer l = ((ListLevelStyleEntry)writer).getListLevel(null);
	            if(l!=null) {
	                if(l.intValue()==listLevel) {
	                    refNode = e;
	                    break;
	                }
	                else if(l.intValue()<listLevel) {
	                    if(refNode==null) {
	                        refNode = e;
	                    }
	                    else if(((ListLevelStyleEntry)refNode.getData()).getListLevel(0)<l) {
	                        refNode = e;
	                    }
	                }
	            }
	        }
	    }
	    if(refNode==null||((ListLevelStyleEntry)refNode.getData()).getListLevel(0)!=listLevel) {
	        final AttributesImpl attributesImpl = new AttributesImpl();
	        attributesImpl.setValue(Namespaces.STYLE, "num-format", "style:num-format", "");
	        final ListLevelStyleNumber newListLevel = new ListLevelStyleNumber(attributesImpl);
	        newListLevel.setListLevel(Integer.valueOf(listLevel));
	        final DLNode<IElementWriter> newListLevelNode = new DLNode<IElementWriter>(newListLevel);
	        if(refNode==null) {
	            listLevelContainer.addFirstNode(newListLevelNode);
	        }
	        else {
	            listLevelContainer.addNode(refNode, newListLevelNode, false);
	        }
	        refNode = newListLevelNode;
	    }
	    final JSONObject paraAttrs = attrs.optJSONObject(OCKey.PARAGRAPH.value());
	    if(paraAttrs!=null&&!paraAttrs.isEmpty()) {
	        ListLevelStyleEntry listLevelStyleEntry = (ListLevelStyleEntry)refNode.getData();
	        final Object bullet = paraAttrs.optJSONObject(OCKey.BULLET.value());
	        if(bullet!=null) {
	            final Object buType = bullet instanceof JSONObject ? ((JSONObject)bullet).opt(OCKey.TYPE.value()) : "none";
	            if(buType!=null) {
	                final AttributesImpl attributesImpl = new AttributesImpl();
                    if(buType.equals("character")) {
                        if(!(listLevelStyleEntry instanceof ListLevelStyleBullet)) {
                            listLevelStyleEntry = new ListLevelStyleBullet(attributesImpl);
                        }
                    }
                    else if(buType.equals("numbering")) {
                        if(!(listLevelStyleEntry instanceof ListLevelStyleNumber)) {
                            listLevelStyleEntry = new ListLevelStyleNumber(attributesImpl);
                        }
                    }
                    else if(buType.equals("bitmap")) {
                        if(!(listLevelStyleEntry instanceof ListLevelStyleImage)) {
                            listLevelStyleEntry = new ListLevelStyleImage(attributesImpl);
                        }
                    }
                    else {  // "none"
                        attributesImpl.setValue(Namespaces.STYLE, "num-format", "style:num-format", "");
                        listLevelStyleEntry = new ListLevelStyleNumber(attributesImpl);
                    }
                    listLevelStyleEntry.setListLevel(listLevel);
                    listLevelStyleEntry.setListLevelProperties(new ListLevelProperties(new AttributesImpl()));
                    listLevelStyleEntry.setTextProperties(new TextProperties(new AttributesImpl()));
                    refNode.setData(listLevelStyleEntry);
	            }
	        }
	        listLevelStyleEntry.applyPresentationAttrs(styleManager, paraAttrs);
	    }
	}

	@Override
	protected int _hashCode() {
        final int prime = 31;
        int result = 1;

        DLNode<IElementWriter> node = getContent().getFirstNode();
        while(node!=null) {
            result = prime * result + node.getData().hashCode();
            node = node.getNext();
        }
        return result;
	}

	@Override
	protected boolean _equals(StyleBase e) {
		final TextListStyle other = (TextListStyle)e;

		final DLList<IElementWriter> otherContent = other.content;
		if (this.content == otherContent) {
            return true;
		}
		if (this.content == null) {
		    return false;
		}
        if (otherContent == null) {
            return false;
        }
        DLNode<IElementWriter> node = this.content.getFirstNode();
        DLNode<IElementWriter> otherNode = otherContent.getFirstNode();
        while(node!=null&&otherNode!=null) {
            if(!node.getData().equals(otherNode.getData())) {
                return false;
            }
            node = node.getNext();
            otherNode = otherNode.getNext();
        }
        if(node!=null||otherNode!=null) {
            return false;
        }
        return true;
	}

	@Override
	public TextListStyle clone() {
	    final TextListStyle clone = (TextListStyle)_clone();
	    final DLList<IElementWriter> cloneContent = new DLList<IElementWriter>();
	    clone.content = cloneContent;
	    final Iterator<IElementWriter> iter = getContent().iterator();
	    while(iter.hasNext()) {
	        final IElementWriter o = iter.next();
	        if(o instanceof ListLevelStyleEntry) {
	            cloneContent.add(((ListLevelStyleEntry)o).clone());
	        }
	        else {
	            cloneContent.add(o);
	        }
	    }
		return clone;
	}

	public void addListDefinition(JSONObject listLevelDefinition, int listLevel) {

		if (listLevelDefinition != null) {
			ListLevelStyleEntry listLevelStyle = null;

			// numberFormat: One of 'none', 'bullet', 'decimal', 'lowerRoman', 'upperRoman', 'lowerLetter', or 'upperLetter'.
			String numberFormat = listLevelDefinition.optString(OCKey.NUMBER_FORMAT.value());
			String levelText = listLevelDefinition.optString(OCKey.LEVEL_TEXT.value());
			String numPrefix = null;
			String numSuffix = null;
			if (numberFormat.equals("bullet")) {
				// if there is also a suffix appended
				if (levelText.length() > 1) {
					// num-suffix
					numSuffix = levelText.substring(1);
					// bullet-char
					levelText = levelText.substring(0, 1);
					// ToDo: API FIX to split prefix & suffix from bullet char, a single levelText will not be able to round-trip
				}
				String levelPicBulletUri = listLevelDefinition.optString(OCKey.LEVEL_PIC_BULLET_URI.value());
				if (StringUtils.isNotEmpty(levelPicBulletUri)) {
					listLevelStyle = createListLevelStyleImage(listLevel, levelPicBulletUri);
				} else {
					listLevelStyle = new ListLevelStyleBullet(new AttributesImpl());
					listLevelStyle.getAttributes().setValue(Namespaces.TEXT, "bullet-char", "text:bullet-char", levelText);
					listLevelStyle.getAttributes().setValue(Namespaces.TEXT, "level", "text:level", Integer.toString(listLevel + 1));
				}
			} else { // *** NUMBERED LIST ***
				final AttributesImpl attrs = new AttributesImpl();
				listLevelStyle = new ListLevelStyleNumber(attrs);
				attrs.setValue(Namespaces.STYLE, "num-format", "style:num-format", getNumFormat(numberFormat));
				attrs.setIntValue(Namespaces.TEXT, "level", "text:level", listLevel + 1);
				attrs.setValue(Namespaces.TEXT, "display-levels", "text:display-levels", Integer.toString(countOccurrences(levelText, '%')));

				// if there is prefix
				if (!levelText.startsWith("%")) {
					// num-prefix
					int prefixEnd = levelText.indexOf('%');
					numPrefix = levelText.substring(0, prefixEnd);
					levelText = levelText.substring(prefixEnd);
				}
				// num-suffix
				int suffixStart = levelText.lastIndexOf('%') + 2;
				if (levelText.length() >= suffixStart) {
					numSuffix = levelText.substring(suffixStart);
				}
				int listStartValue = listLevelDefinition.optInt(OCKey.LIST_START_VALUE.value(), -1);
				if (listStartValue != -1) {
					attrs.setValue(Namespaces.TEXT, "start-value", "text:start-value", Integer.toString(listStartValue));
				}
	       	}
	       	if (StringUtils.isNotEmpty(numPrefix)) {
	       		listLevelStyle.getAttributes().setValue(Namespaces.STYLE, "num-prefix", "style:num-prefix", numPrefix);
	       	}
	       	if (StringUtils.isNotEmpty(numSuffix)) {
	       		listLevelStyle.getAttributes().setValue(Namespaces.STYLE, "num-suffix", "style:num-suffix", numSuffix);
	       	}
	       	getContent().add(listLevelStyle);
	       	// Add common list style properties
	       	addCommonListStyles(listLevelStyle, listLevelDefinition);
		}
	}

	private static int countOccurrences(String haystack, char needle) {
        int count = 0;
        for (int i = 0; i < haystack.length(); i++) {
            if (haystack.charAt(i) == needle) {
                count++;
            }
        }
        return count;
    }

	private static ListLevelStyleImage createListLevelStyleImage(int listLevel, String levelPicBulletUri) {
		final AttributesImpl attributes = new AttributesImpl();
		final ListLevelStyleImage listLevelStyle = new ListLevelStyleImage(attributes);
		attributes.setIntValue(Namespaces.TEXT, "level", "text:level", listLevel + 1);
        if(StringUtils.isNotEmpty(levelPicBulletUri)) {
            attributes.setValue(Namespaces.XLINK, "href", "xlink:href", levelPicBulletUri);
            attributes.setValue(Namespaces.XLINK, "actuate", "xlink:actuate", "onLoad");
            attributes.setValue(Namespaces.XLINK, "show", "xlink:show", "embed");
            attributes.setValue(Namespaces.XLINK, "type", "xlink:type", "simple");
        }
        return listLevelStyle;
    }

	private void addCommonListStyles(ListLevelStyleEntry listLevelStyle, JSONObject listLevelDefinition) {
		final AttributesImpl attrs = new AttributesImpl();
		final ListLevelProperties styleListLevelProperties = new ListLevelProperties(attrs);
		listLevelStyle.setListLevelProperties(styleListLevelProperties);

        addListLabelAlignment(styleListLevelProperties, listLevelDefinition);

        if (listLevelDefinition.has(OCKey.HEIGHT.value())) {
        	attrs.setValue(Namespaces.FO, "height", "fo:height", listLevelDefinition.optInt(OCKey.HEIGHT.value()) / 100 + "mm");
        }
        if (listLevelDefinition.has(OCKey.TEXT_ALIGN.value())) {
        	attrs.setValue(Namespaces.FO, "text-align", "fo:text-align", listLevelDefinition.optString(OCKey.TEXT_ALIGN.value()));
        }
        if (listLevelDefinition.has(OCKey.WIDTH.value())) {
        	attrs.setValue(Namespaces.FO, "width", "fo:width", listLevelDefinition.optInt(OCKey.WIDTH.value()) / 100 + "mm");
        }
        if (listLevelDefinition.has(OCKey.FONT_NAME.value())) {
        	attrs.setValue(Namespaces.STYLE, "font-name", "style:font-name", listLevelDefinition.optString(OCKey.FONT_NAME.value()));
        }
        if (listLevelDefinition.has(OCKey.VERTICAL_POS.value())) {
        	attrs.setValue(Namespaces.STYLE, "verical-pos", "style:vertical-pos", listLevelDefinition.optString(OCKey.VERTICAL_POS.value()));
        }
        if (listLevelDefinition.has(OCKey.VERTICAL_REL.value())) {
        	attrs.setValue(Namespaces.STYLE, "vertical-rel", "style:vertical-rel", listLevelDefinition.optString(OCKey.VERTICAL_REL.value()));
        }
        if (listLevelDefinition.has(OCKey.Y.value())) {
        	attrs.setValue(Namespaces.SVG, "y", "svg:y", listLevelDefinition.optString(OCKey.Y.value()));
        }
        if (listLevelDefinition.has(OCKey.LIST_LEVEL_POSITION_AND_SPACE_MODE.value())) {
        	attrs.setValue(Namespaces.TEXT, "list-level-position-and-space-mode", "text:list-level-position-and-space-mode", listLevelDefinition.optString(OCKey.LIST_LEVEL_POSITION_AND_SPACE_MODE.value()));
        }
        if (listLevelDefinition.has(OCKey.MIN_LABEL_DISTANCE.value())) {
        	attrs.setValue(Namespaces.TEXT, "min-label-distance", "text:min-label-distance", listLevelDefinition.optInt(OCKey.MIN_LABEL_DISTANCE.value()) / 100 + "mm");
        }
        if (listLevelDefinition.has(OCKey.MIN_LABEL_WIDTH.value())) {
        	attrs.setValue(Namespaces.TEXT, "min-label-width", "text:min-label-width", listLevelDefinition.optInt(OCKey.MIN_LABEL_WIDTH.value()) / 100 + "mm");
        }
        if (listLevelDefinition.has(OCKey.SPACE_BEFORE.value())) {
        	attrs.setValue(Namespaces.TEXT, "space-before", "text:space-before", listLevelDefinition.optInt(OCKey.SPACE_BEFORE.value()) / 100 + "mm");
        }
    }

    private static void addListLabelAlignment(ListLevelProperties styleListLevelProperties, JSONObject listLevelDefinition) {
    	if (listLevelDefinition.has(OCKey.INDENT_LEFT.value()) || listLevelDefinition.has(OCKey.INDENT_FIRST_LINE.value())) {
    		styleListLevelProperties.getAttributes().setValue(Namespaces.TEXT, "list-level-position-and-space-mode", "text:list-level-position-and-space-mode", "label-alignment");
            String labelFollowedBy = listLevelDefinition.optString(OCKey.LABEL_FOLLOWED_BY.value(), "listtab");
            final AttributesImpl labelAttributes = new AttributesImpl();
            final ListLevelLabelAlignment levelAlignment = new ListLevelLabelAlignment(labelAttributes);
            labelAttributes.setValue(Namespaces.TEXT, "label-followed-by", "text:label-followed-by", labelFollowedBy);
            styleListLevelProperties.setListLevelLabelAlignment(levelAlignment);

            double indentLeft = 0;
            if (listLevelDefinition.has(OCKey.INDENT_LEFT.value())) {
                indentLeft = listLevelDefinition.optInt(OCKey.INDENT_LEFT.value()) / 100.0;
                labelAttributes.setValue(Namespaces.FO, "margin-left", "fo:margin-left", indentLeft + "mm");
            }
            if (listLevelDefinition.has(OCKey.INDENT_FIRST_LINE.value())) {
                int indentFirstLine = listLevelDefinition.optInt(OCKey.INDENT_FIRST_LINE.value());
                labelAttributes.setValue(Namespaces.FO, "text-indent", "fo:text-indent", indentFirstLine / 100.0 + "mm");
            }
            if (listLevelDefinition.has(OCKey.TAB_STOP_POSITION.value())) {
                indentLeft = listLevelDefinition.optInt(OCKey.TAB_STOP_POSITION.value()) / 100.0;
            }
            labelAttributes.setValue(Namespaces.TEXT, "list-tab-stop-position", "text:list-tab-stop-position", indentLeft + "mm");
            labelAttributes.setValue(Namespaces.TEXT, "label-followed-by", "text:label-followed-by", "listtab");
        }
    }

    public static String getNumFormat(String numberFormat) {
        String numFormat = ""; // "none" is nothing set
        if (numberFormat.equals("decimal")) {
            numFormat = "1";
        } else if (numberFormat.equals("lowerRoman")) {
            numFormat = "i";
        } else if (numberFormat.equals("upperRoman")) {
            numFormat = "I";
        } else if (numberFormat.equals("lowerLetter")) {
            numFormat = "a";
        } else if (numberFormat.equals("upperLetter")) {
            numFormat = "A";
        }
        return numFormat;
    }

    public static void createListLevelDefinition(ListLevelStyleEntry listLevelStyle, OpAttrs attrs, int listLevel)  {
        final Map<String, Object> listLevelDefinition = attrs.getMap(OCKey.LIST_LEVEL.value() + Integer.toString(listLevel-1), true);
        if (listLevelStyle instanceof ListLevelStyleNumber) {
            ListLevelStyleNumber listLevelNumberStyle = (ListLevelStyleNumber) listLevelStyle;
            listLevelDefinition.put(OCKey.LEVEL_TEXT.value(), getLabel(listLevelNumberStyle, listLevel-1));
            final String startValue = listLevelStyle.getAttribute("text:start-value");
            if (startValue!=null) {
                listLevelDefinition.put(OCKey.LIST_START_VALUE.value(), Integer.parseInt(startValue));
            }
            // There is always the number format set
            listLevelDefinition.put(OCKey.NUMBER_FORMAT.value(), getNumberFormat(listLevelNumberStyle));
        } else if (listLevelStyle instanceof ListLevelStyleBullet) {
            ListLevelStyleBullet listLevelBulletStyle = (ListLevelStyleBullet) listLevelStyle;
            listLevelDefinition.put(OCKey.LEVEL_TEXT.value(), getLabel(listLevelBulletStyle, listLevel));
            final String buRelSize = listLevelStyle.getAttribute("text:bullet-relative-size");
            if (buRelSize!=null) {
                listLevelDefinition.put("bulletRelativeSize", buRelSize);
            }
            listLevelDefinition.put(OCKey.NUMBER_FORMAT.value(), "bullet");
        } else if (listLevelStyle instanceof ListLevelStyleImage) {
            listLevelDefinition.put(OCKey.LEVEL_PIC_BULLET_URI.value(), listLevelStyle.getAttribute("xlink:href"));
            listLevelDefinition.put(OCKey.NUMBER_FORMAT.value(), "bullet");
        }
        final String styleName = listLevelStyle.getAttribute("text:style-name");
        if(styleName!=null) {
            listLevelDefinition.put(OCKey.STYLE_ID.value(), styleName);
        }

        final ListLevelProperties listLevelProperties = listLevelStyle.getListLevelProperties(false);
        if(listLevelProperties!=null) {
            final String height = listLevelProperties.getAttribute("fo:height");
            if(height!=null) {
                listLevelDefinition.put(OCKey.HEIGHT.value(), AttributesImpl.normalizeLength(height));
            }
            final String textAlign = listLevelProperties.getAttribute("fo:text-align");
            if(textAlign!=null) {
                listLevelDefinition.put(OCKey.TEXT_ALIGN.value(), PropertyHelper.mapFoTextAlign(textAlign));
            }
            final String width = listLevelProperties.getAttribute("fo:width");
            if(width!=null) {
                listLevelDefinition.put(OCKey.WIDTH.value(), AttributesImpl.normalizeLength(width));
            }
            final String fontName = listLevelProperties.getAttribute("style:font-name");
            if(fontName!=null) {
                listLevelDefinition.put(OCKey.FONT_NAME.value(), fontName);
            }
            final String verticalPos = listLevelProperties.getAttribute("style:vertical-pos");
            if(verticalPos!=null) {
                listLevelDefinition.put(OCKey.VERTICAL_POS.value(), verticalPos);
            }
            final String verticalRel = listLevelProperties.getAttribute("style:vertical-rel");
            if(verticalRel!=null) {
                listLevelDefinition.put(OCKey.VERTICAL_REL.value(), verticalRel);
            }
            final String svgY = listLevelProperties.getAttribute("svg:y");
            if(svgY!=null) {
                listLevelDefinition.put(OCKey.Y.value(), svgY);
            }
            final String positionAndSpaceMode = listLevelProperties.getAttribute("text:list-level-position-and-space-mode");
            if(positionAndSpaceMode!=null) {
                listLevelDefinition.put(OCKey.LIST_LEVEL_POSITION_AND_SPACE_MODE.value(), positionAndSpaceMode);
            }
            final String minLabelDistance = listLevelProperties.getAttribute("text:min-label-distance");
            if(StringUtils.isNotEmpty(minLabelDistance)) {
                listLevelDefinition.put(OCKey.MIN_LABEL_DISTANCE.value(), AttributesImpl.normalizeLength(minLabelDistance));
            }
            final String minLabelWidth = listLevelProperties.getAttribute("text:min-label-width");
            if (StringUtils.isNotEmpty(minLabelWidth)) {
                listLevelDefinition.put(OCKey.MIN_LABEL_WIDTH.value(), AttributesImpl.normalizeLength(minLabelWidth));
            }
            final String spaceBeforeValue = listLevelProperties.getAttribute("text:space-before");
            if(StringUtils.isNotEmpty(spaceBeforeValue)) {
                listLevelDefinition.put(OCKey.SPACE_BEFORE.value(), AttributesImpl.normalizeLength(spaceBeforeValue));
            }

            // Mapping list XML ODF 1.1 to ODF 1.2: Adding @text:min-label-width & @text:space-before to margin-left
            mapIndent(minLabelWidth, spaceBeforeValue, null, listLevelDefinition);

            final ListLevelLabelAlignment listLevelLabelAlignment = listLevelProperties.getListLevelLabelAlignment(false);
            if(listLevelLabelAlignment!=null) {
                final String marginLeft = listLevelLabelAlignment.getAttribute("fo:margin-left");
                if(StringUtils.isNotEmpty(marginLeft)) {
                    listLevelDefinition.put(OCKey.INDENT_LEFT.value(), AttributesImpl.normalizeLength(marginLeft));
                } else {
                    mapIndent(minLabelWidth, spaceBeforeValue, null, listLevelDefinition);
                }
                final String textIndent = listLevelLabelAlignment.getAttribute("fo:text-indent");
                if(StringUtils.isNotEmpty(textIndent)) {
                    listLevelDefinition.put(OCKey.INDENT_FIRST_LINE.value(), AttributesImpl.normalizeLength(textIndent));
                }
                final String listTabStopPosition = listLevelLabelAlignment.getAttribute("text:list-tab-stop-position");
                if(StringUtils.isNotEmpty(listTabStopPosition)) {
                    listLevelDefinition.put(OCKey.TAB_STOP_POSITION.value(), AttributesImpl.normalizeLength(listTabStopPosition));
                }
                final String labelFollowedBy = listLevelLabelAlignment.getAttribute("text:label-followed-by");
                if(labelFollowedBy!=null) {
                    listLevelDefinition.put(OCKey.LABEL_FOLLOWED_BY.value(), labelFollowedBy);
                }
            }
        }
    }

    private static String getLabel(StylePropertiesBase listLevelStyle, int listLevel) {
        StringBuilder levelText = new StringBuilder();

        // creating label prefix
        String labelPrefix = listLevelStyle.getAttribute("style:num-prefix");
        if (StringUtils.isNotEmpty(labelPrefix)) {
            levelText.append(labelPrefix);
        }

        // creating label number
        if (listLevelStyle instanceof ListLevelStyleNumber) {
            String displayLevels = listLevelStyle.getAttribute("text:display-levels");
            if (StringUtils.isNotEmpty(displayLevels)) {
                int showLevels = Integer.parseInt(displayLevels);
                // Creating the label, in ODF always adding the low levelText first, adding each follow up level for display level
                // Custom string with one of the placeholders from '%1' to '%9') for numbered lists.
                for (int i = showLevels; i > 0; i--) {
                    levelText.append("%").append(listLevel + 2 - i);
                    // Although not commented in the specification a "." is being added to the text level
                    if (i != 1) {
                        levelText.append('.');
                    }
                }
            } else {
                levelText.append("%").append(listLevel + 1);
            }
            // creating label suffix
            String labelSuffix = listLevelStyle.getAttribute("style:num-suffix");
            if (StringUtils.isNotEmpty(labelSuffix)) {
                levelText.append(labelSuffix);
            }
        } else {
            String bulletChar = listLevelStyle.getAttribute("text:bullet-char");
            if (StringUtils.isNotEmpty(bulletChar)) {
                levelText.append(bulletChar);
            }
        }
        return levelText.toString();
    }

    public static void mapIndent(String minLabelWidthValue, String spaceBeforeValue, String marginLeftValue, Map<String, Object> listLevelDefinition) {
        int minLabelWidth = 0;
        boolean isValidMinLabelWidth = Length.isValid(minLabelWidthValue);
        if (isValidMinLabelWidth) {
            minLabelWidth = AttributesImpl.normalizeLength(minLabelWidthValue);
        }
        int spaceBefore = 0;
        boolean isValidSpaceBefore = Length.isValid(spaceBeforeValue);
        if (isValidSpaceBefore) {
            spaceBefore = AttributesImpl.normalizeLength(spaceBeforeValue);
        }
        int marginLeft = 0;
        boolean isMarginValid = Length.isValid(marginLeftValue);
        if(isMarginValid) {
            marginLeft = AttributesImpl.normalizeLength(marginLeftValue);
        }
        if (isValidMinLabelWidth || isValidSpaceBefore || isMarginValid) {
            listLevelDefinition.put(OCKey.INDENT_LEFT.value(), minLabelWidth + spaceBefore + marginLeft);
        }
    }

    public static void mapFirstLineIndent(String minLabelWidthValue, String spaceBeforeValue, String indentFirstLineValue, Map<String, Object> listLevelDefinition) {
        int indentFirstLine = 0;
        boolean isValidIndentFirstLine = Length.isValid(indentFirstLineValue);
        if (isValidIndentFirstLine) {
            indentFirstLine = AttributesImpl.normalizeLength(indentFirstLineValue);
        }
        int spaceBefore = 0;
        boolean isValidSpaceBefore = Length.isValid(spaceBeforeValue);
        if (isValidSpaceBefore) {
            spaceBefore = AttributesImpl.normalizeLength(spaceBeforeValue);
        }
        int minLabelWidth = 0;
        boolean isValidMinLabelWidth = Length.isValid(minLabelWidthValue);
        if(isValidMinLabelWidth) {
            minLabelWidth = AttributesImpl.normalizeLength(minLabelWidthValue);
        }
        if (isValidMinLabelWidth || isValidIndentFirstLine || isValidSpaceBefore) {
            listLevelDefinition.put(OCKey.INDENT_FIRST_LINE.value(), -minLabelWidth);
        }
    }

    public static String getNumberFormat(StylePropertiesBase listLevelStyle) {
        String numberFormat = listLevelStyle.getAttribute("style:num-format");
        String numFormat;
        if (numberFormat == null || numberFormat.isEmpty()) {
            numFormat = "none";
        } else if (numberFormat.equals("1")) {
            numFormat = "decimal";
        } else if (numberFormat.equals("i")) {
            numFormat = "lowerRoman";
        } else if (numberFormat.equals("I")) {
            numFormat = "upperRoman";
        } else if (numberFormat.equals("a")) {
            numFormat = "lowerLetter";
        } else if (numberFormat.equals("A")) {
            numFormat = "upperLetter";
        } else {
            // a value of type string 18.2. (COMPLEX NUMBERING ie. ASIAN oder ERSTENS..)
            numFormat = numberFormat;
        }
        return numFormat;
    }
}
