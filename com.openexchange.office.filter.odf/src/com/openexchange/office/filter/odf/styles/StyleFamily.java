/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

public enum StyleFamily {

    TEXT            ("text"),
    PARAGRAPH       ("paragraph"),
    SECTION         ("section"),
    RUBY            ("ruby"),
    TABLE           ("table"),
    TABLE_COLUMN    ("table-column"),
    TABLE_ROW       ("table-row"),
    TABLE_CELL      ("table-cell"),
    GRAPHIC         ("graphic"),
    PRESENTATION    ("presentation"),
    DRAWING_PAGE    ("drawing-page"),
    CHART           ("chart"),
    PAGE_LAYOUT     ("page-layout"),
    NUMBER_STYLE    ("number-style"),
    BOOLEAN_STYLE   ("boolean-style"),
    CURRENCY_STYLE  ("currency-style"),
    DATE_STYLE      ("date-style"),
    PERCENTAGE_STYLE("percentage-style"),
    TEXT_STYLE      ("text-style"),
    TIME_STYLE      ("time-style"),
    LIST_STYLE      ("list-style"),
    STROKE_DASH     ("stoke-dash"),
    FILL_IMAGE      ("fill-image"),
    TABLE_TEMPLATE  ("table-template"),
    DATA_STYLE      ("data-style"),
    MARKER          ("marker"),
    MASTER_PAGE     ("master-page"),
    OUTLINE_STYLE   ("outline-style"),
    GRADIENT        ("gradient");

    final String name;

    private StyleFamily(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
