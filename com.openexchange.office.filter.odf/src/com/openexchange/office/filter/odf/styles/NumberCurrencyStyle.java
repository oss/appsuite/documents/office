/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Currency;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.spreadsheet.FormatCode;
import com.openexchange.office.filter.core.spreadsheet.FormatCode.Entry;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.properties.NumberCurrencySymbol;
import com.openexchange.office.filter.odf.properties.NumberNumber;
import com.openexchange.office.filter.odf.properties.NumberText;
import com.openexchange.office.filter.odf.properties.PropertyHelper;
import com.openexchange.office.filter.odf.properties.TextProperties;

public class NumberCurrencyStyle extends NumberStyleBase {

	public NumberCurrencyStyle(String name, boolean automaticStyle, boolean contentStyle) {
		super(null, name, automaticStyle, contentStyle);
	}

	public NumberCurrencyStyle(String name, AttributesImpl attributesImpl, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, false, automaticStyle, contentStyle);
	}

	@Override
	public String getQName() {
		return "number:currency-style";
	}

	@Override
	public String getLocalName() {
		return "currency-style";
	}

	@Override
	public String getNamespace() {
		return Namespaces.NUMBER;
	}

	@Override
	public String getFormat(StyleManager styleManager, Map<String, String> additionalStyleProperties, boolean contentAutoStyle) {
		String result = "";
		final Iterator<IElementWriter> propertiesIter = getContent().iterator();
		while(propertiesIter.hasNext()) {
			final IElementWriter properties = propertiesIter.next();
			if (properties instanceof NumberCurrencySymbol) {
				final NumberCurrencySymbol numberCurrencySymbol = (NumberCurrencySymbol)properties;
			    result += "[$";
				result += numberCurrencySymbol.getTextContent();
				final String language = numberCurrencySymbol.getAttribute("number:language");
				final String country = numberCurrencySymbol.getAttribute("number:country");
                if(language!=null&&!language.isEmpty() ) {
                    result += "-";
                    //convert local to ms language id
                    result += PropertyHelper.getMSLangCode(language, country);
                }
                result += "]";
			} else if (properties instanceof NumberNumber) {
				result += ((NumberNumber)properties).getNumberFormat();
			} else if (properties instanceof NumberText) {
				String textcontent = ((NumberText)properties).getTextContent();
				if (textcontent == null || textcontent.length() == 0) {
					textcontent = " ";
				}
				result += textcontent;
            } else if (properties instanceof TextProperties) {
                result += ((TextProperties)properties).getColorFromElement();
            }
		}
		if(!getMapStyleList().isEmpty()) {
            result = getMapStyleList().get(0).getMapping(styleManager, contentAutoStyle, getFamily()) + ";" + result;
		}
		return result;
	}

	@Override
	public void setFormat(FormatCode formatCode, Map<String, String> additionalStyleProperties) {
	    NumberNumber number = null;
        DLNode<Entry> entry = formatCode.getEntries().getFirstNode();
        while(entry != null) {
            final Entry formatCodeEntry = entry.getData();
            if(formatCodeEntry instanceof FormatCode.TextEntry) {
                appendNumberText(formatCodeEntry.toString());
            }
            else if(formatCodeEntry instanceof FormatCode.SquareBracketEntry) {
                final String squareBracketContent = ((FormatCode.SquareBracketEntry)formatCodeEntry).getContent();
                final String color = getColorElement(squareBracketContent);
                if(getContent().isEmpty() && color != null) {
                    final TextProperties textProperties = new TextProperties(new AttributesImpl());
                    textProperties.getAttributes().setValue(Namespaces.FO, "color", "fo:color", color);
                    getContent().add(textProperties);
                } else if(squareBracketContent.charAt(0) == '$' && squareBracketContent.length() > 1){
                    final int dashPos = squareBracketContent.indexOf("-");
                    final String currencySymbol = squareBracketContent.substring(1, dashPos > 0 ? dashPos : squareBracketContent.length());
                    final String languageCode = dashPos > 0 ? squareBracketContent.substring(dashPos + 1) : "";
                    final NumberCurrencySymbol cSymbol = new NumberCurrencySymbol(new AttributesImpl());
                    final String locale = PropertyHelper.getLocaleFromLangCode(languageCode);
                    if(!locale.isEmpty()) {
                        int localeDashPos = locale.indexOf("-");
                        cSymbol.getAttributes().setValue(Namespaces.NUMBER, "language", "number:language", locale.substring(0, localeDashPos));
                        if(localeDashPos > 0) {
                            cSymbol.getAttributes().setValue(Namespaces.NUMBER, "country", "number:country", locale.substring(localeDashPos + 1));
                        }
                    }
                    cSymbol.setTextContent(currencySymbol);
                    getContent().add(cSymbol);
                }
            }
            else  if(number == null && (formatCodeEntry instanceof FormatCode.DigitPlaceholder || formatCodeEntry instanceof FormatCode.DecimalPointPlaceholder)) {
                number = new NumberNumber(new AttributesImpl());
                if(formatCode.isGrouping()) {
                    number.getAttributes().setBooleanValue(Namespaces.NUMBER, "grouping", "number:grouping", true);
                }
                number.getAttributes().setIntValue(Namespaces.NUMBER, "min-integer-digits", "number:min-integer-digits", formatCode.getIntegerDigits());
                number.getAttributes().setIntValue(Namespaces.NUMBER, "decimal-places", "number:decimal-places", formatCode.getDecimalDigits());
                final DLNode<Entry> decimalPlaceholderNode = formatCode.getDecimalPointPlaceholderNode();
                if(decimalPlaceholderNode != null) {
                    final DLNode<Entry> followerNode  = decimalPlaceholderNode.getNext();
                    if(followerNode != null && followerNode.getData() instanceof FormatCode.TextEntry) {
                        number.getAttributes().setValue(Namespaces.NUMBER, "decimal-replacement", "number:decimal-replacement", followerNode.getData().toString());
                        entry = followerNode;
                    }
                }
                getContent().add(number);
            }
            entry = entry.getNext();
        }
	}

	public String getCurrencyCode() {
		String currencyValue = null;

		final Iterator<IElementWriter> propertiesIter = getContent().iterator();
		while(propertiesIter.hasNext()) {
			final IElementWriter properties = propertiesIter.next();
			if(properties instanceof NumberCurrencySymbol) {
				currencyValue = ((NumberCurrencySymbol)properties).getTextContent();
				if(currencyValue!=null&&currencyValue.equals("DM")) {
					return "DEM";
				}
				final String language =  ((NumberCurrencySymbol)properties).getAttribute("number:language");
				final String country = ((NumberCurrencySymbol)properties).getAttribute("number:country");
				if(language!=null) {
					try {
						final Currency currency = Currency.getInstance(country!=null ? new Locale(language, country) : new Locale(language));
						if(currency!=null) {
							return currency.toString();
						}
					}
					catch(IllegalArgumentException e) {
					    //
					}
				}
				break;
			}
		}
		return currencyValue;
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
	    //
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
	    //
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		// TODO Auto-generated method stub

	}
}
