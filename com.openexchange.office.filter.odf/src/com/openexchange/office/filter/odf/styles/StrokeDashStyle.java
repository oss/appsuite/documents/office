/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributeImpl;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;

final public class StrokeDashStyle extends StyleBase {

	public StrokeDashStyle(String name) {
		super(null, name, false, false);
	}

	public StrokeDashStyle(String name, AttributesImpl attributesImpl) {
		super(name, attributesImpl, false, false, false);
	}

	@Override
    protected void writeNameAttribute(SerializationHandler output) throws SAXException {
        output.addAttribute(Namespaces.DRAW, "name", "draw:name", "", getName());
    }

	@Override
	public String getQName() {
		return "draw:stroke-dash";
	}

	@Override
	public String getLocalName() {
		return "stroke-dash";
	}

	@Override
	public String getNamespace() {
		return Namespaces.DRAW;
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		//
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs) {
		//
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeObject(SerializationHandler output) throws SAXException {
		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

	@Override
	public StyleFamily getFamily() {
		return StyleFamily.STROKE_DASH;
	}

	@Override
	public StrokeDashStyle clone() {
		return (StrokeDashStyle)_clone();
	}

	@Override
	protected int _hashCode() {
		return 0;
	}

	@Override
	protected boolean _equals(StyleBase e) {
		return true;
	}

    static public String getStrokeDash(StyleManager styleManager, String dashStyle) {
        final StrokeDashStyle strokeDashStyle = new StrokeDashStyle(styleManager.getUniqueStyleName(StyleFamily.STROKE_DASH, false));
    	double length1 = 0.050;
    	Integer dots2 = null;
    	double length2 = 0.254;

    	switch(dashStyle) {
    		default:
    		case "dotted": {
	    		break;
	    	}
	    	case "dashed": {
	    		length1 = 0.254;
	    		break;
	    	}
	    	case "dashDot": {
	    		dots2 = 1;
	    		length1 = 0.254;
	    		length2 = 0.050;
	    		break;
	    	}
	    	case "dashDotDot": {
	    		dots2 = 2;
	    		length1 = 0.254;
	    		length2 = 0.050;
	    		break;
	    	}
    	}
    	strokeDashStyle.getAttributes().setValue(Namespaces.DRAW, "display-name", "draw:display-name", dashStyle);
    	strokeDashStyle.getAttributes().setValue(Namespaces.DRAW, "style", "draw:style", "rect");
    	strokeDashStyle.getAttributes().setValue(Namespaces.DRAW, "distance", "draw:distance", "0.050cm");
		strokeDashStyle.getAttributes().setValue(Namespaces.DRAW, "dots1", "draw:dots1", "1");
		strokeDashStyle.getAttributes().setValue(Namespaces.DRAW, "dots1-length", "draw:dots1-length", Double.valueOf(length1).toString() + "cm");
		if(dots2!=null) {
			strokeDashStyle.getAttributes().setValue(Namespaces.DRAW, "dots2", "draw:dots2", "1");
			strokeDashStyle.getAttributes().setValue(Namespaces.DRAW, "dots2-length", "draw:dots2-length", Double.valueOf(length2).toString() + "cm");
		}
		final String existingStyleId = styleManager.getExistingStyleIdForStyleBase(strokeDashStyle);
		if(existingStyleId!=null) {
			return existingStyleId;
		}
		styleManager.addStyle(strokeDashStyle);
		return strokeDashStyle.getName();
    }

	static public String getStrokeDashEnumFromStyleName(StyleManager styleManager, AttributeImpl dashStyle) {
		if(dashStyle==null) {
			return "dotted";
		}
		final StyleBase styleBase = styleManager.getStyle(dashStyle.getValue(), StyleFamily.STROKE_DASH, false);
		if(!(styleBase instanceof StrokeDashStyle)) {
			return "dotted";
		}
		final int dots2 = styleBase.getInteger("draw:dots2", 0);
        if(dots2==0) {
            final String l1 = styleBase.getAttribute("draw:dots1-length");
            if(l1!=null) {
                final Integer length1 = AttributesImpl.normalizeLength(l1, true);
    			if(length1!=null&&length1>100) {
    				return "dashed";
    			}
            }
		}
		else if(dots2==1) {
			return "dashDot";
		}
		else if(dots2==2) {
			return "dashDotDot";
		}
		return "dotted";
	}
}
