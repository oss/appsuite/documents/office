/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.DrawingPageProperties;

final public class StyleDrawingPage extends StyleBase {

	private DrawingPageProperties drawingPageProperties = new DrawingPageProperties(new AttributesImpl());

	public StyleDrawingPage(String name, boolean automaticStyle, boolean contentStyle) {
		super(StyleFamily.DRAWING_PAGE, name, automaticStyle, contentStyle);
	}

	public StyleDrawingPage(String name, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle, boolean contentStyle) {
		super(name, attributesImpl, defaultStyle, automaticStyle, contentStyle);
	}

	@Override
    public StyleFamily getFamily() {
		return StyleFamily.DRAWING_PAGE;
	}

	public DrawingPageProperties getDrawingPageProperties() {
		return drawingPageProperties;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, getNamespace(), getLocalName(), getQName());
		writeAttributes(output);
		drawingPageProperties.writeObject(output);
		writeMapStyleList(output);
		SaxContextHandler.endElement(output, getNamespace(), getLocalName(), getQName());
	}

	@Override
	public void mergeAttrs(StyleBase styleBase) {
		if(styleBase instanceof StyleDrawingPage) {
			getDrawingPageProperties().mergeAttrs(((StyleDrawingPage)styleBase).getDrawingPageProperties());
		}
	}

	@Override
	public void applyAttrs(StyleManager styleManager, JSONObject attrs)
	    throws JSONException {

	    getDrawingPageProperties().applyAttrs(styleManager, attrs);
	}

	@Override
	public void createAttrs(StyleManager styleManager, OpAttrs attrs) {
		drawingPageProperties.createAttrs(styleManager, isContentStyle(), attrs);
	}

	@Override
	protected int _hashCode() {
		return drawingPageProperties.hashCode();
	}

	@Override
	protected boolean _equals(StyleBase e) {
		final StyleDrawingPage other = (StyleDrawingPage)e;
		if(!drawingPageProperties.equals(other.drawingPageProperties)) {
			return false;
		}
		return true;
	}

	@Override
	public StyleDrawingPage clone() {
		final StyleDrawingPage clone = (StyleDrawingPage)_clone();
		clone.drawingPageProperties = drawingPageProperties.clone();
		return clone;
	}
}
