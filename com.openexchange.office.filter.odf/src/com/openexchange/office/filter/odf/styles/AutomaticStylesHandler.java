/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;

public class AutomaticStylesHandler extends SaxContextHandler {

    private final StyleManager styleManager;
    private final boolean contentStyle;

    public AutomaticStylesHandler(SaxContextHandler parentContext, StyleManager styleManager, boolean contentStyle) {
    	super(parentContext);

    	this.styleManager = styleManager;
    	this.contentStyle = contentStyle;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {

    	final AttributesImpl attributesImpl = new AttributesImpl(attributes);
    	String name = StylesHandler.removeNameFromAttributes(attributesImpl);
    	if(name==null) {
    		// default styles does not have a name, we will check this...
    		if(qName.equals("style:default-style")) {
    			name = "_default";
    		}
    	}
    	if(name!=null) {
	    	switch(qName) {
				case "style:default-style" : {
					final SaxContextHandler styleHandler = StylesHandler.getStyleHandler(this, name, styleManager, attributesImpl, true, true, contentStyle);
					if(styleHandler!=null) {
						return styleHandler;
					}
					break;
				}
				case "style:style" : {
					final SaxContextHandler styleHandler = StylesHandler.getStyleHandler(this, name, styleManager, attributesImpl, false, true, contentStyle);
					if(styleHandler!=null) {
						return styleHandler;
					}
					break;
				}
				case "style:page-layout": {
					return new StylePageLayoutHandler(this, new StylePageLayout(name, attributesImpl, false, true, contentStyle), styleManager);
				}
				case "number:number-style": {
		    		return new NumberNumberStyleHandler(this, new NumberNumberStyle(name, attributesImpl, true, contentStyle), styleManager);
				}
				case "number:boolean-style": {
		    		return new NumberBooleanStyleHandler(this, new NumberBooleanStyle(name, attributesImpl, true, contentStyle), styleManager);
				}
				case "number:currency-style" : {
		    		return new NumberCurrencyStyleHandler(this, new NumberCurrencyStyle(name, attributesImpl, true, contentStyle), styleManager);
				}
				case "number:date-style" : {
		    		return new NumberDateStyleHandler(this, new NumberDateStyle(name, attributesImpl, true, contentStyle), styleManager);
				}
				case "number:percentage-style" : {
		    		return new NumberPercentageStyleHandler(this, new NumberPercentageStyle(name, attributesImpl, true, contentStyle), styleManager);
				}
				case "number:text-style" : {
		    		return new NumberTextStyleHandler(this, new NumberTextStyle(name, attributesImpl, true, contentStyle), styleManager);
				}
				case "number:time-style" : {
		    		return new NumberTimeStyleHandler(this, new NumberTimeStyle(name, attributesImpl, true, contentStyle), styleManager);
				}
				case "text:list-style" : {
		    		return new TextListStyleHandler(this, new TextListStyle(name, attributesImpl, true, contentStyle), styleManager);
				}
			}
    	}
    	final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
    	styleManager.getAutomaticStyles(contentStyle).getContent().add(element);
		return new UnknownContentHandler(this, element);
    }
}
