/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import java.util.Iterator;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.properties.MapProperties;

public abstract class StyleBase implements IElementWriter, Cloneable {

	protected AttributesImpl attributes;
	protected DLList<MapProperties> mapStyleList;

	private boolean defaultStyle;
	private boolean automaticStyle;
	private boolean contentStyle;
	private String name;

	protected StyleBase(StyleFamily family, String name, boolean automaticStyle, boolean contentStyle) {
		this.attributes = new AttributesImpl();
		this.defaultStyle = false;
		this.automaticStyle = automaticStyle;
		this.contentStyle = contentStyle;
		this.name = name;

		if(family!=null) {
			attributes.setValue(Namespaces.STYLE, "family", "style:family", family.getName());
		}
	}

	protected StyleBase(String name, AttributesImpl attributesImpl, boolean defaultStyle, boolean automaticStyle, boolean contentStyle) {
		this.attributes = attributesImpl;
		this.defaultStyle = defaultStyle;
		this.automaticStyle = automaticStyle;
		this.contentStyle = contentStyle;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	protected void writeNameAttribute(SerializationHandler output) throws SAXException {
        output.addAttribute(Namespaces.STYLE, "name", "style:name", "", name);
	}

	public String getDisplayName() {
		return attributes.getValue("style:display-name");
	}

	public void setDisplayName(String displayName) {
		attributes.setValue(Namespaces.STYLE, "display-name", "style:display-name", displayName);
	}

	public String getParent() {
		return attributes.getValue("style:parent-style-name");
	}

	public void setParent(String parent) {
		attributes.setValue(Namespaces.STYLE, "parent-style-name", "style:parent-style-name", parent);
	}

	public DLList<MapProperties> getMapStyleList() {
		if(mapStyleList==null) {
			mapStyleList = new DLList<MapProperties>();
		}
		return mapStyleList;
	}

	protected void writeAttributes(SerializationHandler output)
		throws SAXException {

		if(!defaultStyle&&name!=null) {
		    writeNameAttribute(output);
		}
		attributes.write(output);
	}

	protected void writeMapStyleList(SerializationHandler output)
		throws SAXException {

		if(!defaultStyle&&mapStyleList!=null) {
			final Iterator<MapProperties> mapStyleIter = mapStyleList.iterator();
			while(mapStyleIter.hasNext()) {
				((IElementWriter)mapStyleIter.next()).writeObject(output);
			}
		}
	}

	abstract public StyleFamily getFamily();

	public String getQName() {
		return defaultStyle ? "style:default-style" : "style:style";
	}

	public String getLocalName() {
		return defaultStyle ? "default-style" : "style";
	}

	public String getNamespace() {
		return Namespaces.STYLE;
	}

	public final boolean isAutoStyle() {
		return automaticStyle;
	}

	public final void setIsAutoStyle(boolean isAutoStyle) {
		automaticStyle = isAutoStyle;
	}

	public final boolean isContentStyle() {
		return contentStyle;
	}

	public final void setIsContentStyle(boolean isContentStyle) {
		contentStyle = isContentStyle;
	}

	public final boolean isDefaultStyle() {
		return defaultStyle;
	}

	public final void setIsDefaultStyle(boolean isDefaultStyle) {
		defaultStyle = isDefaultStyle;
		if(defaultStyle) {
			// defaults exists only once for each family, to be able to use default styles
			// within our style collections we give them a name (which is not saved into the
			// document later)
			name = "_default";
		}
	}

	public boolean isMasterStyle() {
		return false;
	}

	public void setIsVolatile(boolean isVolatile) {
		attributes.setValue(Namespaces.STYLE, "volatile", "style:volatile", isVolatile ? "true" : null);
	}

	public String getAttribute(String qName) {
		return attributes.getValue(qName);
	}

	public Boolean getBoolean(String qName, Boolean defaultValue) {
		return attributes.getBooleanValue(qName, defaultValue);
	}

	public Integer getInteger(String qName, Integer defaultValue) {
		final Integer ret = attributes.getIntValue(qName);
		return ret!=null ? ret : defaultValue;
	}

	public AttributesImpl getAttributes() {
		return attributes;
	}

	public abstract void applyAttrs(StyleManager styleManager, JSONObject attrs)
		throws JSONException;

	public abstract void createAttrs(StyleManager styleManager, OpAttrs attrs);

	public abstract void mergeAttrs(StyleBase style);

	@Override
	public abstract StyleBase clone();

	protected Object _clone() {
		try {
			final StyleBase clone = (StyleBase)super.clone();
			clone.attributes = attributes.clone();
			if(mapStyleList!=null&&!mapStyleList.isEmpty()) {
				clone.mapStyleList = new DLList<MapProperties>();
				final Iterator<MapProperties> sourceIter = mapStyleList.iterator();
				while(sourceIter.hasNext()) {
					clone.mapStyleList.add(sourceIter.next().clone());
				}
			}
			return clone;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	protected abstract int _hashCode();

	protected abstract boolean _equals(StyleBase e);

	@Override
	final public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getQName().hashCode();
		result = prime * result + getFamily().hashCode();
		result = prime * result + attributes.hashCode();
		result = prime * result + (automaticStyle ? 1231 : 1237);
		result = prime * result + (contentStyle ? 1231 : 1237);
		result = prime * result + (isMasterStyle() ? 1231 : 1237);
		result = prime * result + (defaultStyle ? 1231 : 1237);
		if(mapStyleList!=null) {
			final Iterator<MapProperties> mapStyleIter = mapStyleList.iterator();
			while(mapStyleIter.hasNext()) {
				result = prime * result + mapStyleIter.next().hashCode();
			}
		}
		result = prime * result + _hashCode();
		return result;
	}

	@Override
	final public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final StyleBase other = (StyleBase)obj;
		if (!attributes.equals(other.attributes))
			return false;
		if (automaticStyle != other.automaticStyle)
			return false;
		if (contentStyle != other.contentStyle)
			return false;
		if (isMasterStyle() != other.isMasterStyle())
			return false;
		if (defaultStyle != other.defaultStyle)
			return false;
		if (!_equals(other)) {
			return false;
		}
		if (mapStyleList == null || mapStyleList.isEmpty()) {
			if (other.mapStyleList != null && !other.mapStyleList.isEmpty())
				return false;
		}
		else {
			if(other.mapStyleList==null) {
				return false;
			}
			if(mapStyleList.size()!=other.mapStyleList.size()) {
				return false;
			}
			final Iterator<MapProperties> sourceIter = mapStyleList.iterator();
			final Iterator<MapProperties> otherIter = other.mapStyleList.iterator();
			while(sourceIter.hasNext()) {
				if(!sourceIter.next().equals(otherIter.next())) {
					return false;
				}
			}
		}
		return true;
	}

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [name=" + name + "]";
    }
}
