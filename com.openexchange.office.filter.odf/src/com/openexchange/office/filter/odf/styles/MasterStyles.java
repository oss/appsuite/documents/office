/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.styles;

import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

abstract public class MasterStyles extends ElementNSImpl implements IElementWriter, INodeAccessor {

	private static final long serialVersionUID = 1L;

	private DLList<Object> content;

	public MasterStyles(OdfFileDom ownerDocument) {
		super(ownerDocument, Namespaces.OFFICE, "office:master-styles");
	}

	@Override
	public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
		return content;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.OFFICE, "master-styles", "office:master-styles");
		for(Object child:getContent()) {
			((IElementWriter)child).writeObject(output);
		}
		SaxContextHandler.endElement(output, Namespaces.OFFICE, "master-styles", "office:master-styles");
	}
}
