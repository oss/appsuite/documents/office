/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.odf.odt.dom.Annotation;

public class Authors {

    int count = 0;
    final private DLList<Author> authors = new DLList<Author>();

    public void  addAuthor(Annotation annotation) {
        final String a = annotation.getCreator()==null ? "" : annotation.getCreator();
        final String i = annotation.getInitials() == null ? getInitialsFromName(a) : annotation.getInitials();

        final Author author = new Author(a, i);
        DLNode<Author> node = authors.getFirstNode();

        int authorsId = 1;
        while(node!=null) {
            if(node.getData().equals(author)) {
                break;
            }
            node = node.getNext();
            authorsId++;
        }
        if(node==null) {
            authors.add(author);
            count++;
        }
        annotation.setAuthorId(authorsId);
    }

    private String getInitialsFromName(String n) {
        if(n.isEmpty()) {
            return "";
        }
        return n.substring(0, 0);
    }

    public JSONArray getJSON()
        throws JSONException {

        if(authors.isEmpty()) {
            return null;
        }
        final JSONArray json = new JSONArray(count);
        DLNode<Author> node = authors.getFirstNode();
        long id = 1;
        while(node!=null) {
            final JSONObject authorEntry = new JSONObject(3);
            final Author author = node.getData();
            authorEntry.put(OCKey.AUTHOR_ID.value(), id);
            authorEntry.put(OCKey.NAME.value(), author.getName());
            authorEntry.put(OCKey.INITIALS.value(), author.getInitials());
            json.put(authorEntry);
            node = node.getNext();
            id++;
        }
        return json;
    }
}
