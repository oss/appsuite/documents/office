/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import java.util.ArrayList;
import java.util.List;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.SAXException;

public class ConfigItemMapIndexed implements IElementWriter {

	final String name;
	final List<ConfigItemMapEntry> configItemMapEntries;

	public ConfigItemMapIndexed(String name) {
		this.name = name;
		configItemMapEntries = new ArrayList<ConfigItemMapEntry>();
	}

	public List<ConfigItemMapEntry> getEntries() {
		return configItemMapEntries;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.CONFIG, "config-item-map-indexed", "config:config-item-map-indexed");
		SaxContextHandler.addAttribute(output, Namespaces.CONFIG, "name", "config:name", name);
		for(ConfigItemMapEntry configItemMapEntry:configItemMapEntries) {
			configItemMapEntry.writeObject(output);
		}
		SaxContextHandler.endElement(output, Namespaces.CONFIG, "config-item-map-indexed", "config:config-item-map-indexed");
	}
}
