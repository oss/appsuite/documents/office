/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

@SuppressWarnings("serial")
public class DatabaseRanges extends ElementNSImpl implements IElementWriter {

	private final HashMap<String, DatabaseRange> databaseRangeList = new HashMap<String, DatabaseRange>();

	public DatabaseRanges(OdfFileDom ownerDocument)
		throws DOMException {
		super(ownerDocument, Namespaces.TABLE, "table:database-ranges");
	}

	public HashMap<String, DatabaseRange> getDatabaseRangeList() {
		return databaseRangeList;
	}

	public DatabaseRange getDatabaseRange(SpreadsheetContent content, int sheetIndex, String name, boolean forceCreate) {
		if(name==null||name.isEmpty()) {
			return getAnonymousRange(content, sheetIndex, forceCreate);
		}
		DatabaseRange databaseRange = databaseRangeList.get(name);
		if(databaseRange==null&&forceCreate) {
			databaseRange = new DatabaseRange(name);
			databaseRangeList.put(name, databaseRange);
		}
		return databaseRange;
	}

	public void deleteTable(SpreadsheetContent content, int sheetIndex, String name) {
		final DatabaseRange databaseRange = getDatabaseRange(content, sheetIndex, name, false);
		if(databaseRange!=null) {
			final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRangeList.entrySet().iterator();
			while(databaseRangeIter.hasNext()) {
				final Entry<String, DatabaseRange> databaseEntry = databaseRangeIter.next();
				if(databaseEntry.getValue().equals(databaseRange)) {
					databaseRangeIter.remove();
					break;
				}
			}
		}
	}

	private DatabaseRange getAnonymousRange(SpreadsheetContent content, int sheetIndex, boolean forceCreate) {
		final Iterator<DatabaseRange> databaseRangeIter = databaseRangeList.values().iterator();
		while(databaseRangeIter.hasNext()) {
			final DatabaseRange databaseRange = databaseRangeIter.next();
			if(databaseRange.getName().startsWith("__Anonymous_Sheet_DB__")) {
				if(databaseRange.getRange().getSheetIndex()==sheetIndex) {
					return databaseRange;
				}
			}
		}
		if(forceCreate) {
			// search for the first unused Anonymous entry
			int count = 0;
			DatabaseRange databaseRange = null;
			String name;
			do {
				name = "__Anonymous_Sheet_DB__" + Integer.toString(count++);
				databaseRange = databaseRangeList.get(name);
			}
			while(databaseRange!=null);

			databaseRange = new DatabaseRange(name);
			databaseRangeList.put(name, databaseRange);
			return databaseRange;
		}
		return null;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		if(!databaseRangeList.isEmpty()) {
			SaxContextHandler.startElement(output, getNamespaceURI(), getLocalName(), getNodeName());
			SaxContextHandler.addAttributes(output, getAttributes(), null);
			final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRangeList.entrySet().iterator();
			while(databaseRangeIter.hasNext()) {
				databaseRangeIter.next().getValue().writeObject(output, (SpreadsheetContent)this.getOwnerDocument());
			}
			SaxContextHandler.endElement(output, getNamespaceURI(), getLocalName(), getNodeName());
		}
	}
}
