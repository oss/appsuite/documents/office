/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.draw.IDrawing;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleGraphic;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class Drawing implements IDrawing {

    private Sheet sheet;
    private DrawingAnchor drawingAnchor;
	final private Shape shape;

	public Drawing(Sheet sheet, DrawingAnchor anchor, Shape shape) {
		drawingAnchor = anchor;
		this.sheet = sheet;
		this.shape = shape;
	}

	@Override
    public DrawingType getType() {
	    return shape.getType();
	}

	@Override
	public boolean preferRepresentation() {
	    return shape.preferRepresentation();
	}

    @Override
    public DLList<Object> getContent() {
        return shape.getContent();
    }

	public DrawingAnchor getAnchor() {
		return drawingAnchor;
	}

    @Override
    public AttributesImpl getAttributes() {
        return shape.getAttributes();
    }

    public Shape getShape() {
        return shape;
    }

    // changes the DrawingAnchor, if the Drawing is already added to Drawings, then
	// this DrawingAnchor should be changed via Drawings.setDrawingAnchor() only
	public void setDrawingAnchor(DrawingAnchor anchor) {
		drawingAnchor = anchor;
	}

    @Override
    public void writeObject(SerializationHandler output) throws SAXException {
        shape.writeObject(output);
    }

    @Override
    public void createAttrs(OdfOperationDoc operationDocument, OpAttrs attrs, boolean contentAutoStyle) {

        shape.createAttrs(operationDocument, attrs, contentAutoStyle);
        boolean protectedSize = false;
		boolean protectedPosition = false;

		final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
		final String graphicStyleName = shape.getAttributes().getValue("draw:style-name");
		if(graphicStyleName!=null) {
			final StyleBase styleBase = styleManager.getStyle(graphicStyleName, StyleFamily.GRAPHIC, true);
			if(styleBase!=null) {
				final String styleProtect = ((StyleGraphic)styleBase).getGraphicProperties().getAttribute("style:protect");
				if(styleProtect!=null) {
					if(styleProtect.contains("size")) {
						protectedSize = true;
					}
					if(styleProtect.contains("position")) {
						protectedPosition = true;
					}
				}
			}
		}
        final OpAttrs drawingAttrs = attrs.getMap(OCKey.DRAWING.value(), true);
		final StringBuilder builder = new StringBuilder();
		if(drawingAnchor!=null) {
            final Object lo = drawingAttrs.remove(OCKey.LEFT.value());
            final Object to = drawingAttrs.remove(OCKey.TOP.value());
            int startCol = drawingAnchor.getColumn();
            int startRow = drawingAnchor.getRow();
            Integer left = Integer.valueOf(lo instanceof Number ? ((Number)lo).intValue() : 0);
            Integer top = Integer.valueOf(to instanceof Number ? ((Number)to).intValue() : 0);

            final String tableEndCellAddress = shape.getAttributes().getValue("table:end-cell-address");
            if(protectedSize||tableEndCellAddress==null||tableEndCellAddress.isEmpty()) {
                builder.append("1 ")
                       .append(CellRef.getCellRef(new CellRef(startCol, startRow)))
                       .append(' ')
                       .append(left.toString())
                       .append(' ')
                       .append(top.toString());
            }
            else {

                final CellRef endAddress = CellRef.createCellRefWithSheet(tableEndCellAddress, ".");
                final Object endXo = shape.getAttributes().getLength100thmm("table:end-x", false);
                final Object endYo = shape.getAttributes().getLength100thmm("table:end-y", false);
                int endCol = endAddress.getColumn();
                int endRow = endAddress.getRow();
                Integer endX = Integer.valueOf(endXo instanceof Number ? ((Number)endXo).intValue() : 0);
                Integer endY = Integer.valueOf(endYo instanceof Number ? ((Number)endYo).intValue() : 0);
                if(endCol < startCol) {
                    endCol = startCol;
                    startCol = endAddress.getColumn();
                    final Integer l = left;
                    left = endX;
                    endX = l;
                }
                if(endRow < startRow) {
                    endRow = startRow;
                    startRow = endAddress.getRow();
                    final Integer t = top;
                    top = endY;
                    endY = t;
                }
                builder.append("2 ")
                .append(CellRef.getCellRef(new CellRef(startCol, startRow)))
                .append(' ')
                .append(left.toString())
                .append(' ')
                .append(top.toString())
                .append(' ')
                .append(CellRef.getCellRef(new CellRef(endCol, endRow)))
                .append(' ')
                .append(endX.toString())
                .append(' ')
                .append(endY.toString());
			}
		}
		else {
		    builder.append('a');
		}
        drawingAttrs.put(OCKey.ANCHOR.value(), builder.toString());
		attrs.removeEmpty(OCKey.LINE.value());
		attrs.removeEmpty(OCKey.FILL.value());
		attrs.removeEmpty(OCKey.IMAGE.value());
    }

    @Override
    public void applyAttrsFromJSON(OdfOperationDoc operationDocument, JSONObject attrs, boolean contentAutoStyle)
        throws JSONException {

        final AttributesImpl shapeAttributes = shape.getAttributes();
        final JSONObject drawingAttrs = attrs.optJSONObject(OCKey.DRAWING.value());
        if(drawingAttrs!=null) {
            final String anchor = drawingAttrs.optString(OCKey.ANCHOR.value(), null);
            if(anchor!=null) {
                final char anchorType = anchor.charAt(0);
                switch(anchorType) {
                    case 'a' : {
                        sheet.getDrawings().setDrawingAnchor(null, this);
                        shapeAttributes.remove("table:end-cell-address");
                        shapeAttributes.remove("table:end-x");
                        shapeAttributes.remove("table:end-y");
                        break;
                    }
                    case '1' :
                    case '2' :
                    case 'O' :
                    case 'A' : {
                        final String[] anchorAttrs = anchor.substring(2).split(" ", -1);

                        // ensure that the cell where the drawing is attached to is physically available ...
                        final CellRef startRef = CellRef.createCellRef(anchorAttrs[0]);
                        sheet.getRow(startRef.getRow(), true, true, true).getCell(startRef.getColumn(), true, true, true);
                        sheet.getDrawings().setDrawingAnchor(new DrawingAnchor(startRef.getColumn(), startRef.getRow()), this);

                        // col/row offsets are needed at the transformer instead of left and top
                        drawingAttrs.remove(OCKey.LEFT.value());
                        drawingAttrs.remove(OCKey.TOP.value());

                        shape.getTransformer().setX(Integer.parseInt(anchorAttrs[1]));
                        shape.getTransformer().setY(Integer.parseInt(anchorAttrs[2]));

                        // applying new endCellAddress
                        if(anchorAttrs.length >= 4) {
                            shapeAttributes.setValue(Namespaces.TABLE, "end-cell-address", "table:end-cell-address", sheet.getName() + "." + anchorAttrs[3]);
                        }
                        if(anchorAttrs.length==6) {
                            shapeAttributes.setLength100thmm(Namespaces.TABLE, "end-x", "table:end-x", Integer.parseInt(anchorAttrs[4]));
                            shapeAttributes.setLength100thmm(Namespaces.TABLE, "end-y", "table:end-y", Integer.parseInt(anchorAttrs[5]));
                        }
                        break;
                    }
                }
            }
        }
        shape.applyAttrsFromJSON(operationDocument, attrs, contentAutoStyle);
    }
}
