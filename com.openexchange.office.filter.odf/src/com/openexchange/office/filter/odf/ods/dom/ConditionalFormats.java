/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

@SuppressWarnings("serial")
public class ConditionalFormats extends ElementNSImpl implements IElementWriter {

    private final List<Condition> conditionalFormatList = new ArrayList<Condition>();

	public ConditionalFormats(OdfFileDom ownerDocument)
		throws DOMException {
		super(ownerDocument, Namespaces.CALCEXT, "calcext:conditional-formats");
	}

	public List<Condition> getConditionalFormatList() {
		return conditionalFormatList;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		if(!conditionalFormatList.isEmpty()) {
			output.startElement(getNamespaceURI(), getLocalName(), getNodeName());
			SaxContextHandler.addAttributes(output, getAttributes(), null);
			for (Condition condition : conditionalFormatList) {
				final String ranges = condition.getRanges().convertToString((SpreadsheetContent)this.getOwnerDocument(), true);
				SaxContextHandler.startElement(output, Namespaces.CALCEXT, "conditional-format", "calcext:conditional-format");
				SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "target-range-address","calcext:target-range-address", ranges);
				condition.writeObject(output);
				SaxContextHandler.endElement(output, Namespaces.CALCEXT, "conditional-format", "calcext:conditional-format");

			}
			output.endElement(getNamespaceURI(), getLocalName(), getNodeName());
		}
	}
}
