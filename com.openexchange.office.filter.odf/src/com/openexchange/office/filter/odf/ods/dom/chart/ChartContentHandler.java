/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom.chart;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.AttributeSet;
import com.openexchange.office.filter.core.chart.ChartSeries;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.DOMBuilder;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.styles.AutomaticStylesHandler;
import com.openexchange.office.filter.odf.styles.FontFaceDeclsHandler;

public class ChartContentHandler extends DOMBuilder {

    private ChartContent content;
    private String chartType;

    public ChartContentHandler(ChartContent fDom, XMLReader reader) {
        super(fDom, reader, fDom);
        content = fDom;
    }

    ChartContent getChart() {
        return content;
    }

    private String getChartType(String type) {
        if (StringUtils.isNotBlank(type)) {

            type = type.substring(6);

            if ("circle".equals(type)) {
                type = "pie";
                if ("chart:ring".equals(content.getChartArea().getAttribute("chart:class"))) {
                    type = "donut";
                }
            } else if ("bar".equals(type) && !content.getPlotAreaStyle().getChartProperties().getBoolean("chart:vertical", false)) {
                type = "column";
            }
            return type;
        }
        return null;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        OdfOperationDoc.abortOnLowMemory(getFileDom());
        AttributesImpl attrs = new AttributesImpl(attributes);
        String styleName = attrs.getValue(ChartContent.ODF_STYLE);

        switch (qName) {
            case "chart:series":

                String type = getChartType(attrs.getValue("chart:class"));
                if (StringUtils.isBlank(type)) {
                    type = chartType;
                }

                if (chartType == null) {
                    chartType = type;
                }

                String valueAttribute = attrs.getValue("chart:" + ChartContent.ODF_VALUES);

                // If the chart use a local table which is not supported is by OX, change the type of the charttype to 'localtable' so the UI show a chart placeholder for
                // not supported charts
                if (!StringUtils.isBlank(valueAttribute) && valueAttribute.startsWith("local-table")) {
                    type = "localtable";
                }

                String values = ChartContent.getFormula(valueAttribute);
                String title = ChartContent.getFormula(attrs.getValue("chart:" + ChartContent.ODF_TITLE));

                int xId = 0;
                int yId = 1;

                Integer zId = content.getChart().containsAxis(10) ? 10 : null;
                if (attrs.containsKey("chart:attached-axis") && attrs.getValue("chart:attached-axis").startsWith("secondary")) {
                    if (content.getChart().containsAxis(2)) {
                        xId = 2;
                    }
                    yId = 3;
                    zId = content.getChart().containsAxis(11) ? 11 : null;
                }

                boolean chart3D = content.getPlotAreaStyle().getChartProperties().getBoolean("chart:three-dimensional", false);

                ElementNS serieEl = content.addDataSeries(type, values, title, styleName, xId, yId, zId, chart3D);
                serieEl.addAttributes(attributes);

                if (content.getChart().isPieOrDonut()) {
                    JSONObject chart = content.getChart().getOrCreate(OCKey.CHART.value());
                    if (!chart.has(OCKey.ROTATION.value())) {
                        chart.putSafe(OCKey.ROTATION.value(), 90);
                    }
                }

                if (StringUtils.isNotEmpty(styleName)) {
                    String symbolType = content.getStyleChart(styleName).getChartProperties().getAttribute("chart:symbol-type");
                    ChartSeries series = (ChartSeries) serieEl.getUserData(ChartContent.USERDATA);
                    JSONObject markerAttributes;
                    if (StringUtils.equals(symbolType, "none")) {
                        if (series.isLine() || series.isScatter()) {
                            series.setHiddenMarker(true);
                        }
                        markerAttributes = new JSONObject();

                        AttributeSet.mergeAttributes(markerAttributes, series.getAttributes().optJSONObject(OCKey.FILL.value()));
                        markerAttributes.putSafe(OCKey.TYPE.value(), "none");
                    } else {
                        markerAttributes = series.getAttributes().optJSONObject(OCKey.FILL.value());
                    }
                    series.getAttributes().putSafe(OCKey.MARKER_FILL.value(), markerAttributes);
                    series.getAttributes().putSafe(OCKey.MARKER_BORDER.value(), markerAttributes);
                }
                return new ChartSeriesHandler(this, serieEl);

            case "office:font-face-decls":
                return new FontFaceDeclsHandler(this, content.getDocument().getStyleManager(), true);
            case "office:automatic-styles":
                return new AutomaticStylesHandler(this, content.getDocument().getStyleManager(), true);

            case "chart:chart":
                JSONObject bgStyle = content.getStyleAttrs(styleName);
                if (!bgStyle.hasAndNotNull(OCKey.FILL.value())) {
                    //TODO HACK
                    JSONObject fill = new JSONObject(2);
                    fill.putSafe(OCKey.TYPE.value(), "solid");

                    JSONObject color = new JSONObject(2);
                    color.putSafe(OCKey.VALUE.value(), "ffffff");
                    color.putSafe(OCKey.TYPE.value(), "rgb");
                    fill.putSafe(OCKey.COLOR.value(), color);

                    bgStyle.putSafe(OCKey.FILL.value(), fill);
                }
                content.getChart().setAttributes(bgStyle);
                super.startElement(attributes, uri, localName, qName);
                content.setChartArea((ElementNS) getCurrentNode());

                return this;

            case "chart:wall":
                ElementNS el = new ElementNS(content, attributes, uri, qName);
                content.getPlotArea().appendChild(el);

                return this;

            case "chart:plot-area":
                ElementNS plotArea = new ElementNS(content, attributes, uri, qName);
                content.setPlotArea(plotArea);
                content.getChartArea().appendChild(plotArea);

                content.getChart().setAttributes(content.getStyleAttrs(styleName));

                String chartStyleId = attrs.getValue("chart:" + ChartContent.CHARTSTYLEID);
                if (chartStyleId != null) {
                    try {
                        content.getChart().setChartStyleId(Integer.parseInt(chartStyleId));
                    } catch (NumberFormatException e) {
                        //ignore broken param
                    }
                }

                chartType = getChartType(content.getChartArea().getAttribute("chart:class"));

                return this;

            case "chart:axis":
                ElementNS axisEl = content.addAxis(getAxisId(attrs), styleName);
                axisEl.addAttributes(attributes);

                return new ChartAxisHandler(this, axisEl);

            case "chart:title":
                ElementNS titleEl = content.addMainTitle(styleName);

                AttributeSet mainTitle = content.getChart().getTitle(null);
                mainTitle.setAttributes(content.getStyleAttrs(styleName));
                return new ChartTitleHandler(this, null, titleEl);

            case "chart:legend":
                String legendPos = attrs.getValue("chart:" + ChartContent.ODF_LEGEND);
                JSONObject legend = new JSONObject(1);
                legend.putSafe(OCKey.POS.value(), ChartContent.LEGENDPOS_ODF_TO_OP.get(legendPos));

                JSONObject a = content.getStyleAttrs(styleName);
                a.putSafe(OCKey.LEGEND.value(), legend);
                content.changeLegend(a);

                return this;

            case "chartooo:coordinate-region":
            case "chart:floor":
                ElementNS ele = new ElementNS(content, attributes, uri, qName);
                content.getPlotArea().appendChild(ele);
                return new UnknownContentHandler(this, ele);
            case "draw:custom-shape":
                return super.startElement(attributes, uri, localName, qName);
            default:
                if (qName.startsWith("table")) {
                    ElementNS ele2 = new ElementNS(content, attributes, uri, qName);
                    content.getChartArea().appendChild(ele2);
                    return new UnknownContentHandler(this, ele2);
                }
                return super.startElement(attributes, uri, localName, qName);
        }
    }

    private int getAxisId(AttributesImpl attrs) {
        int axisId;

        String axName = attrs.getValue("chart:name");
        String axType = attrs.getValue("chart:dimension");

        boolean barChart = "bar".equals(this.chartType);
        boolean axisTypeX = barChart ? axType.equals("y") : axType.equals("x");
        boolean axisTypeY = barChart ? axType.equals("x") : axType.equals("y");
        boolean secondary = axName != null && axName.startsWith("secondary");

        if (axisTypeX) {
            axisId = secondary ? 2 : 0;
        } else if (axisTypeY){
            axisId = secondary ? 3 : 1;
        } else {
            axisId = secondary ? 11 : 10;
        }

        return axisId;
    }

    @Override
    public void endElement(String localName, String qName) {
        if (qName.equals("office:automatic-styles") || qName.equals("office:font-face-decls")) {
            // do nothing since we do not want this element to be inserted into the dom,
            // this is done elsewhere
            return;
        }
        super.endElement(localName, qName);
    }

}
