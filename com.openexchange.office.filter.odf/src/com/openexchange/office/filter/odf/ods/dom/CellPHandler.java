/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.SmlUtils.*;

public class CellPHandler extends SaxContextHandler {

	final CellHandler cellHandler;
	final Sheet sheet;
	final Row row;
	final Cell cell;

	public CellPHandler(CellHandler parentContextHandler, Sheet sheet, Row row, Cell cell) {
		super(parentContextHandler);
    	cellHandler = parentContextHandler;
		this.sheet = sheet;
		this.row = row;
		this.cell = cell;
	}

	@Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        OdfOperationDoc.abortOnLowMemory(getFileDom());
		if(localName.equals("a")&&cellHandler.getStringValue()==null) {
			final String href = attributes.getValue(Namespaces.XLINK, "href");
			if(href!=null) {
				final CellRef cellRef = new CellRef(cell.getColumn(), row.getRow());
				sheet.getHyperlinks().add(new Hyperlink(new CellRefRange(cellRef, cellRef.clone()), href));
			}
		}
    	return this;
	}

	@Override
	public void endContext(String qName, String characters) {
		cellHandler.appendStringValue(characters);
	}
}
