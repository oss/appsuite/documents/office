/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.odf.Length;
import com.openexchange.office.filter.odf.Length.Unit;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.PropertyHelper;
import com.openexchange.office.filter.odf.properties.TableRowProperties;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleTableRow;

public class Row extends AbstractColRow implements Comparable<Row>, IAttrs<Row> {

    private int r;
    private int repeated;

    private final TreeSet<Cell> cells = new TreeSet<Cell>();

    Row(int row, int repeated) {
        super("table-row-group");
        this.r = row;
        this.repeated = repeated;
    }

    Row(int row) {
        this(row, 1);
    }

    private void copyCellsFrom(Row source, boolean withContent) {
        final Iterator<Cell> iter = source.cells.iterator();
        while (iter.hasNext()) {
            Cell newCell = iter.next().clone(withContent);
            if (!withContent) { newCell.setCellStyle(null); }
            cells.add(newCell);
        }
    }

    /**
     * Creates a row instance intended to be used for an "insert rows" document operation.
     */
    public static Row createInserted(int row, int repeated, Row prevRow) {
        final Row newRow = new Row(row, repeated);
        if (prevRow != null) {
            newRow.setStyleName(prevRow.getStyleName());
            newRow.setDefaultCellStyle(prevRow.getDefaultCellStyle());
            // inserted rows shall become visible (do not copy "visible")
            newRow.setGroupLevel(prevRow.getGroupLevel());
            // inserted rows do not start a row group (do not copy "collapse")
            newRow.copyCellsFrom(prevRow, false);
        }
        return newRow;
    }

    /**
     * Returns a clone of this instance, spanning over all rows preceding the specified row index.
     * This instance will be updated so that it starts at the specified row index.
     */
    public Row splitBefore(int row) {

        if (row <= r || row > getMax()) { return null; }

        // create a clone of this row
        final Row clone = new Row(r, row - r);
        clone.setStyleName(getStyleName());
        clone.setDefaultCellStyle(getDefaultCellStyle());
        clone.setVisibility(getVisibility());
        clone.setGroupLevel(getGroupLevel());
        clone.setGroupCollapse(getGroupCollapse());
        clone.copyCellsFrom(this, true);

        // update the settings of this row
        repeated = getMax() - row + 1;
        r = row;

        return clone;
    }

    /* returns a live list to the columns */
    public TreeSet<Cell> getCells() {
        return cells;
    }

    public Cell getCell(int c, boolean forceCreate, boolean cutFloor, boolean cutCeiling) {
        if(cells.isEmpty()) {
            if(!forceCreate) {
                return null;
            }
            // creating new cells up to c
            final Cell newCell = new Cell(0);
            newCell.setRepeated(c+1);
            cells.add(newCell);
        }
        Cell retValue = cells.floor(new Cell(c));
        if((retValue==null)||(retValue.getColumn()+retValue.getRepeated()<=c)) {
            if(!forceCreate) {
                return null;
            }
            // we have to create new cells from end of floorValue up to c
            final Cell newCell = new Cell(retValue!=null?retValue.getColumn()+retValue.getRepeated():0);
            newCell.setRepeated((c-newCell.getColumn())+1);
            cells.add(newCell);
            retValue = newCell;
        }
        // Cells up to c are available
        if(cutFloor&&retValue.getColumn()!=c) {
            final Cell newCell = retValue.clone(true);
            newCell.setColumn(c);
            newCell.setRepeated((retValue.getColumn()+retValue.getRepeated())-c);
            cells.add(newCell);
            retValue.setRepeated(c-retValue.getColumn());
            retValue = newCell;
        }
        if(cutCeiling&&((retValue.getColumn()+retValue.getRepeated())-1)>c) {
            final Cell newCell = retValue.clone(true);
            newCell.setColumn(c+1);
            newCell.setRepeated(((retValue.getColumn()+retValue.getRepeated())-1)-c);
            cells.add(newCell);
            retValue.setRepeated(retValue.getRepeated()-newCell.getRepeated());
        }
        return retValue;
    }

    public int getRow() {
        return r;
    }

    public void setRow(int row) {
        r = row;
    }

    public int getRepeated() {
        return repeated;
    }

    public void setRepeated(int val) {
        repeated = val;
    }

    @Override
    public int getMin() {
        return r;
    }

    @Override
    public int getMax() {
        return r + repeated - 1;
    }

    public void insertCells(int start, int count) {

        // assuring that the first cell is available
        getCell(start, true, true, false);

        final Object[] rs = cells.tailSet(new Cell(start), true).toArray();
        for(int i=rs.length-1;i>=0;i--) {
            final Cell ce = (Cell)rs[i];
            cells.remove(ce);
            int newCellNumber = ce.getColumn()+count;
            if(newCellNumber<Sheet.getMaxRowCount()) {
                ce.setColumn(ce.getColumn()+count);
                cells.add(ce);
                if(newCellNumber+ce.getRepeated()>Sheet.getMaxRowCount()) {
                    ce.setRepeated(Sheet.getMaxRowCount()-newCellNumber);
                }
            }
        }
        Cell previousCell = null;
        if(start>0) {
            previousCell = getCell(start-1, false, false, false);
        }
        final Cell newRow = new Cell(start);
        if(previousCell!=null) {
            newRow.setCellStyle(previousCell.getCellStyle());
            newRow.setContentValidationName(previousCell.getContentValidationName());
        }
        newRow.setRepeated(count);
        cells.add(newRow);
    }

    public void deleteCells(int start, int count) {

        getCell(start, true, true, true);
        if(count>1) {
            getCell((start+count)-1, true, false, true);
        }
        final Object[] rs = cells.tailSet(new Cell(start), true).toArray();
        for(Object obj:rs) {
            final Cell ce = (Cell)obj;
            cells.remove(ce);
            if(ce.getColumn()>=start+count) {
                ce.setColumn(ce.getColumn()-count);
                cells.add(ce);
            }
        }
    }

    public void writeObject(SerializationHandler output, Sheet sheet)
        throws SAXException {

        SaxContextHandler.startElement(output, Namespaces.TABLE, "table-row", "table:table-row");
        if(repeated>1) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "number-rows-repeated", "table:number-rows-repeated", Integer.toString(repeated));
        }
        if(getStyleName()!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "style-name", "table:style-name", getStyleName());
        }
        if(getDefaultCellStyle()!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "default-cell-style-name", "table:default-cell-style-name", getDefaultCellStyle());
        }
        if(getVisibility()!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "visibility", "table:visibility", getVisibility().toString());
        }
        if(getId()!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "id", "table:id", getId());
        }

        MergeCell merge = null;
        final List<MergeCell> mergeCells = sheet.getMergeCells();
        for(Cell cell:cells) {
            if(merge!=null) {
                // check if the cell fits into the old merge
                if(merge.getCellRefRange(true).getEnd().getColumn()<cell.getColumn()) {
                    merge = null;
                }
            }
            if(merge==null) {
                // check if we can find a new merge for our new cell
                for(MergeCell mergeCell:mergeCells) {
                    final CellRef mergeCellStart = mergeCell.getCellRefRange(true).getStart();
                    if(mergeCellStart.getColumn()==cell.getColumn()&&mergeCellStart.getRow()<=getRow()) {
                        final CellRef mergeCellEnd = mergeCell.getCellRefRange(true).getEnd();
                        if(mergeCellEnd.getRow()>=getRow()) {
                            merge = mergeCell;
                        }
                    }
                }
            }
            cell.writeObject(output, sheet, this, merge);
        }
        SaxContextHandler.endElement(output, Namespaces.TABLE, "table-row", "table:table-row");
    }

    @Override
    public void createAttributes(JSONObject attrs, OdfSpreadsheetDocument doc)
        throws JSONException {

        createGroupAttributes(attrs);

        final JSONObject rowProperties = new JSONObject(2);
        if(getVisibility()!=null) {
            if(getVisibility()==Visibility.COLLAPSE) {
                rowProperties.put(OCKey.VISIBLE.value(), false);
            }
            else if(getVisibility()==Visibility.FILTER) {
                rowProperties.put(OCKey.FILTERED.value(),  true);
            }
            else {
                rowProperties.put(OCKey.VISIBLE.value(),  true);
                rowProperties.put(OCKey.FILTERED.value(), false);
            }
        }
        else {
            rowProperties.put(OCKey.VISIBLE.value(), true);
            rowProperties.put(OCKey.FILTERED.value(), false);
        }
        if(hasStyleName()) {
            final StyleManager styleManager = doc.getStyleManager();
            final StyleBase styleBase = styleManager.getStyle(getStyleName(), StyleFamily.TABLE_ROW, true);
            if(styleBase instanceof StyleTableRow) {
                final TableRowProperties tableRowProperties = ((StyleTableRow)styleBase).getTableRowProperties();
                final String width = tableRowProperties.getAttribute("style:row-height");
                if(width!=null&&!width.isEmpty()) {
                    final Double dest = Length.getLength(width, Unit.MILLIMETER);
                    if(dest!=null) {
                        rowProperties.put(OCKey.HEIGHT.value(), Math.round(dest*100));
                    }
                }
                final String optimalRowHeight = tableRowProperties.getAttribute("style:use-optimal-row-height");
                if(optimalRowHeight!=null&&!optimalRowHeight.isEmpty()) {
                    rowProperties.put(OCKey.CUSTOM_HEIGHT.value(), !Boolean.parseBoolean(optimalRowHeight));
                }
                final String fillColor = tableRowProperties.getAttribute("fo:background-color");
                if(fillColor!=null&&!fillColor.isEmpty()) {
                    final Map<String, Object> color = PropertyHelper.createColorMap(fillColor);
                    if(color!=null) {
                        rowProperties.put(OCKey.FILL_COLOR.value(), color);
                    }
                }
            }
        }
        if(!rowProperties.isEmpty()) {
            attrs.put(OCKey.ROW.value(), rowProperties);
        }
    }

    public static class RowAttrHash extends AttrsHash<Row> {

        public RowAttrHash(Row val) {
            super(val, val.getRepeated());
        }

        // hash and equals includes: defaultCellStyle, rowStyle and visibility

        @Override
        public int hashCode() {
            final Row row = getObject();
            final int prime = 31;
            int result = 1;
            result = prime * result + ((row.getDefaultCellStyle() == null) ? 0 : row.getDefaultCellStyle().hashCode());
            result = prime * result + ((row.getStyleName() == null) ? 0 : row.getStyleName().hashCode());
            result = prime * result + ((row.getVisibility() == null) ? 0 : row.getVisibility().hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {

            if (this == obj) { return true; }
            if ((obj == null) || (getClass() != obj.getClass())) { return false; }

            RowAttrHash other = (RowAttrHash) obj;
            return getObject().equalCommonAttrs(other.getObject());
        }
    }

    @Override
    public RowAttrHash createAttrHash() {
        return new RowAttrHash(this);
    }

    @Override
    public int compareTo(Row o) {
        return r - o.getRow();
    }
}
