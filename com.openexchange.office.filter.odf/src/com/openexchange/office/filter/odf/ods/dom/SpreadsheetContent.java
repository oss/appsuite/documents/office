/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.Iterator;
import java.util.Map.Entry;
import org.apache.xerces.dom.ElementNSImpl;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.dom.OdfDocumentNamespace;
import org.odftoolkit.odfdom.pkg.NamespaceName;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.spreadsheet.ISpreadsheet;
import com.openexchange.office.filter.odf.IContentDom;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawObject;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.ods.dom.chart.ChartContent;
import com.openexchange.office.filter.odf.ods.dom.components.RootComponent;
import com.openexchange.office.filter.odf.styles.AutomaticStyles;
import com.openexchange.office.filter.odf.styles.FontFaceDecls;
import com.openexchange.office.filter.odf.styles.StyleManager;

/**
 * The DOM representation of the ODS content.xml file of an ODF document.
 */
@SuppressWarnings("serial")
public class SpreadsheetContent extends OdfFileDom implements ISpreadsheet<Sheet>, IContentDom {

	private int maxColumnCount;
	private DLList<Sheet> sheets;
	private CalculationSettings calculationSettings;
	private NamedExpressions namedExpressions;
	private DatabaseRanges databaseRanges;
	private ContentValidations contentValidations;
	private ElementNSImpl officeSpreadsheetElement;

	/**
	 * Creates the DOM representation of an XML file of an Odf document.
	 *
	 * @param odfDocument   the document the XML files belongs to
	 * @param packagePath   the internal package path to the XML file
	 */
	public SpreadsheetContent(OdfSpreadsheetDocument odfDocument, String packagePath) throws SAXException {
		super(odfDocument, packagePath);
	}

	@Override
	protected void initialize() throws SAXException {
		for (NamespaceName name : OdfDocumentNamespace.values()) {
			mUriByPrefix.put(name.getPrefix(), name.getUri());
			mPrefixByUri.put(name.getUri(), name.getPrefix());
		}
		final StyleManager styleManager = getDocument().getStyleManager();
		styleManager.setFontFaceDecls(new FontFaceDecls(this), true);
		styleManager.setAutomaticStyles(new AutomaticStyles(this), true);

		final XMLReader xmlReader = mPackage.getXMLReader();
		super.initialize(new SpreadsheetContentHandler(this, xmlReader), xmlReader);

        final Node root = getRootElement();
        root.insertBefore(styleManager.getAutomaticStyles(true), root.getFirstChild());
        root.insertBefore(styleManager.getFontFaceDecls(true), root.getFirstChild());
	}

	/**
	 * Retrieves the Odf Document
	 *
	 * @return The <code>OdfDocument</code>
	 */
	@Override
	public OdfSpreadsheetDocument getDocument() {
		return (OdfSpreadsheetDocument)mPackageDocument;
	}

    @Override
    public RootComponent getRootComponent(OdfOperationDoc operationDocument, String target) {
        INodeAccessor targetAccessor = this;
        if(target!=null&&!target.isEmpty()) {
            targetAccessor = getDocument().getTargetNodes().get(target);
        }
        return new RootComponent(operationDocument, targetAccessor);
    }

    @Override
    public  DLList<Sheet> getContent() {
        if(sheets==null) {
            sheets = new DLList<Sheet>();
        }
        return sheets;
    }

	// returns the sheetindex for the given name or -1 if this sheet was not found
	public int getSheetIndex(String sheetName) {
		if(sheetName!=null) {
			sheetName = sheetName.replace("'", "");
			for(int i=0; i<getContent().size(); i++) {
				final String name = getContent().get(i).getName();
				if(sheetName.equals(name)) {
					return i;
				}
			}
		}
		return -1;
	}

	public void setSpreadsheet(ElementNSImpl element) {
		officeSpreadsheetElement = element;
	}

	public void setCalculationSettings(CalculationSettings calculationSettings) {
	    this.calculationSettings = calculationSettings;
	}

	public CalculationSettings getCalculationSettings() {
	    return calculationSettings;
	}

	public void setNamedExpressions(NamedExpressions namedExpressions) {
		this.namedExpressions = namedExpressions;
	}

	public NamedExpressions getNamedExpressions(boolean forceCreate) {
		if(namedExpressions==null&&forceCreate) {
			namedExpressions = new NamedExpressions(this);

			// try to insert this node behind the last sheet---
			if(!sheets.isEmpty()) {
				final Node refNode = getContent().get(sheets.size()-1).getNextSibling();
				getContent().get(0).getParentNode().insertBefore(namedExpressions, refNode);
			}
			else {
				officeSpreadsheetElement.appendChild(namedExpressions);
			}
		}
		return namedExpressions;
	}

	public void setDatabaseRanges(DatabaseRanges databaseRanges) {
		this.databaseRanges = databaseRanges;
	}

	public DatabaseRanges getDatabaseRanges(boolean forceCreate) {
		if(databaseRanges==null&&forceCreate) {
			databaseRanges = new DatabaseRanges(this);

			if(namedExpressions!=null) {
				// inserting database ranges behind named expressions, otherwise the document won't be opened by Excel
				namedExpressions.getParentNode().insertBefore(databaseRanges, namedExpressions.getNextSibling());
			}
			else if(!sheets.isEmpty()) {
				final Node refNode = getContent().get(sheets.size()-1).getNextSibling();
				getContent().get(0).getParentNode().insertBefore(databaseRanges, refNode);
			}
			else {
				officeSpreadsheetElement.appendChild(databaseRanges);
			}
		}
		return databaseRanges;
	}

    public void setContentValidations(ContentValidations contentValidations) {
        this.contentValidations = contentValidations;
    }

	public ContentValidations getContentValidations(boolean forceCreate) {
		if(contentValidations==null&&forceCreate) {
			contentValidations = new ContentValidations(this);

			final CalculationSettings settingsNode = getCalculationSettings();
			if(settingsNode!=null) {
	            // office:content-validations needs to come behind calculation-settings, otherwise Excel won't load this document
			    settingsNode.getParentNode().insertBefore(contentValidations, settingsNode.getNextSibling());
			}
			else {
			    // if there are no calculation-settings, we will insert this as first child
			    officeSpreadsheetElement.insertBefore(contentValidations, officeSpreadsheetElement.getFirstChild());
			}
		}
		return contentValidations;
	}

	public int getMaxColumnCount() {
		return maxColumnCount==0?1024:maxColumnCount;
	}

	public void setMaxColumnCount(int value) {
		maxColumnCount = value;
	}

	public void insertSheet(int sheetIndex, String newName) {

		final Sheet newSheet = new Sheet(this, null);
		insertSheetNode(newSheet, sheetIndex);
		newSheet.setName(newName);

		// update database ranges:
		if(databaseRanges!=null) {
			final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRanges.getDatabaseRangeList().entrySet().iterator();
			while(databaseRangeIter.hasNext()) {
				final Range range = databaseRangeIter.next().getValue().getRange();
				if(range.getSheetIndex()>=sheetIndex) {
					range.setSheetIndex(range.getSheetIndex()+1);
				}
			}
		}
	}

	private void insertSheetNode(Sheet sheet, int sheetIndex) {
		final boolean append = sheetIndex>(sheets.size()-1);
		if(append) {
			final Node refNode = getContent().get(sheetIndex-1).getNextSibling();
			getContent().get(0).getParentNode().insertBefore(sheet, refNode);
		}
		else {
			final Sheet refSheet = getContent().get(sheetIndex);
			refSheet.getParentNode().insertBefore(sheet, refSheet);
		}
		getContent().add(sheetIndex, sheet);
	}

    @Override
    public void moveSheet(int sheetIndex, int to) {
		final Sheet sheet = getContent().get(sheetIndex);
		sheet.getParentNode().removeChild(sheet);
		getContent().remove(sheetIndex);
        insertSheetNode(sheet, to);

        // update database ranges:
		if(databaseRanges!=null) {
			final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRanges.getDatabaseRangeList().entrySet().iterator();
			while(databaseRangeIter.hasNext()) {
				final Range range = databaseRangeIter.next().getValue().getRange();
				if(range.getSheetIndex()==sheetIndex) {
					range.setSheetIndex(to);
					continue;
				}
				if(range.getSheetIndex()>sheetIndex) {
					range.setSheetIndex(range.getSheetIndex()-1);
				}
				if(range.getSheetIndex()>=to) {
					range.setSheetIndex(range.getSheetIndex()+1);
				}
			}
		}
	}

	public Sheet deleteSheet(int sheetIndex) {
		final Sheet sheet = getContent().get(sheetIndex);
		final Drawings drawings = sheet.getDrawings();
		if(drawings!=null) {
		    for(int i=0; i<drawings.getCount(); i++) {
		        final Drawing drawing = drawings.getDrawing(i);
		        if(drawing.getType()==DrawingType.CHART) {
		            final ChartContent chart = ((DrawObject)((DrawFrame)drawing.getShape()).getDrawing()).getChart();
		            if (chart!=null) {
		                Drawings.deleteChartPart(getDocument().getPackage(), chart);
		            }
		        }
		    }
		}
		sheet.getParentNode().removeChild(sheet);
		getContent().remove(sheetIndex);

		// update database ranges:
		if(databaseRanges!=null) {
			final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRanges.getDatabaseRangeList().entrySet().iterator();
			while(databaseRangeIter.hasNext()) {
				final Range range = databaseRangeIter.next().getValue().getRange();
				if(range.getSheetIndex()==sheetIndex) {
					databaseRangeIter.remove();
				}
				else if(range.getSheetIndex()>sheetIndex) {
					range.setSheetIndex(range.getSheetIndex()-1);
				}
			}
		}
		return sheet;
	}
}
