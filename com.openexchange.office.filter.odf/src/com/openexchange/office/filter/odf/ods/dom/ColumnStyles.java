/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * 
 * @author sven.jacobiATopen-xchange.com
 * 
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.Iterator;
import java.util.TreeSet;

@SuppressWarnings("serial")
final class ColumnStyles extends TreeSet<ColumnStyle> {

	public ColumnStyles(Sheet sheet) {
		int lastRow = -1;
    	for(Row row:sheet.getRows()) {
    		lastRow = (row.getRow() + row.getRepeated()) - 1;
    		int lastCellColumn = -1;
    		for(Cell cell:row.getCells()) {
    			lastCellColumn = (cell.getColumn() + cell.getRepeated()) - 1;
    			addCellStyle(sheet, row.getDefaultCellStyle(), row.getRepeated(), cell.getColumn(), cell.getRepeated(), cell.getCellStyle());
    		}
			lastCellColumn++;
    		if(lastCellColumn<sheet.getMaxColCount()) {
    			addCellStyle(sheet, row.getDefaultCellStyle(), row.getRepeated(), lastCellColumn, sheet.getMaxColCount() - lastCellColumn, null);
    		}
    	}
    	lastRow++;
    	if(lastRow<Sheet.getMaxRowCount()) {
    		addCellStyle(sheet, null, Sheet.getMaxRowCount() - lastRow, 0, sheet.getMaxColCount(), null);
    	}
	}

	private void addCellStyle(Sheet sheet, String rowStyle, int rowRepeat, int column, int repeated, String cellStyle) {
		if(cellStyle==null) {
			cellStyle = rowStyle;
		}
		if(repeated==1) {
			if(cellStyle==null) {
				final Column col = sheet.getColumn(column, false, false, false);
				if(col!=null) {
					cellStyle = col.getDefaultCellStyle();
				}
			}
			getColumnStyle(column, true, true, true).addStyleUsage(cellStyle, rowRepeat);
		}
		else {
			getColumnStyle(column, true, true, false);
			getColumnStyle((column + repeated) - 1, true, false, true);
			final Iterator<ColumnStyle> columns = subSet(new ColumnStyle(column), true, new ColumnStyle((column + repeated) - 1), true).iterator();
			while(columns.hasNext()) {
				final ColumnStyle nextColumnStyle = columns.next();
				String c = cellStyle;
				if(c==null) {
					final Column col = sheet.getColumn(nextColumnStyle.getMin(), false, false, false);
					if(col!=null) {
						c = col.getDefaultCellStyle();
					}
				}
				nextColumnStyle.addStyleUsage(c, rowRepeat);
			}
		}
	}

	public ColumnStyle getColumnStyle(int c, boolean forceCreate, boolean cutFloor, boolean cutCeiling) {
    	if(isEmpty()) {
    		if(!forceCreate) {
    			return null;
    		}
    		// creating new columns up to c
    		final ColumnStyle newColumn = new ColumnStyle(0, c);
    		add(newColumn);
    	}
    	ColumnStyle retValue = floor(new ColumnStyle(c, c));
    	if((retValue==null)||(retValue.getMax()<c)) {
    		if(!forceCreate) {
    			return null;
    		}
    		// we have to create new columns from end of floorValue up to c
        	final ColumnStyle newColumn = new ColumnStyle(retValue!=null?retValue.getMax()+1:0, c);
        	add(newColumn);
        	retValue = newColumn;
    	}
    	// rows up to c are available
    	if(cutFloor&&retValue.getMin()!=c) {
			final ColumnStyle newColumn = new ColumnStyle(retValue, c, retValue.getMax());
	    	add(newColumn);
	    	retValue.setMax(c-1);
	    	retValue = newColumn;
    	}
    	if(cutCeiling&&(retValue.getMax()>c)) {
			final ColumnStyle newColumn = new ColumnStyle(retValue, c+1, retValue.getMax());
			add(newColumn);
	    	retValue.setMax(c);
    	}
    	return retValue;
    }
}
