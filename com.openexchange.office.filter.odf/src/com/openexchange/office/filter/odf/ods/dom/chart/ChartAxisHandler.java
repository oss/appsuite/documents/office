/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom.chart;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.core.AttributeSet;
import com.openexchange.office.filter.core.chart.ChartAxis;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.DOMBuilder;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class ChartAxisHandler extends DOMBuilder {

    private final ChartContentHandler parentContext;
    private final ElementNS axisEl;

    public ChartAxisHandler(ChartContentHandler parentContext, ElementNS axisEl) {
        super(axisEl, parentContext);
        this.parentContext = parentContext;
        this.axisEl = axisEl;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        OdfOperationDoc.abortOnLowMemory(getFileDom());
        AttributesImpl attrs = new AttributesImpl(attributes);
        ChartAxis axis = (ChartAxis) axisEl.getUserData(ChartContent.USERDATA);
        String styleName = attrs.getValue(ChartContent.ODF_STYLE);

        switch (qName) {
            case "chart:grid":
                if ("major".equals(attrs.getValue("chart:class"))) {
                    ElementNS gridEl = parentContext.getChart().addGrid(axisEl, styleName);
                    gridEl.addAttributes(attributes);
                }
                return this;
            case "chart:title":
                ElementNS titleEl = parentContext.getChart().addTitle(axisEl, styleName);
                titleEl.addAttributes(attributes);

                AttributeSet title = (AttributeSet) titleEl.getUserData(ChartContent.USERDATA);
                title.setAttributes(parentContext.getChart().getStyleAttrs(styleName));

                return new ChartTitleHandler(parentContext, axis.getAxis(), titleEl);
            case "chart:categories":
                return super.startElement(attributes, uri, localName, qName);
            default:
                return this;
        }
    }

}
