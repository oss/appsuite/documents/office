/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobiATopen-xchange.com
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang3.mutable.MutableInt;

@SuppressWarnings("serial")
final public class ColumnStyle extends HashMap<String, MutableInt> implements Comparable<ColumnStyle>{

	private int min;
	private int max;

	private boolean init = false;
	private String maxUsedStyle = null;

	public ColumnStyle(int min) {
		this.min = min;
		this.max = min;
	}

	public ColumnStyle(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public ColumnStyle(ColumnStyle c, int min, int max) {
		this.min = min;
		this.max = max;

		// cloning HashMap
		final Iterator<java.util.Map.Entry<String, MutableInt>> iter = c.entrySet().iterator();
		while(iter.hasNext()) {
			final java.util.Map.Entry<String, MutableInt> entry = iter.next();
			put(entry.getKey(), new MutableInt(entry.getValue().intValue()));
		}
	}

	public void addStyleUsage(String style, int count) {
		final MutableInt val = get(style);
		if(val!=null) {
			val.add(count);
		}
		else {
			put(style, new MutableInt(count));
		}
	}

	public String getMaxUsedStyle() {
		if(!init) {
			int max = 0;
			final Iterator<java.util.Map.Entry<String, MutableInt>> iter = entrySet().iterator();
			while(iter.hasNext()) {
				final java.util.Map.Entry<String, MutableInt> entry = iter.next();
				if(entry.getValue().intValue()>max) {
					max = entry.getValue().intValue();
					maxUsedStyle = entry.getKey();
				}
			}
			init = true;
			clear();
		}
		return maxUsedStyle;
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	@Override
	public int compareTo(ColumnStyle arg0) {
		return min - arg0.getMin();
	}
}
