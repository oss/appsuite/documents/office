/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.StyleManager;

@SuppressWarnings("serial")
public class ContentValidations extends ElementNSImpl implements IElementWriter {

	private final HashMap<Integer, List<ContentValidation>> contentValidationsByHash = new HashMap<Integer, List<ContentValidation>>();
	private final HashMap<String, ContentValidation> contentValidationsByName = new HashMap<String, ContentValidation>();
	private final HashSet<String> usedValidations = new HashSet<String>();

	public ContentValidations(OdfFileDom ownerDocument)
		throws DOMException {
		super(ownerDocument, Namespaces.TABLE, "table:content-validations");
	}

	public ContentValidation getContentValidationByName(String n) {
	    return contentValidationsByName.get(n);
	}

	public ContentValidation getExistingContentValidation(ContentValidation v) {
	    final List<ContentValidation> validations = contentValidationsByHash.get(v.hashCode());
	    if(validations!=null) {
	        for(ContentValidation validation:validations) {
	            if(validation.equals(v)) {
	                return validation;
	            }
	        }
	    }
	    return null;
	}

	public HashSet<String> getUsedValidations() {
	    return usedValidations;
	}

	public ContentValidation addValidation(ContentValidation contentValidation, boolean checkForExistingValidation) {
	    if(checkForExistingValidation) {
	        final ContentValidation existingValidation = getExistingContentValidation(contentValidation);
	        if(existingValidation!=null) {
	            return existingValidation;
	        }
	        // we have to insert a new validation using a unique name;
            int rnd = StyleManager.getRandom();
            while(true) {
                final String newValidationName = Integer.valueOf(rnd++).toString();
                if(!contentValidationsByName.containsKey(newValidationName)) {
                    contentValidation.setName(newValidationName);
                    break;
                }
            }
	    }
	    else if(contentValidationsByName.containsKey(contentValidation.getName())) {
	        return null;
	    }
	    final int key = contentValidation.hashCode();
	    List<ContentValidation> contentValidationList = contentValidationsByHash.get(key);
	    if(contentValidationList==null) {
	        contentValidationList = new ArrayList<ContentValidation>();
	        contentValidationsByHash.put(key, contentValidationList);
	    }
	    contentValidationList.add(contentValidation);
	    contentValidationsByName.put(contentValidation.getName(), contentValidation);
	    usedValidations.add(contentValidation.getName());
	    return contentValidation;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		if(!contentValidationsByName.isEmpty()) {
			output.startElement(getNamespaceURI(), getLocalName(), getNodeName());
			SaxContextHandler.addAttributes(output, getAttributes(), null);
			final Iterator<ContentValidation> contentValidationIter = contentValidationsByName.values().iterator();
			while(contentValidationIter.hasNext()) {
			    final ContentValidation contentValidation = contentValidationIter.next();
			    if(usedValidations.contains(contentValidation.getName())) {
			        contentValidation.writeObject(output);
			    }
			}
			output.endElement(getNamespaceURI(), getLocalName(), getNodeName());
		}
	}

    public static void createContentValidations(SpreadsheetContent content, JSONArray operationQueue, int sheetIndex, List<Pair<CellRefRange, String>> contentValidations)
        throws JSONException {

        final HashMap<String, List<CellRefRange>> ranges = new HashMap<String, List<CellRefRange>>();
        for(Pair<CellRefRange, String> entry:contentValidations) {
            List<CellRefRange> rangeList = ranges.get(entry.getRight());
            if(rangeList==null) {
                rangeList = new ArrayList<CellRefRange>();
                ranges.put(entry.getRight(), rangeList);
            }
            rangeList.add(entry.getLeft());
        }
        for(Entry<String, List<CellRefRange>> validation:ranges.entrySet()) {
            ContentValidation.createContentValidation(content, operationQueue, sheetIndex, validation.getKey(), validation.getValue());
        }
    }
}
