/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xerces.dom.ElementNSImpl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.ColumnRef;
import com.openexchange.office.filter.core.spreadsheet.Interval;
import com.openexchange.office.filter.core.spreadsheet.RowRef;
import com.openexchange.office.filter.odf.ConfigItem;
import com.openexchange.office.filter.odf.ConfigItemMapEntry;
import com.openexchange.office.filter.odf.ConfigItemSet;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.Settings;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.components.TextComponent;
import com.openexchange.office.filter.odf.components.TextFieldComponent;
import com.openexchange.office.filter.odf.components.TextLineBreakComponent;
import com.openexchange.office.filter.odf.components.TextSpan_Base;
import com.openexchange.office.filter.odf.components.TextTabComponent;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawObject;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.draw.IDrawing;
import com.openexchange.office.filter.odf.draw.IDrawingType;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.ods.dom.components.DrawingComponent;
import com.openexchange.office.filter.odf.ods.dom.components.FrameComponent;
import com.openexchange.office.filter.odf.ods.dom.components.ParagraphComponent;
import com.openexchange.office.filter.odf.ods.dom.components.ShapeComponent;
import com.openexchange.office.filter.odf.ods.dom.components.ShapeGroupComponent;
import com.openexchange.office.filter.odf.ods.dom.components.SheetComponent;
import com.openexchange.office.filter.odf.odt.dom.Annotation;
import com.openexchange.office.filter.odf.odt.dom.Text;
import com.openexchange.office.filter.odf.odt.dom.TextField;
import com.openexchange.office.filter.odf.properties.TableProperties;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleTable;

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

public class JsonOperationProducer {

    private JSONArray operationQueue;

    private final OdfOperationDoc opsDoc;
    private final OdfSpreadsheetDocument doc;
    private final SpreadsheetContent content;
    private final StyleManager styleManager;
    private final Settings settings;

    public JsonOperationProducer(OdfOperationDoc opsDoc)
    	throws SAXException {

        this.opsDoc = opsDoc;
        doc = (OdfSpreadsheetDocument)opsDoc.getDocument();
        doc.getStylesDom();
        content = (SpreadsheetContent)doc.getContentDom();
        styleManager = doc.getStyleManager();
        settings = doc.getSettingsDom();
    }

    public JSONObject getDocumentOperations()
    	throws JSONException, SAXException {

    	operationQueue = new JSONArray();
    	createSetDocumentattributesOperation();
        final HashSet<String> createdStyles = new HashSet<String>();
    	styleManager.createInsertStyleOperations(styleManager.getStyles(), "spreadsheet", operationQueue, createdStyles);
    	styleManager.createInsertStyleOperations(styleManager.getAutomaticStyles(true), "spreadsheet", operationQueue, createdStyles);

//    	styles.createInsertStyleOperations(operationQueue, styles.getOfficeStyles(), false, false);
//    	styles.createInsertStyleOperations(operationQueue, content.getAutomaticStyles_DEPRECATED(), true, false);

    	for(int i=0; i<content.getContent().size();i++) {
    		createSheetOperations(content.getContent().get(i), i);
    	}
    	createNamedExpressions(content.getNamedExpressions(false), null);
    	createDatabaseRanges(content.getDatabaseRanges(false));
        final JSONObject operations = new JSONObject(1);
        operations.put("operations", operationQueue);
        return operations;
    }

    private void createSetDocumentattributesOperation()
       	throws JSONException {

    	final JSONObject documentPropsObject = new JSONObject(2);
    	final JSONObject docPropsJson = Tools.writeFilterVersion(documentPropsObject);
        docPropsJson.putOpt(OCKey.FILE_FORMAT.value(), "odf");
    	docPropsJson.put(OCKey.COLS.value(), content.getMaxColumnCount());
    	final int activeSheet = content.getSheetIndex(settings.getActiveSheet());
    	if(activeSheet>=0) {
    		docPropsJson.put(OCKey.ACTIVE_SHEET.value(), activeSheet);
    	}
    	final ConfigItemSet configItemSet = settings.getConfigItemSet("ooo:configuration-settings", false);
    	if(configItemSet!=null) {
    		final HashMap<String, IElementWriter> configItems = configItemSet.getItems();
    		final IElementWriter autoCalculate = configItems.get("AutoCalculate");
    		if(autoCalculate instanceof ConfigItem) {
    			final String val = ((ConfigItem)autoCalculate).getValue();
    			if(val!=null&&val.equals("true")) {
    				docPropsJson.put(OCKey.CALC_ON_LOAD.value(), true);
    			}
    		}
    	}
        documentPropsObject.put(OCKey.DOCUMENT.value(), docPropsJson);
        final JSONObject characterProps = new JSONObject(1);
        characterProps.put(OCKey.FONT_SIZE.value(), 10);
        documentPropsObject.put(OCKey.CHARACTER.value(), characterProps);
        final JSONObject insertComponentObject = new JSONObject(2);
        insertComponentObject.put(OCKey.NAME.value(), OCValue.SET_DOCUMENT_ATTRIBUTES.value());
        insertComponentObject.put(OCKey.ATTRS.value(), documentPropsObject);
        operationQueue.put(insertComponentObject);
    }

    private void createConditionalFormats(ConditionalFormats conditionalFormats, Integer sheetIndex)
    	throws JSONException, SAXException {

        int i = 0;
    	if(conditionalFormats!=null) {
    	    for (Condition condition : conditionalFormats.getConditionalFormatList()) {
    	        operationQueue.put(condition.createCondFormatRuleOperation(doc, sheetIndex, i++));
    	    }
    	}
    }

    private void createNamedExpressions(NamedExpressions namedExpressions, Integer sheetIndex)
    	throws JSONException {

    	if(namedExpressions!=null) {
    		final Iterator<Entry<String, NamedExpression>> namedExpressionIter = namedExpressions.getExpressionList().entrySet().iterator();
    		while(namedExpressionIter.hasNext()) {
    			final NamedExpression namedExpression = namedExpressionIter.next().getValue();
	        	final JSONObject insertComponentObject = new JSONObject(3);
	        	insertComponentObject.put(OCKey.NAME.value(), OCValue.INSERT_NAME.value());
	        	insertComponentObject.put(OCKey.LABEL.value(), namedExpression.getName());
	        	if(namedExpression.getExpression()!=null) {
	        		insertComponentObject.put(OCKey.FORMULA.value(), namedExpression.getExpression());
        			insertComponentObject.put(OCKey.IS_EXPR.value(), true);
	        	}
	        	else {
	        		String cellRange = namedExpression.getCellRangeAddress();
	        		if(cellRange==null) {
	        			cellRange = namedExpression.getBaseCellAddress();
	        		}
        			insertComponentObject.put(OCKey.FORMULA.value(), (cellRange == null) ? "" : ("[" + cellRange + "]"));
    			}
	        	if(namedExpression.getBaseCellAddress()!=null) {
	        		insertComponentObject.put(OCKey.REF.value(), "[" + namedExpression.getBaseCellAddress() + "]");
	        	}
        		if(sheetIndex!=null) {
        			insertComponentObject.put(OCKey.SHEET.value(), sheetIndex);
        		}
	        	operationQueue.put(insertComponentObject);
    		}
    	}
    }

    private void createDatabaseRanges(DatabaseRanges databaseRanges)
    	throws JSONException {

    	if(databaseRanges==null) {
    		return;
    	}
    	final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRanges.getDatabaseRangeList().entrySet().iterator();
    	while(databaseRangeIter.hasNext()) {
    		final DatabaseRange databaseRange = databaseRangeIter.next().getValue();
    		if(!databaseRange.getName().startsWith("__Anonymous_Sheet_DB__") || databaseRange.isDisplayFilterButtons()) {
    			final String tableName = !databaseRange.getName().isEmpty()&&!databaseRange.getName().startsWith("__Anonymous_Sheet_DB__")
    					? databaseRange.getName()
    					: null;
    			final Range sheetRange = databaseRange.getRange();
    			if(sheetRange.getSheetIndex()>=0) {
	    			JSONObject insertComponentObject = new JSONObject(5);
	    			insertComponentObject.put(OCKey.NAME.value(), OCValue.INSERT_TABLE.value());
	    			final int sheetIndex = sheetRange.getSheetIndex();
	    			insertComponentObject.put(OCKey.SHEET.value(), sheetIndex);
	    			if(tableName!=null) {
	    				insertComponentObject.put(OCKey.TABLE.value(), tableName);
	    			}
	    			insertComponentObject.put(OCKey.RANGE.value(), CellRefRange.getCellRefRange(sheetRange.getCellRefRange(true)));
	    			final JSONObject tableAttrs = databaseRange.createTableAttrs();
	    			if(tableAttrs!=null) {
		    			final JSONObject attrs = new JSONObject(1);
		    			attrs.put(OCKey.TABLE.value(), tableAttrs);
		    			insertComponentObject.put(OCKey.ATTRS.value(), attrs);
	    			}
	    			operationQueue.put(insertComponentObject);

	    			final List<ElementNSImpl> databaseRangeChildList = databaseRange.getChilds();
	    			for(ElementNSImpl databaseRangeChild:databaseRangeChildList) {
	    				createDatabaseRangesImpl(databaseRangeChild, insertComponentObject, sheetIndex, tableName);
	    			}
    			}
    		}
    	}
    }

    private void createDatabaseRangesImpl(Node child, JSONObject insertComponentObject, int sheetIndex, String tableName)
    	throws JSONException {

    	while(child!=null) {
			final Node nextChild = child.getNextSibling();
			final MutableInt column = new MutableInt(-1);
			final JSONArray entries = new JSONArray();
			if("filter-condition".equals(child.getLocalName())) {
				createDatabaseColumnEntries(sheetIndex, child, column, new MutableBoolean(false), entries);
    			if(column.intValue()>=0&&entries.length()>0) {
	    			insertComponentObject = new JSONObject(4);
	    			insertComponentObject.put(OCKey.NAME.value(), OCValue.CHANGE_TABLE_COLUMN.value());
	    			insertComponentObject.put(OCKey.SHEET.value(), sheetIndex);
	    			if(tableName!=null) {
	    				insertComponentObject.put(OCKey.TABLE.value(), tableName);
	    			}
                    insertComponentObject.put(OCKey.COL.value(), column.intValue());
                    final JSONObject attrs = new JSONObject(1);
                    final JSONObject filterAttrs = new JSONObject(2);
                    filterAttrs.put(OCKey.TYPE.value(), "discrete");
                    filterAttrs.put(OCKey.ENTRIES.value(), entries);
                    attrs.put(OCKey.FILTER.value(), filterAttrs);
                    insertComponentObject.put(OCKey.ATTRS.value(), attrs);
	    			operationQueue.put(insertComponentObject);
    			}
			} else if ("sort-by".equals(child.getLocalName())) {
			    final String fieldNumber = ((ElementNSImpl)child).getAttributeNS(Namespaces.TABLE, "field-number");
			    final int columnIndex = Integer.parseInt(fieldNumber);
			    String descending = ((ElementNSImpl)child).getAttributeNS(Namespaces.TABLE, "order");
                if (null == descending) {
                    descending = "ascending";
                }
			    insertComponentObject = new JSONObject(4);
                insertComponentObject.put(OCKey.NAME.value(), OCValue.CHANGE_TABLE_COLUMN.value());
                insertComponentObject.put(OCKey.SHEET.value(), sheetIndex);
                if(tableName!=null) {
                    insertComponentObject.put(OCKey.TABLE.value(), tableName);
                }
                insertComponentObject.put(OCKey.COL.value(), columnIndex);
                insertComponentObject.put(OCKey.SORT.value(), true);
                final JSONObject attrs = new JSONObject(1);
                final JSONObject filterAttrs = new JSONObject(2);
                filterAttrs.put(OCKey.TYPE.value(), "value");
                filterAttrs.put(OCKey.DESCENDING.value(), ("descending".equals(descending) ? true : false));
                attrs.put(OCKey.SORT.value(), filterAttrs);
                insertComponentObject.put(OCKey.ATTRS.value(), attrs);
                operationQueue.put(insertComponentObject);
			} else {
        		final Node childChild = child.getFirstChild();
        		if(childChild!=null) {
        			createDatabaseRangesImpl(childChild, insertComponentObject, sheetIndex, tableName);
        		}
			}
			child = nextChild;
		}
    }

    private void createDatabaseColumnEntries(int sheetIndex, Node node, MutableInt column, MutableBoolean setItemFound, JSONArray entries)
    	throws JSONException {

    	if(node instanceof ElementNSImpl) {
    		final String elementName = ((ElementNSImpl)node).getLocalName();
    		if(elementName.equals("filter-condition")) {
                final String fieldNumber = ((ElementNSImpl) node).getAttributeNS(Namespaces.TABLE, "field-number");
                if(!fieldNumber.isEmpty()&&column.intValue()==-1) {
                	column.setValue(Integer.parseInt(fieldNumber));
                }
                final String value = ((ElementNSImpl) node).getAttributeNS(Namespaces.TABLE, "value");
                final String operator = ((ElementNSImpl) node).getAttributeNS(Namespaces.TABLE, "operator");
                if(operator.equals("=")&&!value.isEmpty()) {
                    entries.put(value);
                }
    		}
    		else if(elementName.equals("filter-set-item")) {
    			if(!setItemFound.booleanValue()) {
    				// if set items are used, then we will reset the entries table the first time so we remove eventual values set by the filter-condition
    				setItemFound.setValue(Boolean.TRUE);
    				entries.reset();
    			}
                final String value = ((ElementNSImpl) node).getAttributeNS(Namespaces.TABLE, "value");
                if(!value.isEmpty()) {
                	entries.put(value);
                }
    		}
    		final NodeList childNodes = ((ElementNSImpl)node).getChildNodes();
    		for(int i=0; i<childNodes.getLength(); i++) {
	    		createDatabaseColumnEntries(sheetIndex, childNodes.item(i), column, setItemFound, entries);
    		}
    	}
    }

    private void createSheetOperations(Sheet sheet, int sheetIndex)
    	throws JSONException, SAXException {

    	final JSONObject insertComponentObject = new JSONObject(3);
        insertComponentObject.put(OCKey.NAME.value(), OCValue.INSERT_SHEET.value());
        insertComponentObject.put(OCKey.SHEET.value(), sheetIndex);
        insertComponentObject.put(OCKey.SHEET_NAME.value(), sheet.getName());

        // creating and writing default attr styles...
        final JSONObject attrs = new JSONObject(4);
        createSheetProperties(sheet, attrs);
        if(!attrs.isEmpty()) {
    		insertComponentObject.put(OCKey.ATTRS.value(), attrs);
    	}
        operationQueue.put(insertComponentObject);

        final ColumnStyles columnStyles = new ColumnStyles(sheet);
        createColumnOperations(columnStyles, sheet.getColumns(), sheetIndex);
        final List<Pair<CellRefRange, String>> contentValidations = new ArrayList<Pair<CellRefRange, String>>();
        createRowOperations(sheet, sheetIndex, columnStyles, contentValidations);
        if(!contentValidations.isEmpty()) {
            ContentValidations.createContentValidations(content, operationQueue, sheetIndex, contentValidations);
        }
        createMergeOperations(sheetIndex, sheet.getMergeCells());
        createHyperlinkOperations(sheetIndex, sheet.getHyperlinks());
        createSheetDrawingOperations((SheetComponent)content.getRootComponent(opsDoc, null).getChildComponent(sheetIndex), sheetIndex, null);
        createConditionalFormats(sheet.getConditionalFormats(false), sheetIndex);
        createNamedExpressions(sheet.getNamedExpressions(false), sheetIndex);
    }

    private void createSheetProperties(Sheet sheet, JSONObject attrs)
    	throws JSONException {

    	final JSONObject sheetProperties = new JSONObject(10);
        final String sheetProtection = sheet.getAttributeNS(Namespaces.TABLE, "protected");
        if(sheetProtection.equals("true")) {
        	sheetProperties.put(OCKey.LOCKED.value(), true);
        }
        final String sheetStyle = sheet.getStyleName();
        if(sheetStyle!=null&&!sheetStyle.isEmpty()) {
        	final StyleBase styleBase = styleManager.getStyle(sheetStyle, StyleFamily.TABLE, true);
        	if(styleBase instanceof StyleTable) {
        		final TableProperties tableProperties = ((StyleTable)styleBase).getTableProperties();
        		final String display = tableProperties.getAttribute("table:display");
        		if(display!=null&&display.equals("false")) {
        			sheetProperties.put(OCKey.VISIBLE.value(), false);
        		}
        	}
        }
        final ConfigItemMapEntry globalViewSettings = settings.getGlobalViewSettings(false);
        if(globalViewSettings!=null) {
			final ConfigItemMapEntry sheetViewSettings = settings.getViewSettings(globalViewSettings, sheet.getName(), false);
			final Integer cursorPositionX = Settings.getConfigValueInt(globalViewSettings, sheetViewSettings, "CursorPositionX");
			final Integer cursorPositionY = Settings.getConfigValueInt(globalViewSettings, sheetViewSettings, "CursorPositionY");
			if(cursorPositionX!=null&&cursorPositionY!=null) {
				final CellRef cellRef = new CellRef(cursorPositionX, cursorPositionY);
                sheetProperties.put(OCKey.ACTIVE_CELL.value(), CellRef.getCellRef(cellRef));
				sheetProperties.put(OCKey.SELECTED_RANGES.value(), CellRef.getCellRef(cellRef));
				sheetProperties.put(OCKey.ACTIVE_INDEX.value(), 0);
			}
			final Integer zoomValue = Settings.getConfigValueInt(globalViewSettings, sheetViewSettings, "ZoomValue");
			if(zoomValue!=null) {
				sheetProperties.put(OCKey.ZOOM.value(), zoomValue / 100.0);
			}
			final Integer positionX1 = Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "PositionLeft", 0);
			if(positionX1>0) {
				sheetProperties.put(OCKey.SCROLL_LEFT.value(), positionX1);
			}
			final Integer positionY1 = Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "PositionTop", 0);
			if(positionY1>0) {
				sheetProperties.put(OCKey.SCROLL_TOP.value(), positionY1);
			}
			final Integer positionX2 = Settings.getConfigValueInt(globalViewSettings, sheetViewSettings, "PositionRight");
			if(positionX2!=null) {
				sheetProperties.put(OCKey.SCROLL_RIGHT.value(), positionX2);
			}
			final Integer positionY2 = Settings.getConfigValueInt(globalViewSettings, sheetViewSettings, "PositionBottom");
			if(positionY2!=null) {
				sheetProperties.put(OCKey.SCROLL_BOTTOM.value(), positionY2);
			}
			final String showGrid = Settings.getConfigValue(globalViewSettings, sheetViewSettings, "ShowGrid");
			if(showGrid!=null&&showGrid.equals("false")) {
				sheetProperties.put(OCKey.SHOW_GRID.value(), false);
			}
			final Integer horizontalSplitMode = Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "HorizontalSplitMode", 0);
            final Integer verticalSplitMode = Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "VerticalSplitMode", 0);
            if(horizontalSplitMode==2||verticalSplitMode==2) {
                sheetProperties.put(OCKey.SPLIT_MODE.value(), "frozen");
                if(horizontalSplitMode==2) {
                    final Integer horizontalSplitPosition = Settings.getConfigValueInt(globalViewSettings, sheetViewSettings, "HorizontalSplitPosition");
                    if(horizontalSplitPosition!=null) {
                        sheetProperties.put(OCKey.SPLIT_WIDTH.value(), horizontalSplitPosition.intValue() - positionX1);
                    }
                }
                if(verticalSplitMode==2) {
                    final Integer verticalSplitPosition = Settings.getConfigValueInt(globalViewSettings, sheetViewSettings, "VerticalSplitPosition");
                    if(verticalSplitPosition!=null) {
                        sheetProperties.put(OCKey.SPLIT_HEIGHT.value(), verticalSplitPosition.intValue() - positionY1);
                    }
                }
            }
            else if(horizontalSplitMode==1||verticalSplitMode==1) {
                sheetProperties.put(OCKey.SPLIT_MODE.value(), "split");
                if(horizontalSplitMode.intValue()==1) {
                    final Integer horizontalSplitPosition = Settings.getConfigValueInt(globalViewSettings, sheetViewSettings, "HorizontalSplitPosition");
                    if(horizontalSplitPosition!=null) {
                        sheetProperties.put(OCKey.SPLIT_WIDTH.value(), (horizontalSplitPosition.intValue() * 2540) / 96);
                    }
                }
                if(verticalSplitMode.intValue()==1) {
                    final Integer verticalSplitPosition = Settings.getConfigValueInt(globalViewSettings, sheetViewSettings, "VerticalSplitPosition");
                    if(verticalSplitPosition!=null) {
                        sheetProperties.put(OCKey.SPLIT_HEIGHT.value(), (verticalSplitPosition.intValue() * 2540) / 96);
                    }
                }
            }
            else {
                sheetProperties.put(OCKey.SPLIT_MODE.value(), "split");
            }
            final Integer activeSplitRange = Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "ActiveSplitRange", 2);
            final String activePane;
            if(activeSplitRange==0) {
                activePane = "topLeft";
            }
            else if(activeSplitRange==1)  {
                activePane = "topRight";
            }
            else if(activeSplitRange==2) {
                activePane = "bottomLeft";
            }
            else {
                activePane = "bottomRight";
            }
            sheetProperties.put(OCKey.ACTIVE_PANE.value(), activePane);
        }
        if(!sheetProperties.isEmpty()) {
            attrs.put(OCKey.SHEET.value(), sheetProperties);
        }
        if(!sheet.getColumns().isEmpty()) {
            sheet.getColumns().last().createAttributes(attrs, doc, true);
        }
        final AttrsHash<Row> defaultRowAttrs = getDefaultAttrs(sheet.getRows());
        if(defaultRowAttrs!=null) {
            defaultRowAttrs.getObject().createAttributes(attrs, doc);
        }
    }

    private <T> AttrsHash<T> getDefaultAttrs(TreeSet<? extends IAttrs<T>> objs) {
    	// retrieving the default style (most used styles)
    	AttrsHash<T> maxUsedAttrs = null;
    	final HashMap<AttrsHash<T>, AttrsHash<T>> attrMap = new HashMap<AttrsHash<T>, AttrsHash<T>>();
    	for(IAttrs<T> obj:objs) {

    		final AttrsHash<T> attrHash = obj.createAttrHash();
    		AttrsHash<T> hashEntry = attrMap.get(attrHash);
    		if(hashEntry==null) {
    			attrMap.put(attrHash, attrHash);
    			hashEntry = attrHash;
    		}
    		else {
    			hashEntry.setCount(hashEntry.getCount()+attrHash.getCount());
    		}
			if(maxUsedAttrs==null) {
				maxUsedAttrs = hashEntry;
			}
			else if(hashEntry.getCount()>maxUsedAttrs.getCount()) {
				maxUsedAttrs = hashEntry;
			}
    	}
    	return maxUsedAttrs;
    }

    private void createMergeOperations(int sheetIndex, List<MergeCell> mergeCells)
    	throws JSONException {

        if(mergeCells!=null&&!mergeCells.isEmpty()) {
            final StringBuffer ranges = new StringBuffer();
            for(MergeCell mergeCell:mergeCells) {
                if(ranges.length()!=0) {
                    ranges.append(" ");
                }
                ranges.append(CellRefRange.getCellRefRange(mergeCell.getCellRefRange(true)));
            }
        	final JSONObject addMergeCellsObject = new JSONObject(5);
            addMergeCellsObject.put(OCKey.NAME.value(), OCValue.MERGE_CELLS.value());
            addMergeCellsObject.put(OCKey.SHEET.value(), sheetIndex);
            addMergeCellsObject.put(OCKey.RANGES.value(), ranges.toString());
            addMergeCellsObject.putOpt(OCKey.TYPE.value(), "merge");
            operationQueue.put(addMergeCellsObject);
        }
    }

    private void createHyperlinkOperations(int sheetIndex, List<Hyperlink> hyperlinks)
    	throws JSONException {

    	for(Hyperlink hyperlink:hyperlinks) {
	    	final JSONObject insertHyperlinkObject = new JSONObject(5);
	    	insertHyperlinkObject.put(OCKey.NAME.value(), OCValue.INSERT_HYPERLINK.value());
	    	insertHyperlinkObject.put(OCKey.SHEET.value(), sheetIndex);
	    	insertHyperlinkObject.put(OCKey.RANGES.value(), CellRefRange.getCellRefRange(hyperlink.getCellRefRange(true)));
	    	insertHyperlinkObject.put(OCKey.URL.value(), hyperlink.getUrl());
	        operationQueue.put(insertHyperlinkObject);
    	}
    }

    private void createSheetDrawingOperations(SheetComponent sheetComponent, int sheetIndex, String target)
        throws JSONException, SAXException {

        final ArrayList<Integer> position = new ArrayList<Integer>();
        position.add(sheetIndex);
        OdfComponent component = (OdfComponent)sheetComponent.getNextChildComponent(null, null);
        while(component!=null) {
            final List<Integer> childPosition = new ArrayList<Integer>(position);
            childPosition.add(component.getComponentNumber());
            if(component instanceof DrawingComponent) {
                createDrawingOperations((DrawingComponent)component, childPosition, target);
            }
            component = (OdfComponent)component.getNextComponent();
        }
    }

    public void createDrawingOperations(DrawingComponent drawingComponent, List<Integer> position, String target)
        throws JSONException, SAXException {

        final OpAttrs attrs = new OpAttrs();
        drawingComponent.createJSONAttrs(attrs);
        addInsertDrawingOperation(position, drawingComponent.getType(), attrs, target);
        if(drawingComponent.getType()==DrawingType.CHART) {
            final Shape shape = drawingComponent.getShape();
            ((DrawObject)((DrawFrame)shape).getDrawing()).getChart().createJSONOperations(position, operationQueue);
        }
        OdfComponent child = (OdfComponent)drawingComponent.getNextChildComponent(null, null);
        while(child!=null) {
            final List<Integer> childPosition = new ArrayList<Integer>(position);
            childPosition.add(child.getComponentNumber());
            if(child instanceof ShapeComponent) {
                createShapeOperations((ShapeComponent)child, childPosition, target);
            }
            else if(child instanceof ShapeGroupComponent) {
                createShapeGroupOperations((ShapeGroupComponent)child, childPosition, target);
            }
            else if(child instanceof FrameComponent) {
                createFrameOperations((FrameComponent)child, childPosition, target);
            }
            if(child instanceof ParagraphComponent) {
                createParagraphOperations((ParagraphComponent)child, childPosition, target);
            }
            child = (OdfComponent)child.getNextComponent();
        }
    }

    public void createFrameOperations(FrameComponent frameComponent, List<Integer> position, String target)
        throws JSONException {

        final OpAttrs attrs = new OpAttrs();
        frameComponent.createJSONAttrs(attrs);
        addInsertDrawingOperation(position, ((IDrawingType)frameComponent).getType(), attrs, target);
        IDrawing drawing = frameComponent.getDrawFrame().getDrawing();
        if (drawing.getType() == DrawingType.CHART) {
            ((DrawObject)(drawing)).getChart().createJSONOperations(position, operationQueue);
        }
        OdfComponent child = (OdfComponent)frameComponent.getNextChildComponent(null, null);
        while(child!=null) {
            final List<Integer> childPosition = new ArrayList<Integer>(position);
            childPosition.add(child.getComponentNumber());
            if(child instanceof ParagraphComponent) {
                createParagraphOperations((ParagraphComponent)child, childPosition, target);
            }
            else if(child instanceof ShapeComponent) {
                createShapeOperations((ShapeComponent)child, childPosition, target);
            }
            child = (OdfComponent)child.getNextComponent();
        }
    }

    public void createShapeOperations(ShapeComponent shapeComponent, List<Integer> position, String target)
        throws JSONException {

        final OpAttrs attrs = new OpAttrs();
        shapeComponent.createJSONAttrs(attrs);
        addInsertDrawingOperation(position, ((IDrawingType)shapeComponent).getType(), attrs, target);
        OdfComponent child = (OdfComponent)shapeComponent.getNextChildComponent(null, null);
        while(child!=null) {
            final List<Integer> childPosition = new ArrayList<Integer>(position);
            childPosition.add(child.getComponentNumber());
            if(child instanceof ParagraphComponent) {
                createParagraphOperations((ParagraphComponent)child, childPosition, target);
            }
            child = (OdfComponent)child.getNextComponent();
        }
    }

    public void createShapeGroupOperations(ShapeGroupComponent shapeComponent, List<Integer> position, String target)
        throws JSONException, SAXException {

        final OpAttrs attrs = new OpAttrs();
        shapeComponent.createJSONAttrs(attrs);
        addInsertDrawingOperation(position, ((IDrawingType)shapeComponent).getType(), attrs, target);
        OdfComponent child = (OdfComponent)shapeComponent.getNextChildComponent(null, null);
        while(child!=null) {
            final List<Integer> childPosition = new ArrayList<Integer>(position);
            childPosition.add(child.getComponentNumber());
            if(child instanceof ShapeComponent) {
                createShapeOperations((ShapeComponent)child, childPosition, target);
            }
            else if(child instanceof ShapeGroupComponent) {
                createShapeGroupOperations((ShapeGroupComponent)child, childPosition, target);
            }
            else if(child instanceof FrameComponent) {
                createFrameOperations((FrameComponent)child, childPosition, target);
            }
            child = (OdfComponent)child.getNextComponent();
        }
    }

    public void createParagraphOperations(ParagraphComponent paragraphComponent, List<Integer> paragraphPosition, String target)
        throws JSONException {

        final JSONObject jsonInsertParagraphOperation = new JSONObject(4);
        jsonInsertParagraphOperation.put(OCKey.NAME.value(), OCValue.INSERT_PARAGRAPH.value());
        jsonInsertParagraphOperation.put(OCKey.START.value(), paragraphPosition);
        if(target!=null) {
            jsonInsertParagraphOperation.put(OCKey.TARGET.value(), target);
        }
        final OpAttrs attrs = new OpAttrs();
        paragraphComponent.createJSONAttrs(attrs);
        if(!attrs.isEmpty()) {
            jsonInsertParagraphOperation.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(jsonInsertParagraphOperation);
        OdfComponent paragraphChild = (OdfComponent)paragraphComponent.getNextChildComponent(null, null);
        while(paragraphChild!=null) {
            final List<Integer> textPosition = new ArrayList<Integer>(paragraphPosition);
            textPosition.add(paragraphChild.getComponentNumber());
            if(paragraphChild instanceof TextComponent) {
                createTextInsertOperation(textPosition, target, ((Text)((TextComponent)paragraphChild).getObject()).getText());
            }
            else if(paragraphChild instanceof TextTabComponent) {
                addInsertTabOperation(textPosition, target);
            }
            else if(paragraphChild instanceof TextFieldComponent) {
                addInsertFieldOperation(textPosition, (TextField)paragraphChild.getObject(), target);
            }
            else if(paragraphChild instanceof TextLineBreakComponent) {
                addInsertHardBreakOperation(textPosition, target);
            }
            paragraphChild = (OdfComponent)paragraphChild.getNextComponent();
        }
        paragraphChild = (OdfComponent)paragraphComponent.getNextChildComponent(null, null);
        while(paragraphChild!=null) {
            createTextAttributesOperation((TextSpan_Base)paragraphChild, paragraphPosition, target);
            paragraphChild = (OdfComponent)paragraphChild.getNextComponent();
        }
    }

    public void createTextInsertOperation(List<Integer> position, String target, String text)
        throws JSONException {

        final JSONObject jsonInsertTextOperation = new JSONObject(4);
        jsonInsertTextOperation.put(OCKey.NAME.value(), OCValue.INSERT_TEXT.value());
        jsonInsertTextOperation.put(OCKey.START.value(), position);
        if(target!=null) {
            jsonInsertTextOperation.put(OCKey.TARGET.value(), target);
        }
        jsonInsertTextOperation.put(OCKey.TEXT.value(), text);
        operationQueue.put(jsonInsertTextOperation);
    }

    public void addInsertDrawingOperation(List<Integer> start, DrawingType type, OpAttrs attrs, String target)
        throws JSONException {

        final JSONObject insertDrawingObject = new JSONObject(5);
        insertDrawingObject.put(OCKey.NAME.value(), OCValue.INSERT_DRAWING.value());
        insertDrawingObject.put(OCKey.START.value(), start);
        insertDrawingObject.put(OCKey.TYPE.value(), type.toString());
        if(!attrs.isEmpty()) {
            insertDrawingObject.put(OCKey.ATTRS.value(), attrs);
        }
        if(target!=null) {
            insertDrawingObject.put(OCKey.TARGET.value(), target);
        }
        operationQueue.put(insertDrawingObject);
    }

    public void addInsertTabOperation(final List<Integer> start, String target)
        throws JSONException {

        final JSONObject insertTabObject = new JSONObject(3);
        insertTabObject.put(OCKey.NAME.value(), OCValue.INSERT_TAB.value());
        insertTabObject.put(OCKey.START.value(), start);
        if(target!=null) {
            insertTabObject.put(OCKey.TARGET.value(), target);
        }
        operationQueue.put(insertTabObject);
    }

    public void addInsertFieldOperation(List<Integer> start, TextField field, String target)
        throws JSONException {

        final JSONObject insertFieldObject = new JSONObject(5);
        insertFieldObject.put(OCKey.NAME.value(), OCValue.INSERT_FIELD.value());
        insertFieldObject.put(OCKey.START.value(), start);
        insertFieldObject.put(OCKey.TYPE.value(), field.getType());
        insertFieldObject.put(OCKey.REPRESENTATION.value(), field.getRepresentation());
        if(target!=null) {
            insertFieldObject.put(OCKey.TARGET.value(), target);
        }
        operationQueue.put(insertFieldObject);
    }

    public void addInsertHardBreakOperation(List<Integer> start, String target)
        throws JSONException {

        final JSONObject insertHardBreakObject = new JSONObject(3);
        insertHardBreakObject.put(OCKey.NAME.value(), OCValue.INSERT_HARD_BREAK.value());
        insertHardBreakObject.put(OCKey.START.value(), start);
        if(target!=null) {
            insertHardBreakObject.put(OCKey.TARGET.value(), target);
        }
        operationQueue.put(insertHardBreakObject);
    }

    public void createTextAttributesOperation(TextSpan_Base textSpan_Base, List<Integer> parentPosition, String target)
        throws JSONException {

        final OpAttrs attrs = new OpAttrs();
        textSpan_Base.createJSONAttrs(attrs);
        if(!attrs.isEmpty()) {
            final JSONObject jsonSetAttributesOperation = new JSONObject(5);
            jsonSetAttributesOperation.put(OCKey.NAME.value(), OCValue.SET_ATTRIBUTES.value());

            final List<Integer> start = new ArrayList<Integer>(parentPosition);
            start.add(textSpan_Base.getComponentNumber());
            jsonSetAttributesOperation.put(OCKey.START.value(), start);

            int startComponent = textSpan_Base.getComponentNumber();
            int endComponent   = textSpan_Base.getNextComponentNumber()-1;
            if(endComponent!=startComponent) {
                final List<Integer> end = new ArrayList<Integer>(parentPosition);
                end.add(endComponent);
                jsonSetAttributesOperation.put(OCKey.END.value(), end);
            }
            if(target!=null) {
                jsonSetAttributesOperation.put(OCKey.TARGET.value(), target);
            }
            jsonSetAttributesOperation.put(OCKey.ATTRS.value(), attrs);
            operationQueue.put(jsonSetAttributesOperation);
        }
    }

    public void createInsertCommentOperation(int sheetIndex, CellRef anchor, Annotation annotation)
        throws JSONException {

        // retrieving text from annotation ...
        final StringBuffer textBuffer = new StringBuffer();
        OdfComponent component = (OdfComponent)(new ShapeComponent(opsDoc, new DLNode<Object>(annotation), 0).getNextChildComponent(null, null));
        while(component!=null) {
            if(component instanceof ParagraphComponent) {
                if(textBuffer.length()!=0) {
                    textBuffer.append("\n");
                }
                OdfComponent paragraphChild = (OdfComponent)component.getNextChildComponent(null, null);
                while(paragraphChild!=null) {
                    if(paragraphChild instanceof TextComponent) {
                        textBuffer.append(((Text)((TextComponent)paragraphChild).getObject()).getText());
                    }
                    else if(paragraphChild instanceof TextTabComponent) {
                        textBuffer.append("\t");
                    }
/* Are line breaks possible ?
                    else if(paragraphChild instanceof TextLineBreakComponent) {

                    }
*/
                    paragraphChild = (OdfComponent)paragraphChild.getNextComponent();
                }
            }
            component = (OdfComponent)component.getNextComponent();
        }

        final JSONObject setNoteObject = new JSONObject(7);
        setNoteObject.put(OCKey.NAME.value(), OCValue.INSERT_NOTE.value());
        setNoteObject.put(OCKey.ANCHOR.value(), CellRef.getCellRef(anchor));
        setNoteObject.put(OCKey.SHEET.value(), sheetIndex);

        final String author = annotation.getCreator();
        if(author!=null && author.length()>0) {
            setNoteObject.put(OCKey.AUTHOR.value(), author);
        }

        final String date = annotation.getDate();
        if(date!=null && date.length()>0) {
            setNoteObject.put(OCKey.DATE.value(), date);
        }

        final String text = textBuffer.toString();
        if (text.length()>0) {
        	setNoteObject.put(OCKey.TEXT.value(), text);
        }

        final OpAttrs attrs = new OpAttrs();
        annotation.createAttrs(opsDoc, true, attrs);
        if(!attrs.isEmpty()) {
            setNoteObject.put(OCKey.ATTRS.value(), attrs);
        }
        operationQueue.put(setNoteObject);
    }

    private void createColumnOperations(ColumnStyles columnStyles, TreeSet<Column> columns, int sheetIndex)
    	throws JSONException {

    	final Iterator<ColumnStyle> columnStyleIter = columnStyles.iterator();
    	while(columnStyleIter.hasNext()) {
    		ColumnStyle columnStyle = columnStyleIter.next();
    		final Iterator<Column> columnIter = columns.subSet(new Column(columnStyle.getMin()), true, new Column(columnStyle.getMax()), true).iterator();
    		while(columnIter.hasNext()) {
    			final Column column = columnIter.next();
        		final JSONObject attrs = new JSONObject();
    			column.createAttributes(attrs, doc, false);
    			if(!attrs.isEmpty()||columnStyle.getMaxUsedStyle()!=null) {
    		        final int start = column.getMin();
    		        final int end = column.getMax();
    		        final JSONObject operation = new JSONObject(6);
    		        operation.put(OCKey.NAME.value(), OCValue.CHANGE_COLUMNS.value());
    		        operation.put(OCKey.SHEET.value(), sheetIndex);
    		        operation.put(OCKey.INTERVALS.value(), new Interval(new ColumnRef(start), new ColumnRef(end)).toString());
    		        if(!attrs.isEmpty()) {
    		        	operation.put(OCKey.ATTRS.value(), attrs);
    		        }
    		        if(columnStyle.getMaxUsedStyle()!=null) {
    		        	operation.put(OCKey.S.value(), columnStyle.getMaxUsedStyle());
    		        }
    		        operationQueue.put(operation);
    			}
        	}
    	}
    }

    public void createRowOperations(Sheet sheet, int sheetIndex, ColumnStyles columnStyles, List<Pair<CellRefRange, String>> contentValidations)
    	throws JSONException {

    	final TreeSet<Row> rows = sheet.getRows();
    	if(rows.isEmpty()) {
    		return;
    	}

    	JSONObject	lastRowObject = null;
    	int         lastRowStart = 0;
    	int 		lastRowEnd = 0;
    	String 		lastRowCellStyle = null;
    	JSONObject	lastRowAttrs = null;
    	for(Row row:rows) {
			final JSONObject attrs = new JSONObject();
    		row.createAttributes(attrs, doc);
    		if(!attrs.isEmpty()||row.getDefaultCellStyle()!=null) {
    	        final JSONObject operation = new JSONObject(6);
    	        final int start = row.getRow();
    	        final int end = (start + row.getRepeated())-1;
    	        operation.put(OCKey.NAME.value(), OCValue.CHANGE_ROWS.value());
    	        operation.put(OCKey.SHEET.value(), sheetIndex);
    	        operation.put(OCKey.INTERVALS.value(), new Interval(new RowRef(start), new RowRef(end)).toString());
	        	operation.put(OCKey.ATTRS.value(), attrs);

	        	final String defaultCellStyle = row.getDefaultCellStyle();
    	        if(defaultCellStyle!=null) {
    	        	operation.put(OCKey.S.value(), defaultCellStyle);
    	        }
    	        boolean identicalRowAttrs = false;
    	        if(lastRowObject!=null&&lastRowCellStyle==defaultCellStyle&&lastRowEnd+1==start) {
	        		identicalRowAttrs = lastRowAttrs!=null && attrs.isEqualTo(lastRowAttrs);
    	        }
    	        if(identicalRowAttrs) {
    	            lastRowObject.put(OCKey.INTERVALS.value(), new Interval(new RowRef(lastRowStart), new RowRef(end)).toString());
    	        }
    	        else {
    	        	lastRowObject = operation;
    	        	lastRowStart = start;
        	        operationQueue.put(operation);
    	        }
	        	lastRowEnd = end;
	        	lastRowCellStyle = defaultCellStyle;
	        	lastRowAttrs = attrs;
    		}
    	}

    	JSONObject contents = new JSONObject();

        final TreeSet<Column> columns = sheet.getColumns();
        TreeMap<Integer, Pair<CellRefRange, String>> lastRowContentValidations = null;
    	for(Row row:rows) {
    		final TreeSet<Cell> cells = row.getCells();
            final int rowRepeat = row.getRepeated();
            TreeMap<Integer, Pair<CellRefRange, String>> currentRowContentValidations = null;
    		for(Cell cell:cells) {

    		    // taking care of annotations
    		    final Annotation annotation = cell.getAnnotation();
    		    if(annotation!=null) {
    		        createInsertCommentOperation(sheetIndex, new CellRef(cell.getColumn(), row.getRow()), annotation);
    		    }

    		    // taking care of content validations ...
    		    final CellAttributesEnhanced enhancedCellAttributes = cell.getCellAttributesEnhanced(false);
                if(enhancedCellAttributes!=null) {
                    final String contentValidationName = enhancedCellAttributes.getContentValidationName();
                    if(contentValidationName!=null&&!contentValidationName.isEmpty()) {
                        if(currentRowContentValidations==null) {
                            currentRowContentValidations = new TreeMap<Integer, Pair<CellRefRange, String>>();
                            currentRowContentValidations.put(cell.getColumn(), Pair.of(new CellRefRange(cell.getColumn(), row.getRow(), (cell.getColumn()+cell.getRepeated())-1, (row.getRow()+row.getRepeated())-1), contentValidationName));
                        }
                        else {
                            final Pair<CellRefRange, String> lastEntry = currentRowContentValidations.lastEntry().getValue();
                            final String lastContentValidationName = lastEntry.getRight();
                            final CellRefRange lastRange = lastEntry.getLeft();
                            if(lastContentValidationName.equals(contentValidationName)&&lastRange.getEnd().getColumn()==cell.getColumn()-1) {
                                lastRange.getEnd().setColumn((cell.getColumn()+cell.getRepeated())-1);
                            }
                            else {
                                currentRowContentValidations.put(cell.getColumn(), Pair.of(new CellRefRange(cell.getColumn(), row.getRow(), (cell.getColumn()+cell.getRepeated())-1, (row.getRow()+row.getRepeated())-1), contentValidationName));
                            }
                        }
                    }
                }

                if(row.getDefaultCellStyle()!=null) {
    				contents = createChangeCellOperation(sheetIndex, contents, cell, row.getRow(), rowRepeat, cell.getColumn(), cell.getRepeated(), columnStyles, row.getDefaultCellStyle());
    			}
    			else if(!columns.isEmpty()) {
    				if(cell.getCellStyle()!=null) {
    					contents = createChangeCellOperation(sheetIndex, contents, cell, row.getRow(), rowRepeat, cell.getColumn(), cell.getRepeated(), columnStyles, null);
    				}
    				else {
    					String defaultCellStyle = null;
    					// no row and no cell style ... the column default cell style is necessary
    					final int max = (cell.getColumn()+cell.getRepeated())-1;
    					for(int min = cell.getColumn(); min<=max; ) {
    					    final Column col = columns.floor(new Column(min));
        					if(col.getMax()<min) {
        						// there are no more columns, the last used default cell style is used
        						contents = createChangeCellOperation(sheetIndex, contents, cell, row.getRow(), rowRepeat, min, (max-min)+1, columnStyles, defaultCellStyle);
        						break;
        					}
    						if(col.getDefaultCellStyle()!=null) {
    							defaultCellStyle = col.getDefaultCellStyle();
    						}
    						if(col.getMax()>=max) {
    							contents = createChangeCellOperation(sheetIndex, contents, cell, row.getRow(), rowRepeat, min, (max-min)+1, columnStyles, defaultCellStyle);
        						break;
    						}
							contents = createChangeCellOperation(sheetIndex, contents, cell, row.getRow(), rowRepeat, min, (col.getMax()-min)+1, columnStyles, defaultCellStyle);
							min = col.getMax()+1;
    					}
    				}
    			}
        		else {
        			contents = createChangeCellOperation(sheetIndex, contents, cell, row.getRow(), rowRepeat, cell.getColumn(), cell.getRepeated(), columnStyles, null);
    			}
    		}
    		if(lastRowContentValidations!=null&&currentRowContentValidations!=null) {
    		    // check if we can apply content validations from last row to the current row validations
    		    final Iterator<Pair<CellRefRange, String>> iter = lastRowContentValidations.values().iterator();
    		    while(iter.hasNext()) {
    		        Pair<CellRefRange, String> lastRowValidation = iter.next();
    		        final CellRefRange ref1 = lastRowValidation.getLeft();
    		        final Pair<CellRefRange, String> p = currentRowContentValidations.get(ref1.getStart().getColumn());
    		        if(p!=null) {
    		            final CellRefRange ref2 = p.getLeft();
    		            if(lastRowValidation.getRight()==p.getRight()&&ref1.getEnd().getColumn()==ref2.getEnd().getColumn()&&ref1.getEnd().getRow()==ref2.getStart().getRow()-1) {
    		                ref2.getStart().setRow(ref1.getStart().getRow());
    		                iter.remove();
    		            }
    		        }
    		    }
    		}
    		addContentValidationRow(contentValidations, lastRowContentValidations);
            lastRowContentValidations = currentRowContentValidations;
    	}
    	addContentValidationRow(contentValidations, lastRowContentValidations);
    	addChangeCellsOperation(sheetIndex, contents);
    }

    private void addContentValidationRow(List<Pair<CellRefRange, String>> dest, TreeMap<Integer, Pair<CellRefRange, String>> sourceRow) {
        if(sourceRow!=null) {
            for(Pair<CellRefRange, String> validation:sourceRow.values()) {
                dest.add(validation);
            }
        }
    }

    public void addChangeCellsOperation(int sheetIndex, JSONObject contents)
        throws JSONException {

        if(!contents.isEmpty()) {
            final JSONObject insertCellsObject = new JSONObject(4);
            insertCellsObject.put(OCKey.NAME.value(), OCValue.CHANGE_CELLS.value());
            insertCellsObject.put(OCKey.SHEET.value(), sheetIndex);
            insertCellsObject.put(OCKey.CONTENTS.value(), contents);

            operationQueue.put(insertCellsObject);
        }
    }

    public JSONObject createChangeCellOperation(int sheetIndex, JSONObject contents, Cell cell, int row, int rowRepeat, int column, int cellRepeat, ColumnStyles columnStyles, String defaultCellStyle)
    	throws JSONException {

    	final int min = column;
    	final int max = (column + cellRepeat) - 1;

    	columnStyles.getColumnStyle(min, true, true, false);
    	columnStyles.getColumnStyle(max, true, false, true);

    	final Iterator<ColumnStyle> columnIter = columnStyles.subSet(new ColumnStyle(min), true, new ColumnStyle(max), true).iterator();
    	while(columnIter.hasNext()) {

    		final ColumnStyle columnStyle = columnIter.next();

    		final JSONObject cellData = new JSONObject();

    		int cMin = columnStyle.getMin();
    		int cMax = columnStyle.getMax();

    		if(cMin<min) {
    			cMin = min;
    		}
    		if(cMax>max) {
    			cMax = max;
    		}

    		final int repeat = (cMax - cMin) + 1;

	    	boolean isEmptyCell = true;
	    	if(cell.getCellFormula()!=null) {
	    		isEmptyCell = false;
	    		cellData.put(OCKey.F.value(), cell.getCellFormula());
	    	}
	    	final Object content = cell.getCellContent();
	    	if(content instanceof Cell.ErrorCode) {
	    		isEmptyCell = false;
	    		final String errorCode = ((Cell.ErrorCode)content).getError();
	    		cellData.put(OCKey.E.value(), errorCode!=null ? errorCode : "");
	    	}
	    	else if(content!=null) {
	    		isEmptyCell = false;
    			cellData.put(OCKey.V.value(), content);
	    	}
	    	if(isEmptyCell) {
	    		String s = null;
		    	if(cell.getCellStyle()!=null) {
		    		s = cell.getCellStyle();
		    	}
		    	else if(defaultCellStyle!=null) {
		    		s = defaultCellStyle;
		    	}
		    	if(s!=null&&!s.equals(columnStyle.getMaxUsedStyle())) {
		    		cellData.put(OCKey.S.value(), s);
		    	}
	    	}
	    	else {
	    		String s = null;
		    	if(cell.getCellStyle()!=null) {
		    		s = cell.getCellStyle();
		    	}
		    	else if(defaultCellStyle!=null) {
		    		s = defaultCellStyle;
		    	}
		    	else {
		    		s = columnStyle.getMaxUsedStyle();
		    	}
		    	if(s!=null) {
		    		cellData.put(OCKey.S.value(), s);
		    	}
	    	}
	    	final CellAttributesEnhanced enhancedCellAttributes = cell.getCellAttributesEnhanced(false);
	    	if(enhancedCellAttributes!=null) {
	    	    final String matrixColumnsSpanned = enhancedCellAttributes.getNumberMatrixColumnsSpanned();
	    	    final String matrixRowsSpanned = enhancedCellAttributes.getNumberMatrixRowsSpanned();
	    	    if(matrixColumnsSpanned!=null||matrixRowsSpanned!=null) {
	    	        final int matrixColumns = matrixColumnsSpanned != null ? Integer.parseInt(matrixColumnsSpanned) : 1;
                    final int matrixRows = matrixRowsSpanned != null ? Integer.parseInt(matrixRowsSpanned) : 1;
                    cellData.put(OCKey.MR.value(), CellRefRange.getCellRefRange(cell.getColumn(), row, (cell.getColumn() + matrixColumns) - 1, (row + matrixRows) - 1));
                }
            }
            if(!cellData.isEmpty()) {
                if(repeat > 1 || rowRepeat > 1) {
                    if(contents.length() == opsDoc.getMaxJsonSize()) {
                        addChangeCellsOperation(sheetIndex, contents);
                        contents = new JSONObject();
                    }
                    contents.put(CellRefRange.getCellRefRange(cMin, row, (cMin + repeat) - 1, (row + rowRepeat) - 1), cellData);
                }
                else {
                    if(contents.length() == opsDoc.getMaxJsonSize()) {
                        addChangeCellsOperation(sheetIndex, contents);
                        contents = new JSONObject();
                    }
                    contents.put(CellRef.getCellRef(new CellRef(cMin, row)), cellData);
                }
             }
    	}
    	return contents;
    }

    public static List<Integer> createPosition(int columnNumber, int rowNumber) {

        final List<Integer> position = new ArrayList<Integer>(2);
    	position.add(columnNumber);
    	position.add(rowNumber);
    	return position;
    }
}
