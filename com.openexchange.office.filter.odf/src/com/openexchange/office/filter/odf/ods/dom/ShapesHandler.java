/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.IContentDom;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawFrameHandler;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.draw.ShapeHelper;
import com.openexchange.office.filter.odf.draw.UndefinedShape;

public class ShapesHandler extends SaxContextHandler {

	private final Sheet sheet;
	private boolean contentStyle;

	public ShapesHandler(SheetHandler parentContextHandler, Sheet sheet) {
		super(parentContextHandler);

		this.sheet = sheet;
		contentStyle = getFileDom() instanceof IContentDom;
	}

	@Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        if(qName.equals("draw:frame")) {
            final DrawFrame drawFrame = new DrawFrame(getFileDom().getDocument(), new AttributesImpl(attributes), null, true, contentStyle);
            final Drawing drawing = new Drawing(sheet, null, drawFrame);
            sheet.getDrawings().getContent().add(drawing);
            return new DrawFrameHandler(this, drawFrame);
        }
        final Shape shape = ShapeHelper.getShape(attributes, uri, localName, qName, null, true, contentStyle);
        if(shape!=null) {
            final Drawing drawing = new Drawing(sheet, null, shape);
            sheet.getDrawings().getContent().add(drawing);
            return shape.getContextHandler(this);
        }
        final UndefinedShape undefinedShape = new UndefinedShape(new AttributesImpl(attributes), uri, localName, qName, null, true, contentStyle);
        sheet.getDrawings().getContent().add(undefinedShape);
        return undefinedShape.getContextHandler(this);
	}
}
