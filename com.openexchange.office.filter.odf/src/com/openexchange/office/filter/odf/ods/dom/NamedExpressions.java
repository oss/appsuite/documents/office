/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

@SuppressWarnings("serial")
public class NamedExpressions extends ElementNSImpl implements IElementWriter {

	private final HashMap<String, NamedExpression> expressionList = new HashMap<String, NamedExpression>();

	public NamedExpressions(OdfFileDom ownerDocument)
		throws DOMException {
		super(ownerDocument, Namespaces.TABLE, "table:named-expressions");
	}

	public HashMap<String, NamedExpression> getExpressionList() {
		return expressionList;
	}

	@Override
	public void writeObject(SerializationHandler output)
		throws SAXException {

		if(!expressionList.isEmpty()) {
			SaxContextHandler.startElement(output, getNamespaceURI(), getLocalName(), getNodeName());
			SaxContextHandler.addAttributes(output, getAttributes(), null);
			final Iterator<Entry<String, NamedExpression>> namedExpressionIter = expressionList.entrySet().iterator();
			while(namedExpressionIter.hasNext()) {
				namedExpressionIter.next().getValue().writeObject(output);
			}
			SaxContextHandler.endElement(output, getNamespaceURI(), getLocalName(), getNodeName());
		}
	}
}
