/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class DatabaseRangesHandler extends SaxContextHandler {

	final private DatabaseRanges databaseRanges;

	public DatabaseRangesHandler(SaxContextHandler parentContextHandler, DatabaseRanges databaseRanges) {
		super(parentContextHandler);

		this.databaseRanges = databaseRanges;
	}

	@Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
		if(qName.equals("table:database-range")) {
			DatabaseRange databaseRange = new DatabaseRange((SpreadsheetContent)this.getFileDom(), attributes);
			if(!databaseRange.getName().isEmpty()) {
				databaseRanges.getDatabaseRangeList().put(databaseRange.getName(), databaseRange);
			}
			return new DatabaseRangeHandler(this, databaseRange);
		}
		return this;
	}
}
