/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.TreeSet;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class RowHandler extends SaxContextHandler {

    final Sheet sheet;
    final Row row;

    public RowHandler(SheetHandler sheetHandler, Attributes attributes, Row r) {
        super(sheetHandler);
        sheet = sheetHandler.getSheet();
        row = r;

        for(int i=0; i<attributes.getLength(); i++) {
            final String localName = attributes.getLocalName(i);
            if(localName.equals("number-rows-repeated")) {
                row.setRepeated(Integer.valueOf(attributes.getValue(i)));
            }
            else if(localName.equals("default-cell-style-name")) {
                row.setDefaultCellStyle(attributes.getValue(i));
            }
            else if(localName.equals("visibility")) {
                row.setVisibility(Visibility.enumValueOf(attributes.getValue(i)));
            }
            else if(localName.equals("id")) {
                row.setId(attributes.getValue(i));
            }
        }

        // split first row from the row range, if it starts a new row group ("collapse" must be located on the leading row only)
        if ((row.getRepeated() > 1) && (row.getGroupCollapse() != null)) {
            Row startRow = row.splitBefore(row.getRow() + 1);
            sheet.getRows().add(startRow);
            row.setGroupCollapse(null);
        }
    }

    public Sheet getSheet() {
        return sheet;
    }

    public Row getRow() {
        return row;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        OdfOperationDoc.abortOnLowMemory(getFileDom());
        if(localName.equals("table-cell")||localName.equals("covered-table-cell")) {
            final TreeSet<Cell> cells = row.getCells();
            int currentColumn = 0;
            if(!cells.isEmpty()) {
                final Cell lastCell = cells.last();
                currentColumn = lastCell.getColumn() + lastCell.getRepeated();
            }
            final Cell newCell = new Cell(currentColumn);
            cells.add(newCell);
            return new CellHandler(this, attributes, sheet, row, newCell);
        }
        return this;
    }
}
