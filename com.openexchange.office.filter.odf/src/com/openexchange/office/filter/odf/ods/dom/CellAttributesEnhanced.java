/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.SAXException;

import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

// This class is containing cell attributes that are rarely used

public class CellAttributesEnhanced {

	private String contentValidationName;
	private String numberMatrixRowsSpanned;
	private String numberMatrixColumnsSpanned;
	private String protect;
	private String protectedC;
	
	public CellAttributesEnhanced() {
		contentValidationName = null;
		numberMatrixRowsSpanned = null;
		numberMatrixColumnsSpanned = null;
		protect = null;
		protectedC = null;
	}

	public String getContentValidationName() {
		return contentValidationName;
	}
	public void setContentValidationName(String contentValidationName) {
		this.contentValidationName = contentValidationName;
	}
	public String getNumberMatrixRowsSpanned() {
		return numberMatrixRowsSpanned;
	}
	public void setNumberMatrixRowsSpanned(String numberMatrixRowsSpanned) {
		this.numberMatrixRowsSpanned = numberMatrixRowsSpanned;
	}
	public String getNumberMatrixColumnsSpanned() {
		return numberMatrixColumnsSpanned;
	}
	public void setNumberMatrixColumnsSpanned(String numberMatrixColumnsSpanned) {
		this.numberMatrixColumnsSpanned = numberMatrixColumnsSpanned;
	}
	public String getProtect() {
		return protect;
	}
	public void setProtect(String protect) {
		this.protect = protect;
	}
	public String getProtected() {
		return protectedC;
	}
	public void setProtected(String protectedC) {
		this.protectedC = protectedC;
	}
	public void writeAttributes(SerializationHandler output)
		throws SAXException {

		if(contentValidationName!=null) {
        	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "content-validation-name", "table:content-validation-name", contentValidationName);
		}
		if(numberMatrixRowsSpanned!=null) {
			SaxContextHandler.addAttribute(output, Namespaces.TABLE, "number-matrix-rows-spanned", "table:number-matrix-rows-spanned", numberMatrixRowsSpanned);
		}
		if(numberMatrixColumnsSpanned!=null) {
			SaxContextHandler.addAttribute(output, Namespaces.TABLE, "number-matrix-columns-spanned", "table:number-matrix-columns-spanned", numberMatrixColumnsSpanned);
		}
		if(protect!=null) {
			SaxContextHandler.addAttribute(output, Namespaces.TABLE, "protect", "table:protect", protect);
		}
		if(protectedC!=null) {
			SaxContextHandler.addAttribute(output, Namespaces.TABLE, "protected", "table:protected", protectedC);
		}
	}
}
