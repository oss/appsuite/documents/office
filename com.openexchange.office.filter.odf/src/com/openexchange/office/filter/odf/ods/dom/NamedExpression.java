/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class NamedExpression {

	private String 	name;
	private String 	baseCellAddress;
	private String	expression;
	private String	cellRangeAddress;
	private String	rangeUsableAs;

	public NamedExpression(String name) {
		this.name = name;
	}

	public NamedExpression(Attributes attributes) {
		name = attributes.getValue("table:name");
		baseCellAddress = attributes.getValue("table:base-cell-address");
		expression = attributes.getValue("table:expression");
		cellRangeAddress = attributes.getValue("table:cell-range-address");
		rangeUsableAs = attributes.getValue("table:range-usable-as");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBaseCellAddress() {
		return baseCellAddress;
	}

	public void setBaseCellAddress(String baseCellAddress) {
		this.baseCellAddress = baseCellAddress;
	}

	public String getCellRangeAddress() {
		return cellRangeAddress;
	}

	public void setCellRangeAddress(String cellRangeAddress) {
		this.cellRangeAddress = cellRangeAddress;
		expression = null;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
		cellRangeAddress = null;
		rangeUsableAs = null;
	}

	public void writeObject(SerializationHandler output)
		throws SAXException {

		if(expression!=null) {
			SaxContextHandler.startElement(output, Namespaces.TABLE, "named-expression", "table:named-expression");
		}
		else {
			SaxContextHandler.startElement(output, Namespaces.TABLE, "named-range", "table:named-range");
		}
    	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "name", "table:name", name);
    	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "base-cell-address", "table:base-cell-address", baseCellAddress==null?"":baseCellAddress);
    	if(expression!=null) {
        	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "expression", "table:expression", expression);
    	}
    	else {
        	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "cell-range-address", "table:cell-range-address", cellRangeAddress==null?"":cellRangeAddress);
        	if(rangeUsableAs!=null) {
        		SaxContextHandler.addAttribute(output, Namespaces.TABLE, "range-usable-as", "table:range-usable-as", rangeUsableAs);
        	}
    	}
		if(expression!=null) {
			SaxContextHandler.endElement(output, Namespaces.TABLE, "named-expression", "table:named-expression");
		}
		else {
			SaxContextHandler.endElement(output, Namespaces.TABLE, "named-range", "table:named-range");
		}
	}
}
