/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom.chart;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.dom.OdfDocumentNamespace;
import org.odftoolkit.odfdom.pkg.NamespaceName;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.odftoolkit.odfdom.pkg.OdfPackageDocument;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.AttributeSet;
import com.openexchange.office.filter.core.chart.Chart;
import com.openexchange.office.filter.core.chart.ChartAxis;
import com.openexchange.office.filter.core.chart.ChartSeries;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.styles.AutomaticStyles;
import com.openexchange.office.filter.odf.styles.StyleChart;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class ChartContent extends OdfFileDom {

    public static final String USERDATA   = "userdata";
    public static final String ODF_STYLE  = "chart:style-name";
    public static final String ODF_LEGEND = "legend-position";

    public static final String ODF_VALUES           = "values-cell-range-address";
    public static final String ODF_TITLE            = "label-cell-address";
    public static final String ODF_CELL_RANGE       = "cell-range-address";
    public static final String ODF_TABLE_CELL_RANGE = "table:cell-range-address";

    public static final String CHARTSTYLEID = "ooxml-style";

    public static final Map<String, String> LEGENDPOS_ODF_TO_OP;
    public static final Map<String, String> LEGENDPOS_OP_TO_ODF;

    public static final List<String> MARKERS = Collections.unmodifiableList(Arrays.asList(new String[] { "circle", "square", "arrow-up", "x" }));

    static {
        HashMap<String, String> odfToOp = new HashMap<>();
        HashMap<String, String> opToOdf = new HashMap<>();

        opToOdf.put(OCKey.TOP.value(), "top");
        opToOdf.put(OCKey.BOTTOM.value(), "bottom");
        opToOdf.put(OCKey.LEFT.value(), "start");
        opToOdf.put(OCKey.RIGHT.value(), "end");
        opToOdf.put(OCKey.TOP_RIGHT.value(), "top-end");
        opToOdf.put(OCKey.TOP_LEFT.value(), "top-start");
        opToOdf.put(OCKey.BOTTOM_RIGHT.value(), "bottom-end");
        opToOdf.put(OCKey.BOTTOM_LEFT.value(), "bottom-start");

        for (Entry<String, String> e : opToOdf.entrySet()) {
            odfToOp.put(e.getValue(), e.getKey());
        }

        LEGENDPOS_ODF_TO_OP = Collections.unmodifiableMap(odfToOp);
        LEGENDPOS_OP_TO_ODF = Collections.unmodifiableMap(opToOdf);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    private Chart     chart;
    private ElementNS plotArea;
    private ElementNS chartArea;

    public ChartContent(OdfPackageDocument packageDocument, String packagePath) throws SAXException {
        super(packageDocument, packagePath);
    }

    @Override
    protected void initialize() throws SAXException {
        for (NamespaceName name : OdfDocumentNamespace.values()) {
            setNamespace(name.getPrefix(), name.getUri());
        }

        final StyleManager styleManager = getDocument().getStyleManager();
        styleManager.setAutomaticStyles(new AutomaticStyles(this), true);

        final XMLReader xmlReader = mPackage.getXMLReader();
        super.initialize(new ChartContentHandler(this, xmlReader), xmlReader);

        final Node root = getRootElement();
        root.insertBefore(styleManager.getAutomaticStyles(true), root.getFirstChild());

    }

    public Chart getChart() {
        if (chart == null) {
            chart = new Chart();
        }
        return chart;
    }

    public StyleChart getPlotAreaStyle() {
        return getStyleChart(getOrCreateStyle(plotArea));
    }

    private ElementNS createSeries(String styleName) {
        return createBoundElement(new ChartSeries(), "series", styleName);
    }

    private ElementNS createAxis(String styleName, Integer axisId) {
        ChartAxis ax = chart.getAxis(axisId);
        ElementNS res = createBoundElement(ax, "axis", styleName);

        setAttribute(res, "class", "major");
        setAttribute(res, "dimension", getDimension(axisId));
        setAttribute(res, "name", getAxisName(ax));
        return res;
    }

    private String getDimension(Integer axisId) {
        String dimension;
        if (axisId == 0 || axisId == 2) {
            dimension = "x";
        } else if (axisId == 1 || axisId == 3) {
            dimension = "y";
        } else {
            // TODO Z-Axis
            dimension = "z";
        }
        if (chart.isBar()) {
            if (dimension.equals("x")) {
                dimension = "y";
            } else if (dimension.equals("y")) {
                dimension = "x";
            }
        }
        return dimension;
    }

    private ElementNS createBoundElement(AttributeSet attrs, String name, String styleName) {
        ElementNS el = newChartElement(name);
        if (attrs != null) {
            el.setUserData(USERDATA, attrs, null);
        }

        if (!StringUtils.isEmpty(styleName)) {
            setAttribute(el, "style-name", styleName);
        }
        return el;
    }

    public ElementNS newChartElement(String name) {
        return newElement(OdfDocumentNamespace.CHART, name);
    }

    public ElementNS newElement(OdfDocumentNamespace namespace, String name) {
        return new ElementNS(this, namespace.getUri(), namespace.getPrefix() + ":" + name);
    }

    public void insertSeries(int seriesIndex, JSONObject attrs) {
        if (seriesIndex == 0) {
            for (ChartAxis axis : getChart().getAxes()) {
                ElementNS node = getChildByUserData(plotArea, axis);
                deleteElement(node);
            }
            getChart().removeAxes();
        }
        ElementNS seriesEl = createSeries(null);
        ChartSeries serie = (ChartSeries) seriesEl.getUserData(USERDATA);

        ChartSeries before = getChart().insertDataSeries(seriesIndex, serie);
        if (before != null) {
            Node bef = getChildByUserData(plotArea, before);
            plotArea.insertBefore(seriesEl, bef);
        } else {
            plotArea.appendChild(seriesEl);
        }

        changeSeries(seriesIndex, attrs, false);

        handleAxisDirections(attrs);
    }

    public ElementNS addDataSeries(String type, String values, String title, String styleName, int axisXIndex, int axisYIndex, Integer axisZIndex, boolean chart3d) {
        ElementNS seriesEl = createSeries(styleName);

        ChartSeries serie = (ChartSeries) seriesEl.getUserData(USERDATA);
        serie.setSeriesAttributes(type, values, title, axisXIndex, axisYIndex, axisZIndex, chart3d);

        plotArea.appendChild(seriesEl);

        getChart().addDataSeries(serie);

        implSetDataSeriesAttributes(getChart().getSeries().size() - 1, getStyleAttrs(seriesEl), null);

        return seriesEl;
    }

    private ElementNS implSetDataSeriesAttributes(int seriesIndex, JSONObject attrs, String symbol) {
        ChartSeries serie = getChart().setDataSeriesAttributes(seriesIndex, attrs);
        ElementNS node = getChildByUserData(plotArea, serie);

        applyStyleAttrs(serie.getAttributes(), node);
        applySeriesAttrs(serie.getOrCreate(OCKey.SERIES.value()), node, symbol);

        return node;
    }

    /**
     * 
     * @param seriesIndex
     * @param attrs
     * @param updateSeriesAttributes true if update the attributes from an existing dataseries, otherwise false to set the series attributes from a new dataseries
     */
    public void changeSeries(int seriesIndex, JSONObject attrs, boolean updateSeriesAttributes) {

        String symbol = "automatic";
        // true if the marker is changed by a attribute
        boolean markerAttributeChange = false;

        if (attrs.hasAndNotNull(OCKey.SERIES.value())) {
            JSONObject series = attrs.optJSONObject(OCKey.SERIES.value());

            if (series.hasAndNotNull(OCKey.DATA_POINTS.value()) && !series.optJSONArray(OCKey.DATA_POINTS.value()).isEmpty() && series.optJSONArray(OCKey.DATA_POINTS.value()).optJSONObject(0) != null && !series.optJSONArray(OCKey.DATA_POINTS.value()).optJSONObject(0).isEmpty()) {
                // workaround for Bug 51731
                JSONObject fdp = series.optJSONArray(OCKey.DATA_POINTS.value()).optJSONObject(0);
                JSONObject marker = fdp != null && fdp.has(OCKey.MARKER_FILL.value()) ? fdp.optJSONObject(OCKey.MARKER_FILL.value()) : null;
                if (marker == null || StringUtils.equals(marker.optString(OCKey.TYPE.value()), "none")) {
                    symbol = "none";
                    markerAttributeChange = true;
                }
            } else if (series.has(OCKey.MARKER_FILL.value())) {
                if (StringUtils.equals(series.optJSONObject(OCKey.MARKER_FILL.value()).optString(OCKey.TYPE.value()), "none")) {
                    symbol = "none";
                    markerAttributeChange = true;
                }
            } else if (attrs.has(OCKey.MARKER_FILL.value())) {
                if (StringUtils.equals(attrs.optJSONObject(OCKey.MARKER_FILL.value()).optString(OCKey.TYPE.value()), "none")) {
                    symbol = "none";
                    markerAttributeChange = true;
                }
            } else {
                symbol = "none";
            }

            if (series.has(OCKey.VALUES.value()) && !series.has(OCKey.NAMES.value())) {
                series.putSafe(OCKey.NAMES.value(), "");
            }
        }

        ElementNS seriesNode = implSetDataSeriesAttributes(seriesIndex, attrs, symbol);
        if (markerAttributeChange || !updateSeriesAttributes) {
            setChartStyleAttribute(seriesNode, "symbol-type", symbol);
        }

        handleAxisDirections(attrs);
    }

    public ElementNS addAxis(Integer axisId, String styleName) {
        ElementNS axisEl = createAxis(styleName, axisId);

        ChartAxis axis = (ChartAxis) axisEl.getUserData(USERDATA);
        axis.setAxis(axisId);

        NodeList otherAxes = plotArea.getElementsByTagName("chart:axis");
        Node refChild = null;
        if (otherAxes.getLength() > 0) {
            refChild = otherAxes.item(otherAxes.getLength() - 1).getNextSibling();
        }
        if (refChild != null) {
            plotArea.insertBefore(axisEl, refChild);
        } else {
            plotArea.appendChild(axisEl);
        }

        getChart().setAxis(axis);

        changeAxis(axisId, getStyleAttrs(axisEl));

        return axisEl;
    }

    public ElementNS addGrid(ElementNS axis, String styleName) {
        ChartAxis ax = (ChartAxis) axis.getUserData(USERDATA);

        ElementNS gridEl = createBoundElement(ax.getGridLine(), "grid", styleName);
        setAttribute(gridEl, "class", "major");

        axis.appendChild(gridEl);

        changeGrid(ax.getAxis(), getStyleAttrs(gridEl));

        return gridEl;
    }

    public ElementNS addTitle(ElementNS axis, String styleName) {
        ChartAxis ax = (ChartAxis) axis.getUserData(USERDATA);

        ElementNS titleEl = createBoundElement(ax.getTitle(), "title", styleName);

        axis.appendChild(titleEl);

        changeTitle(ax.getAxis(), getStyleAttrs(titleEl));

        return titleEl;
    }

    public ElementNS addMainTitle(String styleName) {
        ElementNS titleEl = createBoundElement(getChart().getTitle(null), "title", styleName);

        chartArea.appendChild(titleEl);

        changeTitle(null, getStyleAttrs(titleEl));

        return titleEl;
    }

    public ElementNS addLegend(String styleName) {
        ElementNS legendEl = createBoundElement(getChart().getLegend(), "legend", styleName);

        // AttributeSet legend = (AttributeSet) legendEl.getUserData(USERDATA);

        chartArea.appendChild(legendEl);

        changeLegend(getStyleAttrs(legendEl));

        return legendEl;
    }

    public void deleteSeries(int seriesIndex) {
        ChartSeries old = getChart().deleteDataSeries(seriesIndex);
        ElementNS node = getChildByUserData(plotArea, old);
        handleDataPoints(node, new JSONArray(), null);
        deleteElement(node);
    }

    public void setPlotArea(ElementNS plotArea) {
        this.plotArea = plotArea;
    }

    public void setChartArea(ElementNS chartArea) {
        this.chartArea = chartArea;
    }

    public ElementNS getPlotArea() {
        return plotArea;
    }

    public ElementNS getChartArea() {
        return chartArea;
    }

    public void changeAxis(Integer axisId, JSONObject attrs) {
        AttributeSet ax = getAxis(axisId);
        ax.setAttributes(attrs);
        ElementNS node = getChildByUserData(plotArea, ax);

        applyAxisAttrs(ax.getOrCreate(OCKey.AXIS.value()), node);
        applyStyleAttrs(ax.getAttributes(), node);
    }

    private ChartAxis getAxis(Integer axisId) {
        ChartAxis ax = getChart().getAxis(axisId);
        ElementNS node = getChildByUserData(plotArea, ax);
        if (node == null) {
            node = addAxis(axisId, null);
        }
        return ax;
    }

    public void changeGrid(Integer axisId, JSONObject attrs) {
        ChartAxis ax = getAxis(axisId);
        AttributeSet grid = ax.getGridLine();
        grid.setAttributes(attrs);

        ElementNS axEl = getChildByUserData(plotArea, ax);
        ElementNS gridEl = getChildByUserData(axEl, grid);

        if (gridEl == null) {
            gridEl = addGrid(axEl, null);
        }

        applyStyleAttrs(grid.getAttributes(), gridEl);
    }

    public void changeTitle(Integer axisId, JSONObject attrs) {

        AttributeSet title = getChart().getTitle(axisId);
        title.setAttributes(attrs);

        ElementNS titleEl = null;

        if (axisId == null) {
            titleEl = getChildByUserData(chartArea, title);
            if (titleEl == null) {
                titleEl = addMainTitle(null);
            }
        } else {
            ChartAxis ax = getAxis(axisId);
            ElementNS axEl = getChildByUserData(plotArea, ax);
            titleEl = getChildByUserData(axEl, title);

            if (titleEl == null) {
                titleEl = addTitle(axEl, null);
            }
        }

        applyStyleAttrs(title.getAttributes(), titleEl);

        if (attrs.has(OCKey.TEXT.value())) {
            JSONObject text = attrs.optJSONObject(OCKey.TEXT.value());

            Node child = titleEl.getFirstChild();
            if (text == null) {
                if (child != null) {
                    titleEl.removeChild(child);
                }
            } else {
                if (child == null) {
                    child = new ElementNS(this, "", "text:p");
                    titleEl.appendChild(child);
                }
                JSONArray link = text.optJSONArray(OCKey.LINK.value());
                if (link != null && link.length() > 0) {
                    child.setTextContent(link.optString(0));
                } else {
                    child.setTextContent("");
                }
            }
        }

    }

    public void changeLegend(JSONObject attrs) {
        AttributeSet legend = getChart().getLegend();

        ElementNS legendEl = getChildByUserData(chartArea, legend);
        if (legendEl == null) {
            legendEl = createBoundElement(legend, "legend", null);
            chartArea.appendChild(legendEl);
        }

        legend.setAttributes(attrs);
        JSONObject legendAttrs = legend.getOrCreate(OCKey.LEGEND.value());
        if ("off".equals(legendAttrs.optString(OCKey.POS.value()))) {
            deleteElement(legendEl);
        } else {
            applyLegendAttrs(legendAttrs, legendEl);
            applyStyleAttrs(legend.getAttributes(), legendEl);
        }
    }

    private void deleteElement(ElementNS el) {
        el.getParentNode().removeChild(el);

        String styleName = el.getAttribute(ChartContent.ODF_STYLE);
        if (StringUtils.isNotEmpty(styleName)) {
            StyleManager styleManager = getDocument().getStyleManager();
            styleManager.removeStyle(StyleFamily.CHART, styleName, true, false);
        }
    }

    public JSONObject getStyleAttrs(String styleName) {
        if (StringUtils.isEmpty(styleName)) {
            // throw new RuntimeException("stylename is null!!!");
            return new JSONObject();
        }

        StyleChart style = getStyleChart(styleName);

        if (style == null) {
            throw new RuntimeException("no style found for " + styleName);
        }

        OpAttrs attrs = new OpAttrs();
        style.createAttrs(getDocument().getStyleManager(), attrs);

        //FIXME: hacks hacks hacks
        if (attrs.containsKey(OCKey.FILL.value()) && !attrs.getMap(OCKey.FILL.value(), false).containsKey(OCKey.TYPE.value())) {
            attrs.getMap(OCKey.FILL.value(), false).put(OCKey.TYPE.value(), "solid");
        }
        if (attrs.containsKey(OCKey.LINE.value()) && !attrs.getMap(OCKey.LINE.value(), false).containsKey(OCKey.TYPE.value())) {
            attrs.getMap(OCKey.LINE.value(), false).put(OCKey.TYPE.value(), "solid");
        }

        return new JSONObject(attrs);
    }

    public StyleChart getStyleChart(String styleName) {
        StyleManager styleManager = getDocument().getStyleManager();
        return (StyleChart) styleManager.getStyle(styleName, StyleFamily.CHART, true);
    }

    public void applyDrawingAttributes(JSONObject attrs) {
        JSONObject json = attrs.optJSONObject(OCKey.CHART.value());
        for (Entry<String, Object> e : json.entrySet()) {
            String key = e.getKey();
            String value = e.getValue().toString();

            if (OCKey.CHART_STYLE_ID.value().equals(key)) {
                setAttribute(plotArea, CHARTSTYLEID, value);
            } else if (OCKey.STACKING.value().equals(key)) {
                if ("percentStacked".equals(value)) {
                    setChartStyleAttribute(plotArea, "percentage", true);
                    setChartStyleAttribute(plotArea, "stacked", null);
                } else if ("stacked".equals(value)) {
                    setChartStyleAttribute(plotArea, "percentage", null);
                    setChartStyleAttribute(plotArea, "stacked", true);
                } else {
                    setChartStyleAttribute(plotArea, "percentage", null);
                    setChartStyleAttribute(plotArea, "stacked", null);
                }
            } else if (OCKey.CURVED.value().equals(key)) {
                if ("true".equals(value)) {
                    setChartStyleAttribute(plotArea, "interpolation", "cubic-spline");
                } else {
                    setChartStyleAttribute(plotArea, "interpolation", null);
                }
            } else if (OCKey.VARY_COLORS.value().equals(key)) {
                // ignore varyColors
            } else if (OCKey.ROTATION.value().equals(key)) {
                setChartStyleAttribute(plotArea, "angle-offset", value);
            }
        }
        applyStyleAttrs(attrs, chartArea);
        setChartStyleAttribute(plotArea, "auto-position", "true");
        
        try {
            if (attrs.has(OCKey.CHART.value())) {
                JSONObject chartAttrs = attrs.getJSONObject(OCKey.CHART.value());
                if (chartAttrs.has(OCKey.SHOW_ONLY_VISIBLE_CELLS_DATA.value())) {
                    setChartStyleAttribute(plotArea, "include-hidden-cells", !chartAttrs.getBoolean(OCKey.SHOW_ONLY_VISIBLE_CELLS_DATA.value()));
                }
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

    }

    private void applySeriesAttrs(JSONObject json, ElementNS el, String symbol) {
        boolean chart3D = false;
        for (Entry<String, Object> e : json.entrySet()) {
            String key = e.getKey();
            String value = null;
            if (e.getValue() != JSONObject.NULL) {
                value = e.getValue().toString();
            }

            if (OCKey.TYPE.value().equals(key)) {
                final String chartAreaType;

                //vertical attribute specifies whether the x-axis in a Cartesian 
                //coordinate system is oriented horizontally or vertically.
                //only for bar and column chart
                boolean vertical = false;

                if ("column".equals(value)) {
                    value = "bar";
                    chartAreaType = "bar";
                } else if ("bar".equals(value)) {
                    chartAreaType = "bar";
                    vertical = true;
                } else if ("pie".equals(value)) {
                    value = "circle";
                    chartAreaType = value;
                } else if ("donut".equals(value)) {
                    value = "circle";
                    chartAreaType = "ring";
                } else if ("stock".equals(value)) {
                    chartAreaType = value;
                    value = null;
                } else if ("line".equals(value)) {
                    chartAreaType = value;
                } else if ("scatter".equals(value)) {
                    chartAreaType = value;
                } else if ("bubble".equals(value)) {
                    chartAreaType = value;
                } else {
                    chartAreaType = null;
                }
                if (chartAreaType != null) {
                    setAttribute(chartArea, "class", "chart:" + chartAreaType);
                } else {
                    setAttribute(chartArea, "class", null);
                }
                if (value != null) {
                    setAttribute(el, "class", "chart:" + value);
                } else {
                    setAttribute(el, "class", null);
                }

                getPlotAreaStyle().getChartProperties().getAttributes().setBooleanValue(OdfDocumentNamespace.CHART.getUri(), "vertical", "chart:vertical", vertical);

            } else if (OCKey.DATA_POINTS.value().equals(key)) {
                handleDataPoints(el, json.optJSONArray(key), symbol);
            } else if (OCKey.VALUES.value().equals(key)) {
                setAttribute(el, ODF_VALUES, fromFormula(value));
            } else if (OCKey.TITLE.value().equals(key)) {
                setAttribute(el, ODF_TITLE, fromFormula(value));
            } else if (OCKey.BUBBLES.value().equals(key)) {
                setChartDomainAttribute(el, fromFormula(value), 0);
            } else if (OCKey.NAMES.value().equals(key)) {
                setNamesAttribute(el, fromFormula(value), chart.isBubble() ? 1 : 0);
            } else if (OCKey.DATA_LABEL.value().equals(key)) {
                String number;
                String numberValue = value;
                if (e.getValue() instanceof JSONObject) {
                    JSONObject dataLabelJsonObject = (JSONObject) e.getValue();
                    try {
                        
                        if (dataLabelJsonObject.has(OCKey.DATA_LABEL_POS.value())) {
                            String pos = getDataLabelPosition(dataLabelJsonObject.getString(OCKey.DATA_LABEL_POS.value()));
                            if (pos != null) {
                                setChartStyleAttribute(el, "label-position", pos);
                            }
                        }
                        
                        if (dataLabelJsonObject.has(OCKey.DATA_LABEL_TEXT.value())) {
                            JSONArray textValues = dataLabelJsonObject.getJSONArray(OCKey.DATA_LABEL_TEXT.value());
                            if (!textValues.isEmpty()) {
                                numberValue = textValues.getString(0);
                            }
                        }
                    } catch (JSONException e1) {
                        // Do nothing
                    }
                    
                } 

                if ("value;percent".equals(numberValue)) {
                    number = "value-and-percentage";
                } else if ("value".equals(numberValue)) {
                    number = "value";
                } else if ("percent".equals(numberValue)) {
                    number = "percentage";
                } else {
                    number = "none";
                }

                setChartStyleAttribute(el, "data-label-text", "false");
                setChartStyleAttribute(el, "data-label-symbol", "false");
                setChartStyleAttribute(el, "data-label-number", number);



            } else if (OCKey.FORMAT.value().equals(key)) {
                //ignore series format!
            } else if (OCKey.AXIS_X_INDEX.value().equals(key)) {
                //ignore axisXIndex!
            } else if (OCKey.AXIS_Y_INDEX.value().equals(key)) {
                //ignore axisYIndex!
            } else if (OCKey.AX_POS.value().equals(key)) {
                if ("1".equals(value) || "b".equals(value)) {
                    setAttribute(el, "attached-axis", "primary-y");
                } else {
                    setAttribute(el, "attached-axis", "secondary-y");
                }
            } else if (OCKey.CHART3D.value().equals(key) && Boolean.valueOf(value)) {
                getPlotAreaStyle().getChartProperties().getAttributes().setBooleanValue(OdfDocumentNamespace.CHART.getUri(), "three-dimensional", "chart:three-dimensional", true);
                getPlotAreaStyle().getChartProperties().getAttributes().setBooleanValue(OdfDocumentNamespace.CHART.getUri(), "right-angled-axes", "chart:right-angled-axes", true);
                chart3D = true;
            }
        }
        if (!chart3D) {
            getPlotAreaStyle().getChartProperties().getAttributes().setBooleanValue(OdfDocumentNamespace.CHART.getUri(), "three-dimensional", "chart:three-dimensional", null);
            getPlotAreaStyle().getChartProperties().getAttributes().setBooleanValue(OdfDocumentNamespace.CHART.getUri(), "right-angled-axes", "chart:right-angled-axes", null);

        }

        setChartStyleAttribute(el, "link-data-style-to-source", "true");
        setGraficStyleAttribute(el, OdfDocumentNamespace.DR3D, "edge-rounding", "5%");
    }
    
    private String getDataLabelPosition(String dataLabelPos) {
        String position = null;
        if (dataLabelPos != null) {
            OCKey pos = OCKey.fromValue(dataLabelPos);

            switch(pos) {
                case DATA_LABEL_POS_BOTTOM:
                    position = "bottom";
                    break;
                case DATA_LABEL_POS_CENTER:
                    position = "center";
                    break;
                case DATA_LABEL_POS_INSIDE_BASE:
                    position = "near-origin";
                    break;
                case DATA_LABEL_POS_INSIDE:
                    position = "inside";
                    break;
                case DATA_LABEL_POS_LEFT:
                    position = "left";
                    break;
                case DATA_LABEL_POS_OUTSIDE:
                    position = "outside";
                    break;
                case DATA_LABEL_POS_RIGHT:
                    position = "right";
                    break;
                case DATA_LABEL_POS_ABOVE:
                    position = "above";
                    break;  
                case DATA_LABEL_POS_BEST_FIT:
                    position = "best-fit";
                    break; 
                default:
                    // Do nothing
            }
        }
        return position;
    }

    private void handleAxisDirections(JSONObject attrs) {
        Integer xAxisId = null;
        Integer yAxisId = null;
        if (attrs.hasAndNotNull(OCKey.SERIES.value())) {
            JSONObject series = attrs.optJSONObject(OCKey.SERIES.value());

            if (series.has(OCKey.AXIS_X_INDEX.value())) {
                xAxisId = series.optInt(OCKey.AXIS_X_INDEX.value());
            }
            if (series.has(OCKey.AXIS_Y_INDEX.value())) {
                yAxisId = series.optInt(OCKey.AXIS_Y_INDEX.value());
            }
        }

        if (xAxisId != null || yAxisId != null) {
            ElementNS xAxis = getChildByUserData(plotArea, getAxis(xAxisId));
            ElementNS yAxis = getChildByUserData(plotArea, getAxis(yAxisId));

            setChartStyleAttribute(xAxis, "reverse-direction", chart.isPieOrDonut() + "");
            setChartStyleAttribute(yAxis, "reverse-direction", "false");

            setChartStyleAttribute(xAxis, "axis-position", "0");
            setChartStyleAttribute(yAxis, "axis-position", "start");
        }

    }

    private void setChartDomainAttribute(ElementNS el, String formula, int index) {
        ElementNS domain = getChildByTag(el, "chart:domain", index);
        while (domain == null) {
            if (formula == null) {
                return;
            }
            el.appendChild(newChartElement("domain"));
            domain = getChildByTag(el, "chart:domain", index);
        }
        setAttribute(domain, OdfDocumentNamespace.TABLE, ODF_CELL_RANGE, formula);
    }

    private void setNamesAttribute(ElementNS el, String formula, int index) {
        if (chart.isScatter() || chart.isBubble()) {
            setChartDomainAttribute(el, formula, index);
        } else {
            ElementNS axis = getChildByTag(plotArea, "chart:axis");
            if (axis == null) {
                if (StringUtils.isEmpty(formula)) {
                    return;
                }
                axis = addAxis(0, null);
            }

            ElementNS cat = getChildByTag(axis, "chart:categories");
            if (cat == null) {
                if (StringUtils.isEmpty(formula)) {
                    return;
                }
                cat = newChartElement("categories");
                axis.appendChild(cat);
            }

            setAttribute(cat, OdfDocumentNamespace.TABLE, ODF_CELL_RANGE, formula);
        }
    }

    private void applyAxisAttrs(JSONObject json, ElementNS el) {
        for (Entry<String, Object> e : json.entrySet()) {
            String key = e.getKey();
            String value = e.getValue().toString();

            if (OCKey.LABEL.value().equals(key)) {
                setChartStyleAttribute(el, "display-label", value);
            }
        }
    }

    private void applyLegendAttrs(JSONObject json, ElementNS el) {
        for (Entry<String, Object> e : json.entrySet()) {
            String key = e.getKey();
            String value = e.getValue().toString();
            if (OCKey.POS.value().equals(key)) {
                setAttribute(el, ODF_LEGEND, LEGENDPOS_OP_TO_ODF.get(value));
            }
        }
    }

    private void handleDataPoints(ElementNS seriesEl, JSONArray dataPoints, String symbol) {
        NodeList oldDataPoints = seriesEl.getChildNodes();
        for (int i = oldDataPoints.getLength() - 1; i >= 0; i--) {
            deleteElement((ElementNS) oldDataPoints.item(i));
        }
        if (dataPoints != null) {
            for (int i = 0; i < dataPoints.length(); i++) {
                JSONObject dataPoint = dataPoints.optJSONObject(i);
                boolean containsDataPoint = dataPoint != null && !dataPoint.isEmpty();

                ElementNS dataPointEl = createBoundElement(null, "data-point", null);
                seriesEl.appendChild(dataPointEl);

                if (containsDataPoint) {
                    final int repeated = dataPoint.optInt(OCKey.REPEATED.value(), 1);
                    if (repeated != 1) {
                        setAttribute(dataPointEl, "repeated", Integer.valueOf(repeated).toString());
                    }
                    applyStyleAttrs(dataPoint, dataPointEl);
                }

                if (symbol != null && containsDataPoint) {
                    setChartStyleAttribute(dataPointEl, "symbol-type", symbol);

                    if (!StringUtils.equals(symbol, "none")) {
                        setChartStyleAttribute(dataPointEl, "symbol-width", "0.25cm");
                        setChartStyleAttribute(dataPointEl, "symbol-height", "0.25cm");
                        setChartStyleAttribute(dataPointEl, "symbol-name", MARKERS.get(i % MARKERS.size()));
                    }
                }

            }
        }

    }

    public void applyStyleAttrs(JSONObject attrs, ElementNS el) {
        if (attrs.isEmpty()) {
            return;
        }
        if (!attrs.has(OCKey.LINE.value()) && !attrs.has(OCKey.FILL.value()) && !attrs.has(OCKey.CHARACTER.value())) {
            return;
        }

        StyleManager styleManager = getDocument().getStyleManager();

        String styleName = getOrCreateStyle(el);

        StyleChart style = getStyleChart(styleName);

        try {
            style.applyAttrs(styleManager, attrs);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public JSONObject getStyleAttrs(ElementNS el) {
        String styleName = getOrCreateStyle(el);
        return getStyleAttrs(styleName);
    }

    private String getOrCreateStyle(Element element) {
        String styleName = element.getAttribute(ChartContent.ODF_STYLE);

        if (StringUtils.isEmpty(styleName)) {
            StyleManager styleManager = getDocument().getStyleManager();
            styleName = getUniqueStyleName();
            styleManager.addStyle(new StyleChart(styleName, true, true));
            setAttribute(element, "style-name", styleName);
        }
        return styleName;
    }

    private String getUniqueStyleName() {
        StyleManager styleManager = getDocument().getStyleManager();
        // String styleName = styleManager.getUniqueStyleName("chart", true);

        int index = 1;
        String styleName = "ch" + index++;
        while (styleManager.getStyle(styleName, StyleFamily.CHART, true) != null) {
            styleName = "ch" + index++;
        }

        return styleName;
    }

    private void setChartStyleAttribute(Element element, String name, Object value) {

        String styleName = getOrCreateStyle(element);

        setAttribute(element, "style-name", styleName);

        StyleChart style = (StyleChart) getDocument().getStyleManager().getAutoStyle(styleName, StyleFamily.CHART, true);

        if (value == null) {
            style.getChartProperties().getAttributes().remove(OdfDocumentNamespace.CHART.getPrefix() + ":" + name);
        } else {
            style.getChartProperties().getAttributes().setValue(OdfDocumentNamespace.CHART.getUri(), name, OdfDocumentNamespace.CHART.getPrefix() + ":" + name, value.toString());
        }
    }

    private void setGraficStyleAttribute(Element element, OdfDocumentNamespace namespace, String name, Object value) {
        String styleName = getOrCreateStyle(element);

        setAttribute(element, "style-name", styleName);

        StyleChart style = (StyleChart) getDocument().getStyleManager().getAutoStyle(styleName, StyleFamily.CHART, true);

        if (value == null) {
            style.getGraphicProperties().getAttributes().remove(namespace.getPrefix() + ":" + name);
        } else {
            style.getGraphicProperties().getAttributes().setValue(namespace.getUri(), name, namespace.getPrefix() + ":" + name, value.toString());
        }
    }

    public static String getFormula(String formula) {
        if (StringUtils.isBlank(formula)) {
            return null;
        }
        return "[" + formula + "]";
    }

    public static String fromFormula(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        return value.substring(1, value.length() - 1);
    }

    private static void setAttribute(Element element, String name, String value) {
        setAttribute(element, OdfDocumentNamespace.CHART, name, value);
    }

    private static void setAttribute(Element element, OdfDocumentNamespace namespace, String name, String value) {
        if (element == null) {
            throw new RuntimeException("element is null!");
        }
        String qName = namespace.getPrefix() + ":" + name;
        if (StringUtils.isBlank(value)) {
            element.removeAttribute(qName);
        } else {
            ElementNS.setAttribute(element, namespace.getUri(), qName, name, value);
        }
    }

    private static ElementNS getChildByUserData(Node parent, Object userData) {
        NodeList children = parent.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);

            if (child.getUserData(USERDATA) == userData) {
                return (ElementNS) child;
            }
        }
        return null;
    }

    private static ElementNS getChildByTag(ElementNS parent, String tag) {
        return getChildByTag(parent, tag, 0);
    }

    private static ElementNS getChildByTag(ElementNS parent, String tag, int index) {
        NodeList tagList = parent.getElementsByTagName(tag);
        if (tagList.getLength() > index) {
            return (ElementNS) tagList.item(index);
        }
        return null;
    }

    private String getAxisName(ChartAxis axis) {
        Integer axId = axis.getAxis();

        String subType = "primary";
        if (axId == 2 || axId == 3 || axId == 11) {
            subType = "secondary";
        }
        String xy = getDimension(axId);

        return subType + "-" + xy;
    }

    public void createJSONOperations(List<Integer> position, JSONArray operationQueue) throws JSONException {

        JSONObject chartOperation = operationQueue.getJSONObject(operationQueue.length() - 1);

        JSONObject attrs;

        if (!chartOperation.has(OCKey.ATTRS.value())) {
            chartOperation.put(OCKey.ATTRS.value(), new JSONObject());
        }
        attrs = chartOperation.getJSONObject(OCKey.ATTRS.value());

        JSONObject chartAttrs;
        if (!attrs.has(OCKey.CHART.value())) {
            attrs.put(OCKey.CHART.value(), new JSONObject());
        }
        chartAttrs = attrs.getJSONObject(OCKey.CHART.value());

        boolean includeHiddenCells = getPlotAreaStyle().getChartProperties().getBoolean("chart:include-hidden-cells", true);

        chartAttrs.put(OCKey.SHOW_ONLY_VISIBLE_CELLS_DATA.value(), !includeHiddenCells);

        if (chart.isBubble()) {
            createBubbleJSONOperations(position, operationQueue);
            return;
        }

        int index = 0;

        String categoriesName = null;
        ElementNS axis = getChildByTag(plotArea, "chart:axis");
        if (axis != null) {
            ElementNS categories = getChildByTag(axis, "chart:categories");
            if (categories != null) {
                categoriesName = getFormula(categories.getAttribute(ODF_TABLE_CELL_RANGE));
            }
        }
        for (ChartSeries serie : chart.getSeries()) {
            ElementNS series = getChildByTag(plotArea, "chart:series", index);
            index++;
            
            if (categoriesName == null) {
                String names = null;
                ElementNS namesElement = getChildByTag(series, "chart:domain", 0);
                if (namesElement != null) {
                    names = getFormula(namesElement.getAttribute(ODF_TABLE_CELL_RANGE));
                }
                serie.setNames(names);
            } else {
                serie.setNames(categoriesName);
            }

            if (!chart.hasErrorBars() && getChildByTag(series, "chart:error-indicator", 0) != null) {
                chart.setHasErrorBar();
            }

        }

        chart.createJSONOperations(position, operationQueue);
    }

    private void createBubbleJSONOperations(List<Integer> position, JSONArray operationQueue) throws JSONException {

        int index = 0;
        for (ChartSeries serie : chart.getSeries()) {

            ElementNS series = getChildByTag(plotArea, "chart:series", index);
            index++;

            String values = getFormula(series.getAttribute("chart:" + ChartContent.ODF_VALUES));
            ElementNS namesElement = getChildByTag(series, "chart:domain", 1);
            ElementNS bubblesElement = getChildByTag(series, "chart:domain", 0);

            serie.setValues(values);
            if (namesElement != null) {
                String names = getFormula(namesElement.getAttribute(ODF_TABLE_CELL_RANGE));
                if (names != null) {
                    serie.setNames(names);

                    if (bubblesElement != null) {
                        String bubbles = getFormula(bubblesElement.getAttribute(ODF_TABLE_CELL_RANGE));
                        if (bubbles != null) {
                            serie.setBubbles(bubbles);
                        }

                    }
                }
            }

            if (!chart.hasErrorBars() && getChildByTag(series, "chart:error-indicator", 0) != null) {
                chart.setHasErrorBar();
            }
        }
        chart.createJSONOperations(position, operationQueue);
    }
}
