/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.List;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.w3c.dom.DOMException;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

@SuppressWarnings("serial")
public class CalculationSettings extends ElementNSImpl implements IElementWriter {

    private List<ElementNSImpl> childs;

	public CalculationSettings(OdfFileDom ownerDocument)
		throws DOMException {
		super(ownerDocument, Namespaces.TABLE, "table:calculation-settings");
	}

	public List<ElementNSImpl> getChilds() {
        if(childs==null) {
            childs = new ArrayList<ElementNSImpl>();
        }
        return childs;
    }

	@Override
	public void writeObject(SerializationHandler output) {

	    if(childs!=null) {
	        for(ElementNSImpl child:getChilds()) {
                SaxContextHandler.serializeElement(output, child);
	        }
	    }
	}
}
