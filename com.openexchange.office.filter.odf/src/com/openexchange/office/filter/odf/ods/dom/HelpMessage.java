/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;

public class HelpMessage implements IElementWriter, INodeAccessor, Cloneable {

    private Boolean display;
    private String title;

    private DLList<Object> content;

    public HelpMessage() {}

    public HelpMessage(Attributes attributes) {
        final String _display = attributes.getValue("table:display");
        if(_display!=null) {
            display = Boolean.parseBoolean(_display);
        }
        title = attributes.getValue("table:title");
	}

	public Boolean getDisplay() {
	    return display;
	}

	public void setDisplay(Boolean display) {
	    this.display = display;
	}

	public String getTitle() {
	    return title;
	}

	public void setTitle(String title) {
	    this.title = title;
	}

    @Override
    public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
        return content;
    }

	@Override
    public void writeObject(SerializationHandler output)
		throws SAXException {

        SaxContextHandler.startElement(output, Namespaces.TABLE, "help-message", "table:help-message");
        if(display!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "display", "table:display", display.toString());
        }
        if(title!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "title", "table:title", title);
        }
        TextContentHelper.write(output, getContent());
        SaxContextHandler.endElement(output, Namespaces.TABLE, "help-message", "table:help-message");
	}

    @Override
    protected HelpMessage clone() {
        try {
            final HelpMessage clone = (HelpMessage)super.clone();
            TextContentHelper.setSimpleText(clone, TextContentHelper.getSimpleText(this));
            return clone;
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + TextContentHelper.getSimpleText(this).hashCode();
        result = prime * result + ((display == null) ? 0 : display.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HelpMessage other = (HelpMessage) obj;
        if(!TextContentHelper.getSimpleText(other).equals(TextContentHelper.getSimpleText(this))) {
            return false;
        }
        if (display == null) {
            if (other.display != null)
                return false;
        } else if (!display.equals(other.display))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }
}
