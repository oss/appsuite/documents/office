/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringUtils;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.dom.OdfSchemaDocument;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class EntryHolderCondition extends Condition {

    private List<ConditionEntry> entries = new ArrayList<ConditionEntry>();

    private String iconType;

    private String maxLength;
    private String negativeColor;
    private String positiveColor;
    private String axisColor;

    private String entryType;

    private boolean show = true;
    private boolean gradient = true;

    //public EntryHolderCondition(int id, String ref, int priority, Ranges ranges, String entryType) {
    public EntryHolderCondition(String ref, int priority, Ranges ranges, String entryType) {
        super(ref, priority, ranges);

        this.entryType = entryType;
    }

    public EntryHolderCondition(Attributes attributes, int priority, Ranges ranges, String entryType, String tmpID) {
        super(attributes, priority, ranges, tmpID);

        show = getBoolean(attributes, "calcext:show-value", true);

        switch (entryType) {
            case "icon-set":
                this.iconType = attributes.getValue("calcext:icon-set-type");
                break;
            case "color-scale":
                //nothing
                break;
            case "data-bar":
                this.maxLength = attributes.getValue("calcext:max-length");
                this.negativeColor = attributes.getValue("calcext:negative-color");
                this.positiveColor = attributes.getValue("calcext:positive-color");
                this.axisColor = attributes.getValue("calcext:axis-color");

                gradient = getBoolean(attributes, "calcext:gradient", true);
                break;
        }

        this.entryType = entryType;

    }

    void addEntry(ConditionEntry entry) {
        this.entries.add(entry);
    }

    @Override
    public void writeObject(SerializationHandler output) throws SAXException {

        switch (entryType) {
            case "icon-set":
                startElement(output, "icon-set", "calcext:icon-set");
                addAttribute(output, "icon-set-type", "calcext:icon-set-type", iconType);
                addBooleanAttribute(output, "show-value", "calcext:show-value", show, true);

                for (ConditionEntry e : entries) {
                    addEntryAttribute(output, e, "formatting-entry", "calcext:formatting-entry");
                }

                endElement(output, "icon-set", "calcext:icon-set");
                break;
            case "color-scale":
                startElement(output, "color-scale", "calcext:color-scale");

                for (ConditionEntry e : entries) {
                    addEntryAttribute(output, e, "color-scale-entry", "calcext:color-scale-entry");
                }

                endElement(output, "color-scale", "calcext:color-scale");
                break;
            case "data-bar":
                startElement(output, "data-bar", "calcext:data-bar");
                addAttribute(output, "max-length", "calcext:max-length", maxLength);
                addAttribute(output, "negative-color", "calcext:negative-color", negativeColor);
                addAttribute(output, "positive-color", "calcext:positive-color", positiveColor);
                addAttribute(output, "axis-color", "calcext:axis-color", axisColor);
                addBooleanAttribute(output, "gradient", "calcext:gradient", gradient, true);
                addBooleanAttribute(output, "show-value", "calcext:show-value", show, true);

                for (ConditionEntry e : entries) {
                    addEntryAttribute(output, e, "formatting-entry", "calcext:formatting-entry");
                }

                endElement(output, "data-bar", "calcext:data-bar");

                break;
        }

    }

    @Override
    JSONObject createCondFormatRuleOperation(OdfSpreadsheetDocument doc, int sheetIndex, int index) throws JSONException, SAXException {
        JSONObject result = super.createCondFormatRuleOperation(doc, sheetIndex, index);

        switch (entryType) {
            case "icon-set":

                JSONObject iconSet = new JSONObject(4);
                iconSet.put(OCKey.IS.value(), iconType);
                iconSet.put(OCKey.S.value(), show);

                JSONArray rules = new JSONArray();
                for (ConditionEntry e : entries) {
                    iconSet.put(OCKey.P.value(), e.type);

                    JSONObject setRule = new JSONObject(3);
                    setRule.put(OCKey.T.value(), e.type);
                    setRule.put(OCKey.V.value(), e.value);
                    setRule.put(OCKey.GTE.value(), true);

                    rules.put(setRule);
                }
                iconSet.put(OCKey.IR.value(), rules);

                result.put(OCKey.ICON_SET.value(), iconSet);
                result.put(OCKey.TYPE.value(), "iconSet");
                break;
            case "color-scale":
                JSONArray colorScale = new JSONArray();

                for (ConditionEntry e : entries) {
                    JSONObject setRule = new JSONObject(3);
                    setRule.put(OCKey.T.value(), e.type);
                    setRule.put(OCKey.V.value(), e.value);
                    setRule.put(OCKey.C.value(), getRealColor(e.color));

                    colorScale.put(setRule);
                }
                result.put(OCKey.COLOR_SCALE.value(), colorScale);
                result.put(OCKey.TYPE.value(), "colorScale");
                break;
            case "data-bar":

                JSONObject dataBar = new JSONObject(12);
                dataBar.put(OCKey.C.value(), getRealColor(positiveColor));
                dataBar.put(OCKey.MAX.value(), maxLength);
                dataBar.put(OCKey.B.value(), false);
                dataBar.put(OCKey.NBS.value(), true);
                dataBar.put(OCKey.G.value(), gradient);
                dataBar.put(OCKey.S.value(), show);

                if (StringUtils.isNotEmpty(negativeColor)) {
                    dataBar.put(OCKey.NCS.value(), false);
                    dataBar.put(OCKey.NC.value(), getRealColor(negativeColor));
                } else {
                    dataBar.put(OCKey.NCS.value(), true);
                }

                if (StringUtils.isNotEmpty(axisColor)) {
                    dataBar.put(OCKey.AP.value(), "automatic");
                    dataBar.put(OCKey.AC.value(), getRealColor(axisColor));
                } else {
                    dataBar.put(OCKey.AP.value(), "none");
                }

                ConditionEntry e = entries.get(0);
                JSONObject setRule = new JSONObject(2);
                setRule.put(OCKey.T.value(), e.type);
                setRule.put(OCKey.V.value(), e.value);
                dataBar.put(OCKey.R1.value(), setRule);

                e = entries.get(1);
                setRule = new JSONObject(2);
                setRule.put(OCKey.T.value(), e.type);
                setRule.put(OCKey.V.value(), e.value);
                dataBar.put(OCKey.R2.value(), setRule);

                result.put(OCKey.DATA_BAR.value(), dataBar);
                result.put(OCKey.TYPE.value(), "dataBar");
                break;
        }
        return result;
    }

    @Override
    void applyCondFormatRuleOperation(OdfSchemaDocument doc, String type, Object value1, String value2, Integer priority, boolean stop, JSONObject dataBar, JSONObject iconSet, JSONArray colorScale, JSONObject attrs) throws JSONException {
        super.applyCondFormatRuleOperation(doc, type, value1, value2, priority, stop, dataBar, iconSet, colorScale, attrs);

        switch (entryType) {
            case "icon-set":
                if (null == iconSet) {
                    return;
                }
                iconType = iconSet.optString(OCKey.IS.value(), iconType);

                entries.clear();
                for (Object e : iconSet.getJSONArray(OCKey.IR.value())) {
                    entries.add(new ConditionEntry((JSONObject) e));
                }
                show = iconSet.optBoolean(OCKey.S.value(), true);
                break;
            case "color-scale":
                if (null == colorScale) {
                    return;
                }
                entries.clear();
                for (Object e : colorScale) {
                    entries.add(new ConditionEntry((JSONObject) e));
                }
                break;
            case "data-bar":
                if (null == dataBar) {
                    return;
                }
                maxLength = dataBar.optString(OCKey.MAX.value(), maxLength);
                positiveColor = fromRealColor(dataBar.getJSONObject(OCKey.C.value()));

                if (dataBar.getBoolean(OCKey.NCS.value())) {
                    negativeColor = "";
                } else {
                    negativeColor = fromRealColor(dataBar.getJSONObject(OCKey.NC.value()));
                }

                if (dataBar.getString(OCKey.AP.value()).equals("none")) {
                    axisColor = "";
                } else {
                    axisColor = fromRealColor(dataBar.getJSONObject(OCKey.AC.value()));
                }

                entries.clear();
                entries.add(new ConditionEntry(dataBar.getJSONObject(OCKey.R1.value())));
                entries.add(new ConditionEntry(dataBar.getJSONObject(OCKey.R2.value())));

                gradient = dataBar.optBoolean(OCKey.G.value(), true);
                show = dataBar.optBoolean(OCKey.S.value(), true);
                break;
        }
    }

    private static String limitTypesXmlToJson(String xml) {
        String json = xml.replace("minimum", "min");
        json = json.replace("maximum", "max");
        json = json.replace("-", "");
        return json.replace("number", "formula");
    }

    private static String limitTypesJsonToXml(String json) {
        String xml = json.replace("auto", "auto-");
        xml = xml.replace("min", "minimum");
        return xml = xml.replace("max", "maximum");
//        return xml.replace("formula", "number");
    }

    private static JSONObject getRealColor(String color) throws JSONException {
        JSONObject res = new JSONObject(2);
        res.put(OCKey.VALUE.value(), color.replace("#", ""));
        res.put(OCKey.TYPE.value(), "rgb");
        return res;
    }

    private static String fromRealColor(JSONObject color) throws JSONException {
        if (color.has(OCKey.FALLBACK_VALUE.value())) {
            return color.getString(OCKey.FALLBACK_VALUE.value());
        }
        return "#" + color.getString(OCKey.VALUE.value());
    }

    private static boolean getBoolean(Attributes attributes, String qName, boolean def) {
        String bool = attributes.getValue(qName);
        if (StringUtils.isNotEmpty(bool)) {
            try {
                return Boolean.parseBoolean(bool);
            } catch (NumberFormatException e) {
                Logger.getAnonymousLogger().info("error while parsing string to boolean");
            }
        }
        return def;
    }

    public static void addBooleanAttribute(SerializationHandler output, String localName, String qName, boolean value, boolean def)
        throws SAXException {

        if (value != def) {
            addAttribute(output, localName, qName, Boolean.toString(value));
        }
    }

    public static String getValueByTypeJsonToXml(String value, String type) {
        String val = "";
        if ("formula".equals(type) && value != null && !value.startsWith("=")) {
            val = "=";
        }
        return val += value;
    }

    public static void addEntryAttribute(SerializationHandler output, ConditionEntry e, String localName, String qName)
        throws SAXException {

        startElement(output, localName, qName);
        String type = limitTypesJsonToXml(e.type);
        addAttribute(output, "type", "calcext:type", type);
        addAttribute(output, "value", "calcext:value", getValueByTypeJsonToXml(e.value, type));
        if (StringUtils.isNotEmpty(e.color)) {
            addAttribute(output, "color", "calcext:color", e.color);
        }

        endElement(output, localName, qName);
    }

    public static void addAttribute(SerializationHandler output, String localName, String qName, String value)
        throws SAXException {

        SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, localName, qName, value);
    }

    public static void startElement(SerializationHandler output, String localName, String qName)
        throws SAXException {

        output.startElement(Namespaces.CALCEXT, localName, qName);
    }

    public static void endElement(SerializationHandler output, String localName, String qName)
        throws SAXException {

        output.endElement(Namespaces.CALCEXT, localName, qName);
    }

    static class ConditionEntry {

        private final String value;
        private final String type;
        private final String color;

        public ConditionEntry(Attributes attributes) {
            type = limitTypesXmlToJson(attributes.getValue("calcext:type"));
            String val = attributes.getValue("calcext:value");
            if ("formula".equals(type) && val.charAt(0) == '=') {
                val = val.substring(1);
            }
            value = val;
            color = attributes.getValue("calcext:color");
        }

        public ConditionEntry(JSONObject attributes) throws JSONException {
            value = attributes.optString(OCKey.V.value());
            type = attributes.optString(OCKey.T.value());
            if (attributes.has(OCKey.C.value())) {
                color = fromRealColor(attributes.getJSONObject(OCKey.C.value()));
            } else {
                color = null;
            }
        }

        @Override
        public String toString() {
            return "ConditionEntry [value=" + value + ", type=" + type + ", color=" + color + "]";
        }

    }
}
