/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom.components;

import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.draw.CustomShape;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawImage;
import com.openexchange.office.filter.odf.draw.DrawTextBox;
import com.openexchange.office.filter.odf.draw.GroupShape;
import com.openexchange.office.filter.odf.draw.LineShape;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.ods.dom.Drawing;
import com.openexchange.office.filter.odf.ods.dom.DrawingAnchor;
import com.openexchange.office.filter.odf.ods.dom.Sheet;

public class SheetComponent extends OdfComponent {

    private final Sheet sheet;

    public SheetComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> _node, int _componentNumber) {
        super(parentContext, _node, _componentNumber);
        this.sheet = (Sheet)getObject();
    }

    public SheetComponent(OdfOperationDoc operationDocument, Sheet sheet, int _componentNumber) {
        super(operationDocument, new DLNode<Object>(sheet), _componentNumber);
        this.sheet = (Sheet)getObject();
    }

    @Override
    public String simpleName() {
        return "Sheet";
    }

    @Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {
        final int nextComponentNumber = previousChildComponent != null ? previousChildComponent.getNextComponentNumber() : 0;
        DLNode<Object> nextNode = previousChildContext != null ? previousChildContext.getNode().getNext() : ((Sheet)getNode().getData()).getContent().getFirstNode();
        while(nextNode!=null) {
            final Object child = nextNode.getData();
            if(child instanceof Drawing) {
                return new DrawingComponent(this, nextNode, nextComponentNumber);
            }
            nextNode = nextNode.getNext();
        }
        return null;
    }

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> contextNode, int number, IComponent<OdfOperationDoc>  child, ComponentType type, JSONObject attrs) {

        DLNode<Object> referenceNode = child != null && child.getComponentNumber()== number ? child.getNode() : null;

        switch(type) {
            case AC_CONNECTOR:
            case AC_SHAPE: {
                final Shape shape = Shape.createShape(operationDocument, attrs, null, true, isContentAutoStyle());
                final Drawing drawing = new Drawing(sheet, new DrawingAnchor(0,0), shape);
                final DLNode<Object> drawingNode = new DLNode<Object>(drawing);
                sheet.getDrawings().addDrawingNode(drawingNode, referenceNode);
                return new DrawingComponent(parentContext, drawingNode, number);
            }
            case AC_GROUP: {
                final GroupShape groupShape = new GroupShape(operationDocument, null, true, isContentAutoStyle());
                final Drawing drawing = new Drawing(sheet, new DrawingAnchor(0, 0), groupShape);
                final DLNode<Object> drawingNode = new DLNode<Object>(drawing);
                sheet.getDrawings().addDrawingNode(drawingNode, referenceNode);
                return new DrawingComponent(parentContext, drawingNode, number);
            }
            case AC_IMAGE: {
                final DrawFrame drawFrame = new DrawFrame(operationDocument, null, true, isContentAutoStyle());
                final Drawing drawing = new Drawing(sheet, new DrawingAnchor(0, 0), drawFrame);
                final DLNode<Object> drawingNode = new DLNode<Object>(drawing);
                final DrawImage drawImage = new DrawImage(drawFrame);
                drawFrame.addContent(drawImage);
                sheet.getDrawings().addDrawingNode(drawingNode, referenceNode);
                return new DrawingComponent(parentContext, drawingNode, number);
            }
            case AC_FRAME: {
                final DrawFrame drawFrame = new DrawFrame(operationDocument, null, true, isContentAutoStyle());
                final Drawing drawing = new Drawing(sheet, new DrawingAnchor(0, 0), drawFrame);
                final DLNode<Object> drawingNode = new DLNode<Object>(drawing);
                final DrawTextBox drawTextBox = new DrawTextBox(drawFrame);
                drawFrame.addContent(drawTextBox);
                sheet.getDrawings().addDrawingNode(drawingNode, referenceNode);
                return new DrawingComponent(parentContext, drawingNode, number);
            }
            case AC_CHART: {
                final DrawFrame drawFrame = getChart(operationDocument, isContentAutoStyle());
                final Drawing drawing = new Drawing(sheet, new DrawingAnchor(0, 0), drawFrame);
                final DLNode<Object> drawingNode = new DLNode<Object>(drawing);
                sheet.getDrawings().addDrawingNode(drawingNode, referenceNode);
                return new DrawingComponent(parentContext, drawingNode, number);
            }
            default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs) {
        //
    }

    @Override
    public void createJSONAttrs(OpAttrs attrs)
    	throws SAXException {

    }
}
