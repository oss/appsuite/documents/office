/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.List;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

/**
 * Base class for columns and rows, provides all common properties.
 *
 * @author Daniel Rentz <daniel.rentz@open-xchange.com>
 */
public abstract class AbstractColRow {

    private String style;
    private String defCellStyle = null;
    private String id;

    private Visibility visibility = null;
    private int level = 0;
    private List<Boolean> collapse;

    // local element name for parent XML elements used for column/row grouping
    private String localGroupName;

    protected AbstractColRow(String localGroupName) {
        this.localGroupName = localGroupName;
    }

    /**
     * Returns the index of the first column/row covered by this instance.
     */
    public abstract int getMin();

    /**
     * Returns the index of the last column/row (inclusive) covered by this instance.
     */
    public abstract int getMax();

    public boolean hasStyleName() {
        return (style != null) && !style.isEmpty();
    }

    public String getStyleName() {
        return style;
    }

    public void setStyleName(String colStyle) {
        style = colStyle;
    }

    public String getDefaultCellStyle() {
        return defCellStyle;
    }

    public void setDefaultCellStyle(String cellStyle) {
        defCellStyle = cellStyle;
    }

    public String getId() {
        return id;
    }

    public void setId(String val) {
        id = val;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility value) {
        visibility = value;
    }

    public int getGroupLevel() {
        return level;
    }

    public void setGroupLevel(int value) {
        level = value;
    }

    public List<Boolean> getGroupCollapse() {
        return collapse;
    }

    public void setGroupCollapse(List<Boolean> value) {
        collapse = (value == null) ? null : new ArrayList<>(value);
    }

    static private boolean equalObj(Object obj1, Object obj2) {
        return (obj1==null) ? (obj2==null) : obj1.equals(obj2);
    }

    /**
     * Returns whether the formatting attributes (style name, default cell style name, visibility, grouping level,
     * and collapsed state of the groups) of this instance are equal to the attributes of the passed instance.
     */
    protected boolean equalCommonAttrs(Object obj) {

        if (this == obj) { return true; }
        if ((obj == null) || (getClass() != obj.getClass())) { return false; }

        final AbstractColRow other = (AbstractColRow)obj;
        return equalObj(style, other.style) &&
            equalObj(defCellStyle, other.defCellStyle) &&
            (visibility == other.visibility) &&
            (level == other.level) &&
            equalObj(collapse, other.collapse);
    }

    /**
     * Creates the JSON formatting attributes for column/row grouping.
     */
    protected void createGroupAttributes(JSONObject attrSet) throws JSONException {

        final JSONObject groupAttrs = new JSONObject();
        if (level > 0) {
            groupAttrs.put(OCKey.LEVEL.value(), level);
        }
        if (collapse != null) {
            groupAttrs.put(OCKey.COLLAPSE.value(), new JSONArray(collapse));
        }
        if (!groupAttrs.isEmpty()) {
            attrSet.put(OCKey.GROUP.value(), groupAttrs);
        }
    }

    /**
     * Changes the grouping attributes according to the passed JSON attribute set.
     */
    public void changeGroupAttributes(JSONObject attrSet) {

        final JSONObject groupAttrs = attrSet.optJSONObject(OCKey.GROUP.value());
        if (groupAttrs == null) { return; }

        // grouping level (JSON "null" resets grouping level to 0)
        final int newLevel = (groupAttrs.opt(OCKey.LEVEL.value()) == JSONObject.NULL) ? 0 : groupAttrs.optInt(OCKey.LEVEL.value(), -1);
        if ((0 <= newLevel) && (newLevel <= 7)) {
            level = newLevel;
        }

        // collapsing information (number of opened groups)
        final JSONArray jsonCollapse = (groupAttrs.opt(OCKey.COLLAPSE.value()) == JSONObject.NULL) ? new JSONArray() : groupAttrs.optJSONArray(OCKey.COLLAPSE.value());
        if (jsonCollapse != null) {
            int len = jsonCollapse.length();
            if (len == 0) {
                collapse = null;
            } else {
                collapse = new ArrayList<>(len);
                for (int i = 0; i < len; ++i) {
                    collapse.add(jsonCollapse.optBoolean(i));
                }
            }
        }
    }

    /**
     * Writes the opening XML elements for column/row groups, according to the settings of this instance, and the
     * specified column/row preceding this instance.
     */
    public void openGroupElements(SerializationHandler output, AbstractColRow prevColRow) throws SAXException {

        // The number of opened groups is represented by the length of the "collapse" array if existing; otherwise by
        // the (positive) difference between the grouping level of the preceding column/row, and this column/row.
        //
        // The existing "collapse" array takes precedence, this allows to open new groups right after some groups have
        // been closed in the preceding column/row.
        //
        // Example: Preceding row is on group level 2. The group on level 2 will be closed, the group on level 1 remains
        // open. The current row opens two groups and therefore will be on group level 3. The level difference between the
        // rows is 1, but the "collapse" array contains 2 elements indicating that the row intends to open two new groups.

        if (collapse != null) {
            for (boolean isCollapsed: collapse) {
                SaxContextHandler.startElement(output, Namespaces.TABLE, localGroupName, "table:" + localGroupName);
                if (isCollapsed) {
                    SaxContextHandler.addAttribute(output, Namespaces.TABLE, "display", "table:display", "false");
                }
            }
            return;
        }

        // no collapse info: start at level 0 if there is a gap between preceding column/row and this column/row
        int prevLevel = ((prevColRow != null) && (prevColRow.getMax() + 1 == getMin())) ? prevColRow.level : 0;
        for (int i = prevLevel; i < level; ++i) {
            SaxContextHandler.startElement(output, Namespaces.TABLE, localGroupName, "table:" + localGroupName);
        }
    }

    /**
     * Writes the closing XML elements for column/row groups, according to the settings of this instance, and the
     * specified column/row that follows this instance.
     */
    public void closeGroupElements(SerializationHandler output, AbstractColRow nextColRow) throws SAXException {

        // The number of groups to be closed depends on the settings of the following column/row. If the next column/row
        // intends to open new groups (indicated by existence of its "collapse" property), these levels have to be closed
        // before if necessary.
        //
        // Example: This row is on group level 3. The groups on level 3 and 2 will be closed, the group on level 1 remains
        // open. The next row wants to open a new group (indicated by a "collapse" array property with length 1), and
        // therefore will be on group level 2. Although the difference between old level and new level is 1 (from level 3
        // to level 2), this method has to close two group levels (level difference plus length of "collapse" array).

        int nextLevel = 0;
        // always go to level 0 if there is a gap between this column/row and the following column/row
        if ((nextColRow != null) && (getMax() + 1 == nextColRow.getMin())) {
            nextLevel = nextColRow.level;
            if (nextColRow.collapse != null) {
                nextLevel -= nextColRow.collapse.size();
            }
        }
        for (int i = level; i > nextLevel; --i) {
            SaxContextHandler.endElement(output, Namespaces.TABLE, localGroupName, "table:" + localGroupName);
        }
    }
}
