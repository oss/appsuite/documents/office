/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.xerces.dom.ElementNSImpl;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.odf.DOMBuilder;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.AutomaticStylesHandler;
import com.openexchange.office.filter.odf.styles.FontFaceDeclsHandler;

/**
 * @author sven.jacobi@open-xchange.com
 */

public class SpreadsheetContentHandler extends DOMBuilder {

    // the empty XML file to which nodes will be added
    private final SpreadsheetContent content;
    private OdfSpreadsheetDocument spreadsheetDocument = null;
    private int maxSheets;
    private boolean sheetWithoutName = false;

    public SpreadsheetContentHandler(Node rootNode, XMLReader xmlReader) {
    	super(rootNode, xmlReader, (SpreadsheetContent)rootNode);

        // Initialize starting DOM node
        content = (SpreadsheetContent)rootNode;
        spreadsheetDocument = content.getDocument();
        spreadsheetDocument.setContentDom(content);
        Object oMaxSheets = content.getDocument().getPackage().getRunTimeConfiguration().get("maxSheets");
        maxSheets = oMaxSheets instanceof Integer ? ((Integer)oMaxSheets).intValue() : 256;
    }

    public SpreadsheetContent getContentDom() {
    	return content;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
    	if(qName.equals("office:automatic-styles")) {
    		return new AutomaticStylesHandler(this, content.getDocument().getStyleManager(), true);
    	}
    	else if(qName.equals("office:font-face-decls")) {
    		return new FontFaceDeclsHandler(this, content.getDocument().getStyleManager(), true);
    	}
    	else if(qName.equals("table:calculation-settings")) {
    	    // needs to be the first element within office:spreadsheet, otherwise Excel won't load this document
    	    final CalculationSettings calculationSettings = new CalculationSettings(content);
    	    content.setCalculationSettings(calculationSettings);
    	    appendChild(calculationSettings);
    	    return new CalculationSettingsHandler(this, calculationSettings);
    	}
    	else if(qName.equals("table:content-validations")) {
    	    final ContentValidations contentValidations = new ContentValidations(content);
    	    content.setContentValidations(contentValidations);
    	    appendChild(contentValidations);
    	    return new ContentValidationsHandler(this, contentValidations);
    	}
    	else if(qName.equals("table:table")) {
    		if(maxSheets>0) {
    			if(content.getContent().size()>=maxSheets) {
		        	final FilterException filterException = new FilterException("", ErrorCode.COMPLEXITY_TOO_HIGH);
		        	filterException.setSubType("MAX_SHEET_COUNT_EXCEEDED");
		        	throw filterException;
    			}
    		}
    		String sheetStyle = null;
    		for(int i=0; i<attributes.getLength(); i++) {
				if(attributes.getLocalName(i).equals("style-name")) {
					sheetStyle = attributes.getValue(i);
					break;
				}
    		}
    		final Sheet sheet = new Sheet(content, sheetStyle);
    		appendChild(ElementNS.addAttributes(sheet, attributes));
    		content.getContent().add(sheet);
    		return new SheetHandler(this, sheet);
    	}
    	else if(qName.equals("table:named-expressions")) {
    		final NamedExpressions namedExpressions = new NamedExpressions(content);
    		content.setNamedExpressions(namedExpressions);
    		appendChild(namedExpressions);
    		return new NamedExpressionsHandler(this, namedExpressions);
    	}
    	else if(qName.equals("table:database-ranges")) {
    		final DatabaseRanges databaseRanges = new DatabaseRanges(content);
    		content.setDatabaseRanges(databaseRanges);
    		appendChild(databaseRanges);
    		return new DatabaseRangesHandler(this, databaseRanges);
    	}

    	// if we come to this postion, a normal node is inserted into the dom...
    	final SaxContextHandler newContext = super.startElement(attributes, uri, localName, qName);
    	if(qName.equals("office:spreadsheet")) {
            content.setSpreadsheet((ElementNSImpl) getCurrentNode());
    	}
    	return newContext;
    }

    @Override
    public void endElement(String localName, String qName) {
    	if(qName.equals("office:automatic-styles")||qName.equals("office:font-face-decls")) {
    		// do nothing since we do not want this element to be inserted into the dom,
    		// this is done elsewhere
    		return;
    	}

    	super.endElement(localName, qName);

    	if(qName.equals("table:table")) {
    		final List<Sheet> sheets = content.getContent();
    		final int lastIndex = sheets.size()-1;
    		final Sheet sheet = sheets.get(lastIndex);
    		if(sheet.isLinkedTable()) {
        		sheets.remove(lastIndex);
    		}
    		else if(sheet.getName()==null||sheet.getName().isEmpty()) {
    		    sheetWithoutName = true;
    		}
    	}
    }

    @Override
    public void endContext(String qName, String characters) {
        super.endContext(qName, characters);

        // #65889#: ensure that each table is having a table name. a name is required for view settings etc.
        if(qName.equals("office:spreadsheet")) {

            // ensure correct sheet names
            if(sheetWithoutName) {
                final Set<String> sheetNames = new HashSet<String>();
                Iterator<Sheet> sheetIter = content.getContent().iterator();
                while(sheetIter.hasNext()) {
                    final Sheet sheet = sheetIter.next();
                    final String name = sheet.getName();
                    if(name!=null&&!name.isEmpty()) {
                        if(sheetNames.contains(name)) {
                            sheet.setName(null);        // name is already used...
                        }
                        else {
                            sheetNames.add(name);
                        }
                    }
                }
                int sheetNumber = 1;
                sheetIter = content.getContent().iterator();
                while(sheetIter.hasNext()) {
                    final Sheet sheet = sheetIter.next();
                    final String name = sheet.getName();
                    if(name==null||name.isEmpty()) {
                        String newName;
                        int n = 0;
                        do {
                            newName = "table" + Integer.valueOf(sheetNumber).toString();
                            if(n > 0) {
                                newName = newName + "-" + Integer.valueOf(n).toString();
                            }
                            n++;
                        }
                        while(sheetNames.contains(newName));

                        sheet.setName(newName);
                    }
                    sheetNumber++;
                }
            }
        }
    }
}
