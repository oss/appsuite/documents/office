/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.apache.commons.lang3.StringUtils;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.dom.OdfSchemaDocument;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.spreadsheet.SmlUtils;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;

public class Condition {

	private Ranges ranges;
	private int priority;

	private String value;
	private String value1;
	private String value2;
	private String date;
	private String styleName;
	private String baseCellAddress;

	public Condition(String ref, int priority, Ranges ranges) {
		this.ranges = ranges;
		this.priority = priority;
		setBaseCellAddress(ref);
	}

	public Condition(Attributes attributes, int priority, Ranges ranges, String tmpID) {
		this.ranges = ranges;
		this.priority = priority;

		// condition element
		value = attributes.getValue("calcext:value");
		styleName = attributes.getValue("calcext:apply-style-name");
		baseCellAddress = attributes.getValue("calcext:base-cell-address");

		// date-is element
		date = attributes.getValue("calcext:date");
		if(styleName==null) {
			styleName = attributes.getValue("calcext:style");
		}
	}

	public int getPriority() {
	    return priority;
	}

	public Ranges getRanges() {
		return ranges;
	}

	public void setRanges(Ranges ranges) {
		this.ranges = ranges;
	}

	public void setBaseCellAddress(String ref) {
		if(ref!=null) {
		    baseCellAddress = SmlUtils.getAddressWithoutBraces(ref);
		}
	}

	public void writeObject(SerializationHandler output)
		throws SAXException {

		if(date!=null) {
			SaxContextHandler.startElement(output, Namespaces.CALCEXT, "date-is", "calcext:date-is");
			SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "date", "calcext:date", date);
			//SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "id", "calcext:id", id);
			if(styleName!=null) {
				SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "style", "calcext:style", styleName);
			}
			SaxContextHandler.endElement(output, Namespaces.CALCEXT, "date-is", "calcext:date-is");
		}
		else {
			SaxContextHandler.startElement(output, Namespaces.CALCEXT, "condition", "calcext:condition");
			//SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "id", "calcext:id", id);
			if(value!=null) {
				SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "value", "calcext:value", value);
			}
			if(value1!=null) {
				SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "value1", "calcext:value1", value1);
			}
			if(value2!=null) {
				SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "value2", "calcext:value2", value2);
			}
			if(styleName!=null) {
				SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "apply-style-name", "calcext:apply-style-name", styleName);
			}
			if(baseCellAddress!=null) {
				SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "base-cell-address", "calcext:base-cell-address", baseCellAddress);
			}
			SaxContextHandler.endElement(output, Namespaces.CALCEXT, "condition", "calcext:condition");
		}
	}

	void applyCondFormatRuleOperation(OdfSchemaDocument doc, String type, Object value1, String value2, Integer priority, boolean stop,  JSONObject dataBar, JSONObject iconSet, JSONArray colorScale, JSONObject attrs)
		throws JSONException {

		if(type!=null) {
			value = null;
			date = null;
			// applying the new type
			switch(type) {
				case "equal": {
					value = "=";
					break;
				}
				case "lessEqual": {
					value = "<=";
					break;
				}
				case "less": {
					value = "<";
					break;
				}
				case "greaterEqual": {
					value = ">=";
					break;
				}
				case "greater": {
					value = ">";
					break;
				}
				case "notEqual": {
					value = "!=";
					break;
				}
				case "between": {
					value = "between";
					break;
				}
				case "notBetween": {
					value = "not-between";
					break;
				}
				case "formula": {
					value = "formula-is";
					break;
				}
				case "topN": {
					value = "top-elements";
					break;
				}
				case "bottomN": {
					value = "bottom-elements";
					break;
				}
				case "topPercent": {
					value = "top-percent";
					break;
				}
				case "bottomPercent": {
					value = "bottom-percent";
					break;
				}
				case "aboveAverage": {
					value = "above-average";
					break;
				}
				case "atLeastAverage": {
					value = "above-equal-average";
					break;
				}
				case "belowAverage": {
					value = "below-average";
					break;
				}
				case "atMostAverage": {
					value = "below-equal-average";
					break;
				}
				case "unique": {
					value = "unique";
					break;
				}
				case "duplicate": {
					value = "duplicate";
					break;
				}
				case "error": {
					value = "is-error";
					break;
				}
				case "noError": {
					value = "is-no-error";
					break;
				}
				case "beginsWith": {
					value = "begins-with";
					break;
				}
				case "endsWith": {
					value = "ends-with";
					break;
				}
				case "contains": {
					value = "contains-text";
					break;
				}
				case "notContains": {
					value = "not-contains-text";
					break;
				}

				case "today": {
					date = "today";
					break;
				}
				case "yesterday": {
					date = "yesterday";
					break;
				}
				case "tomorrow": {
					date = "tomorrow";
					break;
				}
				case "last7Days": {
					date = "last-7-days";
					break;
				}
				case "lastWeek": {
					date = "last-week";
					break;
				}
				case "thisWeek": {
					date = "this-week";
					break;
				}
				case "nextWeek": {
					date = "next-week";
					break;
				}
				case "lastMonth": {
					date = "last-month";
					break;
				}
				case "thisMonth": {
					date = "this-month";
					break;
				}
				case "nextMonth": {
					date = "next-month";
					break;
				}
				case "lastYear": {
					date = "last-year";
					break;
				}
				case "thisYear": {
					date = "this-year";
					break;
				}
				case "nextYear": {
					date = "next-year";
					break;
				}
			}
		}
		if(value1 instanceof String || value1 instanceof Number) {
			if(value!=null) {
				if(value.startsWith("=")) {
					value = "=" + value1;
				}
				else if(value.startsWith("<=")) {
					value = "<=" + value1;
				}
				else if(value.startsWith("<")) {
					value = "<" + value1;
				}
				else if(value.startsWith(">=")) {
					value = ">=" + value1;
				}
				else if(value.startsWith(">")) {
					value = ">" + value1;
				}
				else if(value.startsWith("!=")) {
					value = "!=" + value1;
				}
				else if(value.startsWith("between")) {
					value = "between(" + value1 + "," + value2 + ")";
				}
				else if(value.startsWith("not-between")) {
					value = "not-between(" + value1 + "," + value2 + ")";
				}
				else if(value.startsWith("formula-is")) {
					value = "formula-is(" + value1 + ")";
				}
				else if(value.startsWith("top-elements")) {
					value = "top-elements(" + value1 + ")";
				}
				else if(value.startsWith("bottom-elements")) {
					value = "bottom-elements(" + value1 + ")";
				}
				else if(value.startsWith("top-percent")) {
					value = "top-percent(" + value1 + ")";
				}
				else if(value.startsWith("bottom-percent")) {
					value = "bottom-percent(" + value1 + ")";
				}
				else if(value.startsWith("begins-with")) {
					value = "begins-with(" + value1 + ")";
				}
				else if(value.startsWith("ends-with")) {
					value = "ends-with(" + value1 + ")";
				}
				else if(value.startsWith("contains-text")) {
					value = "contains-text(" + value1 + ")";
				}
				else if(value.startsWith("not-contains-text")) {
					value = "not-contains-text(" + value1 + ")";
				}
			}
		}

		if(attrs!=null&&attrs.has(OCKey.STYLE_ID.value())) {
			// OMG, Odf is storing the display name instead of the styleId
			// -> we have to correct this, as we are only using the styleId
            String styleId = attrs.getString(OCKey.STYLE_ID.value());
            if (StringUtils.isNotEmpty(styleId)) {
                StyleBase odfStyle = doc.getStyleManager().getStyle(styleId, StyleFamily.TABLE_CELL, true);
                if (odfStyle != null && StringUtils.isNotEmpty(odfStyle.getDisplayName())) {
                    styleId = odfStyle.getDisplayName();
                }
			}
            styleName = styleId;
		}
	}

	JSONObject createCondFormatRuleOperation(OdfSpreadsheetDocument doc, int sheetIndex, int index)
		throws JSONException, SAXException {

		final JSONObject insertCondFormatRuleObject = new JSONObject();
		insertCondFormatRuleObject.put(OCKey.NAME.value(), OCValue.INSERT_CF_RULE.value());
		insertCondFormatRuleObject.put(OCKey.SHEET.value(), sheetIndex);
		insertCondFormatRuleObject.put(OCKey.ID.value(), String.valueOf(index));
		if(baseCellAddress!=null) {
		    insertCondFormatRuleObject.put(OCKey.REF.value(), "[" + baseCellAddress + "]");
		}
		insertCondFormatRuleObject.put(OCKey.RANGES.value(), ranges.convertToString((SpreadsheetContent)doc.getContentDom(), true));
		insertCondFormatRuleObject.put(OCKey.PRIORITY.value(), getPriority());

		String type = null;
		Object value1 = null;
		Object value2 = null;
		try {
			if(value!=null) {
				if(value.startsWith("=")) {
					type = "equal";
					value1 = value.substring(1);
				}
				else if(value.startsWith("<=")) {
					type = "lessEqual";
					value1 = value.substring(2);
				}
				else if(value.startsWith("<")) {
					type = "less";
					value1 = value.substring(1);
				}
				else if(value.startsWith(">=")) {
					type = "greaterEqual";
					value1 = value.substring(2);
				}
				else if(value.startsWith(">")) {
					type = "greater";
					value1 = value.substring(1);
				}
				else if(value.startsWith("!=")) {
					type = "notEqual";
					value1 = value.substring(2);
				}
				else if(value.startsWith("between")) {
					final String[] vals = getParameters(valueToString(value, 8));
					if(vals.length==2) {
						value1 = vals[0];
						value2 = vals[1];
					}
					type = "between";
				}
				else if(value.startsWith("not-between")) {
					final String[] vals = getParameters(valueToString(value, 12));
					if(vals.length==2) {
						value1 = vals[0];
						value2 = vals[1];
					}
					type = "notBetween";
				}
				else if(value.startsWith("formula-is")) {
					type = "formula";
					value1 = valueToString(value, 11);
				}
				else if(value.startsWith("top-elements")) {
					type = "topN";
					value1 = valueToNumber(value, 13);
				}
				else if(value.startsWith("bottom-elements")) {
					type = "bottomN";
					value1 = valueToNumber(value, 16);
				}
				else if(value.startsWith("top-percent")) {
					type = "topPercent";
					value1 = valueToNumber(value, 12);
				}
				else if(value.startsWith("bottom-percent")) {
					type = "bottomPercent";
					value1 = valueToNumber(value, 15);
				}
				else if(value.startsWith("above-average")) {
					type = "aboveAverage";
					value1 = valueToNumber(value, 14);
				}
				else if(value.startsWith("above-equal-average")) {
					type = "atLeastAverage";
					value1 = valueToNumber(value, 20);
				}
				else if(value.startsWith("below-average")) {
					type = "belowAverage";
					value1 = valueToNumber(value, 14);
				}
				else if(value.startsWith("below-equal-average")) {
					type = "atMostAverage";
					value1 = valueToNumber(value, 20);
				}
				else if(value.equals("unique")) {
					type = "unique";
				}
				else if(value.equals("duplicate")) {
					type = "duplicate";
				}
				else if(value.equals("is-error")) {
					type = "error";
				}
				else if(value.equals("is-no-error")) {
					type = "noError";
				}
				else if(value.startsWith("begins-with")) {
					value1 = getParameters(valueToString(value, 12))[0];
					type = "beginsWith";
				}
				else if(value.startsWith("ends-with")) {
					value1 = getParameters(valueToString(value, 10))[0];
					type = "endsWith";
				}
				else if(value.startsWith("contains-text")) {
					value1 = getParameters(valueToString(value, 14))[0];
					type = "contains";
				}
				else if(value.startsWith("not-contains-text")) {
					value1 = getParameters(valueToString(value, 18))[0];
					type = "notContains";
				}
			}
			else if(date!=null) {
				if(date.equals("today")) {
					type = "today";
				}
				else if(date.equals("yesterday")) {
					type = "yesterday";
				}
				else if(date.equals("tomorrow")) {
					type = "tomorrow";
				}
				else if(date.equals("last-7-days")) {
					type = "last7Days";
				}
				else if(date.equals("last-week")) {
					type = "lastWeek";
				}
				else if(date.equals("this-week")) {
					type = "thisWeek";
				}
				else if(date.equals("next-week")) {
					type = "nextWeek";
				}
				else if(date.equals("last-month")) {
					type = "lastMonth";
				}
				else if(date.equals("this-month")) {
					type = "thisMonth";
				}
				else if(date.equals("next-month")) {
					type = "nextMonth";
				}
				else if(date.equals("last-year")) {
					type = "lastYear";
				}
				else if(date.equals("this-year")) {
					type = "thisYear";
				}
				else if(date.equals("next-year")) {
					type = "nextYear";
				}
			}
		}
		catch(IndexOutOfBoundsException e) {
		    //
		}
		if(type!=null) {
			insertCondFormatRuleObject.put(OCKey.TYPE.value(), type);
		}
		if(value1!=null) {
			insertCondFormatRuleObject.put(OCKey.VALUE1.value(), value1);
		}
		if(value2!=null) {
			insertCondFormatRuleObject.put(OCKey.VALUE2.value(), value2);
		}

		if(styleName!=null) {
            String styleId = styleName;
			// OMG, Odf is storing the display name instead of the styleId
			// -> we have to correct this, as we are only using the styleId
            StyleBase style = doc.getStyleManager().getStyleByDisplayName(styleId, StyleFamily.TABLE_CELL, true);
            if (null != style && StringUtils.isNotEmpty(style.getName())) {
                styleId = style.getName();
            }

			final JSONObject attrs = new JSONObject(1);
            attrs.put(OCKey.STYLE_ID.value(), styleId);
			insertCondFormatRuleObject.put(OCKey.ATTRS.value(), attrs);
		}
		return insertCondFormatRuleObject;
	}

	private String[] getParameters(String v) {

		int brackets = 0;
		for(int i=0;i<v.length();i++) {
			char c = v.charAt(i);
			if(c==','&&brackets==0) {
				final String[] strings = new String[2];
				if(i>0) {
					strings[0] = v.substring(0, i);
				}
				else {
					strings[0] = "";
				}
				if(i+1<v.length()) {
					strings[1] = v.substring(i+1, v.length());
				}
				else {
					strings[1] = "";
				}
				return strings;
			}
			else if(c=='(') {
				brackets++;
			}
			else if(c==')') {
				brackets--;
			}
		}
		final String[] strings = new String[1];
		strings[0] = v;
		return strings;
	}

    private static String valueToString(String value, int startIndex) {
    	return value.substring(startIndex, value.length()-1);
    }

    private static Integer valueToNumber(String value, int startIndex) {
    	return Integer.parseInt(valueToString(value, startIndex));
    }
}
