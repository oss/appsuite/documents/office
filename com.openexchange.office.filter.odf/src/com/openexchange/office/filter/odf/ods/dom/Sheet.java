/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.TreeSet;
import org.apache.xerces.dom.AttrNSImpl;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.ISheet;
import com.openexchange.office.filter.odf.AttrNS;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.odt.dom.Annotation;

@SuppressWarnings("serial")
public class Sheet extends ElementNSImpl implements ISheet, IElementWriter, INodeAccessor {

    private final TreeSet<Row> rows;
    private final TreeSet<Column> columns;

    // each cell that is merged
    private List<MergeCell> mergeCells;

    // hyperlink ranges
    private List<Hyperlink> hyperlinks;

    // office forms if available ...
    // take care, these forms must be written before table:shapes, otherwise
    // LO won't display form controls (this detail would be good to be part of
    // the odf spec or LO should fix the import filter to not assume a specific
    // element order)
    private ElementNSImpl officeForms = null;

    // each drawing of a page
    private Drawings drawings = null;

    // named expressions
    private NamedExpressions namedExpressions = null;

    // conditional formats
    private ConditionalFormats conditionalFormats = null;

    private List<Object> childs;

    boolean columnPlaceholderAvailable = false;
    boolean rowPlaceholderAvailable = false;
    boolean isLinkedTable = false;

    public Sheet(OdfFileDom ownerDocument, String StyleName)
        throws DOMException {

        super(ownerDocument, Namespaces.TABLE, "table:table");

        columns = new TreeSet<Column>();
        rows = new TreeSet<Row>();
    }

    @Override
    public String getName() {
        final Attr name = this.getAttributeNodeNS(Namespaces.TABLE, "name");
        return name!=null ? name.getValue() : null;
    }

    public void setName(String name) {
        final AttrNSImpl tableNameAttr = new AttrNS((OdfFileDom)getOwnerDocument(), Namespaces.TABLE, "table:name");
        tableNameAttr.setNodeValue(name);
        setAttributeNodeNS(tableNameAttr);
    }

    public String getStyleName() {
        final Attr name = this.getAttributeNodeNS(Namespaces.TABLE, "style-name");
        return name!=null ? name.getValue() : null;
    }

    public void setStyleName(String name) {
        final AttrNS tableStyleNameAttr = new AttrNS((OdfFileDom)getOwnerDocument(), Namespaces.TABLE, "table:style-name");
        tableStyleNameAttr.setNodeValue(name);
        setAttributeNodeNS(tableStyleNameAttr);
    }

    public boolean isLinkedTable() {
        return isLinkedTable;
    }

    public void setIsLinkedTable(boolean isLinkedTable) {
        this.isLinkedTable = isLinkedTable;
    }

    public static int getMaxRowCount() {
        return 1048576;
    }

    public int getMaxColCount() {
        return ((SpreadsheetContent)getOwnerDocument()).getMaxColumnCount();
    }

    /* returns a live list to the columns */
    public TreeSet<Column> getColumns() {
        return columns;
    }

    /* returns a live list to the rows */
    public TreeSet<Row> getRows() {
        return rows;
    }

    @Override
    public DLList<Object> getContent() {
        return getDrawings().getContent();
    }

    public Drawings getDrawings() {
        if(drawings==null) {
            drawings = new Drawings();
        }
        return drawings;
    }

    public void setOfficeForms(ElementNSImpl officeForms) {
        this.officeForms = officeForms;
    }

    public Row getRow(int r, boolean forceCreate, boolean cutBefore, boolean cutAfter) {
        if(rows.isEmpty()) {
            if(!forceCreate) {
                return null;
            }
            // creating new rows up to r
            final Row newRow = new Row(0, r+1);
            newRow.setDefaultCellStyle("Default");
            rows.add(newRow);
        }
        Row retValue = rows.floor(new Row(r));
        if((retValue==null)||(retValue.getMax()<r)) {
            if(!forceCreate) {
                return null;
            }
            // we have to create new rows from end of floorValue up to r (no style, no grouping)
            final int startRow = (retValue != null) ? (retValue.getMax() + 1) : 0;
            retValue = new Row(startRow, r - startRow + 1); // plus 1 so that the row entry includes "r"
            retValue.setDefaultCellStyle("Default");
            rows.add(retValue);
        }
        // cut away preceding rows if specified
        if(cutBefore && (retValue.getRow() < r)) {
            rows.add(retValue.splitBefore(r));
        }
        // cut away following rows if specified
        if(cutAfter && (retValue.getMax() > r)) {
            final Row newRow = retValue.splitBefore(r + 1);
            rows.add(newRow);
            retValue = newRow;
        }
        return retValue;
    }

    public Column getColumn(int c, boolean forceCreate, boolean cutBefore, boolean cutAfter) {
        if(columns.isEmpty()) {
            if(!forceCreate) {
                return null;
            }
            // creating new columns up to c
            final Column newColumn = new Column(0, c);
            columns.add(newColumn);
        }
        Column retValue = columns.floor(new Column(c));
        if((retValue==null)||(retValue.getMax()<c)) {
            if(!forceCreate) {
                return null;
            }
            // we have to create new columns from end of preceding column up to c (no style, no grouping)
            final int startCol = (retValue!=null) ? (retValue.getMax()+1) : 0;
            retValue = new Column(startCol, c);
            columns.add(retValue);
        }
        // cut away preceding columns if specified
        if(cutBefore && (retValue.getMin() < c)) {
            columns.add(retValue.splitBefore(c));
        }
        // cut away following columns if specified
        if(cutAfter && (retValue.getMax() > c)) {
            final Column newColumn = retValue.splitBefore(c + 1);
            columns.add(newColumn);
            retValue = newColumn;
        }
        return retValue;
    }

    public void insertRows(int sheetIndex, int start, int count)
        throws SAXException {

        // assuring that the first row is available
        getRow(start, true, true, false);

        final Object[] rs = rows.tailSet(new Row(start), true).toArray();
        for(int i=rs.length-1;i>=0;i--) {
            final Row ro = (Row)rs[i];
            rows.remove(ro);
            int newRowNumber = ro.getRow()+count;
            if(newRowNumber<getMaxRowCount()) {
                ro.setRow(ro.getRow()+count);
                rows.add(ro);
                if(newRowNumber+ro.getRepeated()>getMaxRowCount()) {
                    ro.setRepeated(getMaxRowCount()-newRowNumber);
                }
            }
        }
        final Row prevRow = (start>0) ? getRow(start-1, false, false, false) : null;
        rows.add(Row.createInserted(start, count, prevRow));

        // taking care of hyperlinks
        if(hyperlinks!=null) {
            CellRefRange.insertRows(hyperlinks, start, count, false, null);
        }

        // taking care of merged cells
        if(mergeCells!=null) {
            CellRefRange.insertRows(mergeCells, start, count, false, getMaxRowCount());
        }

        // taking care of tables
        final DatabaseRanges databaseRanges = ((SpreadsheetContent)getOwnerDocument()).getDatabaseRanges(false);
        if(databaseRanges!=null) {
            final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRanges.getDatabaseRangeList().entrySet().iterator();
            while(databaseRangeIter.hasNext()) {
                final DatabaseRange databaseRange = databaseRangeIter.next().getValue();
                if(databaseRange.getName().startsWith("__Anonymous_Sheet_DB__")) {
                    final Range sheetRange = databaseRange.getRange();
                    if(sheetRange.getSheetIndex()==sheetIndex) {
                        final CellRefRange cellRefRange = sheetRange.getCellRefRange(false);
                        if(cellRefRange!=null) {
                            final CellRefRange newCellRefRange = CellRefRange.insertRowRange(cellRefRange, start, count, false);
                            if(newCellRefRange!=null&&!newCellRefRange.equals(cellRefRange)) {
                                databaseRange.setRange(new Range(sheetIndex, newCellRefRange));
                                // TODO: take care of changed table columns
                            }
                        }
                    }
                }
            }
        }
    }

    public void deleteRows(int sheetIndex, int start, int count)
        throws SAXException {

        getRow(start, true, true, true);
        if(count>1) {
            getRow((start+count)-1, true, false, true);
        }
        final Object[] rs = rows.tailSet(new Row(start), true).toArray();
        for(Object obj:rs) {
            final Row ro = (Row)obj;
            rows.remove(ro);
            if(ro.getRow()>=start+count) {
                ro.setRow(ro.getRow()-count);
                rows.add(ro);
            }
        }

        // taking care of hyperlinks
        if(hyperlinks!=null) {
            CellRefRange.deleteRows(hyperlinks, start, count, false, null);
        }

        // taking care of merged cells
        if(mergeCells!=null) {
            CellRefRange.deleteRows(mergeCells, start, count, true, getMaxRowCount());
        }

        final DatabaseRanges databaseRanges = ((SpreadsheetContent)getOwnerDocument()).getDatabaseRanges(false);
        if(databaseRanges!=null) {
            final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRanges.getDatabaseRangeList().entrySet().iterator();
            while(databaseRangeIter.hasNext()) {
                final DatabaseRange databaseRange = databaseRangeIter.next().getValue();
                if(databaseRange.getName().startsWith("__Anonymous_Sheet_DB__")) {
                    final Range sheetRange = databaseRange.getRange();
                    if(sheetRange.getSheetIndex()==sheetIndex) {
                        final CellRefRange cellRefRange = sheetRange.getCellRefRange(false);
                        if(cellRefRange!=null) {
                            final CellRefRange newCellRefRange = CellRefRange.deleteRowRange(cellRefRange, start, count);
                            if(newCellRefRange==null) {
                                databaseRanges.deleteTable(((SpreadsheetContent)getOwnerDocument()), sheetIndex, databaseRange.getName());
                            }
                            else if(!newCellRefRange.equals(cellRefRange)) {
                                databaseRange.setRange(new Range(sheetIndex, newCellRefRange));
                                // TODO: take care of changed table columns
                            }
                        }
                    }
                }
            }
        }
    }

    public void insertColumns(int sheetIndex, int start, int count)
        throws SAXException {

        for(Row row:getRows()) {
            row.insertCells(start, count);
        }

        // assuring that the first column is available
        getColumn(start, true, true, false);

        final Object[] rs = columns.tailSet(new Column(start), true).toArray();
        for(int i=rs.length-1;i>=0;i--) {
            final Column co = (Column)rs[i];
            columns.remove(co);
            co.setMin(co.getMin()+count);
            co.setMax(co.getMax()+count);
            if(co.getMin()<getMaxColCount()) {
                columns.add(co);
                if(co.getMax()>=getMaxColCount()) {
                    co.setMax(getMaxColCount()-1);
                }
            }
        }
        if(start-1>=0) {
            final Column previousColumn = getColumn(start-1, false, false, false);
            if(previousColumn!=null) {
                columns.remove(previousColumn);
                previousColumn.setMax(previousColumn.getMax()+count);
                columns.add(previousColumn);
            }
        }

        // taking care of hyperlinks
        if(hyperlinks!=null) {
            CellRefRange.insertColumns(hyperlinks, start, count, false, null);
        }

        // taking care of merged cells
        if(mergeCells!=null) {
            CellRefRange.insertColumns(mergeCells, start, count, false, getMaxColCount());
        }

        // taking care of tables
        final DatabaseRanges databaseRanges = ((SpreadsheetContent)getOwnerDocument()).getDatabaseRanges(false);
        if(databaseRanges!=null) {
            final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRanges.getDatabaseRangeList().entrySet().iterator();
            while(databaseRangeIter.hasNext()) {
                final DatabaseRange databaseRange = databaseRangeIter.next().getValue();
                final Range sheetRange = databaseRange.getRange();
                if(sheetRange.getSheetIndex()==sheetIndex) {
                    final CellRefRange cellRefRange = sheetRange.getCellRefRange(false);
                    if(cellRefRange!=null) {
                        final CellRefRange newCellRefRange = CellRefRange.insertColumnRange(cellRefRange, start, count, false);
                        if(newCellRefRange!=null&&!newCellRefRange.equals(cellRefRange)) {
                            databaseRange.setRange(new Range(sheetIndex, newCellRefRange));
                            databaseRange.changeColumn(cellRefRange, start, count);
                        }
                    }
                }
            }
        }
    }

    public void deleteColumns(int sheetIndex, int start, int count)
        throws SAXException {

        for(Row row:getRows()) {
            row.deleteCells(start, count);
        }

        getColumn(start, true, true, true);
        if(count>1) {
            getColumn((start+count)-1, true, false, true);
        }
        final Object[] rs = columns.tailSet(new Column(start), true).toArray();
        for(Object obj:rs) {
            final Column co = (Column)obj;
            columns.remove(co);
            if(co.getMin()>=start+count) {
                co.setMin(co.getMin()-count);
                co.setMax(co.getMax()-count);
                columns.add(co);
            }
        }

        // taking care of hyperlinks
        if(hyperlinks!=null) {
            CellRefRange.deleteColumns(hyperlinks, start, count, false, null);
        }

        // taking care of merged cells
        if(mergeCells!=null) {
            CellRefRange.deleteColumns(mergeCells, start, count, true, getMaxColCount());
        }

        // taking care of tables
        final DatabaseRanges databaseRanges = ((SpreadsheetContent)getOwnerDocument()).getDatabaseRanges(false);
        if(databaseRanges!=null) {
            final Iterator<Entry<String, DatabaseRange>> databaseRangeIter = databaseRanges.getDatabaseRangeList().entrySet().iterator();
            while(databaseRangeIter.hasNext()) {
                final DatabaseRange databaseRange = databaseRangeIter.next().getValue();
                final Range sheetRange = databaseRange.getRange();
                if(sheetRange.getSheetIndex()==sheetIndex) {
                    final CellRefRange cellRefRange = sheetRange.getCellRefRange(false);
                    if(cellRefRange!=null) {
                        final CellRefRange newCellRefRange = CellRefRange.deleteColumnRange(cellRefRange, start, count);
                        if(newCellRefRange==null) {
                            databaseRanges.deleteTable(((SpreadsheetContent)getOwnerDocument()), sheetIndex, databaseRange.getName());
                        }
                        else if(!newCellRefRange.equals(cellRefRange)) {
                            databaseRange.setRange(new Range(sheetIndex, newCellRefRange));
                            databaseRange.changeColumn(cellRefRange, start, -count);
                        }
                    }
                }
            }
        }
    }

    /*
     * clearCellRange is not deleting annotations and contentValidations that are stored to a cell
     */
    public void clearCellRange(int r1, int r2, int c1, int c2) {

        final class CellDataPreservePtr {

            final int c;
            Annotation annotation;
            String contentValidationName;

            CellDataPreservePtr(int c) {
                this.c = c;
            }

            void setAnnotation(Annotation annotation) {
                this.annotation = annotation;
            }
            void setContentValidationName(String contentValidationName) {
                this.contentValidationName = contentValidationName;
            }
        }

        List<CellDataPreservePtr> deletions = null;

        getRow(r1, false, true, false);
        getRow(r2, false, false, true);

        final NavigableSet<Row> rowSelection = getRows().subSet(new Row(r1), true, new Row(r2), true);
        final Iterator<Row> rowIter = rowSelection.iterator();

        while(rowIter.hasNext()) {
            final Row row = rowIter.next();

            // creating cell entries....
            row.getCell(c1, false, true, false);
            row.getCell(c2, false, false, true);

            final Iterator<Cell> cellSubSetIter = row.getCells().subSet(new Cell(c1), true, new Cell(c2), true).iterator();
            while(cellSubSetIter.hasNext()) {
                CellDataPreservePtr preservePtr = null;
                final Cell cell = cellSubSetIter.next();
                final Annotation annotation = cell.getAnnotation();
                if(annotation!=null) {
                    if(deletions==null) {
                        deletions = new ArrayList<CellDataPreservePtr>();
                    }
                    preservePtr = new CellDataPreservePtr(cell.getColumn());
                    deletions.add(preservePtr);
                    preservePtr.setAnnotation(annotation);
                }
                final CellAttributesEnhanced enhancedCellAttributes = cell.getCellAttributesEnhanced(false);
                if(enhancedCellAttributes!=null) {
                    if(enhancedCellAttributes.getContentValidationName()!=null) {
                        if(deletions==null) {
                            deletions = new ArrayList<CellDataPreservePtr>();
                        }
                        if(preservePtr==null) {
                            preservePtr = new CellDataPreservePtr(cell.getColumn());
                            deletions.add(preservePtr);
                        }
                        preservePtr.setContentValidationName(enhancedCellAttributes.getContentValidationName());
                    }
                }
                cellSubSetIter.remove();
            }

            // check if we need to create empty repeated cells, only necessary if a ceilling cell is available
            final Cell ceiling = row.getCell(c2+1, false, false, false);
            if(ceiling!=null) {
                final Cell newCell = new Cell(c1);
                newCell.setRepeated((c2-c1)+1);
                row.getCells().add(newCell);
            }

            if(deletions!=null) {
                for(CellDataPreservePtr ptr:deletions) {
                    final Cell cell = row.getCell(ptr.c, true, true, true);
                    if(ptr.annotation!=null) {
                        cell.getContent(true).add(ptr.annotation);
                    }
                    if(ptr.contentValidationName!=null) {
                        cell.setContentValidationName(ptr.contentValidationName);
                    }
                }
            }
        }
    }

    public NamedExpressions getNamedExpressions(boolean forceCreate) {
        if(namedExpressions==null&&forceCreate) {
            namedExpressions = new NamedExpressions((SpreadsheetContent)getOwnerDocument());
        }
        return namedExpressions;
    }

    public ConditionalFormats getConditionalFormats(boolean forceCreate) {
        if(conditionalFormats==null&&forceCreate) {
            conditionalFormats = new ConditionalFormats((SpreadsheetContent)getOwnerDocument());
        }
        return conditionalFormats;
    }

    public List<MergeCell> getMergeCells() {
        if(mergeCells==null) {
            mergeCells = new ArrayList<MergeCell>();
        }
        return mergeCells;
    }

    public List<Hyperlink> getHyperlinks() {
        if(hyperlinks==null) {
            hyperlinks = new ArrayList<Hyperlink>();
        }
        return hyperlinks;
    }

    public Hyperlink getHyperlink(int column, int row) {
        if(hyperlinks!=null) {
            for(Hyperlink hyperlink:hyperlinks) {
                if(hyperlink.getCellRefRange(true).isInside(column, row)) {
                    return hyperlink;
                }
            }
        }
        return null;
    }

    /* returns a live list to the each child */
    public List<Object> getChilds() {
        if(childs==null) {
            childs = new ArrayList<Object>();
        }
        return childs;
    }

    @Override
    public void writeObject(SerializationHandler output)
        throws SAXException {

        if(!getColumns().isEmpty()&&columnPlaceholderAvailable==false) {
            getChilds().add(getColumns().first());
            columnPlaceholderAvailable = true;
        }
        if(!getRows().isEmpty()&&rowPlaceholderAvailable==false) {
            getChilds().add(getRows().first());
            rowPlaceholderAvailable = true;
        }

        SaxContextHandler.startElement(output, getNamespaceURI(), getLocalName(), getNodeName());
        SaxContextHandler.addAttributes(output, getAttributes(), null);

        // office forms needs to be written before shapes
        if(officeForms!=null) {
            SaxContextHandler.serializeElement(output, officeForms);
        }
        if(drawings!=null&&drawings.getCount()>0) {
            drawings.writeSheetDrawings(output);
        }
        for(Object child:getChilds()) {
            if(child instanceof Column) {
                // taking care of column-group-elements
                Column lastColumn = null;
                for(Column column:columns) {
                    if (lastColumn!=null) { lastColumn.closeGroupElements(output, column); }
                    column.openGroupElements(output, lastColumn);
                    column.writeObject(output);
                    lastColumn = column;
                }
                if (lastColumn!=null) { lastColumn.closeGroupElements(output, null); }
            }
            else if(child instanceof Row) {
                Row lastRow = null;
                for(Row row:rows) {
                    if (lastRow!=null) { lastRow.closeGroupElements(output, row); }
                    row.openGroupElements(output, lastRow);
                    row.writeObject(output, this);
                    lastRow = row;
                }
                if (lastRow!=null) { lastRow.closeGroupElements(output, null); }
            }
            else if(child instanceof IElementWriter) {
                ((IElementWriter)child).writeObject(output);
            }
            else if(child instanceof ElementNSImpl) {
                SaxContextHandler.serializeElement(output, (ElementNSImpl)child);
            }
        }
        if(conditionalFormats!=null) {
            conditionalFormats.writeObject(output);
        }
        if(namedExpressions!=null) {
            namedExpressions.writeObject(output);
        }
        SaxContextHandler.endElement(output, getNamespaceURI(), getLocalName(), getNodeName());
    }
}
