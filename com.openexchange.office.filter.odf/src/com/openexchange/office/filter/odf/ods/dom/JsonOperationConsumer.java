/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.TreeSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.FilterException.ErrorCode;
import com.openexchange.office.filter.core.SplitMode;
import com.openexchange.office.filter.core.component.CUtil;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.ColumnRef;
import com.openexchange.office.filter.core.spreadsheet.Interval;
import com.openexchange.office.filter.core.spreadsheet.IntervalArray;
import com.openexchange.office.filter.core.spreadsheet.RowRef;
import com.openexchange.office.filter.core.spreadsheet.SUtil;
import com.openexchange.office.filter.core.spreadsheet.SmlUtils;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.ConfigItem;
import com.openexchange.office.filter.odf.ConfigItemMapEntry;
import com.openexchange.office.filter.odf.ConfigItemMapNamed;
import com.openexchange.office.filter.odf.ConfigItemSet;
import com.openexchange.office.filter.odf.IParagraph;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.Settings;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.components.TextFieldComponent;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawObject;
import com.openexchange.office.filter.odf.draw.Transformer;
import com.openexchange.office.filter.odf.ods.dom.chart.ChartContent;
import com.openexchange.office.filter.odf.ods.dom.components.DrawingComponent;
import com.openexchange.office.filter.odf.ods.dom.components.FrameComponent;
import com.openexchange.office.filter.odf.odt.dom.Annotation;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;
import com.openexchange.office.filter.odf.odt.dom.TextField;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class JsonOperationConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(JsonOperationConsumer.class);

    private final OdfOperationDoc opsDoc;
    private final OdfSpreadsheetDocument doc;
    private final SpreadsheetContent content;
    private final StyleManager styleManager;
    private final Settings settings;

    public JsonOperationConsumer(OdfOperationDoc opsDoc)
        throws SAXException {

        this.opsDoc = opsDoc;
        doc = (OdfSpreadsheetDocument)opsDoc.getDocument();
        doc.getStylesDom();
        content = (SpreadsheetContent)doc.getContentDom();
        styleManager = doc.getStyleManager();
        settings = doc.getSettingsDom();
    }

    public int applyOperations(JSONArray operations)
        throws Exception {

        for (int i = 0; i < operations.length(); i++) {
            doc.getPackage().setSuccessfulAppliedOperations(i);
            final JSONObject op = (JSONObject) operations.get(i);
            final OCValue opName = OCValue.fromValue(op.getString(OCKey.NAME.value()));
            switch(opName)
            {
                case CHANGE_CELLS : {
                    changeCells(op.getInt(OCKey.SHEET.value()), op.getJSONObject(OCKey.CONTENTS.value()));
                    break;
                }
                case MERGE_CELLS : {
                    mergeCells(op.getInt(OCKey.SHEET.value()), SmlUtils.createCellRefRanges(op.getString(OCKey.RANGES.value())), op.optString(OCKey.TYPE.value(), "merge"));
                    break;
                }
                case INSERT_ROWS : {
                    insertRows(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(false, op.getString(OCKey.INTERVALS.value())));
                    break;
                }
                case DELETE_ROWS : {
                    deleteRows(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(false, op.getString(OCKey.INTERVALS.value())));
                    break;
                }
                case INSERT_COLUMNS : {
                    insertColumns(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(true, op.getString(OCKey.INTERVALS.value())));
                    break;
                }
                case DELETE_COLUMNS : {
                    deleteColumns(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(true, op.getString(OCKey.INTERVALS.value())));
                    break;
                }
                case INSERT_SHEET : {
                    insertSheet(op.getInt(OCKey.SHEET.value()), op.getString(OCKey.SHEET_NAME.value()), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case MOVE_SHEET : {
                    content.moveSheet(op.getInt(OCKey.SHEET.value()), op.getInt(OCKey.TO.value()));
                    break;
                }
                case MOVE_SHEETS : {
                    SUtil.moveSheets(content, op.getJSONArray(OCKey.SHEETS.value()));
                    break;
                }
                case DELETE_SHEET : {
                    content.deleteSheet(op.getInt(OCKey.SHEET.value()));
                    break;
                }
                case CHANGE_COLUMNS : {
                    changeColumns(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(true, op.getString(OCKey.INTERVALS.value())), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.S.value(), null));
                    break;
                }
                case CHANGE_ROWS : {
                    changeRows(op.getInt(OCKey.SHEET.value()), IntervalArray.createIntervals(false, op.getString(OCKey.INTERVALS.value())), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.S.value(), null));
                    break;
                }
                case INSERT_AUTO_STYLE : {
                    styleManager.insertAutoStyle(op.optString(OCKey.TYPE.value(), "cell"), op.getString(OCKey.STYLE_ID.value()), op.getJSONObject(OCKey.ATTRS.value()), op.optBoolean(OCKey.DEFAULT.value()));
                    break;
                }
                case DELETE_AUTO_STYLE : {
                    styleManager.deleteAutoStyle(op.optString(OCKey.TYPE.value(), "cell"), op.getString(OCKey.STYLE_ID.value()));
                    break;
                }
                case CHANGE_AUTO_STYLE : {
                    styleManager.changeAutoStyle(op.optString(OCKey.TYPE.value(), "cell"), op.getString(OCKey.STYLE_ID.value()), op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_STYLE_SHEET : {
                    styleManager.insertStyleSheet(op.optString(OCKey.TYPE.value(), "cell"), op.getString(OCKey.STYLE_ID.value()), op.optString(OCKey.STYLE_NAME.value()), op.optJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.PARENT.value()), op.optBoolean(OCKey.DEFAULT.value()));
                    break;
                }
                case INSERT_DRAWING : {
                    insertDrawing(op.getJSONArray(OCKey.START.value()), op.getString(OCKey.TYPE.value()), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case DELETE_DRAWING : {
                    deleteDrawing(op.getJSONArray(OCKey.START.value()));
                    break;
                }
                case MOVE_DRAWING : {
                    moveDrawing(op.getJSONArray(OCKey.START.value()), op.getJSONArray(OCKey.TO.value()));
                    break;
                }
                case CHANGE_DRAWING : {
                    changeDrawing(op.getJSONArray(OCKey.START.value()), op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_NAME : {
                    changeOrInsertName(op.optInt(OCKey.SHEET.value(), -1), op.getString(OCKey.LABEL.value()), op.getString(OCKey.FORMULA.value()), null, op.optBoolean(OCKey.IS_EXPR.value(), false), op.getString(OCKey.REF.value()), true);
                    break;
                }
                case CHANGE_NAME : {
                    changeOrInsertName(op.optInt(OCKey.SHEET.value(), -1), op.getString(OCKey.LABEL.value()), op.optString(OCKey.FORMULA.value(), null), op.optString(OCKey.NEW_LABEL.value(), null), op.optBoolean(OCKey.IS_EXPR.value(), false), op.optString(OCKey.REF.value(), null), false);
                    break;
                }
                case DELETE_NAME : {
                    deleteName(op.optInt(OCKey.SHEET.value(), -1), op.getString(OCKey.LABEL.value()));
                    break;
                }
                case INSERT_TABLE : {
                    insertTable(op.getInt(OCKey.SHEET.value()), op.optString(OCKey.TABLE.value(), null), op.getString(OCKey.RANGE.value()), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case CHANGE_TABLE : {
                    insertTable(op.getInt(OCKey.SHEET.value()), op.optString(OCKey.TABLE.value(), null), op.optString(OCKey.RANGE.value(), null), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case DELETE_TABLE : {
                    deleteTable(op.getInt(OCKey.SHEET.value()), op.optString(OCKey.TABLE.value(), null));
                    break;
                }
                case CHANGE_TABLE_COLUMN : {
                    changeTableColumn(op.getInt(OCKey.SHEET.value()), op.optString(OCKey.TABLE.value(), null), op.getInt(OCKey.COL.value()), op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_HYPERLINK : {
                    insertHyperlink(op.getInt(OCKey.SHEET.value()), SmlUtils.createCellRefRanges(op.getString(OCKey.RANGES.value())), op.getString(OCKey.URL.value()));
                    break;
                }
                case DELETE_HYPERLINK : {
                    deleteHyperlink(op.getInt(OCKey.SHEET.value()), SmlUtils.createCellRefRanges(op.getString(OCKey.RANGES.value())));
                    break;
                }
                case SET_DOCUMENT_ATTRIBUTES : {
                    setDocumentAttributes(op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_CF_RULE : {
                    changeOrinsertCondFormatRule(true, op.getInt(OCKey.SHEET.value()), Integer.valueOf(op.getString(OCKey.ID.value())), op.optString(OCKey.REF.value(), null), op.getString(OCKey.RANGES.value()), op.optString(OCKey.TYPE.value(), "formula"),
                        op.has(OCKey.VALUE1.value()) ? op.get(OCKey.VALUE1.value()) : "", op.optString(OCKey.VALUE2.value(), ""), op.optInt(OCKey.PRIORITY.value(), 0), op.optBoolean(OCKey.STOP.value(), false),
                        op.optJSONObject("dataBar"), op.optJSONObject(OCKey.ICON_SET.value()), op.optJSONArray(OCKey.COLOR_SCALE.value()),
                        op.has(OCKey.ATTRS.value()) ? op.getJSONObject(OCKey.ATTRS.value()) : new JSONObject());
                    break;
                }
                case CHANGE_CF_RULE : {
                    changeOrinsertCondFormatRule(false, op.getInt(OCKey.SHEET.value()), Integer.valueOf(op.getString(OCKey.ID.value())), op.optString(OCKey.REF.value(), null), op.optString(OCKey.RANGES.value(), null), op.optString(OCKey.TYPE.value(), null),
                        op.opt(OCKey.VALUE1.value()), op.optString(OCKey.VALUE2.value(), null), op.optInt(OCKey.PRIORITY.value(), 0), op.optBoolean(OCKey.STOP.value()),
                        op.optJSONObject(OCKey.DATA_BAR.value()), op.optJSONObject(OCKey.ICON_SET.value()), op.optJSONArray(OCKey.COLOR_SCALE.value()),
                        op.has(OCKey.ATTRS.value()) ? op.getJSONObject(OCKey.ATTRS.value()) : new JSONObject());
                    break;
                }
                case DELETE_CF_RULE : {
                    deleteCondFormatRule(op.getInt(OCKey.SHEET.value()), Integer.valueOf(op.getString(OCKey.ID.value())));
                    break;
                }
                case CHANGE_SHEET : {
                    changeSheet(op.getInt(OCKey.SHEET.value()), op.optString(OCKey.SHEET_NAME.value(), null), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                }
                case INSERT_CHART_SERIES :
                    getChart(op.getJSONArray(OCKey.START.value())).insertSeries(op.getInt(OCKey.SERIES.value()), op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                case CHANGE_CHART_SERIES :
                    getChart(op.getJSONArray(OCKey.START.value())).changeSeries(op.getInt(OCKey.SERIES.value()), op.getJSONObject(OCKey.ATTRS.value()), true);
                    break;
                case DELETE_CHART_SERIES :
                    getChart(op.getJSONArray(OCKey.START.value())).deleteSeries(op.getInt(OCKey.SERIES.value()));
                    break;
                case CHANGE_CHART_AXIS :
                    getChart(op.getJSONArray(OCKey.START.value())).changeAxis(op.getInt(OCKey.AXIS.value()), op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                case CHANGE_CHART_GRID :
                    getChart(op.getJSONArray(OCKey.START.value())).changeGrid(op.getInt(OCKey.AXIS.value()), op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                case CHANGE_CHART_TITLE :
                    getChart(op.getJSONArray(OCKey.START.value())).changeTitle(op.has(OCKey.AXIS.value()) ? op.getInt(OCKey.AXIS.value()) : null, op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                case CHANGE_CHART_LEGEND :
                    getChart(op.getJSONArray(OCKey.START.value())).changeLegend(op.getJSONObject(OCKey.ATTRS.value()));
                    break;
                case INSERT_PARAGRAPH :
                    insertParagraph(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value(), ""), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                case SPLIT_PARAGRAPH :
                    splitParagraph(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value(), ""));
                    break;
                case MERGE_PARAGRAPH :
                    mergeParagraph(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value(), ""));
                    break;
                case INSERT_TEXT : {
                    insertText(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value(), ""), op.optJSONObject(OCKey.ATTRS.value()), op.getString(OCKey.TEXT.value()).replaceAll("\\p{Cc}", " "));
                    break;
                }
                case MOVE :
                    move(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value(), ""), op.optJSONArray(OCKey.END.value()), op.getJSONArray(OCKey.TO.value()));
                    break;
                case DELETE :
                    CUtil.delete(content.getRootComponent(opsDoc, op.optString(OCKey.TARGET.value(), "")), op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()));
                    break;
                case SET_ATTRIBUTES :
                    setAttributes(op.getJSONObject(OCKey.ATTRS.value()), op.optString(OCKey.TARGET.value(), ""), op.getJSONArray(OCKey.START.value()), op.optJSONArray(OCKey.END.value()));
                    break;
                case INSERT_HARD_BREAK :
                    insertHardBreak(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value(), ""), /* op.optString(OCKey.TYPE.shortName()), */ op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                case INSERT_TAB :
                    insertTab(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value(), ""), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                case INSERT_FIELD :
                    insertField(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value(), ""), op.getString(OCKey.TYPE.value()), op.getString(OCKey.REPRESENTATION.value()), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                case UPDATE_FIELD :
                    updateField(op.getJSONArray(OCKey.START.value()), op.optString(OCKey.TARGET.value(), ""), op.optString(OCKey.TYPE.value()), op.optString(OCKey.REPRESENTATION.value()));
                    break;
                case INSERT_NOTE :
                    insertNote(op.getInt(OCKey.SHEET.value()), CellRef.createCellRef(op.getString(OCKey.ANCHOR.value())), op.getString(OCKey.TEXT.value()), op.optString(OCKey.AUTHOR.value(), null), op.optString(OCKey.USER_ID.value(), null), op.optString(OCKey.DATE.value(), null), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                case CHANGE_NOTE :
                    changeNote(op.getInt(OCKey.SHEET.value()), CellRef.createCellRef(op.getString(OCKey.ANCHOR.value())), op.optString(OCKey.TEXT.value(), null), op.optJSONObject(OCKey.ATTRS.value()));
                    break;
                case DELETE_NOTE :
                    deleteNote(op.getInt(OCKey.SHEET.value()), CellRef.createCellRef(op.getString(OCKey.ANCHOR.value())));
                    break;
                case MOVE_NOTES :
                    moveNotes(op.getInt(OCKey.SHEET.value()), op.getString(OCKey.FROM.value()).split(" ", -1), op.getString(OCKey.TO.value()).split(" ", -1));
                    break;
                case INSERT_DV_RULE : {
                    ContentValidation.insertValidation(content, op);
                    break;
                }
                case CHANGE_DV_RULE : {
                    ContentValidation.changeValidation(content, op);
                    break;
                }
                case DELETE_DV_RULE : {
                    ContentValidation.deleteValidation(content, op);
                    break;
                }
                case NO_OP : {
                    break;
                }
                case CREATE_ERROR : {
                    throw new FilterException("createError operation detected: " + opName, ErrorCode.UNSUPPORTED_OPERATION_USED);
                }
                default: {
                    final String unsupportedOps = opName==OCValue.UNKNOWN_VALUE ? op.getString(OCKey.NAME.value()) : opName.value(true);
                    LOG.warn("Ignoring unsupported operation: " + unsupportedOps);
                }
            }
        }
        Transformer.prepareSave(opsDoc);
        doc.getPackage().setSuccessfulAppliedOperations(operations.length());
        return 1;
    }

    private ChartContent getChart(JSONArray start) {
        final IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, null).getComponent(start, start.length());
        DrawFrame drawFrame;
        if (component instanceof DrawingComponent) {
            drawFrame = (DrawFrame) ((DrawingComponent)component).getShape();
        } else {
            drawFrame = ((FrameComponent)component).getDrawFrame();
        }
        return ((DrawObject)drawFrame.getDrawing()).getChart();
    }

    private void changeOrinsertCondFormatRule(boolean insert, int sheetIndex, int id, String ref, String ranges, String type, Object value1, String value2, int priority, boolean stop, JSONObject dataBar, JSONObject iconSet, JSONArray colorScale, JSONObject attrs)
        throws JSONException {

        final List<Condition> conditions = content.getContent().get(sheetIndex).getConditionalFormats(true).getConditionalFormatList();
        Condition condition;
        if(insert) {
            switch (type) {
                case "dataBar" :
                    condition = new EntryHolderCondition(ref, priority, new Ranges(content, ranges), "data-bar");
                    break;
                case "colorScale" :
                    condition = new EntryHolderCondition(ref, priority, new Ranges(content, ranges), "color-scale");
                    break;
                case "iconSet" :
                    condition = new EntryHolderCondition(ref, priority, new Ranges(content, ranges), "icon-set");
                    break;
                default:
                    condition = new Condition(ref, priority, new Ranges(content, ranges));
                    break;
            }
            conditions.add(id, condition);
        }
        else {
            condition = conditions.get(id);
            if(ranges!=null) {
                condition.setRanges(new Ranges(content, ranges));
            }
            if(ref!=null) {
            	condition.setBaseCellAddress(ref);
            }
        }
        condition.applyCondFormatRuleOperation(doc, type, value1, value2, priority, stop, dataBar, iconSet, colorScale, attrs);
    }

    private void deleteCondFormatRule(int sheetIndex, int id) {


        content.getContent().get(sheetIndex).getConditionalFormats(false).getConditionalFormatList().remove(id);
    }

    private void setDocumentAttributes(JSONObject attrs) {

        final JSONObject documentAttrs = attrs.optJSONObject(OCKey.DOCUMENT.value());
        if(documentAttrs!=null) {
            final Object activeSheet = documentAttrs.opt(OCKey.ACTIVE_SHEET.value());
            if(activeSheet instanceof Integer) {
                final String sheetName = content.getContent().get(((Integer)activeSheet).intValue()).getName();
                if(sheetName!=null) {
                    settings.setActiveSheet(sheetName);
                }
            }
            final Object calcOnLoad = documentAttrs.opt(OCKey.CALC_ON_LOAD.value());
            if(calcOnLoad instanceof Boolean) {
                final ConfigItemSet configItemSet = settings.getConfigItemSet("ooo:configuration-settings", true);
                final HashMap<String, IElementWriter> configItems = configItemSet.getItems();
                final ConfigItem calcOnLoadConfigItem = new ConfigItem("AutoCalculate", (Boolean)calcOnLoad);
                configItems.put("AutoCalculate", calcOnLoadConfigItem);
            }
        }
    }

    private void insertHyperlink(int sheet, CellRefRange[] ranges, String url) {
        for(CellRefRange cellRefRange:ranges) {
            final List<Hyperlink> hyperlinkList = content.getContent().get(sheet).getHyperlinks();
            for(int i=hyperlinkList.size()-1;i>=0;i--) {
                final Hyperlink hyperlink = hyperlinkList.get(i);
                if((hyperlink.getCellRefRange(false)==null)||cellRefRange.contains(hyperlink.getCellRefRange(false))) {
                    hyperlinkList.remove(i);
                }
            }
            hyperlinkList.add(new Hyperlink(cellRefRange, url));
        }
    }

    private void deleteHyperlink(int sheet, CellRefRange[] ranges) {
        for(CellRefRange cellRefRange:ranges) {
            final List<Hyperlink> hyperlinkList = content.getContent().get(sheet).getHyperlinks();
            for(int i=hyperlinkList.size()-1;i>=0;i--) {
                final Hyperlink hyperlink = hyperlinkList.get(i);
                if((hyperlink.getCellRefRange(false)==null)||cellRefRange.intersects(hyperlink.getCellRefRange(false))) {
                    hyperlinkList.remove(i);
                }
            }
        }
    }

    public IComponent<OdfOperationDoc> insertDrawing(JSONArray start, String type, JSONObject attrs) throws Exception {

        ComponentType childComponentType = ComponentType.AC_IMAGE;
        if(type.equals("shape")) {
            if(attrs==null||!attrs.has(OCKey.GEOMETRY.value())) {
                childComponentType = ComponentType.AC_FRAME;
            }
            else {
                childComponentType = ComponentType.AC_SHAPE;
            }
        }
        else if(type.equals("connector")) {
            childComponentType = ComponentType.AC_CONNECTOR;
        }
        else if(type.equals("chart")) {
            childComponentType = ComponentType.AC_CHART;
        }
        else if(type.equals("group")) {
            childComponentType = ComponentType.AC_GROUP;
        }
        else if(type.equals("image")) {
            childComponentType = ComponentType.AC_IMAGE;
        }
        return content.getRootComponent(opsDoc, null).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, childComponentType);
    }

    public void moveDrawing(JSONArray start, JSONArray to) throws JSONException {
        final Sheet sheet = content.getContent().get(start.getInt(0));
        sheet.getDrawings().moveDrawing(start.getInt(1), to.getInt(1));
    }

    public void deleteDrawing(JSONArray start)
        throws JSONException {

        final Sheet sheet = content.getContent().get(start.getInt(0));
        sheet.getDrawings().deleteDrawing(start.getInt(1), opsDoc);
    }

    private void changeOrInsertName(int sheet, String label, String formula, String newLabel, boolean expr, String ref, boolean insert) {

        final NamedExpressions namedExpressions = sheet==-1?content.getNamedExpressions(true):content.getContent().get(sheet).getNamedExpressions(true);
        NamedExpression namedExpression;
        if(insert) {
            namedExpression = new NamedExpression(label);
        }
        else {
            namedExpression = namedExpressions.getExpressionList().remove(label);
            if(newLabel!=null) {
                namedExpression.setName(newLabel);
            }
        }
        if(formula!=null) {
            if(expr) {
                namedExpression.setExpression(formula);
            }
            else {
                namedExpression.setCellRangeAddress(SmlUtils.getAddressWithoutBraces(formula));
            }
        }
        if(ref!=null) {
            namedExpression.setBaseCellAddress(SmlUtils.getAddressWithoutBraces(ref));
        }
        namedExpressions.getExpressionList().put(namedExpression.getName(), namedExpression);
    }

    private void deleteName(int sheet, String label) {

        final NamedExpressions namedExpressions = sheet==-1?content.getNamedExpressions(false):content.getContent().get(sheet).getNamedExpressions(false);
        namedExpressions.getExpressionList().remove(label);
    }

    private void insertTable(int sheetIndex, String table, String range, JSONObject attrs) {

        final DatabaseRange databaseRange = content.getDatabaseRanges(true).getDatabaseRange(content, sheetIndex, table, true);
        databaseRange.setDisplayFilterButtons(true);
        final Range sheetRange = databaseRange.getRange();
        if(range!=null) {
            sheetRange.setCellRefRange(CellRefRange.createCellRefRange(range));
        }
        sheetRange.setSheetIndex(sheetIndex);
        if(attrs!=null) {
            final JSONObject tableAttrs = attrs.optJSONObject(OCKey.TABLE.value());
            if(tableAttrs!=null) {
                Object containsHeader = tableAttrs.opt(OCKey.HEADER_ROW.value());
                if(containsHeader instanceof Boolean) {
                    databaseRange.setContainsHeader((Boolean)containsHeader);
                }
                Object hideButtons = tableAttrs.opt(OCKey.HIDE_BUTTONS.value());
                if(hideButtons!=null) {
                    if(hideButtons==JSONObject.NULL) {
                        databaseRange.setDisplayFilterButtons(true);
                    }
                    else if(hideButtons instanceof Boolean) {
                        databaseRange.setDisplayFilterButtons(!((Boolean)hideButtons).booleanValue());
                    }
                }
            }
        }
    }

    private void deleteTable(int sheet, String table) {
        content.getDatabaseRanges(false).deleteTable(content, sheet, table);
    }

    private void changeTableColumn(int sheet, String table, int col, JSONObject attrs) {
        content.getDatabaseRanges(false).getDatabaseRange(content, sheet, table, false).changeTableColumn(content, col, attrs);
    }

    public void changeDrawing(JSONArray start, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, null).getComponent(start, start.length())
            .applyAttrsFromJSON(attrs);
    }

    private void mergeCells(int sheetIndex, CellRefRange[] ranges, String type) {

        for(CellRefRange cellRefRange:ranges) {
            final Sheet sheet = content.getContent().get(sheetIndex);

            // first we will remove each mergeCell that is covering our new mergeCellRange
            final List<MergeCell> mergeCellList = sheet.getMergeCells();
            for(int i=mergeCellList.size()-1;i>=0;i--) {
                final MergeCell mergeCell = mergeCellList.get(i);
                if(cellRefRange.intersects(mergeCell.getCellRefRange(true))) {
                    mergeCellList.remove(i);
                }
            }
            if(type.equals("merge")) {
                addMergeCellRange(sheet, cellRefRange);
            }
            else if(type.equals("horizontal")) {
                for(int row=cellRefRange.getStart().getRow();row<=cellRefRange.getEnd().getRow();row++) {
                    addMergeCellRange(sheet, new CellRefRange(new CellRef(cellRefRange.getStart().getColumn(), row), new CellRef(cellRefRange.getEnd().getColumn(), row)));
                }
            }
            else if(type.equals("vertical")) {
                for(int column=cellRefRange.getStart().getColumn();column<=cellRefRange.getEnd().getColumn();column++) {
                    addMergeCellRange(sheet, new CellRefRange(new CellRef(column, cellRefRange.getStart().getRow()), new CellRef(column, cellRefRange.getEnd().getRow())));
                }
            }
        }
    }

    private void addMergeCellRange(Sheet sheet, CellRefRange mergeCellRange) {
        final List<MergeCell> mergeCellList = sheet.getMergeCells();
        mergeCellList.add(new MergeCell(mergeCellRange));
        // creating the last row (with ceiling cut)
        sheet.getRow(mergeCellRange.getEnd().getRow(), true, false, true);
        // creating the first row without repetings (needed for covered cells, they are not allowed to be repeated)
        sheet.getRow(mergeCellRange.getStart().getRow(), true, true, true);

        final NavigableSet<Row> rowSelection = sheet.getRows().subSet(new Row(mergeCellRange.getStart().getRow()), true, new Row(mergeCellRange.getEnd().getRow()), true);
        final Iterator<Row> rowIter = rowSelection.iterator();
        boolean first = true;
        while(rowIter.hasNext()) {
            final Row rowEntry = rowIter.next();
            rowEntry.getCell(mergeCellRange.getEnd().getColumn(), true, false, true);
            rowEntry.getCell(mergeCellRange.getStart().getColumn(), true, true, first);
            first = false;
        }
    }

    public void insertRows(int sheetIndex, List<Interval> intervals)
        throws Exception {

        for(Interval interval:intervals) {
            insertRows(sheetIndex, interval);
        }
    }

    public void insertRows(int sheetIndex, Interval interval)
        throws Exception {

        final int count = getColumnRowCount(interval.getMin().getValue(), interval.getMax().getValue());
        content.getContent().get(sheetIndex).insertRows(sheetIndex, interval.getMin().getValue(), count);
    }

    public void deleteRows(int sheetIndex, List<Interval> intervals)
        throws Exception {

        for(Interval interval:intervals) {
            deleteRows(sheetIndex, interval);
        }
    }

    public void deleteRows(int sheetIndex, Interval interval)
        throws Exception {

        final int count = getColumnRowCount(interval.getMin().getValue(), interval.getMax().getValue());
        content.getContent().get(sheetIndex).deleteRows(sheetIndex, interval.getMin().getValue(), count);
    }

    public void insertColumns(int sheetIndex, IntervalArray intervals)
        throws Exception {

        for(Interval interval:intervals) {
            insertColumns(sheetIndex, interval);
        }
    }

    public void insertColumns(int sheetIndex, Interval interval)
        throws Exception {

        final int count = getColumnRowCount(interval.getMin().getValue(), interval.getMax().getValue());
        content.getContent().get(sheetIndex).insertColumns(sheetIndex, interval.getMin().getValue(), count);
    }

    public void deleteColumns(int sheetIndex, List<Interval> intervals)
        throws Exception {

        for(Interval interval:intervals) {
            deleteColumns(sheetIndex, interval);
        }
    }

    public void deleteColumns(int sheetIndex, Interval interval)
        throws Exception {

        final int count = getColumnRowCount(interval.getMin().getValue(), interval.getMax().getValue());
        content.getContent().get(sheetIndex).deleteColumns(sheetIndex, interval.getMin().getValue(), count);
    }

    private int getColumnRowCount(int start, int optEnd) {
        if(start<0) {
            throw new RuntimeException("ods::deleteColumns: start<0");
        }
        int count = 1;
        if(optEnd!=-1) {
            if(optEnd<start) {
                throw new RuntimeException("ods::deleteColumns: optEnd<start");
            }
            count = (optEnd-start)+1;
        }
        return count;
    }

    public void changeCells(int sheetIndex, JSONObject contents) throws JSONException {

        final Sheet sheet = content.getContent().get(sheetIndex);;

        final Iterator<Entry<String, Object>> iter = contents.entrySet().iterator();
        while(iter.hasNext()) {

            final Entry<String, Object> entry = iter.next();
            final Object o = entry.getValue();
            JSONObject cellData = null;

            if(o==JSONObject.NULL) {
                cellData = new JSONObject(1);
                cellData.put(OCKey.V.value(), JSONObject.NULL);
            }
            else if(o instanceof Number  || o instanceof String || o instanceof Boolean) {
                cellData = new JSONObject(1);
                cellData.put(OCKey.V.value(), o);
            }
            else if(o instanceof JSONObject) {
                cellData = (JSONObject)o;
            }
            if(cellData!=null) {

                final CellRefRange cellRefRange = CellRefRange.createCellRefRange(entry.getKey());
                if(cellData.optBoolean(OCKey.U.value())) {
                    sheet.clearCellRange(cellRefRange.getStart().getRow(), cellRefRange.getEnd().getRow(), cellRefRange.getStart().getColumn(), cellRefRange.getEnd().getColumn());
                    if(cellData.length()==1) {
                        continue;
                    }
                }
                else {
                    // creating row entries...
                    sheet.getRow(cellRefRange.getEnd().getRow(), true, false, true); // ensure that the exact rowrange is available without extension
                    sheet.getRow(cellRefRange.getStart().getRow(), true, true, false);
                }

                final String s = cellData.optString(OCKey.S.value(), null);
                final Object f = cellData.opt(OCKey.F.value());
                final Object v = cellData.opt(OCKey.V.value());
                final String e = cellData.optString(OCKey.E.value(), null);
                final String m = cellData.optString(OCKey.MR.value(), null);

                if(f instanceof String  && !cellRefRange.single()) { // oo / lo does not like repetitions with formulas :-(
                    for(int ri = cellRefRange.getStart().getRow(); ri <= cellRefRange.getEnd().getRow(); ri++) {
                        final Row row = sheet.getRow(ri, true, true, true);
                        for(int ci = cellRefRange.getStart().getColumn(); ci <= cellRefRange.getEnd().getColumn(); ci++) {
                            row.getCell(ci, true, true, true); // now this cell is without repetitions
                        }
                    }
                }

                final NavigableSet<Row> rowSelection = sheet.getRows().subSet(new Row(cellRefRange.getStart().getRow()), true, new Row(cellRefRange.getEnd().getRow()), true);
                final Iterator<Row> rowIter = rowSelection.iterator();
                while(rowIter.hasNext()) {
                    final Row row = rowIter.next();
                    // creating cell entries....
                    row.getCell(cellRefRange.getEnd().getColumn(), true, false, true);
                    row.getCell(cellRefRange.getStart().getColumn(), true, true, false);

                    final NavigableSet<Cell> cellSelection = row.getCells().subSet(new Cell(cellRefRange.getStart().getColumn()), true, new Cell(cellRefRange.getEnd().getColumn()), true);
                    final Iterator<Cell> cellIter = cellSelection.iterator();
                    while(cellIter.hasNext()) {
                        changeCell(cellIter.next(), s, f, v, e, m);
                    }
                }
            }
        }
    }

    private void changeCell(Cell cell, String s, Object f, Object v, String e, String m) {
        if(s!=null) {
            cell.setCellStyle(s);
        }
        if(e!=null) {
            cell.setCellContent(new Cell.ErrorCode(e));
        }
        else if(v==JSONObject.NULL) {
            cell.setCellContent(null);
        }
        else if(v!=null) {
            cell.setCellContent(v);
        }

        if(f instanceof String) {
            cell.setCellFormula((String)f);
        }
        else if(f==JSONObject.NULL) {
            cell.setCellFormula(null);
        }
        if(m!=null) {
            if(m==JSONObject.NULL) {
                final CellAttributesEnhanced enhancedCellAttributes = cell.getCellAttributesEnhanced(false);
                if(enhancedCellAttributes!=null) {
                    enhancedCellAttributes.setNumberMatrixColumnsSpanned(null);
                    enhancedCellAttributes.setNumberMatrixRowsSpanned(null);
                }
            }
            else {
                final CellAttributesEnhanced enhancedCellAttributes = cell.getCellAttributesEnhanced(true);
                final CellRefRange range = CellRefRange.createCellRefRange(m);
                enhancedCellAttributes.setNumberMatrixColumnsSpanned(Integer.toString(range.getColumns()));
                enhancedCellAttributes.setNumberMatrixRowsSpanned(Integer.toString(range.getRows()));
            }
        }
    }

    public void insertSheet(int sheetIndex, String sheetName, JSONObject attrs)
        throws JSONException {

        content.insertSheet(sheetIndex, sheetName);
        final HashMap<String, Object> destAttrs = new HashMap<String, Object>(1);
        final HashMap<String, Object> columnAttrs = new HashMap<String, Object>(1);
        destAttrs.put(OCKey.COLUMN.value(), columnAttrs);
        columnAttrs.put(OCKey.WIDTH.value(), 2258);
        if (attrs!=null) {
            OpAttrs.deepCopy(attrs.asMap(), destAttrs);
        }
        changeSheet(sheetIndex, null, new JSONObject(destAttrs));

        // insert a cell to enable a way to insert a image or chart copy without edit the sheet (Bug 61860)
        Row row = content.getContent().get(sheetIndex).getRow(0, true, false, true);
        row.getCell(0, true, true, true);
    }

    public void setSheetName(int sheetIndex, String sheetName) {
        final Sheet _sheet = content.getContent().get(sheetIndex);

        final String oldSheetName = _sheet.getName();
        final String encodedOldName = encodeSheetName(oldSheetName);
        final String encodedNewName = encodeSheetName(sheetName);

        if(encodedOldName.length()<=0&&encodedNewName.length()<=0) {
            return;
        }
        _sheet.setName(sheetName);

        // taking care of viewSettings
        settings.setActiveSheet(updateSheetName(settings.getActiveSheet(), encodedOldName, encodedNewName));
        final ConfigItemMapEntry globalViewSettings = settings.getGlobalViewSettings(false);
        if(globalViewSettings!=null) {
            final ConfigItemMapNamed viewTables = settings.getViewTables(globalViewSettings, false);
            if(viewTables!=null) {
                final HashMap<String, ConfigItemMapEntry> viewTableMap = viewTables.getItems();
                final ConfigItemMapEntry oldViewSettings = viewTableMap.remove(encodedOldName);
                if(oldViewSettings!=null) {
                    oldViewSettings.setName(encodedNewName);
                    viewTableMap.put(encodedNewName, oldViewSettings);
                }
            }
        }
    }

    public static int getNameOffset(String content, int startOffset) {

        boolean stringLiteral = false;
        boolean complexString = false;
        for(int i=startOffset; i<content.length();i++) {
            final char n = content.charAt(i);
            if(complexString) {
                if(n=='\'') {
                    if(i+1<content.length()&&content.charAt(i+1)=='\'') {
                        i++;
                    }
                    else {
                        complexString = false;
                    }
                }
            }
            else if(stringLiteral) {
                if(n=='"') {
                    if(i+1<content.length()&&content.charAt(i+1)=='"') {
                        i++;
                    }
                    else {
                        stringLiteral = false;
                    }
                }
            }
            else if(n=='\'') {
                complexString = true;
            }
            else if(n=='"') {
                stringLiteral = true;
            }
            else if(n=='[') {
                return i;
            }
        }
        return -1;
    }

    public static String updateSheetName(String content, String oldName, String newName) {
        String newContent = content;
        if(content!=null&&content.length()>oldName.length()) {
            newContent = content.replaceAll(oldName, newName);
        }
        return newContent;
    }

    private static String simpleCharPattern ="^[\\w.\\xa1-\\u2027\\u202a-\\uffff]+$";

    public static String updateFormulaSheetName(String content, String oldName, String newName) {
        String newContent = content;
        List<Integer> replaceOffsets = null;
        if(content!=null&&content.length()>oldName.length()) {
            for(int startOffset = 0; startOffset<content.length();) {
                final int nameOffset = getNameOffset(content, startOffset);
                if(nameOffset<0) {
                    break;
                }
                final int possibleLength = nameOffset - startOffset;
                if(possibleLength>=oldName.length()) {
                    boolean replace = false;
                    final boolean isComplex = content.charAt(nameOffset-1)=='\'';
                    final String subString = content.substring(nameOffset-oldName.length(), nameOffset);

                    final int mOffset = nameOffset - oldName.length();
                    if(isComplex&&oldName.charAt(0)=='\'') {
                        if(subString.equals(oldName)) {
                            replace = true;
                        }
                    }
                    else if(oldName.charAt(0)!='\'') {
                        if(subString.equals(oldName)) {
                            if(mOffset>startOffset) {
                                final String prev = content.substring(mOffset-1, mOffset);
                                if(!prev.matches(simpleCharPattern)) {
                                    replace = true;
                                }
                            }
                            else {
                                replace = true;
                            }
                        }
                    }
                    if(replace) {
                        if(replaceOffsets==null) {
                            replaceOffsets = new ArrayList<Integer>();
                        }
                        replaceOffsets.add(mOffset);
                    }
                }
                startOffset = nameOffset + 1;
            }
        }
        if(replaceOffsets!=null) {
            final StringBuffer buffer = new StringBuffer(content);
            for(int i=replaceOffsets.size()-1;i>=0;i--) {
                final int offset = replaceOffsets.get(i);
                buffer.replace(offset, offset + oldName.length(), newName);
            }
            newContent = buffer.toString();
        }
        return newContent;
    }

    public static String encodeSheetName(String sheetName) {
        final StringBuffer encodedName = new StringBuffer(sheetName.length());
        for(int i=0; i<sheetName.length();i++) {
            final char c = sheetName.charAt(i);
            encodedName.append(c);
            if(c=='\'') {
                encodedName.append(c);
            }
        }
        if(!sheetName.matches(simpleCharPattern)) {
            encodedName.insert(0, '\'');
            encodedName.append('\'');
        }
        return encodedName.toString();
    }

    public void changeColumns(int sheetIndex, List<Interval> intervals, JSONObject optAttrs, String s)
        throws JSONException {

        for(Interval interval:intervals) {
            changeColumns(sheetIndex, interval, optAttrs, s);
        }
    }

    public void changeColumns(int sheetIndex, Interval interval, JSONObject optAttrs, String s)
        throws JSONException {

        if(s==null&&optAttrs==null) {
            return;
        }
        final Sheet sheet = content.getContent().get(sheetIndex);

        // #55538#, ODS does not have a default column width, the width of the last column will be used instead
        if(!sheet.getColumns().isEmpty()) {
            final Column lastColumn = sheet.getColumns().last();
            final int maxColumn = lastColumn.getMax();
            if(maxColumn<=interval.getMax().getValue()&&maxColumn<sheet.getMaxColCount()-1) {
                lastColumn.setMax(sheet.getMaxColCount()-1);
                final Column column = sheet.getColumn(maxColumn+1, true, true, false);
                column.setVisibility(null);
            }
        }

        // ... splitting up corresponding columns (repeated)..
        sheet.getColumn(interval.getMax().getValue(), true, false, true);
        sheet.getColumn(interval.getMin().getValue(), true, true, false);

        final JSONObject columnAttrs = optAttrs!=null ? optAttrs.optJSONObject(OCKey.COLUMN.value()) : null;
        final NavigableSet<Column> columns = sheet.getColumns().subSet(new Column(interval.getMin().getValue()), true, new Column(interval.getMax().getValue()), true);
        for(Column column:columns) {
            if(s!=null) {
                column.setDefaultCellStyle(s);
            }

            // set the column attributes (attribute family "column")
            if(columnAttrs!=null) {
                column.setStyleName(styleManager.createStyle(StyleFamily.TABLE_COLUMN, column.getStyleName(), true, optAttrs));
                final Object visible = columnAttrs.opt(OCKey.VISIBLE.value());
                if(visible!=null) {
                    if(visible instanceof Boolean) {
                        column.setVisibility(((Boolean)visible).booleanValue()?Visibility.VISIBLE:Visibility.COLLAPSE);
                    }
                    else {
                        column.setVisibility(null);
                    }
                }
            }

            // set the grouping attributes (attribute family "group")
            if (optAttrs!=null) {
                column.changeGroupAttributes(optAttrs);
            }
        }
        // creating max repeated rows ...
        if(s!=null) {
            sheet.getRow(Sheet.getMaxRowCount()-1, true, false, false);
            final Iterator<Row> rowIter = sheet.getRows().iterator();
            while(rowIter.hasNext()) {
                final Row row = rowIter.next();
                // splitting up repeated cells and allow to create a proper iterator over the NavigableSet
                row.getCell(interval.getMax().getValue(), true, false, true);
                row.getCell(interval.getMin().getValue(), true, true, false);
                final NavigableSet<Cell> cellSelection = row.getCells().subSet(new Cell(interval.getMin().getValue()), true, new Cell(interval.getMax().getValue()), true);
                final Iterator<Cell> cellIter = cellSelection.iterator();
                while(cellIter.hasNext()) {
                    cellIter.next().setCellStyle(s);
                }
            }
        }
    }

    public void changeRows(int sheetIndex, List<Interval> intervals, JSONObject optAttrs, String s)
        throws JSONException {

        for(Interval interval:intervals) {
            changeRows(sheetIndex, interval, optAttrs, s);
        }
    }

    public void changeRows(int sheetIndex, Interval interval, JSONObject optAttrs, String s)
        throws JSONException {

        final Sheet sheet = content.getContent().get(sheetIndex);
        // splitting up repeated rows and allow to create a proper iterator over the NavigableSet
        sheet.getRow(interval.getMax().getValue(), true, false, true);
        sheet.getRow(interval.getMin().getValue(), true, true, false);

        final NavigableSet<Row> rows = sheet.getRows().subSet(new Row(interval.getMin().getValue()), true, new Row(interval.getMax().getValue()), true);
        for(Row row:rows) {

            // set the row attributes (attribute family "row")
            final JSONObject rowAttrs = optAttrs!=null ? optAttrs.optJSONObject(OCKey.ROW.value()) : null;
            if(rowAttrs!=null) {
                row.setStyleName(styleManager.createStyle(StyleFamily.TABLE_ROW, row.getStyleName(), true, optAttrs));

                Object visibility = rowAttrs.opt(OCKey.VISIBLE.value());
                Object filtered = rowAttrs.opt(OCKey.FILTERED.value());
                if(visibility!=null||filtered!=null) {
                    if(filtered instanceof Boolean) {
                        if(((Boolean)filtered).booleanValue()) {
                            row.setVisibility(Visibility.FILTER);
                        }
                        else if(visibility instanceof Boolean) {
                            row.setVisibility(((Boolean)visibility).booleanValue() ? Visibility.VISIBLE : Visibility.COLLAPSE);
                        }
                        else {
                            row.setVisibility(Visibility.VISIBLE);
                        }
                    }
                    else if(visibility instanceof Boolean) {
                        if(((Boolean)visibility).booleanValue()) {
                            row.setVisibility(Visibility.VISIBLE);
                        }
                        else {
                            row.setVisibility(Visibility.COLLAPSE);
                        }
                    }
                    else {
                        row.setVisibility(null);
                    }
                }
            }

            // set the grouping attributes (attribute family "group")
            if (optAttrs!=null) {
                row.changeGroupAttributes(optAttrs);
            }
            if(s!=null) {

                row.setDefaultCellStyle(s);

                // creating row entries...
                row.getCell(sheet.getMaxColCount()-1, true, false, false);

                final TreeSet<Cell> cells = row.getCells();
                final Iterator<Cell> cellIter = cells.iterator();

                while(cellIter.hasNext()) {
                    cellIter.next().setCellStyle(s);
                }
            }
        }
    }

    public void changeSheet(int sheetIndex, String sheetName, JSONObject attrs)
        throws JSONException {

        if(attrs!=null) {
            final Sheet sheet = content.getContent().get(sheetIndex);
            final JSONObject sheetProperties = attrs.optJSONObject(OCKey.SHEET.value());
            if(sheetProperties!=null) {
                final ConfigItemMapEntry globalViewSettings = settings.getGlobalViewSettings(true);
                final ConfigItemMapEntry sheetViewSettings = settings.getViewSettings(globalViewSettings, sheet.getName(), true);

                String ranges = null;
                int activeIndex = 0;

                Integer horizontalSplitPosition = null;
                Integer verticalSplitPosition = null;
                String splitMode = null;
                String activePane = null;

                for(Entry<String, Object> sheetPropEntry:sheetProperties.entrySet()) {
                    final Object o = sheetPropEntry.getValue();
                    switch(OCKey.fromValue(sheetPropEntry.getKey())) {
                        case VISIBLE : {
                            final JSONObject a = new JSONObject();
                            final JSONObject t = new JSONObject();
                            a.put(OCKey.TABLE.value(), t);
                            t.put(OCKey.VISIBLE.value(), o);
                            sheet.setStyleName(styleManager.createStyle(StyleFamily.TABLE, sheet.getStyleName(), true, a));
                            break;
                        }
                        case SELECTED_RANGES : {
                            ranges = (String)o;
                            break;
                        }
                        case ACTIVE_INDEX : {
                            activeIndex = (Integer)o;
                            break;
                        }
                        case ZOOM : {
                            sheetViewSettings.addConfigItem("ZoomValue", Double.valueOf(((Number)o).doubleValue() * 100).intValue());
                            break;
                        }
                        case SCROLL_LEFT : {
                            sheetViewSettings.addConfigItem("PositionLeft", (Integer)o);
                            break;
                        }
                        case SCROLL_TOP : {
                            sheetViewSettings.addConfigItem("PositionTop", (Integer)o);
                            break;
                        }
                        case SCROLL_BOTTOM : {
                            sheetViewSettings.addConfigItem("PositionBottom", (Integer)o);
                            break;
                        }
                        case SCROLL_RIGHT : {
                            sheetViewSettings.addConfigItem("PositionRight", (Integer)o);
                            break;
                        }
                        case SHOW_GRID : {
                            sheetViewSettings.addConfigItem("ShowGrid", (Boolean)o);
                            break;
                        }
                        case SPLIT_MODE : {
                            splitMode = (String)o;
                            break;
                        }
                        case SPLIT_WIDTH : {
                            horizontalSplitPosition = (Integer)o;
                            break;
                        }
                        case SPLIT_HEIGHT : {
                            verticalSplitPosition = (Integer)o;
                            break;
                        }
                        case LOCKED : {
                            if(o instanceof Boolean) {
                                sheet.setAttributeNS(Namespaces.TABLE, "table:protected", ((Boolean)o).toString());
                            }
                            else if(o==JSONObject.NULL) {
                                sheet.removeAttributeNS(Namespaces.TABLE, "protected");
                            }
                            break;
                        }
                        case ACTIVE_PANE : {
                            if(o instanceof String) {
                                activePane = (String)o;
                            }
                            break;
                        }
                    }
                }
                if(ranges!=null) {
                    String[] r = ranges.split(" ", -1);
                    if(r.length>=(activeIndex+1)) {
                        CellRef ref = CellRef.createCellRef(r[activeIndex]);
                        sheetViewSettings.addConfigItem("CursorPositionX", ref.getColumn());
                        sheetViewSettings.addConfigItem("CursorPositionY", ref.getRow());
                    }
                }

                short verticalSplitMode = Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "VerticalSplitMode", 0).shortValue();
                short horizontalSplitMode = Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "HorizontalSplitMode", 0).shortValue();

                if(horizontalSplitPosition!=null||verticalSplitPosition!=null||splitMode!=null) {
                    if(splitMode==null) {
                        splitMode = verticalSplitMode==2||horizontalSplitMode==2 ? "frozenSplit" : "split";
                    }
                    if("frozenSplit".equals(splitMode)||"frozen".equals(splitMode)) {
                        if(verticalSplitPosition!=null) {
                            verticalSplitMode = (short) (verticalSplitPosition==0 ? 0 : 2);
                        }
                        if(horizontalSplitPosition!=null) {
                            horizontalSplitMode = (short) (horizontalSplitPosition==0 ? 0 : 2);
                        }
                    }
                    else {
                        if(horizontalSplitPosition!=null) {
                            horizontalSplitMode = (short) (horizontalSplitPosition==0 ? 0 : 1);
                        }
                        if(verticalSplitPosition!=null) {
                            verticalSplitMode = (short) (verticalSplitPosition==0 ? 0 : 1);
                        }
                    }
                    sheetViewSettings.addConfigItem("HorizontalSplitMode", horizontalSplitMode);
                    sheetViewSettings.addConfigItem("VerticalSplitMode", verticalSplitMode);

                    if(horizontalSplitPosition!=null) {
                        if(horizontalSplitMode==1) {
                            sheetViewSettings.addConfigItem("HorizontalSplitPosition", (horizontalSplitPosition * 96) / 2540);
                        }
                        else if(horizontalSplitMode==2) {
                            sheetViewSettings.addConfigItem("HorizontalSplitPosition", horizontalSplitPosition + Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "PositionLeft", 0));
                        }
                    }
                    if(verticalSplitPosition!=null) {
                        if(verticalSplitMode==1) {
                            sheetViewSettings.addConfigItem("VerticalSplitPosition", (verticalSplitPosition * 96) / 2540);
                        }
                        else if(verticalSplitMode==2) {
                            sheetViewSettings.addConfigItem("VerticalSplitPosition", verticalSplitPosition + Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "PositionTop", 0));
                        }
                    }
                }

                short currentPane = Integer.valueOf(Settings.getConfigValueIntDefault(globalViewSettings, sheetViewSettings, "ActiveSplitRange", 2)).shortValue();

                if("topLeft".equals(activePane)) {
                    currentPane = verticalSplitMode!=0 ? (short)0 : (short)2;
                }
                else if("topRight".equals(activePane)) {
                    if(verticalSplitMode!=0) {
                        currentPane&=~2;
                    }
                    if(horizontalSplitMode!=0) {
                        currentPane|=1;
                    }
                }
                else if("bottomLeft".equals(activePane)) {
                    currentPane = 2;
                }
                else if("bottomRight".equals(activePane)) {
                    currentPane = horizontalSplitMode!=0 ? (short)3 : (short)2;
                }
                sheetViewSettings.addConfigItem("ActiveSplitRange", currentPane);
            }
            if(attrs.optJSONObject(OCKey.COLUMN.value())!=null) {
                changeColumns(sheetIndex, new Interval(new ColumnRef(0), new ColumnRef(sheet.getMaxColCount()-1)), attrs, null);
            }
            if(attrs.optJSONObject(OCKey.ROW.value())!=null) {
                changeRows(sheetIndex, new Interval(new RowRef(0), new RowRef(sheet.getMaxRowCount()-1)), attrs, null);
            }
        }
        if(sheetName!=null) {
            setSheetName(sheetIndex, sheetName);
        }
    }

    public void setAttributes(JSONObject attrs, String target, JSONArray start, JSONArray end) throws Exception {

        if(attrs==null) {
            return;
        }
        int startIndex = start.getInt(start.length()-1);
        int endIndex = startIndex;

        if(end!=null) {
            if(end.length()!=start.length())
                return;
            endIndex = end.getInt(end.length()-1);
        }
        IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, target).getComponent(start, start.length());
        component.splitStart(startIndex, SplitMode.DELETE);
        while(component!=null&&component.getComponentNumber()<=endIndex) {
            if(component.getNextComponentNumber()>=endIndex+1) {
                component.splitEnd(endIndex, SplitMode.DELETE);
            }
            component.applyAttrsFromJSON(attrs);
            component = component.getNextComponent();
        }
    }

    public void insertParagraph(JSONArray start, String target, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.PARAGRAPH);
    }

    public void splitParagraph(JSONArray start, String target)
        throws JSONException {

        ((IParagraph)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1))
            .splitParagraph(start.getInt(start.length()-1));
    }

    public void mergeParagraph(JSONArray start, String target) {

        ((IParagraph)content.getRootComponent(opsDoc, target).getComponent(start, start.length()))
            .mergeParagraph();
    }

    public void move(JSONArray start, String target, JSONArray end, JSONArray to)
        throws JSONException {

        OdfComponent.move(content.getRootComponent(opsDoc, target), start, end, to);
    }

    public void insertText(JSONArray start, String target, JSONObject attrs, String text) throws Exception {

        if(text.length()>0) {
            final IComponent<OdfOperationDoc> component = content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1);
            ((IParagraph)component).insertText(start.getInt(start.length()-1), text, attrs);
        }
    }

    public void insertTab(JSONArray start, String target, JSONObject attrs) throws Exception {

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.TAB);
    }

    public void insertHardBreak(JSONArray start, String target, JSONObject attrs) throws Exception{

        content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.HARDBREAK);
    }

    public void insertField(JSONArray start, String target, String type, String representation, JSONObject attrs) throws Exception {

        final TextField textField = (TextField)content.getRootComponent(opsDoc, target).getComponent(start, start.length()-1)
            .insertChildComponent(start.getInt(start.length()-1), attrs, ComponentType.FIELD).getObject();

        textField.setType(type);
        textField.setRepresentation(representation);
    }

    public void updateField(JSONArray start, String target, String type, String representation)
        throws UnsupportedOperationException {

        final TextFieldComponent tfComponent = (TextFieldComponent)content.getRootComponent(opsDoc, target).getComponent(start, start.length());
        if(type!=null) {
            ((TextField)tfComponent.getObject()).setType(type);
        }
        if(representation!=null) {
            ((TextField)tfComponent.getObject()).setRepresentation(representation);
        }
    }

    public void insertNote(int sheetIndex, CellRef anchor, String text, String author, String uid, String date, JSONObject attrs) throws JSONException {
        final Annotation annotation = new Annotation("", true);
        content.getContent().get(sheetIndex).getRow(anchor.getRow(), true, true, true).getCell(anchor.getColumn(), true, true, true).getContent(true).add(annotation);
        if(author!=null) {
            annotation.setCreator(author);
        }
        if(date!=null) {
            annotation.setDate(date);
        }
        TextContentHelper.setSimpleText(annotation, text);
        if(attrs!=null) {
            annotation.applyAttrsFromJSON(opsDoc, attrs, true);
        }
    }

    public void changeNote(int sheetIndex, CellRef anchor, String text, JSONObject attrs) throws JSONException {
        final Annotation annotation = content.getContent().get(sheetIndex).getRow(anchor.getRow(), false, false, false).getCell(anchor.getColumn(), false, false, false).getAnnotation();
        if(text!=null) {
            TextContentHelper.setSimpleText(annotation, text);
        }
        if(attrs!=null) {
            annotation.applyAttrsFromJSON(opsDoc, attrs, true);
        }
    }

    public Annotation deleteNote(int sheetIndex, CellRef anchor) throws JSONException {
        // the cell has to be available, so we don't check anything. the crash is intended if no annotation
        return content.getContent().get(sheetIndex).getRow(anchor.getRow(), false, false, false).getCell(anchor.getColumn(), false, false, false).deleteAnnotation();
    }

    public void moveNotes(int sheetIndex, String[] from, String[] to) throws JSONException {
        final Annotation[] anos = new Annotation[from.length];
        for(int a=0; a < from.length; a++)  {
            anos[a] = deleteNote(sheetIndex, CellRef.createCellRef(from[a]));
        }
        for(int a=0; a < from.length; a++) {
            final CellRef cellRef = CellRef.createCellRef(to[a]);
            content.getContent().get(sheetIndex).getRow(cellRef.getRow(), true, true, true).getCell(cellRef.getColumn(), true, true, true).getContent(true).add(anos[a]);
        }
    }
}
