/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.List;

import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class ConditionalFormat {

	private Ranges ranges;
	private List<Condition> conditions = new ArrayList<Condition>();

	public ConditionalFormat(Ranges ranges) {
		this.ranges = ranges;
	}

	public ConditionalFormat(SpreadsheetContent content, Attributes attributes) {
		ranges = new Ranges(content, attributes.getValue("calcext:target-range-address"));
	}

	public List<Condition> getConditions() {
		return conditions;
	}

	public Ranges getRanges() {
		return ranges;
	}

	public void setRanges(Ranges ranges) {
		this.ranges = ranges;
	}

	public void writeObject(SerializationHandler output, SpreadsheetContent content)
		throws SAXException {

		SaxContextHandler.startElement(output, Namespaces.CALCEXT, "conditional-format", "calcext:conditional-format");
		if(ranges!=null) {
			SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "target-range-address","calcext:target-range-address", ranges.convertToString(content, true));
		}
		for(Condition condition:conditions) {
			condition.writeObject(output);
		}
		SaxContextHandler.endElement(output, Namespaces.CALCEXT, "conditional-format", "calcext:conditional-format");
	}
}
