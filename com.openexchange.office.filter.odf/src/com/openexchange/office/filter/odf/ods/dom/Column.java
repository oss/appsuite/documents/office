/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.odf.Length;
import com.openexchange.office.filter.odf.Length.Unit;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.properties.TableColumnProperties;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleTableColumn;

public class Column extends AbstractColRow implements Comparable<Column> {

    private int min;
    private int max;

    Column(int colMin, int colMax) {
        super("table-column-group");
        this.min = colMin;
        this.max = colMax;
    }

    Column(int col) {
        this(col, col);
    }

    /**
     * Returns a clone of this instance, spanning over all columns preceding the specified column index.
     * This instance will be updated so that it starts at the specified column index.
     */
    public Column splitBefore(int col) {

        if (col <= min || col > max) { return null; }

        // create a clone of this column
        final Column clone = new Column(min, col - 1);
        clone.setStyleName(getStyleName());
        clone.setDefaultCellStyle(getDefaultCellStyle());
        clone.setVisibility(getVisibility());
        clone.setGroupLevel(getGroupLevel());
        clone.setGroupCollapse(getGroupCollapse());

        // update the settings of this column
        min = col;

        return clone;
    }

    @Override
    public int getMin() {
        return min;
    }

    public void setMin(int value) {
        this.min = value;
    }

    @Override
    public int getMax() {
        return max;
    }

    public void setMax(int value) {
        this.max = value;
    }

    public void writeObject(SerializationHandler output)
        throws SAXException {

        SaxContextHandler.startElement(output, Namespaces.TABLE, "table-column", "table:table-column");
        if(max-min>0) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "number-columns-repeated", "table:number-columns-repeated", Integer.toString(((max-min)+1)));
        }
        if(getStyleName()!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "style-name", "table:style-name", getStyleName());
        }
        if(getDefaultCellStyle()!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "default-cell-style-name", "table:default-cell-style-name", getDefaultCellStyle());
        }
        if(getVisibility()!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "visibility", "table:visibility", getVisibility().toString());
        }
        if(getId()!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "id", "table:id", getId());
        }
        SaxContextHandler.endElement(output, Namespaces.TABLE, "table-column", "table:table-column");
    }

    public void createAttributes(JSONObject attrs, OdfSpreadsheetDocument doc, boolean sheetDefaultProperties)
        throws JSONException {

        createGroupAttributes(attrs);

        final JSONObject columnProperties = new JSONObject(2);
        if(!sheetDefaultProperties&&getVisibility()!=null&&getVisibility()==Visibility.COLLAPSE) {
            columnProperties.put(OCKey.VISIBLE.value(), false);
        }
        if(hasStyleName()) {
            final StyleManager styleManager = doc.getStyleManager();
            final StyleBase styleBase = styleManager.getStyle(getStyleName(), StyleFamily.TABLE_COLUMN, true);
            if(styleBase instanceof StyleTableColumn) {
                final TableColumnProperties tableColumnProperties = ((StyleTableColumn)styleBase).getTableColumnProperties();
                final String width = tableColumnProperties.getAttribute("style:column-width");
                if(width!=null&&!width.isEmpty()) {
                    final Double dest = Length.getLength(width, Unit.MILLIMETER);
                    if(dest!=null) {
                        columnProperties.put(OCKey.WIDTH.value(), Math.round(dest*100));
                    }
                }
                final String customWidth = tableColumnProperties.getAttribute("style:use-optimal-column-width");
                if(customWidth!=null&&!customWidth.isEmpty()) {
                    columnProperties.put(OCKey.CUSTOM_WIDTH.value(), !Boolean.parseBoolean(customWidth));
                }
            }
        }
        if(!columnProperties.isEmpty()) {
            attrs.put(OCKey.COLUMN.value(), columnProperties);
        }
    }

    class ColumnAttrHash extends AttrsHash<Column> {

        public ColumnAttrHash(Column val) {
            super(val, val.getMax()-val.getMin()+1);
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((getStyleName() == null) ? 0 : getStyleName().hashCode());
            result = prime * result + ((getDefaultCellStyle() == null) ? 0 : getDefaultCellStyle().hashCode());
            result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
            result = prime * result + ((getVisibility() == null) ? 0 : getVisibility().hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {

            if (this == obj) { return true; }
            if ((obj == null) || (getClass() != obj.getClass())) { return false; }

            ColumnAttrHash other = (ColumnAttrHash) obj;
            return equalCommonAttrs(other);
        }
    }

    public ColumnAttrHash createAttrHash() {
        return new ColumnAttrHash(this);
    }

    @Override
    public int compareTo(Column o) {
        return min - o.getMin();
    }
}
