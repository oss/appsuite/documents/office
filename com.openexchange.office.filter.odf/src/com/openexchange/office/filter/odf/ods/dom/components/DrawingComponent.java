/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.ods.dom.components;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.draw.ConnectorShape;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawImage;
import com.openexchange.office.filter.odf.draw.DrawTextBox;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.draw.GroupShape;
import com.openexchange.office.filter.odf.draw.IDrawingType;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.ods.dom.Drawing;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;

public class DrawingComponent extends OdfComponent implements IDrawingType {

	final Drawing drawing;

	public DrawingComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> shapeNode, int componentNumber) {
		super(parentContext, shapeNode, componentNumber);
		drawing = (Drawing)getObject();
	}

    @Override
    public String simpleName() {
        return "Drawing";
    }

    public Shape getShape() {
		return drawing.getShape();
	}

	@Override
    public DrawingType getType() {
		return drawing.getType();
	}

	@Override
	public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {
        final int nextComponentNumber = previousChildComponent != null ? previousChildComponent.getNextComponentNumber() : 0;
        DLNode<Object> nextNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : drawing.getShape().getContent().getFirstNode();
        while(nextNode!=null) {
            final Object child = nextNode.getData();
            if(child instanceof GroupShape) {
                return new ShapeGroupComponent(this, nextNode, nextComponentNumber);
            }
            else if(child instanceof DrawFrame) {
                return new FrameComponent(this, nextNode, nextComponentNumber);
            }
            else if(child instanceof ConnectorShape) {
                return new ShapeConnectorComponent(this, nextNode, nextComponentNumber);
            }
            else if(child instanceof Shape) {
                return new ShapeComponent(this, nextNode, nextComponentNumber);
            }
            else if(child instanceof Paragraph) {
                return new ParagraphComponent(this, nextNode, nextComponentNumber);
            }
            nextNode = nextNode.getNext();
        }
        return null;
	}

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> contextNode, int number, IComponent<OdfOperationDoc> child, ComponentType type, JSONObject attrs) {
    	DLList<Object> DLList = drawing.getShape().getContent();
        DLNode<Object> referenceNode = child != null && child.getComponentNumber()== number ? child.getNode() : null;

        switch(type) {
            case AC_CONNECTOR:
            case AC_SHAPE: {
                final DLNode<Object> newShapeNode = new DLNode<Object>(Shape.createShape(operationDocument, attrs, drawing.getShape() instanceof GroupShape ? (GroupShape)drawing.getShape() : null, false, isContentAutoStyle()));
                DLList.addNode(referenceNode, newShapeNode, true);
                return new ShapeComponent(parentContext, newShapeNode, number);
            }
            case AC_GROUP: {
                final DLNode<Object> newGroupNode = new DLNode<Object>(new GroupShape(operationDocument, drawing.getShape() instanceof GroupShape ? (GroupShape)drawing.getShape() : null, false, isContentAutoStyle()));
                DLList.addNode(referenceNode, newGroupNode, true);
                return new ShapeGroupComponent(parentContext, newGroupNode, number);
            }
            case AC_IMAGE: {
                final DrawFrame drawFrame = new DrawFrame(operationDocument, drawing.getShape() instanceof GroupShape ? (GroupShape)drawing.getShape() : null, false, isContentAutoStyle());
                final DLNode<Object> drawFrameNode = new DLNode<Object>(drawFrame);
                DLList.addNode(referenceNode, new DLNode<Object>(drawFrame), true);
                final DrawImage drawImage = new DrawImage(drawFrame);
                drawFrame.addContent(drawImage);
                return new FrameComponent(parentContext, drawFrameNode, number);
            }
            case AC_FRAME: {
                final DrawFrame drawFrame = new DrawFrame(operationDocument, drawing.getShape() instanceof GroupShape ? (GroupShape)drawing.getShape() : null, false, isContentAutoStyle());
                final DLNode<Object> drawFrameNode = new DLNode<Object>(drawFrame);
                DLList.addNode(referenceNode, new DLNode<Object>(drawFrame), true);
                final DrawTextBox drawTextBox = new DrawTextBox(drawFrame);
                drawFrame.addContent(drawTextBox);
                return new FrameComponent(parentContext, drawFrameNode, number);
            }
            case PARAGRAPH : {
                // if this is a normal shape, then paragraphs can be inserted
                final DLNode<Object> newParagraphNode = new DLNode<Object>(new Paragraph(null));
                DLList.addNode(referenceNode, newParagraphNode, true);
                return new ParagraphComponent(parentContext, newParagraphNode, number);
            }
            case AC_CHART:
                DrawFrame drawFrame = getChart(operationDocument, isContentAutoStyle());

                final DLNode<Object> drawFrameNode = new DLNode<Object>(drawFrame);
                DLList.addNode(referenceNode, new DLNode<Object>(drawFrame), true);
                return new FrameComponent(parentContext, drawFrameNode, number);

            default : {
                throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(int number, JSONObject attrs, ComponentType type) throws Exception {

        final IComponent<OdfOperationDoc> c = insertChildComponent(this, getNode(), number, getChildComponent(number), type, attrs);
        if(attrs!=null) {
            c.applyAttrsFromJSON(attrs);
        }
        return c;
    }

	@Override
	public void applyAttrsFromJSON(JSONObject attrs)
			throws JSONException {

	    drawing.applyAttrsFromJSON(operationDocument, attrs, contentAutoStyle);
	}

	@Override
	public void createJSONAttrs(OpAttrs attrs) {

	    drawing.createAttrs(operationDocument, attrs, contentAutoStyle);
	}
}
