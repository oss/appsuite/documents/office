/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;

public class ContentValidationHandler extends SaxContextHandler {

	final private ContentValidation contentValidation;
	final private ContentValidations contentValidations;

	public ContentValidationHandler(SaxContextHandler parentContextHandler, ContentValidations contentValidations, ContentValidation contentValidation) {
		super(parentContextHandler);

		this.contentValidation = contentValidation;
		this.contentValidations = contentValidations;
	}

	@Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
	    if(qName.equals("table:help-message")) {
	        final HelpMessage helpMessage = new HelpMessage(attributes);
	        contentValidation.setHelpMessage(helpMessage);
	        return new HelpMessageHandler(this, helpMessage);
	    }
	    else if(qName.equals("table:error-message")) {
	        final ErrorMessage errorMessage = new ErrorMessage(attributes);
	        contentValidation.setErrorMessage(errorMessage);
	        return new ErrorMessageHandler(this, errorMessage);
	    }
		final ElementNS child = new ElementNS(getFileDom(), attributes, uri, qName);
		contentValidation.getContent().add(child);
		return new UnknownContentHandler(this, child);
	}

    @Override
    public void endContext(String qName, String characters) {
        super.endContext(qName, characters);

        if(qName.equals("table:content-validation")) {
            if(contentValidation.getName()!=null&&!contentValidation.getName().isEmpty()) {
                contentValidations.addValidation(contentValidation, false);
            }
        }
    }
}
