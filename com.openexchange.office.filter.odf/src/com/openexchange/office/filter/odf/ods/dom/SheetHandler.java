/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;
import org.apache.xerces.dom.ElementNSImpl;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;

public class SheetHandler extends SaxContextHandler {

    final private Sheet sheet;

    private GroupStack colGroupStack = new GroupStack();
    private GroupStack rowGroupStack = new GroupStack();

    public SheetHandler(SpreadsheetContentHandler parentContextHandler, Sheet sheet) {
        super(parentContextHandler);

        this.sheet = sheet;
    }

    public Sheet getSheet() {
        return sheet;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        if(localName.equals("table-column")) {
            final TreeSet<Column> columns = sheet.getColumns();
            final int nextCol = columns.isEmpty() ? 0 : columns.last().getMax() + 1;
            String columnStyle = null;
            for(int i=0; i<attributes.getLength(); i++) {
                if(attributes.getLocalName(i).equals("style-name")) {
                    columnStyle = attributes.getValue(i);
                    break;
                }
            }
            final Column column = new Column(nextCol);
            column.setStyleName(columnStyle);
            column.setGroupLevel(colGroupStack.getLevel());
            column.setGroupCollapse(colGroupStack.pullCollapse());

            if(columns.isEmpty()) {
                // adding column placeholder only once
                sheet.getChilds().add(column);
                sheet.columnPlaceholderAvailable = true;
            }
            columns.add(column);
            return new ColumnHandler(this, attributes, column);
        }
        else if(localName.equals("table-row")) {
            final TreeSet<Row> rows = sheet.getRows();
            int currentRow = 0;
            if(!rows.isEmpty()) {
                final Row lastRow = rows.last();
                currentRow = lastRow.getMax()+1;
            }
            String rowStyle = null;
            for(int i=0; i<attributes.getLength(); i++) {
                if(attributes.getLocalName(i).equals("style-name")) {
                    rowStyle = attributes.getValue(i);
                    break;
                }
            }
            final Row row = new Row(currentRow, 1);
            row.setStyleName(rowStyle);
            row.setGroupLevel(rowGroupStack.getLevel());
            row.setGroupCollapse(rowGroupStack.pullCollapse());

            // for the first row we are inserting a placeholder node, at this point
            // the all rows are saved in ascending order
            if(rows.isEmpty()) {
                sheet.getChilds().add(row);
                sheet.rowPlaceholderAvailable = true;
            }
            rows.add(row);
            return new RowHandler(this, attributes, row);
        }
        else if(localName.equals("table-header-rows")) {
            return this;
        }
        else if(localName.equals("table-column-group")) {
            final AttributesImpl attrsImpl = new AttributesImpl(attributes);
            final boolean collapsed = !attrsImpl.getBooleanValue("table:display", true);
            colGroupStack.pushLevel(collapsed);
        }
        else if(localName.equals("table-row-group")) {
            final AttributesImpl attrsImpl = new AttributesImpl(attributes);
            final boolean collapsed = !attrsImpl.getBooleanValue("table:display", true);
            rowGroupStack.pushLevel(collapsed);
        }
        else if(localName.equals("shapes")&&uri.equals(Namespaces.TABLE)) {
            return new ShapesHandler(this, sheet);
        }
        else if(localName.equals("forms")&&uri.equals(Namespaces.OFFICE)) {
            final ElementNSImpl officeForms = new ElementNS(getFileDom(), attributes, uri, qName);
            sheet.setOfficeForms(officeForms);
            return new UnknownContentHandler(this, officeForms);
        }
        else if(qName.equals("table:named-expressions")) {
            return new NamedExpressionsHandler(this, sheet.getNamedExpressions(true));
        }
        else if(qName.equals("calcext:conditional-formats")) {
            return new ConditionalFormatsHandler(this, sheet.getConditionalFormats(true));
        }
        else {
            if(qName.equals("table:table-source")) {
                sheet.setIsLinkedTable(true);
            }
            final ElementNSImpl element = new ElementNS(getFileDom(), attributes, uri, qName);
            sheet.getChilds().add(element);
            return new UnknownContentHandler(this, element);
        }
        return this;
    }

    @Override
    public void endElement(String localName, String qName) {
        if(localName.equals("table-column-group")) {
            colGroupStack.popLevel();
        }
        else if(localName.equals("table-row-group")) {
            rowGroupStack.popLevel();
        }
    }

    @Override
    public void endContext(String qName, String characters) {
        super.endContext(qName, characters);

        if(qName.equals("table:table")) {
            final TreeSet<Row> rows = sheet.getRows();
            int emptyRows = Sheet.getMaxRowCount();
            if(!rows.isEmpty()) {
                emptyRows -= rows.last().getMax()+1;
            }
            if(emptyRows>0) {
                final Row emptyRow = new Row(1048576 - emptyRows, emptyRows);
                emptyRow.setDefaultCellStyle("Default");
                rows.add(emptyRow);
                final Cell emptyCell = new Cell(0);
                emptyCell.setRepeated(sheet.getMaxColCount());
                emptyRow.getCells().add(emptyCell);
            }
            prepareDrawingsForOps();
        }
    }

    private void prepareDrawingsForOps() {
        Collections.sort(sheet.getDrawings().getIndexedDrawings(), new Comparator<Drawing>() {

            @Override
            public int compare(Drawing d0, Drawing d1) {

                long zIndex0 = -1;
                long zIndex1 = -1;

                String attrZIndex0 = d0.getShape().getAttributes().getValue("draw:z-index");
                if (attrZIndex0 != null) {
                    try {
                        zIndex0 = Long.parseLong(attrZIndex0);
                    } catch (NumberFormatException e) {
                    }
                }
                String attrZIndex1 = d1.getShape().getAttributes().getValue("draw:z-index");
                if (attrZIndex1 != null) {
                    try {
                        zIndex1 = Long.parseLong(attrZIndex1);
                    } catch (NumberFormatException e) {
                    }
                }
                return Long.compare(zIndex0, zIndex1);
            }
        });
    }
}
