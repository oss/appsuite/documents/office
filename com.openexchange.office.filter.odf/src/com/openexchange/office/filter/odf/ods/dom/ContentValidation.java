/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.Set;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.IElementWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.SmlUtils;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.odt.dom.TextContentHelper;

public class ContentValidation implements IElementWriter, INodeAccessor, Cloneable {

	private String name;
	private String baseCellAddress;
	private String condition;
	private Boolean allowEmptyCell;

	// "none", "sorted" or "sort-ascending", the default is unsorted...
	private String displayList;
	private HelpMessage helpMessage;
    private ErrorMessage errorMessage;
	private DLList<Object> content;

	public ContentValidation(Attributes attributes) {
		name = attributes.getValue("table:name");
		baseCellAddress = attributes.getValue("table:base-cell-address");
		condition = attributes.getValue("table:condition");
        displayList = attributes.getValue("table:display-list");
		final String _allowEmptyCell = attributes.getValue("table:allow-empty-cell");
		if(_allowEmptyCell!=null) {
		    allowEmptyCell = Boolean.parseBoolean(_allowEmptyCell);
		}
	}

	public ContentValidation() {
	    allowEmptyCell = Boolean.TRUE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCondition() {
	    return condition;
	}

	public void setCondition(String condition) {
	    this.condition = condition;
	}

	public String getBaseCellAddress() {
	    return baseCellAddress;
	}

	public void setBaseCellAddress(String baseCellAddress) {
	    this.baseCellAddress = baseCellAddress;
	}

	public Boolean getAllowEmptyCell() {
	    return allowEmptyCell;
	}

	public String getDisplayList() {
	    return displayList;
	}

	public void setDisplayList(String displayList) {
	    this.displayList = displayList;
	}

	public void setAllowEmptyCell(Boolean allowEmptyCell) {
	    this.allowEmptyCell = allowEmptyCell;
	}

	public HelpMessage getHelpMessage(boolean forceCreate) {
	    if(helpMessage==null&&forceCreate) {
	        helpMessage = new HelpMessage();
	    }
	    return helpMessage;
	}

	public void setHelpMessage(HelpMessage helpMessage) {
	    this.helpMessage = helpMessage;
	}

	public ErrorMessage getErrorMessage(boolean forceCreate) {
	    if(errorMessage==null&&forceCreate) {
	        errorMessage = new ErrorMessage();
	    }
	    return errorMessage;
	}

	public void setErrorMessage(ErrorMessage errorMessage) {
	    this.errorMessage = errorMessage;
	}

    @Override
    public DLList<Object> getContent() {
        if (content == null) {
            content = new DLList<Object>();
        }
        return content;
    }

	@Override
    public void writeObject(SerializationHandler output)
		throws SAXException {

        SaxContextHandler.startElement(output, Namespaces.TABLE, "content-validation", "table:content-validation");
        SaxContextHandler.addAttribute(output, Namespaces.TABLE, "name", "table:name", name);
        if(baseCellAddress!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "base-cell-address", "table:base-cell-address", baseCellAddress);
        }
        if(condition!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "condition", "table:condition", condition);
        }
        if(displayList!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "display-list", "table:display-list", displayList);
        }
        if(allowEmptyCell!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "allow-empty-cell", "table:allow-empty-cell", allowEmptyCell.toString());
        }
        if(helpMessage!=null) {
            helpMessage.writeObject(output);
        }
        if(errorMessage!=null) {
            errorMessage.writeObject(output);
        }
	    if(content!=null) {
	        TextContentHelper.write(output, content);
	    }
        SaxContextHandler.endElement(output, Namespaces.TABLE, "content-validation", "table:content-validation");
	}

    @Override
    protected ContentValidation clone() {
        try {
            final ContentValidation clone = (ContentValidation)super.clone();
            if(helpMessage!=null) {
                clone.setHelpMessage(helpMessage.clone());
            }
            if(errorMessage!=null) {
                clone.setErrorMessage(errorMessage.clone());
            }
            TextContentHelper.setSimpleText(clone, TextContentHelper.getSimpleText(this));
            return clone;
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((allowEmptyCell == null) ? 0 : allowEmptyCell.hashCode());
        result = prime * result + ((baseCellAddress == null) ? 0 : baseCellAddress.hashCode());
        result = prime * result + ((condition == null) ? 0 : condition.hashCode());
        final Iterator<Object> iter = getContent().iterator();
        while(iter.hasNext()) {
            result = prime * result + iter.next().hashCode();
        }
        result = prime * result + ((displayList == null) ? 0 : displayList.hashCode());
        result = prime * result + ((errorMessage == null) ? 0 : errorMessage.hashCode());
        result = prime * result + ((helpMessage == null) ? 0 : helpMessage.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ContentValidation other = (ContentValidation) obj;
        if (allowEmptyCell == null) {
            if (other.allowEmptyCell != null)
                return false;
        } else if (!allowEmptyCell.equals(other.allowEmptyCell))
            return false;
        if (baseCellAddress == null) {
            if (other.baseCellAddress != null)
                return false;
        } else if (!baseCellAddress.equals(other.baseCellAddress))
            return false;
        if (condition == null) {
            if (other.condition != null)
                return false;
        } else if (!condition.equals(other.condition))
            return false;

        final DLList<Object> c = getContent();
        final DLList<Object> oc = other.getContent();

        if(c.isEmpty()) {
            if(!oc.isEmpty()) {
                return false;
            }
        }
        else {
            if(oc.isEmpty()) {
                return false;
            }
            DLNode<Object> node = c.getFirstNode();
            DLNode<Object> otherNode = oc.getFirstNode();
            while(node!=null&&otherNode!=null) {
                if(!node.getData().equals(otherNode.getData())) {
                    return false;
                }
                node = node.getNext();
                otherNode = otherNode.getNext();
            }
            if(node!=null||otherNode!=null) {
                return false;
            }
        }
        if (displayList == null) {
            if (other.displayList != null)
                return false;
        } else if (!displayList.equals(other.displayList))
            return false;
        if (errorMessage == null) {
            if (other.errorMessage != null)
                return false;
        } else if (!errorMessage.equals(other.errorMessage))
            return false;
        if (helpMessage == null) {
            if (other.helpMessage != null)
                return false;
        } else if (!helpMessage.equals(other.helpMessage))
            return false;
        return true;
    }

    final static String CONDITION_contentTextLength = "cell-content-text-length()";
    final static String CONDITION_cellContentTextLengthIsBetween = "cell-content-text-length-is-between";
    final static String CONDITION_cellContentTextLengthIsNotBetween = "cell-content-text-length-is-not-between";
    final static String CONDITION_cellContentIsInList = "cell-content-is-in-list(";
    final static String CONDITION_cellContentIsDecimalNumber_and = "cell-content-is-decimal-number() and ";
    final static String CONDITION_cellContentIsWholeNumber_and = "cell-content-is-whole-number() and ";
    final static String CONDITION_cellContentIsDate_and = "cell-content-is-date() and ";
    final static String CONDITION_cellContentIsTime_and = "cell-content-is-time() and ";
    final static String CONDITION_isTrueFormula = "is-true-formula(";
    final static String CONDITION_cellContent = "cell-content()";
    final static String CONDITION_cellContentIsBetween = "cell-content-is-between";
    final static String CONDITION_cellContentIsNotBetween = "cell-content-is-not-between";

    public static void createContentValidation(SpreadsheetContent content, JSONArray operationQueue, int sheetIndex, String name, List<CellRefRange> ranges)
        throws JSONException {

        final JSONObject insertValidationObject = new JSONObject();
        insertValidationObject.put(OCKey.NAME.value(), OCValue.INSERT_DV_RULE.value());
        insertValidationObject.put(OCKey.SHEET.value(), sheetIndex);

        final StringBuffer rangesBuffer = new StringBuffer();
        for(CellRefRange cellRefRange:ranges) {
            if(rangesBuffer.length()!=0) {
                rangesBuffer.append(" ");
            }
            rangesBuffer.append(CellRefRange.getCellRefRange(cellRefRange));
        }
        insertValidationObject.put(OCKey.RANGES.value(), rangesBuffer.toString());
        final ContentValidation contentValidation = content.getContentValidations(true).getContentValidationByName(name);
        if(contentValidation.getBaseCellAddress()!=null) {
            insertValidationObject.put(OCKey.REF.value(), "[" + contentValidation.getBaseCellAddress() + "]");
        }
        if(contentValidation.getAllowEmptyCell()!=null) {
            insertValidationObject.put(OCKey.IGNORE_EMPTY.value(), contentValidation.getAllowEmptyCell());
        }
        if(contentValidation.getDisplayList()!=null) {
            insertValidationObject.put(OCKey.SHOW_DROP_DOWN.value(), !"none".equals(contentValidation.getDisplayList()));
        }
        if(contentValidation.getErrorMessage(false)!=null) {
            final ErrorMessage errorMessage = contentValidation.getErrorMessage(true);
            final Boolean display = errorMessage.getDisplay();
            if(display!=null&&display.booleanValue()==false) {
                insertValidationObject.put(OCKey.SHOW_ERROR.value(), false);
            }
            if(errorMessage.getTitle()!=null) {
                insertValidationObject.put(OCKey.ERROR_TITLE.value(), errorMessage.getTitle());
            }
            insertValidationObject.put(OCKey.ERROR_TEXT.value(), TextContentHelper.getSimpleText(errorMessage));
            final String messageType = errorMessage.getMessageType();
            if("warning".equals(messageType)) {
                insertValidationObject.put(OCKey.ERROR_TYPE.value(), "warning");
            }
            else if("information".equals(messageType)) {
                insertValidationObject.put(OCKey.ERROR_TYPE.value(), "info");
            }
        }
        if(contentValidation.getHelpMessage(false)!=null) {
            final HelpMessage helpMessage = contentValidation.getHelpMessage(true);
            final Boolean display = helpMessage.getDisplay();
            if(display!=null&&display.booleanValue()==false) {
                insertValidationObject.put(OCKey.SHOW_INFO.value(), false);
            }
            if(helpMessage.getTitle()!=null) {
                insertValidationObject.put(OCKey.INFO_TITLE.value(), helpMessage.getTitle());
            }
            insertValidationObject.put(OCKey.INFO_TEXT.value(), TextContentHelper.getSimpleText(helpMessage));
        }
        final ConditionToParameters parameters = new ConditionToParameters(contentValidation.getCondition());
        if(parameters.isValid) {
            if(parameters.type!=null) {
                insertValidationObject.put(OCKey.TYPE.value(), parameters.type);
            }
            if(parameters.compare!=null) {
                insertValidationObject.put(OCKey.COMPARE.value(), parameters.compare);
            }
            if(parameters.value1!=null) {
                insertValidationObject.put(OCKey.VALUE1.value(), parameters.value1);
            }
            if(parameters.value2!=null) {
                insertValidationObject.put(OCKey.VALUE2.value(), parameters.value2);
            }
            operationQueue.put(insertValidationObject);
        }
    }

    public static void insertValidation(SpreadsheetContent content, JSONObject op)
        throws JSONException {

        final int sheetIndex = op.getInt(OCKey.SHEET.value());
        final Sheet sheet = content.getContent().get(sheetIndex);
        final CellRefRange[] ranges = SmlUtils.createCellRefRanges(op.getString(OCKey.RANGES.value()));

        final ContentValidation contentValidation = new ContentValidation();
        applyJsonAttributes(contentValidation, op);
        final String newContentValidationName = content.getContentValidations(true).addValidation(contentValidation, true).getName();
        for(CellRefRange o:ranges) {

            final CellRef startRef = o.getStart();
            final CellRef endRef = o.getEnd();

            // splitting up repeated rows and allow to create a proper iterator over the NavigableSet
            sheet.getRow(endRef.getRow(), true, false, true);
            sheet.getRow(startRef.getRow(), true, true, false);

            final NavigableSet<Row> rows = sheet.getRows().subSet(new Row(startRef.getRow()), true, new Row(endRef.getRow()), true);
            for(Row row:rows) {

                // splitting up repeated cells and allow to create a proper iterator over the NavigableSet
                row.getCell(endRef.getColumn(), true, false, true);
                row.getCell(startRef.getColumn(), true, true, false);

                final NavigableSet<Cell> cellSelection = row.getCells().subSet(new Cell(startRef.getColumn()), true, new Cell(endRef.getColumn()), true);
                final Iterator<Cell> cellIter = cellSelection.iterator();
                while(cellIter.hasNext()) {
                    final Cell cell = cellIter.next();
                    cell.getCellAttributesEnhanced(true).setContentValidationName(newContentValidationName);
                }
            }
        }
    }

    public static void changeValidation(SpreadsheetContent content, JSONObject op)
        throws JSONException {

        final int sheetIndex = op.getInt(OCKey.SHEET.value());
        final Sheet sheet = content.getContent().get(sheetIndex);
        final CellRefRange[] ranges = SmlUtils.createCellRefRanges(op.getString(OCKey.RANGES.value()));
        final HashMap<String, String> changedValidations = new HashMap<String, String>();
        for(CellRefRange o:ranges) {

            final CellRef startRef = o.getStart();
            final CellRef endRef = o.getEnd();

            // splitting up repeated rows and allow to create a proper iterator over the NavigableSet
            sheet.getRow(endRef.getRow(), true, false, true);
            sheet.getRow(startRef.getRow(), true, true, false);

            final NavigableSet<Row> rows = sheet.getRows().subSet(new Row(startRef.getRow()), true, new Row(endRef.getRow()), true);
            for(Row row:rows) {

                // splitting up repeated cells and allow to create a proper iterator over the NavigableSet
                row.getCell(endRef.getColumn(), true, false, true);
                row.getCell(startRef.getColumn(), true, true, false);

                final NavigableSet<Cell> cellSelection = row.getCells().subSet(new Cell(startRef.getColumn()), true, new Cell(endRef.getColumn()), true);
                final Iterator<Cell> cellIter = cellSelection.iterator();
                while(cellIter.hasNext()) {
                    final Cell cell = cellIter.next();
                    final String contentValidationName = cell.getContentValidationName();
                    if(contentValidationName!=null) {
                        final String changedValidation = changedValidations.get(contentValidationName);
                        if(changedValidation!=null) {
                            cell.setContentValidationName(changedValidation);
                        }
                        else {
                            final ContentValidations contentValidations = content.getContentValidations(true);
                            final ContentValidation clonedContentValidation = contentValidations.getContentValidationByName(contentValidationName).clone();
                            applyJsonAttributes(clonedContentValidation, op);
                            changedValidations.put(contentValidationName, contentValidations.addValidation(clonedContentValidation, true).getName());
                        }
                    }
                }
            }
        }
    }

    public static void deleteValidation(SpreadsheetContent content, JSONObject op)
        throws JSONException {

        final CellRefRange[] ranges = SmlUtils.createCellRefRanges(op.getString(OCKey.RANGES.value()));
        final Sheet sheet = content.getContent().get(op.getInt(OCKey.SHEET.value()));

        for(CellRefRange o:ranges) {

            final CellRef startRef = o.getStart();
            final CellRef endRef = o.getEnd();

            // splitting up repeated rows and allow to create a proper iterator over the NavigableSet
            sheet.getRow(endRef.getRow(), true, false, true);
            sheet.getRow(startRef.getRow(), true, true, false);

            final NavigableSet<Row> rows = sheet.getRows().subSet(new Row(startRef.getRow()), true, new Row(endRef.getRow()), true);
            for(Row row:rows) {

                // splitting up repeated cells and allow to create a proper iterator over the NavigableSet
                row.getCell(endRef.getColumn(), true, false, true);
                row.getCell(startRef.getColumn(), true, true, false);

                final NavigableSet<Cell> cellSelection = row.getCells().subSet(new Cell(startRef.getColumn()), true, new Cell(endRef.getColumn()), true);
                final Iterator<Cell> cellIter = cellSelection.iterator();
                while(cellIter.hasNext()) {
                    final CellAttributesEnhanced cellAttributes = cellIter.next().getCellAttributesEnhanced(false);
                    if(cellAttributes!=null) {
                        cellAttributes.setContentValidationName(null);
                    }
                }
            }
        }
    }

    public static void applyJsonAttributes(ContentValidation contentValidation, JSONObject op) {
        if(op!=null) {
            ConditionToParameters parameters = null;
            final Set<Entry<String, Object>> entrySet = op.entrySet();
            for(Entry<String, Object> entry:entrySet) {
                final Object o = entry.getValue();
                switch(OCKey.fromValue(entry.getKey())) {
                    case REF : {
                        contentValidation.setBaseCellAddress(o instanceof String ? SmlUtils.getAddressWithoutBraces((String)o) : null);
                        break;
                    }
                    case TYPE : {
                        if(parameters==null) {
                            parameters = new ConditionToParameters(contentValidation.getCondition());
                        }
                        parameters.type = o instanceof String ? (String)o : null;
                        break;
                    }
                    case COMPARE : {
                        if(parameters==null) {
                            parameters = new ConditionToParameters(contentValidation.getCondition());
                        }
                        parameters.compare = o instanceof String ? (String)o : null;
                        break;
                    }
                    case VALUE1 : {
                        if(parameters==null) {
                            parameters = new ConditionToParameters(contentValidation.getCondition());
                        }
                        parameters.value1 = o instanceof String ? (String)o : null;
                        break;
                    }
                    case VALUE2 : {
                        if(parameters==null) {
                            parameters = new ConditionToParameters(contentValidation.getCondition());
                        }
                        parameters.value2 = o instanceof String ? (String)o : null;
                        break;
                    }
                    case SHOW_INFO : {
                        contentValidation.getHelpMessage(true).setDisplay((o instanceof Boolean&&!((Boolean)o).booleanValue()) ? false : null);
                        break;
                    }
                    case INFO_TITLE : {
                        contentValidation.getHelpMessage(true).setTitle(o instanceof String ? (String)o : "");
                        break;
                    }
                    case INFO_TEXT : {
                        TextContentHelper.setSimpleText(contentValidation.getHelpMessage(true), o instanceof String ? (String)o : "");
                        break;
                    }
                    case SHOW_ERROR : {
                        contentValidation.getErrorMessage(true).setDisplay((o instanceof Boolean&&!((Boolean)o).booleanValue()) ? false : null);
                        break;
                    }
                    case ERROR_TITLE : {
                        contentValidation.getErrorMessage(true).setTitle(o instanceof String ? (String)o : "");
                        break;
                    }
                    case ERROR_TEXT : {
                        final String simpleText = o instanceof String ? (String)o : "";
                        if(simpleText.isEmpty()) {
                            contentValidation.getErrorMessage(true).getContent().clear();
                        }
                        else {
                            TextContentHelper.setSimpleText(contentValidation.getErrorMessage(true), simpleText);
                        }
                        break;
                    }
                    case ERROR_TYPE : {
                        if("warning".equals(o)) {
                            contentValidation.getErrorMessage(true).setMessageType("warning");
                        }
                        else if("info".equals(o)) {
                            contentValidation.getErrorMessage(true).setMessageType("information");
                        }
                        else {
                            contentValidation.getErrorMessage(true).setMessageType(null);
                        }
                        break;
                    }
                    case SHOW_DROP_DOWN : {
                        contentValidation.setDisplayList(o instanceof Boolean&&!((Boolean)o).booleanValue() ? "none" : null);
                        break;
                    }
                    case IGNORE_EMPTY : {
                        contentValidation.setAllowEmptyCell(o instanceof Boolean&&!((Boolean)o).booleanValue() ? Boolean.FALSE : null);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
            if(parameters!=null) {
                contentValidation.setCondition(parameters.getCondition());
            }
        }
    }

    static class ConditionToParameters {

        String type;
        String compare;
        String value1;
        String value2;
        boolean isValid;

        public ConditionToParameters(String condition) {
            if(condition==null) {
                type = "all";
                isValid = true;
            }
            else {
                isValid = parseCondition(condition);
            }
        }

        public String getCondition() {
            if("all".equals(type)) {
                return null;
            }

            final StringBuffer buffer = new StringBuffer("of:");
            if("integer".equals(type)) {
                buffer.append(CONDITION_cellContentIsDecimalNumber_and);
                appendValueCondition(buffer);
            }
            else if("number".equals(type)) {
                buffer.append(CONDITION_cellContentIsDecimalNumber_and);
                appendValueCondition(buffer);
            }
            else if("date".equals(type)) {
                buffer.append(CONDITION_cellContentIsDate_and);
                appendValueCondition(buffer);
            }
            else if("time".equals(type)) {
                buffer.append(CONDITION_cellContentIsTime_and);
                appendValueCondition(buffer);
            }
            else if("length".equals(type)) {
                if("between".equals(compare)) {
                    buffer.append(CONDITION_cellContentTextLengthIsBetween);
                    appendTwoParameters(buffer);
                }
                else if("notBetween".equals(compare)) {
                    buffer.append(CONDITION_cellContentTextLengthIsNotBetween);
                    appendTwoParameters(buffer);
                }
                else {
                    buffer.append(CONDITION_contentTextLength);
                    appendOpValue(buffer);
                }
            }
            else if("list".equals(type)||"source".equals(type)) {
                buffer.append(CONDITION_cellContentIsInList);
                if(value1!=null) {
                    buffer.append(value1);
                }
                buffer.append(')');
            }
            else if("custom".equals(type)) {
                buffer.append(CONDITION_isTrueFormula);
                buffer.append('(');
                if(value1!=null) {
                    buffer.append(value1);
                }
                buffer.append(')');
            }
            return buffer.toString();
        }

        private void appendValueCondition(StringBuffer buffer) {
            if("between".equals(compare)) {
                buffer.append(CONDITION_cellContentIsBetween);
                appendTwoParameters(buffer);
            }
            else if("notBetween".equals(compare)) {
                buffer.append(CONDITION_cellContentIsNotBetween);
                appendTwoParameters(buffer);
            }
            else {
                buffer.append(CONDITION_cellContent);
                appendOpValue(buffer);
            }
        }

        private void appendTwoParameters(StringBuffer buffer) {
            buffer.append('(');
            buffer.append(value1);
            buffer.append(',');
            buffer.append(value2);
            buffer.append(')');
        }

        private void appendOpValue(StringBuffer buffer) {
            if("equal".equals(compare)) {
                buffer.append('=');
            }
            else if("notEqual".equals(compare)) {
                buffer.append("!=");
            }
            else if("less".equals(compare)) {
                buffer.append('<');
            }
            else if("lessEqual".equals(compare)) {
                buffer.append("<=");
            }
            else if("greater".equals(compare)) {
                buffer.append('>');
            }
            else if("greaterEqual".equals(compare)) {
                buffer.append(">=");
            }
            if(value1!=null) {
                buffer.append(value1);
            }
        }

        private boolean parseCondition(String condition) {
            if(condition==null||condition.isEmpty()) {
                return false;
            }
            int index = -1;
            if(condition.startsWith("of:")) {
                index = 3;
            }
            else if(!condition.contains(":")) {
                index = 0;
            }
            if(index<0) {
                return false;
            }

            // only parsing if namespace is default or "of:"
            String c = condition.substring(index).trim();
            if(c.startsWith(CONDITION_contentTextLength)) {
                type = "length";
                return getOpValue(c, CONDITION_contentTextLength.length());
            }
            else if(c.startsWith(CONDITION_cellContentTextLengthIsBetween)) {
                type = "length";
                compare = "between";
                return getBetweenParameters(c.substring(CONDITION_cellContentTextLengthIsBetween.length()+1));
            }
            else if(c.startsWith(CONDITION_cellContentTextLengthIsNotBetween)) {
                type = "length";
                compare = "notBetween";
                return getBetweenParameters(c.substring(CONDITION_cellContentTextLengthIsNotBetween.length()+1));
            }
            else if(c.startsWith(CONDITION_cellContentIsInList)) {
                type = "list";
                final int i = c.lastIndexOf(')');
                if(i==-1) {
                    return false;
                }
                c = c.substring(CONDITION_cellContentIsInList.length(), i);
                value1 = c;
                if(!c.startsWith("\"")) {
                    type = "source";
                }
            }
            else if(c.startsWith(CONDITION_cellContentIsDecimalNumber_and)) {
                type = "number";
                return getValueCondition(c, CONDITION_cellContentIsDecimalNumber_and.length());
            }
            else if(c.startsWith(CONDITION_cellContentIsWholeNumber_and)) {
                type = "integer";
                return getValueCondition(c, CONDITION_cellContentIsWholeNumber_and.length());
            }
            else if(c.startsWith(CONDITION_cellContentIsDate_and)) {
                type = "date";
                return getValueCondition(c, CONDITION_cellContentIsDate_and.length());
            }
            else if(c.startsWith(CONDITION_cellContentIsTime_and)) {
                type = "time";
                return getValueCondition(c, CONDITION_cellContentIsTime_and.length());
            }
            else if(c.startsWith(CONDITION_isTrueFormula)) {
                type = "custom";
                final int i = c.lastIndexOf(')');
                if(i==-1) {
                    return false;
                }
                value1 = c.substring(CONDITION_isTrueFormula.length(), i);
            }
            else {
                return false;
            }
            return true;
        }

        private boolean getOpValue(String source, int index) {

            final String opString = source.substring(index).trim();
            if(opString.startsWith("<=")) {
                compare = "lessEqual";
                return getParameter(opString.substring(2));
            }
            else if(opString.startsWith(">=")) {
                compare = "greaterEqual";
                return getParameter(opString.substring(2));
            }
            else if(opString.startsWith("!=")) {
                compare = "notEqual";
                return getParameter(opString.substring(2));
            }
            else if(opString.startsWith("<")) {
                compare = "less";
                return getParameter(opString.substring(1));
            }
            else if(opString.startsWith(">")) {
                compare = "greater";
                return getParameter(opString.substring(1));
            }
            else if(opString.startsWith("=")) {
                compare = "equal";
                return getParameter(opString.substring(1));
            }
            return false;
        }

        private boolean getValueCondition(String source, int index) {

            String c = source.substring(index).trim();
            if(c.startsWith(CONDITION_cellContent)) {
                return getOpValue(c, CONDITION_cellContent.length());
            }
            else if(c.startsWith(CONDITION_cellContentIsBetween)) {
                compare = "between";
                return getBetweenParameters(c.substring(CONDITION_cellContentIsBetween.length()+1));
            }
            else if(c.startsWith(CONDITION_cellContentIsNotBetween)) {
                compare = "notBetween";
                return getBetweenParameters(c.substring(CONDITION_cellContentIsNotBetween.length()+1));
            }
            return false;
        }

        private boolean getBetweenParameters(String source) {

            final int i = source.lastIndexOf(')');
            if(i==-1) {
                return false;
            }
            final String c = source.substring(0, i);
            final String[] arrays = c.split(",", -1);
            if(arrays.length!=2) {
                return false;
            }
            value1 = arrays[0].trim();
            value2 = arrays[1].trim();
            return true;
        }

        private boolean getParameter(String source) {
            value1 = source;
            return true;
        }
    }
}
