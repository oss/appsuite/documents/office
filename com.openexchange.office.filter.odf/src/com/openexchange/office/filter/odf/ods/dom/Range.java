/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.json.JSONArray;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.core.spreadsheet.ICellRefRangeAccess;

public class Range implements ICellRefRangeAccess, Cloneable {

	private int sheet;
	private CellRefRange cellRefRange;

	public Range(SpreadsheetContent content, String rangeAddress) {
		sheet = -1;
		setRangeAddress(content, rangeAddress);
	}

	public Range(int sheet, CellRefRange cellRefRange) {
		this.sheet = sheet;
		this.cellRefRange = cellRefRange;
	}

	public Range(int sheetIndex, JSONArray start, JSONArray end) {
		this.sheet = sheetIndex;
		this.cellRefRange = new CellRefRange(new CellRef(start), new CellRef(end));
	}

	@Override
	public CellRefRange getCellRefRange(boolean forceCreate) {
	    if(cellRefRange==null && forceCreate) {
	        cellRefRange = new CellRefRange();
	    }
	    return cellRefRange;
	}

    @Override
	public void setCellRefRange(CellRefRange cellRefRange) {
    	this.cellRefRange = cellRefRange;
    }

    public int getSheetIndex() {
		return sheet;
	}

	public void setSheetIndex(int index) {
		sheet = index;
	}

	public String convertToString(SpreadsheetContent content, boolean createSheetRef) {

		String sheetName = createSheetRef&&sheet>=0 ?	content.getContent().get(sheet).getName() : null;
		if(sheetName!=null) {
			if(sheetName.matches(".*[\\.\\! ].*")) {
				sheetName = '\'' + sheetName + '\'';
			}
			return new StringBuffer().append(sheetName)
						 			 .append(".")
						 			 .append(CellRef.getCellRef(cellRefRange.getStart()))
						 			 .append(":")
						 			 .append(sheetName)
						 			 .append(".")
						 			 .append(CellRef.getCellRef(cellRefRange.getEnd())).toString();
		}
		return new StringBuffer().append(CellRef.getCellRef(cellRefRange.getStart()))
								 .append(":")
								 .append(CellRef.getCellRef(cellRefRange.getEnd())).toString();
	}

    private void setRangeAddress(SpreadsheetContent content, String value) {

    	CellRef start = new CellRef(0, 0);
    	CellRef end = new CellRef(0, 0);

    	if(value!=null) {
	    	final String[] refs = value.split(":");
	    	if(refs.length<=2) {
		    	int i;
		    	for(i=0;i<refs.length;i++) {
		    		final String ref = refs[i];
		    		int index = ref.lastIndexOf(".");
		    		if(i==0&&index>0) {
						sheet = content.getSheetIndex(ref.substring(0, index));
		    		}
		    		final CellRef cRef = CellRef.createCellRef(ref.substring(index+1));
		    		if(cRef!=null) {
			    		if(i==0) {
			    			start = cRef;
			    		}
			    		else {
			    			end = cRef;
			    		}
		    		}
		    	}
		    	if(i==1) {
		    		end = (CellRef)start.clone();
		    	}
	    	}
    	}
    	cellRefRange = new CellRefRange(start, end);
    }

	@Override
	public Range clone() throws CloneNotSupportedException {
		return new Range(sheet, cellRefRange.clone());
	}

	@Override
	public String toString() {
		return "Range [sheet=" + sheet + ", cellRefRange=" + cellRefRange + "]";
	}
}
