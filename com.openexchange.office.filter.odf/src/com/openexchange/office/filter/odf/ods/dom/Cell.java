/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.Iterator;
import java.util.List;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.SmlUtils.*;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.odt.dom.Annotation;
import com.openexchange.office.filter.odf.properties.MapProperties;
import com.openexchange.office.filter.odf.styles.NumberCurrencyStyle;
import com.openexchange.office.filter.odf.styles.NumberDateStyle;
import com.openexchange.office.filter.odf.styles.NumberPercentageStyle;
import com.openexchange.office.filter.odf.styles.NumberTimeStyle;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleTableCell;

public class Cell implements Comparable<Cell> {

	public static class ErrorCode {

		final String errorCode;

		public ErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}
		public String getError() {
			return errorCode;
		}
	}

	int column;
	private String cellStyle = null;
	private int repeated = 1;

	// the cell content is stored in one of the two following variables
	private Object content = null;				// immutable objects only, otherwise the clone needs to be changed
	private String formula = null;

	private DLList<IElementWriter> childs;
	private CellAttributesEnhanced cellAttributesEnhanced = null;

	Cell(int c) {
		column = c;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int c) {
		column = c;
	}

	public String getCellStyle() {
    	return cellStyle;
    }

    public void setCellStyle(String val) {
    	cellStyle = val;
    }

    public int getRepeated() {
    	return repeated;
    }

    public void setRepeated(int val) {
    	repeated = val;
    }

    public String getContentValidationName() {
        if(cellAttributesEnhanced!=null) {
            return cellAttributesEnhanced.getContentValidationName();
        }
        return null;
    }

    public void setContentValidationName(String name) {
        if(name==null) {
            if(cellAttributesEnhanced!=null) {
                cellAttributesEnhanced.setContentValidationName(name);
                return;
            }
        }
        getCellAttributesEnhanced(true).setContentValidationName(name);
    }

    public CellAttributesEnhanced getCellAttributesEnhanced(boolean createIfMissing) {
    	if(createIfMissing&&cellAttributesEnhanced==null) {
    		cellAttributesEnhanced = new CellAttributesEnhanced();
    	}
    	return cellAttributesEnhanced;
    }

    public DLList<IElementWriter> getContent(boolean forceCreate) {
        if(childs==null&&forceCreate) {
            childs = new DLList<IElementWriter>();
        }
        return childs;
    }

    // mergeCell can be null if cell is not merged / covered
    public void writeObject(SerializationHandler output, Sheet sheet, Row row, MergeCell mergeCell)
        throws SAXException {

        boolean covered = false;
        boolean spannedAttribute = false;

        if(mergeCell!=null) {
            final CellRef mergeStart = mergeCell.getCellRefRange(true).getStart();
            if(mergeStart.getColumn()==getColumn()&&row.getRow()==mergeStart.getRow()) {
                // only the top left cell of the merge range gets the spanned row/column attributes
                spannedAttribute = true;
            }
            else {
                covered = true;
            }
        }
        if(covered) {
            SaxContextHandler.startElement(output, Namespaces.TABLE, "covered-table-cell", "table:covered-table-cell");
        }
        else {
            SaxContextHandler.startElement(output, Namespaces.TABLE, "table-cell", "table:table-cell");
        }
        if(getRepeated()>1) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "number-columns-repeated", "table:number-columns-repeated", Integer.toString(getRepeated()));
        }
        if(getCellStyle()!=null) {
            SaxContextHandler.addAttribute(output, Namespaces.TABLE, "style-name", "table:style-name", getCellStyle());
        }
        if(spannedAttribute) {
            final int columns = mergeCell.getCellRefRange(true).getEnd().getColumn() - mergeCell.getCellRefRange(true).getStart().getColumn();
            final int rows = mergeCell.getCellRefRange(true).getEnd().getRow() - mergeCell.getCellRefRange(true).getStart().getRow();
            if(columns>0||rows>0) {
                SaxContextHandler.addAttribute(output, Namespaces.TABLE, "number-columns-spanned", "table:number-columns-spanned", Integer.toString(columns+1));
                SaxContextHandler.addAttribute(output, Namespaces.TABLE, "number-rows-spanned", "table:number-rows-spanned", Integer.toString(rows+1));
            }
        }

        // writing cell content
        if(cellAttributesEnhanced!=null) {
            cellAttributesEnhanced.writeAttributes(output);
        }
        if(content!=null) {
            if(formula!=null) {
                SaxContextHandler.addAttribute(output, Namespaces.TABLE, "formula", "table:formula", "of:=".concat(formula));
            }
            if(content instanceof String) {
                final String contentString = (String)content;
                if(!contentString.isEmpty()) {
                    SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "value-type", "office:value-type", "string");
                    SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "value-type", "calcext:value-type", "string");
                    writeContentString(output, sheet, row, contentString);
                }
            }
            else if(content instanceof Boolean) {
                SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "value-type", "office:value-type", "boolean");
                SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "value-type", "calcext:value-type", "boolean");
                SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "boolean-value", "office:boolean-value", ((Boolean)content)?"true":"false");
            }
            else if(content instanceof ErrorCode) {
                SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "value-type", "office:value-type", "string");
                SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "string-value", "office:string-value", "");
                SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "value-type", "calcext:value-type", "error");
                final String errorCode = ((ErrorCode)content).getError();
                if(errorCode!=null) {
                    writeContentString(output, sheet, row, errorCode);
                }
            }
            else if(content instanceof Number) {
                String valueType = "float";
                String styleName = getCellStyle();
                if(styleName==null) {
                    styleName = row.getDefaultCellStyle();
                    if(styleName==null) {
                        final Column col = sheet.getColumn(getColumn(), false, false, false);
                        if(col!=null) {
                            styleName = col.getDefaultCellStyle();
                        }
                    }
                }
                if(styleName!=null&&!styleName.isEmpty()) {
                    final StyleManager styleManager = ((OdfFileDom)sheet.getOwnerDocument()).getDocument().getStyleManager();
                    final StyleBase styleBase = styleManager.getStyle(styleName, StyleFamily.TABLE_CELL, true);
                    if(styleBase instanceof StyleTableCell) {
                        final StyleTableCell styleTableCell = (StyleTableCell)styleBase;
                        final String dataStyleName = styleTableCell.getAttribute("style:data-style-name");
                        if(dataStyleName!=null&&!dataStyleName.isEmpty()) {
                            StyleBase dataStyleBase = styleManager.getStyle(dataStyleName, StyleFamily.DATA_STYLE, true);
                            if(dataStyleBase!=null) {
                                final Iterator<MapProperties> mapPropertyIter = dataStyleBase.getMapStyleList().iterator();
                                while(mapPropertyIter.hasNext()) {
                                    final MapProperties mapProperties = mapPropertyIter.next();
                                    final String mapping = mapProperties.getMapping(styleManager, true, StyleFamily.TABLE_CELL);
                                    if(mapping!=null) {
                                        final String applyStyleName = mapProperties.getAttribute("style:apply-style-name");
                                        if(applyStyleName!=null) {
                                            final StyleBase conditionalDataStyleBase = styleManager.getStyle(applyStyleName, StyleFamily.DATA_STYLE, true);
                                            if(conditionalDataStyleBase!=null) {
                                                dataStyleBase = conditionalDataStyleBase;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if(dataStyleBase instanceof NumberCurrencyStyle) {
                                final String currencyCode = ((NumberCurrencyStyle)dataStyleBase).getCurrencyCode();
                                if(currencyCode!=null) {
                                    SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "currency", "office:currency", currencyCode);
                                }
                                valueType = "currency";
                            }
                            else if(dataStyleBase instanceof NumberPercentageStyle) {
                                valueType = "percentage";
                            }
                            else if(dataStyleBase instanceof NumberDateStyle) {
                                // TODO: date-value instead of office-value should be created
                                valueType = "date";
                            }
                            else if(dataStyleBase instanceof NumberTimeStyle) {
//                                      SaxContextHandler.addOdfAttribute(output, OfficeTimeValueAttribute.ATTRIBUTE_NAME, null, MapHelper.doubleToTime(((Number)content).doubleValue()));
//                                      officeValueAttributeRequired = false;
                                valueType = "time";
                            }
                        }
                    }
                }
                SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "value", "office:value", ((Number)content).toString());
                SaxContextHandler.addAttribute(output, Namespaces.OFFICE, "value-type", "office:value-type", valueType);
                SaxContextHandler.addAttribute(output, Namespaces.CALCEXT, "value-type", "calcext:value-type", valueType);
            }
        }
        final List<Drawing> anchoredDrawings = sheet.getDrawings().getAnchoredDrawings(new DrawingAnchor(this.getColumn(), row.getRow()));
        if(anchoredDrawings!=null) {
            for(Drawing drawing:anchoredDrawings) {
                drawing.writeObject(output);
            }
        }
        if(childs!=null) {
            for(IElementWriter child:childs) {
                child.writeObject(output);
            }
        }
        if(covered) {
            SaxContextHandler.endElement(output, Namespaces.TABLE, "covered-table-cell", "table:covered-table-cell");
        }
        else {
            SaxContextHandler.endElement(output, Namespaces.TABLE, "table-cell", "table:table-cell");
        }
    }

	private void writeContentString(SerializationHandler output, Sheet sheet, Row row, String contentString)
		throws SAXException {

		final String[] paragraphs = contentString.split("\r\n|\n|\r", -1);
    	for(int i=0; i<paragraphs.length; i++) {
        	// simply writing text ... what to do if the content is a error an starts with '#'
        	SaxContextHandler.startElement(output, Namespaces.TEXT, "p", "text:p");
        	final Hyperlink hyperlink = i==0 ? sheet.getHyperlink(column, row.getRow()) : null;
        	if(hyperlink!=null) {
        		SaxContextHandler.startElement(output, Namespaces.TEXT, "a", "text:a");
        		SaxContextHandler.addAttribute(output, Namespaces.XLINK, "type", "xlink:type", "simple");
        		SaxContextHandler.addAttribute(output, Namespaces.XLINK, "href", "xlink:href", hyperlink.getUrl());
        	}
        	if(!paragraphs[i].isEmpty()) {
        	    output.characters(paragraphs[i]);
        	}
        	if(hyperlink!=null) {
        		SaxContextHandler.endElement(output, Namespaces.TEXT, "a", "text:a");
        	}
        	SaxContextHandler.endElement(output, Namespaces.TEXT, "p", "text:p");
    	}
	}

	public Object getCellContent() {
		return content;
    }

    public void setCellContent(Object value) {
    	content = value;
    }

    public String getCellFormula() {
    	return formula;
    }

    public void setCellFormula(String formula) {
    	this.formula = formula;
    }

    public Annotation getAnnotation() {
        if(childs!=null) {
            for(IElementWriter child:childs) {
                if(child instanceof Annotation) {
                    return (Annotation)child;
                }
            }
        }
        return null;
    }

    public Annotation deleteAnnotation() {
        if(childs!=null) {
            final Iterator<IElementWriter> childIter = childs.iterator();
            while(childIter.hasNext()) {
                final IElementWriter child = childIter.next();
                if(child instanceof Annotation) {
                    childIter.remove();
                    return (Annotation)child;
                }
            }
        }
        return null;
    }

    public Cell clone(boolean cloneContent) {
    	final Cell clone = new Cell(column);
    	clone.setRepeated(getRepeated());
    	clone.setCellStyle(getCellStyle());
    	if(getContentValidationName()!=null) {
    	    clone.setContentValidationName(getContentValidationName());
    	}
    	if(cloneContent) {
	    	if(childs!=null&&!childs.isEmpty()) {
	    	    final DLList<IElementWriter> destContent = clone.getContent(true);
	    	    for(IElementWriter child:childs) {
	    	        if(child instanceof ElementNSWriter) {
	    	            destContent.add((ElementNSWriter)((ElementNSWriter)child).cloneNode(true));
	    	        }
	    	    }
	    	    // it shouldn't be necessary to clone annotations
	    	}
    		if(content!=null) {
    			clone.content = content;
    		}
    		clone.formula = formula;
    	}
    	return clone;
    }

	@Override
	public int compareTo(Cell o) {
		return column - o.getColumn();
	}
}
