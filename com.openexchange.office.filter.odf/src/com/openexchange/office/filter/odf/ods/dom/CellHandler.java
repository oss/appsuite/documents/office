/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.time.Duration;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import jakarta.xml.bind.DatatypeConverter;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.core.spreadsheet.SmlUtils.*;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.ElementNSWriter;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.UnknownContentHandler;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawFrameHandler;
import com.openexchange.office.filter.odf.draw.Shape;
import com.openexchange.office.filter.odf.draw.ShapeHelper;
import com.openexchange.office.filter.odf.odt.dom.Annotation;
import com.openexchange.office.filter.odf.odt.dom.AnnotationHandler;

public class CellHandler extends SaxContextHandler {

	final private Sheet sheet;
	final private Row row;
	final private Cell cell;

	private String value;
	private String officeValueType;
	private String calcExtValueType;
	private String formula;
	private String currency;
	private String boolValue;
	private String dateValue;
	private String timeValue;
	private String stringValue;
	private boolean hasStringValueAttribute;

	public CellHandler(RowHandler parentContextHandler, Attributes attributes, Sheet sheet, Row row, Cell cell) {
		super(parentContextHandler);
		this.sheet = sheet;
		this.row = row;
		this.cell = cell;

		Integer columnsSpanned = 1;
		Integer rowsSpanned = 1;

		value = null;
		officeValueType = null;
		calcExtValueType = null;
		formula = null;
		currency = null;
		boolValue = null;
		dateValue = null;
		timeValue = null;
		hasStringValueAttribute = false;
		stringValue = null;

		for(int i=0; i<attributes.getLength(); i++) {
			final String localName = attributes.getLocalName(i);
			if(localName.equals("style-name")) {
				cell.setCellStyle(attributes.getValue(i));
			}
			else if(localName.equals("number-columns-repeated")) {
				final int repeated = Integer.valueOf(attributes.getValue(i));
				if((repeated>=1)&&(repeated<=16384)&&(repeated+cell.getColumn()<=16384)) {
					cell.setRepeated(repeated);
				}
			}
			else if(localName.equals("number-columns-spanned")) {
				columnsSpanned = Integer.valueOf(attributes.getValue(i));
			}
			else if(localName.equals("number-rows-spanned")) {
				rowsSpanned = Integer.valueOf(attributes.getValue(i));
			}
			else if(localName.equals("value-type")) {
				if(attributes.getURI(i).equals("urn:oasis:names:tc:opendocument:xmlns:office:1.0")) {
					officeValueType = attributes.getValue(i);
				}
				else if(attributes.getURI(i).equals("urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0")) {
					calcExtValueType = attributes.getValue(i);
				}
			}
			else if(localName.equals("value")) {
				value = attributes.getValue(i);
			}
			else if(localName.equals("formula")) {
				formula = getFormulaValue(attributes.getValue(i));
			}
			else if(localName.equals("currency")) {
				currency = attributes.getValue(i);
			}
			else if(localName.equals("boolean-value")) {
				boolValue = attributes.getValue(i);
			}
			else if(localName.equals("date-value")) {
				dateValue = attributes.getValue(i);
			}
			else if(localName.equals("time-value")) {
				timeValue = attributes.getValue(i);
			}
			else if(localName.equals("string-value")) {
				hasStringValueAttribute = true;
				stringValue = attributes.getValue(i);
				if(stringValue.isEmpty()) {
					hasStringValueAttribute = false;
					stringValue = null;
				}
			}
			else if(localName.equals("content-validation-name")) {
			    final String contentValidationName = attributes.getValue(i);
			    if(!contentValidationName.isEmpty()) {
    				cell.getCellAttributesEnhanced(true).setContentValidationName(contentValidationName);
    				((SpreadsheetContent)sheet.getOwnerDocument()).getContentValidations(true).getUsedValidations().add(contentValidationName);
			    }
			}
			else if(localName.equals("number-matrix-rows-spanned")) {
				cell.getCellAttributesEnhanced(true).setNumberMatrixRowsSpanned(attributes.getValue(i));
			}
			else if(localName.equals("number-matrix-columns-spanned")) {
				cell.getCellAttributesEnhanced(true).setNumberMatrixColumnsSpanned(attributes.getValue(i));
			}
			else if(localName.equals("protect")) {
				cell.getCellAttributesEnhanced(true).setProtect(attributes.getValue(i));
			}
			else if(localName.equals("protected")) {
				cell.getCellAttributesEnhanced(true).setProtected(attributes.getValue(i));
			}
/* TODO store unspecified attributes...
			else {

			}
*/
		}
        // inserting a new mergeCells area
        if(columnsSpanned>1||rowsSpanned>1) {
            final List<MergeCell> mergeCells = parentContextHandler.getSheet().getMergeCells();
            mergeCells.add(new MergeCell(new CellRef(cell.getColumn(), parentContextHandler.getRow().getRow()), columnsSpanned, rowsSpanned));
        }
		// set the maximum allowed number of columns to 16384 if a column greater than 1024 is used
		if(cell.getColumn()+cell.getRepeated()>1024) {
			((SpreadsheetContent)parentContextHandler.getFileDom()).setMaxColumnCount(16384);
		}
	}

	public String getStringValue() {
		return stringValue;
	}

	public void appendStringValue(String _value) {
		if(!hasStringValueAttribute&&_value!=null) {
			if(stringValue!=null) {
				stringValue = stringValue + _value;
			}
			else {
				stringValue = _value;
			}
		}
	}

	@Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        OdfOperationDoc.abortOnLowMemory(getFileDom());
		if(qName.equals("text:p")) {
			if(!hasStringValueAttribute && stringValue!=null) {
				stringValue = stringValue + "\n";
			}
			return new CellPHandler(this, sheet, row, cell);
		}
		if(qName.equals("office:annotation")) {
            final Annotation annotation = new Annotation("", attributes, true);
            cell.getContent(true).add(annotation);
            return new AnnotationHandler(this, annotation);
		}
		if(qName.equals("draw:frame")) {
            final DrawFrame drawFrame = new DrawFrame(getFileDom().getDocument(), new AttributesImpl(attributes), null, true, true);
            final Drawing drawing = new Drawing(sheet, new DrawingAnchor(cell.getColumn(), row.getRow()), drawFrame);
            sheet.getDrawings().addDrawing(drawing);
            return new DrawFrameHandler(this, drawFrame);
		}
        final Shape shape = ShapeHelper.getShape(attributes, uri, localName, qName, null, true, true);
        if(shape!=null) {
            final Drawing drawing = new Drawing(sheet, new DrawingAnchor(cell.getColumn(), row.getRow()), shape);
            sheet.getDrawings().addDrawing(drawing);
            return shape.getContextHandler(this);
        }
		// using a new Handler that adds each element (beside p elements) as child to the cellElement, this is done only for complex
		// cells as most cells are only having one p element.
        final ElementNSWriter element = new ElementNSWriter(getFileDom(), attributes, uri, qName);
        cell.getContent(true).add(element);
        return new UnknownContentHandler(this, element);
	}

    @Override
    public void endContext(String qName, String characters) {
    	if(qName.equals("table:table-cell")||qName.equals("table:covered-table-cell")) {
			Object content = null;
			if(calcExtValueType!=null&&calcExtValueType.equals("error")) {
				content = new Cell.ErrorCode(stringValue);
			}
			else if(officeValueType!=null&&!officeValueType.isEmpty()) {
	        	if(value!=null&&!value.isEmpty()) {
	        		try {
	        			content = Double.parseDouble(value);
	        		}
	        		catch(NumberFormatException e) {
	        		    //
	        		}
	    		}
	            if (officeValueType.equals("boolean")) {
	                //office:boolean-value
	                if(boolValue!=null&&!boolValue.isEmpty()) {
	                	switch(boolValue.toLowerCase()) {
	                		case "1" :
	                		case "true" :
	                		case "yes" :
	                			content = Boolean.valueOf(true);
	                		break;
	                		case "0" :
	                		case "false" :
	                		case "no" :
	                			content = Boolean.valueOf(false);
	                		break;
	                	}
	                }
	            }
	            else if (officeValueType.equals("date")) {
	                if(dateValue!=null&&!dateValue.isEmpty() ) {
	                    content = dateToDouble(dateValue);
	                }
	                else if(content==null) {
	                	content = tryGetOtherValues();
	                }
	            }
	            else if (officeValueType.equals("float")&&content==null) {
	            	content = tryGetOtherValues();
	            }
	            else if (officeValueType.equals("currency")&&content==null) {
	            	content = tryGetOtherValues();
	            }
	            else if (officeValueType.equals("percentage")&&content==null) {
	            	content = tryGetOtherValues();
	            }
	            else if (officeValueType.equals("string")) {
	            	content = stringValue;
	            }
	            else if (officeValueType.equals("time")) {
	                //office:time-value
	                if(timeValue!=null&&!timeValue.isEmpty()) {
	                    content = timeToDouble(timeValue);
	                }
	                else if(content==null) {
	                	content = tryGetOtherValues();
	                }
	            }
	            // do not allow empty strings.
	            if (content instanceof String && ((String)content).isEmpty()) {
	                content = null;
	            }
	        }
	        else if (value!=null&&!value.isEmpty()) {
        		try {
        			content = Double.parseDouble(value);
        		}
        		catch(NumberFormatException e) {
        		    //
        		}
	        }
			if (formula!=null&&!formula.isEmpty()) {
				cell.setCellFormula(getFormulaValue(formula));
			}
	        if(content==null&&!characters.isEmpty()) {
	        	content = characters;
	        }
	        cell.setCellContent(content);
    	}
	}

	// sometimes a officeValueType "float", "currency" or "percentage" is used without corresponding office-value
	// then we have to check if we can other values such as date-value
    private Object tryGetOtherValues() {
    	if(dateValue!=null&&!dateValue.isEmpty()) {
            return dateToDouble(dateValue);
    	}
    	else if(timeValue!=null&&!timeValue.isEmpty()) {
            return timeToDouble(timeValue);
    	}
    	else if(boolValue!=null&&!boolValue.isEmpty()) {
            try {
            	return Double.parseDouble(boolValue);
            } catch(NumberFormatException e) {
                //
            }
        }
    	return null;
    }

    public String getFormulaValue(String _formula) {

		if (_formula.startsWith("of:=")) {
            return _formula.substring(4);
        }
        else if (_formula.startsWith("oooc:=")) {
            return _formula.substring(6);
        }
/*        else if (formula.startsWith("msoxl:")) {
        	return formula.substring(6);
        }
*/
        return _formula;
    }

    //convert xmlschema-2 date to double
    private static Double dateToDouble(Object value) {
        Double ret = Double.valueOf(0);
        if(value != null && value instanceof String) {
            TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
            try {
                Calendar cal = DatatypeConverter.parseDateTime((String)value);
                long diff = cal.getTimeInMillis() + 2209161600000l; //30.12.1899
                ret = diff / 86400000.;
            } catch (IllegalArgumentException e) {
                //
            }
            TimeZone.setDefault(null);
        }
        return ret;
    }

    //convert xmlschema-2 duration to double
    private static Double timeToDouble(String duration) {
        try {
            return Long.valueOf(Duration.parse(duration).toMillis()).doubleValue() / 86400000.0;
        }
        catch(DateTimeParseException e) {
            return null;
        }
    }
}
