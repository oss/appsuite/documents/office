/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class ConditionalFormatHandler extends SaxContextHandler {

	final private ConditionalFormats conditionalFormats;
	final private Ranges ranges;
	private int priority = 0;

	public ConditionalFormatHandler(SaxContextHandler parentContextHandler, ConditionalFormats conditionalFormats, Attributes attributes) {
		super(parentContextHandler);

		this.conditionalFormats = conditionalFormats;
		this.ranges = new Ranges((SpreadsheetContent)this.getFileDom(), attributes.getValue("calcext:target-range-address"));
	}

	@Override
	public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
		final String tmpID = "gen" + (conditionalFormats.getConditionalFormatList().size() + 1) + "-" + localName;

		if(qName.equals("calcext:condition") || qName.equals("calcext:date-is")) {
			final Condition condition = new Condition(attributes, priority++, ranges.clone(), tmpID);
			//conditionalFormats.getConditionalFormatList().put(condition.getId(), condition);
			conditionalFormats.getConditionalFormatList().add(condition);

		} else if(qName.equals("calcext:icon-set") || qName.equals("calcext:color-scale") || qName.equals("calcext:data-bar")) {
			final EntryHolderCondition condition = new EntryHolderCondition(attributes, priority++, ranges.clone(), localName, tmpID);
			//conditionalFormats.getConditionalFormatList().put(condition.getId(), condition);
			conditionalFormats.getConditionalFormatList().add(condition);

			return new ConditionalEntryHandler(this, condition);
		}
		return this;
	}
}
