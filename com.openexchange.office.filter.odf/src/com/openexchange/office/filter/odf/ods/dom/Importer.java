/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.io.InputStream;

import org.odftoolkit.odfdom.doc.OdfDocument.OdfMediaType;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.springframework.stereotype.Service;

import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.tools.annotation.RegisteredService;

@Service
@RegisteredService
public class Importer extends com.openexchange.office.filter.odf.Importer {

    @Override
    public InputStream getDefaultDocument(InputStream templateDocument, DocumentProperties docProps) {

        InputStream defaultDocument = null;
        if(templateDocument!=null) {
            return templateDocument;
        }
        try (OdfOperationDoc odfdomDocument = new OdfOperationDoc(null, null, docProps)) {
        	bundleCtx.prepareObject(odfdomDocument);
            defaultDocument = odfdomDocument.createPackage(OdfMediaType.SPREADSHEET, docProps);
        }
        catch(Throwable e) {
            OdfOperationDoc.rethrowFilterException(e, (OdfPackage)null);
        }
        return defaultDocument;
    }
}
