/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class DatabaseRange {

	private String name;
	private Range  range;

	private String containsHeader;
	private boolean displayFilterButtons;
	private String hasPersistentData;
	private String isSelection;
	private String onUpdateKeepSize;
	private String onUpdateKeepStyles;
	private String orientation;
	private String refreshDelay;
	private List<ElementNSImpl> childs;

	public DatabaseRange(String name) {
		this.name = name;
		this.range = new Range(null, null);
	}

	public DatabaseRange(SpreadsheetContent content, Attributes attributes) {
		name = attributes.getValue("table:name");
		if(name==null) {
			name = "__Anonymous_Sheet_DB__0";
		}
		containsHeader = attributes.getValue("table:contains-header");
		displayFilterButtons = Boolean.parseBoolean(attributes.getValue("table:display-filter-buttons"));
		hasPersistentData = attributes.getValue("table:has-persistent-data");
		isSelection = attributes.getValue("table:is-selection");
		onUpdateKeepSize = attributes.getValue("table:on-update-keep-size");
		onUpdateKeepStyles = attributes.getValue("table:on-update-keep-styles");
		orientation = attributes.getValue("table:orientation");
		refreshDelay = attributes.getValue("table:refresh-delay");
		range = new Range(content, attributes.getValue("table:target-range-address"));
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Range getRange() {
		return range;
	}

	public void setRange(Range range) {
		this.range = range;
	}

	public boolean isDisplayFilterButtons() {
		return displayFilterButtons;
	}

	public void setDisplayFilterButtons(boolean display) {
		displayFilterButtons = display;
	}

	public boolean getContainsHeader() {
		return containsHeader==null?Boolean.TRUE:containsHeader.equals("true");
	}

	public void setContainsHeader(boolean containsHeader) {
		this.containsHeader = containsHeader ? "true" : "false";
	}

	public List<ElementNSImpl> getChilds() {
		if(childs==null) {
			childs = new ArrayList<ElementNSImpl>();
		}
		return childs;
	}

	public JSONObject createTableAttrs()
		throws JSONException {

		final JSONObject tableAttrs = new JSONObject(2);
		tableAttrs.put(OCKey.HEADER_ROW.value(), getContainsHeader());
		if(!isDisplayFilterButtons()) {
		    tableAttrs.put(OCKey.HIDE_BUTTONS.value(), Boolean.TRUE);
		}
		return tableAttrs;
	}

	public void changeTableColumn(SpreadsheetContent content, int col, JSONObject attrs) {
        Node tableFilter = getFilterElementChild();
        if(tableFilter!=null) {
        	findAndRemoveColumn(tableFilter, col);
        }
		List<Object> entries = null;
        JSONObject filter = attrs.optJSONObject(OCKey.FILTER.value());
        if(filter != null) {
            String type = filter.optString(OCKey.TYPE.value());
            if( type != null && type.equals("discrete")) {
                JSONArray entriesArray = filter.optJSONArray(OCKey.ENTRIES.value());
                if(entriesArray != null && entriesArray.length() > 0) {
                    entries = new ArrayList<Object>();
                    Iterator<Object> entryIt = entriesArray.iterator();
                    while(entryIt.hasNext()) {
                        entries.add(entryIt.next().toString());
                    }
                }
            }
        }
        if(entries!=null) {
            if(tableFilter==null) {
                //insert new filter
                tableFilter = new ElementNS(content, Namespaces.TABLE, "table:filter");
                getChilds().add(0, (ElementNSImpl)tableFilter);
            }
            //insert new FilterOr with FilterCondition
            ElementNS tableFilterOr = new ElementNS(content, Namespaces.TABLE, "table:filter-or");
            tableFilter.insertBefore(tableFilterOr, null);
            ElementNS tableFilterCondition = new ElementNS(content, Namespaces.TABLE, "table:filter-condition");
            tableFilterOr.insertBefore(tableFilterCondition, null);
            tableFilterCondition.setAttributeNS(Namespaces.TABLE, "table:field-number", Integer.toString(col));
            tableFilterCondition.setAttributeNS(Namespaces.TABLE, "table:value", entries.get(0).toString() );
            tableFilterCondition.setAttributeNS(Namespaces.TABLE, "table:operator", "=" );
            ListIterator<Object> entriesIt = entries.listIterator();
            while(entriesIt.hasNext()) {
            	ElementNS filterSetItem = new ElementNS(content, Namespaces.TABLE, "table:filter-set-item");
                tableFilterCondition.insertBefore(filterSetItem, null);
                filterSetItem.setAttributeNS(Namespaces.TABLE, "table:value", entriesIt.next().toString() );
            }
        }

        handleTableSort(content, col, attrs);
	}

	private void handleTableSort(SpreadsheetContent content, int col, JSONObject attrs) {
	    JSONObject sort = attrs.optJSONObject(OCKey.SORT.value());
        if (null != sort) {
            Node sortNode = null;
            Node sortByChildNode = null;
            for (Node childNode : childs) {
                if (childNode.getLocalName().equals("sort")) {
                    // There can be only one sort element.
                    sortNode = childNode;
                    if (childNode.hasChildNodes()) {
                        for (int i=0; i<childNode.getChildNodes().getLength(); i++) {
                            Node sortByNode = childNode.getChildNodes().item(i);
                            if (sortByNode.getLocalName().equals("sort-by")) {
                                final String fieldNumber = ((ElementNSImpl)sortByNode).getAttributeNS(Namespaces.TABLE, "field-number");
                                final int columnIndex = Integer.parseInt(fieldNumber);
                                if (columnIndex == col) {
                                    sortByChildNode = sortByNode;
                                }
                            }
                        }
                    }
                    break;
                }
            }
            final String type = sort.optString(OCKey.TYPE.value(), "none");
            if(type.equals("none")) {
                if (sortNode!=null) {
                    childs.remove(sortNode);
                }
            }
            else {
                if (null == sortNode) {
                    sortNode = new ElementNS(content, Namespaces.TABLE, "table:sort");
                    getChilds().add((ElementNSImpl)sortNode);
                }

                if (null == sortByChildNode) {
                    sortByChildNode = new ElementNS(content, Namespaces.TABLE, "table:sort-by");
                    sortNode.appendChild(sortByChildNode);
                }

                ((ElementNSImpl)sortByChildNode).setAttributeNS(Namespaces.TABLE, "table:field-number", Integer.toString(col));
                String order = sort.optBoolean("descending", false) ? "descending" : "ascending";
                ((ElementNSImpl)sortByChildNode).setAttributeNS(Namespaces.TABLE, "table:order", order);
            }
        }
	}

	public void changeColumn(CellRefRange cellRefRange, int position, int change) {
        final Node tableFilter = getFilterElementChild();
        if(tableFilter!=null) {
        	changeColumnChilds(tableFilter, cellRefRange, position, change);
        }
	}

    private void changeColumnChilds(Node childNode, CellRefRange cellRefRange, int position, int change) {
    	while(childNode!=null) {
	    	Node nextChild = childNode.getNextSibling();
	    	if(childNode.getLocalName().equals("filter-condition")) {
	    		String fieldNumber = ((ElementNSImpl)childNode).getAttributeNS(Namespaces.TABLE, "field-number");
	    		if(!fieldNumber.isEmpty()) {
	    			int column = Integer.parseInt(fieldNumber);
	    			if(change>0) {
	    				if((position>cellRefRange.getStart().getColumn())&&(position<=cellRefRange.getStart().getColumn()+column)) {
	    					column+=change;
	    				}
	    			}
	    			else {
    					int delColStart = position - cellRefRange.getStart().getColumn();
    					int delColEnd = (delColStart - change) - 1;

    					if(column>=delColStart && column<=delColEnd) {
							childNode.getParentNode().removeChild(childNode);
							childNode = null;
						}
						else if (cellRefRange.getStart().getColumn() + column >= position) {
							column+=change;
    					}
	    			}
	    			if(childNode!=null) {
	    				((ElementNSImpl)childNode).setAttributeNS(Namespaces.TABLE, "table:field-number", Integer.toString(column));
	    			}
	    		}
	    	}
	    	if(childNode!=null) {
	    		final Node childChild = childNode.getFirstChild();
	        	if(childChild!=null) {
	        		changeColumnChilds(childChild, cellRefRange, position, change);
	        	}
	    	}
	    	childNode = nextChild;
    	}
    }

	private void findAndRemoveColumn(Node node, int col) {
        while(node!=null) {
        	final Node nextNode = node.getNextSibling();
        	if(node.getLocalName().equals("filter-condition")) {
	            String column = ((ElementNSImpl)node).getAttributeNS(Namespaces.TABLE, "field-number");
	            if(Integer.parseInt(column)==col) {
	            	node.getParentNode().removeChild(node);
	            }
        	}
        	else {
        		final Node child = node.getFirstChild();
        		if(child!=null) {
        			findAndRemoveColumn(child, col);
        			// the filter node itself does not have a parent, so this node remains even if all childs are deleted
        			if(!node.hasChildNodes()&&node.getParentNode()!=null) {
        				node.getParentNode().removeChild(node);
        			}
        		}
        	}
            node = nextNode;
        }
    }

    private Node getFilterElementChild() {
       	for(ElementNSImpl e:getChilds()) {
       		if(e.getLocalName().equals("filter")) {
       			return e;
       		}
       	}
       	return null;
    }

	public void writeObject(SerializationHandler output, SpreadsheetContent content)
		throws SAXException {

    	SaxContextHandler.startElement(output, Namespaces.TABLE, "database-range", "table:database-range");
    	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "name", "table:name", name);
    	if(containsHeader!=null) {
        	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "contains-header", "table:contains-header", containsHeader);
    	}
    	if(displayFilterButtons) {
    		SaxContextHandler.addAttribute(output, Namespaces.TABLE, "display-filter-buttons", "table:display-filter-buttons", "true");
    	}
    	if(hasPersistentData!=null) {
        	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "has-persistent-data", "table:has-persistent-data", hasPersistentData);
    	}
    	if(isSelection!=null) {
        	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "is-selection", "table:is-selection", isSelection);
    	}
    	if(onUpdateKeepSize!=null) {
    		SaxContextHandler.addAttribute(output, Namespaces.TABLE, "on-update-keep-size", "table:on-update-keep-size", onUpdateKeepSize);
    	}
    	if(onUpdateKeepStyles!=null) {
    		SaxContextHandler.addAttribute(output, Namespaces.TABLE, "on-update-keep-styles", "table:on-update-keep-styles", onUpdateKeepStyles);
    	}
    	if(orientation!=null) {
    		SaxContextHandler.addAttribute(output, Namespaces.TABLE, "orientation", "table:orientation", orientation);
    	}
    	if(refreshDelay!=null) {
        	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "refresh-delay", "table:refresh-delay", refreshDelay);
    	}
    	if(range!=null) {
        	SaxContextHandler.addAttribute(output, Namespaces.TABLE, "target-range-address", "table:target-range-address", range.convertToString(content, true));
    	}
    	if(childs!=null) {
    		for(ElementNSImpl child:childs) {
    			SaxContextHandler.serializeElement(output, child);
    		}
    	}
    	SaxContextHandler.endElement(output, Namespaces.TABLE, "database-range", "table:database-range");
	}
}
