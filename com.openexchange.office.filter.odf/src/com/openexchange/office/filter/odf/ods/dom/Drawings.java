/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.INodeAccessor;
import com.openexchange.office.filter.odf.Namespaces;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.SaxContextHandler;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawObject;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.ods.dom.chart.ChartContent;

public class Drawings implements INodeAccessor {

	// each drawing of a page
    private DLList<Drawing> indexedDrawings;
	// each drawing that is anchored to a cell (top left anchor position is hashed)
	private final HashMap<DrawingAnchor, List<Drawing>> anchoredDrawings;

	public Drawings() {
		indexedDrawings = new DLList<Drawing>();
		anchoredDrawings = new HashMap<DrawingAnchor, List<Drawing>>();
    }

	public void writeSheetDrawings(SerializationHandler output)
		throws SAXException {

	    int i = 0;

	    final Iterator<Drawing> drawingIter = indexedDrawings.iterator();
	    while(drawingIter.hasNext()) {
	        final Drawing drawing = drawingIter.next();
	        drawing.getAttributes().setIntValue(Namespaces.DRAW, "z-index", "draw:z-index", i++);

			// only writing drawings without anchor...
			if(drawing.getAnchor()==null) {
				SaxContextHandler.startElement(output, Namespaces.TABLE, "shapes", "table:shapes");
				drawing.writeObject(output);
				SaxContextHandler.endElement(output, Namespaces.TABLE, "shapes", "table:shapes");
			}
		}
	}

    @Override
    public DLList<Object> getContent() {
        return (DLList)indexedDrawings;
    }

	public int getCount() {
		return indexedDrawings.size();
	}

	public Drawing getDrawing(int index) {
		return indexedDrawings.get(index);
	}

	public void addDrawing(Drawing drawing) {
	    addDrawingNode(new DLNode<Object>(drawing), null);
    }

    public void addDrawingNode(DLNode<Object> drawingNode, DLNode<Object> referenceNode) {
        ((DLList)indexedDrawings).addNode(referenceNode, drawingNode, true);

        final Drawing drawing = (Drawing)drawingNode.getData();
		final DrawingAnchor anchor = drawing.getAnchor();
		if(anchor!=null) {
			List<Drawing> anchoredDrawingsList = anchoredDrawings.get(anchor);
			if(anchoredDrawingsList==null) {
				anchoredDrawingsList = new ArrayList<Drawing>();
				anchoredDrawings.put(anchor, anchoredDrawingsList);
			}
			anchoredDrawingsList.add(drawing);
		}
	}

    public void moveDrawing(int from, int to) {
        Drawing drawing = indexedDrawings.remove(from);
        indexedDrawings.add(to, drawing);
    }

	public void setDrawingAnchor(DrawingAnchor newAnchor, Drawing drawing) {

		// first removing the old anchor if any
		if(drawing.getAnchor()!=null) {
			final List<Drawing> anchoredDrawingList = anchoredDrawings.get(drawing.getAnchor());
			anchoredDrawingList.remove(drawing);
			if(anchoredDrawingList.isEmpty()) {
				anchoredDrawings.remove(drawing.getAnchor());
			}
 		}
		// adding our new drawingAnchor
		drawing.setDrawingAnchor(newAnchor);
		if(newAnchor!=null) {
		    List<Drawing> anchoredDrawingsList = anchoredDrawings.get(newAnchor);
			if(anchoredDrawingsList==null) {
				anchoredDrawingsList = new ArrayList<Drawing>();
				anchoredDrawings.put(newAnchor, anchoredDrawingsList);
			}
			anchoredDrawingsList.add(drawing);
		}
	}

	public List<Drawing> getIndexedDrawings() {
	    return indexedDrawings;
	}

	public List<Drawing> getAnchoredDrawings(DrawingAnchor drawingAnchor) {
		return anchoredDrawings.get(drawingAnchor);
	}

    public void deleteDrawing(int index, OdfOperationDoc opsDoc) {
		final Drawing drawing = indexedDrawings.remove(index);
		if(drawing.getAnchor()!=null) {
			final List<Drawing> anchoredDrawingList = anchoredDrawings.get(drawing.getAnchor());
			anchoredDrawingList.remove(drawing);
			if(anchoredDrawingList.isEmpty()) {
				anchoredDrawings.remove(drawing.getAnchor());
			}
		}
		if(drawing.getType()==DrawingType.CHART) {
		    final ChartContent chart = ((DrawObject)((DrawFrame)drawing.getShape()).getDrawing()).getChart();
	        if (chart!=null) {
	            deleteChartPart(opsDoc.getDocument().getPackage(), chart);
	        }
		}
	}

    public static void deleteChartPart(OdfPackage pkg, ChartContent chart) {
        final String internalPath = chart.getDocument().getDocumentPath();
        pkg.remove(internalPath + "content.xml");
        pkg.remove(internalPath + "meta.xml");
        pkg.remove(internalPath + "styles.xml");
        pkg.removeDocument(internalPath);
    }
}
