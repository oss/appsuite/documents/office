/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom.chart;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.chart.ChartSeries;
import com.openexchange.office.filter.odf.AttributesImpl;
import com.openexchange.office.filter.odf.DOMBuilder;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class ChartSeriesHandler extends DOMBuilder {

    private final ChartContentHandler parentContext;
    private final ElementNS seriesEl;

    public ChartSeriesHandler(ChartContentHandler parentContext, ElementNS seriesEl) {
        super(seriesEl, parentContext);
        this.parentContext = parentContext;
        this.seriesEl = seriesEl;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        OdfOperationDoc.abortOnLowMemory(getFileDom());

        AttributesImpl attrs = new AttributesImpl(attributes);

        ChartSeries series = (ChartSeries) seriesEl.getUserData(ChartContent.USERDATA);

        switch (qName) {
            case "chart:data-point":

                Integer repeated = attrs.getIntValue("chart:repeated");
                if (repeated == null) {
                    repeated = 1;
                }

                String styleName = attrs.getValue(ChartContent.ODF_STYLE);

                JSONObject dataPointStyle = null;
                if (StringUtils.isNotEmpty(styleName)) {
                    dataPointStyle = parentContext.getChart().getStyleAttrs(styleName);
                    if (dataPointStyle.isEmpty()) {
                        dataPointStyle = null;
                    } else {
                        dataPointStyle.putSafe(OCKey.MARKER_FILL.value(), dataPointStyle.optJSONObject(OCKey.FILL.value()));
                        dataPointStyle.putSafe(OCKey.MARKER_BORDER.value(), dataPointStyle.optJSONObject(OCKey.FILL.value()));
                    }
                }
                if (dataPointStyle == null || dataPointStyle.isEmpty()) {
                    dataPointStyle = new JSONObject();
                }

                String position = null;
                final JSONObject seriesJSON = dataPointStyle.optJSONObject(OCKey.SERIES.value());
                if (seriesJSON != null) {
                    final JSONObject dataLabel = seriesJSON.optJSONObject(OCKey.DATA_LABEL.value());
                    if (dataLabel != null) {
                        position = dataLabel.optString(OCKey.DATA_LABEL_POS.value(), null);
                        dataPointStyle.remove(OCKey.DATA_LABELS.value());
                    }
                    dataPointStyle.remove(OCKey.SERIES.value());
                }
                if (position != null) {
                    series.addLabelPointPosition(position);
                }

                if (repeated > 1) {
                    dataPointStyle.putSafe(OCKey.REPEATED.value(), repeated);
                }
                series.addDataPointStyle(dataPointStyle);

                seriesEl.appendChild(new ElementNS(getFileDom(), attributes, uri, qName));

                return this;

            case "chart:domain":
                String values = ChartContent.getFormula(attrs.getValue(ChartContent.ODF_TABLE_CELL_RANGE));

                if (series.isScatter() || series.getSeriesJSON().has(OCKey.BUBBLES.value())) {
                    series.setNames(values);
                } else {
                    series.setBubbles(values);
                }
                seriesEl.appendChild(new ElementNS(getFileDom(), attributes, uri, qName));

                return this;
                
            case "chart:error-indicator":
                
                return super.startElement(attributes, uri, localName, qName);
            default:
                return this;
        }
    }

}
