/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.ods.dom.components;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.DLList;
import com.openexchange.office.filter.core.DLNode;
import com.openexchange.office.filter.core.component.ComponentContext;
import com.openexchange.office.filter.core.component.ComponentType;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.OpAttrs;
import com.openexchange.office.filter.odf.components.OdfComponent;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.draw.DrawingType;
import com.openexchange.office.filter.odf.draw.IDrawing;
import com.openexchange.office.filter.odf.draw.IDrawingType;
import com.openexchange.office.filter.odf.odt.dom.Paragraph;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleParagraph;

public class FrameComponent extends OdfComponent implements IDrawingType {

    final DrawFrame drawFrame;

    public FrameComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> textFrameNode, int componentNumber) {
        super(parentContext, textFrameNode, componentNumber);
        drawFrame = (DrawFrame)getObject();
    }

    @Override
    public String simpleName() {
        return "Frame";
    }

    public DrawFrame getDrawFrame() {
        return drawFrame;
    }

    @Override
    public DrawingType getType() {
        return drawFrame.getType();
    }

    @Override
    public IComponent<OdfOperationDoc> getNextChildComponent(ComponentContext<OdfOperationDoc> previousChildContext, IComponent<OdfOperationDoc> previousChildComponent) {
        final IDrawing iDrawing = drawFrame.getDrawing();
        if(iDrawing!=null) {
            final int nextComponentNumber = previousChildComponent != null ? previousChildComponent.getNextComponentNumber() : 0;
            DLNode<Object> nextNode = previousChildContext!=null ? previousChildContext.getNode().getNext() : iDrawing.getContent().getFirstNode();
            while(nextNode!=null) {
                if(nextNode.getData() instanceof Paragraph) {
                    return new ParagraphComponent(this, nextNode, nextComponentNumber);
                }
                nextNode = nextNode.getNext();
            }
        }
        return null;
    }

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(ComponentContext<OdfOperationDoc> parentContext, DLNode<Object> contextNode, int number, IComponent<OdfOperationDoc> child, ComponentType type, JSONObject attrs) {
        final IDrawing iDrawing = drawFrame.getDrawing();
        if(iDrawing!=null) {
            DLList<Object> DLList = iDrawing.getContent();
            DLNode<Object> referenceNode = child != null && child.getComponentNumber()== number ? child.getNode() : null;

            switch(type) {
                case PARAGRAPH : {
                    final DLNode<Object> newParagraphNode = new DLNode<Object>(new Paragraph(null));
                    DLList.addNode(referenceNode, newParagraphNode, true);
                    return new ParagraphComponent(parentContext, newParagraphNode, number);
                }
                default : {
                    throw new UnsupportedOperationException();
                }
            }
        }
        return null;
    }

    @Override
    public IComponent<OdfOperationDoc> insertChildComponent(int number, JSONObject attrs, ComponentType type) throws Exception {

        final IComponent<OdfOperationDoc> c = insertChildComponent(this, getNode(), number, getChildComponent(number), type, attrs);
        if(c.getComponentNumber()==0&&c instanceof ParagraphComponent) {
            // inheriting paragraph attributes from FrameStyle
            final StyleManager styleManager = operationDocument.getDocument().getStyleManager();
            final StyleParagraph paragraphStyle = new StyleParagraph(styleManager.getUniqueStyleName(StyleFamily.PARAGRAPH, isContentAutoStyle()), true, isContentAutoStyle());

            final OpAttrs opAttrs = new OpAttrs();
            styleManager.collectAllParagraphPropertiesFromStyle(opAttrs, drawFrame.getStyleName(), StyleFamily.GRAPHIC, isContentAutoStyle());
            styleManager.collectAllTextPropertiesFromStyle(opAttrs, drawFrame.getStyleName(), StyleFamily.GRAPHIC, isContentAutoStyle());

            // taking care of frame margins, they have to be applied to the paragraph style :-(
            final OpAttrs allDrawingAttrs = new OpAttrs();
            styleManager.collectAllGraphicPropertiesFromStyle(allDrawingAttrs, drawFrame.getStyleName(), StyleFamily.GRAPHIC, isContentAutoStyle(), false);
            final Map<String, Object> drawingAttrs = allDrawingAttrs.getMap(OCKey.DRAWING.value(), false);
            if(drawingAttrs!=null) {
                final Map<String, Object> paragraphAttrs = opAttrs.getMap(OCKey.PARAGRAPH.value(), true);
                final Object marginLeft = drawingAttrs.get(OCKey.MARGIN_LEFT.value());
                final Object marginTop = drawingAttrs.get(OCKey.MARGIN_TOP.value());
                final Object marginRight = drawingAttrs.get(OCKey.MARGIN_RIGHT.value());
                final Object marginBottom = drawingAttrs.get(OCKey.MARGIN_BOTTOM.value());
                if(marginLeft!=null) {
                    paragraphAttrs.put(OCKey.MARGIN_LEFT.value(), marginLeft);
                }
                if(marginTop!=null) {
                    paragraphAttrs.put(OCKey.MARGIN_TOP.value(), marginTop);
                }
                if(marginRight!=null) {
                    paragraphAttrs.put(OCKey.MARGIN_RIGHT.value(), marginRight);
                }
                if(marginBottom!=null) {
                    paragraphAttrs.put(OCKey.MARGIN_BOTTOM.value(), marginBottom);
                }
            }
            if(attrs!=null) {
                StyleManager.deepCopy(attrs.asMap(), opAttrs);
            }
            paragraphStyle.applyAttrs(styleManager, new JSONObject(opAttrs));
            final String existingStyleId = styleManager.getExistingStyleIdForStyleBase(paragraphStyle);
            if(existingStyleId!=null) {
                ((Paragraph)((ParagraphComponent)c).getObject()).setStyleName(existingStyleId);
            }
            else {
                styleManager.addStyle(paragraphStyle);
                ((Paragraph)((ParagraphComponent)c).getObject()).setStyleName(paragraphStyle.getName());
            }
        }
        else if(attrs!=null) {
            c.applyAttrsFromJSON(attrs);
        }
        return c;
    }

    @Override
    public void applyAttrsFromJSON(JSONObject attrs)
            throws JSONException {

        drawFrame.applyAttrsFromJSON(operationDocument, attrs, isContentAutoStyle());
    }

    @Override
    public void createJSONAttrs(OpAttrs attrs) {

        drawFrame.createAttrs(operationDocument, attrs, isContentAutoStyle());
    }
}
