/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.dom.OdfDocumentNamespace;
import org.odftoolkit.odfdom.dom.OdfSchemaDocument;
import org.odftoolkit.odfdom.pkg.NamespaceName;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.odf.styles.AutomaticStyles;
import com.openexchange.office.filter.odf.styles.DocumentStyles;
import com.openexchange.office.filter.odf.styles.FontFaceDecls;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.Styles;

@SuppressWarnings("serial")
public class SpreadsheetStyles extends DocumentStyles {

    public SpreadsheetStyles(OdfDocument odfDocument, String packagePath) throws SAXException {
        super(odfDocument, packagePath);
    }

    @Override
    protected void initialize() throws SAXException {
        for (NamespaceName name : OdfDocumentNamespace.values()) {
            mUriByPrefix.put(name.getPrefix(), name.getUri());
            mPrefixByUri.put(name.getUri(), name.getPrefix());
        }
        final StyleManager styleManager = getDocument().getStyleManager();
        styleManager.setStyles(new Styles(this));
        styleManager.setMasterStyles(new SpreadsheetMasterStyles(this));
        styleManager.setAutomaticStyles(new AutomaticStyles(this), false);
        styleManager.setFontFaceDecls(new FontFaceDecls(this), false);

        final XMLReader xmlReader = mPackage.getXMLReader();
        super.initialize(new SpreadsheetStylesHandler(this, xmlReader), xmlReader);

        final Node root = getRootElement();
        root.appendChild(styleManager.getFontFaceDecls(false));
        root.appendChild(styleManager.getStyles());
        root.appendChild(styleManager.getAutomaticStyles(false));
        root.appendChild(styleManager.getMasterStyles());
    }

    @Override
    public OdfSchemaDocument getDocument() {
        return (OdfSchemaDocument) mPackageDocument;
    }
}
