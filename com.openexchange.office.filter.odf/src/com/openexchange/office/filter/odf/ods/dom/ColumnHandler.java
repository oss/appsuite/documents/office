/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import org.xml.sax.Attributes;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class ColumnHandler extends SaxContextHandler {

    public ColumnHandler(SheetHandler sheetHandler, Attributes attributes, Column col) {
        super(sheetHandler);
        for(int i=0; i<attributes.getLength(); i++) {
            String localName = attributes.getLocalName(i);
            if(localName.equals("number-columns-repeated")) {
                col.setMax(col.getMin()+(Integer.valueOf(attributes.getValue(i))-1));
            }
            else if(localName.equals("default-cell-style-name")) {
                col.setDefaultCellStyle(attributes.getValue(i));
            }
            else if(localName.equals("visibility")) {
                col.setVisibility(Visibility.enumValueOf(attributes.getValue(i)));
            }
            else if(localName.equals("id")) {
                col.setId(attributes.getValue(i));
            }
        }

        // split first column from the column range, if it starts a new column group ("collapse" must be located on the leading column only)
        if ((col.getMin() < col.getMax()) && (col.getGroupCollapse() != null)) {
            Column startCol = col.splitBefore(col.getMin() + 1);
            sheetHandler.getSheet().getColumns().add(startCol);
            col.setGroupCollapse(null);
        }

        // set the maximum allowed number of columns to 16384 if a column greater than 1024 is used
        if(col.getMax()>=1024) {
            ((SpreadsheetContent)sheetHandler.getFileDom()).setMaxColumnCount(16384);
        }
    }
}
