/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom;

import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.spreadsheet.CellRefRange;

@SuppressWarnings("serial")
public class Ranges extends ArrayList<Range> {

	private Ranges(Ranges source) throws CloneNotSupportedException {
		for(Range range : source) {
			add(range.clone());
		}
	}

	public Ranges(SpreadsheetContent content, String rangeAddress) {
		super();

		if(rangeAddress!=null) {
			boolean encoded = false;
			Integer firstIndex = null;
			for(int index = 0; index >=0 && index<rangeAddress.length(); index++) {
				final char c = rangeAddress.charAt(index);
				if(c==' ') {
					if(encoded||firstIndex==null) {
						continue;
					}
					add(new Range(content, rangeAddress.substring(firstIndex, index)));
					firstIndex = null;
					continue;
				}
				if(firstIndex==null) {
					firstIndex = Integer.valueOf(index);
				}
				if(c=='\'') {
					if(encoded==false) {
						encoded = true;
					}
					else {
						encoded = false;
					}
					continue;
				}
				if(index+1==rangeAddress.length()) {
					add(new Range(content, rangeAddress.substring(firstIndex, index+1)));
				}
			}
		}
		if(isEmpty()) {
			add(new Range(null, null));
		}
	}

	public Ranges(int sheetIndex, JSONArray rangeAddress) {
		super();
		if(rangeAddress!=null&&!rangeAddress.isEmpty()) {
			final Iterator<Object> rangeIter = rangeAddress.iterator();
			while(rangeIter.hasNext()) {
				final Object o = rangeIter.next();
				if(o instanceof JSONObject) {
					final JSONArray start = ((JSONObject)o).optJSONArray(OCKey.START.value());
					if(start!=null) {
						final JSONArray end = ((JSONObject)o).optJSONArray(OCKey.END.value());
						add(new Range(sheetIndex, start, end==null ? start : end));
					}
				}
			}
		}
		else {
			add(new Range(null, null));
		}
	}

	public String convertToString(SpreadsheetContent content, boolean createSheetRef) {
		final StringBuffer buffer = new StringBuffer();
		for(Range range:this) {
			if(buffer.length()!=0) {
				buffer.append(' ');
			}
			buffer.append(range.convertToString(content, createSheetRef));
		}
		return buffer.toString();
	}

	public JSONArray getJSON()
		throws JSONException {

		final JSONArray jsonRanges = new JSONArray(size());
		for(Range range:this) {
			final JSONObject jsonRange = new JSONObject(2);
			final CellRefRange cellRefRange = range.getCellRefRange(true);
			jsonRange.put(OCKey.START.value(), cellRefRange.getStart().getJSONArray());
			jsonRange.put(OCKey.END.value(), cellRefRange.getEnd().getJSONArray());
			jsonRanges.put(jsonRange);
		}
		return jsonRanges;
	}

	@Override
	public Ranges clone() {
		try {
			return new Ranges(this);
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

}
