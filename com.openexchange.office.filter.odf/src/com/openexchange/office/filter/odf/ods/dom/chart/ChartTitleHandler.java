/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf.ods.dom.chart;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.core.AttributeSet;
import com.openexchange.office.filter.odf.ElementNS;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.SaxContextHandler;

public class ChartTitleHandler extends SaxContextHandler {

    private final ChartContentHandler parentContext;
    private final ElementNS titleEl;
    private final Integer axis;

    public ChartTitleHandler(ChartContentHandler parentContext, Integer axis, ElementNS titleEl) {
        super(parentContext);
        this.parentContext = parentContext;
        this.titleEl = titleEl;
        this.axis = axis;
    }

    @Override
    public SaxContextHandler startElement(Attributes attributes, String uri, String localName, String qName) {
        OdfOperationDoc.abortOnLowMemory(getFileDom());
        return this;
    }

    @Override
    public void endContext(String qName, String characters) {
        if (qName.equals("text:p")) {
            AttributeSet title = (AttributeSet) titleEl.getUserData(ChartContent.USERDATA);
            JSONObject text = title.getOrCreate(OCKey.TEXT.value());
            JSONArray link = new JSONArray();
            link.put(characters.replaceAll("\n", ""));
            text.putSafe(OCKey.LINK.value(), link);

            parentContext.getChart().changeTitle(axis, title.getAttributes());
        }
        super.endContext(qName, characters);
    }

}
