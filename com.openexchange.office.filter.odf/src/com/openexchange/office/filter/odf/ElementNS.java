/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.odf;

import org.apache.xerces.dom.AttributeMap;
import org.apache.xerces.dom.CoreDocumentImpl;
import org.apache.xerces.dom.ElementNSImpl;
import org.odftoolkit.odfdom.Names;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.odftoolkit.odfdom.pkg.OdfNamespace;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;

public class ElementNS extends ElementNSImpl {

    public ElementNS(CoreDocumentImpl ownerDocument, String namespaceURI, String qualifiedName) {
        super(ownerDocument, namespaceURI, qualifiedName);
    }

    public ElementNS(OdfFileDom ownerDocument, Attributes attributes, String namespaceURI, String qualifiedName) {
        super(ownerDocument, namespaceURI, qualifiedName);
        addAttributes(attributes);
    }

    @Override
    public ElementNS cloneNode(boolean deep) {
        ElementNS clone = (ElementNS) super.cloneNode(deep);
        if (attributes != null) {
            clone.attributes = (AttributeMap) attributes.cloneMap(clone);
        }
        return clone;
    }

    public void addAttributes(Attributes attributes) {
        addAttributes(this, attributes);
    }

    public static Node addAttributes(Element element, Attributes attributes) {
        String attrQname;
        String attrURL;
        String attrLocalName;
        String attrValue;

        for (int i = 0; i < attributes.getLength(); i++) {
            attrURL = attributes.getURI(i);
            attrQname = attributes.getQName(i);
            attrLocalName = attributes.getLocalName(i);
            attrValue = attributes.getValue(i);

            setAttribute(element, attrURL, attrQname, attrLocalName, attrValue);
        }
        return element;
    }

    public static void setAttribute(Element element, String attrURL, String attrQname, String attrLocalName, String attrValue) {
        Attr attr;
        final OdfFileDom odfFileDom = (OdfFileDom) element.getOwnerDocument();

        // if no namespace exists
        if (attrURL.equals(Names.EMPTY_STRING) || attrQname.equals(Names.EMPTY_STRING)) {
            // create attribute without prefix
            attr = odfFileDom.createAttribute(attrLocalName);
        } else {
            if (attrQname.startsWith("xmlns:")) {
                // in case of xmlns prefix we have to create a new OdfNamespace
                OdfNamespace namespace = odfFileDom.setNamespace(attrLocalName, attrValue);
                // if the file Dom is already associated to parsed XML addChild the new namespace to the root element
                Element root = odfFileDom.getRootElement();
                if (root == null) {
                    root = element;
                }
                root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:" + namespace.getPrefix(), namespace.getUri());
            }
            // create all attributes, even namespace attributes
            attr = odfFileDom.createAttributeNS(attrURL, attrQname);
        }
        // namespace attributes will not be created and return null
        if (attr != null) {
            element.setAttributeNodeNS(attr);
            // don't exit because of invalid attribute values
            try {
                // set Value in the attribute to allow validation in the attribute
                attr.setValue(attrValue);
            } // if we detect an attribute with invalid value: remove attribute node
            catch (IllegalArgumentException e) {
                element.removeAttributeNode(attr);
            }
        }
    }
}
