

package org.odftoolkit.odfdom;

import org.apache.xml.serializer.SerializationHandler;
import org.xml.sax.SAXException;


public interface IElementWriter {
	
	void writeObject(SerializationHandler output)
		throws SAXException;
}
