

/**
 * **********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * Copyright 2008, 2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0. You can also
 * obtain a copy of the License at http://odftoolkit.org/docs/license.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***********************************************************************
 */
package org.odftoolkit.odfdom.doc;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.odftoolkit.odfdom.dom.OdfSchemaConstraint;
import org.odftoolkit.odfdom.dom.OdfSchemaDocument;
import org.odftoolkit.odfdom.pkg.MediaType;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.odftoolkit.odfdom.pkg.OdfValidationException;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.NotSupportedMediaTypeException;
import com.openexchange.office.filter.odf.MetaData;
import com.openexchange.office.filter.odf.OdfOperationDoc;

/**
 * This abstract class is representing one of the possible ODF documents.
 *
 */
public abstract class OdfDocument extends OdfSchemaDocument {
	// Static parts of file references

	private static final String SLASH = "/";
	private OdfMediaType mMediaType;
	private static final Pattern CONTROL_CHAR_PATTERN = Pattern.compile("\\p{Cntrl}");
	private static final String EMPTY_STRING = "";

	// Using static factory instead of constructor
	protected OdfDocument(OdfPackage pkg, String internalPath, OdfMediaType mediaType) {
		super(pkg, internalPath, mediaType.getMediaTypeString());
		mMediaType = mediaType;
	}

	/**
	 * This enum contains all possible media types of OpenDocument documents.
	 */
	public enum OdfMediaType implements MediaType {

		CHART("application/vnd.oasis.opendocument.chart", "odc"),
		CHART_TEMPLATE("application/vnd.oasis.opendocument.chart-template", "otc"),
		FORMULA("application/vnd.oasis.opendocument.formula", "odf"),
		FORMULA_TEMPLATE("application/vnd.oasis.opendocument.formula-template", "otf"),
		DATABASE_FRONT_END("application/vnd.oasis.opendocument.base", "odb"),
		GRAPHICS("application/vnd.oasis.opendocument.graphics", "odg"),
		GRAPHICS_TEMPLATE("application/vnd.oasis.opendocument.graphics-template", "otg"),
		IMAGE("application/vnd.oasis.opendocument.image", "odi"),
		IMAGE_TEMPLATE("application/vnd.oasis.opendocument.image-template", "oti"),
		PRESENTATION("application/vnd.oasis.opendocument.presentation", "odp"),
		PRESENTATION_TEMPLATE("application/vnd.oasis.opendocument.presentation-template", "otp"),
		SPREADSHEET("application/vnd.oasis.opendocument.spreadsheet", "ods"),
		SPREADSHEET_TEMPLATE("application/vnd.oasis.opendocument.spreadsheet-template", "ots"),
		TEXT("application/vnd.oasis.opendocument.text", "odt"),
		TEXT_MASTER("application/vnd.oasis.opendocument.text-master", "odm"),
		TEXT_TEMPLATE("application/vnd.oasis.opendocument.text-template", "ott"),
		TEXT_WEB("application/vnd.oasis.opendocument.text-web", "oth");
		private final String mMediaType;
		private final String mSuffix;

		OdfMediaType(String mediaType, String suffix) {
			this.mMediaType = mediaType;
			this.mSuffix = suffix;
		}

		/**
		 * @return the mediatype String of this document
		 */
		@Override
        public String getMediaTypeString() {
			return mMediaType;
		}

		/**
		 * @return the ODF filesuffix of this document
		 */
		@Override
        public String getSuffix() {
			return mSuffix;
		}

		/**
		 *
		 * @param mediaType string defining an ODF document
		 * @return the according OdfMediatype encapuslating the given string and
		 * the suffix
		 */
		public static OdfMediaType getOdfMediaType(String mediaType) {
			OdfMediaType odfMediaType = null;
			if (mediaType != null) {

				String mediaTypeShort = mediaType.substring(mediaType.lastIndexOf(".") + 1, mediaType.length());
				mediaTypeShort = mediaTypeShort.replace('-', '_').toUpperCase();
				try {
					odfMediaType = OdfMediaType.valueOf(mediaTypeShort);

				} catch (IllegalArgumentException e) {
					throw new IllegalArgumentException("Given mediaType '" + mediaType + "' is not an ODF mediatype!");
				}
			}
			return odfMediaType;
		}
	}

	/**
	 * Loads the ODF root document from the given Resource.
	 *
	 * NOTE: Initial meta data (like the document creation time) will be added
	 * in this method.
	 *
	 * @param res a resource containing a package with a root document
	 * @param odfMediaType the media type of the root document
	 * @return the OpenDocument document or NULL if the media type is not
	 * supported by ODFDOM.
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	protected static OdfDocument loadTemplate(Resource res, OdfMediaType odfMediaType) throws Exception {
		InputStream in = res.createInputStream();
		OdfPackage pkg = null;
		try {
			pkg = OdfPackage.loadPackage(in);
		} finally {
			in.close();
		}
		OdfDocument newDocument = newDocument(pkg, ROOT_DOCUMENT_PATH, odfMediaType);
		//add creation time, the metadata have to be explicitly set
		return newDocument;
	}

	/**
	 * Loads the ODF root document from the ODF package provided by its path.
	 *
	 * <p>OdfDocument relies on the file being available for read access over
	 * the whole lifecycle of OdfDocument.</p>
	 *
	 * @param documentPath - the path from where the document can be loaded
	 * @return the OpenDocument from the given path or NULL if the media type is
	 * not supported by ODFDOM.
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfDocument loadDocument(String documentPath) throws Exception {
		return loadDocument(OdfPackage.loadPackage(documentPath));
	}



	/**
	 * Loads the ODF root document from the ODF package provided by a Stream.
	 *
	 * <p>Since an InputStream does not provide the arbitrary (non sequentiell)
	 * read access needed by OdfDocument, the InputStream is cached. This
	 * usually takes more time compared to the other createInternalDocument
	 * methods. An advantage of caching is that there are no problems
	 * overwriting an input file.</p>
	 *
	 * @param inStream - the InputStream of the ODF document.
	 * @param configuration - key/value pairs of user given run-time settings (configuration)
	 * @return the document created from the given InputStream
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfDocument loadDocument(InputStream inStream, Map<String, Object> configuration) throws Exception {
		return loadDocument(OdfPackage.loadPackage(inStream, configuration));
	}


	/**
	 * Loads the ODF root document from the ODF package provided by a Stream.
	 *
	 * <p>Since an InputStream does not provide the arbitrary (non sequentiell)
	 * read access needed by OdfDocument, the InputStream is cached. This
	 * usually takes more time compared to the other createInternalDocument
	 * methods. An advantage of caching is that there are no problems
	 * overwriting an input file.</p>
	 *
	 * @param inStream - the InputStream of the ODF document.
	 * @return the document created from the given InputStream
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfDocument loadDocument(InputStream inStream) throws Exception {
		return loadDocument(OdfPackage.loadPackage(inStream));
	}

	/**
	 * Loads the ODF root document from the ODF package provided as a File.
	 *
	 * @param file - a file representing the ODF document.
	 * @return the document created from the given File
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfDocument loadDocument(File file) throws Exception {
		return loadDocument(OdfPackage.loadPackage(file));
	}

	/**
	 * Loads the ODF root document from the ODF package.
	 *
	 * @param odfPackage - the ODF package containing the ODF document.
	 * @return the root document of the given OdfPackage
	 * @throws java.lang.Exception - if the ODF document could not be created.
	 */
	public static OdfDocument loadDocument(OdfPackage odfPackage) throws Exception {
		return loadDocument(odfPackage, ROOT_DOCUMENT_PATH);
	}

	/**
	 * Creates an OdfDocument from the OpenDocument provided by an ODF package.
	 *
	 * @param odfPackage - the ODF package containing the ODF document.
	 * @param internalPath - the path to the ODF document relative to the
	 * package root, or an empty String for the root document.
	 * @return the root document of the given OdfPackage
	 * @throws java.lang.Exception - if the ODF document could not be created.
	 */
	public static OdfDocument loadDocument(OdfPackage odfPackage, String internalPath) throws Exception {
		String documentMediaType = odfPackage.getMediaTypeString(internalPath);
		OdfMediaType odfMediaType = null;
		try {
			odfMediaType = OdfMediaType.getOdfMediaType(documentMediaType);
		} catch (IllegalArgumentException e) {
			// the returned NULL will be taking care of afterwards
		}
		if (odfMediaType == null) {
			if (documentMediaType != null) {
				Matcher matcherCTRL = CONTROL_CHAR_PATTERN.matcher(documentMediaType);
				if (matcherCTRL.find()) {
					documentMediaType = matcherCTRL.replaceAll(EMPTY_STRING);
				}
			}
			throw new OdfValidationException(OdfSchemaConstraint.DOCUMENT_WITHOUT_ODF_MIMETYPE, internalPath, documentMediaType);
		}
		return newDocument(odfPackage, internalPath, odfMediaType);
	}

	//return null if the media type can not be recognized.
	public static OdfDocument loadDocumentFromTemplate(OdfMediaType odfMediaType) throws Exception {

		final Resource documentTemplate;
		switch (odfMediaType) {
			case TEXT:
			case TEXT_TEMPLATE:
			case TEXT_MASTER:
			case TEXT_WEB:
				documentTemplate = OdfTextDocument.EMPTY_TEXT_DOCUMENT_RESOURCE;
				break;

			case SPREADSHEET:
			case SPREADSHEET_TEMPLATE:
				documentTemplate = OdfSpreadsheetDocument.EMPTY_SPREADSHEET_DOCUMENT_RESOURCE;
				break;

			case PRESENTATION:
			case PRESENTATION_TEMPLATE:
				documentTemplate = OdfPresentationDocument.EMPTY_PRESENTATION_DOCUMENT_RESOURCE;
				break;

			default:
				throw new IllegalArgumentException("Given mediaType '" + odfMediaType.mMediaType + "' is not yet supported!");
		}
		return loadTemplate(documentTemplate, odfMediaType);
	}

	/**
	 * Creates one of the ODF documents based a given mediatype.
	 *
	 * @param odfMediaType The ODF Mediatype of the ODF document to be created.
	 * @return The ODF document, which mediatype dependends on the parameter or
	 * NULL if media type were not supported.
	 */
	private static OdfDocument newDocument(OdfPackage pkg, String internalPath, OdfMediaType odfMediaType) throws SAXException {
		OdfDocument newDoc = null;
		switch (odfMediaType) {
			case TEXT:
				newDoc = new OdfTextDocument(pkg, internalPath, OdfTextDocument.OdfMediaType.TEXT);
				break;

			case TEXT_TEMPLATE:
				newDoc = new OdfTextDocument(pkg, internalPath, OdfTextDocument.OdfMediaType.TEXT_TEMPLATE);
				break;

			case TEXT_MASTER:
				newDoc = new OdfTextDocument(pkg, internalPath, OdfTextDocument.OdfMediaType.TEXT_MASTER);
				break;

			case TEXT_WEB:
				newDoc = new OdfTextDocument(pkg, internalPath, OdfTextDocument.OdfMediaType.TEXT_WEB);
				break;

			case SPREADSHEET:
				newDoc = new OdfSpreadsheetDocument(pkg, internalPath, OdfSpreadsheetDocument.OdfMediaType.SPREADSHEET);
				break;

			case SPREADSHEET_TEMPLATE:
				newDoc = new OdfSpreadsheetDocument(pkg, internalPath, OdfSpreadsheetDocument.OdfMediaType.SPREADSHEET_TEMPLATE);
				break;

			case PRESENTATION:
				newDoc = new OdfPresentationDocument(pkg, internalPath, OdfPresentationDocument.OdfMediaType.PRESENTATION);
				break;

			case PRESENTATION_TEMPLATE:
				newDoc = new OdfPresentationDocument(pkg, internalPath, OdfPresentationDocument.OdfMediaType.PRESENTATION_TEMPLATE);
				break;

            case CHART:
                newDoc = new OdfChartDocument(pkg, internalPath, OdfChartDocument.OdfMediaType.CHART);
                break;

            case CHART_TEMPLATE:
                newDoc = new OdfChartDocument(pkg, internalPath, OdfChartDocument.OdfMediaType.CHART_TEMPLATE);
                break;

			default:
				throw new NotSupportedMediaTypeException("Given mediaType '" + odfMediaType.mMediaType + "' is not yet supported!", odfMediaType.mMediaType);
		}
		return newDoc;
	}

	/**
	 * Returns an embedded OdfPackageDocument from the given package path.
	 *
	 * @param documentPath to the ODF document within the package. The path is
	 * relative the current ODF document path.
	 * @return an embedded OdfPackageDocument
	 */
	@Override
	public OdfDocument loadSubDocument(String documentPath) {
		return (OdfDocument) super.loadSubDocument(documentPath);
	}

	/**
	 * Method returns all embedded OdfPackageDocuments, which match a valid
	 * OdfMediaType, of the current OdfPackageDocument. Note: The root document
	 * is not part of the returned collection.
	 *
	 * @return a map with all embedded documents and their paths of the current
	 * OdfPackageDocument
	 */
	public Map<String, OdfDocument> loadSubDocuments() {
		return loadSubDocuments(null);
	}

	/**
	 * Method returns all embedded OdfPackageDocuments of sthe current
	 * OdfPackageDocument matching the according MediaType. This is done by
	 * matching the subfolder entries of the manifest file with the given
	 * OdfMediaType.
	 *
	 * @param desiredMediaType media type of the documents to be returned (used
	 * as a filter).
	 * @return embedded documents of the current OdfPackageDocument matching the
	 * given media type
	 */
	public Map<String, OdfDocument> loadSubDocuments(OdfMediaType desiredMediaType) {
		String wantedMediaString = null;
		if (desiredMediaType != null) {
			wantedMediaString = desiredMediaType.getMediaTypeString();
		}
		Map<String, OdfDocument> embeddedObjectsMap = new HashMap<String, OdfDocument>();
		// check manifest for current embedded OdfPackageDocuments
		Set<String> manifestEntries = mPackage.getFilePaths();
		for (String path : manifestEntries) {
			// any directory that is not the root document "/"
			if (path.length() > 1 && path.endsWith(SLASH)) {
				String entryMediaType = mPackage.getFileEntry(path).getMediaTypeString();
				// if the entry is a document (directory has mediaType)
				if (entryMediaType != null) {
					// if a specific ODF mediatype was requested
					if (wantedMediaString != null) {
						// test if the desired mediatype matches the current
						if (entryMediaType.equals(wantedMediaString)) {
							path = normalizeDocumentPath(path);
							embeddedObjectsMap.put(path, (OdfDocument) mPackage.loadDocument(path));
						}
					} else {
						// test if any ODF mediatype matches the current
						for (OdfMediaType mediaType : OdfMediaType.values()) {
							if (entryMediaType.equals(mediaType.getMediaTypeString())) {
								embeddedObjectsMap.put(path, (OdfDocument) mPackage.loadDocument(path));
							}
						}
					}
				}
			}
		}
		return embeddedObjectsMap;
	}

	/**
	 * Sets the media type of the OdfDocument
	 *
	 * @param odfMediaType media type to be set
	 */
	protected void setOdfMediaType(OdfMediaType odfMediaType) {
		mMediaType = odfMediaType;
		super.setMediaTypeString(odfMediaType.getMediaTypeString());
	}

	/**
	 * Gets the media type of the OdfDocument
	 */
	protected OdfMediaType getOdfMediaType() {
		return mMediaType;
	}

	/**
	 * Get the meta data feature instance of the current document
	 *
	 * @return the meta data feature instance which represent
	 * <code>office:meta</code> in the meta.xml
	 * @throws SAXException
	 */
	public MetaData getOfficeMetaData(boolean forceCreate) throws SAXException {
		MetaData metaDom = getMetaDom();
		if (metaDom==null&&forceCreate) {
			metaDom = new MetaData(this, OdfSchemaDocument.OdfXMLFile.META.getFileName());
		}
		return metaDom;
	}

	/**
	 * Save the document to an OutputStream. Delegate to the root document and
	 * save possible embedded OdfDocuments.
	 *
	 * <p>If the input file has been cached (this is the case when loading from
	 * an InputStream), the input file can be overwritten.</p>
	 *
	 * <p>If not, the OutputStream may not point to the input file! Otherwise
	 * this will result in unwanted behaviour and broken files.</p>
	 *
	 * <p>When save the embedded document to a stand alone document, all the
	 * file entries of the embedded document will be copied to a new document
	 * package. If the embedded document is outside of the current document
	 * directory, you have to embed it to the sub directory and refresh the link
	 * of the embedded document. you should reload it from the stream to get the
	 * saved embedded document.
	 *
	 * @param out - the OutputStream to write the file to
	 * @throws java.lang.Exception if the document could not be saved
	 */
	public void save(OutputStream out) throws Exception {
		updateMetaData();
		if (!isRootDocument()) {
			OdfDocument newDoc = loadDocumentFromTemplate(getOdfMediaType());
			newDoc.insertDocument(this, ROOT_DOCUMENT_PATH);
			newDoc.updateMetaData();
			newDoc.mPackage.save(out);
			// ToDo: (Issue 219 - PackageRefactoring) - Return the document, when not closing!
			// Should we close the sources now? User will never receive the open package!
		} else {
			mPackage.save(out);
		}
	}

	/**
	 * Save the document to a given file.
	 *
	 * <p>If the input file has been cached (this is the case when loading from
	 * an InputStream), the input file can be overwritten.</p>
	 *
	 * <p>Otherwise it's allowed to overwrite the input file as long as the same
	 * path name is used that was used for loading (no symbolic link foo2.odt
	 * pointing to the loaded file foo1.odt, no network path X:\foo.odt pointing
	 * to the loaded file D:\foo.odt).</p>
	 *
	 * <p>When saving the embedded document to a stand alone document, all files
	 * of the embedded document will be copied to a new document package. If the
	 * embedded document is outside of the current document directory, you have
	 * to embed it to the sub directory and refresh the link of the embedded
	 * document. You should reload it from the given file to get the savedloadTemplate
	 * embedded document.
	 *
	 * @param file - the file to save the document
	 * @throws java.lang.Exception if the document could not be saved
	 */
	@Override
	public void save(File file) throws Exception {
		updateMetaData();
		if (!isRootDocument()) {
			OdfDocument newDoc = loadDocumentFromTemplate(getOdfMediaType());
			newDoc.insertDocument(this, ROOT_DOCUMENT_PATH);
			newDoc.updateMetaData();
			newDoc.mPackage.save(file);
			// ToDo: (Issue 219 - PackageRefactoring) - Return the document, when not closing!
			// Should we close the sources now? User will never receive the open package!
		} else {
			this.mPackage.save(file);
		}
	}

	@Override
	public String toString() {
		return "\n" + getMediaTypeString() + " - ID: " + this.hashCode() + " " + getPackage().getBaseURI();
	}


	/**
	 * Update document meta data in the ODF document. Following metadata data is
	 * being updated:
	 * <ul>
	 * <li>The name of the person who last modified this document will be the
	 * Java user.name System property</li>
	 * <li>The date and time when the document was last modified using current
	 * data</li>
	 * <li>The number of times this document has been edited is incremented by
	 * 1</li>
	 * <li>The total time spent editing this document</li>
	 * </ul>
	 *
	 * TODO:This method will be moved to OdfMetadata class. see
	 * http://odftoolkit.org/bugzilla/show_bug.cgi?id=204
	 * @throws SAXException
	 *
	 */
	public void updateMetaData() throws SAXException {
	    getOfficeMetaData(true).updataMetaData();
	}

    public abstract int applyOperations(OdfOperationDoc operationDocument, JSONArray ops)
    	throws Exception;

    protected void removeCachedView() {
        mPackage = getPackage();
        // removes the LO/AO view caching
        mPackage.remove("Thumbnails/thumbnail.png");
    }
}
