

package org.odftoolkit.odfdom;

public final class Names {
	public static final String VISIBLE = "visible";
	public static final String COLLAPSE = "collapse";
	public static final String APOSTROPHE = "'";
	public static final String APOSTROPHE_AND_EQUATION = "'=";
	public static final String EMPTY_STRING = "";
	public static final String SPACE_CHAR = " ";
	public static final String NONE = "none";
	public static final String DOT_CHAR = ".";
	public static final String DOT = "dot";
	public static final String EQUATION = "=";
	public static final String HYPHEN_CHAR = "-";
	public static final String HYPHEN = "hyphen";
	public static final String UNDERSCORE_CHAR = "_";
	public static final String UNDERSCORE = "underscore";
	
    public static final String OX_DEFAULT_LIST = "OX_DEFAULT_LIST";
    public static final String OX_DEFAULT_STYLE_PREFIX = "default_";
    public static final String OX_DEFAULT_STYLE_SUFFIX = "_style";
    public static final char PATH_SEPARATOR = '/';
    public static final String PERCENT = "%";
    public static final double DOTS_PER_INCH = 72.0;
}
