/************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * Copyright 2008, 2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0. You can also
 * obtain a copy of the License at http://odftoolkit.org/docs/license.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ************************************************************************/
package org.odftoolkit.odfdom.doc;

import java.io.File;
import java.io.InputStream;
import org.json.JSONArray;
import org.json.JSONObject;
import org.odftoolkit.odfdom.pkg.MediaType;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.odp.dom.JsonOperationConsumer;
import com.openexchange.office.filter.odf.odp.dom.JsonOperationProducer;
import com.openexchange.office.filter.odf.ods.dom.chart.ChartContent;
import com.openexchange.office.filter.odf.ods.dom.chart.ChartStyles;

public class OdfChartDocument extends OdfDocument {

    private static final String EMPTY_CHART_DOCUMENT_PATH = "/OdfChartDocument.odp";
    static final Resource EMPTY_CHART_DOCUMENT_RESOURCE = new Resource(EMPTY_CHART_DOCUMENT_PATH);

	@Override
    public String getDocumentType() {
        return "chart";
	}

	/**
     * This enum contains all possible media types of OdfChartDocument
     * documents.
     */
	public enum OdfMediaType implements MediaType {

        CHART(OdfDocument.OdfMediaType.CHART),
        CHART_TEMPLATE(OdfDocument.OdfMediaType.CHART_TEMPLATE);
		private final OdfDocument.OdfMediaType mMediaType;

		OdfMediaType(OdfDocument.OdfMediaType mediaType) {
			this.mMediaType = mediaType;
		}

		/**
		 * @return the ODF mediatype of this document
		 */
		public OdfDocument.OdfMediaType getOdfMediaType() {
			return mMediaType;
		}

		/**
		 * @return the mediatype of this document
		 */
		@Override
        public String getMediaTypeString() {
			return mMediaType.getMediaTypeString();
		}

		/**
		 * @return the ODF filesuffix of this document
		 */
		@Override
        public String getSuffix() {
			return mMediaType.getSuffix();
		}

		/**
		 *
		 * @param mediaType string defining an ODF document
		 * @return the according OdfMediatype encapuslating the given string and the suffix
		 */
		public static OdfDocument.OdfMediaType getOdfMediaType(String mediaType) {
			return OdfDocument.OdfMediaType.getOdfMediaType(mediaType);
		}
	}

	/**
     * Creates an empty chart document.
     *
     * @return ODF chart document based on a default template
     * @throws java.lang.Exception - if the document could not be created
     */
    public static OdfChartDocument newChartDocument() throws Exception {
        return (OdfChartDocument) OdfDocument.loadTemplate(EMPTY_CHART_DOCUMENT_RESOURCE, OdfDocument.OdfMediaType.CHART);
	}

	/**
     * Creates an empty chart template.
     *
     * @return ODF chart template based on a default
     * @throws Exception - if the template could not be created
     */
    public static OdfChartDocument newChartTemplateDocument() throws Exception {
        OdfChartDocument doc = (OdfChartDocument) OdfDocument.loadTemplate(EMPTY_CHART_DOCUMENT_RESOURCE, OdfDocument.OdfMediaType.CHART_TEMPLATE);
        doc.changeMode(OdfMediaType.CHART_TEMPLATE);
		return doc;
	}

	/** To avoid data duplication a new document is only created, if not already opened.
	 * A document is cached by this constructor using the internalpath as key. */
	protected OdfChartDocument(OdfPackage pkg, String internalPath, OdfChartDocument.OdfMediaType odfMediaType) throws SAXException {
		super(pkg, internalPath, odfMediaType.mMediaType);
	}

	/**
     * Creates an OdfChartDocument from the OpenDocument provided by a resource Stream.
     *
     * <p>Since an InputStream does not provide the arbitrary (non sequentiell)
     * read access needed by OdfChartDocument, the InputStream is cached. This usually
     * takes more time compared to the other createInternalDocument methods.
     * An advantage of caching is that there are no problems overwriting
     * an input file.</p>
     *
     * <p>If the resource stream is not a ODF chart document, ClassCastException might be thrown.</p>
     *
     * @param inputStream - the InputStream of the ODF chart document.
     * @return the chart document created from the given InputStream
     * @throws java.lang.Exception - if the document could not be created.
     */
	public static OdfChartDocument loadDocument(InputStream inputStream) throws Exception {
		return (OdfChartDocument) OdfDocument.loadDocument(inputStream);
	}

	/**
     * Loads an OdfChartDocument from the provided path.
     *
     * <p>OdfChartDocument relies on the file being available for read access over
     * the whole lifecycle of OdfChartDocument.</p>
     *
     * <p>If the resource stream is not a ODF chart document, ClassCastException might be thrown.</p>
     *
     * @param documentPath - the path from where the document can be loaded
     * @return the chart document from the given path
     *         or NULL if the media type is not supported by ODFDOM.
     * @throws java.lang.Exception - if the document could not be created.
     */
	public static OdfChartDocument loadDocument(String documentPath) throws Exception {
		return (OdfChartDocument) OdfDocument.loadDocument(documentPath);
	}

	/**
     * Creates an OdfChartDocument from the OpenDocument provided by a File.
     *
     * <p>OdfChartDocument relies on the file being available for read access over
     * the whole lifecycle of OdfChartDocument.</p>
     *
     * <p>If the resource stream is not a ODF chart document, ClassCastException might be thrown.</p>
     *
     * @param file - a file representing the ODF chart document.
     * @return the chart document created from the given File
     * @throws java.lang.Exception - if the document could not be created.
     */
	public static OdfChartDocument loadDocument(File file) throws Exception {
		return (OdfChartDocument) OdfDocument.loadDocument(file);
	}

	/**
	 * Switches this instance to the given type. This method can be used to e.g.
	 * convert a document instance to a template and vice versa.
	 * Changes take affect in the package when saving the document.
	 *
	 * @param type the compatible ODF mediatype.
	 */
	public void changeMode(OdfMediaType type) {
		setOdfMediaType(type.mMediaType);
	}

    @Override
    public OdfFileDom newFileDom(String packagePath) throws SAXException {

        OdfFileDom newFileDom = null;
        // before creating a new dom, make sure that there no DOM opened for this file already
        Document existingDom = getPackage().getCachedDom(packagePath);
        if (existingDom == null) {
            if (packagePath.equals("content.xml") || packagePath.endsWith("/content.xml")) {
                newFileDom = new ChartContent(this, packagePath);
            }
            else if (packagePath.equals("styles.xml") || packagePath.endsWith("/styles.xml")) {
                newFileDom = new ChartStyles(this, packagePath);
            }
        }
        if(newFileDom==null) {
            newFileDom = super.newFileDom(packagePath);
        }
        return newFileDom;
    }

	@Override
	public int applyOperations(OdfOperationDoc operationDocument, JSONArray ops) throws Exception {

	    return new JsonOperationConsumer(operationDocument).applyOperations(ops);
	}

    @Override
    public JSONObject getOperations(OdfOperationDoc operationDoc) throws Exception {
        return new JsonOperationProducer(operationDoc).getDocumentOperations();
    }
}
