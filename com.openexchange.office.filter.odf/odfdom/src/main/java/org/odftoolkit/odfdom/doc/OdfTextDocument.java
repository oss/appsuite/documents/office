

/**
 * **********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * Copyright 2008, 2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0. You can also
 * obtain a copy of the License at http://odftoolkit.org/docs/license.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***********************************************************************
 */
package org.odftoolkit.odfdom.doc;

import java.io.File;
import java.io.InputStream;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.pkg.MediaType;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.odt.dom.JsonOperationConsumer;
import com.openexchange.office.filter.odf.odt.dom.JsonOperationProducer;
import com.openexchange.office.filter.odf.odt.dom.TextContent;
import com.openexchange.office.filter.odf.odt.dom.TextStyles;

/**
 * This class represents an empty ODF text document.
 *
 */
public class OdfTextDocument extends OdfDocument {

	private static final String EMPTY_TEXT_DOCUMENT_PATH = "/OdfTextDocument.odt";
	static final Resource EMPTY_TEXT_DOCUMENT_RESOURCE = new Resource(EMPTY_TEXT_DOCUMENT_PATH);

    @Override
    public String getDocumentType() {
        return "text";
    }

    /**
	 * This enum contains all possible media types of OdfSpreadsheetDocument
	 * documents.
	 */
	public enum OdfMediaType implements MediaType {

		TEXT(OdfDocument.OdfMediaType.TEXT),
		TEXT_TEMPLATE(OdfDocument.OdfMediaType.TEXT_TEMPLATE),
		TEXT_MASTER(OdfDocument.OdfMediaType.TEXT_MASTER),
		TEXT_WEB(OdfDocument.OdfMediaType.TEXT_WEB);
		private final OdfDocument.OdfMediaType mMediaType;

		OdfMediaType(OdfDocument.OdfMediaType mediaType) {
			this.mMediaType = mediaType;
		}

		/**
		 * @return the media type of this document
		 */
		public String getMediaTypeString() {
			return mMediaType.getMediaTypeString();
		}

		/**
		 * @return the ODF file suffix of this document
		 */
		public String getSuffix() {
			return mMediaType.getSuffix();
		}

		/**
		 *
		 * @param mediaType string defining an ODF document
		 * @return the according OdfMediatype encapsulating the given string and
		 * the suffix
		 */
		public static OdfDocument.OdfMediaType getOdfMediaType(String mediaType) {
			return OdfDocument.OdfMediaType.getOdfMediaType(mediaType);
		}
	}

	/**
	 * Creates an empty text document.
	 *
	 * @return ODF text document based on a default template
	 * @throws java.lang.Exception - if the document could not be created
	 */
	public static OdfTextDocument newTextDocument() throws Exception {
		return (OdfTextDocument) OdfDocument.loadTemplate(EMPTY_TEXT_DOCUMENT_RESOURCE, OdfDocument.OdfMediaType.TEXT);
	}

	/**
	 * Creates an empty text document.
	 *
	 * @return ODF text document based on a default template
	 * @throws java.lang.Exception - if the document could not be created
	 */
	public static OdfTextDocument newTextDocument(OdfTextDocument.OdfMediaType mimeType) throws Exception {
		return (OdfTextDocument) OdfDocument.loadTemplate(EMPTY_TEXT_DOCUMENT_RESOURCE, OdfDocument.OdfMediaType.TEXT);
	}

	/**
	 * Creates an empty text template.
	 *
	 * @return ODF text template based on a default
	 * @throws java.lang.Exception - if the template could not be created
	 */
	public static OdfTextDocument newTextTemplateDocument() throws Exception {
		return (OdfTextDocument) OdfDocument.loadTemplate(EMPTY_TEXT_DOCUMENT_RESOURCE, OdfDocument.OdfMediaType.TEXT_TEMPLATE);
	}

	/**
	 * Creates an empty text master document.
	 *
	 * @return ODF text master based on a default
	 * @throws java.lang.Exception - if the document could not be created
	 */
	public static OdfTextDocument newTextMasterDocument() throws Exception {
		OdfTextDocument doc = (OdfTextDocument) OdfDocument.loadTemplate(EMPTY_TEXT_DOCUMENT_RESOURCE, OdfDocument.OdfMediaType.TEXT_MASTER);
		doc.changeMode(OdfMediaType.TEXT_MASTER);
		return doc;
	}

	/**
	 * Creates an empty text web.
	 *
	 * @return ODF text web based on a default
	 * @throws java.lang.Exception - if the document could not be created
	 */
	public static OdfTextDocument newTextWebDocument() throws Exception {
		OdfTextDocument doc = (OdfTextDocument) OdfDocument.loadTemplate(EMPTY_TEXT_DOCUMENT_RESOURCE, OdfDocument.OdfMediaType.TEXT_WEB);
		doc.changeMode(OdfMediaType.TEXT_WEB);
		return doc;
	}

	/**
	 * Creates an OdfTextDocument from the OpenDocument provided by a resource
	 * Stream.
	 *
	 * <p>Since an InputStream does not provide the arbitrary (non sequential)
	 * read access needed by OdfTextDocument, the InputStream is cached. This
	 * usually takes more time compared to the other createInternalDocument
	 * methods. An advantage of caching is that there are no problems
	 * overwriting an input file.</p>
	 *
	 * <p>If the resource stream is not a ODF text document, ClassCastException
	 * might be thrown.</p>
	 *
	 * @param inputStream - the InputStream of the ODF text document.
	 * @return the text document created from the given InputStream
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfTextDocument loadDocument(InputStream inputStream) throws Exception {
		return (OdfTextDocument) OdfDocument.loadDocument(inputStream);
	}

	/**
	 * Creates an OdfTextDocument from the OpenDocument provided by a resource
	 * Stream.
	 *
	 * <p>Since an InputStream does not provide the arbitrary (non sequential)
	 * read access needed by OdfTextDocument, the InputStream is cached. This
	 * usually takes more time compared to the other createInternalDocument
	 * methods. An advantage of caching is that there are no problems
	 * overwriting an input file.</p>
	 *
	 * <p>If the resource stream is not a ODF text document, ClassCastException
	 * might be thrown.</p>
	 *
	 * @param inputStream - the InputStream of the ODF text document.
	 * @param configuration - key/value pairs of user given run-time settings
	 * (configuration)
	 * @return the text document created from the given InputStream
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfTextDocument loadDocument(InputStream inputStream, Map<String, Object> configuration) throws Exception {
		return (OdfTextDocument) OdfDocument.loadDocument(inputStream, configuration);
	}

	/**
	 * Loads an OdfTextDocument from the provided path.
	 *
	 * <p>OdfTextDocument relies on the file being available for read access
	 * over the whole life-cycle of OdfTextDocument.</p>
	 *
	 * <p>If the resource stream is not a ODF text document, ClassCastException
	 * might be thrown.</p>
	 *
	 * @param documentPath - the path from where the document can be loaded
	 * @return the text document from the given path or NULL if the media type
	 * is not supported by ODFDOM.
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfTextDocument loadDocument(String documentPath) throws Exception {
		return (OdfTextDocument) OdfDocument.loadDocument(documentPath);
	}

	/**
	 * Creates an OdfTextDocument from the OpenDocument provided by a File.
	 *
	 * <p>OdfTextDocument relies on the file being available for read access
	 * over the whole lifecycle of OdfTextDocument.</p>
	 *
	 * <p>If the resource stream is not a ODF text document, ClassCastException
	 * might be thrown.</p>
	 *
	 * @param file - a file representing the ODF text document.
	 * @return the text document created from the given File
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfTextDocument loadDocument(File file) throws Exception {
		return (OdfTextDocument) OdfDocument.loadDocument(file);
	}

	/**
	 * To avoid data duplication a new document is only created, if not already
	 * opened. A document is cached by this constructor using the internal path
	 * as key.
	 */
	protected OdfTextDocument(OdfPackage pkg, String internalPath, OdfTextDocument.OdfMediaType odfMediaType) throws SAXException {
		super(pkg, internalPath, odfMediaType.mMediaType);
	}

	/**
	 * Changes the document to the given mediatype. This method can only be used
	 * to convert a document to a related mediatype, e.g. template.
	 *
	 * @param mediaType the related ODF mimetype
	 */
	public void changeMode(OdfMediaType mediaType) {
		setOdfMediaType(mediaType.mMediaType);
	}

	@Override
    public OdfFileDom newFileDom(String packagePath) throws SAXException {

		OdfFileDom newFileDom = null;
        // before creating a new dom, make sure that there no DOM opened for this file already
        Document existingDom = getPackage().getCachedDom(packagePath);
        if (existingDom == null) {
            if (packagePath.equals("content.xml") || packagePath.endsWith("/content.xml")) {
                newFileDom = new TextContent(this, packagePath);
            }
            else if (packagePath.equals("styles.xml") || packagePath.endsWith("/styles.xml")) {
                newFileDom = new TextStyles(this, packagePath);
            }
        }
        if(newFileDom==null) {
        	newFileDom = super.newFileDom(packagePath);
        }
        return newFileDom;
    }

	@Override
    public int applyOperations(OdfOperationDoc operationDoc, JSONArray ops)
    	throws Exception {

		return new JsonOperationConsumer(operationDoc).applyOperations(ops);
    }

    @Override
    public JSONObject getOperations(OdfOperationDoc operationDoc)
       	throws SAXException, JSONException {

		return new JsonOperationProducer(operationDoc).getDocumentOperations();
    }
}
