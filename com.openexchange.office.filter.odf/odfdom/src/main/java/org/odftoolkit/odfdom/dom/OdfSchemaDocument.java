

/**
 * **********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * Copyright 2008, 2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0. You can also
 * obtain a copy of the License at http://odftoolkit.org/docs/license.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ***********************************************************************
 */
package org.odftoolkit.odfdom.dom;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.odftoolkit.odfdom.pkg.OdfPackageDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.odf.MetaData;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.Settings;
import com.openexchange.office.filter.odf.styles.DocumentStyles;

/**
 * A document in ODF is from the package view a directory with a media type. If
 * the media type represents a document described by the ODF 1.2 Schema, certain
 * files are assumed within: content.xml, styles.xml, metadata.xml and
 * settings.xml.
 *
 * The class represents such a document, providing easier access to its XML
 * files.
 */
public abstract class OdfSchemaDocument extends OdfPackageDocument {

    private OdfFileDom mContentDom;
    private DocumentStyles mStylesDom;
    private MetaData mMetaDom;
    private Settings mSettingsDom;

    /**
     * Creates a new OdfSchemaDocument.
     *
     * @param pkg - the ODF Package that contains the document. A baseURL is
     * being generated based on its location.
     * @param internalPath - the directory path within the package from where
     * the document should be loaded.
     * @param mediaTypeString - media type of stream. If unknown null can be
     * used.
     */
    protected OdfSchemaDocument(OdfPackage pkg, String internalPath, String mediaTypeString) {
        super(pkg, internalPath, mediaTypeString);
    }

    /**
     * This enum contains all possible standardized XML ODF files of the
     * OpenDocument document.
     */
    public static enum OdfXMLFile {

        /**
         * The XML file containing the content of an ODF document as specified
         * by the ODF 1.2 specification part 1.
         */
        CONTENT("content.xml"),
        /**
         * The XML file containing a predifined set of metadata related to an
         * ODF document as specified by the ODF 1.2 specification part 1.
         */
        META("meta.xml"),
        /**
         * The XML file containing the settings of an ODF document as specified
         * by the ODF 1.2 specification part 1.
         */
        SETTINGS("settings.xml"),
        /**
         * The XML file containing the styles of an ODF document as specified by
         * the ODF 1.2 specification part 1.
         */
        STYLES("styles.xml");
        private final String mFileName;

        /**
         * @return the file name of xml files contained in odf packages.
         */
        public String getFileName() {
            return mFileName;
        }

        OdfXMLFile(String fileName) {
            this.mFileName = fileName;
        }
    }

    public JSONObject getOperations(OdfOperationDoc operationDoc) throws Exception {
        return null;
    }

	public void initPartitioning(DocumentProperties documentProperties)
		throws FilterException {
	}

	public Map<String, Object> getMetaData(DocumentProperties documentProperties)
		throws Exception {

		return new HashMap<String, Object>(1);
	}

	public Map<String, Object> getActivePart(DocumentProperties documentProperties)
		throws Exception {

		final HashMap<String, Object> activePart = new HashMap<String, Object>(1);
		final OdfOperationDoc operationDocument = (OdfOperationDoc)documentProperties.get(DocumentProperties.PROP_DOCUMENT);
		final JSONObject docOperations = getOperations(operationDocument);
		activePart.put("operations", docOperations.getJSONArray("operations"));
		return activePart;
	}

	public Map<String, Object> getNextPart(DocumentProperties documentProperties)
		throws Exception {

		return null;
	}

    /**
     * Get the relative path for an embedded ODF document including its file
     * name.
     *
     * @param file represents one of the standardized XML ODF files.
     * @return path to embedded ODF XML file relative to ODF package root.
     */
    protected String getXMLFilePath(OdfXMLFile file) {
        return file.mFileName;
    }

    /**
     * Get the URI, where this ODF document is stored.
     *
     * @return the URI to the ODF document. Returns null if document is not
     * stored yet.
     */
    public String getBaseURI() {
        return mPackage.getBaseURI();

    }

    /**
     * Return the ODF type-based content DOM of the content.xml
     *
     * @return ODF type-based content DOM or null if no content.xml exists.
     * @throws Exception if content DOM could not be initialized
     */
    public OdfFileDom getContentDom() throws SAXException {
        if (mContentDom == null) {
            mContentDom = getFileDom(OdfXMLFile.CONTENT);
        }
        return mContentDom;
    }

    /**
     * Return the ODF type-based styles DOM of the styles.xml
     *
     * @return ODF type-based styles DOM or null if no styles.xml exists.
     * @throws Exception if styles DOM could not be initialized
     */
    public DocumentStyles getStylesDom() throws SAXException {
        if (mStylesDom == null) {
            mStylesDom = (DocumentStyles) getFileDom(OdfXMLFile.STYLES);
        }
        return mStylesDom;
    }

    /**
     * Return the ODF type-based metadata DOM of the meta.xml
     *
     * @return ODF type-based meta DOM or null if no meta.xml exists.
     * @throws Exception if meta DOM could not be initialized
     */
    public MetaData getMetaDom() throws SAXException {
        if (mMetaDom == null) {
            mMetaDom = (MetaData)getFileDom(OdfXMLFile.META);
        }
        return mMetaDom;
    }

    /**
     * Return the ODF type-based settings DOM of the settings.xml
     *
     * @return ODF type-based settings DOM or null if no settings.xml exists.
     * @throws Exception if settings DOM could not be initialized
     */
    public Settings getSettingsDom() throws SAXException {
        if (mSettingsDom == null) {
            mSettingsDom = (Settings)getFileDom(OdfXMLFile.SETTINGS);
        }
        return mSettingsDom;
    }

    /**
     * Sets the ODF type-based content DOM of the content.xml
     *
     * @param contentDom ODF type-based content DOM or null if no content.xml
     * exists.
     */
    public void setContentDom(OdfFileDom contentDom) {
        mContentDom = contentDom;
    }

    /**
     * Sets the ODF type-based styles DOM of the styles.xml
     *
     * @param stylesDom ODF type-based styles DOM or null if no styles.xml
     * exists.
     */
    public void setStylesDom(DocumentStyles stylesDom) {
        mStylesDom = stylesDom;
    }

    /**
     * Sets the ODF type-based meta DOM of the meta.xml
     *
     * @param metaDom ODF type-based meta DOM or null if no meta.xml exists.
     */
    public void setMetaDom(MetaData metaDom) {
        mMetaDom = metaDom;
    }

    /**
     * Sets the ODF type-based settings DOM of the settings.xml
     *
     * @param settingsDom ODF type-based settings DOM or null if no settings.xml
     * exists.
     */
    public void setSettingsDom(Settings settingsDom) {
        mSettingsDom = settingsDom;
    }

    public OdfFileDom getFileDom(OdfXMLFile file) throws SAXException {
        return getFileDom(getXMLFilePath(file));
    }
}
