/************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * Copyright 2008, 2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0. You can also
 * obtain a copy of the License at http://odftoolkit.org/docs/license.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ************************************************************************/
package org.odftoolkit.odfdom.pkg;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.xerces.dom.DOMOutputImpl;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xml.serializer.SerializationHandler;
import org.odftoolkit.odfdom.IElementWriter;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfDocument.OdfMediaType;
import org.odftoolkit.odfdom.pkg.manifest.Algorithm;
import org.odftoolkit.odfdom.pkg.manifest.EncryptionData;
import org.odftoolkit.odfdom.pkg.manifest.KeyDerivation;
import org.odftoolkit.odfdom.pkg.manifest.OdfFileEntry;
//import org.odftoolkit.odfdom.pkg.serializer.SerializationHandler;
//import org.odftoolkit.odfdom.pkg.serializer.Serializer;
//import org.odftoolkit.odfdom.pkg.serializer.dom3.LSSerializerImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSParserFilter;
import org.w3c.dom.ls.LSSerializerFilter;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import com.openexchange.office.filter.core.Tools;
import com.openexchange.office.tools.common.parser.CharMatcher;

/**
 * OdfPackage represents the package view to an OpenDocument document. The
 * OdfPackage will be created from an ODF document and represents a copy of the
 * loaded document, where files can be inserted and deleted. The changes take
 * effect, when the OdfPackage is being made persisted by save().
 */
public class OdfPackage {

	// Static parts of file references
	private static final String DOUBLE_DOT = "..";
	private static final String DOT = ".";
	private static final String SLASH = "/";
	private static final String COLON = ":";
	private static final String ENCODED_APOSTROPHE = "&apos;";
	private static final String ENCODED_QUOTATION = "&quot;";
	private static final String EMPTY_STRING = "";
	private static final String XML_MEDIA_TYPE = "text/xml";
	// Search patterns to be used in RegEx expressions
	private static final Pattern BACK_SLASH_PATTERN = Pattern.compile("\\\\");
	private static final Pattern DOUBLE_SLASH_PATTERN = Pattern.compile("//");
	private static final Pattern QUOTATION_PATTERN = Pattern.compile("\"");
	private static final Pattern APOSTROPHE_PATTERN = Pattern.compile("'");
	private static final Pattern CONTROL_CHAR_PATTERN = Pattern.compile("\\p{Cntrl}");

	private static final Set<String> COMPRESSEDFILETYPES;
	private static final byte[] HREF_PATTERN = {'x', 'l', 'i', 'n', 'k', ':', 'h', 'r', 'e', 'f', '=', '"'}; // xlink:href="
	private static final Set<String> CLEANUP_FILES;


	// some well known streams inside ODF packages
	private String mMediaType;
	private String mBaseURI;
	private ZipHelper mZipFile;
	private Resolver mResolver;
	private Map<String, ZipEntry> mZipEntries;
    private Map<String, ZipEntry> mOriginalZipEntries;
	private Map<String, OdfFileEntry> mManifestEntries;
	// All opened documents from the same package are cached (including the root document)
	private Map<String, OdfPackageDocument> mPkgDocuments;

	// Three different incarnations of a package file/data
	// save() will check 1) mPkgDoms, 2) if not check mMemoryFileCache
    private Map<String, Document> mPkgDoms;
    private Map<String, byte[]> mMemoryFileCache;
	private Map<String, Object> mConfiguration = new HashMap<String, Object>();
	private Map<String, Pair<Integer, Integer>> pixelSizes = new HashMap<String, Pair<Integer, Integer>>();
    private Map<String, Pair<Double, Double>> dpiFromImage = new HashMap<String, Pair<Double, Double>>();
    private String mManifestVersion;

    private Set<String> allUsedDrawings;

	/* Commonly used files within the ODF Package */
	public enum OdfFile {

		/** The image directory is not defined by the OpenDocument standard, nevertheless the most spread ODF application OpenOffice.org is using the directory named "Pictures". */
		IMAGE_DIRECTORY("Pictures"),
		/** The "META-INF/manifest.xml" file is defined by the ODF 1.2 part 3 Package specification. This manifest is the 'content table' of the ODF package and describes the file entries of the ZIP including directories, but should not contain empty directories.*/
		MANIFEST("META-INF/manifest.xml"),
		/** The "mimetype" file is defined by the ODF 1.2 part 3 Package specification. It contains the mediatype string of the root document and must be the first file in the ZIP and must not be compressed. */
		MEDIA_TYPE("mimetype");
		final String internalPath;

		OdfFile(String internalPath) {
			this.internalPath = internalPath;
		}

		public String getPath() {
			return internalPath;
		}
	}

	static {
		HashSet<String> compressedFileTypes = new HashSet<String>();
		String[] typelist = new String[]{"jpg", "gif", "png", "zip", "rar",
			"jpeg", "mpe", "mpg", "mpeg", "mpeg4", "mp4", "7z", "ari",
			"arj", "jar", "gz", "tar", "war", "mov", "avi"};
		compressedFileTypes.addAll(Arrays.asList(typelist));
		COMPRESSEDFILETYPES = Collections.unmodifiableSet(compressedFileTypes);

		HashSet<String> cleanUpFiles = new HashSet<>();
		cleanUpFiles.add("styles.xml");
		cleanUpFiles.add("content.xml");
		CLEANUP_FILES = Collections.unmodifiableSet(cleanUpFiles);
	}

    private boolean lowMemoryAbort = false;

    public boolean isLowMemoryAbort() {
        return lowMemoryAbort;
    }
    public void setLowMemoryAbort(boolean l) {
        lowMemoryAbort = l;
    }

    private int successfulAppliedOperations = 0;

    public void setSuccessfulAppliedOperations(int opCount) {
        successfulAppliedOperations = opCount;
    }
    public int getSuccessfulAppliedOperations() {
        return successfulAppliedOperations;
    }

	/**
	 * Creates the ODFPackage as an empty Package.
	 */
	private OdfPackage() {
		mMediaType = null;
		mResolver = null;
		mPkgDocuments = new HashMap<String, OdfPackageDocument>();
		mPkgDoms = new HashMap<String, Document>();
		mMemoryFileCache = new HashMap<String, byte[]>();
		mManifestEntries = new HashMap<String, OdfFileEntry>();
		// specify whether validation should be enabled and what SAX ErrorHandler should be used.
	}

	// is called if a low memory notification was received... then its tried to free as much memory as possible
	public void freeMemory() {
		mZipFile = null;
		mResolver = null;
		mZipEntries = null;
		mOriginalZipEntries = null;
		mManifestEntries = null;
		mPkgDocuments = null;
		mPkgDoms = null;
		mMemoryFileCache = null;
		mConfiguration = null;
	}

	/**
	 * Creates an OdfPackage from the OpenDocument provided by a File.
	 *
	 * <p>
	 * OdfPackage relies on the file being available for read access over the
	 * whole life-cycle of OdfPackage.
	 * </p>
	 *
	 * @param pkgFile
	 *            - a file representing the ODF document
	 * @throws SAXException if there's an XML- or validation-related error while loading the package
	 * @throws IOException if there's an I/O error while loading the package
	 */
	private OdfPackage(File pkgFile) throws SAXException, IOException {
		this(pkgFile, getBaseURLFromFile(pkgFile));
	}

	/**
	 * Creates an OdfPackage from the OpenDocument provided by a File.
	 *
	 * <p>
	 * OdfPackage relies on the file being available for read access over the
	 * whole life-cycle of OdfPackage.
	 * </p>
	 *
	 * @param pkgFile a file representing the ODF document
	 * @param baseURI defining the base URI of ODF package.
	 * @param errorHandler - SAX ErrorHandler used for ODF validation
	 * @throws SAXException if there's an XML- or validation-related error while loading the package
	 * @throws IOException if there's an I/O error while loading the package
	 *
	 * @see #getErrorHandler*
	 */
	private OdfPackage(File pkgFile, String baseURI) throws SAXException, IOException {
		this();
		mBaseURI = baseURI;
		try(InputStream packageStream = new FileInputStream(pkgFile)) {
			initializeZip(packageStream);
		}
	}


	/**
	 * Creates an OdfPackage from the OpenDocument provided by a InputStream.
	 *
	 * <p>Since an InputStream does not provide the arbitrary (non sequential)
	 * read access needed by OdfPackage, the InputStream is cached. This usually
	 * takes more time compared to the other constructors. </p>
	 *
	 * @param packageStream - an inputStream representing the ODF package
	 * @param baseURI defining the base URI of ODF package.
	 * @param errorHandler - SAX ErrorHandler used for ODF validation
	 * @param configuration - key/value pairs of user given run-time settings (configuration)
	 * @throws SAXException if there's an XML- or validation-related error while loading the package
	 * @throws IOException if there's an I/O error while loading the package
	 *
	 * @see #getErrorHandler*
	 */
	private OdfPackage(InputStream packageStream, String baseURI, Map<String, Object> configuration)
			throws SAXException, IOException {
		this(); // calling private constructor

		mConfiguration = configuration;
		mBaseURI = baseURI;
		initializeZip(packageStream);
	}

	/**
	 * Loads an OdfPackage from the given documentURL.
	 *
	 * <p>
	 * OdfPackage relies on the file being available for read access over the
	 * whole life-cycle of OdfPackage.
	 * </p>
	 *
	 * @param path
	 *            - the documentURL to the ODF package
	 * @return the OpenDocument document represented as an OdfPackage
	 * @throws java.lang.Exception
	 *             - if the package could not be loaded
	 */
	public static OdfPackage loadPackage(String path) throws Exception {
		File pkgFile = new File(path);
		return new OdfPackage(pkgFile, getBaseURLFromFile(pkgFile));
	}

	/**
	 * Loads an OdfPackage from the OpenDocument provided by a File.
	 *
	 * <p>
	 * OdfPackage relies on the file being available for read access over the
	 * whole life-cycle of OdfPackage.
	 * </p>
	 *
	 * @param pkgFile - the ODF Package
	 * @return the OpenDocument document represented as an OdfPackage
	 * @throws java.lang.Exception - if the package could not be loaded
	 */
	public static OdfPackage loadPackage(File pkgFile) throws Exception {
		return new OdfPackage(pkgFile, getBaseURLFromFile(pkgFile));
	}

	/**
	 * Creates an OdfPackage from the given InputStream.
	 *
	 * <p>
	 * Since an InputStream does not provide the arbitrary (non sequential)
	 * read access needed by OdfPackage, the InputStream is cached. This usually
	 * takes more time compared to the other loadPackage methods.
	 * </p>
	 *
	 * @param packageStream
	 *            - an inputStream representing the ODF package
	 * @return the OpenDocument document represented as an OdfPackage
	 * @throws java.lang.Exception
	 *             - if the package could not be loaded
	 */
	public static OdfPackage loadPackage(InputStream packageStream)
			throws Exception {
		return new OdfPackage(packageStream, null, null);
	}

	/**
	 * Creates an OdfPackage from the given InputStream.
	 *
	 * <p>
	 * Since an InputStream does not provide the arbitrary (non sequential)
	 * read access needed by OdfPackage, the InputStream is cached. This usually
	 * takes more time compared to the other loadPackage methods.
	 * </p>
	 *
	 * @param packageStream
	 *            - an inputStream representing the ODF package
	 * @param configuration - key/value pairs of user given run-time settings (configuration)
	 * @return the OpenDocument document represented as an OdfPackage
	 * @throws java.lang.Exception
	 *             - if the package could not be loaded
	 */
	public static OdfPackage loadPackage(InputStream packageStream, Map<String, Object> configuration)
			throws Exception {
		return new OdfPackage(packageStream, null, configuration);
	}

	/**
	 * Run-time configuration such as special logging or maximum table size to create operations from are stored in this map.
	 * @return key/value pairs of user given run-time settings (configuration)
	 */
	public Map<String, Object> getRunTimeConfiguration(){
		return mConfiguration;
	}

	// Initialize using memory
	private void initializeZip(InputStream odfStream) throws SAXException, IOException {
		ByteArrayOutputStream tempBuf = new ByteArrayOutputStream();
		StreamHelper.transformStream(odfStream, tempBuf);
		byte[] mTempByteBuf = tempBuf.toByteArray();
		tempBuf.close();
		if (mTempByteBuf.length < 3) {
			OdfValidationException ve = new OdfValidationException(OdfPackageConstraint.PACKAGE_IS_NO_ZIP, getBaseURI());
			throw new IllegalArgumentException(ve);
		}
		mZipFile = new ZipHelper(this, mTempByteBuf);
		readZip();
	}

	private void readZip() throws SAXException, IOException {
		mZipEntries = new HashMap<String, ZipEntry>();
		String firstEntryName = mZipFile.entriesToMap(mZipEntries);
		if (mZipEntries.isEmpty()) {
			OdfValidationException ve = new OdfValidationException(OdfPackageConstraint.PACKAGE_IS_NO_ZIP, getBaseURI());
			throw new IllegalArgumentException(ve);
		}
		// initialize the files of the package (fileEnties of Manifest)
		parseManifest();

		// initialize the package media type
		initializeMediaType(firstEntryName);

		// ToDo: Remove all META-INF/* files from the fileEntries of Manifest
		mOriginalZipEntries = new HashMap<String, ZipEntry>();
		mOriginalZipEntries.putAll(mZipEntries);
		mZipEntries.remove(OdfPackage.OdfFile.MEDIA_TYPE.getPath());
		mZipEntries.remove(OdfPackage.OdfFile.MANIFEST.getPath());
		mZipEntries.remove("META-INF/");
		Iterator<String> zipPaths = mZipEntries.keySet().iterator();
		while (zipPaths.hasNext()) {
			String internalPath = zipPaths.next();
			// every resource aside the /META-INF/manifest.xml (and META-INF/ directory)
			// and "mimetype" will be added as fileEntry
			if (!internalPath.equals(OdfPackage.OdfFile.MANIFEST.getPath())
					&& !internalPath.equals("META-INF/")
					&& !internalPath.equals(OdfPackage.OdfFile.MEDIA_TYPE.getPath())) {
				// aside "mediatype" and "META-INF/manifest"
				// add manifest entry as to be described by a <manifest:file-entry>
				ensureFileEntryExistence(internalPath);
			}
		}
	}

	/** Reads the uncompressed "mimetype" file, which contains the package media/mimte type*/
	private void initializeMediaType(String firstEntryName) throws IOException {
		ZipEntry mimetypeEntry = mZipEntries.get(OdfPackage.OdfFile.MEDIA_TYPE.getPath());
		if (mimetypeEntry != null) {
			// get mediatype value of the root document/package from the mediatype file stream
			String entryMediaType = getMediaTypeFromEntry(mimetypeEntry);
			// get mediatype value of the root document/package from the manifest.xml
			String manifestMediaType = getMediaTypeFromManifest();
			// if a valid mediatype was set by the "mimetype" file
			if (entryMediaType != null && !entryMediaType.equals(EMPTY_STRING)) {
				// the root document's mediatype is taken from the "mimetype" file
				mMediaType = entryMediaType;
			} else { // if there is no media-type was set by the "mimetype" file
				// try as fall-back the mediatype of the root document from the manifest.xml
				if (manifestMediaType != null && !manifestMediaType.equals(EMPTY_STRING)) {
					// and used as fall-back for the mediatype of the package
					mMediaType = manifestMediaType;
				}
			}
		} else {
			String manifestMediaType = getMediaTypeFromManifest();
			if (manifestMediaType != null && !manifestMediaType.equals(EMPTY_STRING)) {
				// if not mimetype file exists, the root document mediaType from the manifest.xml is taken
				mMediaType = manifestMediaType;
			}
		}
	}

	/** @returns the media type of the root document from the manifest.xml */
	private String getMediaTypeFromManifest() {
		OdfFileEntry rootDocumentEntry = mManifestEntries.get(SLASH);
		if (rootDocumentEntry != null) {
			return rootDocumentEntry.getMediaTypeString();
		}
		return null;
	}

	/** @returns the media type of the root document from the manifest.xml */
	private String getMediaTypeFromEntry(ZipEntry mimetypeEntry) throws IOException {
		String entryMediaType = null;
		try(ByteArrayOutputStream out = new ByteArrayOutputStream()) {
			StreamHelper.transformStream(mZipFile.getInputStream(mimetypeEntry), out);
			entryMediaType = new String(out.toByteArray(), 0, out.size(), "UTF-8");
		}
		return entryMediaType;
	}

	/**
	 * Insert an Odf document into the package at the given path.
	 * The path has to be a directory and will receive the MIME type of the OdfPackageDocument.
	 *
	 * @param doc the OdfPackageDocument to be inserted.
	 * @param internalPath
	 *		path relative to the package root, where the document should be inserted.
	 */
	void cacheDocument(OdfPackageDocument doc, String internalPath) {
		internalPath = normalizeDirectoryPath(internalPath);
		updateFileEntry(ensureFileEntryExistence(internalPath), doc.getMediaTypeString());
		mPkgDocuments.put(internalPath, doc);
	}

	/**
	 * Set the baseURI for this ODF package. NOTE: Should only be set during
	 * saving the package.
	 * @param baseURI defining the location of the package
	 */
	void setBaseURI(String baseURI) {
		mBaseURI = baseURI;
	}

	/**
	 * @return The URI to the ODF package, usually the URL, where this ODF package is located.
	 * If the package has not URI NULL is returned.
	 * This is the case if the package was new created without an URI and not saved before.
	 */
	public String getBaseURI() {
		return mBaseURI;
	}

	/**
	 * Returns on ODF documents based a given mediatype.
	 *
	 * @param internalPath path relative to the package root, where the document should be inserted.
	 * @return The ODF document, which mediatype dependends on the parameter or
	 *	NULL if media type were not supported.
	 */
	public OdfPackageDocument loadDocument(String internalPath) {
		OdfPackageDocument doc = getCachedDocument(internalPath);
		if (doc == null) {
			String mediaTypeString = getMediaTypeString();
			// ToDo: Issue 265 - Remove dependency to higher layer by factory
			OdfMediaType odfMediaType = OdfMediaType.getOdfMediaType(mediaTypeString);
			if (odfMediaType == null) {
				doc = new OdfPackageDocument(this, internalPath, mediaTypeString);
			} else {
				try {
					String documentMediaType = getMediaTypeString(internalPath);
					odfMediaType = OdfMediaType.getOdfMediaType(documentMediaType);
					if (odfMediaType == null) {
						return null;
					}
					// ToDo: Issue 265 - Remove dependency to higher layer by factory
					doc = OdfDocument.loadDocument(this, internalPath);
				} catch (Exception ex) {
					// ToDo: catching Exception, logging it and continuing is bad style.
					//Refactor exception handling in higher layer, too.
					Logger.getLogger(OdfPackageDocument.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return doc;
	}

	/**
	 * @deprecated This method is only added temporary as workaround for the IBM fork using different DOC classes.
	 * Until the registering of DOC documents to the PKG layer has been finished.
	 * @param internalPath
	 *		path relative to the package root, where the document should be inserted.
	 * @return an already open OdfPackageDocument via its path, otherwise NULL.
	 */
	@Deprecated
	public OdfPackageDocument getCachedDocument(String internalPath) {
		internalPath = normalizeDirectoryPath(internalPath);
		return mPkgDocuments.get(internalPath);
	}

	/**
	 * @param dom
	 *		the DOM tree that has been parsed and should be added to the cache.
	 * @param internalPath
	 *		path relative to the package root, where the XML of the DOM is located.
	 * @return an already open OdfPackageDocument via its path, otherwise NULL.
	 */
	void cacheDom(Document dom, String internalPath) {
		internalPath = normalizeFilePath(internalPath);
		this.insert(dom, internalPath, null);
	}

	/**
	 * @param internalPath
	 *		path relative to the package root, where the document should be inserted.
	 * @return an already open W3C XML Documenet via its path, otherwise NULL.
	 */
	public Document getCachedDom(String internalPath) {
		internalPath = normalizeFilePath(internalPath);
		return this.mPkgDoms.get(internalPath);
	}

	/**
	 * @return a map with all open W3C XML documents with their internal package path as key.
	 */
	Map<String, Document> getCachedDoms() {
		return this.mPkgDoms;
	}

	/**
	 * Removes a document from the package via its path. Independent if it was already opened or not.
	 * @param internalPath
	 *		path relative to the package root, where the document should be removed.
	 */
	public void removeDocument(String internalPath) {
		// Note: the EMPTY String for rrot path will be exchanged to a SLASH
		internalPath = normalizeDirectoryPath(internalPath);
		// get all files of the package
		Set<String> allPackageFileNames = getFilePaths();

		// If the document is the root document
		// the "/" representing the root document is outside the manifest.xml in the API an empty path
		// still normalizeDirectoryPath() already exchanged the EMPTY_STRING to SLASH
		if (internalPath.equals(SLASH)) {
		    final Iterator<String> allPackageNameIter = allPackageFileNames.iterator();
		    while(allPackageNameIter.hasNext()) {
		        final String entryName = allPackageNameIter.next();
		        allPackageNameIter.remove();
		        remove(entryName);
		    }
			remove(SLASH);
		} else {
			//remove all the stream of the directory, such as pictures
			List<String> directoryEntryNames = new ArrayList<String>();
			for (String entryName : allPackageFileNames) {
				if (entryName.startsWith(internalPath)) {
					directoryEntryNames.add(entryName);
				}
			}
			for (String entryName : directoryEntryNames) {
				remove(entryName);
			}
			remove(internalPath);
		}
	}

	/** @return all currently opened OdfPackageDocument of this OdfPackage */
	Set<String> getCachedPackageDocuments() {
		return mPkgDocuments.keySet();
	}

	public OdfPackageDocument getRootDocument() {
		return mPkgDocuments.get(OdfPackageDocument.ROOT_DOCUMENT_PATH);
	}

	/**
	 * Get the media type of the ODF file or document (ie. a directory).
	 * A directory with a mediatype can be loaded as <code>OdfPackageDocument</code>.
	 *  Note: A directoy is represented by in the package as directory with media type
	 * @param internalPath within the package of the file or document.
	 * @return the mediaType for the resource of the given path
	 */
	public String getMediaTypeString(String internalPath) {
		String mediaType = null;
		if (internalPath != null) {
			if (internalPath.equals(EMPTY_STRING) || internalPath.equals(SLASH)) {
				return mMediaType;
			}
			mediaType = getMediaTypeFromEntry(normalizePath(internalPath));
			// if no file was found, look for a normalized directory name
			if (mediaType == null) {
				mediaType = getMediaTypeFromEntry(normalizeDirectoryPath(internalPath));
			}
		}
		return mediaType;
	}

	private String getMediaTypeFromEntry(String internalPath) {
		OdfFileEntry entry = getFileEntry(internalPath);
		// if the document is not in the package, the return is NULL
		if (entry != null) {
			return entry.getMediaTypeString();
		}
		return null;
	}

	/**
	 * Get the media type of the ODF package (equal to media type of ODF root
	 * document)
	 *
	 * @return the mediaType string of this ODF package
	 */
	public String getMediaTypeString() {
		return mMediaType;
	}

	/**
	 * Set the media type of the ODF package (equal to media type of ODF root
	 * document)
	 *
	 * @param mediaType
	 *            string of this ODF package
	 */
	void setMediaTypeString(String mediaType) {
		mMediaType = mediaType;
	}

	/**
	 *
	 * Get an OdfFileEntry for the internalPath NOTE: This method should be
	 * better moved to a DOM inherited Manifest class
	 *
	 * @param internalPath
	 *            The relative package path within the ODF package
	 * @return The manifest file entry will be returned.
	 */
	public OdfFileEntry getFileEntry(String internalPath) {
		internalPath = normalizeFilePath(internalPath);
		return mManifestEntries.get(internalPath);
	}

	/**
	 * Get a OdfFileEntries from the manifest file (i.e. /META/manifest.xml")
	 *
	 * @return The paths of the manifest file entries will be returned.
	 */
	public Set<String> getFilePaths() {
		return mManifestEntries.keySet();
	}

	/**
	 *
	 * Check existence of a file in the package.
	 *
	 * @param internalPath
	 *            The relative package documentURL within the ODF package
	 * @return True if there is an entry and a file for the given documentURL
	 */
	public boolean contains(String internalPath) {
		internalPath = normalizeFilePath(internalPath);
		return mManifestEntries.containsKey(internalPath);
	}

	/**
	 * Save the package to given documentURL.
	 *
	 * @param odfPath
	 *            - the path to the ODF package destination
	 * @throws java.io.IOException
	 *             - if the package could not be saved
	 * @throws SAXException
	 */
	public void save(String odfPath) throws IOException {
		File f = new File(odfPath);
		save(f);
	}

	/**
	 * Save package to a given File.
	 *
	 * @param pkgFile
	 *            - the File to save the ODF package to
	 * @throws java.lang.IOException
	 *             - if the package could not be saved
	 * @throws SAXException
	 */
	public void save(File pkgFile) throws IOException {
		String baseURL = getBaseURLFromFile(pkgFile);
//		if (baseURL.equals(mBaseURI)) {
//			// save to the same file: cache everything first
//			// ToDo: (Issue 219 - PackageRefactoring) --maybe it's better to write to a new file and copy that
//			// to the original one - would be less memory footprint
//			cacheContent();
//		}
		try(FileOutputStream fos = new FileOutputStream(pkgFile)) {
			save(fos, baseURL);
		}
	}

	/**
	 * Saves the package to a given {@link OutputStream}. The given stream is not closed by this method.
	 * @param odfStream the output stream
	 * @throws IOException if an I/O error occurs while saving the package
	 * @throws SAXException
	 */
	public void save(OutputStream odfStream) throws IOException {
		save(odfStream, null);
	}

	/**
	 * Save an ODF document to the OutputStream.
	 *
	 * @param odfStream
	 *            - the OutputStream to insert content to
	 * @param baseURL defining the location of the package
	 * @throws java.io.IOException if an I/O error occurs while saving the package
	 * @throws SAXException
	 */
	private void save(OutputStream odfStream, String baseURL)
			throws IOException {
		mBaseURI = baseURL;
		OdfFileEntry rootEntry = mManifestEntries.get(SLASH);
		if (rootEntry == null) {
			rootEntry = new OdfFileEntry(SLASH, mMediaType);
			mManifestEntries.put(SLASH, rootEntry);
		} else {
			rootEntry.setMediaTypeString(mMediaType);
		}
		ZipOutputStream zos = new ZipOutputStream(odfStream);
		try {
			// remove mediatype path and use it as first
			this.mManifestEntries.remove(OdfFile.MEDIA_TYPE.getPath());
			Set<String> keys = mManifestEntries.keySet();
			Set<String> drawings = new HashSet<>();

			CRC32 crc = new CRC32();
			long modTime = (new java.util.Date()).getTime();

			// ODF requires the "mimetype" file to be at first in the package
			// create "mimetype" from current attribute value
			createZipEntry(OdfFile.MEDIA_TYPE.getPath(), mMediaType.getBytes("UTF-8"), zos, modTime, crc);
			// Create "META-INF/" directory
			createZipEntry("META-INF/", null, zos, modTime, crc);

			for (String path : keys) {
				if (path.startsWith("Pictures/") && !path.equals("Pictures/")) {
					drawings.add(path);
					continue;
				}
				createZipEntry(path, zos, modTime, crc, allUsedDrawings);
			}

			checkUnusedRelations(drawings, allUsedDrawings);

			for (String path : drawings) {
				createZipEntry(path, zos, modTime, crc, null);
			}

			// Create "META-INF/manifest.xml" file
			createZipEntry(OdfFile.MANIFEST.getPath(), getBytes(OdfFile.MANIFEST.getPath()), zos, modTime, crc);
		} finally {
			zos.close();
		}
		odfStream.flush();
	}

	private void createZipEntry(String path, ZipOutputStream zos, long modTime, CRC32 crc, Set<String> usedDrawings)
			throws IOException{
		// not interested to reuse previous mediaType nor manifest from ZIP
		if (!path.endsWith(SLASH) && !path.equals(OdfPackage.OdfFile.MANIFEST.getPath()) && !path.equals(OdfPackage.OdfFile.MEDIA_TYPE.getPath())) {
			createZipEntry(path, getBytes(path, usedDrawings), zos, modTime, crc);
		}
	}

	private void createZipEntry(String path, byte[] data, ZipOutputStream zos, long modTime, CRC32 crc)
			throws IOException {
		ZipEntry ze = mZipEntries.get(path);
		if (ze == null) {
			ze = new ZipEntry(path);
		}
		ze.setTime(modTime);
		if (fileNeedsCompression(path)) {
			ze.setMethod(ZipEntry.DEFLATED);
		} else {
			ze.setMethod(ZipEntry.STORED);
		}
		crc.reset();
		if (data != null) {
			ze.setSize(data.length);
			crc.update(data);
			ze.setCrc(crc.getValue());
		} else {
			ze.setSize(0);
			ze.setCrc(0);
		}
		ze.setCompressedSize(-1);
		zos.putNextEntry(ze);
		if (data != null) {
			zos.write(data, 0, data.length);
		}
		zos.closeEntry();
		mZipEntries.put(path, ze);
	}

	/**
	 * Determines if a file have to be compressed.
	 * @param internalPath the file location
	 * @return true if the file needs compression, false, otherwise
	 */
	private boolean fileNeedsCompression(String internalPath) {
		boolean result = true;

		// ODF spec does not allow compression of "./mimetype" file
		if (internalPath.equals(OdfPackage.OdfFile.MEDIA_TYPE.getPath())) {
			return false;
		}
		// see if the file was already compressed
		if (internalPath.lastIndexOf(".") > 0) {
			String suffix = internalPath.substring(internalPath.lastIndexOf(".") + 1, internalPath.length());
			if (COMPRESSEDFILETYPES.contains(suffix.toLowerCase())) {
				result = false;
			}
		}
		return result;
	}

	/**
	 * Parse the Manifest file
	 */
	private void parseManifest() throws SAXException, IOException {
		try(InputStream is = mZipFile.getInputStream(mZipEntries.get(OdfPackage.OdfFile.MANIFEST.internalPath))) {
			XMLReader xmlReader = getXMLReader();
			xmlReader.setEntityResolver(getEntityResolver());
			xmlReader.setContentHandler(new OdfManifestSaxHandler(this));
			InputSource ins = new InputSource(is);
			String uri = mBaseURI + SLASH + OdfPackage.OdfFile.MANIFEST.internalPath;
			ins.setSystemId(uri);
			xmlReader.parse(ins);
			// ToDo: manifest.xml will be held in the future as DOM, now its being generated for each save()
			mMemoryFileCache.remove(OdfPackage.OdfFile.MANIFEST.internalPath);
		}
	}

	public XMLReader getXMLReader() throws SAXException {
		// create sax parser
		SAXParserFactory saxFactory = new org.apache.xerces.jaxp.SAXParserFactoryImpl();
		saxFactory.setNamespaceAware(true);
		saxFactory.setValidating(false);
		try {
			saxFactory.setXIncludeAware(false);
			saxFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            // removing potential vulnerability: see https://www.owasp.org/index.php/XML_External_Entity_%28XXE%29_Processing
            saxFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            saxFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            saxFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		} catch (Exception ex) {
			Logger.getLogger(OdfPackage.class.getName()).log(Level.SEVERE, null, ex);
			throw new RuntimeException();
		}

		SAXParser parser;
		try {
			parser = saxFactory.newSAXParser();
		} catch (ParserConfigurationException pce) {
			//Re-throw as SAXException in order not to introduce too many checked exceptions
			throw new SAXException(pce);
		}
		XMLReader xmlReader = parser.getXMLReader();
		// More details at http://xerces.apache.org/xerces2-j/features.html#namespaces
		xmlReader.setFeature("http://xml.org/sax/features/namespaces", true);
		// More details at http://xerces.apache.org/xerces2-j/features.html#namespace-prefixes
		xmlReader.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// More details at http://xerces.apache.org/xerces2-j/features.html#xmlns-uris
		xmlReader.setFeature("http://xml.org/sax/features/xmlns-uris", true);
        // removing potential vulnerability: see https://www.owasp.org/index.php/XML_External_Entity_%28XXE%29_Processing
        xmlReader.setFeature("http://xml.org/sax/features/external-general-entities", false);
        xmlReader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        xmlReader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		return xmlReader;
	}

	// Add the given path and all its subdirectories to the internalPath list
	// to be written later to the manifest
	private void createSubEntries(String internalPath) {
		StringTokenizer tok = new StringTokenizer(internalPath, SLASH);
		if (tok.countTokens() > 1) {
			String path = EMPTY_STRING;
			while (tok.hasMoreTokens()) {
				String directory = tok.nextToken();
				// it is a directory, if there are more token
				if (tok.hasMoreTokens()) {
					path = path + directory + SLASH;
					OdfFileEntry fileEntry = mManifestEntries.get(path);
					if (fileEntry == null) {
						mManifestEntries.put(path, new OdfFileEntry(path, null));
					}
				}
			}
		}
	}

	/**
	 * Insert DOM tree into OdfPackage. An existing file will be replaced.
	 *
	 * @param fileDOM
	 *            - XML DOM tree to be inserted as file.
	 * @param internalPath
	 *            - relative documentURL where the DOM tree should be inserted as XML file
	 * @param mediaType
	 *            - media type of stream. Set to null if unknown
	 * @throws java.lang.Exception
	 *             when the DOM tree could not be inserted
	 */
	public void insert(Document fileDOM, String internalPath, String mediaType) {
		internalPath = normalizeFilePath(internalPath);
		if (mediaType == null) {
			mediaType = XML_MEDIA_TYPE;
		}
		if (fileDOM == null) {
			mPkgDoms.remove(internalPath);
		} else {
			mPkgDoms.put(internalPath, fileDOM);
		}
		updateFileEntry(ensureFileEntryExistence(internalPath), mediaType);
		// remove byte array version of new DOM
		mMemoryFileCache.remove(internalPath);
	}

	/**
	 * Embed an OdfPackageDocument to the current OdfPackage.
	 * All the file entries of child document will be inserted.
	 * @param sourceDocument the OdfPackageDocument to be embedded.
	 * @param internalPath path to the directory the ODF document should be inserted (relative to ODF package root).
	 */
	public void insertDocument(OdfPackageDocument sourceDocument, String internalPath) {
		internalPath = normalizeDirectoryPath(internalPath);
		// opened DOM of descendant Documents will be flashed to the their pkg
		flushDoms(sourceDocument);

		// Gets the OdfDocument's manifest entry info, no matter it is a independent document or an embeddedDocument.
		Map<String, OdfFileEntry> entryMapToCopy;
		if (sourceDocument.isRootDocument()) {
			entryMapToCopy = sourceDocument.getPackage().getManifestEntries();
		} else {
			entryMapToCopy = sourceDocument.getPackage().getSubDirectoryEntries(sourceDocument.getDocumentPath());
		}
		//insert to package and add it to the Manifest
		internalPath = sourceDocument.setDocumentPath(internalPath);
		String documentDirectory = null;
		if(internalPath.equals(SLASH)){
			documentDirectory = EMPTY_STRING;
		}else{
			documentDirectory = internalPath;
		}
		Set<String> entryNameList = entryMapToCopy.keySet();
		for (String entryName : entryNameList) {
			OdfFileEntry entry = entryMapToCopy.get(entryName);
			if (entry != null) {
				try {
					// if entry is a directory (e.g. an ODF document root)
					if (entryName.endsWith(SLASH)) {
						// insert directory
						if (entryName.equals(SLASH)) {
							insert((byte[]) null, documentDirectory, sourceDocument.getMediaTypeString());
						} else {
							insert((byte[]) null, documentDirectory + entry.getPath(), entry.getMediaTypeString());
						}
					} else {
						String packagePath = documentDirectory + entry.getPath();
						insert(sourceDocument.getPackage().getInputStream(entryName), packagePath, entry.getMediaTypeString());
					}
				} catch (IOException ex) {
					//Catching IOException should be fine here: in-memory operations only
					Logger.getLogger(OdfPackage.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		//make sure the media type of embedded Document is right set.
		OdfFileEntry embedDocumentRootEntry = new OdfFileEntry(internalPath, sourceDocument.getMediaTypeString());
		mManifestEntries.put(internalPath, embedDocumentRootEntry);
		// the new document will be attached to its new package (it has been inserted to)
		sourceDocument.setPackage(this);
		cacheDocument(sourceDocument, internalPath);
	}

	/**
	 * Insert all open DOMs of XML files beyond parent document to the package.
	 * The XML files will be updated in the package after calling save.
	 *
	 * @param parentDocument the document, which XML files shall be serialized
	 */
	void flushDoms(OdfPackageDocument parentDocument) {
		OdfPackage pkg = parentDocument.getPackage();
		if (parentDocument.isRootDocument()) {
			// for every parsed XML file (DOM)
			for (String xmlFilePath : pkg.getCachedDoms().keySet()) {
				// insert it to the package (serializing and caching it till final save)
				pkg.insert(pkg.getCachedDom(xmlFilePath), xmlFilePath, "text/xml");
			}
		} else {
			// if not root document, check ..
			String parentDocumentPath = parentDocument.getDocumentPath();
			// for every parsed XML file (DOM)
			for (String xmlFilePath : pkg.getCachedDoms().keySet()) {
				// if the file is within the given document
				if (xmlFilePath.startsWith(parentDocumentPath)) {
					// insert it to the package (serializing and caching it till final save)
					pkg.insert(pkg.getCachedDom(xmlFilePath), xmlFilePath, "text/xml");
				}
			}
		}
	}

	/** Get all the file entries from a sub directory */
	private Map<String, OdfFileEntry> getSubDirectoryEntries(String directory) {
		directory = normalizeDirectoryPath(directory);
		Map<String, OdfFileEntry> subEntries = new HashMap<String, OdfFileEntry>();
		Map<String, OdfFileEntry> allEntries = getManifestEntries();
		Set<String> rootEntryNameSet = getFilePaths();
		for (String entryName : rootEntryNameSet) {
			if (entryName.startsWith(directory)) {
				String newEntryName = entryName.substring(directory.length());
				if (newEntryName.length() == 0) {
					continue;
				}
				OdfFileEntry srcFileEntry = allEntries.get(entryName);
				OdfFileEntry newFileEntry = new OdfFileEntry();
				newFileEntry.setEncryptionData(srcFileEntry.getEncryptionData());
				newFileEntry.setMediaTypeString(srcFileEntry.getMediaTypeString());
				newFileEntry.setPath(newEntryName);
				newFileEntry.setSize(srcFileEntry.getSize());
				subEntries.put(entryName, newFileEntry);
			}
		}
		return subEntries;
	}

	/**
	 * Method returns the paths of all document within the package.
	 *
	 * @return A set of paths of all documents of the package, including the root document.
	 */
	public Set<String> getDocumentPaths() {
		return getDocumentPaths(null, null);
	}

	/**
	 * Method returns the paths of all document within the package matching the given criteria.
	 *
	 * @param mediaTypeString limits the desired set of document paths to documents of the given mediaType
	 * @return A set of paths of all documents of the package, including the root document, that match the given parameter.
	 */
	public Set<String> getDocumentPaths(String mediaTypeString) {
		return getDocumentPaths(mediaTypeString, null);
	}

	/**
	 * Method returns the paths of all document within the package matching the given criteria.
	 *
	 * @param mediaTypeString limits the desired set of document paths to documents of the given mediaType
	 * @param subDirectory limits the desired set document paths to those documents below of this subdirectory
	 * @return A set of paths of all documents of the package, including the root document, that match the given parameter.
	 */
	Set<String> getDocumentPaths(String mediaTypeString, String subDirectory) {
		Set<String> innerDocuments = new HashSet<String>();
		Set<String> packageFilePaths = getFilePaths();
		// check manifest for current embedded OdfPackageDocuments
		for (String filePath : packageFilePaths) {
			// check if a subdirectory was the criteria and if the files are beyond the given subdirectory
			if (subDirectory == null || filePath.startsWith(subDirectory) && !filePath.equals(subDirectory)) {
				// with documentURL is not empty and is a directory (ie. a potential document)
				if (filePath.length() > 1 && filePath.endsWith(SLASH)) {
					String fileMediaType = getFileEntry(filePath).getMediaTypeString();
					if (fileMediaType != null && !fileMediaType.equals(EMPTY_STRING)) {
						// check if a certain mediaType was the critera and was matched
						if (mediaTypeString == null || mediaTypeString.equals(fileMediaType)) {
							// only relative path is allowed as path
							innerDocuments.add(filePath);
						}
					}
				}
			}
		}
		return innerDocuments;
	}

	/**
	 * Adding a manifest:file-entry to be saved in manifest.xml.
	 * In addition, sub directories will be added as well to the manifest.
	 */
	private OdfFileEntry ensureFileEntryExistence(String internalPath) {
		// if it is NOT the resource "/META-INF/manifest.xml"
		OdfFileEntry fileEntry = null;
		if (!OdfPackage.OdfFile.MANIFEST.internalPath.equals(internalPath) ||
			!internalPath.equals(EMPTY_STRING)) {
			if (mManifestEntries == null) {
				mManifestEntries = new HashMap<String, OdfFileEntry>();
			}
			fileEntry = mManifestEntries.get(internalPath);
			// for every new file entry
			if (fileEntry == null) {
				fileEntry = new OdfFileEntry(internalPath);
				mManifestEntries.put(internalPath, fileEntry);
				// creates recursive file entries for all sub directories
				createSubEntries(internalPath);
			}
		}
		return fileEntry;
	}

	/**
	 * update file entry setting.
	 */
	private void updateFileEntry(OdfFileEntry fileEntry, String mediaType) {
		// overwrite previous settings
		fileEntry.setMediaTypeString(mediaType);
		// reset encryption data (ODFDOM does not support encryption yet)
		fileEntry.setEncryptionData(null);
		// reset size to be unset
		fileEntry.setSize(-1);
	}

	public static DocumentBuilder getDocumentBuilder(boolean namespaceAware)
	    throws ParserConfigurationException {

	    DocumentBuilderFactory factory = new org.apache.xerces.jaxp.DocumentBuilderFactoryImpl();
        factory.setNamespaceAware(namespaceAware);
        factory.setValidating(false);
        try {
            factory.setXIncludeAware(false);
            factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            // removing potential vulnerability: see https://www.owasp.org/index.php/XML_External_Entity_%28XXE%29_Processing
            factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        } catch (Exception ex) {
            Logger.getLogger(OdfPackage.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException();
        }
        return factory.newDocumentBuilder();
	}

	/**
	 * Gets org.w3c.dom.Document for XML file contained in package.
	 *
	 * @param internalPath to a file within the Odf Package (eg. content.xml)
	 * @return an org.w3c.dom.Document
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws IllegalArgumentException
	 * @throws TransformerConfigurationException
	 * @throws TransformerException
	 */
	public Document getDom(String internalPath) throws SAXException,
			ParserConfigurationException, IllegalArgumentException, IOException {

		Document doc = mPkgDoms.get(internalPath);
		if (doc != null) {
			return doc;
		}

		InputStream is = getInputStream(internalPath);

		final DocumentBuilder builder = getDocumentBuilder(true);
		builder.setEntityResolver(getEntityResolver());

		String uri = getBaseURI() + internalPath;

		InputSource ins = new InputSource(is);
		ins.setSystemId(uri);

		doc = builder.parse(ins);

		if (doc != null) {
			mPkgDoms.put(internalPath, doc);
			mMemoryFileCache.remove(internalPath);
		}
		return doc;
	}

	/**
	 * Inserts an external file into an OdfPackage. An existing file will be
	 * replaced.
	 *
	 * @param sourceURI
	 *            - the source URI to the file to be inserted into the package.
	 * @param internalPath
	 *            - relative documentURL where the tree should be inserted as XML
	 *            file
	 * @param mediaType
	 *            - media type of stream. Set to null if unknown
	 * @throws java.io.IOException
	 *             In case the file could not be saved
	 */
	public void insert(URI sourceURI, String internalPath, String mediaType)
			throws IOException {
		InputStream is = null;
		if (sourceURI.isAbsolute()) {
			// if the URI is absolute it can be converted to URL
			is = sourceURI.toURL().openStream();
		} else {
			// otherwise create a file class to open the stream
			is = new FileInputStream(sourceURI.toString());
		}
		insert(is, internalPath, mediaType);
	}

	/**
	 * Inserts InputStream into an OdfPackage. An existing file will be
	 * replaced.
	 *
	 * @param fileStream
	 *            - the stream of the file to be inserted into the ODF package.
	 * @param internalPath
	 *            - relative documentURL where the tree should be inserted as XML
	 *            file
	 * @param mediaType
	 *            - media type of stream. Set to null if unknown
	 */
	public void insert(InputStream fileStream, String internalPath, String mediaType) throws IOException {
		internalPath = normalizeFilePath(internalPath);
		if (fileStream == null) {
			//adding a simple directory without MIMETYPE
			insert((byte[]) null, internalPath, mediaType);
		} else {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			BufferedInputStream bis = null;
			if (fileStream instanceof BufferedInputStream) {
				bis = (BufferedInputStream) fileStream;
			} else {
				bis = new BufferedInputStream(fileStream);
			}
			StreamHelper.transformStream(bis, baos);
			byte[] data = baos.toByteArray();
			insert(data, internalPath, mediaType);
		}
	}

	/**
	 * Inserts a byte array into OdfPackage. An existing file will be replaced.
	 * If the byte array is NULL a directory with the given mimetype will be created.
	 *
	 * @param fileBytes
	 *      - data of the file stream to be stored in package.
	 *		If NULL a directory with the given mimetype will be created.
	 * @param internalPath
	 *      - path of the file or directory relative to the package root.
	 * @param mediaTypeString
	 *      - media type of stream. If unknown null can be used.
	 */
	public void insert(byte[] fileBytes, String internalPath, String mediaTypeString) {
		internalPath = normalizeFilePath(internalPath);
		if (OdfPackage.OdfFile.MEDIA_TYPE.getPath().equals(internalPath)) {
			try {
				setMediaTypeString(new String(fileBytes, "UTF-8"));
			} catch (UnsupportedEncodingException useEx) {
				Logger.getLogger(OdfPackage.class.getName()).log(Level.SEVERE,
						"ODF file could not be created as string!", useEx);
			}
			return;
		}
		if (fileBytes != null) {
			mMemoryFileCache.put(internalPath, fileBytes);
			// as DOM would overwrite data cache, any existing DOM cache will be deleted
			if (mPkgDoms.containsKey(internalPath)) {
				mPkgDoms.remove(internalPath);
			}
		}
		updateFileEntry(ensureFileEntryExistence(internalPath), mediaTypeString);
	}

	// changed to package access as the manifest interiors are an implementation detail
	Map<String, OdfFileEntry> getManifestEntries() {
		return mManifestEntries;
	}

	/**
	 * Get Manifest as String NOTE: This functionality should better be moved to
	 * a DOM based Manifest class
	 *
	 * @return the /META-INF/manifest.xml as a String
	 */
	public String getManifestAsString() {
		if (mManifestEntries == null) {
			return null;
		}
		StringBuilder buf = new StringBuilder();
		buf.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		buf.append("<manifest:manifest xmlns:manifest=\"urn:oasis:names:tc:opendocument:xmlns:manifest:1.0\"");
// REMOVING VERSION DUE TO MS OFFICE 2010 issue
//                        if(mManifestVersion != null){
//                            buf.append(" manifest:version=\"" + mManifestVersion + "\"");
//                        }
                    buf.append(" >\n");
		Iterator<String> it = new TreeSet<String>(mManifestEntries.keySet()).iterator();
		while (it.hasNext()) {
			String key = it.next();
			String s = null;
			OdfFileEntry fileEntry = mManifestEntries.get(key);
			if (fileEntry != null) {
				s = fileEntry.getPath();
				// only directories with a mimetype (documents) will be written into the manifest.xml
				if (s != null && !s.endsWith(SLASH) || !fileEntry.getMediaTypeString().equals(EMPTY_STRING)) {
					buf.append(" <manifest:file-entry");
					if (s != null) {
						buf.append(" manifest:full-path=\"");
						buf.append(encodeXMLAttributes(s));
						buf.append("\"");

					}
					s = fileEntry.getMediaTypeString();
					buf.append(" manifest:media-type=\"");
					buf.append(encodeXMLAttributes(s));
					buf.append("\"");

					int i = fileEntry.getSize();
					if (i > 0) {
						buf.append(" manifest:size=\"");
						buf.append(i);
						buf.append("\"");
					}
					EncryptionData enc = fileEntry.getEncryptionData();
					if (enc != null) {
						buf.append(">\n");
						buf.append("  <manifest:encryption-data>\n");
						Algorithm alg = enc.getAlgorithm();
						if (alg != null) {
							buf.append("   <manifest:algorithm");
							s = alg.getName();
							if (s != null) {
								buf.append(" manifest:algorithm-name=\"");
								buf.append(encodeXMLAttributes(s));
								buf.append("\"");
							}
							s = alg.getInitializationVector();
							if (s != null) {
								buf.append(" manifest:initialization-vector=\"");
								buf.append(encodeXMLAttributes(s));
								buf.append("\"");
							}
							buf.append("/>\n");
						}
						KeyDerivation keyDerivation = enc.getKeyDerivation();
						if (keyDerivation != null) {
							buf.append("   <manifest:key-derivation");
							s = keyDerivation.getName();
							if (s != null) {
								buf.append(" manifest:key-derivation-name=\"");
								buf.append(encodeXMLAttributes(s));
								buf.append("\"");
							}
							s = keyDerivation.getSalt();
							if (s != null) {
								buf.append(" manifest:salt=\"");
								buf.append(encodeXMLAttributes(s));
								buf.append("\"");
							}
							buf.append(" manifest:iteration-count=\"");
							buf.append(keyDerivation.getIterationCount());
							buf.append("\"/>\n");
						}
						buf.append("  </manifest:encryption-data>\n");
						buf.append(" </<manifest:file-entry>\n");
					} else {
						buf.append("/>\n");
					}
				}
			}
		}
		buf.append("</manifest:manifest>");
		return buf.toString();
	}

	/**
	 * Get package (sub-) content as byte array
	 *
	 * @param internalPath relative documentURL to the package content
	 * @return the unzipped package content as byte array
	 */
	public byte[] getBytes(String internalPath) {
		return getBytes(internalPath, null);
	}

	/**
	 * Get package (sub-) content as byte array
	 *
	 * @param internalPath relative documentURL to the package content
	 * @return the unzipped package content as byte array
	 */
	public byte[] getBytes(String internalPath, Set<String> usedDrawings) {
		// if path is null or empty return null
		if (internalPath == null || internalPath.equals(EMPTY_STRING)) {
			return null;
		}
		internalPath = normalizeFilePath(internalPath);
		byte[] data = null;
		// if the file is "mimetype"
		if (internalPath.equals(OdfPackage.OdfFile.MEDIA_TYPE.getPath())) {
			if (mMediaType == null) {
				return null;
			}
			try {
				data = mMediaType.getBytes("UTF-8");
			} catch (UnsupportedEncodingException use) {
				Logger.getLogger(OdfPackage.class.getName()).log(Level.SEVERE, null, use);
				return null;
			}
			// if the file is "/META-INF/manifest.xml"
		} else if (internalPath.equals(OdfPackage.OdfFile.MANIFEST.internalPath)) {
			if (mManifestEntries == null) {
				// manifest was not present
				return null;
			}
			String s = getManifestAsString();
			if (s == null) {
				return null;
			}
			try {
				data = s.getBytes("UTF-8");
			} catch (UnsupportedEncodingException ex) {
				Logger.getLogger(OdfPackage.class.getName()).log(Level.SEVERE, null, ex);
			}
			// if the path is already loaded as DOM (highest priority)
		} else if (mPkgDoms.get(internalPath) != null) {
			data = flushDom(mPkgDoms.get(internalPath));
			mMemoryFileCache.put(internalPath, data);
			// if the path's file was cached to memory (second high priority)
		} else if (mManifestEntries.containsKey(internalPath)
				&& mMemoryFileCache.get(internalPath) != null) {
			data = mMemoryFileCache.get(internalPath);

			// if the path's file was cached to disc (lowest priority)
		}
		// if not available, check if file exists in ZIP
		if (data == null) {
			ZipEntry entry = null;
			if ((entry = mZipEntries.get(internalPath)) != null) {
				InputStream inputStream = null;
				try {
					inputStream = mZipFile.getInputStream(entry);
					if (inputStream != null) {
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						StreamHelper.transformStream(inputStream, out);
						data = out.toByteArray();
						// store for further usage; do not care about manifest: that
						// is handled exclusively
						mMemoryFileCache.put(internalPath, data);
					}
				} catch (IOException ex) {
					//Catching IOException here should be fine: in-memory operations only
					Logger.getLogger(OdfPackage.class.getName()).log(Level.SEVERE, null, ex);
				} finally {
					try {
						if (inputStream != null) {
							inputStream.close();
						}
					} catch (IOException ex) {
						Logger.getLogger(OdfPackage.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}
		if (null != data && null != usedDrawings && CLEANUP_FILES.contains(internalPath)) {
			CharMatcher.findAllMatches(data, HREF_PATTERN, '"', usedDrawings, HREF_PATTERN.length);
		}
		return data;
	}

	private class PackageFilter implements LSSerializerFilter {

		private final SerializationHandler hdl;
		private int val = 0;
		public PackageFilter(SerializationHandler o) {
			hdl = o;
		}

		@Override
		public short acceptNode(Node nodeArg) {
			if(nodeArg instanceof IElementWriter) {
				// acceptNode is called twice (beginNode and endNode) ... we will
				// only write the object once
				if(((++val)&1)!=0) {
					try {
						((IElementWriter)nodeArg).writeObject(hdl);
					}
					catch(SAXException e) {

					}
				}
				return LSParserFilter.FILTER_REJECT;
			}
			return 0;
		}

		@Override
		public int getWhatToShow() {
			// TODO Auto-generated method stub
			return LSSerializerFilter.SHOW_ELEMENT;
		}

	}

	// Serializes a DOM tree into a byte array.
	// Providing the counterpart of the generic Namespace handling of OdfFileDom.
	private byte[] flushDom(Document dom) {
		// if it is one of our DOM files we may flush all collected namespaces to the root element
		if (dom instanceof OdfFileDom) {
			OdfFileDom odfDom = (OdfFileDom) dom;
			Map<String, String> nsByUri = odfDom.getMapNamespacePrefixByUri();
			ElementNSImpl root = odfDom.getRootElement();
			if (root != null) {
				for (Entry<String, String> entry : nsByUri.entrySet()) {
					root.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:" + entry.getValue(), entry.getKey());
				}
			}
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		// DOMXSImplementationSourceImpl dis = new org.apache.xerces.dom.DOMXSImplementationSourceImpl();
		// DOMImplementationLS impl = new org.odftoolkit.odfdom.pkg.serializer.dom3.LSSerializerImpl(null);
		//(DOMImplementationLS) dis.getDOMImplementation("LS");

		// creating our own LSSerializerImpl as we need access to the SerializationHandler
		final org.odftoolkit.odfdom.pkg.serializer.dom3.LSSerializerImpl writer = new org.odftoolkit.odfdom.pkg.serializer.dom3.LSSerializerImpl();
		final LSOutput output = new DOMOutputImpl();
		writer.setFilter(new PackageFilter(writer.getSerializationHandler()));
		output.setByteStream(baos);
		writer.write(dom, output);
		return baos.toByteArray();
	}

	/**
	 * Get the latest version of package content as InputStream, as it would be saved.
	 * This might not be the original version once loaded from the package.
	 *
	 * @param internalPath
	 *            of the desired stream.
	 * @return Inputstream of the ODF file within the package for the given
	 *         path.
	 */
	public InputStream getInputStream(String internalPath) {
		internalPath = normalizeFilePath(internalPath);
		// else we always cache here and return a ByteArrayInputStream because
		// if
		// we would return ZipFile getInputStream(entry) we would not be
		// able to read 2 Entries at the same time. This is a limitation of the
		// ZipFile class.
		// As it would be quite a common thing to read the content.xml and the
		// styles.xml
		// simultaneously when using XSLT on OdfPackages we want to circumvent
		// this limitation
		byte[] data = getBytes(internalPath);
		if (data != null && data.length != 0) {
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			return bais;
		}
		return null;
	}

	/**
	 * Get the latest version of package content as InputStream, as it would be saved.
	 * This might not be the original version once loaded from the package.
	 *
	 * @param internalPath
	 *            of the desired stream.
	 * @param useOriginal true uses the stream as loaded from the ZIP.
	 *				False will return even modified file content as a stream.
	 * @return Inputstream of the ODF file within the package for the given
	 *         path.
	 */
	public InputStream getInputStream(String internalPath, boolean useOriginal) {
		InputStream stream = null;
		if (useOriginal) {
			ZipEntry entry = mOriginalZipEntries.get(internalPath);
			if (entry != null) {
				try {
					stream = mZipFile.getInputStream(entry);
				} catch (IOException ex) {
					//Catching IOException here should be fine: in-memory operations only
					Logger.getLogger(OdfPackage.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		} else {
			stream = getInputStream(internalPath);
		}
		return stream;
	}

	/**
	 * Gets the InputStream containing whole OdfPackage.
	 *
	 * @return the ODF package as input stream
	 * @throws java.io.IOException
	 *             - if the package could not be read
	 * @throws SAXException
	 */
	public InputStream getInputStream()
			throws IOException, SAXException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		save(out, mBaseURI);
		return new ByteArrayInputStream(out.toByteArray());
	}

	/**
	 * Insert the OutputStream for into OdfPackage. An existing file will be
	 * replaced.
	 *
	 * @param internalPath
	 *            - relative documentURL where the DOM tree should be inserted as
	 *            XML file
	 * @return outputstream for the data of the file to be stored in package
	 * @throws java.lang.Exception
	 *             when the DOM tree could not be inserted
	 */
	public OutputStream insertOutputStream(String internalPath) throws Exception {
		return insertOutputStream(internalPath, null);
	}

	/**
	 * Insert the OutputStream - to be filled after method - when stream is
	 * closed into OdfPackage. An existing file will be replaced.
	 *
	 * @param internalPath
	 *            - relative documentURL where the DOM tree should be inserted as
	 *            XML file
	 * @param mediaType
	 *            - media type of stream
	 * @return outputstream for the data of the file to be stored in package
	 * @throws java.lang.Exception
	 *             when the DOM tree could not be inserted
	 */
	public OutputStream insertOutputStream(String internalPath, String mediaType) {
		internalPath = normalizeFilePath(internalPath);
		final String fPath = internalPath;
		final OdfFileEntry fFileEntry = getFileEntry(internalPath);
		final String fMediaType = mediaType;

		ByteArrayOutputStream baos = new ByteArrayOutputStream() {

			@Override
			public void close() throws IOException {
				byte[] data = this.toByteArray();
				if (fMediaType == null || fMediaType.length() == 0) {
					insert(data, fPath, fFileEntry == null ? null
							: fFileEntry.getMediaTypeString());
				} else {
					insert(data, fPath, fMediaType);
				}
				super.close();
			}
		};
		return baos;
	}

	/** Removes a single file from the package.
	 * @param internalPath of the file relative to the package root
	 */
	public void remove(String internalPath) {
        internalPath = normalizePath(internalPath);

        if (mZipEntries != null) {
            mZipEntries.remove(internalPath);
        }
        if (mManifestEntries != null) {
            mManifestEntries.remove(internalPath);
        }

        if (mPkgDocuments != null) {
            mPkgDocuments.remove(internalPath);
        }

        if (mPkgDoms != null) {
            mPkgDoms.remove(internalPath);
        }

        if (mMemoryFileCache != null) {
            mMemoryFileCache.remove(internalPath);
        }
	}


	/** Get the size of an internal file from the package.
	 * @param internalPath of the file relative to the package root
	 * @return the size of the file in bytes or -1 if the size could not be received.
	 */
	public long getSize(String internalPath) {
		long size = -1;
		internalPath = normalizePath(internalPath);
		if (mZipEntries != null && mZipEntries.containsKey(internalPath)) {
			ZipEntry zipEntry = mZipEntries.get(internalPath);
			size = zipEntry.getSize();;
		}
		return size;
	}

	/**
	 * Encoded XML Attributes
	 */
	private String encodeXMLAttributes(String attributeValue) {
		String encodedValue = QUOTATION_PATTERN.matcher(attributeValue).replaceAll(ENCODED_QUOTATION);
		encodedValue = APOSTROPHE_PATTERN.matcher(encodedValue).replaceAll(ENCODED_APOSTROPHE);
		return encodedValue;
	}

	/**
	 * Get EntityResolver to be used in XML Parsers which can resolve content
	 * inside the OdfPackage
	 *
	 * @return a SAX EntityResolver
	 */
	public EntityResolver getEntityResolver() {
		if (mResolver == null) {
			mResolver = new Resolver(this);
		}
		return mResolver;
	}

	/**
	 * Get URIResolver to be used in XSL Transformations which can resolve
	 * content inside the OdfPackage
	 *
	 * @return a TraX Resolver
	 */
	public URIResolver getURIResolver() {
		if (mResolver == null) {
			mResolver = new Resolver(this);
		}
		return mResolver;
	}

	private static String getBaseURLFromFile(File file) throws IOException {
		String baseURL = file.getCanonicalFile().toURI().toString();
		baseURL = BACK_SLASH_PATTERN.matcher(baseURL).replaceAll(SLASH);
		return baseURL;
	}

	/**
	 * Ensures that the given file path is not null nor empty and not an external reference
	 *	<ol>
	 *	<li>All backslashes "\" are exchanged by slashes "/"</li>
	 *  <li>Any substring "/../", "/./" or "//" will be removed</li>
	 *	<li>A prefix "./" and "../" will be removed</li>
	 *  </ol>
	 *
	 * @throws IllegalArgumentException If the path is NULL, empty or an external path (e.g. starting with "../" is given).
	 *  None relative URLs will NOT throw an exception.
	 * @return the normalized path or the URL
	 */
	static String normalizeFilePath(String internalPath) {
		if (internalPath.equals(EMPTY_STRING)) {
			String errMsg = "The internalPath given by parameter is an empty string!";
			Logger.getLogger(OdfPackage.class.getName()).severe(errMsg);
			throw new IllegalArgumentException(errMsg);
		}
		return normalizePath(internalPath);
	}

	/**
	 * Ensures the given directory path is not null nor an external reference to resources outside the package.
	 * An empty path and slash "/" are both mapped to the root directory/document.
	 *
	 * NOTE: Although ODF only refer the "/" as root,
	 * the empty path aligns more adequate with the file system concept.
	 *
	 * To ensure the given directory path within the package can be used as a key (is unique for the Package) the path will be normalized.
	 * @see #normalizeFilePath(String)
	 * In addition to the file path normalization a trailing slash will be used for directories.
	 */
	static String normalizeDirectoryPath(String directoryPath) {
		directoryPath = normalizePath(directoryPath);
		// if not the root document - which is from ODF view a '/' and no
		// trailing '/'
		if (!directoryPath.equals(OdfPackageDocument.ROOT_DOCUMENT_PATH)
				&& !directoryPath.endsWith(SLASH)) {
			// add a trailing slash
			directoryPath = directoryPath + SLASH;
		}
		return directoryPath;
	}

	/** Normalizes both directory and file path */
	static String normalizePath(String path) {
		if (path == null) {
			String errMsg = "The internalPath given by parameter is NULL!";
			Logger.getLogger(OdfPackage.class.getName()).severe(errMsg);
			throw new IllegalArgumentException(errMsg);
		} else if (!mightBeExternalReference(path)) {
			if (path.equals(EMPTY_STRING)) {
				path = SLASH;
			} else {
				// exchange all backslash "\" with a slash "/"
				if (path.indexOf('\\') != -1) {
					path = BACK_SLASH_PATTERN.matcher(path).replaceAll(SLASH);
				}
				// exchange all double slash "//" with a slash "/"
				while (path.indexOf("//") != -1) {
					path = DOUBLE_SLASH_PATTERN.matcher(path).replaceAll(SLASH);
				}
				// if directory replacements (e.g. ..) exist, resolve and remove them
				if (path.indexOf("/.") != -1 || path.indexOf("./") != -1) {
					path = removeChangeDirectories(path);
				}
			}
		}
		return path;
	}

	/** Normalizes both directory and file path */
	private static boolean mightBeExternalReference(String internalPath) {
		boolean isExternalReference = false;
		// if the fileReference is a external relative documentURL..
		if (internalPath.startsWith(DOUBLE_DOT)
				|| // or absolute documentURL AND not root document
				internalPath.startsWith(SLASH) && !internalPath.equals(SLASH)
				|| // or absolute IRI
				internalPath.contains(COLON)) {
			isExternalReference = true;
		}
		return isExternalReference;
	}

	/** Resolving the directory replacements (ie. "/../" and "/./") with a slash "/" */
	private static String removeChangeDirectories(String path) {
		boolean isDirectory = path.endsWith(SLASH);
		StringTokenizer tokenizer = new StringTokenizer(path, SLASH);
		int tokenCount = tokenizer.countTokens();
		List<String> tokenList = new ArrayList<String>(tokenCount);
		// add all paths to a list
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			tokenList.add(token);
		}
		if (!isDirectory) {
			String lastPath = tokenList.get(tokenCount - 1);
			if (lastPath.equals(DOT) || lastPath.equals(DOUBLE_DOT)) {
				isDirectory = true;
			}
		}
		String currentToken;
		int removeDirLevel = 0;
		StringBuilder out = new StringBuilder();
		// work on the list from back to front
		for (int i = tokenCount - 1; i >= 0; i--) {
			currentToken = tokenList.get(i);
			// every ".." will remove an upcoming path
			if (currentToken.equals(DOUBLE_DOT)) {
				removeDirLevel++;
			} else if (currentToken.equals(DOT)) {
			} else {
				// if a path have to be remove, neglect current path
				if (removeDirLevel > 0) {
					removeDirLevel--;
				} else {
					// add the path segment
					out.insert(0, SLASH);
					out.insert(0, currentToken);
				}
			}
		}
		if (removeDirLevel > 0) {
			return EMPTY_STRING;
		}
		if (!isDirectory) {
			// remove trailing slash /
			out.deleteCharAt(out.length() - 1);
		}
		return out.toString();
	}

	/**
	 * Checks if the given reference is a reference, which points outside the
	 * ODF package
	 *
	 * @param internalPath
	 *            the file reference to be checked
	 * @return true if the reference is an package external reference
	 */
	public static boolean isExternalReference(String internalPath) {
		if (mightBeExternalReference(internalPath)) {
			return true;
		}
		return mightBeExternalReference(normalizePath(internalPath));
	}

	public Pair<Integer, Integer> getPixelSize(String imageUrl) {
	    if(pixelSizes.containsKey(imageUrl)) {
	        return pixelSizes.get(imageUrl);
	    }
	    final Pair<Integer, Integer> pixelSize = Tools.getPixelSizeFromImage(getInputStream(imageUrl));
	    pixelSizes.put(imageUrl, pixelSize);
        return pixelSize;
	}

	public Pair<Double, Double> getDPIFromImage(String imageUrl) {
        if(dpiFromImage.containsKey(imageUrl)) {
            return dpiFromImage.get(imageUrl);
        }
        final Pair<Double, Double> dpi = Tools.getDPIFromImage(getInputStream(imageUrl));
        dpiFromImage.put(imageUrl, dpi);
        return dpi;
	}

	/** @param odfVersion parsed from the manifest */
	void setManifestVersion(String odfVersion){
		mManifestVersion = odfVersion;
	}

	/** @return the ODF version found in the manifest.
	 * Meant to be used to reuse when the manifest is recreated */
	String getManifestVersion(){
		return mManifestVersion;
	}

	public void setCheckUnusedRelations(boolean checkUnusedRelations) {
		if (checkUnusedRelations) {
			allUsedDrawings = new HashSet<>();
		} else {
			allUsedDrawings = null;
		}

	}

	private void checkUnusedRelations(Set<String> drawings, Set<String> usedDrawings) {
		if (null != usedDrawings) {
			Set<String> toDel = new HashSet<>();

			for(String draw : drawings) {
				String search = "xlink:href=\"" + draw;
				if (!usedDrawings.contains(search)) {
					toDel.add(draw);

					mZipEntries.remove(draw);
					mManifestEntries.remove(draw);
				}
			}
			drawings.removeAll(toDel);
		}

	}
}
