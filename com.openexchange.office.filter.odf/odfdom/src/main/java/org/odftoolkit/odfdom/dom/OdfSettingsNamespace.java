

package org.odftoolkit.odfdom.dom;

import org.odftoolkit.odfdom.pkg.NamespaceName;

/**
 * Namespaces of OpenDocument 1.2 XML Schema
 */
public enum OdfSettingsNamespace implements NamespaceName {

	OFFICE("office", "urn:oasis:names:tc:opendocument:xmlns:office:1.0"),
	XLINK("xlink", "http://www.w3.org/1999/xlink"),
	CONFIG("config", "urn:oasis:names:tc:opendocument:xmlns:config:1.0"),
	OOO("ooo", "http://openoffice.org/2004/office");

	private String mPrefix;
	private String mUri;

	OdfSettingsNamespace(String prefix, String uri) {
		mPrefix = prefix;
		mUri = uri;
	}

	/**
	 * @return the prefix by default related to ODF Namespace.
	 */
	@Override
    public String getPrefix() {
		return mPrefix;
	}

	/**
	 * @return the URI identifiying the ODF Namespace.
	 */
	@Override
    public String getUri() {
		return mUri;
	}

	/**
	 * @return the prefix by default related to ODF Namespace
	 */
	@Override
    public String toString() {
		return mPrefix;
	}
}
