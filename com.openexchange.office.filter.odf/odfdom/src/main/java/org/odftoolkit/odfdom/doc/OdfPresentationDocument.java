/************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * Copyright 2008, 2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0. You can also
 * obtain a copy of the License at http://odftoolkit.org/docs/license.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ************************************************************************/
package org.odftoolkit.odfdom.doc;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.odftoolkit.odfdom.pkg.MediaType;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.draw.DrawFrame;
import com.openexchange.office.filter.odf.odp.dom.DrawingPage;
import com.openexchange.office.filter.odf.odp.dom.JsonOperationConsumer;
import com.openexchange.office.filter.odf.odp.dom.JsonOperationProducer;
import com.openexchange.office.filter.odf.odp.dom.MasterPage;
import com.openexchange.office.filter.odf.odp.dom.Page;
import com.openexchange.office.filter.odf.odp.dom.PresentationContent;
import com.openexchange.office.filter.odf.odp.dom.PresentationStyles;
import com.openexchange.office.filter.odf.odp.dom.components.RootComponent;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleGraphic;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StylePresentation;
import com.openexchange.office.filter.odf.styles.TextListStyle;

public class OdfPresentationDocument extends OdfDocument {

	private static final String EMPTY_PRESENTATION_DOCUMENT_PATH = "/OdfPresentationDocument.odp";
    static final Resource EMPTY_PRESENTATION_DOCUMENT_RESOURCE = new Resource(EMPTY_PRESENTATION_DOCUMENT_PATH);

	@Override
	public String getDocumentType() {
	    return "presentation";
	}

	public TextListStyle getDefaultTextListStyle(OdfOperationDoc operationDocument, String type, Page page, boolean clone)
	    throws SAXException {

	    final boolean content = !(page instanceof MasterPage);
	    if("title".equals(type)) {
	        return getDefaultTitleTextListStyle(getMasterPage(operationDocument, page), content, clone);
	    }
	    if("body".equals(type)) {
            return getDefaultOutlineTextListStyle(getMasterPage(operationDocument, page), content, clone);
        }
	    if("subtitle".equals(type)) {
	        return getDefaultOutlineTextListStyle(getMasterPage(operationDocument, page), content, clone);
	    }
	    return getDefaultTextListStyle(content, clone);
	}

	private MasterPage getMasterPage(OdfOperationDoc operationDocument, Page page)
	    throws SAXException {
	    if(page instanceof MasterPage) {
	        return (MasterPage)page;
	    }
	    else if(page instanceof DrawingPage) {
	        final RootComponent rootComponent = ((PresentationContent)getContentDom()).getRootComponent(operationDocument, ((DrawingPage)page).getMasterPageName());
	        if(rootComponent!=null && rootComponent.getObject()!=null) {
	            return (MasterPage)rootComponent.getNextChildComponent(null, null).getObject();
	        }
            return (MasterPage)getStyleManager().getMasterStyles().getContent().getLast();
	    }
	    return null;
	}

	private HashMap<MasterPage, TextListStyle> defaultContentTitleTextListStyles = new HashMap<MasterPage, TextListStyle>();
	private HashMap<MasterPage, TextListStyle> defaultSystemTitleTextListStyles = new HashMap<MasterPage, TextListStyle>();

	private TextListStyle getDefaultTitleTextListStyle(MasterPage masterPage, boolean content, boolean clone) {
	    TextListStyle textListStyle = content ? defaultContentTitleTextListStyles.get(masterPage) : defaultSystemTitleTextListStyles.get(masterPage);
	    if(textListStyle==null) {
	        textListStyle = createDefaultTitleListStyle(masterPage, content);
	        if(content) {
	            defaultContentTitleTextListStyles.put(masterPage, textListStyle);
	        }
	        else {
	            defaultSystemTitleTextListStyles.put(masterPage, textListStyle);
	        }
	    }
	    return clone ? getTextListStyleClone(textListStyle, content) : textListStyle;
	}

	private TextListStyle createDefaultTitleListStyle(MasterPage masterPage, boolean content) {
	    TextListStyle defaultTitleListStyle = null;
        final StyleManager styleManager = getStyleManager();
	    final DrawFrame titleObject = masterPage.getTitleObject();
	    if(titleObject!=null) {
	        final String presentationStyleName = titleObject.getAttributes().getValue("presentation:style-name");
            if(presentationStyleName!=null&&!presentationStyleName.isEmpty()) {
                final StylePresentation presentationStyle = (StylePresentation)styleManager.getStyle(presentationStyleName, StyleFamily.PRESENTATION, false);
                if(presentationStyle!=null) {
                    final TextListStyle titleListStyle = presentationStyle.getGraphicProperties().getTextListStyle();
                    if(titleListStyle!=null) {
                        defaultTitleListStyle = titleListStyle.clone();
                        defaultTitleListStyle.setName(styleManager.getUniqueStyleName(StyleFamily.LIST_STYLE, content));
                        defaultTitleListStyle.setIsContentStyle(content);
                        defaultTitleListStyle.setIsAutoStyle(true);
                    }
                }
            }
	    }
	    if(defaultTitleListStyle==null) {
            // TODO: initializing TextListStyle if there is no graphic default text list style ..
            // this should be done also for the defaultTextListStyle that is used in setDocumentAttributes operation
	        defaultTitleListStyle = new TextListStyle(styleManager.getUniqueStyleName(StyleFamily.LIST_STYLE, content), true, content);
	    }
        final String existingStyleId = styleManager.getExistingStyleIdForStyleBase(defaultTitleListStyle);
        if(existingStyleId!=null) {
            defaultTitleListStyle = (TextListStyle)styleManager.getStyle(existingStyleId, StyleFamily.LIST_STYLE, content);
        }
        else {
            styleManager.addStyle(defaultTitleListStyle);
        }
	    return defaultTitleListStyle;
	}

    private HashMap<MasterPage, TextListStyle> defaultContentOutlineTextListStyles = new HashMap<MasterPage, TextListStyle>();
    private HashMap<MasterPage, TextListStyle> defaultSystemOutlineTextListStyles = new HashMap<MasterPage, TextListStyle>();

	private TextListStyle getDefaultOutlineTextListStyle(MasterPage masterPage, boolean content, boolean clone) {
        TextListStyle textListStyle = content ? defaultContentOutlineTextListStyles.get(masterPage) : defaultSystemOutlineTextListStyles.get(masterPage);
        if(textListStyle==null) {
            textListStyle = createDefaultOutlineListStyle(masterPage, content);
            if(content) {
                defaultContentOutlineTextListStyles.put(masterPage, textListStyle);
            }
            else {
                defaultSystemOutlineTextListStyles.put(masterPage, textListStyle);
            }
        }
        return clone ? getTextListStyleClone(textListStyle, content) : textListStyle;
	}

	private TextListStyle createDefaultOutlineListStyle(MasterPage masterPage, boolean content) {
        TextListStyle defaultOutlineListStyle = null;
        final StyleManager styleManager = getStyleManager();
        final DrawFrame outlineObject = masterPage.getOutlineObject();
        if(outlineObject!=null) {
            final String presentationStyleName = outlineObject.getAttributes().getValue("presentation:style-name");
            if(presentationStyleName!=null&&!presentationStyleName.isEmpty()) {
                final StylePresentation presentationStyle = (StylePresentation)styleManager.getStyle(presentationStyleName, StyleFamily.PRESENTATION, false);
                if(presentationStyle!=null) {
                    final TextListStyle titleListStyle = presentationStyle.getGraphicProperties().getTextListStyle();
                    if(titleListStyle!=null) {
                        defaultOutlineListStyle = titleListStyle.clone();
                        defaultOutlineListStyle.setName(styleManager.getUniqueStyleName(StyleFamily.LIST_STYLE, content));
                        defaultOutlineListStyle.setIsContentStyle(content);
                        defaultOutlineListStyle.setIsAutoStyle(true);
                    }
                }
            }
        }
        if(defaultOutlineListStyle==null) {
            // TODO: initializing TextListStyle if there is no graphic default text list style ..
            // this should be done also for the defaultTextListStyle that is used in setDocumentAttributes operation
            defaultOutlineListStyle = new TextListStyle(styleManager.getUniqueStyleName(StyleFamily.LIST_STYLE, content), true, content);
        }
        final String existingStyleId = styleManager.getExistingStyleIdForStyleBase(defaultOutlineListStyle);
        if(existingStyleId!=null) {
            defaultOutlineListStyle = (TextListStyle)styleManager.getStyle(existingStyleId, StyleFamily.LIST_STYLE, content);
        }
        else {
            styleManager.addStyle(defaultOutlineListStyle);
        }
        return defaultOutlineListStyle;
	}

	private TextListStyle defaultContentTextListStyle = null;
    private TextListStyle defaultSystemTextListStyle = null;

	// returns the default text list style (a clone is not inserted into the StyleManager)
	private TextListStyle getDefaultTextListStyle(boolean content, boolean clone) {
	    TextListStyle textListStyle = content ? defaultContentTextListStyle : defaultSystemTextListStyle;
	    if(textListStyle==null) {
	        textListStyle = createDefaultTextListStyle(content);
	        if(content) {
	            defaultContentTextListStyle = textListStyle;
	        }
	        else {
	            defaultSystemTextListStyle = textListStyle;
	        }
	    }
	    return clone ? getTextListStyleClone(textListStyle, content) : textListStyle;
	}

    private TextListStyle createDefaultTextListStyle(boolean content) {
        TextListStyle defaultTextListStyle = null;
        final StyleManager styleManager = getStyleManager();
        StyleGraphic graphicStyle = (StyleGraphic)styleManager.getStyle("standard", StyleFamily.GRAPHIC, false);
        if(graphicStyle!=null) {
            final TextListStyle graphicTextListStyle = graphicStyle.getGraphicProperties().getTextListStyle();
            if(graphicTextListStyle!=null) {
                defaultTextListStyle = graphicTextListStyle.clone();
                defaultTextListStyle.setName(styleManager.getUniqueStyleName(StyleFamily.LIST_STYLE, content));
                defaultTextListStyle.setIsContentStyle(content);
                defaultTextListStyle.setIsAutoStyle(true);
            }
        }
        if(defaultTextListStyle==null) {
            // TODO: initializing TextListStyle if there is no graphic default text list style ..
            // this should be done also for the defaultTextListStyle that is used in setDocumentAttributes operation
            defaultTextListStyle = new TextListStyle(styleManager.getUniqueStyleName(StyleFamily.LIST_STYLE, content), true, content);
        }
        final String existingStyleId = styleManager.getExistingStyleIdForStyleBase(defaultTextListStyle);
        if(existingStyleId!=null) {
            defaultTextListStyle = (TextListStyle)styleManager.getStyle(existingStyleId, StyleFamily.LIST_STYLE, content);
        }
        else {
            styleManager.addStyle(defaultTextListStyle);
        }
        return defaultTextListStyle;
	}

	private TextListStyle getTextListStyleClone(TextListStyle source, boolean content) {
	    final TextListStyle defaultTextListStyleClone = source.clone();
	    defaultTextListStyleClone.setIsAutoStyle(true);
	    defaultTextListStyleClone.setIsContentStyle(content);
	    defaultTextListStyleClone.setName(getStyleManager().getUniqueStyleName(StyleFamily.LIST_STYLE, content));
	    return defaultTextListStyleClone;
	}

   /**
	 * This enum contains all possible media types of OdfPresentationDocument
	 * documents.
	 */
	public enum OdfMediaType implements MediaType {

		PRESENTATION(OdfDocument.OdfMediaType.PRESENTATION),
		PRESENTATION_TEMPLATE(OdfDocument.OdfMediaType.PRESENTATION_TEMPLATE);
		private final OdfDocument.OdfMediaType mMediaType;

		OdfMediaType(OdfDocument.OdfMediaType mediaType) {
			this.mMediaType = mediaType;
		}

		/**
		 * @return the ODF mediatype of this document
		 */
		public OdfDocument.OdfMediaType getOdfMediaType() {
			return mMediaType;
		}

		/**
		 * @return the mediatype of this document
		 */
		@Override
        public String getMediaTypeString() {
			return mMediaType.getMediaTypeString();
		}

		/**
		 * @return the ODF filesuffix of this document
		 */
		@Override
        public String getSuffix() {
			return mMediaType.getSuffix();
		}

		/**
		 *
		 * @param mediaType string defining an ODF document
		 * @return the according OdfMediatype encapuslating the given string and the suffix
		 */
		public static OdfDocument.OdfMediaType getOdfMediaType(String mediaType) {
			return OdfDocument.OdfMediaType.getOdfMediaType(mediaType);
		}
	}

	/**
	 * Creates an empty presentation document.
	 * @return ODF presentation document based on a default template
	 * @throws java.lang.Exception - if the document could not be created
	 */
	public static OdfPresentationDocument newPresentationDocument() throws Exception {
		return (OdfPresentationDocument) OdfDocument.loadTemplate(EMPTY_PRESENTATION_DOCUMENT_RESOURCE, OdfDocument.OdfMediaType.PRESENTATION);
	}

	/**
	 * Creates an empty presentation template.
	 * @return ODF presentation template based on a default
	 * @throws Exception - if the template could not be created
	 */
	public static OdfPresentationDocument newPresentationTemplateDocument() throws Exception {
		OdfPresentationDocument doc = (OdfPresentationDocument) OdfDocument.loadTemplate(EMPTY_PRESENTATION_DOCUMENT_RESOURCE, OdfDocument.OdfMediaType.PRESENTATION_TEMPLATE);
		doc.changeMode(OdfMediaType.PRESENTATION_TEMPLATE);
		return doc;
	}

	/** To avoid data duplication a new document is only created, if not already opened.
	 * A document is cached by this constructor using the internalpath as key. */
	protected OdfPresentationDocument(OdfPackage pkg, String internalPath, OdfPresentationDocument.OdfMediaType odfMediaType) throws SAXException {
		super(pkg, internalPath, odfMediaType.mMediaType);
	}

	/**
	 * Creates an OdfPresentationDocument from the OpenDocument provided by a resource Stream.
	 *
	 * <p>Since an InputStream does not provide the arbitrary (non sequentiell)
	 * read access needed by OdfPresentationDocument, the InputStream is cached. This usually
	 * takes more time compared to the other createInternalDocument methods.
	 * An advantage of caching is that there are no problems overwriting
	 * an input file.</p>
	 *
	 * <p>If the resource stream is not a ODF presentation document, ClassCastException might be thrown.</p>
	 *
	 * @param inputStream - the InputStream of the ODF presentation document.
	 * @return the presentation document created from the given InputStream
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfPresentationDocument loadDocument(InputStream inputStream) throws Exception {
		return (OdfPresentationDocument) OdfDocument.loadDocument(inputStream);
	}

	/**
	 * Loads an OdfPresentationDocument from the provided path.
	 *
	 * <p>OdfPresentationDocument relies on the file being available for read access over
	 * the whole lifecycle of OdfPresentationDocument.</p>
	 *
	 * <p>If the resource stream is not a ODF presentation document, ClassCastException might be thrown.</p>
	 *
	 * @param documentPath - the path from where the document can be loaded
	 * @return the presentation document from the given path
	 *		  or NULL if the media type is not supported by ODFDOM.
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfPresentationDocument loadDocument(String documentPath) throws Exception {
		return (OdfPresentationDocument) OdfDocument.loadDocument(documentPath);
	}

	/**
	 * Creates an OdfPresentationDocument from the OpenDocument provided by a File.
	 *
	 * <p>OdfPresentationDocument relies on the file being available for read access over
	 * the whole lifecycle of OdfPresentationDocument.</p>
	 *
	 * <p>If the resource stream is not a ODF presentation document, ClassCastException might be thrown.</p>
	 *
	 * @param file - a file representing the ODF presentation document.
	 * @return the presentation document created from the given File
	 * @throws java.lang.Exception - if the document could not be created.
	 */
	public static OdfPresentationDocument loadDocument(File file) throws Exception {
		return (OdfPresentationDocument) OdfDocument.loadDocument(file);
	}

	/**
	 * Switches this instance to the given type. This method can be used to e.g.
	 * convert a document instance to a template and vice versa.
	 * Changes take affect in the package when saving the document.
	 *
	 * @param type the compatible ODF mediatype.
	 */
	public void changeMode(OdfMediaType type) {
		setOdfMediaType(type.mMediaType);
	}

    @Override
    public OdfFileDom newFileDom(String packagePath) throws SAXException {

        OdfFileDom newFileDom = null;
        // before creating a new dom, make sure that there no DOM opened for this file already
        Document existingDom = getPackage().getCachedDom(packagePath);
        if (existingDom == null) {
            if (packagePath.equals("content.xml") || packagePath.endsWith("/content.xml")) {
                newFileDom = new PresentationContent(this, packagePath);
            }
            else if (packagePath.equals("styles.xml") || packagePath.endsWith("/styles.xml")) {
                newFileDom = new PresentationStyles(this, packagePath);
            }
        }
        if(newFileDom==null) {
            newFileDom = super.newFileDom(packagePath);
        }
        return newFileDom;
    }

	@Override
	public int applyOperations(OdfOperationDoc operationDocument, JSONArray ops) throws Exception {

	    return new JsonOperationConsumer(operationDocument).applyOperations(ops);
	}

    @Override
    public JSONObject getOperations(OdfOperationDoc operationDoc) throws Exception {

        return new JsonOperationProducer(operationDoc).getDocumentOperations();
    }
}
