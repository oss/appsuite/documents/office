/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * Copyright 2008, 2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0. You can also
 * obtain a copy of the License at http://odftoolkit.org/docs/license.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ************************************************************************/

package com.openexchange.office.filter.odf.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.odftoolkit.odfdom.doc.OdfDocument;
import com.openexchange.office.filter.odf.ods.dom.SpreadsheetContent;
import com.openexchange.office.filter.odf.styles.DocumentStyles;
import com.openexchange.office.filter.odf.styles.NumberStyleBase;
import com.openexchange.office.filter.odf.styles.StyleBase;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;

public class ODSNumberFormatTest {

    // the value is the expected result, if it is null then it is expected that the result is equal to the key
    final Map<String, String> mNumberFormatCodes = new LinkedHashMap<String, String>();

    public ODSNumberFormatTest() {

        mNumberFormatCodes.put("000,000,000,000.000000000000;[RED]-000,000,000,000.000000000000", null);
        mNumberFormatCodes.put("000,000,000,000.000000000000 [$€-407];[RED]-000,000,000,000.000000000000 [$€-407]", null); // docs-2538

        // mNumberFormatCodes.add("[$-F800]DDDD, MMMM D, YYYY");
        // mNumberFormatCodes.add("[$-F400]h:mm:ss AM/PM");
        // mNumberFormatCodes.add("DD.MM.YYYY H:mm:ss");
        // mNumberFormatCodes.add("$#,##0.00_);($#,##0.00)");

        mNumberFormatCodes.put("$#,##0.00_);($#,##0.00)", "$#,##0.00 ;($#,##0.00)");  // DOCS-2625, the _) value is changed to space
        // <number:number-style style:name="N111P0" style:volatile="true">
        //     <number:text>$</number:text>
        //     <number:number number:decimal-places="4" loext:min-decimal-places="4" number:min-integer-digits="0"/>
        //     <number:text> </number:text>
        // </number:number-style>
        // <number:number-style style:name="N111">
        //     <number:text>($</number:text>
        //     <number:number number:decimal-places="4" loext:min-decimal-places="4" number:min-integer-digits="0"/>
        //     <number:text>)</number:text>
        //     <style:map style:condition="value()&gt;=0" style:apply-style-name="N111P0"/>
        // </number:number-style>

        mNumberFormatCodes.put("$#,##0.00;($#,##0.00)", null);
        // <number:number-style style:name="N112P0" style:volatile="true">
        //     <number:text>$</number:text>
        //     <number:number number:decimal-places="4" loext:min-decimal-places="4" number:min-integer-digits="0"/>
        // </number:number-style>
        // <number:number-style style:name="N112">
        //     <number:text>($</number:text>
        //     <number:number number:decimal-places="4" loext:min-decimal-places="4" number:min-integer-digits="0"/>
        //     <number:text>)</number:text>
        //     <style:map style:condition="value()&gt;=0" style:apply-style-name="N112P0"/>
        // </number:number-style>

        mNumberFormatCodes.put("0", null); //<<--- general style - always available and will be found in the officeStyles!
        mNumberFormatCodes.put("general", null);
        mNumberFormatCodes.put("#.00", null);
        mNumberFormatCodes.put("0.00", null);
        mNumberFormatCodes.put("#,##0", null);
        mNumberFormatCodes.put("#,##0.00", null);
        // scientific formats: minimum required "0E+0" or "#E+0", spaces in between can not be saved in ODF
        mNumberFormatCodes.put("0,000E+0", null);
        mNumberFormatCodes.put("#,000E+0", null);
        mNumberFormatCodes.put("#,#00E+0", null);
        mNumberFormatCodes.put("#,##0E+0", null);
        mNumberFormatCodes.put("#,##0E+00", null);
        mNumberFormatCodes.put("#,##0E+000", null);
        mNumberFormatCodes.put("0.00E+0", null);
        mNumberFormatCodes.put("0.00E+00", null);
        mNumberFormatCodes.put("00 ??/??", null);
        mNumberFormatCodes.put("0 ??/??", null);
        mNumberFormatCodes.put("?/?", null);
        mNumberFormatCodes.put("??/??", null);
        mNumberFormatCodes.put("# ?/?", null);
        mNumberFormatCodes.put("# ??/??", null);
        mNumberFormatCodes.put("?/10", null);
        mNumberFormatCodes.put("# ?/10", null);
        mNumberFormatCodes.put("0.00;[RED]0.00", null);
        mNumberFormatCodes.put("[GREEN]0.00;[BLUE]0.00;[CYAN]0.00", null);
        mNumberFormatCodes.put("MMDDYY", null);
        mNumberFormatCodes.put("#,##0 €;[RED]-#,##0 €", null);
        mNumberFormatCodes.put("#,##0.00 €;-#,##0.00 €", null);
        mNumberFormatCodes.put("#,##0.00 €;[RED]-#,##0.00 €", null);
        mNumberFormatCodes.put("0%", null);
        mNumberFormatCodes.put("0.00%", null);
        mNumberFormatCodes.put("M/D/YYYY", null);
        mNumberFormatCodes.put("D-MMM-YY", null);
        mNumberFormatCodes.put("D-MMM", null);
        mNumberFormatCodes.put("MMM-YY", null);
        mNumberFormatCodes.put("h:mm AM/PM", null);
        mNumberFormatCodes.put("h:mm:ss AM/PM", null);
        mNumberFormatCodes.put("h:m:ss AM/PM", null);
        mNumberFormatCodes.put("h:mm", null);
        mNumberFormatCodes.put("h:mm:ss", null);
        mNumberFormatCodes.put("M/D/YYYY h:mm", null);
        mNumberFormatCodes.put("#,##0 ;(#,##0)", null);
        mNumberFormatCodes.put("#,##0.00 ;(#,##0.00)", null);
        mNumberFormatCodes.put("#,##0.00 ;[RED](#,##0.00)", null);
        mNumberFormatCodes.put("mm:ss", null);
        mNumberFormatCodes.put("[h]:mm:ss", null);
        mNumberFormatCodes.put("000.0E+0", null);
        mNumberFormatCodes.put("@", null);
        mNumberFormatCodes.put("W-YY", null);
        mNumberFormatCodes.put("W", null);
        mNumberFormatCodes.put("WW", null);
        // mNumberFormatCodes.put("QQ 16", null);
// mNumberFormatCodes.put("mm:ss.0", null); lo only
// mNumberFormatCodes.put("mm:ss.00", null); lo only
// mNumberFormatCodes.put("mm:ss.000", null); lo only
        mNumberFormatCodes.put("hh:mm:ss", null);
        mNumberFormatCodes.put("hh:mm:s", null);
        mNumberFormatCodes.put("hh:mm:ss AM/PM", null);
        mNumberFormatCodes.put("hh:mm:s AM/PM", null);
        mNumberFormatCodes.put("DD-MM-YYYY hh:mm:ss AM/PM", null);
        mNumberFormatCodes.put("hh:mm:ss AM/PM", null);
        mNumberFormatCodes.put("hh:mm AM/PM", null);
        mNumberFormatCodes.put("MM/DD/YYYY h:mm:ss", null);
        mNumberFormatCodes.put("DD.MM.YYYY h:mm:ss", null);
        mNumberFormatCodes.put("MM/DD/YYYY h:mm:s", null);
        mNumberFormatCodes.put("DD.MM.YYYY h:mm:s", null);
        mNumberFormatCodes.put("MM/DD/YYYY h:mm:s AM/PM", null);
        mNumberFormatCodes.put("DD.MM.YYYY h:mm:s AM/PM", null);

        mNumberFormatCodes.put("* #,##0    ;-* #,##0    ;* -    ;@", null);
        mNumberFormatCodes.put("* #,##0 € ;-* #,##0 € ;* - € ;@ ", null);
        mNumberFormatCodes.put("* #,##0.00    ;-* #,##0.00    ;* -#    ;@ ", null);
        mNumberFormatCodes.put("* #,##0.00 € ;-* #,##0.00 € ;* -# € ;@", null);
        mNumberFormatCodes.put("[RED]-#,##0.-- [$€-407]", null);
        mNumberFormatCodes.put("[RED][$€-407] -#,##0.--", null);
        mNumberFormatCodes.put("[RED] -#,##0.--[$€-407] ", null);
        mNumberFormatCodes.put("[RED] -#,##0.-- [$€-407]", null);
        mNumberFormatCodes.put("[RED] -#,##0.--[$€-407]", null);
        mNumberFormatCodes.put("#,##0.00000%;[BLUE]-#,##0.00000%", null);

        // mNumberFormatCodes.put(".00", null);//-> will be loaded as "#.00"
        // mNumberFormatCodes.put("[>5]0.00;[RED]0.00", null); //conditions are not supported, yet
        // mNumberFormatCodes.put(" \"Y\"  0.00     ", null); //conditions are not supported, yet
        // mNumberFormatCodes.put("[>0]* #,##0\"    \";[<0]-* #,##0\"    \";* \"-    \";@", null); //conditions are not supported, yet add: quotings are restored only if required
        // mNumberFormatCodes.put("[>0]* #,##0\" € \";[<0]-* #,##0\" € \";* \"- € \";@ ", null); //conditions are not supported, yet
        // mNumberFormatCodes.put("[>0]* #,##0.00\"    \";[<0]-* #,##0.00\"    \";* -#\"    \";@ ", null); //conditions are not supported, yet
        // mNumberFormatCodes.put("[>0]* #,##0.00\" € \";[<0]-* #,##0.00\" € \";* -#\" € \";@", null); //conditions are not supported, yet

        mNumberFormatCodes.put("DD.MM.YY", null);
        mNumberFormatCodes.put("DD.MM.YYYY", null);
        mNumberFormatCodes.put("D.M.YY", null);
        mNumberFormatCodes.put("D-MMM-YY", null);
        mNumberFormatCodes.put("D. MMM YY", null);

        mNumberFormatCodes.put("#,##0.0[$€-407]", null);
        mNumberFormatCodes.put("#,##0.00[$€-407]", null);
    }

    @Test
    public void testNumberFormats(){
    	final Set<String> errorSet = new HashSet<String>();
        try {
        	final OdfDocument document = TestUtils.loadSpreadsheetDocument(null).getDocument();
        	final DocumentStyles styles = document.getStylesDom();
    		final SpreadsheetContent content = (SpreadsheetContent)document.getContentDom();
           	final StyleManager styleManager = document.getStyleManager();

           	final Iterator<Entry<String, String>> formatCodeIter = mNumberFormatCodes.entrySet().iterator();
            while(formatCodeIter.hasNext()){
                final Entry<String, String> formatCodeEntry = formatCodeIter.next();
                final String numberFormatCode = formatCodeEntry.getKey();
            	final String dataStyleName = styleManager.applyDataStyle(numberFormatCode, null, -1, false, true, true);
    			final StyleBase dataStyle = styleManager.getStyle(dataStyleName, StyleFamily.DATA_STYLE, true);
    			String createdFormat = "";
    			if(dataStyle instanceof NumberStyleBase) {
    				createdFormat = ((NumberStyleBase)dataStyle).getFormat(styleManager, null, true);
    			}

    			final String expectedRoundTripResult = formatCodeEntry.getValue() == null ? numberFormatCode : formatCodeEntry.getValue();
                assertTrue(expectedRoundTripResult.equals(createdFormat), "NumberFormat failure:" + numberFormatCode + " -> " + createdFormat + " but should be: " + expectedRoundTripResult);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            fail("odf test failed:" + e.getMessage());
        }
        assertTrue(errorSet.isEmpty(), "Errors occured: : " + errorSet.toString());
    }
}
