/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.test;

import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.OdfOperationDoc;

public class ODPTest {

    @Test
    public void test(){
        try {

            /*
             * load empty odp document
             */
            OdfOperationDoc operationDocument = TestUtils.loadAndCheck(
                "test/com/openexchange/office/filter/odf/test/empty.odp",
                "Root(Slide(Frame,Frame))");

            /*
             * deleting the first presentation frame
             */
            TestUtils.applyAndCheck(operationDocument,
                    "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE.value() + "\",\"" + OCKey.START.value() + "\":[0,0]}]",
                    "Root(Slide(Frame))", null, null);

        }
        catch (Throwable e) {
            fail("odpTableTest failed:" + e.getMessage());
        }
    }
}
