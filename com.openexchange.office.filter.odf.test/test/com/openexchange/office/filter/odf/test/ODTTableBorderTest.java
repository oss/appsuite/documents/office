/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.Border;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.odt.dom.JsonOperationConsumer;
import com.openexchange.office.filter.odf.odt.dom.TextContent;
import com.openexchange.office.filter.odf.odt.dom.components.CellComponent;
import com.openexchange.office.filter.odf.properties.TableCellProperties;
import com.openexchange.office.filter.odf.styles.StyleFamily;
import com.openexchange.office.filter.odf.styles.StyleManager;
import com.openexchange.office.filter.odf.styles.StyleTableCell;
import com.openexchange.office.filter.odf.table.Cell;

public class ODTTableBorderTest {

    @Test
    public void testTable(){
        try {

            // exemplary testing the left border of the first cell after inserting a table at 0 with two rows and two cells.
            // A top, left, right and bottom border is applied with a width of 212 (100thmm)

            // loading test document and applying operations
            final OdfOperationDoc operationDocument = TestUtils.loadTextDocument("test/com/openexchange/office/filter/odf/test/testDocs/TableTest.odt");
            new JsonOperationConsumer(operationDocument).applyOperations(new JSONArray(
                "[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TABLE.value() + "\",\"" + OCKey.START.value() + "\":[0],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.TABLE.value() + "\":{\"" + OCKey.TABLE_GRID.value() + "\":[1000,1000],\"width\":\"auto\",\"exclude\":[\"lastRow\",\"lastCol\",\"bandsVert\"]},\"" + OCKey.STYLE_ID.value() + "\":\"_default\"}},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_ROWS.value() + "\",\"" + OCKey.START.value() + "\":[0,0],\""+ OCKey.COUNT.value() + "\":2,\"" + OCKey.INSERT_DEFAULT_CELLS.value() + "\":true},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[0,0,0],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.CELL.value() + "\":{\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}}}}},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[0,0,1],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.CELL.value() + "\":{\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}}}}},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[0,1,0],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.CELL.value() + "\":{\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}}}}},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[0,1,1],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.CELL.value() + "\":{\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}}}}},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[0],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.TABLE.value() + "\":{\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderInsideVert\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderInsideHor\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}}}}},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[0,0,0],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.CELL.value() + "\":{\"borderTop\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}}}}},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[0,0,1],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.CELL.value() + "\":{\"borderTop\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}}}}},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[0,1,0],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.CELL.value() + "\":{\"borderTop\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}}}}},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SET_ATTRIBUTES.value() + "\",\"" + OCKey.START.value() + "\":[0,1,1],\"" + OCKey.ATTRS.value() + "\":{\"" + OCKey.CELL.value() + "\":{\"borderTop\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"" + OCKey.TYPE.value() + "\":\"auto\"}}}}}]"));

            // saving & reloading the document ...
            final OdfOperationDoc destDocument = TestUtils.saveLoad(operationDocument);

            // accessing the top left cell and checking its left border width
            final TextContent content = (TextContent)((OdfTextDocument)destDocument.getDocument()).getContentDom();
            final JSONArray start = new JSONArray(3);
            start.put(0);
            start.put(0);
            start.put(0);
            final CellComponent cell = (CellComponent)content.getRootComponent(destDocument, null).getComponent(start, start.length());
            final StyleManager styleManager = destDocument.getDocument().getStyleManager();
            final StyleTableCell tableCellStyle = (StyleTableCell)styleManager.getStyle(((Cell)cell.getObject()).getStyleName(), StyleFamily.TABLE_CELL, true);
            final TableCellProperties cellProperties = tableCellStyle.getTableCellProperties();
            final Border leftBorder = new Border();
            leftBorder.applyFoBorder(cellProperties.getAttribute("fo:border-left"));
            assertTrue(Integer.valueOf(212).equals(leftBorder.getWidth()), "border width does not match");
        }
        catch (Throwable e) {
        	e.printStackTrace(System.out);
            fail("odtTableTest failed:" + e.getMessage());
        }
    }
}
