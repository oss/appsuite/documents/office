/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.test;

import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.OdfOperationDoc;

public class ODTAnnotations {

    @Test
    public void testAnnotations(){
        try {

        	/*
        	 * The document is containing four annotations, two annotations are a reply to the previous annotation
        	 *
        	 */
        	OdfOperationDoc operationDocument = TestUtils.loadAndCheck(
        		"test/com/openexchange/office/filter/odf/test/testDocs/Comment.odt",
        		"Root(Para(Text'Test ',Annotation-Ref[__Annotation__0_1867282312],Text'Text',Annotation-End[__Annotation__0_1867282312],Annotation-Ref[cmt0],Annotation-Ref[cmt1],Text'… T',Annotation-Ref[__Annotation__1_1867282312],Text'es',Annotation-End[__Annotation__1_1867282312],Text't'))");

        	/*
        	 * we check the content of the first annotation : __Annotation__0_1867282312
        	 */
        	TestUtils.applyAndCheck(operationDocument,
    			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.NO_OP.value() + "\"}]",
    			"Root(Para(Text'Comment 1'))", "__Annotation__0_1867282312", null);

        	/*
        	 * we check if we can insert something into this annotation
        	 */
        	TestUtils.applyAndCheck(operationDocument,
    			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TEXT.value() +"\",\"" + OCKey.START.value() + "\":[0,3],\"" + OCKey.TEXT.value() + "\":\"TEST\",\"" + OCKey.TARGET.value() + "\":\"__Annotation__0_1867282312\"}]",
    			"Root(Para(Text'ComTESTment 1'))", "__Annotation__0_1867282312", null);

        	/*
        	 * we check the content of the first reply : cmt0
        	 */
        	TestUtils.applyAndCheck(operationDocument,
    			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.NO_OP.value() + "\"}]",
    			"Root(Para(Text'Reply to Comment 1'))", "cmt0", null);

        	/*
        	 * we split the first paragraph at 0, 16
        	 */
        	TestUtils.applyAndCheck(operationDocument,
    			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.SPLIT_PARAGRAPH.value() + "\",\"" + OCKey.START.value() + "\":[0,16]}]",
    			"Root(Para(Text'Test ',Annotation-Ref[__Annotation__0_1867282312],Text'Text',Annotation-End[__Annotation__0_1867282312],Annotation-Ref[cmt0],Annotation-Ref[cmt1],Text'… T'),Para(Annotation-Ref[__Annotation__1_1867282312],Text'es',Annotation-End[__Annotation__1_1867282312],Text't'))", null, null);

        	/*
        	 * delete first paragraph
        	 */
        	TestUtils.applyAndCheck(operationDocument,
    			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE.value() + "\",\"" + OCKey.START.value() + "\":[0]}]",
    			"Root(Para(Annotation-Ref[__Annotation__1_1867282312],Text'es',Annotation-End[__Annotation__1_1867282312],Text't'))", null, null);
        }
        catch (Throwable e) {
            fail("ODTAnnotations failed:" + e.getMessage());
        }
    }
}
