/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.test;

import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.OdfOperationDoc;

public class ODTPageAnchored {

    @Test
    public void testPageAnchoredObjects(){
        try {

        	/*
        	 * The document is containing three page anchored objects (textFrame, image and shape)
        	 * not residing inside a paragraph + some normal text
        	 *
        	 * After loading the three anchored objects are moved into the first paragraph
        	 *
        	 */
        	OdfOperationDoc operationDocument = TestUtils.loadAndCheck(
        		"test/com/openexchange/office/filter/odf/test/testDocs/PageAnchored.odt",
        		"Root(Para(Frame(Para(Text'Frame')),Frame,Shape(Para(Text'Shape')),Text'Hello'))");

        	/*
        	 * we delete the third page anchored object
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE.value() + "\",\"" + OCKey.START.value() + "\":[0,2]}]",
        			"Root(Para(Frame(Para(Text'Frame')),Frame,Text'Hello'))", null, null);

        	/*
        	 * deleting the first paragraph the document is empty
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE.value() + "\",\"" + OCKey.START.value() + "\":[0]}]",
        			"Root(Para)", null, null);

        }
        catch (Throwable e) {
            fail("ODTPageAnchored failed:" + e.getMessage());
        }
    }
}
