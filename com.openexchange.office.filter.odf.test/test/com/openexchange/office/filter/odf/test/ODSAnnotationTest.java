/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import com.openexchange.office.filter.core.spreadsheet.CellRef;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.ods.dom.JsonOperationConsumer;
import com.openexchange.office.filter.odf.ods.dom.Sheet;
import com.openexchange.office.filter.odf.ods.dom.SpreadsheetContent;
import com.openexchange.office.filter.odf.ods.dom.SpreadsheetStyles;
import com.openexchange.office.filter.odf.odt.dom.Annotation;

public class ODSAnnotationTest {

    @Test
    public void testAnnotation(){
        try {
            // inserting a annotation on 0/100/100, save, reload -> a annotation needs to be available at 0/100/100 with the proper creator

            final OdfOperationDoc operationDocument = TestUtils.loadSpreadsheetDocument("test/com/openexchange/office/filter/odf/test/empty.ods");
            final JsonOperationConsumer consumer = new JsonOperationConsumer(operationDocument);
            consumer.insertNote(0, new CellRef(100,100), "\r\n" + "I am a new Comment" + "\r\n" + "with five paragraphs" + "\r\n", "sven.jacobi", null, null, new JSONObject());
            final OdfOperationDoc doc2 = TestUtils.saveLoad(operationDocument);
            final OdfSpreadsheetDocument document = (OdfSpreadsheetDocument)doc2.getDocument();
            final SpreadsheetStyles  styles = (SpreadsheetStyles)document.getStylesDom();
            final SpreadsheetContent content = (SpreadsheetContent)document.getContentDom();
            final Sheet sheet = content.getContent().get(0);
            final Annotation annotation = sheet.getRow(100, false, false, false).getCell(100, false, false, false).getAnnotation();
            assertTrue(annotation.getCreator().equals("sven.jacobi"));
        }
        catch (Throwable e) {
            fail("odsAnnotationTest failed:" + e.getMessage());
        }
    }
}
