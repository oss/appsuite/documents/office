/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.Iterator;
import org.junit.jupiter.api.Test;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.core.component.CUtil;
import com.openexchange.office.filter.odf.OdfOperationDoc;
import com.openexchange.office.filter.odf.odt.dom.TextContent;
import com.openexchange.office.filter.odf.styles.DocumentStyles;
import com.openexchange.office.filter.odf.styles.MasterStyles;
import com.openexchange.office.filter.odf.styles.StyleHeaderFooter;
import com.openexchange.office.filter.odf.styles.StyleMasterPage;

public class ODTHeaderFooter {

@Test
public void TestHeaderFooter() {

	try {
        	OdfOperationDoc operationDocument = TestUtils.loadAndCheck(
        		"test/com/openexchange/office/filter/odf/test/testDocs/HeaderFooterTest.odt",
        		"Root(Para(Text'p'),Para,Para)");
            /*
        	 * The document contains three headers and one footer
        	 *
        	 * default header: Root(Para(Text'HEADER DEFAULT'))
        	 *
        	 * first page header: Root(Para(Text'HEADER FIRST PAGE'))
        	 *
        	 * header left: Root(Para(Text'HEADER LEFT'))
        	 *
        	 * footer first: Root(Para(Text'FOOTER FIRST'))
        	 *
        	 */

            // only creating target nodes for the correct master page, so this is possible only after loading styles + content
        	createHeaderFooterTargetNodes(operationDocument);

        	// the content of each standard header / footer is checked.
        	assertTrue(getHeaderFooterContent(operationDocument).equals("Root(Para(Text'HEADER DEFAULT'))Root(Para(Text'HEADER FIRST PAGE'))Root(Para(Text'HEADER LEFT'))Root(Para)Root(Para(Text'FOOTER FIRST'))"), "HeaderFooter validation error:");

        	// we delete the header first
        	TestUtils.applyAndCheck(operationDocument,
            		"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE_HEADER_FOOTER.value() + "\",\"" + OCKey.ID.value() + "\":\"header_first_Standard\"}]",
        			"Root(Para(Text'p'),Para,Para)", null, null);

        	assertTrue(getHeaderFooterContent(operationDocument).equals("Root(Para(Text'HEADER DEFAULT'))Root(Para(Text'HEADER LEFT'))Root(Para)Root(Para(Text'FOOTER FIRST'))"), "HeaderFooter validation error:");
		}
        catch (Throwable e) {
            fail("ODTHeaderFooter failed:" + e.getMessage());
        }
    }

    public static void createHeaderFooterTargetNodes(OdfOperationDoc operationDocument)
        throws SAXException {

        final DocumentStyles styles = operationDocument.getDocument().getStylesDom();
        final TextContent content = (TextContent)operationDocument.getDocument().getContentDom();
        final MasterStyles masterStyles = content.getDocument().getStyleManager().getMasterStyles();
        final String masterPageName = content.getBody().getMasterPageName(true);
        final Iterator<Object> styleMasterPageIter = masterStyles.getContent().iterator();
        while(styleMasterPageIter.hasNext()) {
            final Object o = styleMasterPageIter.next();
            if(o instanceof StyleMasterPage) {
                if(((StyleMasterPage)o).getName().equals(masterPageName)) {
                    for(StyleHeaderFooter styleHeaderFooter:((StyleMasterPage)o).getStyleHeaderFooters()) {
                        content.getTargetNodes().put(styleHeaderFooter.getId(), styleHeaderFooter);
                    }
                    return;
                }
            }
        }
    }

	private static String getHeaderFooterContent(OdfOperationDoc operationDocument)
		throws SAXException {

		final OdfTextDocument doc = (OdfTextDocument)operationDocument.getDocument();
		final DocumentStyles styles = doc.getStylesDom();

	    final StringBuffer headerFooterComponents = new StringBuffer();
		final MasterStyles masterStyles = styles.getDocument().getStyleManager().getMasterStyles();
		if(masterStyles!=null) {
			final Iterator<Object> masterStyleIter = masterStyles.getContent().iterator();
			while(masterStyleIter.hasNext()) {
				final Object o = masterStyleIter.next();
				if(o instanceof StyleMasterPage) {
					StyleMasterPage styleMasterPage = (StyleMasterPage)o;
					if(styleMasterPage.getName().equals("Standard")) {
						for(StyleHeaderFooter styleHeaderFooter:styleMasterPage.getStyleHeaderFooters()) {
							headerFooterComponents.append(CUtil.componentToString(TestUtils.getComponent(operationDocument, styleHeaderFooter.getId(), null)));
		    			}
					}
				}
			}
		}
		return headerFooterComponents.toString();
	}

}

