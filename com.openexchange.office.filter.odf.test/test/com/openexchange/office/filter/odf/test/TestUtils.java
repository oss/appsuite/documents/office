/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.json.JSONArray;
import org.json.JSONException;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.xml.sax.SAXException;
import com.openexchange.office.filter.core.component.CUtil;
import com.openexchange.office.filter.core.component.IComponent;
import com.openexchange.office.filter.odf.IContentDom;
import com.openexchange.office.filter.odf.OdfOperationDoc;

final public class TestUtils {

    private static OdfOperationDoc loadDocument(String fileName)
        throws IOException, Exception {

        final File file = new File(fileName);
        OdfOperationDoc operationDoc = new OdfOperationDoc(null, null, null);
        operationDoc.loadDocument(new ByteArrayInputStream(Files.readAllBytes(file.toPath())));
        return operationDoc;
    }

    // loads the corresponding spreadsheet document, returns an empty default document if fileName is null
	public static OdfOperationDoc loadSpreadsheetDocument(String fileName)
		throws Exception {

	    return loadDocument(fileName!=null?fileName:"test/com/openexchange/office/filter/odf/test/empty.ods");
    }

    // loads the corresponding presentation document, returns an empty default document if fileName is null
    public static OdfOperationDoc loadPresentationDocument(String fileName)
        throws Exception {

        return loadDocument(fileName!=null?fileName:"test/com/openexchange/office/filter/odf/test/empty.odp");
    }

	// loads the corresponding text document, returns an empty default document if fileName is null
	public static OdfOperationDoc loadTextDocument(String fileName)
		throws Exception {

	    return loadDocument(fileName!=null?fileName:"test/com/openexchange/office/filter/odf/test/empty.odt");
    }

	public static OdfOperationDoc saveLoad(OdfOperationDoc operationDoc)
		throws Exception {

	    final ByteArrayOutputStream output = new ByteArrayOutputStream();
	    operationDoc.getDocument().save(output);
	    final ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
	    OdfOperationDoc doc = new OdfOperationDoc(null, null, operationDoc.getDocumentProperties());
	    doc.loadDocument(input);
	    return doc;
	}

	public static OdfOperationDoc loadAndCheck(String fileName, String verification)
		throws Exception {

		final OdfOperationDoc operationDocument = loadDocument(fileName);
		final String components = CUtil.componentToString(getComponent(operationDocument, null, null));
    	assertTrue(components.equals(verification), "error:\n" + components + "\n does not match:\n" + verification);
    	return operationDocument;
	}

	public static void applyAndCheck(OdfOperationDoc operationDoc, String operations, String verification, String target, int... position)
		throws SAXException, JSONException, Exception {

	    operationDoc.applyOperations(operations);
        operationDoc = saveLoad(operationDoc);
        final String components = CUtil.componentToString(TestUtils.getComponent(operationDoc, target, position));
    	assertTrue(components.equals(verification), "error:\n" + components + "\n does not match:\n" + verification);
	}

	public static JSONArray getPosition(int... integers) {
		final JSONArray json = new JSONArray(integers.length);
		for(int i:integers) {
			json.put(i);
		}
		return json;
	}

	public static IComponent<?> getComponent(OdfOperationDoc operationDocument, String target, int... position)
		throws SAXException {

		final OdfDocument document = operationDocument.getDocument();
    	final IContentDom content = (IContentDom)document.getContentDom();
    	if(position==null) {
    		return content.getRootComponent(operationDocument, target);
    	}
    	return content.getRootComponent(operationDocument, target).getComponent(TestUtils.getPosition(position), position.length);
	}
}
