/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.filter.odf.test;

import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.filter.odf.OdfOperationDoc;

public class ODTTableTest {

    @Test
    public void testTable(){
        try {

        	/*
        	 * The document is containing a table with four columns and 12 cells, in the last column is text content "Hello"
        	 * ---------------------------------
        	 * -       -       -       - Hello -			(no merged cell)
        	 * -       -               - Hello -			(double merged cell)
        	 * -                       - Hello -			(triple merged cell)
        	 * ---------------------------------
        	 * P
        	 */
        	OdfOperationDoc operationDocument = TestUtils.loadAndCheck(
        		"test/com/openexchange/office/filter/odf/test/testDocs/TableTest.odt",
        		"Root(Table(Row(Cell(Para),Cell(Para),Cell(Para),Cell(Para(Text'Hello'))),Row(Cell(Para),Cell(Para),Cell(Para(Text'Hello'))),Row(Cell(Para),Cell(Para(Text'Hello')))),Para)");

        	/*
        	 * If now the third column is deleted, the table should look like:
        	 * -------------------------
        	 * -       -       - Hello -
        	 * -       - Hello -
        	 * - Hello -
        	 * ---------
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        		"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE_COLUMNS.value() + "\",\"" + OCKey.START.value() + "\":[0],\"" + OCKey.START_GRID.value() + "\":2,\"" + OCKey.END_GRID.value() + "\":2}]",
        		"Root(Table(Row(Cell(Para),Cell(Para),Cell(Para(Text'Hello'))),Row(Cell(Para),Cell(Para(Text'Hello'))),Row(Cell(Para(Text'Hello')))),Para)", null, null);

        	/*
        	 * If now the third column is deleted, the table should look like:
        	 * -----------------
        	 * -       -       -
        	 * -       - Hello -
        	 * - Hello -
        	 * ---------
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE_COLUMNS.value() + "\",\"" + OCKey.START.value() + "\":[0],\"" + OCKey.START_GRID.value() + "\":2,\"" + OCKey.END_GRID.value() + "\":2}]",
        			"Root(Table(Row(Cell(Para),Cell(Para)),Row(Cell(Para),Cell(Para(Text'Hello'))),Row(Cell(Para(Text'Hello')))),Para)", null, null);

        	/*
        	 * inserting two columns at 0
        	 * ---------------------------------
        	 * -       -       -       -       -
        	 * -       -       -       - Hello -
        	 * -       -       - Hello -
        	 * -------------------------
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\""+ OCValue.INSERT_COLUMN.value() + "\",\"" + OCKey.START.value() + "\":[0],\"" + OCKey.GRID_POSITION.value() + "\":0,\"" + OCKey.TABLE_GRID.value() + "\":[1,1,1,1],\"" + OCKey.END_GRID.value() + "\":2,\"" + OCKey.INSERT_MODE.value() + "\":\"before\"}]",
        			"Root(Table(Row(Cell(Para),Cell(Para),Cell(Para)),Row(Cell(Para),Cell(Para),Cell(Para(Text'Hello'))),Row(Cell(Para),Cell(Para(Text'Hello')))),Para)", null, null);

        	/*
        	 * inserting two rows at 0 with insertDefaultCells true
        	 * ---------------------------------
        	 * -       -       -       -       -
        	 * -       -       -       -       -
        	 * -       -       -       -       -
        	 * -       -       -       - Hello -
        	 * -       -       - Hello -
        	 * -------------------------
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_ROWS.value() + "\",\"" + OCKey.START.value() + "\":[0,0],\"count\":2,\"" + OCKey.INSERT_DEFAULT_CELLS.value() + "\":true}]",
        			"Root(Table(Row(Cell(Para),Cell(Para),Cell(Para),Cell(Para)),Row(Cell(Para),Cell(Para),Cell(Para),Cell(Para)),Row(Cell(Para),Cell(Para),Cell(Para)),Row(Cell(Para),Cell(Para),Cell(Para(Text'Hello'))),Row(Cell(Para),Cell(Para(Text'Hello')))),Para)", null, null);


        	/*
        	 * deleting first four rows
        	 * ---------------------------------
        	 * -       -       - Hello -
        	 * -------------------------
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE.value() + "\",\"" + OCKey.START.value() + "\":[0,0],\"" + OCKey.END.value() + "\":[0,3]}]",
        			"Root(Table(Row(Cell(Para),Cell(Para(Text'Hello')))),Para)", null, null);

        	/*
        	 * inserting row at 0 with insertDefaultCells false
        	 * ---------------------------------
        	 * -
        	 * -       -       - Hello -
        	 * -------------------------
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_ROWS.value() + "\",\"" + OCKey.START.value() + "\":[0,0],\"count\":1,\"" + OCKey.INSERT_DEFAULT_CELLS.value() + "\":false}]",
        			"Root(Table(Row,Row(Cell(Para),Cell(Para(Text'Hello')))),Para)", null, null);

        	/*
        	 * inserting row at 2 with insertDefaultCells true
        	 * ---------------------------------
        	 * -
        	 * -       -       - Hello -
        	 * -       -       -       -       -
        	 * ---------------------------------
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_ROWS.value() + "\",\"" + OCKey.START.value() + "\":[0,2],\"count\":1,\"" + OCKey.INSERT_DEFAULT_CELLS.value() + "\":true}]",
        			"Root(Table(Row,Row(Cell(Para),Cell(Para(Text'Hello'))),Row(Cell(Para),Cell(Para),Cell(Para),Cell(Para))),Para)", null, null);

        	/*
        	 * delete table at 0
        	 * P
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.DELETE.value() + "\",\"" + OCKey.START.value() + "\":[0]}]",
        			"Root(Para)", null, null);

        	/*
        	 * insert table at 1
        	 * P
        	 * ------
        	 * ------
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_TABLE.value() + "\",\"" + OCKey.START.value() + "\":[1]}]",
        			"Root(Para,Table)", null, null);

        	/*
        	 * insert 2 rows and 1 column
        	 * P
        	 * ------
        	 * -    -
        	 * -    -
        	 * ------
        	 */
        	TestUtils.applyAndCheck(operationDocument,
        			"[{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_ROWS.value() + "\",\"" + OCKey.START.value() + "\":[1,0],\"count\":2},{\"" + OCKey.NAME.value() + "\":\"" + OCValue.INSERT_COLUMN.value() + "\",\"" + OCKey.START.value() + "\":[1],\"" + OCKey.GRID_POSITION.value() + "\":0,\"" + OCKey.TABLE_GRID.value() + "\":[1]}]",
        			"Root(Para,Table(Row(Cell(Para)),Row(Cell(Para))))", null, null);

        }
        catch (Throwable e) {
        	e.printStackTrace(System.out);
            fail("odtTableTest failed:" + e.getMessage());
        }
    }
}
