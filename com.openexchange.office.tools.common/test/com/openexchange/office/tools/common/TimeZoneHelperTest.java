/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TimeZoneHelperTest {
    private static final String STR_UTC_TZ = "UTC";
    private static final String STR_GMT_TZ = "GMT";
    private static final String STR_CET_TZ = "CET";
    private static final String STR_EUROPE_BERLIN_TZ = "Europe/Berlin";
    private static final String STR_AMERICA_LOS_ANGELES_TZ = "America/Los Angeles";
    private static final String STR_ASIA_TOKYO_TZ = "Asia/Tokyo";
    private static final String STR_PACIFIC_GUADCALCANAL_TZ = "Pacific/Guadalcanal";
    private static final String STR_AFRICA_CAIRO_TZ = "Africa/Cairo";
    private static final String STR_AUSTRALIA_SYDNEY_TZ = "Australia/Sydney";
    private static final String STR_AMERICA_ANCHORAGE_TZ = "America/Anchorage";
    private static final Set<String> SET_WITH_TZ = new HashSet<>();

    //-------------------------------------------------------------------------
    @BeforeEach
    public void setUp()
        throws Exception
    {
        SET_WITH_TZ.add(STR_UTC_TZ);
        SET_WITH_TZ.add(STR_GMT_TZ);
        SET_WITH_TZ.add(STR_EUROPE_BERLIN_TZ);
        SET_WITH_TZ.add(STR_AMERICA_LOS_ANGELES_TZ);
        SET_WITH_TZ.add(STR_ASIA_TOKYO_TZ);
        SET_WITH_TZ.add(STR_PACIFIC_GUADCALCANAL_TZ);
        SET_WITH_TZ.add(STR_AFRICA_CAIRO_TZ);
        SET_WITH_TZ.add(STR_AUSTRALIA_SYDNEY_TZ);
        SET_WITH_TZ.add(STR_AMERICA_ANCHORAGE_TZ);
        SET_WITH_TZ.add(STR_CET_TZ);
    }

    //-------------------------------------------------------------------------
    @AfterEach
    public void tearDown()
        throws Exception
    {
        // nothing to do
    }

    //-------------------------------------------------------------------------
    @Test
    public void testTimeZoneHelperInstances() {
        final TimeZone aUTCTimeZoneFromTimeZone = TimeZone.getTimeZone(STR_UTC_TZ);
        final TimeZone aUTCTimeZoneFromHelper   = TimeZoneHelper.getTimeZone(STR_UTC_TZ);

        assertEquals(aUTCTimeZoneFromTimeZone, aUTCTimeZoneFromHelper);

        final TimeZone aGMTTimeZoneFromTimeZone = TimeZone.getTimeZone(STR_GMT_TZ);
        final TimeZone aGMTTimeZoneFromHelper   = TimeZoneHelper.getTimeZone(STR_GMT_TZ);

        assertEquals(aGMTTimeZoneFromTimeZone, aGMTTimeZoneFromHelper);

        final TimeZone aEurBerlinTimeZoneFromTimeZone = TimeZone.getTimeZone(STR_EUROPE_BERLIN_TZ);
        final TimeZone aEurBerlinTimeZoneFromHelper   = TimeZoneHelper.getTimeZone(STR_EUROPE_BERLIN_TZ);

        assertEquals(aEurBerlinTimeZoneFromTimeZone, aEurBerlinTimeZoneFromHelper);

        final TimeZone aLATimeZoneFromTimeZone = TimeZone.getTimeZone(STR_AMERICA_LOS_ANGELES_TZ);
        final TimeZone aLATimeZoneFromHelper   = TimeZoneHelper.getTimeZone(STR_AMERICA_LOS_ANGELES_TZ);

        assertEquals(aLATimeZoneFromTimeZone, aLATimeZoneFromHelper);
    }

    //-------------------------------------------------------------------------
    @Test
    public void testTimeZoneHelperMultiThread() {
        final Set<String> fixedTimeZoneStrings = Collections.unmodifiableSet(SET_WITH_TZ);

        final int NUM_OF_THREADS = 16;
        final AtomicInteger atomicCounter = new AtomicInteger(0);
        final Executor executor = Executors.newFixedThreadPool(NUM_OF_THREADS);
        final CompletionService<TimeZone> completionService =
               new ExecutorCompletionService<TimeZone>(executor);

        for(int t = 0; t < NUM_OF_THREADS; t++) {
            completionService.submit(new Callable<TimeZone>() {

                @Override
                public TimeZone call() {

                    TimeZone tz = null;
                    for (int i = 0; i < 1000000; i++) {
                        final Iterator<String> tzStrIter = fixedTimeZoneStrings.iterator();

                        while (tzStrIter.hasNext()) {
                            tz = TimeZoneHelper.getTimeZone(tzStrIter.next());
                        }
                    }
                    return tz;
                }
           });
        }

        final long startTime = System.currentTimeMillis();
        int received = 0;
        boolean errors = false;

        while(received < NUM_OF_THREADS && !errors) {
            try {
                Future<TimeZone> resultFuture = completionService.take();
                try {
                    TimeZone result = resultFuture.get();
                    received ++;

                    if (null != result) {
                        atomicCounter.incrementAndGet();
                    }
                }
                catch(Exception e) {
                       //log
                   errors = true;
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        final long endTime = System.currentTimeMillis();
        assertEquals(NUM_OF_THREADS, atomicCounter.get());
        System.out.println("Time: " + (endTime - startTime));
    }

    //-------------------------------------------------------------------------
    @Test
    public void testTimeZoneMultiThread() {
        final Set<String> fixedTimeZoneStrings = Collections.unmodifiableSet(SET_WITH_TZ);

        final int NUM_OF_THREADS = 16;
        final AtomicInteger atomicCounter = new AtomicInteger(0);
        final Executor executor = Executors.newFixedThreadPool(NUM_OF_THREADS);
        final CompletionService<TimeZone> completionService =
               new ExecutorCompletionService<TimeZone>(executor);

        for(int t = 0; t < NUM_OF_THREADS; t++) {
            completionService.submit(new Callable<TimeZone>() {

                @Override
                public TimeZone call() {

                    TimeZone tz = null;
                    for (int i = 0; i < 1000000; i++) {
                        final Iterator<String> tzStrIter = fixedTimeZoneStrings.iterator();

                        while (tzStrIter.hasNext()) {
                            tz = TimeZone.getTimeZone(tzStrIter.next());
                        }
                    }
                    return tz;
                }
           });
        }

        final long startTime = System.currentTimeMillis();
        int received = 0;
        boolean errors = false;

        while(received < NUM_OF_THREADS && !errors) {
            try {
                Future<TimeZone> resultFuture = completionService.take();
                try {
                    TimeZone result = resultFuture.get();
                    received ++;

                    if (null != result) {
                        atomicCounter.incrementAndGet();
                    }
                }
                catch(Exception e) {
                       //log
                   errors = true;
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        final long endTime = System.currentTimeMillis();
        assertEquals(NUM_OF_THREADS, atomicCounter.get());
        System.out.println("Time: " + (endTime - startTime));
    }
}
