/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.json;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JSONHelperTest {
    private final String PROP_SIMPLE = "prop_simple";
    private final String PROP_COMPLEX_INTEGER = "prop_complex_integer";
    private final String PROP_COMPLEX_STRING = "prop_complex_string";
    private final String PROP_COMPLEX_JSONOBJ = "prop_complex_jsonobj";
    private final String PROP_COMPLEX_JSONARRAY = "prop_complex_jsonarray";
    private final String PROP_COMPLEX_STRING_VALUE = "This is a small test string with some characters";

    private final JSONObject objEmpty = new JSONObject();
    private final JSONObject objFilledSimple = new JSONObject();
    private final JSONObject objFilledComplex = new JSONObject();

    private final JSONArray sourceEmpty = new JSONArray();
    private final JSONArray sourceOneInteger = new JSONArray();
    private final JSONArray targetTemplateFilled = new JSONArray();

    //-------------------------------------------------------------------------
    @BeforeEach
    public void setUp() throws Exception {
        sourceOneInteger.put(10);

        objFilledSimple.put(PROP_SIMPLE, 10);
        objFilledComplex.put(PROP_COMPLEX_INTEGER, 10);
        objFilledComplex.put(PROP_COMPLEX_STRING, PROP_COMPLEX_STRING_VALUE);
        objFilledComplex.put(PROP_COMPLEX_JSONOBJ, objFilledSimple);
        objFilledComplex.put(PROP_COMPLEX_JSONARRAY, sourceOneInteger);

        targetTemplateFilled.put(10);
        targetTemplateFilled.put(objEmpty);
        targetTemplateFilled.put(objFilledSimple);
        targetTemplateFilled.put(objFilledComplex);
        targetTemplateFilled.put(sourceOneInteger);
    }

    //-------------------------------------------------------------------------
    @AfterEach
    public void tearDown() throws Exception {
    }

    //-------------------------------------------------------------------------
    // JSONArray com.openexchange.office.tools.json.JSONHelper.appendArray(JSONArray target, JSONArray source) throws JSONException
    @Test
    public void testAppendArray2 () {
        JSONArray targetEmpty = new JSONArray();

        try {
            final JSONArray result1 = JSONHelper.appendArray(targetEmpty, sourceEmpty);
            assertTrue(result1 != null);
            assertTrue(result1.length() == 0);
            assertTrue(result1.isEmpty());
            assertTrue(result1 == targetEmpty);

            targetEmpty = new JSONArray();
            final JSONArray result2 = JSONHelper.appendArray(targetEmpty, sourceOneInteger);
            assertTrue(result2 != null);
            assertTrue(result2.length() == 1);
            assertFalse(result2.isEmpty());
            assertTrue(result2 == targetEmpty);
            assertTrue(result2.getInt(0) == 10);

            JSONArray targetOneElement = new JSONArray();
            targetOneElement.put(10);
            final JSONArray result3 = JSONHelper.appendArray(targetOneElement, sourceEmpty);
            assertTrue(result3 != null);
            assertTrue(result3.length() == 1);
            assertFalse(result3.isEmpty());
            assertTrue(result3 == targetOneElement);
            assertTrue(result3.getInt(0) == 10);

            targetOneElement = new JSONArray();
            targetOneElement.put(9);
            final JSONArray result4 = JSONHelper.appendArray(targetOneElement, sourceOneInteger);
            assertTrue(result4 != null);
            assertTrue(result4.length() == 2);
            assertFalse(result4.isEmpty());
            assertTrue(result4 == targetOneElement);
            assertTrue(result4.getInt(0) == 9);
            assertTrue(result4.getInt(1) == 10);

            JSONArray target = new JSONArray();
            target.put(1);
            target.put(2);
            target.put(3);
            target.put(4);
            JSONArray source = new JSONArray();
            source.put(5);
            source.put(6);
            source.put(7);
            source.put(8);
            final JSONArray result5 = JSONHelper.appendArray(target, source);
            assertTrue(result5 != null);
            assertTrue(result5.length() == 8);
            assertFalse(result5.isEmpty());
            assertTrue(result5 == target);
            assertTrue(result5.getInt(0) == 1);
            assertTrue(result5.getInt(1) == 2);
            assertTrue(result5.getInt(2) == 3);
            assertTrue(result5.getInt(3) == 4);
            assertTrue(result5.getInt(4) == 5);
            assertTrue(result5.getInt(5) == 6);
            assertTrue(result5.getInt(6) == 7);
            assertTrue(result5.getInt(7) == 8);

            final JSONArray result6 = JSONHelper.appendArray(null, sourceEmpty);
            assertTrue(result6 == null);

            targetEmpty = new JSONArray();
            final JSONArray result7 = JSONHelper.appendArray(targetEmpty, null);
            assertTrue(result7 != null);
            assertTrue(result7.length() == 0);
            assertTrue(result7.isEmpty());
            assertTrue(result7 == targetEmpty);

            target = new JSONArray();
            target.put(1);
            target.put(2);
            target.put(3);
            final JSONArray result8 = JSONHelper.appendArray(target, null);
            assertTrue(result8 != null);
            assertTrue(result8.length() == 3);
            assertFalse(result8.isEmpty());
            assertTrue(result8 == target);

            final JSONArray result9 = JSONHelper.appendArray(null, sourceEmpty);
            assertTrue(result9 == null);

            target = new JSONArray();
            final JSONArray result10 = JSONHelper.appendArray(target, null);
            assertTrue(result10 != null);
            assertTrue(result10.isEmpty());

            final JSONArray result11 = JSONHelper.appendArray(null, null);
            assertTrue(result11 == null);
        } catch (Exception e) {
            fail("Unexpected exception during appendArray2 test");
        }

    }

    //-------------------------------------------------------------------------
    // JSONArray com.openexchange.office.tools.json.JSONHelper.appendArray(JSONArray target, JSONArray source, int start) throws JSONException
    @Test
    public void testAppendArray3 () {
        JSONArray targetEmpty = new JSONArray();

        try {
            final JSONArray result1 = JSONHelper.appendArray(targetEmpty, sourceEmpty, -1);
            assertTrue(result1 != null);
            assertTrue(result1.length() == 0);
            assertTrue(result1.isEmpty());
            assertTrue(result1 == targetEmpty);

            final JSONArray result2 = JSONHelper.appendArray(targetEmpty, sourceEmpty, 0);
            assertTrue(result2 != null);
            assertTrue(result2.length() == 0);
            assertTrue(result2.isEmpty());
            assertTrue(result2 == targetEmpty);

            final JSONArray result3 = JSONHelper.appendArray(targetEmpty, sourceEmpty, 1);
            assertTrue(result3 != null);
            assertTrue(result3.length() == 0);
            assertTrue(result3.isEmpty());
            assertTrue(result3 == targetEmpty);

            targetEmpty = new JSONArray();
            final JSONArray result4 = JSONHelper.appendArray(targetEmpty, sourceOneInteger, 0);
            assertTrue(result4 != null);
            assertTrue(result4.length() == 1);
            assertFalse(result4.isEmpty());
            assertTrue(result4 == targetEmpty);
            assertTrue(result4.getInt(0) == 10);

            targetEmpty = new JSONArray();
            final JSONArray result5 = JSONHelper.appendArray(targetEmpty, sourceOneInteger, -1);
            assertTrue(result5 != null);
            assertTrue(result5.length() == 1);
            assertFalse(result5.isEmpty());
            assertTrue(result5 == targetEmpty);
            assertTrue(result5.getInt(0) == 10);

            targetEmpty = new JSONArray();
            final JSONArray result6 = JSONHelper.appendArray(targetEmpty, sourceOneInteger, 1);
            assertTrue(result6 != null);
            assertTrue(result6.length() == 0);
            assertTrue(result6.isEmpty());
            assertTrue(result6 == targetEmpty);

            JSONArray targetOneElement = new JSONArray();
            targetOneElement.put(9);
            final JSONArray result7 = JSONHelper.appendArray(targetOneElement, sourceOneInteger, 0);
            assertTrue(result7 != null);
            assertTrue(result7.length() == 2);
            assertFalse(result7.isEmpty());
            assertTrue(result7 == targetOneElement);
            assertTrue(result7.getInt(0) == 9);
            assertTrue(result7.getInt(1) == 10);

            targetOneElement = new JSONArray();
            targetOneElement.put(9);
            final JSONArray result8 = JSONHelper.appendArray(targetOneElement, sourceOneInteger, 1);
            assertTrue(result8 != null);
            assertTrue(result8.length() == 1);
            assertFalse(result8.isEmpty());
            assertTrue(result8 == targetOneElement);
            assertTrue(result8.getInt(0) == 9);

            JSONArray target = new JSONArray();
            target.put(1);
            target.put(2);
            target.put(3);
            target.put(4);
            JSONArray source = new JSONArray();
            source.put(5);
            source.put(6);
            source.put(7);
            source.put(8);
            final JSONArray result9 = JSONHelper.appendArray(target, source, 0);
            assertTrue(result9 != null);
            assertTrue(result9.length() == 8);
            assertFalse(result9.isEmpty());
            assertTrue(result9 == target);
            assertTrue(result9.getInt(0) == 1);
            assertTrue(result9.getInt(1) == 2);
            assertTrue(result9.getInt(2) == 3);
            assertTrue(result9.getInt(3) == 4);
            assertTrue(result9.getInt(4) == 5);
            assertTrue(result9.getInt(5) == 6);
            assertTrue(result9.getInt(6) == 7);
            assertTrue(result9.getInt(7) == 8);

            target = new JSONArray();
            target.put(1);
            target.put(2);
            target.put(3);
            target.put(4);
            source = new JSONArray();
            source.put(5);
            source.put(6);
            source.put(7);
            source.put(8);
            final JSONArray result10 = JSONHelper.appendArray(target, source, 2);
            assertTrue(result10 != null);
            assertTrue(result10.length() == 6);
            assertFalse(result10.isEmpty());
            assertTrue(result10 == target);
            assertTrue(result10.getInt(0) == 1);
            assertTrue(result10.getInt(1) == 2);
            assertTrue(result10.getInt(2) == 3);
            assertTrue(result10.getInt(3) == 4);
            assertTrue(result10.getInt(4) == 7);
            assertTrue(result10.getInt(5) == 8);

            final JSONArray result11 = JSONHelper.appendArray(null, sourceEmpty, 0);
            assertTrue(result11 == null);

            target = new JSONArray();
            final JSONArray result12 = JSONHelper.appendArray(target, null, 0);
            assertTrue(result12 != null);
            assertTrue(result12.isEmpty());

            final JSONArray result13 = JSONHelper.appendArray(null, null, 0);
            assertTrue(result13 == null);
        } catch (Exception e) {
            fail("Unexpected exception during appendArray3 test");
        }
    }

    //-------------------------------------------------------------------------
    // boolean com.openexchange.office.tools.json.JSONHelper.isNullOrEmpty(JSONArray array)
    // boolean com.openexchange.office.tools.json.JSONHelper.isNotNullAndEmpty(JSONArray array)
    // boolean com.openexchange.office.tools.json.JSONHelper.isNullOrEmpty(JSONObject jsonObj)
    // boolean com.openexchange.office.tools.json.JSONHelper.isNotNullAndEmpty(JSONObject jsonObj)
    @Test
    public void testIsMethods() {
        final JSONArray nullArray = null;
        final JSONObject nullObj = null;
        final JSONArray emptyArray = new JSONArray();
        final JSONObject emptyObj = new JSONObject();

        assertTrue(JSONHelper.isNullOrEmpty(nullArray));
        assertTrue(JSONHelper.isNullOrEmpty(emptyArray));
        assertFalse(JSONHelper.isNullOrEmpty(sourceOneInteger));

        assertFalse(JSONHelper.isNotNullAndEmpty(nullArray));
        assertFalse(JSONHelper.isNotNullAndEmpty(emptyArray));
        assertTrue(JSONHelper.isNotNullAndEmpty(sourceOneInteger));

        assertTrue(JSONHelper.isNullOrEmpty(nullObj));
        assertTrue(JSONHelper.isNullOrEmpty(emptyObj));
        assertFalse(JSONHelper.isNullOrEmpty(objFilledSimple));

        assertFalse(JSONHelper.isNotNullAndEmpty(nullObj));
        assertFalse(JSONHelper.isNotNullAndEmpty(emptyObj));
        assertTrue(JSONHelper.isNotNullAndEmpty(objFilledSimple));
    }

    //-------------------------------------------------------------------------
    // JSONArray com.openexchange.office.tools.json.JSONHelper.subArray(JSONArray source, int startIndex) throws JSONException
    @Test
    public void testSubArray2() {
        JSONArray sourceEmpty = new JSONArray();

        try {
            final JSONArray result1 = JSONHelper.subArray(sourceEmpty, -1);
            assertTrue(result1 != null);
            assertTrue(result1.length() == 0);
            assertTrue(result1.isEmpty());
            assertFalse(result1 == sourceEmpty);

            final JSONArray result2 = JSONHelper.subArray(sourceEmpty, 0);
            assertTrue(result2 != null);
            assertTrue(result2.length() == 0);
            assertTrue(result2.isEmpty());
            assertFalse(result2 == sourceEmpty);

            final JSONArray result3 = JSONHelper.subArray(sourceEmpty, 1);
            assertTrue(result3 != null);
            assertTrue(result3.length() == 0);
            assertTrue(result3.isEmpty());
            assertFalse(result3 == sourceEmpty);

            final JSONArray result4 = JSONHelper.subArray(sourceOneInteger, -1);
            assertTrue(result4 != null);
            assertTrue(result4.length() == 1);
            assertFalse(result4.isEmpty());
            assertFalse(result4 == sourceOneInteger);

            final JSONArray result5 = JSONHelper.subArray(sourceOneInteger, 0);
            assertTrue(result5 != null);
            assertTrue(result5.length() == 1);
            assertFalse(result5.isEmpty());
            assertFalse(result5 == sourceOneInteger);

            final JSONArray result6 = JSONHelper.subArray(sourceOneInteger, 1);
            assertTrue(result6 != null);
            assertTrue(result6.length() == 0);
            assertTrue(result6.isEmpty());
            assertFalse(result5 == sourceOneInteger);

            JSONArray filledArray = new JSONArray();
            filledArray.put(1);
            filledArray.put(2);
            filledArray.put(3);
            filledArray.put(4);
            final JSONArray result7 = JSONHelper.subArray(filledArray, 0);
            assertTrue(result7 != null);
            assertTrue(result7.length() == filledArray.length());
            assertFalse(result7.isEmpty());
            assertFalse(result7 == filledArray);

            final JSONArray result8 = JSONHelper.subArray(filledArray, 1);
            assertTrue(result8 != null);
            assertTrue(result8.length() == 3);
            assertFalse(result8.isEmpty());
            assertFalse(result8 == filledArray);
            assertTrue(result8.getInt(0) == 2);
            assertTrue(result8.getInt(1) == 3);
            assertTrue(result8.getInt(2) == 4);

            final JSONArray result9 = JSONHelper.subArray(null, -1);
            assertNull(result9);

            final JSONArray result10 = JSONHelper.subArray(null, 0);
            assertNull(result10);

            final JSONArray result11 = JSONHelper.subArray(null, 1);
            assertNull(result11);
        } catch (Exception e) {
            fail("Unexpected exception during subArray2 test");
        }
    }

    //-------------------------------------------------------------------------
    // JSONArray com.openexchange.office.tools.json.JSONHelper.subArray(JSONArray source, int startIndex, int endIndex) throws JSONException
    @Test
    public void testSubArray3() {
        JSONArray sourceEmpty = new JSONArray();

        try {
            final JSONArray result1 = JSONHelper.subArray(sourceEmpty, -1, -1);
            assertTrue(result1 != null);
            assertTrue(result1.length() == 0);
            assertTrue(result1.isEmpty());
            assertFalse(result1 == sourceEmpty);

            final JSONArray result2 = JSONHelper.subArray(sourceEmpty, 0, 0);
            assertTrue(result2 != null);
            assertTrue(result2.length() == 0);
            assertTrue(result2.isEmpty());
            assertFalse(result2 == sourceEmpty);

            final JSONArray result3 = JSONHelper.subArray(sourceEmpty, 1, 1);
            assertTrue(result3 != null);
            assertTrue(result3.length() == 0);
            assertTrue(result3.isEmpty());
            assertFalse(result3 == sourceEmpty);

            final JSONArray result4 = JSONHelper.subArray(sourceOneInteger, 0, 1);
            assertTrue(result4 != null);
            assertTrue(result4.length() == 1);
            assertFalse(result4.isEmpty());
            assertFalse(result4 == sourceOneInteger);

            JSONArray filledArray = new JSONArray();
            filledArray.put(1);
            filledArray.put(2);
            filledArray.put(3);
            filledArray.put(4);
            final JSONArray result5 = JSONHelper.subArray(filledArray, 0, 4);
            assertTrue(result5 != null);
            assertTrue(result5.length() == filledArray.length());
            assertFalse(result5.isEmpty());
            assertFalse(result5 == filledArray);

            final JSONArray result6 = JSONHelper.subArray(filledArray, 1, 2);
            assertTrue(result6 != null);
            assertTrue(result6.length() == 1);
            assertFalse(result6.isEmpty());
            assertFalse(result6 == filledArray);
            assertTrue(result6.getInt(0) == 2);

            final JSONArray result7 = JSONHelper.subArray(filledArray, 3, 1);
            assertTrue(result7 != null);
            assertTrue(result7.length() == 0);
            assertTrue(result7.isEmpty());
            assertFalse(result7 == filledArray);

            final JSONArray result9 = JSONHelper.subArray(null, -1, -1);
            assertNull(result9);

            final JSONArray result10 = JSONHelper.subArray(null, 0, 0);
            assertNull(result10);

            final JSONArray result11 = JSONHelper.subArray(null, 1, 1);
            assertNull(result11);
        } catch (Exception e) {
            fail("Unexpected exception during subArray3 test");
        }
    }

    //-------------------------------------------------------------------------
    // void com.openexchange.office.tools.json.JSONHelper.remove(JSONArray source, int startIndex, int length) throws JSONException
    @Test
    public void testRemove() {

        try {
            JSONArray sourceEmpty = new JSONArray();
            JSONHelper.remove(sourceEmpty, -1, -1);
            assertTrue(sourceEmpty.length() == 0);
            assertTrue(sourceEmpty.isEmpty());

            sourceEmpty = new JSONArray();
            JSONHelper.remove(sourceEmpty, 0, 0);
            assertTrue(sourceEmpty.length() == 0);
            assertTrue(sourceEmpty.isEmpty());

            sourceEmpty = new JSONArray();
            JSONHelper.remove(sourceEmpty, 1, 1);
            assertTrue(sourceEmpty.length() == 0);
            assertTrue(sourceEmpty.isEmpty());

            JSONArray filledArray = new JSONArray();
            filledArray.put(1);
            filledArray.put(2);
            filledArray.put(3);
            filledArray.put(4);
            JSONHelper.remove(filledArray, 0, 1);
            assertTrue(filledArray.length() == 3);
            assertFalse(filledArray.isEmpty());
            assertTrue(filledArray.getInt(0) == 2);
            assertTrue(filledArray.getInt(1) == 3);
            assertTrue(filledArray.getInt(2) == 4);

            filledArray = new JSONArray();
            filledArray.put(1);
            filledArray.put(2);
            filledArray.put(3);
            filledArray.put(4);
            JSONHelper.remove(filledArray, 3, 1);
            assertTrue(filledArray.length() == 3);
            assertFalse(filledArray.isEmpty());
            assertTrue(filledArray.getInt(0) == 1);
            assertTrue(filledArray.getInt(1) == 2);
            assertTrue(filledArray.getInt(2) == 3);

            filledArray = new JSONArray();
            filledArray.put(1);
            filledArray.put(2);
            filledArray.put(3);
            filledArray.put(4);
            JSONHelper.remove(filledArray, 1, 1);
            assertTrue(filledArray.length() == 3);
            assertFalse(filledArray.isEmpty());
            assertTrue(filledArray.getInt(0) == 1);
            assertTrue(filledArray.getInt(1) == 3);
            assertTrue(filledArray.getInt(2) == 4);

            filledArray = new JSONArray();
            filledArray.put(1);
            filledArray.put(2);
            filledArray.put(3);
            filledArray.put(4);
            JSONHelper.remove(filledArray, 1, 10);
            assertTrue(filledArray.length() == 1);
            assertFalse(filledArray.isEmpty());
            assertTrue(filledArray.getInt(0) == 1);

            filledArray = new JSONArray();
            filledArray.put(1);
            filledArray.put(2);
            filledArray.put(3);
            filledArray.put(4);
            JSONHelper.remove(filledArray, -1, 2);
            assertTrue(filledArray.length() == 3);
            assertFalse(filledArray.isEmpty());
            assertTrue(filledArray.getInt(0) == 2);
            assertTrue(filledArray.getInt(1) == 3);
            assertTrue(filledArray.getInt(2) == 4);

            JSONHelper.remove(null, 0, 0);
        } catch (Exception e) {
            fail("Unexpected exception during subArray test");
        }
    }

    //-------------------------------------------------------------------------
    // JSONObject com.openexchange.office.tools.json.JSONHelper.cloneJSONObject(JSONObject source) throws JSONException
    @Test
    public void testCloneObject() {

        try {
            final JSONObject result1 = JSONHelper.cloneJSONObject(objFilledSimple);
            assertTrue(result1 != objFilledSimple);
            assertTrue(result1.length() == objFilledSimple.length());
            assertTrue(result1.getInt(PROP_SIMPLE) == objFilledSimple.getInt(PROP_SIMPLE));

            final JSONObject result2 = JSONHelper.cloneJSONObject(objFilledComplex);
            assertTrue(result2 != objFilledComplex);
            assertTrue(result2.length() == objFilledComplex.length());

            assertTrue(result2.keySet()
                                     .stream()
                                     .allMatch(k -> { return isPropertyEqual(k, result2, objFilledComplex); }));

            assertFalse(result2.get(PROP_COMPLEX_JSONOBJ) == objFilledComplex.get(PROP_COMPLEX_JSONOBJ));
            assertFalse(result2.get(PROP_COMPLEX_JSONARRAY) == objFilledComplex.get(PROP_COMPLEX_JSONARRAY));
        } catch (Exception e) {
            fail("Unexpected exception during cloneJSONObject test");
        }
    }

    //-------------------------------------------------------------------------
    // JSONObject com.openexchange.office.tools.json.JSONHelper.cloneJSONObject(JSONObject source) throws JSONException
    @Test
    public void testCloneArray() {

        try {
            final JSONArray sourceEmpty = new JSONArray();

            final JSONArray result1 = JSONHelper.cloneJSONArray(sourceEmpty);
            assertTrue(result1 != sourceEmpty);
            assertTrue(result1.length() == sourceEmpty.length());

            final JSONArray result2 = JSONHelper.cloneJSONArray(targetTemplateFilled);
            assertTrue(result2 != targetTemplateFilled);
            assertTrue(result2.length() == targetTemplateFilled.length());

            for (int i=0; i < result2.length(); i++) {
                final Object o1 = result2.get(i);
                final Object o2 = targetTemplateFilled.get(i);
                assertTrue(o1.equals(o2));

                if ((o1 instanceof JSONObject) || (o1 instanceof JSONArray)) {
                    assertFalse(o1 == o2);
                }
            }
        } catch (Exception e) {
            fail("Unexpected exception during cloneJSONArray test");
        }
    }

    //-------------------------------------------------------------------------
    // JSONArray com.openexchange.office.tools.json.JSONHelper.shallowCopy(JSONArray array) throws JSONException
    @Test
    public void testShallowCopy() {

        try {
            final JSONArray sourceEmpty = new JSONArray();

            final JSONArray result1 = JSONHelper.shallowCopy(sourceEmpty);
            assertTrue(result1 != sourceEmpty);
            assertTrue(result1.length() == sourceEmpty.length());

            final JSONArray result2 = JSONHelper.shallowCopy(targetTemplateFilled);
            assertTrue(result2 != targetTemplateFilled);
            assertTrue(result2.length() == targetTemplateFilled.length());

            for (int i=0; i < result2.length(); i++) {
                final Object o1 = result2.get(i);
                final Object o2 = targetTemplateFilled.get(i);
                assertTrue(o1.equals(o2));

                if ((o1 instanceof JSONObject) || (o1 instanceof JSONArray)) {
                    assertTrue(o1 == o2);
                }
            }

            final JSONArray result3 = JSONHelper.shallowCopy(null);
            assertTrue(result3 == null);

        } catch (Exception e) {
            fail("Unexpected exception during shallowCopy test");
        }
    }

    //-------------------------------------------------------------------------
    // JSONArray subArrayLength(final JSONArray source, int startIndex, int length) throws JSONException {
    @Test
    public void testSubArrayLength() {
        try {
            JSONArray sourceEmpty = new JSONArray();

            final JSONArray result1 = JSONHelper.subArrayLength(sourceEmpty, -1, -1);
            assertTrue(result1 != null);
            assertTrue(result1.length() == 0);
            assertTrue(result1.isEmpty());
            assertFalse(result1 == sourceEmpty);

            final JSONArray result2 = JSONHelper.subArrayLength(sourceEmpty, 0, 0);
            assertTrue(result2 != null);
            assertTrue(result2.length() == 0);
            assertTrue(result2.isEmpty());
            assertFalse(result2 == sourceEmpty);

            final JSONArray result3 = JSONHelper.subArrayLength(sourceEmpty, 1, 1);
            assertTrue(result3 != null);
            assertTrue(result3.length() == 0);
            assertTrue(result3.isEmpty());
            assertFalse(result3 == sourceEmpty);

            final JSONArray result4 = JSONHelper.subArrayLength(sourceOneInteger, 0, 1);
            assertTrue(result4 != null);
            assertTrue(result4.length() == 1);
            assertFalse(result4.isEmpty());
            assertFalse(result4 == sourceOneInteger);

            JSONArray filledArray = new JSONArray();
            filledArray.put(1);
            filledArray.put(2);
            filledArray.put(3);
            filledArray.put(4);

            final JSONArray result5 = JSONHelper.subArrayLength(filledArray, -1, 4);
            assertTrue(result5 != null);
            assertTrue(result5.length() == 3);
            assertFalse(result5.isEmpty());
            assertFalse(result5 == filledArray);
            assertTrue(result5.getInt(0) == 1);
            assertTrue(result5.getInt(1) == 2);
            assertTrue(result5.getInt(2) == 3);

            final JSONArray result6 = JSONHelper.subArrayLength(filledArray, 0, 4);
            assertTrue(result6 != null);
            assertTrue(result6.length() == filledArray.length());
            assertFalse(result6.isEmpty());
            assertFalse(result6 == filledArray);

            final JSONArray result7 = JSONHelper.subArrayLength(filledArray, 1, 1);
            assertTrue(result7 != null);
            assertTrue(result7.length() == 1);
            assertFalse(result7.isEmpty());
            assertFalse(result7 == filledArray);
            assertTrue(result7.getInt(0) == 2);

            final JSONArray result8 = JSONHelper.subArrayLength(filledArray, 3, 3);
            assertTrue(result8 != null);
            assertTrue(result8.length() == 1);
            assertFalse(result8.isEmpty());
            assertFalse(result8 == filledArray);
            assertTrue(result8.getInt(0) == 4);

            final JSONArray result9 = JSONHelper.subArrayLength(null, -1, -1);
            assertNull(result9);

            final JSONArray result10 = JSONHelper.subArrayLength(null, 0, 0);
            assertNull(result10);

            final JSONArray result11 = JSONHelper.subArrayLength(null, 1, 1);
            assertNull(result11);
        } catch (Exception e) {
            fail("Unexpected exception during subArrayLength test");
        }
    }

    //-------------------------------------------------------------------------
    public static boolean isPropertyEqual(String key, JSONObject obj1, JSONObject obj2) {
        boolean result = false;

        try {
            Object o1 = obj1.get(key);
            Object o2 = obj2.get(key);
            result = (o1.equals(o2));
        } catch (JSONException e) {
            // result will be false
        }

        return result;
    }
}
