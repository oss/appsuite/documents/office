/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.office.tools.common.error.ErrorCode;

public class RecentFileListTest {

    //-------------------------------------------------------------------------
    @BeforeEach
    public void setUp()
        throws Exception
    {
    }

    //-------------------------------------------------------------------------
    @AfterEach
    public void tearDown()
        throws Exception
    {
    }

    //-------------------------------------------------------------------------
    @Test
    public void testRecentFileListErrorBehaviour ()
        throws Exception
    {
        ErrorCode errorCode;
        boolean   bIsImportant;

        // FolderExceptionErrorMessage
        /*
        errorCode    = ExceptionToErrorCode.map(FolderExceptionErrorMessage.FOLDER_NOT_VISIBLE.create("infostore", 100, 10, 1), ErrorCode.NO_ERROR, false);
        bIsImportant = RecentFileListManager.isImportantException(errorCode);
        Assert.assertFalse(bIsImportant);

        errorCode    = ExceptionToErrorCode.map(FolderExceptionErrorMessage.NOT_FOUND.create("infostore", 100, 10, 1), ErrorCode.NO_ERROR, false);
        bIsImportant = RecentFileListManager.isImportantException(errorCode);
        Assert.assertFalse(bIsImportant);

        errorCode    = ExceptionToErrorCode.map(FolderExceptionErrorMessage.INVALID_FOLDER_ID.create("infostore", 100, 10, 1), ErrorCode.NO_ERROR, false);
        bIsImportant = RecentFileListManager.isImportantException(errorCode);
        Assert.assertTrue(bIsImportant);

        errorCode    = ExceptionToErrorCode.map(FolderExceptionErrorMessage.DUPLICATE_NAME.create("infostore", "test", "parent"), ErrorCode.GENERAL_UNKNOWN_ERROR, false);
        bIsImportant = RecentFileListManager.isImportantException(errorCode);
        Assert.assertTrue(bIsImportant);
        */
    }
}
