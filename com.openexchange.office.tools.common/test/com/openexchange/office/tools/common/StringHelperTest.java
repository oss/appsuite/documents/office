/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import com.openexchange.office.tools.common.string.StringHelper;

public class StringHelperTest {

    //-------------------------------------------------------------------------
    @Test
    public void testStringHelperReplacePlaceholdersWithArgs() {
        String clientUid = UUID.randomUUID().toString();
        String docUid = "098f6bcd4621d373cade4e832627b4f6";

        String msg1 = StringHelper.replacePlaceholdersWithArgs("Close request has not been answered in time by server with com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {}", clientUid, docUid);
        String msg2 = StringHelper.replacePlaceholdersWithArgs("Close response with error code != NO_ERROR received with com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {}", clientUid, docUid);
        String msg3 = StringHelper.replacePlaceholdersWithArgs("Leave request has not been answered in time by server! com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {}", clientUid, docUid);
        String msg4 = StringHelper.replacePlaceholdersWithArgs("Leave response with error code received, with com.openexchange.rt2.client.uid {} and com.openexchange.rt2.document.uid {}", clientUid, docUid);

        String expectedMsg1 = "Close request has not been answered in time by server with com.openexchange.rt2.client.uid " + clientUid + " and com.openexchange.rt2.document.uid " + docUid;
        String expectedMsg2 = "Close response with error code != NO_ERROR received with com.openexchange.rt2.client.uid " + clientUid + " and com.openexchange.rt2.document.uid " + docUid;
        String expectedMsg3 = "Leave request has not been answered in time by server! com.openexchange.rt2.client.uid " + clientUid + " and com.openexchange.rt2.document.uid " + docUid;
        String expectedMsg4 = "Leave response with error code received, with com.openexchange.rt2.client.uid " + clientUid + " and com.openexchange.rt2.document.uid " + docUid;

        System.out.println("Strings: " + msg1 + " == " + expectedMsg1);

        assertTrue(msg1.equals(expectedMsg1));
        assertTrue(msg2.equals(expectedMsg2));
        assertTrue(msg3.equals(expectedMsg3));
        assertTrue(msg4.equals(expectedMsg4));

        msg1 = StringHelper.replacePlaceholdersWithArgs("test", null, null);
        assertTrue(msg1.equals("test"));

        msg1 = StringHelper.replacePlaceholdersWithArgs("test {}", null, null);
        assertTrue(msg1.equals("test null"));

        msg1 = StringHelper.replacePlaceholdersWithArgs("test {} {}", null, null);
        assertTrue(msg1.equals("test null null"));

        msg1 = StringHelper.replacePlaceholdersWithArgs("test {} {}", "{}", "{}");
        assertTrue(msg1.equals("test {} {}"));

        msg1 = StringHelper.replacePlaceholdersWithArgs("test {}");
        assertTrue(msg1.equals("test null"));

        try {
            StringHelper.replacePlaceholdersWithArgs(null);
            fail("IllegalArgumentException missed!");
        } catch (IllegalArgumentException e) {
            // expected
        }
    }

}
