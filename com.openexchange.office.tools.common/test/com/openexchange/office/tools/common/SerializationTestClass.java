/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

//=============================================================================
class SerializationTestClass implements Serializable
{
	public static final long serialVersionUID = -4389007779672223467L;

	@Override
	public String toString ()
	{
		final StringBuffer sString = new StringBuffer (256);
		sString.append (super.toString ()    );
		sString.append (" {"                 );
		sString.append (" byte="  +m_nByte   );
		sString.append (" short=" +m_nShort  );
		sString.append (" int="   +m_nInt    );
		sString.append (" long="  +m_nLong   );
		sString.append (" bool="  +m_bBool   );
		sString.append (" string="+m_sString );
		sString.append (" }"                 );
		return sString.toString ();
	}

	@Override
	public boolean equals (Object aReference)
	{
		if (aReference == null)
			return false;

		if ( ! (aReference instanceof SerializationTestClass))
			return false;

		final SerializationTestClass aCheck = (SerializationTestClass)aReference;
		if (aCheck.m_nByte  != m_nByte )
			return false;
		if (aCheck.m_nShort != m_nShort)
			return false;
		if (aCheck.m_nInt   != m_nInt  )
			return false;
		if (aCheck.m_nLong  != m_nLong )
			return false;
		if (aCheck.m_bBool  != m_bBool )
			return false;
		if ( ! StringUtils.equals(aCheck.m_sString, m_sString))
			return false;

		return true;
	}

	public byte    m_nByte   = 0;
	public short   m_nShort  = 1;
	public int     m_nInt    = 2;
	public long    m_nLong   = 3;
	public boolean m_bBool   = false;
	public String  m_sString = "test";
}
