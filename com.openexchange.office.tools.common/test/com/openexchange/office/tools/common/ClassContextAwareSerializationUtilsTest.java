/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

//=============================================================================
public class ClassContextAwareSerializationUtilsTest
{
    //-------------------------------------------------------------------------
    @Test
    public void test ()
        throws Exception
    {
        final SerializationTestClass aTestObj = new SerializationTestClass ();

        final String sData = ClassContextAwareSerializationUtils.serializeObject(aTestObj);
        assertFalse(StringUtils.isEmpty(sData), "test [01] serialization result has not to be 'empty'");

        final Object aRestoredObj = ClassContextAwareSerializationUtils.deserializeObject(sData, this);
        assertNotNull(aRestoredObj, "test [02] deserialization result has not to be NULL");
        assertTrue((aRestoredObj instanceof SerializationTestClass), "test [03] deserialization result its wrong class");

        SerializationTestClass aCheck = (SerializationTestClass) aRestoredObj;

        System.out.println (aTestObj);
        System.out.println (aRestoredObj);

        assertEquals(aTestObj, aCheck, "test [04] detect differences between original and restored object");
    }
}
