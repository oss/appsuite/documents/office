/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.error;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.office.tools.common.error.ErrorCode;


public class ErrorCodeTest
{
    //-------------------------------------------------------------------------
    @BeforeEach
    public void setUp()
        throws Exception
    {
    }

    //-------------------------------------------------------------------------
    @AfterEach
    public void tearDown()
        throws Exception
    {
    }

    //-------------------------------------------------------------------------
    @Test
    public void checkPropsGeneralErrorCodes ()
        throws Exception
    {
        ErrorCode checkErrCode;

        checkErrCode = ErrorCode.NO_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_NO_ERROR);
        assertTrue(checkErrCode.isNoError());
        assertFalse(checkErrCode.isError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "NO_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_UNKNOWN_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_UNKNOWN_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_ARGUMENTS_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_ARGUMENTS_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_ARGUMENTS_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_FILE_NOT_FOUND_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_FILE_NOT_FOUND_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_QUOTA_REACHED_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_QUOTA_REACHED_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_QUOTA_REACHED_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_PERMISSION_CREATE_MISSING_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_PERMISSION_CREATE_MISSING_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_PERMISSION_CREATE_MISSING_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_PERMISSION_READ_MISSING_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_PERMISSION_READ_MISSING_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_PERMISSION_READ_MISSING_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_MEMORY_TOO_LOW_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_MEMORY_TOO_LOW_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_MEMORY_TOO_LOW_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_SYSTEM_BUSY_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_SYSTEM_BUSY_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_SYSTEM_BUSY_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_FILE_LOCKED_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_FILE_LOCKED_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_FILE_LOCKED_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_FILE_CORRUPT_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_FILE_CORRUPT_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_FILE_CORRUPT_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_BACKGROUNDSAVE_IN_PROGRESS_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_FOLDER_NOT_VISIBLE_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_FOLDER_NOT_VISIBLE_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_FOLDER_NOT_VISIBLE_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_FOLDER_NOT_FOUND_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_FOLDER_NOT_FOUND_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_FOLDER_NOT_FOUND_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);

        checkErrCode = ErrorCode.GENERAL_FOLDER_INVALID_ID_ERROR;
        assertEquals(checkErrCode.getCode(), ErrorCode.CODE_GENERAL_FOLDER_INVALID_ID_ERROR);
        assertTrue(checkErrCode.isError());
        assertFalse(checkErrCode.isNoError());
        assertEquals(checkErrCode.getCodeAsStringConstant(), "GENERAL_FOLDER_INVALID_ID_ERROR");
        assertEqual(checkErrCode.getAsJSON(), checkErrCode);
    }

    //-------------------------------------------------------------------------
    private void assertEqual(final JSONObject errJSON, final ErrorCode errCode) throws Exception {
        assertEquals(errJSON.get(ErrorCode.JSON_ERROR), errCode.getCodeAsStringConstant());
        assertEquals(errJSON.get(ErrorCode.JSON_ERRORCODE), errCode.getCode());
        assertEquals(errJSON.get(ErrorCode.JSON_ERRORDESCRIPTION), errCode.getDescription());
        assertEquals(errJSON.get(ErrorCode.JSON_ERRORCLASS), errCode.getErrorClass());
    }
}
