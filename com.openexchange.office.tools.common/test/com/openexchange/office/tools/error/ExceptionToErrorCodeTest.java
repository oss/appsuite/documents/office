/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.error;

import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.openexchange.file.storage.FileStorageExceptionCodes;
import com.openexchange.filestore.FileStorageCodes;
import com.openexchange.filestore.QuotaFileStorageExceptionCodes;
import com.openexchange.folderstorage.FolderExceptionErrorMessage;
import com.openexchange.groupware.infostore.InfostoreExceptionCodes;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.ExceptionToErrorCode;

public class ExceptionToErrorCodeTest
{
    //-------------------------------------------------------------------------
    @BeforeEach
    public void setUp()
        throws Exception
    {
    }

    //-------------------------------------------------------------------------
    @AfterEach
    public void tearDown()
        throws Exception
    {
    }

    //-------------------------------------------------------------------------
    @Test
    public void testMapping ()
        throws Exception
    {
        // FolderExceptionErrorMessage
        checkErrorCode(ExceptionToErrorCode.map(FolderExceptionErrorMessage.FOLDER_NOT_VISIBLE.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_FOLDER_NOT_VISIBLE_ERROR.getCode());

        checkErrorCode(ExceptionToErrorCode.map(FolderExceptionErrorMessage.NOT_FOUND.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_FOLDER_NOT_FOUND_ERROR.getCode());

        checkErrorCode(ExceptionToErrorCode.map(FolderExceptionErrorMessage.INVALID_FOLDER_ID.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_FOLDER_INVALID_ID_ERROR.getCode());

        checkErrorCode(ExceptionToErrorCode.map(FolderExceptionErrorMessage.DUPLICATE_NAME.create("infostore"), ErrorCode.GENERAL_UNKNOWN_ERROR, false),
                ErrorCode.GENERAL_UNKNOWN_ERROR.getCode());


        // QuotaFileStorageExceptionCodes
        checkErrorCode(ExceptionToErrorCode.map(QuotaFileStorageExceptionCodes.STORE_FULL.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_QUOTA_REACHED_ERROR.getCode());

        // FileStorageExceptionCodes
        checkErrorCode(ExceptionToErrorCode.map(FileStorageExceptionCodes.FILE_NOT_FOUND.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR.getCode());

        checkErrorCode(ExceptionToErrorCode.map(FileStorageExceptionCodes.ILLEGAL_CHARACTERS.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.RENAMEDOCUMENT_VALIDATION_FAILED_CHARACTERS_ERROR.getCode());

        // FileStorageCodes
        checkErrorCode(ExceptionToErrorCode.map(FileStorageCodes.FILE_NOT_FOUND.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR.getCode());

        // InfostoreExceptionCodes
        checkErrorCode(ExceptionToErrorCode.map(InfostoreExceptionCodes.NO_READ_PERMISSION.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_PERMISSION_READ_MISSING_ERROR.getCode());

        checkErrorCode(ExceptionToErrorCode.map(InfostoreExceptionCodes.NO_WRITE_PERMISSION.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_PERMISSION_WRITE_MISSING_ERROR.getCode());

        checkErrorCode(ExceptionToErrorCode.map(InfostoreExceptionCodes.NOT_EXIST.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR.getCode());

        checkErrorCode(ExceptionToErrorCode.map(InfostoreExceptionCodes.DOCUMENT_NOT_EXIST.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR.getCode());

        checkErrorCode(ExceptionToErrorCode.map(InfostoreExceptionCodes.ALREADY_LOCKED.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.GENERAL_FILE_LOCKED_ERROR.getCode());

        checkErrorCode(ExceptionToErrorCode.map(InfostoreExceptionCodes.VALIDATION_FAILED_CHARACTERS.create("infostore"), ErrorCode.NO_ERROR, false),
                ErrorCode.RENAMEDOCUMENT_VALIDATION_FAILED_CHARACTERS_ERROR.getCode());

    }

    //-------------------------------------------------------------------------
    public void checkErrorCode(ErrorCode errorCode, int nExpectedCode)
    {
        assertTrue(errorCode.isError());
        assertTrue(errorCode.getCode() == nExpectedCode);
    }
}
