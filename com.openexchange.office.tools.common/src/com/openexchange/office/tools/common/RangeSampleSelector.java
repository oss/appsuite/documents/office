/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

//=============================================================================
public class RangeSampleSelector
{
	//-------------------------------------------------------------------------
	public RangeSampleSelector (@SuppressWarnings("unused") final int nPercentage)
	{
	    super();
	}

	//-------------------------------------------------------------------------
	public RangeSampleSelector (final int nMin       ,
								final int nMax       ,
								final int nPercentage)
	{
		m_fMin        = nMin       ;
		m_fMax        = nMax       ;
		m_fPercentage = nPercentage;

		impl_calculate ();
	}

	//-------------------------------------------------------------------------
	public boolean isSample (final int nStep)
	{
		double fCurrent = nStep;
		double fDiff    = fCurrent - m_fLast;

//		System.out.println ("... current   = "+fCurrent    );
//		System.out.println ("... frequency = "+m_fFrequency);
//		System.out.println ("... last      = "+m_fLast     );
//		System.out.println ("... diff      = "+fDiff       );

		if (fDiff >= m_fFrequency)
		{
//			System.out.println ("... => HIT !");
			m_fLast = fCurrent;
			return true;
		}

		return false;
	}

	//-------------------------------------------------------------------------
	private void impl_calculate ()
	{
		double fRange             =   m_fMax   - m_fMin;
		double fSamples           = ((fRange * m_fPercentage) / 100.0d);
		double fSamplesNormed2Int = Math.round(fSamples);
		double fFrequency         = 0.0d;

		if (fSamplesNormed2Int > 0.0d)
			fFrequency = Math.round(fRange / fSamplesNormed2Int);
		else
			fFrequency = fRange + 1.0d; // ensure frequency is bigger then range in case NO sample has to be calculated .-)

		m_fRange     = fRange;
		m_fSamples   = fSamplesNormed2Int;
		m_fFrequency = fFrequency;
		m_fLast      = m_fMin;

//		System.out.println ("... min         = "+m_fMin            );
//		System.out.println ("... max         = "+m_fMax            );
//		System.out.println ("... range       = "+m_fRange          );
//		System.out.println ("... samples     = "+fSamples          );
//		System.out.println ("... samples int = "+fSamplesNormed2Int);
//		System.out.println ("... frequency   = "+m_fFrequency      );
	}

	//-------------------------------------------------------------------------
	private double m_fMin = 0.0d;

	//-------------------------------------------------------------------------
	private double m_fMax = 100.0d;

	//-------------------------------------------------------------------------
	@SuppressWarnings("unused")
    private double m_fRange = m_fMax - m_fMin;

	//-------------------------------------------------------------------------
	private double m_fPercentage = 0;

	//-------------------------------------------------------------------------
	@SuppressWarnings("unused")
    private double m_fSamples = 0.0d;

	//-------------------------------------------------------------------------
	private double m_fFrequency = 0.0d;

	//-------------------------------------------------------------------------
	private double m_fLast = 0.0d;
}