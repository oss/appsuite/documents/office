/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.csv;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

public class CSVEntryParser
{

    public static List<String> getCSVContentFromLine(
        String input,
        String[] seps)
            throws Exception
    {
        List<Object> toParse = new ArrayList<Object>();
        toParse.add(input);

        parseQuote(toParse, "\"");
        parseQuote(toParse, "'");
        for (String sep : seps)
        {
            parseSplit(toParse, sep);
        }

        List<String> res = new ArrayList<String>();
        for (Object o : toParse)
        {
            if (o instanceof SoftReference)
            {
                res.add(((SoftReference<?>) o).get().toString());
            }
            else
            {
                res.add((String) o);
            }
        }

        return res;
    }

    private static void parseSplit(
        List<Object> toParse,
        String split)
    {
        List<Object> tmpList = new ArrayList<Object>();

        for (int i = 0; i < toParse.size(); i++)
        {
            Object o = toParse.get(i);
            if (o instanceof SoftReference)
            {
                tmpList.add(o);
            }
            else
            {
                String[] splited = ((String) o).split(split);
                for (String s : splited)
                {
                    tmpList.add(s);
                }

            }
        }

        toParse.clear();
        toParse.addAll(tmpList);
    }

    private static void parseQuote(
        List<Object> toParse,
        String quote)
            throws Exception
    {
        List<Object> tmpList = new ArrayList<Object>();

        for (int i = 0; i < toParse.size(); i++)
        {
            Object o = toParse.get(i);
            if (o instanceof SoftReference)
            {
                tmpList.add(o);
            }
            else
            {
                String s = (String) o;
                int start = s.indexOf(quote);
                if (start >= 0)
                {
                    int end = s.indexOf(quote, start + 1);
                    if (end >= 0)
                    {
                        String before = s.substring(0, start);
                        String content = s.substring(start + 1, end);
                        String after = s.substring(end + 1);

                        if (before.length() > 0)
                        {
                            tmpList.add(before);
                        }
                        tmpList.add(new SoftReference<String>(content));

                        if (after.length() > 0)
                        {
                            toParse.add(i + 1, after);
                        }
                    }
                    else
                    {
                        throw new Exception("no end quote found (" + quote + ") in String: " + s);
                    }
                }
                else
                {
                    tmpList.add(o);
                }
            }
        }
        toParse.clear();
        toParse.addAll(tmpList);

    }

}
