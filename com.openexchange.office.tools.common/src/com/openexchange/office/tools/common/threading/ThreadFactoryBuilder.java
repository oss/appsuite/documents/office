/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.threading;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Locale;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.Validate;

public class ThreadFactoryBuilder {
	
	  private String nameFormat = null;
	  private Boolean daemon = null;
	  private UncaughtExceptionHandler uncaughtExceptionHandler = null;
	  private ThreadGroup threadGroup = null;

	  public ThreadFactoryBuilder() {}

	  public ThreadFactoryBuilder(String nameFormat) {
		  format(nameFormat, 0); // fail fast if the format is bad or null
		  this.nameFormat = nameFormat;		    
	  }
	  
	  public ThreadFactoryBuilder setNameFormat(String nameFormat) {
	    format(nameFormat, 0); // fail fast if the format is bad or null
	    this.nameFormat = nameFormat;
	    return this;
	  }

	  public ThreadFactoryBuilder setDaemon(boolean daemon) {
	    this.daemon = daemon;
	    return this;
	  }

	  public ThreadFactoryBuilder setThreadGroup(ThreadGroup threadGroup) {
		  this.threadGroup = Validate.notNull(threadGroup);
		  return this;
	  }	  
	  
	  public ThreadFactoryBuilder setUncaughtExceptionHandler(UncaughtExceptionHandler uncaughtExceptionHandler) {
	    this.uncaughtExceptionHandler = Validate.notNull(uncaughtExceptionHandler);
	    return this;
	  }

	  public ThreadFactory build() {
	    return doBuild(this);
	  }

	  // Split out so that the anonymous ThreadFactory can't contain a reference back to the builder.
	  // At least, I assume that's why. TODO(cpovirk): Check, and maybe add a test for this.
	  private static ThreadFactory doBuild(ThreadFactoryBuilder builder) {
	    final String nameFormat = builder.nameFormat;
	    final Boolean daemon = builder.daemon;
	    final UncaughtExceptionHandler uncaughtExceptionHandler = builder.uncaughtExceptionHandler;
	    final AtomicLong count = (nameFormat != null) ? new AtomicLong(0) : null;
	    final ThreadGroup threadGroup = builder.threadGroup;
	    return new ThreadFactory() {
	      @Override
	      public Thread newThread(Runnable runnable) { 
	        Thread thread;
	        if (threadGroup == null) {
	        	thread = new Thread(runnable);
	        } else {
	        	thread = new Thread(threadGroup, runnable);
	        }
	        if (nameFormat != null) {
	          thread.setName(format(nameFormat, count.getAndIncrement()));
	        }
	        if (daemon != null) {
	          thread.setDaemon(daemon);
	        }
	        if (uncaughtExceptionHandler != null) {
	          thread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
	        }
	        return thread;
	      }
	    };
	  }

	  private static String format(String format, Object... args) {
	    return String.format(Locale.ROOT, format, args);
	  }
}
