/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

/**
 * Provides keys for the status update properties.
 *
 * @author Carsten Driesner
 * @since 7.6.2
 *
 */
public class StatusProperties {
    public static final String KEY_EDIT_USER = "editUser";
    public static final String KEY_EDIT_USERID = "editUserId";
    public static final String KEY_HAS_ERRORS = "hasErrors";
    public static final String KEY_WRITE_PROTECTED = "writeProtected";
    public static final String KEY_LOCKED = "locked";
    public static final String KEY_LOCKED_BY_USER = "lockedByUser";
    public static final String KEY_LOCKED_BY_ID = "lockedByUserId";
    public static final String KEY_FAILSAFESAVEDONE = "failSafeSaveDone";
    public static final String KEY_WANTSEDIT_USER = "wantsToEditUser";
    public static final String KEY_WANTSEDIT_USERID = "wantsToEditUserId";
    public static final String KEY_SERVER_OSN = "serverOSN";
    public static final String KEY_ACTIVE_USERS = "activeUsers";
    public static final String KEY_LASTMODIFYING_USERID = "lastModifyingUserId";
    public static final String KEY_FINALLOADUPDATE = "finalLoadUpdate";
    public static final String KEY_SYNCLOAD = "syncLoad";
    public static final String KEY_ERRORCODE = "error";
    public static final String KEY_ACKS = "acks";
    public static final String KEY_RESCUEDOC = "rescueDoc";
}
