/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.log;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.event.Level;

public class LogMethodCallHelper {
		
	public static void logMethodCall(Logger logger, Object instance, String methodName, Object...params) {
		logMethodCall(logger, instance.getClass(), methodName, Level.DEBUG, params);
	}

	public static void logMethodCall(Logger logger, Class<?> clazz, String methodName, Level logLevel, Object...params) {
		StringBuilder strBuilderParams = buildParamLogString(params);
		log(logger, clazz, logLevel, methodName, strBuilderParams.toString(), "");
	}
	
	public static <T> T logMethodCallRes(Logger logger, Class<?> clazz, String methodName, T res, Object...params) {
		return logMethodCallRes(logger, clazz, methodName, Level.DEBUG, res, params);
	}

	public static <T> T logMethodCallRes(Logger logger, Class<?> clazz, String methodName, Level logLevel, T res, Object...params) {
		StringBuilder strBuilderParams = buildParamLogString(params);
		log(logger, clazz, logLevel, methodName, strBuilderParams.toString(), "");
		return res;
	}

	public static void logMethodCall(Loggable loggable, String methodName, Object...params) {
		logMethodCall(loggable, methodName, Level.DEBUG, params);
	}

	public static void logMethodCall(Loggable loggable, String methodName, Level logLevel, Object...params) {
		StringBuilder strBuilderParams = buildParamLogString(params);
		Class<?> clazz = loggable.getClass();
		log(loggable.getLogger(), clazz, logLevel, methodName, strBuilderParams.toString(), formatAdditionalInfoMap(loggable));
	}
	
	public static <T> T logMethodCallRes(Loggable loggable, String methodName, T res, Object...params) {
		return logMethodCallRes(loggable, methodName, Level.DEBUG, res, params);
	}

	public static <T> T logMethodCallRes(Loggable loggable, String methodName, Level logLevel, T res, Object...params) {
		StringBuilder strBuilderParams = buildParamLogString(params);
		log(loggable.getLogger(), loggable.getClass(), logLevel, methodName, strBuilderParams.toString(), formatAdditionalInfoMap(loggable));
		return res;
	}

	private static String formatAdditionalInfoMap(Loggable loggable) {
		Map<String, Object> addInfoMap = loggable.getAdditionalLogInfo();
		StringBuilder strBuilderAddInfo = new StringBuilder();
		boolean first = true;
		boolean found = false;
		for (Map.Entry<String, Object> entry : addInfoMap.entrySet()) {
			if (entry.getValue() != null) {
				if (!found) {
					strBuilderAddInfo.append(", additional info: ");
				}
				found = true;
				if (!first) {
					strBuilderAddInfo.append(", ");
				} else {
					first = false;
				}
				strBuilderAddInfo.append(entry.getKey());
				strBuilderAddInfo.append(": ");
				strBuilderAddInfo.append(entry.getValue());
			}
		}
		return strBuilderAddInfo.toString();
		
	}

	private static void log(Logger logger, Class<?> clazz, Level logLevel, String methodName, String methodParams, String addInfo) {
		switch (logLevel) {
			case DEBUG: logger.debug("Called {}::{} with parameters {}{}", clazz.getName(), methodName, methodParams, addInfo);
								  break;
			case ERROR: logger.error("Called {}::{} with parameters {}{}", clazz.getName(), methodName, methodParams, addInfo);
			  					  break;			
			case INFO:  logger.info("Called {}::{} with parameters {}{}", clazz.getName(), methodName, methodParams, addInfo);
								  break;
			case TRACE: logger.trace("Called {}::{} with parameters {}{}", clazz.getName(), methodName, methodParams, addInfo);
								  break;
			case WARN:  logger.warn("Called {}::{} with parameters {}{}", clazz.getName(), methodName, methodParams, addInfo);
		}
	}

	private static StringBuilder buildParamLogString(Object... params) {
		StringBuilder strBuilderParams = new StringBuilder();
		boolean first = true;
		for (Object param : params) {
			if (!first) {
				strBuilderParams.append(", ");
			} else {
				first = false;
			}
			strBuilderParams.append((param != null) ? param.getClass().getName() : "null");
			strBuilderParams.append(", ");
			strBuilderParams.append(param);
		}
		return strBuilderParams;
	}	
	
	private LogMethodCallHelper() {}
}
