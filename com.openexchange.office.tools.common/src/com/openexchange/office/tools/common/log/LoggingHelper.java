/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.log;

import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.event.Level;

public class LoggingHelper {
    private LoggingHelper() {
        // nothing to do - static methods only
    }

    /**
     * Output a log message in the context of the provided logger and the provided log level.
     *
     * @param logger the logger to be used for the log message. Must not be null.
     * @param level the log level to be used
     * @param message the log message to be output
     */
    public static void log(@NotNull Logger logger, Level level, String message) {
        switch (level) {
            case ERROR: logger.error(message); break;
            case WARN: logger.warn(message); break;
            case INFO: logger.info(message); break;
            case DEBUG: logger.debug(message); break;
            case TRACE: logger.trace(message); break;
        }
    }
}
