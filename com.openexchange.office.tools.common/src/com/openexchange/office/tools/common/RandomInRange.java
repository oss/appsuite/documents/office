/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.util.Random;

//=============================================================================
/** random generator able to work in a specified range
 *  <p>
 *  By default the range is set to it maximum : Integer.MIN_VALUE...Integer.MAX_VALUE.
 *  You can define a new range. All generated random values will be inside this range
 *  (including MIN and MAX)
 *  </p>
 */
public class RandomInRange
{
	//-------------------------------------------------------------------------
	public RandomInRange ()
	{}

	//-------------------------------------------------------------------------
	public RandomInRange (final int nMin,
						  final int nMax)
	{
		setMinRange (nMin);
		setMaxRange (nMax);
	}

	//-------------------------------------------------------------------------
	public void setMinRange (final int nMin)
	{
		m_nMin = nMin;
	}
	
	//-------------------------------------------------------------------------
	public void setMaxRange (final int nMax)
	{
		m_nMax = nMax;
	}

	//-------------------------------------------------------------------------
	public int next ()
	{
		final Random aGenerator = mem_Generator ();
		final int    nRange     = (m_nMax - m_nMin) + 1;
		final int    nRandom    = aGenerator.nextInt(nRange) + m_nMin;
		return nRandom;
	}

	//-------------------------------------------------------------------------
	private Random mem_Generator ()
	{
		if (m_aGenerator == null)
			m_aGenerator = new Random ();
		return m_aGenerator;
	}
	
	//-------------------------------------------------------------------------
	private int m_nMin = Integer.MIN_VALUE;

	//-------------------------------------------------------------------------
	private int m_nMax = Integer.MAX_VALUE;

	//-------------------------------------------------------------------------
	private Random m_aGenerator = null;
}
