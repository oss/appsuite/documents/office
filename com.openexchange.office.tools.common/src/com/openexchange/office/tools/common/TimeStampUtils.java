/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

public class TimeStampUtils {
	public static final long NO_TIMESTAMP = -1;

	private TimeStampUtils() {
		// nothing to do
	}

	public static long timeDiff(long start, long end) {
		if ((start == NO_TIMESTAMP) || (end == NO_TIMESTAMP))
			return 0;

		return (end > start) ? end - start : start - end;
	}

	public static long nanoToMillis(long time) {
	    return (time / 1000000);
	}

	public static boolean inBetween(long start, long end, long timeStamp) {
	    return ((start <= timeStamp) && (timeStamp <= end));
	}
}
