/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.json;

import java.io.IOException;


/**
 * JSONDebugWriter implements java.io.Writer to limit the log output of received operations.
 *
 * @author Carsten Driesner
 */
public class JSONDebugWriter extends java.io.Writer {

    private final StringBuffer m_output;

    private static final int MAX_LENGTH = 1024;

    private int lastWrittenLength = -1;

    public JSONDebugWriter() {
        super();

        m_output = new StringBuffer(512);
    }

    public String getData() {
        return m_output.toString();
    }

    @Override
    public void close() throws IOException {
        // nothing
    }

    @Override
    public void flush() throws IOException {
        // nothing
    }

    /**
     * Writes the content of the provided buffer using a predefined limit where the buffer content is cut of.
     *
     * @param cbuf A character buffer to be written.
     * @param off A offset within the buffer.
     * @param len The number of character to output beginning from off.
     */
    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        int correctLen = len;

        // Limit the amount of data written for an operation.
        // We assume that image data is written in chunks which exceeds
        // our own MAX_LENGTH
        if (lastWrittenLength < MAX_LENGTH) {
            m_output.append(cbuf, off, correctLen = Math.min(len, MAX_LENGTH));
        } else if (len < MAX_LENGTH) {
            m_output.append(cbuf, off, correctLen);
        }

        lastWrittenLength = correctLen;
    }
}
