/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.json;

import java.util.Iterator;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONValue;

/**
 * A set of functions to support the work with JSONObjects and JSONArrays.
 *
 * @author Carsten Driesner
 */
public class JSONHelper {

    /**
     * Checks if a JSONArray reference is null or empty.
     *
     * @param array a JSONArray reference
     * @return TRUE if the reference is null or the JSONArray is empty
     */
    static public boolean isNullOrEmpty(final JSONArray array) {
        return ((null == array) || (array.isEmpty()));
    }

    /**
     * Checks if a JSONArray reference is not null and not empty.
     *
     * @param array a JSONArray reference
     * @return TRUE if the reference is not null and the JSONArray is not empty
     */
    static public boolean isNotNullAndEmpty(final JSONArray array) {
        return ((null != array) && (!array.isEmpty()));
    }

    /**
     * Checks if a JSONObject reference is null or empty.
     *
     * @param array a JSONObject reference
     * @return TRUE if the reference is null or the JSONObject contains no properties.
     */
    static public boolean isNullOrEmpty(final JSONObject jsonObj) {
        return ((null == jsonObj) || (jsonObj.isEmpty()));
    }

    /**
     * Checks if a JSONObject reference is not null and contains properties.
     *
     * @param array a JSONObject reference
     * @return TRUE if the reference is not null and the JSONObject contains properties.
     */
    static public boolean isNotNullAndEmpty(final JSONObject jsonObj) {
        return ((null != jsonObj) && (!jsonObj.isEmpty()));
    }

    /**
     * Creates a shallow copy of the JSONArray, means that the function provides a new JSONArray with references to the entries within the
     * source array.
     *
     * @param array The source array which should be shallow cloned.
     * @return A new JSONArray containing the references to the old entries.
     * @throws Exception
     */
    static public final JSONArray shallowCopy(final JSONArray array) throws JSONException {
        JSONArray copy = null;

        if (null != array) {
            copy = new JSONArray();
            for (int i = 0; i < array.length(); i++) {
                copy.put(array.get(i));
            }
        }

        return copy;
    }

    // TODO: optimize this function to have a faster deep copy for JSONArrays
    static public final JSONArray deepCopy(final JSONArray array) throws JSONException {
        JSONArray copy = null;

        if (null != array) {
            copy = new JSONArray();
            for (int i = 0; i < array.length(); i++) {
                Object o = array.get(i);
                if (o instanceof JSONArray) {
                    o = deepCopy((JSONArray)o);
                } else if (o instanceof JSONObject) {
                    o = new JSONObject(o.toString());
                }
                copy.put(o);
            }
        }

        return copy;
    }

    /**
     * Appends a JSONArray to an existing JSONArray.
     *
     * @param target The target JSONArray.
     * @param source The source JSONArray.
     * @return The target array contains at the end all entries of the source if both arguments are set, otherwise null will be returned.
     * @throws Exception
     */
    static public final JSONArray appendArray(final JSONArray target, final JSONArray source) throws JSONException {
        if ((target != null) && (source != null)) {
            for (int i = 0; i < source.length(); i++) {
                target.put(source.get(i));
            }
        }

        return target;
    }

    /**
     * Appends a JSONArray starting at a specified index to an existing JSONArray.
     *
     * @param target The target JSONArray.
     * @param source The source JSONArray.
     * @return The target array contains at the end all entries of the source if both arguments are set, otherwise null will be returned.
     * @throws Exception
     */
    static public final JSONArray appendArray(final JSONArray target, final JSONArray source, int start) throws JSONException {
        if ((target != null) && (source != null)) {
            for (int i = 0; i < source.length(); i++) {
                if (i >= start) { target.put(source.get(i)); }
            }
        }

        return target;
    }

    /**
     * Removing an item from a JSONArray at a specified position. This is necessary, because the 'remove()' function is not always available
     * to JSONArrays. This function creates a local JSONArray, that is filled with the values of the JSONArray, that is a parameter of it. A
     * second parameter is required to specify, which element shall be removed. Its value must be between 0 and the length of the JSONArray
     * - 1. The locally created JSONArray is returned from this function.
     *
     * @param arr The array, from which one element shall be removed
     * @param pos The position of the item, that shall be removed from the array. This value must be between 0 and arr.length - 1
     * @return If a problem occurred, the JSONArray will not be modified and returned. Otherwise a reduced version of the JSONArray is
     *         returned, that does not contain the specified item.
     */
    static public final JSONArray removeItemFromJSONArray(final JSONArray arr, int pos) {

        if ((arr == null) || (arr.length() - 1 < pos)) {
            return arr;
        }

        final JSONArray newArr = new JSONArray();

        for (int i = 0; i < arr.length(); i++) {

            if (i != pos) {
                try {
                    newArr.put(arr.get(i));
                } catch (JSONException e) {
                    return arr;
                }
            }
        }

        return newArr;
    }

    /**
     * Creates a new array which contains the entries from the source beginning with the index startIndex until the end of source.
     *
     * @param source The source JSONArray where entries should be
     * @param startIndex
     * @return
     * @throws Exception
     */
    static public final JSONArray subArray(final JSONArray source, int startIndex) throws JSONException {
        JSONArray subArray = null;

        if (null != source) {
        	startIndex = Math.max(startIndex, 0);
            int endIndex = source.length();

            subArray = new JSONArray();
            for (int i = startIndex; i < endIndex; i++) {
                subArray.put(source.get(i));
            }
        }

        return subArray;
    }

    /**
     * Creates a new array which contains a subset of entries specified by startIndex and endIndex.
     *
     * @param source
     * @param startIndex index to start from
     * @param endIndex index to end, without including the value at that index
     * @return an JSONArray filled with entries between startIndex (inclusive) and endIndex (exclusive) or
     *         null if source is null
     * @throws JSONException
     */
    static public final JSONArray subArray(final JSONArray source, int startIndex, int endIndex) throws JSONException {
        JSONArray subArray = null;

        if (null != source) {
        	startIndex = Math.max(startIndex, 0);
            if (endIndex == -1) {
                endIndex = source.length();
            }

            subArray = new JSONArray();
            for (int i = startIndex; i < endIndex; i++) {
                subArray.put(source.get(i));
            }
        }

        return subArray;
    }

    /**
     * Removes entries from the provided JSONArray using startIndex and length to determine
     * what entries should be removed.
     *
     * @param source the JSONArray where entries should be removed
     * @param startIndex the index to start removing entries
     * @param length the number of entries to be removed
     * @throws JSONException
     */
    static public void remove(final JSONArray source, int startIndex, int length) throws JSONException {
        JSONArray cleanedArray = source;

        if (JSONHelper.isNotNullAndEmpty(source)) {
        	// It's clearly faster (up to  10x for thousands of entries) to create a new temporary
        	// array and copy all non removed entries into it, clear the source and add all
        	// entries from the temporary array, instead of using the remove() method of the
        	// JSONArray for every entry to be removed. Even calling remove() for just one entry
        	// is ~20% faster, therefore this is a good general purpose algorithm.
        	cleanedArray = new JSONArray();

        	final int sourceLength = source.length();
        	final int index2 = Math.min(startIndex + length, sourceLength);

        	// front part
            for (int i = 0; i < startIndex; i++) {
            	cleanedArray.put(source.get(i));
            }

            // back part
            for (int i = index2; i < sourceLength; i++) {
            	cleanedArray.put(source.get(i));
            }

            source.reset();
            for (int i = 0; i < cleanedArray.length(); i++) {
            	source.put(cleanedArray.get(i));
            }
        }
    }

    /**
     * Creates a new array which contains a subset of entries specified by startIndex and length.
     *
     * @param source
     * @param startIndex
     * @param length
     * @return
     */
    static public final JSONArray subArrayLength(final JSONArray source, int startIndex, int length) throws JSONException {
        JSONArray subArray = null;

        if (null != source) {
            subArray = new JSONArray();

            int endIndex = Math.min(Math.max(0, (startIndex + length)), Math.max(0, source.length()));
        	startIndex = Math.max(startIndex, 0);

            try {
                for (int i = startIndex; i < endIndex; i++) {
                    subArray.put(source.get(i));
                }
            } catch (JSONException e) {
                // nothing to do
            }
        }

        return subArray;
     }

    /**
     * Check, whether the two passed numeric JSONArrays are identical. It is checked,
     * if they are identical in length and for all elements.
     *
     * @param {number[]} array1
     *  The first array that will be compared to the second array.
     *
     * @param {number[]} array2
     *  The second array that will be compared to the first array.
     *
     * @returns {Boolean}
     *  Whether the two passed numeric arrays are identical.
     */

    static public final boolean isEqual(final JSONArray array1, final JSONArray array2) throws JSONException {
        return compareNumberArrays(array1, array2) == 0;
    }

    /**
     * Check, whether the two passed numeric JSONArrays are identical. It is checked,
     * if they are identical for a specified number of leading elements.
     *
     * @param {number[]} array1
     *  The first array that will be compared to the second array.
     *
     * @param {number[]} array2
     *  The second array that will be compared to the first array.
     *
     * @param {number} maxLength
     *  The leading number of elements that will be compared.
     *
     * @returns {Boolean}
     *  Whether the two passed numeric arrays are identical.
     */

    static public final boolean isEqual(final JSONArray array1, final JSONArray array2, int maxLength) throws JSONException {
        return compareNumberArrays(array1, array2, maxLength) == 0;
    }

    /**
     * Compares the two passed numeric JSONArrays lexicographically (starting with the
     * first element of both arrays, and visiting the following elements until a
     * pair of elements is different). The comparison is limited to the specified
     * number of elements.
     *
     * @param array1
     *  The first array that will be compared to the second array.
     *
     * @param array2
     *  The second array that will be compared to the first array.
     *
     * @param maxLength
     *  Comparing the specified number of leading elements in the
     *  passed arrays.
     *
     * @returns
     *  A negative number, if array1 is lexicographically less than array2; a
     *  positive number, if array1 is lexicographically greater than array2; or
     *  zero, if both arrays are equal.
     */
    static public final int compareNumberArrays(final JSONArray array1, final JSONArray array2, int maxLength) throws JSONException {
        for (int i = 0; i < maxLength; i += 1) {
            final int diff = array1.getInt(i) - array2.getInt(i);
            if (diff != 0) { return diff; }
        }
        return 0;
    }


    /**
     * Compares the two passed numeric JSONArrays lexicographically (starting with the
     * first element of both arrays, and visiting the following elements until a
     * pair of elements is different). The entire arrays will be compared.
     *
     * @param array1
     *  The first array that will be compared to the second array.
     *
     * @param array2
     *  The second array that will be compared to the first array.
     *
     * @returns
     *  A negative number, if array1 is lexicographically less than array2; a
     *  positive number, if array1 is lexicographically greater than array2; or
     *  zero, if both arrays are equal.
     */
    static public final int compareNumberArrays(final JSONArray array1, final JSONArray array2) throws JSONException {
        int maxLength;
        final int length1 = array1.length();
        final int length2 = array2.length();
        if (length1 <= length2) {
            maxLength = length1;
        } else {
            maxLength = length2;
        }
        for (int i = 0; i < maxLength; i += 1) {
            final int diff = array1.getInt(i) - array2.getInt(i);
            if (diff != 0) { return diff; }
        }
        return length1 - length2; // all compared elements are equal: compare array lengths
    }

    /**
     * Check, whether the specified logical position posA describes an ancestor of the logical position posB.
     *
     * @param {JSONArray} posA
     *  A logical position.
     *
     * @param {JSONArray} posB
     *  A logical position.
     * @throws JSONException
     *
     * @returns {Boolean}
     *  Whether the specified logical position posA describes an ancestor of the logical position posB.
     */
    static public final boolean isAncestorPos(final JSONArray posA, final JSONArray posB) throws JSONException {
        return (posA.length() < posB.length() && isEqual(posA, posB, posA.length()));
    }

    /**
     * Check, whether the logical start position of specified reference operation is removed by the start or end
     * position of a specified delete operation.
     * Example:
     * 'delete start: [2]' removes an 'insertText start: [2,6]'
     * 'delete start: [1] end: [2]' removes an 'insertText start: [2,6]'
     * The second example is important, because the insertText position is 'behind' the end position, if the
     * function 'compareNumberArrays' is used.
     *
     * @param {JSONObject} deleteOp
     *  A delete operation. It must contain a start position and can contain an end position.
     *
     * @param {JSONObject} startOp
     *  A reference operation. It must contain a start position.
     *
     * @returns {Boolean}
     *  Whether the start position of the reference operation is removed by the specified start and end positions
     *  of the delete operation.
     */
    static public final boolean ancestorRemoved(final JSONObject deleteOp, final JSONObject startOp) throws JSONException {

        String startKey = "o";
        String endKey = "q";
        boolean minified = deleteOp.optJSONArray("o") != null;

        if (!minified) {
            startKey = "start";
            endKey = "end";
        }

        final JSONArray startStartOp = startOp.getJSONArray(startKey);
        final JSONArray startDeleteOp = deleteOp.getJSONArray(startKey);
        final JSONArray endDeleteOp = deleteOp.optJSONArray(endKey);

        return isAncestorPos(startDeleteOp, startStartOp) || (endDeleteOp != null && isAncestorPos(endDeleteOp, startStartOp));
    }

    /**
     * Check, whether the specified operations have completely separated ranges, without an overlap. Both
     * operations must have a property named 'start'. Optionally a property named 'end' is evaluated.
     *
     * This function also handles problematic case like: delete [3][4] and delete [4,1][4,3] (ancestors)
     *
     * @param {JSONObject} op1
     *  An operation with a 'start' property and optionally an 'end' property.
     *
     * @param {JSONObject} op2
     *  An operation with a 'start' property and optionally an 'end' property.
     * @throws JSONException
     *
     * @returns {Boolean}
     *  Whether the specified operations have completely separated ranges, without overlap.
     */
    static public final boolean separateRanges(final JSONObject op1, final JSONObject op2) throws JSONException {

        String startKey = "o";
        String endKey = "q";
        boolean isSeparated = false;
        boolean minified = op1.optJSONArray("o") != null;

        if (!minified) {
            startKey = "start";
            endKey = "end";
        }

        final JSONArray startOp1 = op1.getJSONArray(startKey);
        final JSONArray startOp2 = op2.getJSONArray(startKey);
        JSONArray endOp1 = op1.optJSONArray(endKey);
        JSONArray endOp2 = op2.optJSONArray(endKey);

        if (endOp1 == null) { endOp1 = startOp1; }
        if (endOp2 == null) { endOp2 = startOp2; }

        if ((compareNumberArrays(endOp1, startOp2) < 0 && !isAncestorPos(endOp1, startOp2)) || (compareNumberArrays(endOp2, startOp1) < 0 && !isAncestorPos(endOp2, startOp1))) {
            isSeparated = true;
        }

        return isSeparated;
    }

    /**
     * Check, whether the specified operations have completely identical ranges. Both operations must
     * have a property named 'start'. Optionally a property named 'end' is evaluated.
     *
     * @param {JSONObject} op1
     *  An operation with a 'start' property and optionally an 'end' property.
     *
     * @param {JSONObject} op2
     *  An operation with a 'start' property and optionally an 'end' property.
     * @throws JSONException
     *
     * @returns {Boolean}
     *  Whether the specified operations have completely identical ranges, without overlap.
     */
    static public final boolean identicalRanges(final JSONObject op1, final JSONObject op2) throws JSONException {

        String startKey = "o";
        String endKey = "q";
        boolean isIdentical = false;
        boolean minified = op1.optJSONArray("o") != null;

        if (!minified) {
            startKey = "start";
            endKey = "end";
        }

        final JSONArray startOp1 = op1.getJSONArray(startKey);
        final JSONArray startOp2 = op2.getJSONArray(startKey);
        JSONArray endOp1 = op1.optJSONArray(endKey);
        JSONArray endOp2 = op2.optJSONArray(endKey);

        boolean compareEnd = (endOp1 != null || endOp2 != null); // no check of 'end' required, if both end positions are not defined

        if (compareEnd) {
            if (endOp1 == null) { endOp1 = startOp1; }
            if (endOp2 == null) { endOp2 = startOp2; }
        }

        if (compareNumberArrays(startOp1, startOp2) == 0 && (!compareEnd || compareNumberArrays(endOp1, endOp2) == 0)) {
            isIdentical = true;
        }

        return isIdentical;
    }

    // helper function to compare two operations in specific order
    // -> this shall only be used by function "surroundingRanges"
    static public final boolean doSurround(final JSONObject opA, final JSONObject opB) throws JSONException {

        String startKey = "o";
        String endKey = "q";
        boolean surrounds = false;
        boolean minified = opA.optJSONArray("o") != null;

        if (!minified) {
            startKey = "start";
            endKey = "end";
        }

        final JSONArray startOpA = opA.getJSONArray(startKey);
        final JSONArray startOpB = opB.getJSONArray(startKey);
        JSONArray endOpA = opA.optJSONArray(endKey);
        JSONArray endOpB = opB.optJSONArray(endKey);

        if (endOpA == null) { endOpA = startOpA; }
        if (endOpB == null) { endOpB = startOpB; }

        if (compareNumberArrays(startOpA, startOpB) <= 0 && (compareNumberArrays(endOpA, endOpB) >= 0 || isAncestorPos(endOpA, endOpB))) {
            surrounds = true;
        }

        return surrounds;
    }

    /**
     * Check, whether the specified operations have surrounding ranges. This means that the range of
     * one operation is completely inside the range of the other operation. This is also the case, if
     * both ranges are identical.
     *
     * This function also handles problematic case like: delete [4] that surrounds delete [4,1][4,3] (ancestors)
     *
     * @param {JSONObject} op1
     *  An operation with a 'start' property and optionally an 'end' property.
     *
     * @param {JSONObject} op2
     *  An operation with a 'start' property and optionally an 'end' property.
     * @throws JSONException
     *
     * @returns {Number}
     *  Returns 1, if the range of op1 surrounds the range of op2 (or if both are equal).
     *  Returns -1, if the range of op2 surrounds the range of op1.
     *  Returns 0, if no range surrounds the other range.
     */
    static public final int surroundingRanges(final JSONObject op1, final JSONObject op2) throws JSONException {

        int isSurrounding = 0;

        if (doSurround(op1, op2)) {
            isSurrounding = 1;
        } else if (doSurround(op2, op1)) {
            isSurrounding = -1;
        }

        return isSurrounding;
    }

    /**
     * Creates a clone of the passed logical position and increases the last
     * element of the array by 1. The passed array object will not be changed.
     *
     * @param {JSONArray} position
     *  The initial logical position.
     * @throws JSONException
     *
     * @returns {JSONArray}
     *  A clone of the passed logical position, with the last element increased.
     */
    static public final JSONArray increaseLastIndex(final JSONArray pos) throws JSONException {
        return increaseLastIndex(pos, 1);
    }

    /**
     * Creates a clone of the passed logical position and increases the last
     * element of the array by the specified value. The passed array object
     * will not be changed.
     *
     * @param {JSONArray} position
     *  The initial logical position.
     *
     * @param {int} increment
     *  The value that will be added to the last element of the position.
     * @throws JSONException
     *
     * @returns {JSONArray}
     *  A clone of the passed logical position, with the last element increased.
     */
    static public final JSONArray increaseLastIndex(final JSONArray pos, int increment) throws JSONException {
        JSONArray position = clonePosition(pos);
        int lastIndex = position.length() - 1;
        position.put(lastIndex, position.getInt(lastIndex) + increment);
        return position;
    }

    /**
     * Creates a clone of the passed logical position.
     *
     * @param {JSONArray} position
     *  The logical position to be cloned.
     *
     * @returns {JSONArray}
     *  A clone of the passed logical position.
     */
    static public final JSONArray clonePosition(JSONArray pos) throws JSONException {
        int max = pos.length();
        JSONArray clone = new JSONArray();
        for (int index = 0; index < max ; index++) { clone.put(index, pos.get(index)); }
        return clone;
    }

    /**
     * Creates a clone of the passed logical position limited to a specified number
     * of elements.
     *
     * @param {JSONArray} position
     *  The logical position to be cloned.
     *
     * @param {int} max
     *  The number of elements to be cloned.
     *
     * @returns {JSONArray}
     *  A clone of the passed logical position.
     */
    static public final JSONArray clonePosition(JSONArray pos, int max) throws JSONException {
        JSONArray clone = new JSONArray();
        for (int index = 0; index < max ; index++) { clone.put(index, pos.get(index)); }
        return clone;
    }

    /**
     * Calculating a reduced set of external attributes (the second parameter) for a specified set of local
     * attributes.
     *
     * Only the attributes in the second parameter are modified within this function.
     *
     * Reducing is required, if an attribute is set locally and externally and the values are different.
     * In this case the external attribute must not be set.
     *
     * In some cases the external operation can be removed completely.
     * In some cases some of the remaining attributes of the external operation must be applied.
     * In some cases the external operation must be splitted in 3 operations at maximum with different
     *    attributes for different ranges.
     *
     *
     * @param {JSONObject} localAttrs
     *  The set of local attributes. This object will not be modified within this function.
     *
     * @param {JSONObject} extAttrs
     *  The set of external attributes. This object will can be modified within this function.
     * @throws JSONException
     *
     * @returns {Boolean}
     *  Whether the external attributes were modified within this function.
     */

    static public final boolean reduceAttributeSet(final JSONObject localAttrs, final JSONObject extAttrs, boolean deleteEqual) throws JSONException {

        boolean reduced = false;

        final Iterator<Entry<String, Object>> localAttrsIter = localAttrs.entrySet().iterator();
        while(localAttrsIter.hasNext()) {
            final Entry<String, Object> localAttrsEntry = localAttrsIter.next();
            final Object localValue = localAttrsEntry.getValue();
            if(localValue instanceof JSONObject) {
                final String family = localAttrsEntry.getKey();
                if(extAttrs.has(family)) {
                    final Iterator<Entry<String, Object>> localFamilyAttributeIter = ((JSONObject)localValue).entrySet().iterator();
                    final JSONObject extFamilyAttributes = extAttrs.getJSONObject(family);
                    while(localFamilyAttributeIter.hasNext()) {
                        final Entry<String, Object> familyAttributeEntry = localFamilyAttributeIter.next();
                        final String attribute = familyAttributeEntry.getKey();
                        if(extFamilyAttributes.has(attribute)) {
                            final boolean equalValue = familyAttributeEntry.getValue().toString().equals(extFamilyAttributes.get(attribute).toString());
                            if(!equalValue) {
                                extFamilyAttributes.remove(attribute);
                                reduced = true;
                            }
                            else if(deleteEqual) {
                                extFamilyAttributes.remove(attribute);
                                localFamilyAttributeIter.remove();
                                reduced = true;
                            }
                        }
                    }
                    if(extFamilyAttributes.isEmpty()) {
                        extAttrs.remove(family);
                    }
                    if(deleteEqual && ((JSONObject)localValue).isEmpty()) {
                        localAttrsIter.remove();
                    }
                }
            }
        }
        return reduced;
    }

    /**
     * Calculating a 'delete' array, that can be used to modify a specified position 'pos'.
     * The logical position 'pos' has to be behind the specified start and end positions,
     * so that its values are reduced caused by the removal of the content between start
     * and end position.
     *
     * Examples:
     * start: [0,4], end: [0,9], pos: [0,20]  -> returned array: [0,6]
     * start: [0,4], end: [0,9], pos: [1,20]  -> returned array: [0,0]
     * start: [0,4], end: [1,9], pos: [1,20]  -> returned array: [0,9]
     * start: [0], end: [1,9], pos: [1,20]    -> returned array: [1,9]
     *
     * @param {JSONArray} start
     *  The logical start position (of the delete operation).
     *
     * @param {JSONArray} end
     *  The logical end position (of the delete operation).
     *
     * @param {JSONArray} pos
     *  The logical position for that the 'delete' array is calculated.
     *
     * @returns {JSONArray}
     *  And array with the length of the specified logical position 'pos', that can be
     *  used to calculate the modified position, after the delete operation is applied.
     */
    static public final JSONArray calculateDeleteArray(JSONArray start, JSONArray end, JSONArray pos) throws JSONException {

        int posLength = pos.length();
        int lastStartIndex = start.length() - 1;
        int lastEndIndex = end.length() - 1;
        boolean sameParent = true; // whether start and end position have same parent during iteration
        boolean relevant = true; // whether the diff is relevant for the specified position
        JSONArray diffArray = new JSONArray();

        for (int index = 0; index < posLength; index += 1) {

            int diff = 0;

            if (relevant) {
                if (sameParent) {
                    if (end.optInt(index, -1) > -1 && start.optInt(index, -1) > -1) {
                        if (end.getInt(index) > start.getInt(index)) {
                            diff = end.getInt(index) - start.getInt(index) - 1;
                            if (index == lastStartIndex) { diff++; }
                            if (index == lastEndIndex) { diff++; } // not if this is the same
                        } else if (index == lastStartIndex && index == lastEndIndex && end.getInt(index) == start.getInt(index)) {
                            diff = 1; // one single character, if both positions are identical
                        }
                    }
                    if (start.optInt(index, -1) == -1 || end.optInt(index, -1) == -1 || end.getInt(index) != start.getInt(index)) { sameParent = false; } // check if start and end position still have the same parent
                    if (end.optInt(index, -1) == -1 || pos.getInt(index) != end.getInt(index)) { relevant = false; } // check if end position and specified position still have the same parent

                } else {
                    diff = 0;
                    if (end.optInt(index, -1) > -1) { diff = end.getInt(index) + 1; } // example: [2,5] to [3,5] for position [3,7] (pos [3,5] means, that 6 characters are removed, therefore '+1')
                    if (end.optInt(index, -1) == -1 || pos.getInt(index) != end.getInt(index)) { relevant = false; } // check if end position and specified position still have the same parent
                }
            }
            diffArray.put(index, diff);
        }

        return diffArray;
    }

    /**
     * Helper function to generate a result object for an OT handler function, that
     * contains the new generated operations required for OT.
     *
     * @param {JSONArray|JSONObject} externalOpsBefore
     *  The new generated external operations that are included before the current
     *  external operation.
     *
     * @param {JSONArray|JSONObject} externalOpsAfter
     *  The new generated external operations that are included behind the current
     *  external operation.
     *
     * @param {JSONArray|JSONObject} localOpsBefore
     *  The new generated local operations that are included before the current
     *  local operation.
     *
     * @param {JSONArray|JSONObject} localOpsAfter
     *  The new generated local operations that are included behind the current
     *  local operation.
     *
     * @returns {JSONObject}
     *  The object with the new generated operations. Or null, if no new operation
     *  is generated.
     */
    static public final JSONObject getResultObject(JSONValue externalOpsBefore, JSONValue externalOpsAfter, JSONValue localOpsBefore, JSONValue localOpsAfter) throws JSONException {

        JSONObject result = null;
        if (externalOpsBefore != null || externalOpsAfter != null || localOpsBefore != null || localOpsAfter != null) {
            result = new JSONObject();
            putArray(result, "externalOpsBefore", externalOpsBefore);
            putArray(result, "externalOpsAfter", externalOpsAfter);
            putArray(result, "localOpsBefore", localOpsBefore);
            putArray(result, "localOpsAfter", localOpsAfter);
        }
        return result;
    }

    static private void putArray(JSONObject o, String s, Object ops) throws JSONException {
        if(ops instanceof JSONArray) {
            o.put(s,  ops);
        }
        else if (ops instanceof JSONObject) {
            final JSONArray a = new JSONArray(1);
            a.put(ops);
            o.put(s, a);
        }
    }

    // -------------------------------------------------------------------------

    /**
     * Creates a deep JSONArray clone, where every entry is also deeply cloned.
     *
     * @param source
     * @return
     * @throws JSONException
     */
    static public JSONArray cloneJSONArray(JSONArray source) throws JSONException {
        final JSONArray dest = new JSONArray(source.length());
        for(int i=0; i < source.length(); i++) {
            final Object s = source.get(i);
            Object d;
            if(s instanceof JSONObject) {
                d = cloneJSONObject((JSONObject)s);
            }
            else if(s instanceof JSONArray) {
                d = cloneJSONArray((JSONArray)s);
            }
            else {
                // should be java primitive now
                d = s;
            }
            dest.put(i, d);
        }
        return dest;
    }

    /**
     * Creates a deep JSONObject clone.
     *
     * @param source
     * @return
     * @throws JSONException
     */
    static public JSONObject cloneJSONObject(JSONObject source) throws JSONException {
        final JSONObject dest = new JSONObject(source.length());
        final Iterator<Entry<String, Object>> sourceIter = source.entrySet().iterator();
        while(sourceIter.hasNext()) {
            final Entry<String, Object> sourceEntry = sourceIter.next();
            final Object s = sourceEntry.getValue();
            Object d;
            if(s instanceof JSONObject) {
                d = cloneJSONObject((JSONObject)s);
            }
            else if(s instanceof JSONArray) {
                d = cloneJSONArray((JSONArray)s);
            }
            else {
                // should be java primitive now
                d = s;
            }
            dest.put(sourceEntry.getKey(), d);
        }
        return dest;
    }
}
