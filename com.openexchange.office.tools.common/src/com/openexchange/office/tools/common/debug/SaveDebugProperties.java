/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.debug;

import java.util.HashMap;


public class SaveDebugProperties {

    public static final String DEBUG_PROP_SLOWSAVE = "debugSlowSave";
    public static final String DEBUG_PROP_SLOWSAVETIME = "debugSlowSaveTime";
    public static final String DEBUG_PROP_SLOWSAVEUSER = "debugSlowSaveUser";
    public static final String DEBUG_PROP_SAVEDOCHISTORY = "debugSaveDocHistory";

    private final HashMap<String, Object> m_debugProperties = new HashMap<>();

    public SaveDebugProperties() {
        // nothing to do
    }

    public boolean slowSave(boolean defValue) {
        final Object value = m_debugProperties.get(DEBUG_PROP_SLOWSAVE);
        return (value instanceof Boolean) ? (Boolean)value : defValue;
    }

    public int slowSaveTime(int defValue) {
        final Object value = m_debugProperties.get(DEBUG_PROP_SLOWSAVETIME);
        return (value instanceof Integer) ? (Integer)value : defValue;
    }

    public boolean slowSaveUser(boolean defValue) {
        final Object value = m_debugProperties.get(DEBUG_PROP_SLOWSAVEUSER);
        return (value instanceof Boolean) ? (Boolean)value : defValue;
    }

    public boolean saveDocHistory(boolean defValue) {
        final Object value = m_debugProperties.get(DEBUG_PROP_SAVEDOCHISTORY);
        return (value instanceof Boolean) ? (Boolean)value : defValue;
    }

    public void setProperty(final String prop, Object value) {
        m_debugProperties.put(prop, value);
    }
}
