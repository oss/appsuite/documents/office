/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.Validate;

//=============================================================================
public class MapImpl implements IMap
                              , Serializable
{
	// NO serialVersionUID FOR BASE CLASS - PLEASE !

	//-------------------------------------------------------------------------
	public MapImpl ()
	    throws Exception
	{}

	//-------------------------------------------------------------------------
	public synchronized void clear ()
	    throws Exception
	{
		mem_Map ().clear();
	}

	//-------------------------------------------------------------------------
	@Override
	public synchronized < T extends Serializable > void set(final String sElement,
									 						final T      aValue  )
		throws Exception
	{
		Validate.notEmpty(sElement, "Invalid argument for 'element'.");

		Map< String, Object > lMap = mem_Map ();
		lMap.put(sElement, aValue);
	}

	//-------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	@Override
	public synchronized <T extends Serializable > T get(final String sElement)
		throws Exception
	{
		Validate.notEmpty(sElement, "Invalid argument for 'element'.");

		Map< String, Object > lMap   = mem_Map ();
		Object                aValue = lMap.get(sElement);
		return (T) aValue;
	}

	//-------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	@Override
	public synchronized < T extends Serializable > T get(final String sElement,
					 			  						 final T      aDefault)
		throws Exception
	{
		Validate.notEmpty(sElement, "Invalid argument for 'element'.");

		Map< String, Object > lMap   = mem_Map ();
		Object                aValue = lMap.get(sElement);

		if (aValue == null)
			aValue = aDefault;

		return (T) aValue;
	}

	//-------------------------------------------------------------------------
	@Override
	public synchronized String toString ()
	{
		StringBuffer sString = new StringBuffer (256);

		sString.append (super.toString ());
		if (m_lMap == null)
			return sString.toString ();

		sString.append (m_lMap);
		return sString.toString ();
	}

	//-------------------------------------------------------------------------
	protected Map< String, Object > mem_Map ()
	    throws Exception
	{
		if (m_lMap == null)
			m_lMap = new HashMap< String, Object > ();
		return m_lMap;
	}

	//-------------------------------------------------------------------------
	private Map< String, Object > m_lMap = null;
}
