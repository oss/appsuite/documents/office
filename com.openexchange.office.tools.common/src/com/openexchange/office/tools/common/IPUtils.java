/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.net.InetAddress;
import java.net.NetworkInterface;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;

import com.openexchange.exception.ExceptionUtils;

//=============================================================================
/**
 * Helper class to ease working with ip addresses.
 *
 * @author Andreas Schluens
 * @since 7.6.2
 */
public class IPUtils {
    //-------------------------------------------------------------------------
    public static final String DNS_ATTR_IPV4_RECORD_A = "A";
    public static final String DNS_ATTR_IPV6_RECORD_A = "AAAA";

	//-------------------------------------------------------------------------
	private IPUtils() {
	    // helper classes with static functions only do not need a ctor
	}

	//-------------------------------------------------------------------------
	/** check if the given IP match to current machine.
	 *
	 *  You can check for IPv4 and IPv6 addresses here ...
	 *  but not for DNS names !
	 *
	 *	@param	sIP	[IN]
	 *			the IP to be checked here.
	 *
	 * 	@return true if IP match; false otherwise.
	 */
    public static boolean isMyIP(String sIP) throws Exception {
    	if (StringUtils.isEmpty(sIP))
    		return false;

    	var lInterfaces = NetworkInterface.getNetworkInterfaces();
    	while (lInterfaces.hasMoreElements()) {
    	    final NetworkInterface aInterface = lInterfaces.nextElement();

    	    if (!aInterface.isUp()) // TODO think about me ...
    	    	continue;

    	    var lAddresses = aInterface.getInetAddresses();
    	    while (lAddresses.hasMoreElements()) {
    	        var aAddress = lAddresses.nextElement();
    	        var sAddress = aAddress.getHostAddress();

    	        if (StringUtils.equals(sAddress, sIP))
    	        	return true;
    	    }
    	}

    	return false;
    }

	//-------------------------------------------------------------------------
    /**
     * TODO might be better to use DNSJava JAR ? (http://www.dnsjava.org)
     */
    public static String detectIP(@NotNull final String sIPOrDNS) throws Exception {
    	var address = InetAddress.getByName(sIPOrDNS);
    	var dirContext = new InitialDirContext();
    	var lookupURL = "dns:/" + address.getHostName();
        var it = (NamingEnumeration< ? extends Attribute >)null;

    	try {
            var dnsAttributes = dirContext.getAttributes(lookupURL);
            var ipV4 = (String)null;
            var ipV6 = (String)null;

            it = dnsAttributes.getAll();

            while (it.hasMore()) {
                var attribute = (BasicAttribute)it.next();
                var attributeId = attribute.getID();
                var value = attribute.get(); // return default value ... which is enough in our case

                // We prefer IPV4 and if we find such IP we can break the loop
                if (StringUtils.equals(attributeId, DNS_ATTR_IPV4_RECORD_A)) {
                    ipV4 = (String)value;
                    break;
                }

                // Search for IPV6, too. But we cache those IP only.
                // It's used in case IPV4 is not detected only !
                if (StringUtils.equals(attributeId, DNS_ATTR_IPV6_RECORD_A)) {
                    ipV6 = (String)value; // ! NO BREAK !
                }
            }

            if (StringUtils.isNotEmpty(ipV4))
                return ipV4;

            if (StringUtils.isNotEmpty(ipV6))
                return ipV6;
    	} catch (Throwable ex) {
             // our error handling is return null
             ExceptionUtils.handleThrowable(ex);
        } finally {
            closeDirContext(dirContext);
            closeNamingEnumeration(it);
        }

		return null;
    }

    private static void closeDirContext(@NotNull DirContext dirContext) {
        try {
            dirContext.close();
        } catch (Exception e) {
            // nothing to do - we ignore it
        }
    }

    private static void closeNamingEnumeration(NamingEnumeration< ? extends Attribute > namingEnumeration) {
        try {
            if (namingEnumeration != null) {
                namingEnumeration.close();
            }
        } catch (Exception e) {
            // nothing to do - we ignore it
        }
    }
}
