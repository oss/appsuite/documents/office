/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.session;

import java.util.Set;
import com.openexchange.session.Origin;
import com.openexchange.session.Session;

public class FakeSession
        implements
        Session
{

    private final Integer userId;
    private final Session realSession;

    public FakeSession(
            int userId, Session realSession)
    {
        this.userId = userId;
        this.realSession = realSession;
    }

    @Override
    public int getUserId()
    {
        return userId;
    }

    @Override
    public int getContextId()
    {
        return realSession.getContextId();
    }

    @Override
    public String getLocalIp()
    {

        return realSession.getLocalIp();
    }

    @Deprecated
    @Override
    public void setLocalIp(
        String ip)
    {
        realSession.setLocalIp(ip);
    }

    @Override
    public String getLoginName()
    {

        return realSession.getLoginName();
    }

    @Override
    public boolean containsParameter(
        String name)
    {

        return realSession.containsParameter(name);
    }

    @Override
    public Object getParameter(
        String name)
    {

        return realSession.getParameter(name);
    }

    @Override
    public String getPassword()
    {

        return realSession.getPassword();
    }

    @Override
    public String getRandomToken()
    {

        return realSession.getRandomToken();
    }

    @Override
    public String getSecret()
    {

        return realSession.getSecret();
    }

    @Override
    public String getSessionID()
    {

        return realSession.getSessionID();
    }

    @Override
    public String getUserlogin()
    {

        return realSession.getUserlogin();
    }

    @Override
    public String getLogin()
    {

        return realSession.getLogin();
    }

    @Override
    public void setParameter(
        String name,
        Object value)
    {
        realSession.setParameter(name, value);
    }

    @Override
    public String getAuthId()
    {

        return realSession.getAuthId();
    }

    @Override
    public String getHash()
    {

        return realSession.getHash();
    }

    @Deprecated
    @Override
    public void setHash(
        String hash)
    {
        realSession.setHash(hash);
    }

    @Override
    public String getClient()
    {

        return realSession.getClient();
    }

    @Deprecated
    @Override
    public void setClient(
        String client)
    {
        realSession.setClient(client);
    }

    @Override
    public boolean isTransient()
    {

        return realSession.isTransient();
    }

    @Override
    public Set<String> getParameterNames()
    {
        return realSession.getParameterNames();
    }

    @Override
    public Origin getOrigin() {
        return realSession.getOrigin();
    }

	@Override
	public boolean isStaySignedIn() {
		return realSession.isStaySignedIn();
	}
}
