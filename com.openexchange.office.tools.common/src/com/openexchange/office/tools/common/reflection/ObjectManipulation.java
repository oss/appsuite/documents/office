/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

//==============================================================================
/** provide several functions where you can manipulate objects at runtime.
 */
public class ObjectManipulation
{
    //--------------------------------------------------------------------------
	/** set a new value on the specified object field ... even it's private
	 *  or final .-)
	 *
	 * 	@param	aTargetObject [IN]
	 * 			the target object where the field value must be set.
	 *
	 * 	@param	sFieldName [IN]
	 * 			the name of the field.
	 *
	 * 	@param	aValue [IN]
	 * 			the new value.
	 */
	public static < TType > void setFieldValue (Object aTargetObject,
					 		     	            String sFieldName   ,
					 		    	            TType  aValue       )
		throws Exception
	{
		Class< ? > aClass = aTargetObject.getClass ();
		Field      aField = aClass.getDeclaredField(sFieldName);
		impl_makeFieldAccessible (aField);
		aField.set(aTargetObject, aValue);
	}

    //--------------------------------------------------------------------------
	/** get the current value of the specified static field ... even it's private
	 *  or final .-)
	 *
	 * 	@param	aClass [IN]
	 * 			the class where the static field value must be retrieved from.
	 *
	 * 	@param	sFieldName [IN]
	 * 			the name of the field.
	 *
	 * 	@return	the current value of that field.
	 */
	@SuppressWarnings("unchecked")
	public static < TType > TType getStaticFieldValue (Class< ? > aClass    ,
					 		     	                   String     sFieldName)
		throws Exception
	{
		Field  aField = aClass.getDeclaredField(sFieldName);
		impl_makeFieldAccessible (aField);
		Object aValue = aField.get (null);
		return (TType) aValue;
	}

	//--------------------------------------------------------------------------
	/** set a new value on the specified static field ... even it's private
	 *  or final .-)
	 *
	 * 	@param	aClass [IN]
	 * 			the class where the static field value must be set.
	 *
	 * 	@param	sFieldName [IN]
	 * 			the name of the field.
	 *
	 * 	@param	aValue [IN]
	 * 			the new value.
	 */
	public static < TType > void setStaticFieldValue (Class< ? > aClass    ,
					 		     	                  String     sFieldName,
					 		    	                  TType      aValue    )
		throws Exception
	{
		Field aField = aClass.getDeclaredField(sFieldName);
		impl_makeFieldAccessible (aField);
		aField.set(null, aValue);
	}

	//--------------------------------------------------------------------------
	/** get the current value from specified field.
	 *
	 * 	@param	aSourceObject [IN]
	 * 			the source object where the field value must be read.
	 *
	 * 	@param	sFieldName [IN]
	 * 			the name of the field.
	 *
	 * 	@return	the current value of these field.
	 * 			Can be null .-)
	 */
	@SuppressWarnings("unchecked")
	public static < TType > TType getFieldValue (Object aSourceObject,
										         String sFieldName   )
		throws Exception
	{
		Class< ? > aClass = aSourceObject.getClass ();
		Field      aField = aClass.getDeclaredField(sFieldName);
		aField.setAccessible(true);
		return (TType) aField.get(aSourceObject);
	}

    //--------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static < TReturnType > TReturnType callPrivateMethod (Object    aTargetObject,
						  				  		     			 String    sMethodName  ,
						  				  		     			 Object... lParameter   )
		throws Exception
	{
		Class< ? >[] lParameterTypes = ObjectManipulation.impl_getParameterTypes(lParameter);
		Class< ? >   aClass          = aTargetObject.getClass ();
		Method[]     lMethods        = aClass.getDeclaredMethods();
		Method       aTargetMethod   = null;

		for (Method aMethod : lMethods)
		{
		    String sNameCheck = aMethod.getName();
		    if ( ! StringUtils.equals(sNameCheck, sMethodName))
		        continue;

		    aTargetMethod = aMethod;

		    Class< ? >[] lParamCheck = aMethod.getParameterTypes();
		    if (ArrayUtils.isEquals(lParamCheck, lParameterTypes))
		        break;
		}

/* ifdef DEBUG .-)
		System.out.println ("ObjectManipulation.callPrivateMethod () invoke '"+aTargetObject+"'.'"+aTargetMethod+"' () ...");
		for (Object aParameter : lParameter)
		    System.out.println (aParameter + " type["+aParameter.getClass ()+"]");
*/

		aTargetMethod.setAccessible(true);
		return (TReturnType) aTargetMethod.invoke(aTargetObject, lParameter);
	}

    //--------------------------------------------------------------------------
	private static Class< ? >[] impl_getParameterTypes (Object... lParameter)
		throws Exception
	{
		int          i               = 0;
		int          c               = lParameter.length;
		Class< ? >[] lParameterTypes = new Class[c];

		for (Object aParameter : lParameter)
			lParameterTypes[i++] = aParameter.getClass ();

		return lParameterTypes;
	}

	//--------------------------------------------------------------------------
	private static void impl_makeFieldAccessible (final Field aField)
		throws Exception
	{
		// remove 'private'
		aField.setAccessible(true);

		// remove 'final'
		final Field aModifiersField = Field.class.getDeclaredField("modifiers");
	    aModifiersField.setAccessible(true);
	    aModifiersField.setInt(aField, aField.getModifiers() & ~Modifier.FINAL);
	}
}