/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.system;

/**
 * Helper class to access system specific information of the JVM.
 *
 * @author Carsten Driesner
 */
public class SystemInfoHelper {

    public final static long KBYTES = 1024;
    public final static long MBYTES = 1024 * 1024;
    public final static long TYPICAL_BACKEND_HEAPSPACE = 250 * MBYTES;
    public final static long MINIMAL_FREE_BACKEND_HEAPSPACE = 100 * MBYTES;

    public static class MemoryInfo {
        public long usedHeapSize = 0;
        public long maxHeapSize = 0;
        public long freeHeapSize = 0;
    }

    /**
     * Provides information about the current free heap size, the total memory
     * used by the JVM and the maximal heap size.
     *
     * @return
     *  A MemoryInfo object which contains the heap memory details or all
     *  values are zero in case the information could not be retrieved.
     */
    static public MemoryInfo getMemoryInfo() {
        MemoryInfo memInfo = new MemoryInfo();

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory(); 
        final long freeHeapSize1 = Runtime.getRuntime().freeMemory();
        final long freeHeapSize2 = maxMemory - totalMemory;
        long freeHeap = freeHeapSize1 + freeHeapSize2;

        memInfo.maxHeapSize = maxMemory;
        memInfo.usedHeapSize = totalMemory;
        memInfo.freeHeapSize = freeHeap;

        return memInfo;
    }

    static public long maxMemory() {
    	return Runtime.getRuntime().maxMemory();
    }

    static public long totalMemory() {
    	return Runtime.getRuntime().totalMemory();
    }

    static public long freeMemory() {
    	// Determine the current free heap space by using the possible
    	// extension for the Java VM to allocate up to max heap space (-Xmx)
    	// and the amount of free memory which only uses the current total
    	// memory size as the limit. The addition of both values should
    	// give a good assumption of the possible free heap space.
        final long freeHeapSize1 = Runtime.getRuntime().freeMemory();
        final long freeHeapSize2 = Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory();
        return freeHeapSize1 + freeHeapSize2;
    }

    /**
     * Calculates the minimal JVM heap size we need to load a specific
     * document.
     *
     * @param filterHeapSpaceNeeded
     *  The memory needed by the filter to process the document.
     *
     * @return
     *  The minimal setting of the JVM heap size to load the document.
     */
    static public long calculateNeededHeapSize(long filterHeapSpaceNeeded) {
        return (filterHeapSpaceNeeded * 2) + TYPICAL_BACKEND_HEAPSPACE;
    }
}
