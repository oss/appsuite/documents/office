/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.user;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Predicate;
import org.apache.commons.logging.Log;

/**
 * Cache data needed for decrypting files from zips in get_file action
 *
 * @author Malte Timmermann
 * @since 7.8.3
 */
public class AuthorizationCache {

    @SuppressWarnings("deprecation")
    static protected final Log LOG = com.openexchange.log.LogFactory.getLog(AuthorizationCache.class);
    private static final long MAX_AGE_FOR_AUTOMATIC_GC = 24 * 60 * 60 * 1000; // 24h
    private static final long PERIOD_AUTOMATIC_GC      = 1 * 60 * 60 * 1000;  // 1h

    private final Map<String, AuthorizationEntry> hmap = new HashMap<>();
    private Timer timer = new Timer("com.openexchange.office.AuthorizationCache.Cleanup");
    private TimerTask timerTask = null;

    private static class Holder {
        private static final AuthorizationCache INSTANCE = new AuthorizationCache();
    }

    private static class AuthorizationEntry {
        private String  value;
        private long    timeStamp;
        private int     refCount;
        private int     refCountPersistent;

        public AuthorizationEntry(String value, boolean persistent) {
            this.value              = value;
            this.timeStamp          = System.currentTimeMillis();
            this.refCount           = 1;
            this.refCountPersistent = (persistent ? 1 : 0);
        }

        String getValue() {
            timeStamp = System.currentTimeMillis();
            return value;
        }

        long getLastAccessTime() { return timeStamp; }

        boolean isPersistent() { return (refCountPersistent > 0); }

        boolean canBeGC() {
            return ((refCount == 0) ||
                    (((Math.abs(System.currentTimeMillis() - getLastAccessTime())) > MAX_AGE_FOR_AUTOMATIC_GC) &&
                     !isPersistent()));
        }

        void inc(boolean persistent) {
            ++refCount;
            refCountPersistent += (persistent ? 1 : 0);
            timeStamp = System.currentTimeMillis();
        }

        boolean dec(boolean persistent) {
            boolean removeEntry = (--refCount <= 0);
            refCountPersistent -= (persistent ? 1 : 0);
            return removeEntry;
        }
    }

    private AuthorizationCache() {
        // create the task to do a gc of aged entries
        timerTask = new TimerTask() {

            @Override
            public void run() {
                try {
                    AuthorizationCache.getInstance().gc();
                }
                catch (Exception e)
                {
                    LOG.error("RT connection: exception caught trying to gc authorization cache entries", e);
                }
            }
        };

        // start the periodic timer with the specified timeout
        timer.schedule(timerTask, PERIOD_AUTOMATIC_GC, PERIOD_AUTOMATIC_GC);
    }

    public static AuthorizationCache getInstance() {
        return Holder.INSTANCE;
    }

    public static void addKey(String context, String user, String fileId, String value) {
        getInstance().addKey(createKey(context, user, fileId), false, value);
    }

    public static void addPersistentKey(String context, String user, String fileId, String value) {
        getInstance().addKey(createKey(context, user, fileId), true, value);
    }

    public static String getKey(String context, String user, String fileId) {
        return getInstance().getKey(createKey(context, user, fileId));
    }

    public static void removeKey(String context, String user, String fileId) {
        getInstance().removeKey(createKey(context, user, fileId), false);
    }

    public static void removePersistentKey(String context, String user, String fileId) {
        getInstance().removeKey(createKey(context, user, fileId), true);
    }

    private static String createKey(String context, String user, String fileId) {
        return "key:" + context + ":" + user + ":" + fileId;
    }

    private synchronized void addKey(String key, boolean persistent, String value) {
        final AuthorizationEntry entry = hmap.get(key);
        if (null == entry)
            hmap.put(key, new AuthorizationEntry(value, persistent));
        else {
            entry.inc(persistent);
        }
    }

    private synchronized String getKey(String key) {
        final AuthorizationEntry entry = hmap.get(key);
        return (null != entry) ? entry.getValue() : null;
    }

    private synchronized void removeKey(String key, boolean persistent) {
        final AuthorizationEntry entry = hmap.get(key);
        if (null != entry) {
            if (entry.dec(persistent))
                hmap.remove(key);
        }
    }

    protected synchronized void gc() {
        if (!hmap.isEmpty()) {
            final Set<String> keys = new HashSet<>(hmap.keySet());
            keys.stream()
                .filter(expiredEntry(hmap))
                .forEach(k -> hmap.remove(k));
        }
    }

    private static Predicate<String> expiredEntry(Map<String, AuthorizationEntry> aMap) {
        return e -> {
            final AuthorizationEntry entry = aMap.get(e);
            return (null != entry) ? entry.canBeGC() : false;
        };
    }
}
