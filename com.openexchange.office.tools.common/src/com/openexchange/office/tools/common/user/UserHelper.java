/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.user;

import org.apache.commons.lang3.StringUtils;
import com.openexchange.user.User;

/**
 * Helper functions regarding the User class
 * {@link UserHelper}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.8.0
 */
public class UserHelper {

    //-------------------------------------------------------------------------
    public static String getFullName(final User user) {
        String fullName = null;

        if (null != user) {
            final String givenName = user.getGivenName();
            final String surName = user.getSurname();
            boolean hasGivenName = StringUtils.isNotBlank(givenName);
            boolean hasSurName = StringUtils.isNotBlank(surName);

            if (hasSurName) {
                final StringBuilder tmp = new StringBuilder(64);
                tmp.append(surName);
                if (hasGivenName) {
                    tmp.append(", ");
                    tmp.append(givenName);
                }
                fullName = tmp.toString();
            } else if(hasGivenName){
                fullName = givenName;
            }
        }

        return fullName;
    }

    //-------------------------------------------------------------------------
    public static String getLangCode(final User user) {
        String sLangCode = null;

        if (null != user) {
            sLangCode =  mapUserLanguageToLangCode(user.getPreferredLanguage());
        }

        return sLangCode;
    }

    //-------------------------------------------------------------------------
    /**
     * Maps the user language provided by a User object to a ISO compliant
     * language code. E.g. en_US => en-US
     *
     * @param userLanguage
     *  The user language as provided by the User methods getPreferredLanguage
     *
     * @return
     *  An ISO compliant language code or null if language code couldn't be
     *  parsed correctly.
     */
    static public String mapUserLanguageToLangCode(String userLanguage) {
        return (null != userLanguage) ? (userLanguage.indexOf('_') >= 0 ? userLanguage.replace('_', '-') : userLanguage) : null;
    }

    private UserHelper() {}
}
