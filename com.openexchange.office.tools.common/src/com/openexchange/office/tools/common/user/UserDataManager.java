/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Manages the user data for every member of a connection instance.
 *
 * @author Carsten Driesner
 * @since 7.8.0
 */
public class UserDataManager {

    private final Map<String, UserData> userDataMap = new HashMap<>();
    private UserData lastUserData = null;
    private String lastUserKey = null;

    /**
     * Initalizes a new UserDataManager instance.
     */
    public UserDataManager() {
        // nothing to do
    }

    public synchronized int getCount() {
        return userDataMap.size();
    }

    public synchronized int getCountOnNode(String sNodeUID) {
    	int result = 0;
        for (final UserData aUserData : userDataMap.values()) {
            result += (sNodeUID.equals(aUserData.getNodeUUID())) ? 1 : 0;
        }
        return result;
    }

    /**
     * Provides a shallow copy of all users/clients known by this
     * instance.
     *
     * @return
     */
    public synchronized List<UserData> getUserData() {
        return new ArrayList<>(userDataMap.values());
    }

    public synchronized List<UserData> getUserDataCloned() {
		final List<UserData> result = new ArrayList<>();

		final Collection<UserData> orgUserDataList = userDataMap.values();
		for (final UserData orgUserData : orgUserDataList) {
			result.add(orgUserData.getCopy());
		}
		return result;
    }

    public synchronized boolean updateUserData(Collection<String> userIds) {
    	Set<String> keys = new HashSet<>(userDataMap.keySet());    	
    	userDataMap.keySet().retainAll(userIds);
    	return keys.containsAll(userDataMap.keySet());
    }
    
    /**
     * Returns the user uuids which fulfill the provided
     * @param aUserDataFilter
     * @return
     */
    public synchronized List<String> getFilteredUserData(Predicate<UserData> aUserDataFilter) {
        return userDataMap.values().stream().filter(aUserDataFilter)
                                            .map(o -> o.getUserId())
                                            .collect(Collectors.toList());
    }

    /**
     * Determines if user-specific data is associated with the real-time id.
     *
     * @param id the real-time id of the user.
     * @return true if user data is available and false if not
     */
    public synchronized boolean hasUserData(final String id) {
        return userDataMap.containsKey(id) || id.equalsIgnoreCase(lastUserKey);
    }

    /**
     * Retrieves the user-specific data associated with the real-time id.
     *
     * @param id the real-time id of the user.
     * @return the user-specific data or null
     */
    public final synchronized UserData getUserData(final String id) {
        if (!userDataMap.isEmpty()) {
            return userDataMap.get(id);
        } else if (id.equalsIgnoreCase(lastUserKey)) {
            return lastUserData;
        }
        return null;
    }

    /**
     * Retrieves the user-specific data associated with the real-time id if
     * that user is still a member of this document group.
     *
     * @param id the real-time id of the user.
     * @return the user-specific data or null
     */
    public final synchronized UserData getMemberUserData(final String id) {
        if (!userDataMap.isEmpty())
            return userDataMap.get(id);
        return null;
    }

    /**
     * Stores the user-specific data associated with the real-time id.
     *
     * @param id the real-time id of the user.
     * @param userData the user-specific data to be stored in association with the real-time id
     */
    public synchronized boolean addUser(final String id, final UserData userData) {
        userData.setUserId(id);
        return userDataMap.put(id, userData) == null;
    }

    /**
     * Removes the user-specific data associated with a real-time id.
     *
     * @param id the real-time id where the user-specific data should be removed.
     */
    public synchronized boolean removeUser(final String id) {
        final UserData userData = userDataMap.remove(id);
        if (userDataMap.isEmpty()) {
            // store last user data for use after the client left, e.g. dispose
            lastUserData = userData;
            lastUserKey = id;
        }
        return userData != null;
    }

}
