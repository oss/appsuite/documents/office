/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.user;

/**
 * Property class to store user-specific data regarding a connection instance.
 * Due to sharing it's now mandatory to store user-specific data and use it
 * for certain requests. Especially the document load/save processing must be
 * done using the user-specific folder & file-ids.
 *
 * @author Carsten Driesner
 * @since 7.8.0
 */
public class UserData {
	private String clientUid;
	private String user_id;
    private final String folder_id;
    private final String file_id;
    private final String session_id;
    private final boolean fastEmpty;
    private String nodeUUID;
    private String encryptionInfo;
    private final String localStorageVersion;
    private final int localStorageOSN;
    private LoadState loadState;
    private int hangupsReceived = 0;

    /**
     * Initialize a new UserData instance.
     *
     * @param folder_id
     * @param file_id
     * @param session
     * @param fastEmpty
     * @param cryptInfo
     */
    public UserData(String clientUid, final String folder_id, final String file_id, final String session, boolean fastEmpty, String cryptInfo, String nodeUUID) {
    	this.clientUid = clientUid;
        this.folder_id = folder_id;
        this.file_id = file_id;
        this.session_id = session;
        this.fastEmpty = fastEmpty;
        this.encryptionInfo = cryptInfo;
        this.nodeUUID = nodeUUID;
        this.localStorageVersion = null;
        this.localStorageOSN = -1;
        this.loadState = LoadState.LOADSTATE_IN_PROGRESS;
    }

    public UserData(String clientUid, final String folder_id, final String file_id, final String session, boolean fastEmpty, String cryptInfo, String localStorageVersion, int localStorageOSN, String nodeUUID) {
    	this.clientUid = clientUid;
        this.folder_id = folder_id;
        this.file_id = file_id;
        this.session_id = session;
        this.fastEmpty = fastEmpty;
        this.encryptionInfo = cryptInfo;
        this.localStorageVersion = localStorageVersion;
        this.localStorageOSN = localStorageOSN;
        this.loadState = LoadState.LOADSTATE_IN_PROGRESS;
        this.nodeUUID = nodeUUID;
    }

    public final String getClientUid() { return clientUid; }

    public final String getUserId() { return user_id; }

    /**
     * Retrieves the folder id of the document for this user.
     *
     * @return
     */
    public final String getFolderId() { return folder_id; }

    /**
     * Retrieves the file id of the document for this user.
     *
     * @return
     */
    public final String getFileId() { return file_id; }

    /**
     * Retrieves the Guard auth code of the document for this user.
     *
     * @return
     */
    public final String getEncryptionInfo() { return encryptionInfo; }

    /**
     * Retrieves the session id of the user,
     *
     * @return
     */
    public final String getSessionId() { return session_id; }

    public boolean getFastEmptyState() { return fastEmpty; }

    public final String getNodeUUID() { return nodeUUID; }

    protected void setUserId(String userId) { user_id = userId; }

    public void setEncryptionInfo(String sEncryptionInfo) { encryptionInfo = sEncryptionInfo; }

    public String getLocalStorageVersion() { return localStorageVersion; }

    public int getLocalStorageOSN() { return localStorageOSN; }

    public LoadState getLoadState() { return loadState; }

    public void setLoadState(LoadState loadState) { this.loadState = loadState; }

    public static boolean filterOutLoadStateInProgress(final UserData aUserData) { return (aUserData.getLoadState() == LoadState.LOADSTATE_COMPLETED); }

    public UserData getCopy() {
    	return new UserData(clientUid, folder_id, file_id, session_id, fastEmpty, encryptionInfo, localStorageVersion, localStorageOSN, nodeUUID);
    }
}
