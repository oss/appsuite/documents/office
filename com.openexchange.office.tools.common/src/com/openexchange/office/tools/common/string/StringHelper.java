/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.string;

import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;
import java.util.function.IntPredicate;
import org.apache.commons.lang3.StringUtils;

public class StringHelper {

    //-------------------------------------------------------------------------
    private static final char[] hexArray = { '0','1','2', '3','4','5','6','7','8','9','A','B','C', 'D','E','F' };
    private static final IntPredicate isHexChar = c -> ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f'));

    //-------------------------------------------------------------------------
    public static final int MAX_BYTES_LENGTH = 1024;
	public static final String STR_PLACEHOLDER = "{}";

    //-------------------------------------------------------------------------
	private StringHelper() {}

    //-------------------------------------------------------------------------
    public static String replacePlaceholdersWithArgs(String msg, String...values)  {
    	if (msg == null)
    		throw new IllegalArgumentException("Placeholder string must not be null!");

    	final StringBuffer tmp = new StringBuffer(msg);
    	int valIndex = 0;
    	int index = 0;
    	while ((index = tmp.indexOf(STR_PLACEHOLDER, index)) > 0) {
    		String tmpValue = (values.length > valIndex) ? values[valIndex] : "null";
    		String value = (tmpValue == null) ? "null" : tmpValue;
    		tmp.replace(index, index + STR_PLACEHOLDER.length() , value);
    		index += value.length();
    		++valIndex;
    	}

    	return tmp.toString();
    }

    //-------------------------------------------------------------------------
    public static boolean isHexString(String isHex) {
        if (StringUtils.isNotEmpty(isHex)) {
            return isHex.chars().allMatch(isHexChar);
        }
        return false;
    }

    //-------------------------------------------------------------------------
	public static String toHexString(final byte[] aBytes) {
		return createHexString(aBytes, aBytes.length);
    }

    //-------------------------------------------------------------------------
	public static String toHexString(final byte[] aBytes, boolean bTruncate) {
		return createHexString(aBytes, bTruncate ? MAX_BYTES_LENGTH : aBytes.length);
    }

    //-------------------------------------------------------------------------
    public static String toHexString (String string) {
        if (StringUtils.isEmpty(string))
            return "";

        StringBuilder tmp = new StringBuilder(256);
        for (int i=0; i < string.length(); i++) {
            tmp.append(String.format("%04x", (int)string.charAt(i)));
        }
        return tmp.toString();
    }

    //-------------------------------------------------------------------------
	public static String toHexStringUTF16 (String string) {
	    byte[] buf = null;
	    try {
	         buf = string.getBytes("UTF-16");
	    } catch (UnsupportedEncodingException e) {
	        return "UnsupportedEncodingException";
	    }
	    return toHexString(buf);
	}

    //-------------------------------------------------------------------------
    public static String toHexStringUTF8 (String string) {
        byte[] buf = null;
        try {
             buf = string.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "UnsupportedEncodingException";
        }
        return toHexString(buf);
    }

    //-------------------------------------------------------------------------
	public static boolean isNotEmptyAndEqual(String s1, String s2) {
		if ((s1 != null) && (s2 != null) && StringUtils.isNotEmpty(s1) && StringUtils.isNotEmpty(s2)) {
			return s1.equals(s2);
		}
		return false;
	}

    //-------------------------------------------------------------------------
	private static String createHexString(final byte[] aBytes, int nMaxLength) {
        final char[] aHexChars = new char[Math.min(nMaxLength, aBytes.length) * 2];

        for ( int i = 0; i < aBytes.length; i++ ) {
            final int b = aBytes[i] & 0xFF;
            aHexChars[i*2]   = hexArray[b >>> 4 ];
            aHexChars[i*2+1] = hexArray[b & 0x0F];
        }

        return new String(aHexChars);
    }

    //-------------------------------------------------------------------------
	private static Set<Character> asSet(char[] chars) {
        final Set<Character> result = new HashSet<>();
        if ((chars != null) && (chars.length > 0)){
            for (char c : chars) {
                result.add(c);
            }
        }
        return result;
    }

}
