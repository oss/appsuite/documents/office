/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.string;

public class UnicodeValidator {

    private UnicodeValidator() {}

    /*
     * This method checks if there is invalid Unicode used (high surrogate without low surrogate or
     * low surrogate without high surrogate)). These invalid surrogates are replaced by replaceChar.
     * The fixed String is then returned, in case nothing was changed the output string is equal
     * to the input string.
     */
    public static String replaceInvalidCharacters(String s, char replaceChar) {
        if(s!=null) {
            for(int i=0; i<s.length(); i++) {
                final char c = s.charAt(i);
                if(Character.isHighSurrogate(c)) {
                    if(i+1==s.length()||!Character.isLowSurrogate(s.charAt(i+1))) {
                        final StringBuilder buffer = new StringBuilder(s);
                        buffer.setCharAt(i, replaceChar);
                        s = buffer.toString();
                    }
                }
                else if(Character.isLowSurrogate(c)) {
                    if(i==0||!Character.isHighSurrogate(s.charAt(i-1))) {
                        final  StringBuilder buffer = new StringBuilder(s);
                        buffer.setCharAt(i, replaceChar);
                        s = buffer.toString();
                    }
                }
            }
        }

        return s;
    }

    public static String replaceComposedUnicodeCharacters(String s, char replaceChar) {
        String result = s;

        if(s!=null) {
            final StringBuilder buffer = new StringBuilder(s);

            for(int i=0; i<s.length(); i++) {
                final char c = s.charAt(i);
                if(Character.isHighSurrogate(c) ||
                  (Character.isLowSurrogate(c))) {
                    buffer.setCharAt(i, replaceChar);
                }
            }

            result = buffer.toString();
        }
        return result;
    }
}
