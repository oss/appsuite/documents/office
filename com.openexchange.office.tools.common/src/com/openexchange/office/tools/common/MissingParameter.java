/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import org.apache.commons.lang3.Validate;

import com.openexchange.ajax.requesthandler.AJAXRequestData;

/**
 * Simple class checking the arguments of a request and provides information
 * if and what mandatory argument is missing.
 *
 * @author Carsten Driesner
 * @since 7.8.1
 */
public class MissingParameter {

    /**
     * Checks a request for mandatory parameters.
     * @param reqDataHelper A RequestDataHelper initialized with the request to check
     * @param mandtoryParameterNames The names of the mandatory parameters
     * @return NULL if no mandatory parameter is missing or the name of the first
     *         parameter missing.
     */
    public static final String hasMissing(final AJAXRequestData request, final String... mandtoryParameterNames) {
        Validate.notNull(request);
        Validate.notNull(mandtoryParameterNames);

        String missing = null;
        for (final String mandatoryParamter : mandtoryParameterNames) {
            final String value = request.getParameter(mandatoryParamter);
            if (null == value) {
                missing = mandatoryParamter;
                break;
            }
        }

        return missing;
    }
}
