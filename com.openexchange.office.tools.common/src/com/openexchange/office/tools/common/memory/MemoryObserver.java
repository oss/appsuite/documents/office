/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.memory;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryNotificationInfo;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;
import java.lang.management.MemoryUsage;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.LongBinaryOperator;
import java.util.stream.Collectors;
import javax.management.Notification;
import javax.management.NotificationEmitter;
import javax.management.NotificationListener;
import javax.management.openmbean.CompositeData;

import org.slf4j.Logger;

import com.openexchange.exception.ExceptionUtils;
import com.openexchange.java.ConcurrentList;
import com.openexchange.office.tools.common.TimeStampUtils;
import com.openexchange.office.tools.common.system.SystemInfoHelper;
import com.openexchange.office.tools.common.system.SystemInfoHelper.MemoryInfo;

/**
 * Memory observer class which tracks the heap usage of the
 * greatest heap memory pool and provides monitoring data.
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 *
 * revised for 7.10.4
 * revised for 8.0.0 / 7.10.5
 */
public class MemoryObserver implements NotificationListener {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(MemoryObserver.class);

    private static final long PERIOD_TO_RESET_VALUES = 30 * 60 * 1000; // 30 minutes (ms)
    private static final long FREQ_TO_CHECK_FOR_RESET = PERIOD_TO_RESET_VALUES / 30; // (ms)
    private static final String GC_NOTIFICATION = "com.sun.management.gc.notification";
    private static final long MAX_WAIT_FOR_GC = 50; // time in ms
    private static final long MIN_TIME_BETWEEN_GC = 300; // seconds
    private static final double MAX_PERCENTAGE_THRESHOLD = 0.85; // 85%
    private static final double TYPICAL_MEMPOOL_PERCENTAGE = 0.80; // 80% of max memory
    private static final long MIN_FREE_HEAP_MEMORY = 131072000; // 128MB minimum free heap space
    private static final MemoryPoolMXBean thresholdMemPool = getThresholdMemPool();
    private static final AtomicReference<MemoryObserver> memoryObserver = new AtomicReference<>(new MemoryObserver());
    private static final LongBinaryOperator add = (x, y) -> (x + y);

    private final ConcurrentList<MemoryListener> registeredListeners = new ConcurrentList<MemoryListener>();
    private long threshold = 0;
    private final AtomicReference<LocalDateTime> lastTriggeredGCRun = new AtomicReference<>(LocalDateTime.now());
    private final AtomicLong countOfDocNotLoadedDueToMemPressure = new AtomicLong(0);
    private final AtomicLong countMemPoolThresholdNotifications = new AtomicLong(0);
    private final AtomicLong countHeapThresholdNotifications = new AtomicLong(0);
    private final AtomicLong countGCTriggered = new AtomicLong(0);
    private final AtomicLong lastGCRun = new AtomicLong(0);
    private final AtomicLong countOfDocReqDeferredDueToMemPressure = new AtomicLong(0);
    private final AtomicLong timeWaitForGCDueToMemPressure = new AtomicLong(0);
    private final AtomicLong startOfEpisodeForResetValues = new AtomicLong(0);
    private final AtomicLong startOfHourPeriodForWarnOnTooManyGCRuns = new AtomicLong(0);
    private final Timer timer;

    public interface MemoryListener {

        void memoryTresholdExceeded(long usedMemory, long maxMemory);
    }

    /**
     * Initializes a new instance of the MemoryObserver.
     */
    private MemoryObserver() {
        MemoryMXBean mbean = ManagementFactory.getMemoryMXBean();
        NotificationEmitter emitter = (NotificationEmitter) mbean;
        emitter.addNotificationListener(this, null, null);

        setUsageThreshold();

        List<GarbageCollectorMXBean> gcBeans = ManagementFactory.getGarbageCollectorMXBeans();
        List<GarbageCollectorMXBean> gcBeansToListen = gcBeans.stream().filter(m -> containsForGCNotification(m)).collect(Collectors.toList());

        if (gcBeansToListen.size() > 0) {
            NotificationListener listener = new NotificationListener() {

                @Override
                public void handleNotification(Notification notification, Object handback) {
                    if (GC_NOTIFICATION.equals(notification.getType())) {
                        lastGCRun.set(System.currentTimeMillis());
                    }
                }
            };

            for (GarbageCollectorMXBean m : gcBeansToListen) {
                emitter = (NotificationEmitter) m;
                emitter.addNotificationListener(listener, null, null);
            }
        }

        long now = System.currentTimeMillis();
        startOfEpisodeForResetValues.set(now);
        startOfHourPeriodForWarnOnTooManyGCRuns.set(now);

        TimerTask resetValuesTask = new TimerTask() {

            @Override
            public void run() {
                final long now = System.currentTimeMillis();
                if (TimeStampUtils.timeDiff(startOfEpisodeForResetValues.get(), now) > PERIOD_TO_RESET_VALUES) {
                    final long countGC = countGCTriggered.get();
                    if (countGC >= (PERIOD_TO_RESET_VALUES / MIN_TIME_BETWEEN_GC)) {
                        long periodInMins = (PERIOD_TO_RESET_VALUES / 1000 / 60);
                        LOG.warn("ATTENTION: The MemoryObserver detected that the GC had to be triggered {} time(s) in {} minutes to fulfill heap space requirements to load a document. The system is under high memory load.", countGC, periodInMins);
                    }
                    countOfDocNotLoadedDueToMemPressure.set(0);
                    countMemPoolThresholdNotifications.set(0);
                    countHeapThresholdNotifications.set(0);
                    countGCTriggered.set(0);
                    countOfDocReqDeferredDueToMemPressure.set(0);
                    timeWaitForGCDueToMemPressure.set(0);
                    startOfEpisodeForResetValues.set(now);
                }
            }
        };
        timer = new Timer("MemoryObserver_ResetTimer");
        timer.schedule(resetValuesTask, FREQ_TO_CHECK_FOR_RESET, FREQ_TO_CHECK_FOR_RESET);
    }

    /**
     * Get the singleton MemoryObserver instance.
     *
     * @return
     *         the single MemoryObserver instance
     */
    public static MemoryObserver get() {
        return memoryObserver.get();
    }

    public long lastGCRun() {
        return lastGCRun.get();
    }

    /**
     * Adds a listener to the MemoryObserver instance.
     *
     * @param listener adds a listener to the MemoryObserver instance.
     * @return
     */
    public boolean addListener(MemoryListener listener) {
        return registeredListeners.add(listener);
    }

    /**
     * Removes a listener from the MemoryObserver instance.
     *
     * @param listener a listener to be removed from the listener list
     * @return true if the listener was removed otherwise false
     */
    public boolean removeListener(MemoryListener listener) {
        return registeredListeners.remove(listener);
    }

    /**
     * Provides the name of the memory pool which is observed for
     * threshold exceeding notifications.
     *
     * @return
     *         name of the observed memory pool
     */
    public String getObservedMemPoolName() {
        return thresholdMemPool.getName();
    }

    /**
     * Provides the max size of the memory pool which is observed
     * for threshold exceeding notifications.
     *
     * @return
     *         the maximum amount of memory in bytes; -1 if undefined.
     */
    public long getObservedMemPoolMaxSize() {
        return thresholdMemPool.getUsage().getMax();
    }

    /**
     * Provides the observed memory pool peak usage
     * in bytes.
     *
     * @return the memory pool peak usage in bytes
     */
    public long getObservedMemPoolPeakUsage() {
        return thresholdMemPool.getPeakUsage().getUsed();
    }

    /**
     * Provides the observed memory pool threshold
     * notification count.
     *
     * @return the number of times the memory pool
     *         has notified listeners about a reached threshold.
     */
    public long getObservedMemPoolThresholdCount() {
        return countMemPoolThresholdNotifications.get();
    }

    /**
     * Provides the number of threshold notifications
     * that were issued due to a heap size limit.
     *
     * @return the number of times the threshold notification
     *         for the complete heap
     */
    public long getCountHeapThresholdNotifications() {
        return countHeapThresholdNotifications.get();
    }

    /**
     * Increases the number of documents which couldn't
     * be loaded due to high memory pressure.
     */
    public void incNumOfDocsNotLoadedDueToMemPressure() {
        countOfDocNotLoadedDueToMemPressure.addAndGet(1);
    }

    /**
     * Resets the number of documents not loaded due to
     * high memory pressure.
     */
    public void resetNumOfDocsNotLoadedDueToMemPressure() {
        countOfDocNotLoadedDueToMemPressure.set(0);
    }

    /**
     * Provides the number of documents which couldn't be
     * loaded due to high memory pressure.
     *
     * @return the number of documents that couldn't be
     *         loaded due to high memory pressure.
     */
    public long getNumOfDocsNotLoadedDueToMemPressure() {
        return countOfDocNotLoadedDueToMemPressure.get();
    }

    /**
     * Provides the number of times the gc was triggered
     * by the MemoryObserver.
     *
     * @return the number of times the gc was triggered
     */
    public long getNumOfGCTriggered() {
        return countGCTriggered.get();
    }

    /**
     * Provides the LocalDateTime when the last gc was
     * triggered by the MemoryObserver.
     *
     * @return
     */
    public LocalDateTime getGCLastTriggered() {
        return lastTriggeredGCRun.get();
    }

    /**
     * Provides the current memory pool threshold used.
     *
     * @return
     *         the current memory pool threshold to be notified.
     */
    public long getMemPoolThreshold() {
        return threshold;
    }

    /**
     * Provides the maximum heap size where exceed
     * notifications would be sent.
     *
     * @return
     *         the maximum heap size where exceed notifications would be sent.
     */
    public long getMaxHeapExceedThreshold() {
        return (SystemInfoHelper.maxMemory() - MIN_FREE_HEAP_MEMORY);
    }

    /**
     * Provides the currently calculated free heap space.
     *
     * @return
     *         the calculated free heap space.
     */
    public long getCalcFreeHeapSize() {
        return SystemInfoHelper.freeMemory();
    }

    public long getCountOfDocReqDeferredDueToMemPressure() {
        return countOfDocReqDeferredDueToMemPressure.get();
    }

    public long getTimeWaitForGCDueToMemPressure() {
        return timeWaitForGCDueToMemPressure.get();
    }

    /**
     * Provides the current state of the threshold exceeding notification.
     *
     * @return
     *         true if the threshold exceeded state was set and the current free
     *         heap space indicates a critical limit. false if no threshold
     *         exceeding limit has been notified.
     */
    public boolean isUsageThresholdExceeded() {
        boolean exceeded = thresholdMemPool.isCollectionUsageThresholdExceeded();
        if (exceeded) {
            final MemoryUsage memPoolUsage = thresholdMemPool.getUsage();
            long memPoolThreshold = getMemPoolThreshold();

            LOG.debug("MemoryObserver.isUsageThresholdExceeded - data of mem pool: max {}, used: {},  limit:  {}", memPoolUsage.getMax(), memPoolUsage.getUsed(), getMemPoolThreshold());

            // Check memory usage manually as the call is true although
            // memory consumption has fallen below threshold and we need
            // to check the complete heap if free heap size is also below
            // our threshold
            exceeded = (memPoolUsage.getUsed() > memPoolThreshold);
            exceeded = (exceeded || isCompleteHeapLimitReached());

            if (!triggerGCIfPossible()) {
                waitForGCWithTimeout(MAX_WAIT_FOR_GC);
            }

            exceeded = (memPoolUsage.getUsed() > memPoolThreshold);
            exceeded = (exceeded || isCompleteHeapLimitReached());

            if (exceeded) {
                logFreeHeapSizeTooLow();
                thresholdMemPool.resetPeakUsage();
            }

            // Attention:
            // The threshold needs to be configured again, otherwise the JVM
            // won't notify us about new threshold exceeds! It's some kind of
            // "refresh" we trigger here ;-)
            thresholdMemPool.setCollectionUsageThreshold(memPoolThreshold);
        }

        return exceeded;
    }

    /**
     * Determines if an additional memory requirement will violate the
     * minimal free heap size.
     *
     * @param additonalMemNeeded the amount of memory needed in bytes
     * @return TRUE if the additional memory would violate the minimal free
     *         heap size, otherwise FALE.
     */
    public boolean willHeapLimitBeReached(long additonalMemNeeded) {
        LOG.trace("MemoryObserver.willHeapLimitBeReached with {} add memory", additonalMemNeeded);

        boolean limitReached = ((SystemInfoHelper.freeMemory() - additonalMemNeeded) < MIN_FREE_HEAP_MEMORY);
        if (limitReached) {
            LOG.debug("MemoryObserver.willHeapLimitBeReached with {} add memory reaches limit: {}, free: {}", additonalMemNeeded, getMaxHeapExceedThreshold(), SystemInfoHelper.freeMemory());

            if (!triggerGCIfPossible()) {
                waitForGCWithTimeout(MAX_WAIT_FOR_GC);
            }

            limitReached = ((SystemInfoHelper.freeMemory() - additonalMemNeeded) < MIN_FREE_HEAP_MEMORY);
            if (limitReached) {
                LOG.debug("MemoryObserver.willHeapLimitBeReached after gc triggered with {} add memory reaches limit: {}, free: {}", additonalMemNeeded, getMaxHeapExceedThreshold(), SystemInfoHelper.freeMemory());
            }
        }
        return limitReached;
    }

    /**
     * Invoked when a JMX notification occurs. The registered listeners are
     * called to react on exceeding the memory threshold, if we checked that
     * our limits for the complete free heap (including all mem pools) has
     * been reached.
     *
     * The threshold must be registered at the "old gen" memory pool which
     * can grow very large although the JVM has enough memory in other memory
     * pool (depends on the Java version and gc strategy). Therefore we need
     * a second check that the free heap has reached a critical limit.
     */
    @Override
    public void handleNotification(Notification n, Object jb) {
        try {
            if (n.getType().equals(MemoryNotificationInfo.MEMORY_COLLECTION_THRESHOLD_EXCEEDED)) {
                countMemPoolThresholdNotifications.incrementAndGet();

                final CompositeData cd = (CompositeData) n.getUserData();
                final MemoryNotificationInfo info = MemoryNotificationInfo.from(cd);
                final MemoryUsage memUsage = info.getUsage();

                // retrieve memory data from the notification
                final long maxMemory = memUsage.getMax();
                final long usedMemory = memUsage.getUsed();
                final long memLimit = threshold;
                LOG.debug("MemoryObserver: Notification due to exceeding memory threshold for the observed mem pool! Max memory: {}, limit: {}, used memory: {}", maxMemory, memLimit, usedMemory);

                // Check limits manually, there are situations where the notification is sent
                // although memory threshold is NOT reached. We also need to check the free
                // heap size as we only observe one memory pool which can grow large before
                // the gc starts (depending on Java version and gc strategy). Try to give JVM
                // a 2nd chance before we report a memory problem. There are situations when the
                // notification is triggered and we see only minor free memory left, but afterwards
                // the GC was able to free a large amount of resources.
                final boolean completeHeapLimitReached = isCompleteHeapLimitReached();
                if (completeHeapLimitReached) {
                    if (!triggerGCIfPossible()) {
                        waitForGCWithTimeout(MAX_WAIT_FOR_GC);
                    }
                }

                if (isCompleteHeapLimitReached()) {
                    countHeapThresholdNotifications.incrementAndGet();
                    logFreeHeapSizeTooLow();

                    final java.util.List<MemoryListener> listeners = registeredListeners.getSnapshot();
                    for (MemoryListener listener : listeners) {
                        try {
                            listener.memoryTresholdExceeded(usedMemory, maxMemory);
                        } catch (Throwable e) {
                            // nothing can be done, but do not swallow severe VM errors
                            LOG.warn("{} failed to handle exceeded memory threshold", listener.getClass().getName(), e);
                            ExceptionUtils.handleThrowable(e);
                        }
                    }
                } else {
                    thresholdMemPool.setCollectionUsageThreshold(getMemPoolThreshold());
                    thresholdMemPool.resetPeakUsage();
                }
            }
        } catch (Throwable t) {
            LOG.warn("Failed to handle JMX notification", t);
            throw t;
        }
    }

    /**
     * Calculates the amount of memory for a certain percentage of the max heap size.
     *
     * @param percent the percentage of the max heap size
     * @return the amount of memory for that percentage
     */
    public static long calcMaxHeapSize(int percent) {
        final SystemInfoHelper.MemoryInfo memInfo = SystemInfoHelper.getMemoryInfo();
        final long maxHeapSize = Math.round(((double) memInfo.maxHeapSize * (double) percent) / 100);
        return maxHeapSize;
    }

    /**
     * Determines if the heap space limit has been reached using the
     * complete free heap space (including all memory pools of this
     * JVM instance).
     *
     * @return
     *         true if the heap space limit has been reached, otherwise false
     */
    private boolean isCompleteHeapLimitReached() {
        final long freeHeapSize = SystemInfoHelper.freeMemory();
        return (freeHeapSize < MIN_FREE_HEAP_MEMORY);
    }

    /**
     * Sets a calculated threshold for the memory pool selected.
     */
    private void setUsageThreshold() {
        long maxMemory = (long) Math.max(thresholdMemPool.getUsage().getMax(), SystemInfoHelper.maxMemory() * TYPICAL_MEMPOOL_PERCENTAGE);
        long threshold1 = (long) (maxMemory * MAX_PERCENTAGE_THRESHOLD);
        long threshold2 = maxMemory - MIN_FREE_HEAP_MEMORY;

        // Use the minimal value to ensure that the mem pool threshold is called early
        // enough to determine if we have free heap size pressure.
        threshold = Math.min(threshold1, threshold2);

        thresholdMemPool.setCollectionUsageThreshold(threshold);
        LOG.info("MemoryObserver: Uses memory pool: {} with maxMemory: {}, threshold: {}", thresholdMemPool.getName(), maxMemory, threshold);
    }

    /**
     * Tries to trigger the garbage collector if a certain amount of
     * time has been expired.
     */
    private boolean triggerGCIfPossible() {
        final LocalDateTime now = LocalDateTime.now();
        final LocalDateTime lastGCRun = lastTriggeredGCRun.get();
        if (lastGCRun.plusSeconds(MIN_TIME_BETWEEN_GC).isBefore(now)) {
            lastTriggeredGCRun.set(now);
            System.gc();
            countGCTriggered.incrementAndGet();
            return true;
        }
        return false;
    }

    /**
     * Determines the memory pool available in the
     * JVM mem pools that supports the
     * MEMORY_COLLECTION_THRESHOLD_EXCEEDED notification.
     *
     * @return
     *         a MemoryPoolMXBean which can be used to register a threshold
     *         exceeded listener or null if no pool is available to be used
     *         for threshold exceeding notifications.
     */
    private static MemoryPoolMXBean getThresholdMemPool() {
        MemoryPoolMXBean thresholdMemPool = null;
        for (MemoryPoolMXBean pool : ManagementFactory.getMemoryPoolMXBeans()) {
            if (pool.getType() == MemoryType.HEAP && pool.isUsageThresholdSupported()) {
                thresholdMemPool = pool;
                break;
            }
        }
        return thresholdMemPool;
    }

    /**
     * Logs a warning with the current heap size data reached the
     * overall heap size limit, where certain actions/operations
     * are not possible to fulfill.
     */
    private static void logFreeHeapSizeTooLow() {
        final MemoryInfo memInfo = SystemInfoHelper.getMemoryInfo();
        final long heapLimit = (memInfo.maxHeapSize - MIN_FREE_HEAP_MEMORY);
        final long freeHeapSize = memInfo.freeHeapSize;
        LOG.warn("MemoryObserver: Detected criticial memory threshold for free heap! Max memory: {}, limit: {}, used memory: {}, free memory: {}", memInfo.maxHeapSize, heapLimit, memInfo.usedHeapSize, freeHeapSize);
    }

    /**
     * Check if the observed memory pool is part of the GarbageCollectorMXBean.
     *
     * @param gcMBean an arbitrary GarbageCollectorMXBean instance to be checked for containment
     * @return TRUE if the bean contains the memory pool otherwise FALSE.
     */
    private boolean containsForGCNotification(GarbageCollectorMXBean gcMBean) {
        if (gcMBean == null)
            return false;

        String obbservedPoolName = getObservedMemPoolName();
        String[] poolNames = gcMBean.getMemoryPoolNames();
        for (String p : poolNames) {
            if (obbservedPoolName.equals(p))
                return true;
        }

        return false;
    }

    /**
     * 
     * waitForGCWithTimeout
     *
     * @param timeoutInMS
     */
    private void waitForGCWithTimeout(long timeoutInMS) {
        countOfDocReqDeferredDueToMemPressure.incrementAndGet();

        final long start = System.currentTimeMillis();

        long waitTime = 0;
        try {
            do {
                long now = System.currentTimeMillis();
                final long lastRunOfGC = lastGCRun.get();
                if (TimeStampUtils.inBetween(start, now, lastRunOfGC))
                    break;

                Thread.sleep(MAX_WAIT_FOR_GC / 10);

                now = System.currentTimeMillis();
                waitTime = TimeStampUtils.timeDiff(start, now);
            } while (waitTime < MAX_WAIT_FOR_GC);

            timeWaitForGCDueToMemPressure.accumulateAndGet(waitTime, add);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
