/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.files;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import com.openexchange.file.storage.File;

public class FileHelper {

    public static final String STR_EXT_PGP = "pgp";
    public static final String STR_EXT_RESCUE = "_ox";

    private FileHelper() {};

    /**
     * Gets the base name, minus the full path and extension, from a full
     * filename.
     *
     * @param fileName
     *  The file name where the base name should be retrieved from. The
     *  fileName can be null.
     *
     * @return
     *  The base name of the given filename or null if fileName is null.
     */
    static public String getBaseName(String fileName) {
        return getBaseName(fileName, false);
    }

    /**
     * Gets the base name, minus the full path and extension, from a full
     * filename.
     *
     * @param fileName
     *  The file name where the base name should be retrieved from. The
     *  fileName can be null.
     *
     * @param ignoreGuardExt
     *  Removes a possible guard file extension.
     *
     * @return
     *  The base name of the given filename or null if fileName is null.
     */
    static public String getBaseName(String fileName, boolean ignoreGuardExt) {
        if (ignoreGuardExt &&
            ((fileName != null) && (fileName.length() > STR_EXT_PGP.length() + 1)) &&
            (STR_EXT_PGP.equals(FileHelper.getExtension(fileName, false)))) {
            fileName = fileName.substring(0, fileName.length() - 1 - STR_EXT_PGP.length() - 1); // remove pgp extension including '.'
        }

        return FilenameUtils.getBaseName(fileName);
    }

    /**
     * Gets the file name including the extension, minus the full path, from
     * a full path filename.
     *
     * @param fullPathFileName
     *  The filename with  the full path including the extension. The
     *  fullPathFileName can be null.
     *
     * @return
     *  The file name of the given full path file name or null if the
     *  fullPathFileName is null.
     */
    static public String getFileName(String fullPathFileName) {
        return getFileName(fullPathFileName, false);
    }

    /**
     * Gets the file name including the extension, minus the full path, from
     * a full path filename.
     *
     * @param fullPathFileName
     *  The filename with  the full path including the extension. The
     *  fullPathFileName can be null.
     *
     * @param ignoreGuardExt
     *  Removes a possible guard file extension.
     *
     * @return
     *  The file name of the given full path file name or null if the
     *  fullPathFileName is null.
     */
    static public String getFileName(String fullPathFileName, boolean ignoreGuardExt) {
        String fileName  = FilenameUtils.getName(fullPathFileName);

        if (ignoreGuardExt) {
            final StringBuffer tmp = new StringBuffer(64);
            tmp.append(getBaseName(fileName, true));
            tmp.append(".");
            tmp.append(getExtension(fileName, true));
            fileName = tmp.toString();
        }

        return fileName;
    }

    /**
     * Retrieves the extension of a file name.
     *
     * @param fileName
     *  The file name, the extension should be retrieved from.
     *
     * @return
     *  The extension of the given filename or an empty string in case of
     *  (null == fileName) or if no extension is set at all
     */
    static public String getExtension(String fileName) {
        return getExtension(fileName, false);
    }

    /**
     * Retrieves the extension of a file name.
     *
     * @param fileName
     *  The file name, the extension should be retrieved from.
     *
     * @param ignoreGuardExt
     *  Ignore the Guard pgp extension
     *
     * @return
     *  The extension of the given filename or an empty string in case of
     *  (null == fileName) or if no extension is set at all
     */
    static public String getExtension(String fileName, boolean ignoreGuardExt) {
        int index = (fileName != null) ? fileName.lastIndexOf('.') : -1;
        String ext = (index >= 0) ? fileName.substring(index + 1).toLowerCase() : "";
        if ((ignoreGuardExt == true) && ext.equals(STR_EXT_PGP)) {
            fileName = fileName.substring(0, index);
            ext = getExtension(fileName, false);
        }

        return ext;
    }

    /**
     * Replaces the extension of a full filename (including extensions) including
     * a possible guard extension.
     *
     * @param fileName
     *  The full file name where the extension(s) are replaced.
     *
     * @param newExtension
     *  The new extension to be used.
     *
     * @param removeGuardExt
     *  Remove a possible guard extension.
     *
     * @return
     *  The file name with the new extension replacing the old one.
     */
    public static String getFileNameWithReplacedExtension(String fileName, String newExtension, boolean removeGuardExt) {
        final String baseFileName = getBaseName(fileName, removeGuardExt);

        return new StringBuffer(baseFileName).append('.').append(newExtension).toString();
    }

    /**
     * Create a file name with postfix string part.
     *
     * @param fileName
     *  A file name with extension and without path part.
     *
     * @param postFix
     *  A text part which should be appended to the base file name.
     *
     * @return
     *  A file name with consists of: <filename><postFix>.<ext>
     */
    public static String createFilenameWithPostfix(final String fileName, final String postFix) {
        return createFilenameWithPostfix(fileName, postFix, false);
    }

    /**
     * Create a file name with postfix string part.
     *
     * @param fileName
     *  A file name with extension and without path part.
     *
     * @param postFix
     *  A text part which should be appended to the base file name.
     *
     * @param ignoreGuardExt
     *  Ignore the Guard pgp extension
     *
     * @return
     *  A file name with consists of: <filename><postFix>.<ext>
     */
    public static String createFilenameWithPostfix(final String fileName, final String postFix, boolean ignoreGuardExt) {
        final StringBuffer tmp = new StringBuffer(64);
        tmp.append(FileHelper.getBaseName(fileName, ignoreGuardExt));
        tmp.append(postFix);
        tmp.append(".");
        tmp.append(getExtension(fileName, ignoreGuardExt));
        return tmp.toString();
    }

    /**
     *
     * @param file
     *  A file reference which should be checked with the provided file name.
     * @param fileName
     *  The file name to be checked. Must NOT be null.
     *
     * @return
     */
    public static boolean hasSameFileName(final File file, final String fileName) {
       return (file != null) ? (file.getFileName().equals(fileName)) : false;
    }

    /**
     * Checks if the provided extension ends with a rescue extension
     * part.
     * @param extension an arbitrary extension with a possible rescue
     *  extension part
     * @return true if there is a rescue extension part or false if not
     */
    public static boolean isRescueExtension(String extension) {
	    return StringUtils.isEmpty(extension) ? false : extension.endsWith(STR_EXT_RESCUE);
    }

    /**
     * Removes an optional rescue extension part "_ox" from
     * the provided extension.
     *
     * @param extension an arbitrary extension with a possible rescue
     *  extension part
     * @return a string with rescue extension part or the unchanged
     * provided string
     */
    public static String removeRescueExtensionPart(String extension) {
	    if (StringUtils.isEmpty(extension))
	    	return extension;

        final int pos = extension.lastIndexOf(STR_EXT_RESCUE);
        return (pos >= 0) ? extension.substring(0, pos) : extension;
    }

    /**
     * Checks if the provided extension ends with a pgp extension
     * part.
     * @param extension an arbitrary extension with a possible pgp
     *  extension part
     * @return true if there is a pgp extension part or false if not
     */
    public static boolean isEncryptedExtension(String extension) {
        return StringUtils.isEmpty(extension) ? false : extension.endsWith(STR_EXT_PGP);
    }

}
