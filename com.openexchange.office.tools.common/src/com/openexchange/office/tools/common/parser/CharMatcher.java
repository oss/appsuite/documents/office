/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.parser;

import java.util.HashSet;
import java.util.Set;

public class CharMatcher {

    //-------------------------------------------------------------------------
	private CharMatcher() {}
	public static Set<String> findAllMatches(byte[] stream, byte[] pattern, char endChar, int reusePatternParts) {
		final HashSet<String> ids = new HashSet<String>();
		findAllMatches(stream, pattern, endChar, ids, reusePatternParts);
		return ids;
	}

	public static void findAllMatches(byte[] stream, byte[] pattern, char endChar, Set<String> ids, int reusePatternParts) {

		// searching and collecting each pattern
		int index = 0;
		do {
			index = indexOfBytes(stream, pattern, index);
			if (index != -1) {
				// we found a pattern
				index += pattern.length;
				int length = 0;
				while (index < stream.length && stream[index] != endChar) {
					length++;
					index++;
				}
				if (length != 0) {
					length += reusePatternParts;
					ids.add(new String(stream, index - length, length));
				}
			}
		} while (index != -1);
	}

	public static int indexOfBytes(byte[] array, byte[] pattern, int index) {
		int ident = 0;
		while (index < array.length) {
			ident = (array[index] == pattern[ident]) ? ++ident : 0;
			if (ident == pattern.length) {
				return index - ident + 1;
			}
			index++;
		}
		return -1;
	}
}
