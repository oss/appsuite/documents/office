/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.io.Serializable;

//=============================================================================
public interface IMap
{
	//-------------------------------------------------------------------------
	/** set a new or override an existing element.
	 * 
	 *  @param  sElement [IN]
	 *  		the name of the element.
	 *  		Has not to be null nor empty.
	 *
	 *  @param  aValue [IN]
	 *  		the value of that element.
	 *          Can be null.
	 */
	public < T extends Serializable > void set (final String sElement,
			             						final T      aValue  )
	    throws Exception;

	//-------------------------------------------------------------------------
	/** @return the current value of an existing element - null otherwise.
	 * 
	 *  @param  sElement [IN]
	 *  		the name of the element.
	 *  		Has not to be null nor empty.
	 */
	public < T extends Serializable > T get (final String sElement)
	    throws Exception;

	//-------------------------------------------------------------------------
	/** @return the current value of an existing element ...
	 * 			the defined default value otherwise.
	 * 
	 *  @param  sElement [IN]
	 *  		the name of the element.
	 *  		Has not to be null nor empty.
	 *  
	 *  @param  aDefault [IN]
	 *  		the default value for return in case
	 *  		element does not exists.
	 */
	public < T extends Serializable > T get (final String sElement,
			          						 final T      aDefault)
	    throws Exception;
}
