/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Thread-safe
 * {@link TimeZoneHelper} which caches already requested
 * TimeZone instances. The original TimeZone implementation uses
 * a synchronized for getTimeZone() which internally needs to read
 * time zone files, therefore this access can be expensive and limit
 * multi-threading performance.
 *
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 * @since v7.10.0
 */
@ThreadSafe
public class TimeZoneHelper {
    private static final ConcurrentHashMap<String, TimeZone> sharedTimeZoneMap = new ConcurrentHashMap<>();

    private TimeZoneHelper() {}

    /**
     * Gets the <code>TimeZone</code> for the given ID. The instances are
     * cached in an internal map for faster access.
     *
     * @param ID the ID for a <code>TimeZone</code>, either an abbreviation
     * such as "PST", a full name such as "America/Los_Angeles", or a custom
     * ID such as "GMT-8:00". Note that the support of abbreviations is
     * for JDK 1.1.x compatibility only and full names should be used.
     *
     * @return the specified <code>TimeZone</code>, or the GMT zone if the given ID
     * cannot be understood.
     */
     public static TimeZone getTimeZone(String timeZoneId) {
        TimeZone tz = sharedTimeZoneMap.get(timeZoneId);

        if (null == tz) {
            tz = TimeZone.getTimeZone(timeZoneId);
            sharedTimeZoneMap.putIfAbsent(timeZoneId, tz);
        }

        return tz;
    }
}
