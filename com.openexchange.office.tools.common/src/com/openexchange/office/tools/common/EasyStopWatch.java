/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import org.apache.commons.lang3.time.StopWatch;

/**
 * Small helper class wrapping the Apache StopWatch class with relaxed
 * methods.
 *
 * @author Carsten Driesner
 */
public class EasyStopWatch {

    private final StopWatch stopWatch;

    public EasyStopWatch() {
        stopWatch = new StopWatch();
    }

    /**
     * Starts the stop watch. Reset it, if called
     * twice and starts it again.
     */
    public void start() {
        try {
            stopWatch.start();
        } catch (IllegalStateException e) {
            stopWatch.reset();
            stopWatch.start();
        }
    }

    /**
     * Gets the elapsed time in mill seconds.
     *
     * @return the elapsed time
     */
    public long getTime() {
        long currTime = 0;

        try {
            currTime = stopWatch.getTime();
        } catch (IllegalStateException e) {

        }

        return currTime;
    }

    /**
     * Stops the stop watch and provides the elapsed
     * time in milliseconds.
     */
    public long stop() {
        long time = 0;
        try {
            time = stopWatch.getTime();
            stopWatch.reset();
        } catch (IllegalStateException e) {
            stopWatch.reset();
        }
        return time;
    }
}
