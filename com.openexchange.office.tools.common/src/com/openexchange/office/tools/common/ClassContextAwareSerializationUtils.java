/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;

//=============================================================================
/** Class serialization inside an OSGI environment can be some kind of pane.
 *
 *  A typical problem : on deserialization the wrong (default) class loader
 *  is used and is not able to load right class within it's context.
 *
 *  This helper solve the problem by hooking into the class loader hierarchy
 *  and select the right loader instead of using the default class loader.
 */
public class ClassContextAwareSerializationUtils
{
	//-------------------------------------------------------------------------
	/** Utility classes should not provide a way to create object instances
	 */
	private ClassContextAwareSerializationUtils ()
	{}

	//-------------------------------------------------------------------------
	/** serialize the given object to a base64 encoded string.
	 *
	 *  @param	aObject [IN]
	 *  		the object to be serialized here.
	 *  		Has not to be null.
	 *
	 *	@return the base64 encoded data set of those object.
	 */
	public static < T > String serializeObject (final T aObject)
	    throws Exception
	{
		Validate.notNull(aObject, "Object for serialization has not to be null.");

		final byte[] lRaw    = org.apache.commons.lang3.SerializationUtils.serialize((Serializable)aObject);
		final byte[] lBase64 = Base64.encodeBase64(lRaw);
		final String sData   = new String (lBase64);
		return sData;
	}

	//-------------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static < T > T deserializeObject (final String sData   ,
											 final Object aContext)
	    throws Exception
	{
		Validate.notEmpty(sData   , "Data for de-serialization has not to be empty."  );
		Validate.notNull (aContext, "Serialization context object has not to be null.");

		final byte[]            lBase64     = sData.getBytes();
		final byte[]            lRaw        = Base64.decodeBase64(lBase64);
		      InputStream       aRawData    = null;
		      ObjectInputStream aObjectData = null;

		try
		{
			aRawData    = new ByteArrayInputStream(lRaw);
			aObjectData = new ClassContextAwareObjectInputStream(aRawData, aContext);

			final Object aObject = aObjectData.readObject();
			return (T) aObject;
		}
		finally
		{
			IOUtils.closeQuietly(aObjectData);
			IOUtils.closeQuietly(aRawData   );
		}
	}
}
