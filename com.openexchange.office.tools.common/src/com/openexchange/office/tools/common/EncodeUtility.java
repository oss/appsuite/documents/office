/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * Utility class to provide encoding for different purposes.
 *
 * @author <a href="mailto:carsten.diesner@open-xchange.com">Carsten Driesner</a>
 * @since 7.10.3
 *
 */
public class EncodeUtility {

    private static final Pattern PATTERN_DOT = Pattern.compile("\\.");

    private EncodeUtility() {
        super();
    }

    /**
     * Encodes given string to be used as a part or completely
     * for a file name.
     *
     * @param a string to be used for a file name which must be encoded
     * @return The encoded part for a file name
     */
    public static String encodeForFileName(String textToBeEncoded) {
        if (StringUtils.isEmpty(textToBeEncoded)) {
            return textToBeEncoded;
        }

        try {
            String encoded = URLEncoder.encode(textToBeEncoded, "UTF-8");
            return PATTERN_DOT.matcher(encoded).replaceAll("%2E");
        } catch (UnsupportedEncodingException e) {
            // Cannot occur
            return textToBeEncoded;
        }
    }

}
