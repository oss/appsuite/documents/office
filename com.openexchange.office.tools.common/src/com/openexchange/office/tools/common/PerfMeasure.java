/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;
import org.apache.commons.io.IOUtils;

/**
 * {@link PerfMeasure}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
/**
 * {@link PerfMeasure}
 *
 * @author <a href="mailto:firstname.lastname@open-xchange.com">Firstname Lastname</a>
 */
public class PerfMeasure {

    final private String perfMeasureDirectory = System.getenv("OX_OFFICE_PERFORMANCEDIR");

    protected BufferedWriter writer = null;

    final HashMap<String, Vector<Long>> timeMap = new HashMap<String, Vector<Long>>();

    // -------------------------------------------------------------------------

    /**
     * Initializes a new {@link PerfMeasure}.
     *
     * @param perfMeasureOutputFileName
     */
    public PerfMeasure(final String perfMeasureOutputFileName) {
        if (null != perfMeasureDirectory) {
            final File outputFile = new File(perfMeasureDirectory, perfMeasureOutputFileName);

            if (outputFile.getParentFile().canWrite() || outputFile.getParentFile().mkdirs() ) {
                try {
                    writer = new BufferedWriter(new FileWriter(outputFile, true));
                } catch (IOException e) {
                    //
                }
            }
        }
    }

    /**
     * @return true if PreviewPerf has been set up
     */
    final public boolean isValid() {
        return (null != writer);
    }

    /**
     * @param key
     */
    final public void init(final String key) {
        this.init(key, 1);
    }

    /**
     * @param key
     * @param repeatCount
     */
    final public void init(final String key, int repeatCount) {
        if (isValid() && (0 < repeatCount)) {
            final Vector<Long> values = new Vector<Long>(repeatCount + 2);

            // create a new map entry with the given key
            timeMap.put(key, values);

            // value #0 contains the current repeat count for further iterations
            values.add(new Long(repeatCount));
        }
    }

    /**
     * @param key
     */
    final public void reset(final String key) {
        if (isValid()) {
            final Vector<Long> values = timeMap.get(key);

            if (null != values ) {
                values.add(Long.valueOf(System.nanoTime()));
            }
        }
    }

    /**
     * @param key
     */
    final public boolean repeat(final String key) {
        return this.repeat(key, false);
    }

    /**
     * @param key
     * @param close if set to true, the underlying output stream will be
     *              closed if all iterations have been finished and the
     *              final measurement string has been written and flushed;
     *              if false, the user of this class needs to ensure to
     *              explicitly call PerfMeasure.close at a later point
     *              of time. This is e.g. the case, if more than one key
     *              is used for making different measurements with one
     *              instance of this class
     */
    final public boolean repeat(final String key, final boolean close) {

        final long endTime = System.nanoTime();
        boolean repeatAgain = false;

        if (isValid() ) {
            final Vector<Long> values = timeMap.get(key);

            if (null != values) {
                // calculate current run time in milliseconds and store in values vector
                values.set(values.size() - 1, Long.valueOf((endTime - values.lastElement().longValue()) / 1000000));

                // check if further iterations are needed;
                // in case of no further iterations, a measurement
                // string with single iteration run times and the
                // mean time of all measuremnts as last value is created
                // and flushed to the output stream
                final long repeatCount = values.get(0).longValue();
                repeatAgain = (repeatCount > 1);

                if (repeatAgain) {
                    values.set(0, new Long(repeatCount - 1));
                } else if (null != writer) {
                    final StringBuffer measureStringBuf = new StringBuffer(key);
                    long totalTime = 0;

                    // add single measurements, separated by a comma, to the string
                    for (int i = 1; i < values.size(); ++i) {
                        final long curValue = values.get(i).longValue();

                        measureStringBuf.append(',');
                        measureStringBuf.append(String.valueOf(curValue));

                        totalTime += curValue;
                    }

                    // add mean value of measured times to the end of
                    // the string with an additonal comma as separator
                    if (values.size() > 1) {
                        measureStringBuf.append(",,");
                        measureStringBuf.append(String.valueOf((long) ((double) totalTime / ((values.size() - 1)))));
                    }

                    try {
                        writer.write(measureStringBuf.toString());
                        writer.newLine();
                        writer.flush();
                    } catch (IOException e) {
                        //
                    }

                    if (close) {
                        this.close();
                    }
                }
            }
        }

        return repeatAgain;
    }

    /**
     */
    final public void close() {
        if (null != writer) {
            IOUtils.closeQuietly(writer);
            writer = null;
        }
    }
}
