/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.office.tools.common.osgi.context;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.Set;
import java.util.stream.Collectors;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.java.ConcurrentSet;

public class OptionalServiceListener implements ServiceListener {
    private static final long CLEANUP_FREQEUNCY = 120000;
    private static final String CLEANUP_THREAD_NAME = "OptionalServiceListener-CleanupThread";

    private static Thread cleanupThread;
    private static final ConcurrentSet<OptionalServiceListener> listeners = new ConcurrentSet<>();

    private final Logger log = LoggerFactory.getLogger(OptionalServiceListener.class);

    private final Field field;

    // DOCS-2767 don't use a hard-ref here that keeps non-used instance alive
    private final WeakReference<Object> serviceWithDepencyToField;

    static {
        cleanupThread = new Thread(new Runnable() {

            @Override
            public void run() {
                boolean interrupted = false;
                while (!interrupted) {
                    try {
                        Thread.sleep(CLEANUP_FREQEUNCY);
                        Set<OptionalServiceListener> canBeRemoved = listeners.stream()
                                 .filter(l -> isServiceWithDependencyNotAlive(l.serviceWithDepencyToField))
                                 .collect(Collectors.toSet());
                        canBeRemoved.stream().forEach(l -> {
                            Class<?> fieldClass = l.field.getType();
                            BundleContext bundleContext = FrameworkUtil.getBundle(fieldClass).getBundleContext();
                            if (bundleContext != null) {
                                bundleContext.removeServiceListener(l);
                            }
                        });
                        listeners.removeAll(canBeRemoved);
                    } catch (InterruptedException e) {
                        interrupted = true;
                        Thread.currentThread().interrupt();
                    }
                }
            }
        });
        cleanupThread.setDaemon(true);
        cleanupThread.setName(CLEANUP_THREAD_NAME);
        cleanupThread.start();
    }

    public OptionalServiceListener(Field field, Object serviceWithDepencyToField) {
        this.field = field;
        this.serviceWithDepencyToField = new WeakReference<>(serviceWithDepencyToField);
        listeners.add(this);
    }

    @Override
    public void serviceChanged(ServiceEvent serviceEvent) {
        log.debug("Received serviceEvent {}", serviceEvent);
        if (serviceEvent.getType() == ServiceEvent.REGISTERED) {
            Bundle bundle = serviceEvent.getServiceReference().getBundle();
            BundleContext bundleContext = bundle.getBundleContext();
            Object service = bundleContext.getService(serviceEvent.getServiceReference());
            log.info("Service with class {} was registered", service.getClass().getName());
            try {
                Object ref = serviceWithDepencyToField.get();
                if (ref != null) {
                    field.set(serviceWithDepencyToField, service);
                    bundleContext.removeServiceListener(this);
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                log.error(e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }
    }

    private static boolean isServiceWithDependencyNotAlive(WeakReference<Object> obj) {
        return (obj == null) ? true : (obj.get() == null);
    }
}
