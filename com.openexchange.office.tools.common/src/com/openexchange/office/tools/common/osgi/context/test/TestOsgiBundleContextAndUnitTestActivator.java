/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.osgi.context.test;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.ajax.requesthandler.AJAXActionServiceFactory;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.office.tools.annotation.ConcreteService;
import com.openexchange.office.tools.common.osgi.context.IOsgiBundleContextIdentificator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;

public abstract class TestOsgiBundleContextAndUnitTestActivator extends OsgiBundleContextAndActivator {	
	
	private static final Logger log = LoggerFactory.getLogger(TestOsgiBundleContextAndUnitTestActivator.class);
	
	private final UUID hazelcastNodeId = UUID.randomUUID();
	
	protected final UnittestInformation unittestInformation;
	
	private Map<Class<?>, Object> mocks = new HashMap<>();
	
	public TestOsgiBundleContextAndUnitTestActivator(IOsgiBundleContextIdentificator bundleIdentificator, UnittestInformation unittestInformation) {
		super(bundleIdentificator);		
		this.unittestInformation = unittestInformation;
		this.unittestInformation.setTestOsgiBundleContextAndUnitTestActivator(this);		
	}

	protected void initConfigService() {
		unittestInformation.initConfigService();
	}
	
	protected Collection<Class<?>> getClassesToMock() {
		return new HashSet<>(unittestInformation.getClassesToMock());
	}

	@SuppressWarnings("unused")
    protected void initMocksBeforRegisteringAdditionalServices(Map<Class<?>, Object> createdMocks) {
	    unittestInformation.initMocksBeforRegisteringAdditionalServices(createdMocks);
	}
		
	protected void initMocks() {
		unittestInformation.initMocks();
	}

	protected Collection<Class<?>> getClassesToInstantiateForUnitttest() {
		return unittestInformation.getClassesToInstantiateForUnitttest();
	}
	
	public UUID getHazelcastNodeId() {
		return hazelcastNodeId;
	}
	
	@Override
	public void start() throws OXException {
		createMocks();
		initMocksBeforRegisteringAdditionalServices(mocks);
		unittestInformation.getAdditionalServices().forEach(s -> this.registerService(s, false));		
		initMocks();
		initConfigService();
		createInstances();
		afterCreatedServices();
        injectValues();        
        injectDependencies();
        injectApplicationContext();
        callPostContruct();        	
        registerAllListenersToEventServices();
        registerManagementObjects();		
	}

	protected void afterCreatedServices() {}
	
	private void createMocks() {
		Collection<Class<?>> col = getClassesToMock();
		col.add(ConfigurationService.class);
		col.forEach(c -> mocks.put(c, createMock(c)));
	}
		
	protected abstract Object createMock(Class<?> clazz);
	
	@Override
	public ConfigurationService getConfigService() {
		return getMock(ConfigurationService.class);
	}
	
	@Override
	protected <T> boolean hasService(Class<T> clazz) {
		if (mocks.containsKey(clazz)) {
			return true;
		}
		return super.hasService(clazz);
	}	
	
	@Override
	protected <S> boolean removeService(Class<? extends S> clazz) {
		boolean res = mocks.remove(clazz) != null;
		if (!res) {
			return super.removeService(clazz);
		}
		return res;
	}

	@Override
	protected <T> T findService(Class<T> clazz, boolean optional) {
		Object res = mocks.get(clazz);
		if (res == null) {
			return super.findService(clazz, optional);
		}
		return clazz.cast(res);
	}

	public <T> T getMock(Class<T> clazz) {
		Object obj = mocks.get(clazz);
		if (obj == null) {
			throw new NullPointerException("Cannot find mock for class " + clazz.getName());
		}
		T res = clazz.cast(obj);
		return res;
	}
	
	@Override
	public void createInstances() throws OXException {
		Collection<Class<?>> col = getClassesToInstantiateForUnitttest();
		for (Class<?> clazz : col) {
			try {
				Object obj;
				ConcreteService concreteService = clazz.getAnnotation(ConcreteService.class);
				if (concreteService != null) {
					obj = concreteService.clazz().newInstance();
				} else {
					obj = clazz.newInstance();
				}					
				registerService(obj, false);
			} catch (InstantiationException | IllegalAccessException e) {
				log.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}	    				
		}
	}

	//----------------------------------------------------------------------------------------------------------------------------
	@Override
	protected void registerServicesToOsgi() {
		// Not needed in Unittests
	}

	public void registerServiceToOsgi(Object service, Class<?> clazzToRegister) {
		// Not needed in Unittests
	}
	
	@Override
	protected void startBundle() throws Exception {
		// Not needed in Unittests
	}

	@Override
	protected void stopBundle() throws Exception {
		// Not needed in Unittests
	}

	public void registerAjaxActionServiceFactoryToOSGI(AJAXActionServiceFactory ajaxActionServiceFactory, String moduleName, boolean multiple) {
		// Not needed in Unittests
	}
	
	protected <T> T getExternalDependency(Class<T> serviceClass, Class<?> owner, boolean optional) {
		// Not needed in Unittests
		return null;
	}

	@Override
	public void shutdown() throws OXException {
		// Not needed in Unittests
	}

	@Override
	public void startScheduledExecutorServices() {
		// Not needed in Unittests
	}

	@Override
	public void startScheduledExecutorServices(Object... objects) {
		// Not needed in Unittests
	}

	@Override
	public void stopScheduledExecutorServices() {
		// Not needed in Unittests
	}
}
