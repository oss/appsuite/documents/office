/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.osgi.context.test;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import com.openexchange.config.ConfigurationService;

public abstract class UnittestInformation {

	protected ConfigurationService configurationService;
	protected TestOsgiBundleContextAndUnitTestActivator testOsgiBundleContextAndUnitTestActivator;
	
	protected abstract Collection<Class<?>> getClassesToMock();
	
	protected void setTestOsgiBundleContextAndUnitTestActivator(TestOsgiBundleContextAndUnitTestActivator testOsgiBundleContextAndUnitTestActivator) {
		this.testOsgiBundleContextAndUnitTestActivator = testOsgiBundleContextAndUnitTestActivator;
	}

	public ConfigurationService initConfigService() {
		if (configurationService == null) {
			configurationService = testOsgiBundleContextAndUnitTestActivator.getMock(ConfigurationService.class);
		}
		return configurationService;
	}

    @SuppressWarnings("unused")
    protected void initMocksBeforRegisteringAdditionalServices(Map<Class<?>, Object> mocks) {
        // Should be implemented by sub-class whenever mocked classes are used by
        // the registered services in the test. Especially if the registered classes
        // call methods of the mocked classes in their afterPropertiesSet() method.
        // See INF-127
    }

	protected void initMocks() {
	    // can be implemented by sub-class
	}

	protected Collection<Class<?>> getClassesToInstantiateForUnitttest() {
		return Collections.emptySet();
	}
	
	protected Collection<Object> getAdditionalServices() {
		return Collections.emptySet();
	}
}
