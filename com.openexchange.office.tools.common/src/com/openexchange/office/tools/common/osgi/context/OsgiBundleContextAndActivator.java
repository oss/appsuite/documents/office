/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.osgi.context;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.wiring.BundleWiring;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.MBeanExportConfiguration.SpecificPlatform;
import org.springframework.core.ResolvableType;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.jmx.support.MBeanServerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;
import com.openexchange.ajax.requesthandler.AJAXActionServiceFactory;
import com.openexchange.config.ConfigurationService;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.exception.OXException;
import com.openexchange.office.tools.annotation.Async;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.annotation.ShutdownOrder;
import com.openexchange.office.tools.common.exception.ValueNotFoundException;
import com.openexchange.office.tools.common.http.HttpModelConverter;
import com.openexchange.office.tools.common.http.HttpModelConverterRegistry;
import com.openexchange.office.tools.common.jms.JmsMessageListener;
import com.openexchange.office.tools.common.threading.ThreadFactoryBuilder;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.server.ServiceLookup;
import com.udojava.jmx.wrapper.JMXBean;
import com.udojava.jmx.wrapper.JMXBeanWrapper;

public abstract class OsgiBundleContextAndActivator extends HousekeepingActivator implements ServiceLookup, ListableBeanFactory {

	public static final String IN_UNIT_TEST = "IN_UNIT_TEST";

	protected List<Object> services = new ArrayList<>();

	private Set<ServiceRegistration<?>> serviceRegistrations = new HashSet<>();

	protected Set<Class<?>> neededServices = null;
	private Set<Class<?>> optionalServices = null;

	private Set<Class<?>> classesOfBundle = null;

	private List<ScheduledExecutorService> scheduledExecutorServices = new ArrayList<>();

	private final IOsgiBundleContextIdentificator bundleIdentificator;

	private Map<String, Class<?>> classNameToClassInstanceMapping = new HashMap<>();

	private final AtomicBoolean stopped = new AtomicBoolean(true);

	private Logger log = null;

	private boolean loggedNeededServices = false;

	private boolean bundleContainsNoServices = false;

	protected boolean logEntry(String entry) {
		return true;
	}

	protected Logger getLogger() {
		if (log == null) {
			log = LoggerFactory.getLogger(this.getClass());
		}
		return log;
	}

	protected OsgiBundleContextAndActivator(IOsgiBundleContextIdentificator bundleIdentificator) {
		this.bundleIdentificator = bundleIdentificator;
	}

	protected OsgiBundleContextAndActivator(IOsgiBundleContextIdentificator bundleIdentificator, boolean bundleContainsNoServices) {
		this.bundleIdentificator = bundleIdentificator;
		this.bundleContainsNoServices = bundleContainsNoServices;
	}

	protected String getBundleId() {
		return bundleIdentificator.getName();
	}

	@Override
	protected Class<?>[] getNeededServices() {
		initNeededAndOptionalServices();
		neededServices.addAll(getAdditionalNeededServices());
		Class<?>[] res = neededServices.toArray(new Class<?>[0]);
		if (!loggedNeededServices) {
		    String packageName = "com.openexchange.";
		    Set<String> logSet =
		    		neededServices.stream()
		    				.map(c -> c.getName())
		    				.map(s -> s.startsWith(packageName) ? s.substring(packageName.length()) : s)
		    				.collect(Collectors.toSet());
			getLogger().info("Needed services of bundle with id " + getBundleId() + ": " + Arrays.toString(logSet.toArray()));
			loggedNeededServices = true;
		}
		return res;
	}

	protected Set<Class<?>> getServicesOfModuleWithForeignPackage() {
		return Collections.emptySet();
	}

	protected Set<Class<?>> getAdditionalNeededServices() {
		return Collections.emptySet();
	}

	@Override
    protected Class<?>[] getOptionalServices() {
		initNeededAndOptionalServices();
		return optionalServices.toArray(new Class<?>[0]);
	}

	protected void initNeededAndOptionalServices() {
	    try {
	        if ((neededServices == null) || (optionalServices == null)) {
	            neededServices = new HashSet<>();
	            neededServices.add(ConfigurationService.class);
	            optionalServices = new HashSet<>();
	            getClassesOfBundle();
	            classesOfBundle.forEach(c -> {
	                getDependenciesOfClass(c, true);
	            });
	            getAdditionalDependenciesOfClasses().forEach(c -> {
	                getDependenciesOfClass(c, false);
	            });
	            Set<Class<?>> servicesOfModuleWithForeignPackage = getServicesOfModuleWithForeignPackage();
	            neededServices.removeAll(servicesOfModuleWithForeignPackage);
	            optionalServices.removeAll(neededServices);
	        }
	    } catch (Throwable t) {
	        getLogger().error("Exception caught trying to determine needed and optional services for bundle: " + this.getBundleId(), t);
	        throw t;
	    }
	}

	protected Set<Class<?>> getClassesOfBundle() {
		if (classesOfBundle == null) {
			classesOfBundle = new HashSet<>();
			final Bundle bundle = FrameworkUtil.getBundle(bundleIdentificator.getClass());
			String topLevelPackage = bundleIdentificator.getClass().getPackage().getName().substring(0, bundleIdentificator.getClass().getPackage().getName().length() - 5);

		    BundleWiring bundleWiring = bundle.adapt(BundleWiring.class);

		    // Getting all the class files (also from imported packages)
		    Set<String> resources = new HashSet<>();
		    resources.addAll(bundleWiring.listResources("/", "*.class", BundleWiring.LISTRESOURCES_RECURSE));
		    getLogger().debug("Classes of bundle with LISTRESOURCES_RECURSE: {}", bundleWiring.listResources("/", "*.class", BundleWiring.LISTRESOURCES_RECURSE));
		    resources.addAll(bundleWiring.listResources("/", "*.class", BundleWiring.FINDENTRIES_RECURSE));
		    getLogger().debug("Classes of bundle with FINDENTRIES_RECURSE: {}", bundleWiring.listResources("/", "*.class", BundleWiring.FINDENTRIES_RECURSE));
		    resources.addAll(bundleWiring.listResources("/", "*.class", BundleWiring.LISTRESOURCES_LOCAL));
		    getLogger().debug("Classes of bundle with LISTRESOURCES_LOCAL: {}", bundleWiring.listResources("/", "*.class", BundleWiring.LISTRESOURCES_LOCAL));
		    getLogger().debug("All classes of bundle with class path scanning: " + Arrays.toString(resources.toArray()));
		    resources = resources.stream().
		    		map(str -> generateNormalClassNameOfOsgiClassName(str)).
		    		filter(str -> str.startsWith(topLevelPackage)).collect(Collectors.toSet());
	    	getLogger().info("Classes of bundle {} with class path scanning: {}", bundle.getSymbolicName(), reduceLoggingEntriesByMainPackageName(resources));
		    for (String className : resources) {
	    		try {
	    			Class<?> clazz = bundleWiring.getClassLoader().loadClass(className);
	    			classNameToClassInstanceMapping.put(className, clazz);
	    			classesOfBundle.add(clazz);
	    		} catch (ClassNotFoundException e) {
	    			getLogger().error(e.getMessage(), e);
	    			throw new RuntimeException(e);
	    		}
		    }
		    if (classesOfBundle.isEmpty()) {
		    	getLogger().warn("Classes of bundle {} is empty!", getBundleId());
		    } else {
		    	getLogger().debug("Classes of bundle {}: {}", getBundleId(), classesOfBundle);
		    }

		}
	    return classesOfBundle;
	}

	private Collection<String> reduceLoggingEntriesByMainPackageName(Collection<String> toReduce) {
	    String packageNameWithOsgi = bundleIdentificator.getClass().getPackage().getName();
	    String packageName = packageNameWithOsgi.substring(0, packageNameWithOsgi.length() - 4);
	    Set<String> logSet =
	    		toReduce.stream()
	    				.filter(s -> logEntry(s) && !s.startsWith("com.openexchange.office.tools.service.validation."))
	    				.map(s -> s.startsWith(packageName) ? s.substring(packageName.length()) : s)
	    				.collect(Collectors.toSet());
	    return logSet;
	}

	private static String generateNormalClassNameOfOsgiClassName(String osgiClassname) {
		return osgiClassname.replaceAll("\\.class$", "").replace('/', '.').replaceAll("bin\\.", "");
	}

	public Collection<Class<?>> getMandantoryDependenciesOfClass(Class<?> clazz) {
		Collection<Class<?>> res = new HashSet<>();
		if ((clazz != null) && !clazz.equals(Object.class)) {
			Field [] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);
				Class<?> fieldClass = field.getType();
				Autowired autowiredAnno = field.getAnnotation(Autowired.class);
				if (autowiredAnno != null) {
					if (autowiredAnno.required()) {
						res.add(fieldClass);
					}
				}
			}
			res.addAll(getMandantoryDependenciesOfClass(clazz.getSuperclass()));
		}
		return res;
	}

	protected void getDependenciesOfClass(Class<?> clazz, boolean isSingletonService) {
		if (isSingletonService && (clazz.getAnnotation(RestController.class) == null) && (clazz.getAnnotation(Service.class) == null) && (clazz.getAnnotation(org.springframework.stereotype.Component.class) == null)) {
			return;
		}
		if ((clazz != null) && !clazz.equals(Object.class)) {
		    try {
	            Field [] fields = clazz.getDeclaredFields();
	            for (Field field : fields) {
	                field.setAccessible(true);
	                Class<?> fieldClass = field.getType();
	                if (!classesOfBundle.contains(fieldClass)) {
	                    Autowired autowiredAnno = field.getAnnotation(Autowired.class);
	                    if (autowiredAnno != null) {
	                        if (autowiredAnno.required()) {
	                            neededServices.add(fieldClass);
	                        } else {
	                            optionalServices.add(fieldClass);
	                        }
	                    }
	                }
	            }
	            getDependenciesOfClass(clazz.getSuperclass(), false);
		    } catch (Throwable t) {
                ExceptionUtils.handleThrowable(t);
                getLogger().error("Exception caught trying to get dependencies for class {}", clazz.getCanonicalName(), t);
		    }
		}
	}

	protected Set<Class<?>> getAdditionalDependenciesOfClasses() {
		return Collections.emptySet();
	}

	@Override
	protected void handleUnavailability(Class<?> clazz) {
	    getLogger().info("{} is handling unavailibility of needed service {}. Going to stop bundle.", getBundleId(), clazz.getSimpleName());

	    try {
	        stopBundle();
	    } catch (Exception e) {
	        getLogger().error("Error while stopping bundle.", e);
	    }
	}

	@Override
	protected void handleAvailability(Class<?> clazz) {
	    if (allAvailable()) {
	        getLogger().debug("{} regained needed service {}. Going to start bundle.", getBundleId(), clazz.getSimpleName());

	        try {
	            startBundle();
	        } catch (Exception e) {
	            getLogger().error("Error while starting bundle.", e);
	        }
	    }
	}

	@Override
	protected void startBundle() throws Exception {
		if (stopped.compareAndSet(true, false)) {
		    getLogger().info("activate bundle with id {} ...", getBundleId());
		    try {
		    	start();
		        getLogger().info("Bundle with id {} activated", getBundleId());
		    } catch (Throwable e) {
		    	ExceptionUtils.handleThrowable(e);
		        getLogger().error("Bundle with id {} caught exception on start-up, it's possible that bundle with id {} is not correctly working, please check your configuration & set-up!", getBundleId(), getBundleId(), e);
		    }
		}
	}

	@Override
	protected void stopBundle() throws Exception {
		if (stopped.compareAndSet(false, true)) {
		    getLogger().info("Deactivate bundle with id {} ...", getBundleId());
			shutdown();
		    getLogger().info("Bundle with id {} deactivated.", getBundleId());
		}
	}

	public boolean isStopped() {
		return stopped.get();
	}

	public void registerAllListenersToEventServices() {}

	public void registerServiceToEventServices(Object service) {}

	public void start() throws OXException {
		createInstances();
        injectValues();
        injectDependencies();
        injectApplicationContext();
        callPostContruct();
        registerHttpConverter();
        registerAllListenersToEventServices();
        registerManagementObjects();
        startScheduledExecutorServices();
        registerServicesToOsgi();
        startJmsMessageReceivers();
	}

	public void startJmsMessageReceivers() {
		for (Object obj : services) {
			if (obj instanceof JmsMessageListener) {
				JmsMessageListener jmsMessageListener = (JmsMessageListener) obj;
				jmsMessageListener.startReceiveMessages();
			}
		}
	}

	public void prepareObject(Object obj) {
		injectValues(obj);
		injectDependencies(obj);
		injectApplicationContext(obj);
		callPostContruct(obj);
		startScheduledExecutorServices(obj);
	}

	public void shutdown() throws OXException {
    	stopScheduledExecutorServices();
        callPreDestroy();
        unregisterServicesFromOsgi();
	}

	private void unregisterServicesFromOsgi() {
		serviceRegistrations.forEach(r -> r.unregister());
	}

	public boolean registerModuleForeignService(Object service) {
		boolean res = registerService(service, false);
		classNameToClassInstanceMapping.put(service.getClass().getName(), service.getClass());
		return res;
	}

	public boolean registerService(Object service, boolean doInject) {
		boolean inUnitTestPreparation = Boolean.valueOf(System.getProperty(IN_UNIT_TEST, Boolean.FALSE.toString()));
		if (inUnitTestPreparation) {
			if (hasService(service.getClass())) {
				return false;
			}
		} else {
			if (hasService(service.getClass())) {
				throw new IllegalArgumentException("A service with class " + service.getClass() + " was already registered!, registered services: " + services);
			}
		}
		return doRegisterService(service, doInject);
	}

	@Override
    public void unregisterService(Object service) {
	    if (hasService(service.getClass())) {
	        services.remove(service);
	    }
	}

	private boolean doRegisterService(Object service, boolean doInject) {
		boolean res = this.services.add(service);
		if (doInject) {
			injectValues(service);
			injectDependencies(service);
			callPostContruct(service);
			registerServiceToEventServices(service);
		}
		return res;
	}

	protected <T> boolean hasService(Class<T> clazz) {
		for (Object obj : services) {
			if (clazz.isInstance(obj)) {
				return true;
			}
		}
		T res = getExternalDependency(clazz, null, true);
		return res != null;

	}

	@SuppressWarnings("unused")
    protected void onAfterCreateInstances() throws OXException {
	    // can be implemented by sub-class
	}

	@SuppressWarnings("unused")
    protected void onPreCreateInstances() throws OXException {
	    // can be implemented by sub-class
	}

	public void createInstances() throws OXException {
		getLogger().debug("create instances of bundle");
		onPreCreateInstances();

		Set<String> foundServices = new HashSet<>();
		Set<Class<?>> classes = getClassesOfBundle();

		for (Class<?> clazz : classes) {
    		if ((clazz.getAnnotation(RestController.class) != null) || (clazz.getAnnotation(Service.class) != null) || (clazz.getAnnotation(org.springframework.stereotype.Component.class) != null)) {
    			ConditionalOnProperty conditionalOnProperty = clazz.getAnnotation(ConditionalOnProperty.class);
    			boolean create = true;
				if (conditionalOnProperty != null) {
					ConfigurationService configurationService = getConfigService();
					String value = configurationService.getProperty(conditionalOnProperty.name()[0], "");
					if ("".equals(value)) {
						create = conditionalOnProperty.matchIfMissing();
					} else {
						create = value.equals(conditionalOnProperty.value()[0]);
					}
				}
				if (create) {
    				try {
						Object obj = clazz.newInstance();
						registerService(obj, false);
						foundServices.add(clazz.getName());
					} catch (InstantiationException | IllegalAccessException e) {
						getLogger().error(e.getMessage(), e);
						throw new RuntimeException(e);
					}
				}
			}
	    }
	    if (foundServices.isEmpty() && !bundleContainsNoServices) {
	    	getLogger().warn("No services in bundle {} found!", getBundleId());
	    } else {
	    	Collection<String> logSet = reduceLoggingEntriesByMainPackageName(foundServices);
	    	getLogger().debug("Found services: {}", Arrays.toString(logSet.toArray()));
	    }
	    onAfterCreateInstances();
	}

	public void startScheduledExecutorServices() {
		getLogger().debug("Starting scheduled executer services");
		startScheduledExecutorServices(services.toArray());
	}

	public void startScheduledExecutorServices(Object...objects) {
		Set<String> foundAsyncServices = new HashSet<>();
		ConfigurationService configurationService = getConfigService();
		for (Object obj : objects) {
			if (obj instanceof Runnable) {
				Runnable runnable = (Runnable) obj;
				Async asyncAnnotation = obj.getClass().getAnnotation(Async.class);
				if (asyncAnnotation != null) {
					foundAsyncServices.add(obj.getClass().getName());
					getLogger().debug("Found async service: {}", obj.getClass().getName());
					ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1, new ThreadFactoryBuilder(obj.getClass().getSimpleName() + "-%d").build());
					scheduledExecutorServices.add(scheduledExecutorService);
					long initDelay = asyncAnnotation.initialDelay();
					long period = asyncAnnotation.period();
					if (StringUtils.isNotBlank(asyncAnnotation.configNameInitialDelay())) {
						initDelay = configurationService.getIntProperty(asyncAnnotation.configNameInitialDelay(), (int) initDelay);
					}
					if (StringUtils.isNoneBlank(asyncAnnotation.configNameInitialPeriod())) {
						period = configurationService.getIntProperty(asyncAnnotation.configNameInitialPeriod(), (int) period);
					}
					scheduledExecutorService.scheduleAtFixedRate(runnable, initDelay, period, asyncAnnotation.timeUnit());
				}
			}
		}
		if (!foundAsyncServices.isEmpty()) {
			Collection<String> toLog = reduceLoggingEntriesByMainPackageName(foundAsyncServices);
			getLogger().info("Found async services: {}", Arrays.toString(toLog.toArray()));
		}
	}

	public void stopScheduledExecutorServices() {
		scheduledExecutorServices.forEach(s -> s.shutdown());
	}

	public void injectApplicationContext() {
		getLogger().debug("Inject applicationcontext into services");
		injectApplicationContext(services.toArray());
	}

	public void injectApplicationContext(Object...objects) {
		for (Object obj : objects) {
			if (obj instanceof OsgiBundleContextAware) {
				((OsgiBundleContextAware) obj).setApplicationContext(this);
			}
		}
	}

	public void injectValues() {
		getLogger().debug("Inject values into services");
		injectValues(services.toArray());
	}

	public void injectValues(Object...objects) {
		ConfigurationService configurationService = getConfigService();
		for (Object obj : objects) {
			Class<?> clazz = obj.getClass();
			while (!clazz.equals(Object.class)) {
				Field [] fields = clazz.getDeclaredFields();
				for (Field  field : fields) {
					Value valueAnno = field.getAnnotation(Value.class);
					if (valueAnno != null) {
						field.setAccessible(true);
						Class<?> fieldClass = field.getType();

				    	String annoValue = valueAnno.value().substring(2, valueAnno.value().length() - 1);
				    	if (!valueAnno.value().startsWith("${") || !valueAnno.value().endsWith("}")) {
				    		getLogger().error("Wrong value annotation syntax in class {} and field {}", clazz.getName(), field.getName());
				    		throw new IllegalArgumentException("Wrong value annotation syntax");
				    	}
				    	String key;
				    	String def = null;
				    	if (annoValue.contains(":")) {
				    		key = annoValue.substring(0, annoValue.indexOf(":")).trim();
				    		def = annoValue.substring(annoValue.indexOf(":") + 1).trim();
				    	} else {
				    		key = annoValue.trim();
				    	}
						Object value = null;
						switch (fieldClass.getName()) {
							case "java.lang.String":
								value = configurationService.getProperty(key, def);
								break;
							case "int":
							case "java.lang.Integer":
							case "java.lang.Long":
								if (def == null) {
									value = configurationService.getIntProperty(key, Integer.MIN_VALUE);
								} else {
									value = configurationService.getIntProperty(key, Integer.parseInt(def));
								}
								if (value.equals(Integer.MIN_VALUE)) {
									throw new ValueNotFoundException(key);
								}
								break;
							case "boolean":
							case "java.lang.Boolean":
								String tmpBoolValue;
								if (def == null) {
									tmpBoolValue = configurationService.getProperty(key, "");
								} else {
									tmpBoolValue = configurationService.getProperty(key, def);
								}
								if (tmpBoolValue.equals("")) {
									throw new ValueNotFoundException(key);
								}
								value = Boolean.valueOf(tmpBoolValue);
								break;
							default: throw new RuntimeException("@Value for type " + fieldClass.getName() + " not implemented yet!");
						}

						try {
							field.set(obj, value);
						} catch (IllegalArgumentException | IllegalAccessException e) {
							getLogger().error(e.getMessage(), e);
							throw new RuntimeException(e);
						}
					}
				}
				clazz = clazz.getSuperclass();
			}
		}
	}

	public void injectDependencies() {
		getLogger().debug("Inject dependencies...");
		injectDependencies(services.toArray());
	}

	public void injectDependencies(Object...objects) {
		Set<String> optionalServiceListeners = new HashSet<>();
		for (Object obj : objects) {
			Class<?> clazz = obj.getClass();
			while (!clazz.equals(Object.class)) {
				Field [] fields = clazz.getDeclaredFields();
				for (Field  field : fields) {
					Autowired autowiredAnno = field.getAnnotation(Autowired.class);
					if (autowiredAnno != null) {
						Class<?> fieldClass = field.getType();
						Object service = findService(fieldClass, !autowiredAnno.required());
						if (autowiredAnno.required()) {
							if (service == null) {
								throw new IllegalArgumentException("No service with class " + fieldClass + " found!");
							}
						}
						field.setAccessible(true);
						if (service != null) {
							try {
								field.set(obj, service);
							} catch (IllegalArgumentException | IllegalAccessException e) {
								getLogger().error(e.getMessage(), e);
								throw new RuntimeException(e);
							}
						} else {
							boolean inUnitTestPrep = Boolean.valueOf(System.getProperty(OsgiBundleContextAndActivator.IN_UNIT_TEST));
							if (!inUnitTestPrep) {
								OptionalServiceListener optionalServiceListener = new OptionalServiceListener(field, obj);
								String filter = "(objectclass=" + fieldClass.getName() + ")";
								log.debug("Registering optional service listener for Object {} with field {} and filter {}.", obj.getClass().getName(), field.getName(), filter);
								optionalServiceListeners.add(obj.getClass().getName());
								try {
									FrameworkUtil.getBundle(fieldClass).getBundleContext().addServiceListener(optionalServiceListener, filter);
								} catch (InvalidSyntaxException e) {
									log.error(e.getMessage(), e);
									throw new RuntimeException(e);
								}
							}
						}
					}
				}
				clazz = clazz.getSuperclass();
			}
		}
		for (Object obj : objects) {
			if (obj instanceof OsgiBundleContextAware) {
				((OsgiBundleContextAware) obj).setApplicationContext(this);
			}
		}
		if (!optionalServiceListeners.isEmpty()) {
			Collection<String> toLog = reduceLoggingEntriesByMainPackageName(optionalServiceListeners);
			getLogger().debug("Registered optional service listener: {}", Arrays.toString(toLog.toArray()));
		}
	}

	public void callPostContruct() {
		getLogger().debug("Calling postcontruct on services");
		callPostContruct(services.toArray());
	}

	public void callPostContruct(Object...objects) {
		for (Object obj : objects) {
			if (obj instanceof InitializingBean) {
				InitializingBean initBean = (InitializingBean) obj;
				try {
					initBean.afterPropertiesSet();
				} catch (Exception e) {
					getLogger().error(e.getMessage(), e);
					throw new RuntimeException(e);
				}
			}
		}
	}

	public void callPreDestroy() {
		getLogger().info("Calling predestroy on services");
		callPreDestroy(services.toArray());
	}

	public void callPreDestroy(Object...objects) {
		List<Object> objListToDestroy = new ArrayList<>();
		for (Object obj : objects) {
			if (obj instanceof DisposableBean) {
				objListToDestroy.add(obj);
			}
		}
		Collections.sort(objListToDestroy, new Comparator<Object>() {

			@Override
			public int compare(Object o1, Object o2) {
				ShutdownOrder shutdownOrderO1 = o1.getClass().getAnnotation(ShutdownOrder.class);
				Integer shutdownValueO1 = shutdownOrderO1 == null ? 0 : shutdownOrderO1.value();
				ShutdownOrder shutdownOrderO2 = o2.getClass().getAnnotation(ShutdownOrder.class);
				Integer shutdownValueO2 = shutdownOrderO2 == null ? 0 : shutdownOrderO2.value();
				return shutdownValueO1.compareTo(shutdownValueO2);
			}
		});
		for (Object obj : objects) {
			JMXBean jmxBean =  obj.getClass().getAnnotation(JMXBean.class);
			if (jmxBean != null) {
				ManagedResource managedResource = obj.getClass().getAnnotation(ManagedResource.class);
				String objectNameString;
				if (managedResource != null) {
					objectNameString = managedResource.objectName();
				} else {
					String packageName = obj.getClass().getPackage().getName();
					String className = obj.getClass().getSimpleName();
					objectNameString = packageName + ":name=" + className;
				}
				try {
					ObjectName objectName = new ObjectName(objectNameString);
					getMbeanServer().unregisterMBean(objectName);
				} catch (JMException e) {
					getLogger().warn(e.getMessage());
				}
			}
		}
		for (Object obj : objListToDestroy) {
			DisposableBean disBean = (DisposableBean) obj;
			try {
				disBean.destroy();
			} catch (Exception e) {
				getLogger().error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected <T> T findService(Class<T> clazz, boolean optional) {
		for (Object obj : services) {
			if (clazz.isInstance(obj)) {
				return (T) obj;
			}
		}
		T res = getExternalDependency(clazz, null, optional);
		return res;
	}

	public void registerManagementObjects() throws OXException {
		Set<String> foundMngtServices = new HashSet<>();
		getLogger().debug("Register managment service objects");
		for (Object service : services) {
			JMXBean jmxBeanAnno = service.getClass().getAnnotation(JMXBean.class);
			if (jmxBeanAnno != null) {
				getLogger().debug("Found management service: {}", service.getClass().getName());
				foundMngtServices.add(service.getClass().getName());
				ManagedResource managedResource = service.getClass().getAnnotation(ManagedResource.class);
				String objectNameString;
				if (managedResource != null) {
					objectNameString = managedResource.objectName();
				} else {
					String packageName = service.getClass().getPackage().getName();
					String className = service.getClass().getSimpleName();
					objectNameString = packageName + ":name=" + className;
				}
				try {
					ObjectName objectName = new ObjectName(objectNameString);
					JMXBeanWrapper wrapperBean = new JMXBeanWrapper(service);
					getMbeanServer().registerMBean(wrapperBean, objectName);
				} catch (Exception ex) {
					getLogger().warn(ex.getMessage());
				}
			}
		}
		if (!foundMngtServices.isEmpty()) {
			Collection<String> toLog = reduceLoggingEntriesByMainPackageName(foundMngtServices);
			log.debug("Found management service object: {}", Arrays.toString(toLog.toArray()));
		}
	}

	private MBeanServer getMbeanServer() {
		SpecificPlatform platform = SpecificPlatform.get();
		if (platform != null) {
			return platform.getMBeanServer();
		}
		MBeanServerFactoryBean factory = new MBeanServerFactoryBean();
		factory.setLocateExistingServerIfPossible(true);
		factory.afterPropertiesSet();
		return factory.getObject();
	}

	protected void registerServicesToOsgi() {
		getLogger().debug("register services to OSGI");
		Set<String> allOsgiServices = new HashSet<>();
		services.forEach(s -> {
			RegisteredService registeredServiceAnno = s.getClass().getAnnotation(RegisteredService.class);
			if (registeredServiceAnno != null) {

				if (registeredServiceAnno.registeredClass().equals(Object.class)) {
					registerServiceToOsgi(s, s.getClass());
					allOsgiServices.add(s.getClass().getName());
				} else {
					registerServiceToOsgi(s, registeredServiceAnno.registeredClass());
					allOsgiServices.add(registeredServiceAnno.registeredClass().getName() + "(" + s.getClass().getName() + ")");
				}
			}
		});
		if (!allOsgiServices.isEmpty()) {
			Set<String> logSet = allOsgiServices.stream().map(s -> s.substring(bundleIdentificator.getClass().getPackage().getName().length())).collect(Collectors.toSet());
			getLogger().debug("Registered services to OSGI: {}", logSet);
		}
	}

	public void registerServiceToOsgi(Object service, Class<?> clazzToRegister) {
	    Bundle bundle = FrameworkUtil.getBundle(service.getClass());
	    BundleContext bundleContext = bundle.getBundleContext();
	    String classNameToRegister = Object.class.equals(clazzToRegister) ? service.getClass().getName() : clazzToRegister.getName();
	    getLogger().debug("ClassnameToRegister: " + classNameToRegister + ", service: " + service.getClass().getName() );

	    Dictionary<String, ?> serviceDictionary = (service instanceof IOsgiBundleDictionaryProvider) ?
	        ((IOsgiBundleDictionaryProvider) service).getDictionary() :
	            null;

	    bundleContext.registerService(classNameToRegister, service, serviceDictionary);
	}

	public void registerAjaxActionServiceFactoryToOSGI(AJAXActionServiceFactory ajaxActionServiceFactory, String moduleName, boolean multiple) {
		final Dictionary< String, Object > dictionary = new Hashtable< >();
        dictionary.put("module"  , moduleName);
        dictionary.put("multiple", multiple ? "true" : "false");

	    Bundle bundle = FrameworkUtil.getBundle(ajaxActionServiceFactory.getClass());
	    BundleContext bundleContext = bundle.getBundleContext();
	    serviceRegistrations.add(bundleContext.registerService(AJAXActionServiceFactory.class, ajaxActionServiceFactory, dictionary));

	}

	public void registerHttpConverter() {
		HttpModelConverterRegistry httpModelConverterRegistry = null;
		for (Object service : services) {
			if (service instanceof HttpModelConverter) {
				if (httpModelConverterRegistry == null) {
					httpModelConverterRegistry = getService(HttpModelConverterRegistry.class);
				}
				httpModelConverterRegistry.registerConverter((HttpModelConverter<?, ?>) service);
			}
		}
	}

	public ConfigurationService getConfigService() {
		ConfigurationService configurationService = getExternalDependency(ConfigurationService.class, this.getClass(), true);
		if (configurationService != null) {
			return configurationService;
		}
		for (Object obj : services) {
			if (obj instanceof ConfigurationService) {
				configurationService = (ConfigurationService) obj;
				break;
			}
		}
		if (configurationService == null) {
			throw new RuntimeException("No configurationService found!!!");
		}
		return configurationService;

	}

	@SuppressWarnings("unchecked")
	public <T> Collection<T> getServicesOfType(Class<T> clazz) {
		Collection<T> res = new HashSet<>();
		for (Object obj : services) {
			if (clazz.isAssignableFrom(obj.getClass())) {
				res.add((T) obj);
			}
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	protected <T> T getExternalDependency(Class<T> serviceClass, Class<?> owner, boolean optional) {
		for (Object service : services) {
			if (serviceClass.isAssignableFrom(service.getClass())) {
				return (T) service;
			}
		}
		final Bundle bundle = FrameworkUtil.getBundle(serviceClass);
		if (bundle == null) {
			getLogger().error("Bundle for service class " + serviceClass + " not found!");
			throw new RuntimeException("Bundle for service class " + serviceClass + " not found!");
		}
		final BundleContext bundleContext = bundle.getBundleContext();
		if ((bundleContext == null) && optional) {
			return null;
		}
		if (bundleContext == null) {
			getLogger().error("No bundle context found for class {}", serviceClass.getName());
			throw new RuntimeException("No bundle context found for class " + serviceClass.getName());
		}
		ServiceReference<T> serviceReference = bundleContext.getServiceReference(serviceClass);
		if (optional && (serviceReference == null)) {
			return null;
		}
		if (serviceReference == null) {
			getLogger().error("No service reference found for class {} in bundle {}", serviceClass.getName(), bundle.getSymbolicName(), new Exception());
			throw new RuntimeException("No service reference found for class " + serviceClass.getName() + " needed in class " + getOwner(owner));
		}
		T res = bundleContext.getService(serviceReference);
		return res;
	}

	private String getOwner(Class<?> owner) {
		if (owner != null) {
			return owner.getName();
		}
		return "<unknown>";
	}

	@Override
	public <S> S getService(Class<? extends S> clazz) {
		return findService(clazz, false);
	}

	@Override
	public <S> S getOptionalService(Class<? extends S> clazz) {
		return findService(clazz, true);
	}

	@Override
	protected <S> boolean addServiceAlt(Class<? extends S> clazz, S service) {
		return registerService(service, true);
	}

	@Override
	protected <S> boolean removeService(Class<? extends S> clazz) {
		Set<Object> toRemove = new HashSet<>();
		for (Object obj : services) {
			if (clazz.isAssignableFrom(obj.getClass())) {
				toRemove.add(obj);
			}
		}
		for (Object obj : toRemove) {
			services.remove(obj);
		}
		return !toRemove.isEmpty();
	}

	//-----------------------------ListableBeanFactory-Methods---------------------------------------------------------
	@Override
	public Object getBean(String name) throws BeansException {
		if (!classNameToClassInstanceMapping.containsKey(name)) {
			throw new BeanCreationException("Cannot find class instance of name " + name);
		}
		Class<?> clazz = classNameToClassInstanceMapping.get(name);
		return getService(clazz);
	}

	@Override
	public <T> T getBean(String name, Class<T> requiredType) throws BeansException {
		return requiredType.cast(getBean(name));
	}

	@Override
	public Object getBean(String name, Object... args) throws BeansException {
		throw new NotImplementedException("OsgiBundleContextAndActivator::getBean(String name, Object... args) throws BeansException");
	}

	@Override
	public <T> T getBean(Class<T> requiredType) throws BeansException {
		return getService(requiredType);
	}

	@Override
	public <T> T getBean(Class<T> requiredType, Object... args) throws BeansException {
		throw new NotImplementedException("OsgiBundleContextAndActivator::getBean(Class<T> requiredType, Object... args) throws BeansException");
	}

	@Override
	public <T> ObjectProvider<T> getBeanProvider(Class<T> requiredType) {
		throw new NotImplementedException("OsgiBundleContextAndActivator::getBeanProvider(Class<T> requiredType)");
	}

	@Override
	public <T> ObjectProvider<T> getBeanProvider(ResolvableType requiredType) {
		throw new NotImplementedException("OsgiBundleContextAndActivator::getBeanProvider(ResolvableType requiredType)");
	}

	@Override
	public boolean containsBean(String name) {
		return getOptionalService(name) != null;
	}

	@Override
	public boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
		return containsBean(name);
	}

	@Override
	public boolean isPrototype(String name) throws NoSuchBeanDefinitionException {
		return false;
	}

	@Override
	public boolean isTypeMatch(String name, ResolvableType typeToMatch) throws NoSuchBeanDefinitionException {
		throw new NotImplementedException("OsgiBundleContextAndActivator::isTypeMatch(String name, ResolvableType typeToMatch) throws NoSuchBeanDefinitionException");
	}

	@Override
	public boolean isTypeMatch(String name, Class<?> typeToMatch) throws NoSuchBeanDefinitionException {
		throw new NotImplementedException("OsgiBundleContextAndActivator::isTypeMatch(String name, Class<?> typeToMatch) throws NoSuchBeanDefinitionException");
	}

	@Override
	public Class<?> getType(String name) throws NoSuchBeanDefinitionException {
		return getBean(name).getClass();
	}

	@Override
	public String[] getAliases(String name) {
		return new String[0];
	}

	@Override
	public boolean containsBeanDefinition(String beanName) {
		return containsBean(beanName);
	}

	@Override
	public int getBeanDefinitionCount() {
		return services.size();
	}

	@Override
	public String[] getBeanDefinitionNames() {
		List<String> res = services.stream().map(o -> o.getClass().getName()).collect(Collectors.toList());
		return res.toArray(new String[0]);
	}

	@Override
	public String[] getBeanNamesForType(ResolvableType type) {
		throw new NotImplementedException("OsgiBundleContextAndActivator::getBeanNamesForType(ResolvableType type)");
	}

	@Override
	public String[] getBeanNamesForType(Class<?> type) {
		throw new NotImplementedException("OsgiBundleContextAndActivator::getBeanNamesForType(Class<?> type)");
	}

	@Override
	public String[] getBeanNamesForType(Class<?> type, boolean includeNonSingletons, boolean allowEagerInit) {
		throw new NotImplementedException("OsgiBundleContextAndActivator::getBeanNamesForType(Class<?> type, boolean includeNonSingletons, boolean allowEagerInit)");
	}

	@Override
	public <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException {
		Map<String, T> res = new HashMap<>();
		Collection<T> coll = getServicesOfType(type);
		coll.forEach(o -> res.put(o.getClass().getName(), o));
		return res;
	}

	@Override
	public <T> Map<String, T> getBeansOfType(Class<T> type, boolean includeNonSingletons, boolean allowEagerInit)
			throws BeansException {
		return getBeansOfType(type);
	}

	@Override
	public String[] getBeanNamesForAnnotation(Class<? extends Annotation> annotationType) {
		List<String> res = services.stream().filter(s -> s.getClass().getAnnotation(annotationType) != null).map(s -> s.getClass().getName()).collect(Collectors.toList());
		return res.toArray(new String[0]);
	}

	@Override
	public Map<String, Object> getBeansWithAnnotation(Class<? extends Annotation> annotationType)
			throws BeansException {
		Map<String, Object> res = new HashMap<>();
		List<Object> beansWithAnnotation = services.stream().filter(s -> s.getClass().getAnnotation(annotationType) != null).collect(Collectors.toList());
		beansWithAnnotation.forEach(o -> res.put(o.getClass().getName(), o));
		return res;
	}

	@Override
	public <A extends Annotation> A findAnnotationOnBean(String beanName, Class<A> annotationType)
			throws NoSuchBeanDefinitionException {
		Object bean = getBean(beanName);
		return bean.getClass().getAnnotation(annotationType);
	}

	@Override
    public String[] getBeanNamesForType(ResolvableType arg0, boolean arg1, boolean arg2) {
		throw new NotImplementedException("OsgiBundleContextAndActivator::getBeanNamesForType(ResolvableType arg0, boolean arg1, boolean arg2)");
	}

	@Override
    public Class<?> getType(String arg0, boolean arg1) throws NoSuchBeanDefinitionException {
		Object res = getOptionalService(arg0);
		if (res == null) {
			throw new NoSuchBeanDefinitionException(arg0);
		}
		return res.getClass();
	}

    @Override
    public <T> ObjectProvider<T> getBeanProvider(Class<T> requiredType, boolean allowEagerInit) {
        throw new NotImplementedException("OsgiBundleContextAndActivator::getBeanProvider(Class<T> requiredType, boolean allowEagerInit)");
    }

    @Override
    public <T> ObjectProvider<T> getBeanProvider(ResolvableType requiredType, boolean allowEagerInit) {
        throw new NotImplementedException("OsgiBundleContextAndActivator::getBeanProvider(ResolvableType requiredType, boolean allowEagerInit)");
    }

    @Override
    public <A extends Annotation> A findAnnotationOnBean(String beanName, Class<A> annotationType, boolean allowFactoryBeanInit) throws NoSuchBeanDefinitionException {
        if (allowFactoryBeanInit)
            throw new NotImplementedException("OsgiBundleContextAndActivator::findAnnotationOnBean(String beanName, Class<A> annotationType, boolean allowFactoryBeanInit) with allowFactoryBeanInit = true");

        return findAnnotationOnBean(beanName, annotationType);
    }

	private Object getOptionalService(String name) {
		try {
			Class<?> clazz = Class.forName(name);
			return getOptionalService(clazz);

		} catch (ClassNotFoundException e) {
			return null;
		}
	}
}
