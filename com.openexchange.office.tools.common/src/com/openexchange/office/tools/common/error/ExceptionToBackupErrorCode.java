/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.error;

import com.openexchange.exception.OXException;
import com.openexchange.groupware.infostore.InfostoreExceptionCodes;

/**
 * A helper class to map certain exception codes to specific error codes
 * defined in the enumeration ErrorCode. This is done specifically for
 * the backup file handling used by Documents, if a storage does not
 * support versions.
 *
 * {@link ErrorCode}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */

public class ExceptionToBackupErrorCode
{
    @SuppressWarnings("deprecation")
    static protected final org.apache.commons.logging.Log LOG = com.openexchange.log.LogFactory.getLog(ExceptionToBackupErrorCode.class);

    /**
     * Maps certain OXExceptions to specific error codes.
     *
     * @param e
     *  An instance of a OXException.
     *
     * @param defErrorCode
     *  The default error code to be used if there is no mapping available.
     *
     * @param logEx
     *  Specifies if the exception should be logged or not.
     *
     * @return
     *  The specific error code or the default if no mapping is available.
     */
    static public ErrorCode map(OXException e, ErrorCode defErrorCode, boolean logEx) {
        ErrorCode errorCode = defErrorCode;
        String msg = "Exception caught";

        // special handling for file storage exceptions
        if (e.getPrefix().equalsIgnoreCase("FLS")) {
            if (e.getCode() == com.openexchange.filestore.QuotaFileStorageExceptionCodes.STORE_FULL.getNumber()) {
                // set specific error code if quota reached
                errorCode = ErrorCode.SAVEDOCUMENT_BACKUPFILE_QUOTA_REACHED_ERROR;
                msg = "RT connection: Quota reached error detected!";
            }
        } else if (e.getPrefix().equalsIgnoreCase("FILE_STORAGE")) {
            if (e.getCode() == com.openexchange.file.storage.FileStorageExceptionCodes.FILE_NOT_FOUND.getNumber()) {
                // file not found error
                errorCode = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR;
                msg = "RT connection: File not found error detected!";
            }
        } else if (e.getPrefix().equalsIgnoreCase("IFO")) {
            if (e.getCode() == InfostoreExceptionCodes.NO_READ_PERMISSION.getNumber()) {
                // read permissions missing
                errorCode = ErrorCode.SAVEDOCUMENT_BACKUPFILE_READ_OR_WRITE_PERMISSION_MISSING_ERROR;
                msg = "RT connection: No read permissions detected!";
            } else if (e.getCode() == InfostoreExceptionCodes.NO_WRITE_PERMISSION.getNumber()) {
                // read permissions missing
                errorCode = ErrorCode.SAVEDOCUMENT_BACKUPFILE_READ_OR_WRITE_PERMISSION_MISSING_ERROR;
                msg = "RT connection: No write permissions!";
            } else if (e.getCode() == InfostoreExceptionCodes.NO_CREATE_PERMISSION.getNumber()) {
                // create permissions missing
                errorCode = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_PERMISSION_MISSING_ERROR;
                msg = "RT connection: No create permissions!";
            } else if (e.getCode() == InfostoreExceptionCodes.NOT_EXIST.getNumber()) {
                // file not found
                errorCode = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR;
                msg = "RT connection: Original document couldn't be found!";
            } else if (e.getCode() == InfostoreExceptionCodes.ALREADY_LOCKED.getNumber()) {
                // file is locked now
                errorCode = ErrorCode.SAVEDOCUMENT_BACKUPFILE_IS_LOCKED_ERROR;
                msg = "RT connection: The document is locked and cannot be modified";
            }
        }

        if (logEx) {
            LOG.error(msg, e);
        }

        return errorCode;
    }
}
