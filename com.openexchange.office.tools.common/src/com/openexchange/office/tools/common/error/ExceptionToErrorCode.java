/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.error;

import com.openexchange.exception.Category;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageExceptionCodes;
import com.openexchange.filestore.FileStorageCodes;
import com.openexchange.filestore.QuotaFileStorageExceptionCodes;
import com.openexchange.folderstorage.FolderExceptionErrorMessage;
import com.openexchange.groupware.infostore.InfostoreExceptionCodes;
import com.openexchange.guard.api.GuardApiExceptionCodes;

/**
 * A helper class to map certain exception codes to specific error codes
 * defined in the enumation ErrorCode.
 *
 * {@link ErrorCode}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */

public class ExceptionToErrorCode {

    static protected final org.apache.commons.logging.Log LOG = com.openexchange.log.LogFactory.getLog(ExceptionToErrorCode.class);

    /**
     * Maps certain OXExceptions to specific error codes.
     *
     * @param e
     *  An instance of a OXException.
     *
     * @param defErrorCode
     *  The default error code to be used if there is no mapping available.
     *
     * @param logEx
     *  Specifies if the exception should be logged or not.
     *
     * @return
     *  The specific error code or the default if no mapping is available.
     */
    static public ErrorCode map(OXException e, ErrorCode defErrorCode, boolean logEx) {
        ErrorCode errorCode = defErrorCode;
        String msg = "Exception caught";
        final String prefix = e.getPrefix();
        final int code = e.getCode();
        final Category category = e.getCategory();

        switch (prefix) {
            case "CTX":
                errorCode = ErrorCode.GENERAL_UNKNOWN_CONTEXT_PROBLEMS_ERROR;
                break;
            case "DBP":
            	// database exceptions
            	if (category == Category.CATEGORY_SERVICE_DOWN) {
            		errorCode = ErrorCode.GENERAL_SERVICE_DOWN_ERROR;
            		msg = "RT Connection: Database service not cannot be contacted!";
            	} else {
            		errorCode = ErrorCode.GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR;
            		msg = "RT Connection: Database error detected!";
            	}
            	break;
            case "FLS":
                // special handling for file storage exceptions
                if (code == QuotaFileStorageExceptionCodes.STORE_FULL.getNumber()) {
                    // set specific error code if quota reached
                    errorCode = ErrorCode.GENERAL_QUOTA_REACHED_ERROR;
                    msg = "RT connection: Quota reached error detected!";
                } else if (code == FileStorageCodes.FILE_NOT_FOUND.getNumber()) {
                    errorCode = ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR;
                    msg = "RT connection: File not found error detected!";
                }
                break;
            case "FLD":
                if (code == FolderExceptionErrorMessage.FOLDER_NOT_VISIBLE.getNumber()) {
                    errorCode = ErrorCode.GENERAL_FOLDER_NOT_VISIBLE_ERROR;
                    msg = "RT connection: Folder not visible, no permissions!";
                } else if (code == FolderExceptionErrorMessage.NOT_FOUND.getNumber()) {
                    errorCode = ErrorCode.GENERAL_FOLDER_NOT_FOUND_ERROR;
                    msg = "RT connection: Folder not found, removed folder?";
                } else if (code == FolderExceptionErrorMessage.INVALID_FOLDER_ID.getNumber()) {
                    errorCode = ErrorCode.GENERAL_FOLDER_INVALID_ID_ERROR;
                    msg = "RT connection: Folder id is invalid!";
                } else {
                    errorCode = ErrorCode.GENERAL_UNKNOWN_ERROR;
                    msg = "RT connection: Unkown folder error occurred!";
                }
                break;
            case "QUOTA":
                // set specific error code if quota reached
                errorCode = ErrorCode.GENERAL_QUOTA_REACHED_ERROR;
                msg = "RT connection: Quota reached error detected!";
                break;
            case "IFO":
                if (code == InfostoreExceptionCodes.NO_READ_PERMISSION.getNumber()) {
                    // read permissions missing
                    errorCode = ErrorCode.GENERAL_PERMISSION_READ_MISSING_ERROR;
                    msg = "RT connection: No read permissions detected!";
                } else if (code == InfostoreExceptionCodes.NO_WRITE_PERMISSION.getNumber()) {
                    // read permissions missing
                    errorCode = ErrorCode.GENERAL_PERMISSION_WRITE_MISSING_ERROR;
                    msg = "RT connection: No write permissions!";
                } else if (code == InfostoreExceptionCodes.NOT_EXIST.getNumber()) {
                    // file not found
                    errorCode = ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR;
                    msg = "RT connection: Original document couldn't be found!";
                } else if (code == InfostoreExceptionCodes.DOCUMENT_NOT_EXIST.getNumber()) {
                    // file not found
                    errorCode = ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR;
                    msg = "RT connection: Original document couldn't be found!";
                } else if (code == InfostoreExceptionCodes.ALREADY_LOCKED.getNumber()) {
                    // file is locked now
                    errorCode = ErrorCode.GENERAL_FILE_LOCKED_ERROR;
                    msg = "RT connection: The document is locked and cannot be modified";
                } else if (code == InfostoreExceptionCodes.VALIDATION_FAILED_CHARACTERS.getNumber()) {
                    // illegal character in file name
                    errorCode = ErrorCode.RENAMEDOCUMENT_VALIDATION_FAILED_CHARACTERS_ERROR;
                    msg = "RT connection: Renamed failed due to invalid character in file name";
                    // try to retrieve the illegal character from the exception
                    errorCode.setValue(getFirstArg(e));
                } else if (code == InfostoreExceptionCodes.FILENAME_NOT_UNIQUE.getNumber()) {
                    errorCode = ErrorCode.RENAMEDOCUMENT_FAILED_DUPLICATE_FILE_NAME;
                    msg = errorCode.getDescription();
                    // try to retrieve the illegal character from the exception
                    errorCode.setValue(getFirstArg(e));
                }
                break;
            case "FILE_STORAGE":
                if (code == FileStorageExceptionCodes.FILE_NOT_FOUND.getNumber()) {
                    // file not found error
                    errorCode = ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR;
                    msg = "RT connection: File not found error detected!";
                } else if (code == FileStorageExceptionCodes.ILLEGAL_CHARACTERS.getNumber()) {
                    errorCode = ErrorCode.RENAMEDOCUMENT_VALIDATION_FAILED_CHARACTERS_ERROR;
                    msg = "RT connection: Rename not possible due to illegal characters!";
                    // try to retrieve the illegal character from the exception
                    errorCode.setValue(getFirstArg(e));
                }
                break;
            case "DROPBOX":
                // special code to detect specific dropbox errors
                if (code == 2) {
                    errorCode = ErrorCode.LOADDOCUMENT_TIMEOUT_RETRIEVING_STREAM_ERROR;
                    msg = "RT connection: Couldn't retrieve stream from Dropbox in time";
                }
                break;
            case "GUARD": {
                if (code == GuardApiExceptionCodes.AUTH_ERROR.getNumber()) {
                    errorCode = ErrorCode.LOADDOCUMENT_GUARD_AUTH_FAILED_ERROR;
                } else {
                    errorCode = ErrorCode.LOADDOCUMENT_ENCRYPTED_FILE_CANNOT_BE_DECRYPTED_ERROR;
                }
            }
            default:
                // nothing to do default values set
                break;
        }

        if (logEx) {
            LOG.error(msg, e);
        }

        return errorCode;
    }

    /**
     * Retrieve the first display argument from the exception.
     *
     * @param e the exception
     * @return
     */
    static private String getFirstArg(final OXException e) {
    	String result = null;

    	final Object[] dispArgs = e.getDisplayArgs();

    	if (dispArgs.length > 0) {
    		if (dispArgs[0] instanceof String)
    			result = (String)dispArgs[0];
    		else if (dispArgs[0] != null) {
    			result = dispArgs[0].toString();
    		}
    	}

    	return result;
    }
}
