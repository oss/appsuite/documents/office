/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common.error;


/**
 * A helper class to map certain error codes to specific error codes
 * that are used by the backup file processing.
 *
 * {@link ErrorCode}
 *
 * @author <a href="mailto:carsten.driesner@open-xchange.com">Carsten Driesner</a>
 */
public class ErrorCode2BackupErrorCode
{
    /**
     * Maps certain general error codes to specific backup file error codes.
     *
     * @param errorCode
     *  The original error code.
     *
     * @param defErrorCode
     *  A default error code to be used, if no defined mapping can be found.
     *
     * @return
     *  A mapped or the default error code to be used by the backup handling
     *  code.
     */
    public static ErrorCode mapToBackupErrorCode(final ErrorCode errorCode, final ErrorCode defErrorCode) {
        ErrorCode result = defErrorCode;
        int code = errorCode.getCode();

        if (code == ErrorCode.CODE_NO_ERROR) // no error must be mapped, too
            result = errorCode;
        else if (code == ErrorCode.GENERAL_FILE_NOT_FOUND_ERROR.getCode())
            result = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_FAILED_ERROR;
        else if (code == ErrorCode.GENERAL_PERMISSION_CREATE_MISSING_ERROR.getCode())
            result = ErrorCode.SAVEDOCUMENT_BACKUPFILE_CREATE_PERMISSION_MISSING_ERROR;
        else if ((code == ErrorCode.GENERAL_PERMISSION_READ_MISSING_ERROR.getCode()) ||
                 (code == ErrorCode.GENERAL_PERMISSION_WRITE_MISSING_ERROR.getCode()))
            result = ErrorCode.SAVEDOCUMENT_BACKUPFILE_READ_OR_WRITE_PERMISSION_MISSING_ERROR;
        else if (code == ErrorCode.GENERAL_QUOTA_REACHED_ERROR.getCode())
            result = ErrorCode.SAVEDOCUMENT_BACKUPFILE_QUOTA_REACHED_ERROR;

        return result;
    }
}
