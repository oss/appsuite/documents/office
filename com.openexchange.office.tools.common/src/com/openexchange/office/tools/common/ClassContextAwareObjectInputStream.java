/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.tools.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.ref.WeakReference;

import org.apache.commons.lang3.Validate;

//=============================================================================
/** Class serialization inside an OSGI environment can be some kind of pane.
 *
 *  A typical problem : on deserialization the wrong (default) class loader
 *  is used and is not able to load right class within it's context.
 *
 *  This helper solve the problem by hooking into the class loader hierarchy
 *  and select the right loader instead of using the default class loader.
 *
 *  The tricky part here is the fact :
 *  - an object knows it's class.
 *  - a class knows it's class loader.
 *  - a object/class of an OSGI environment has a class loader set which knows
 *    the rules of OSGI already .-)
 *
 *  So - if we use that class loader of the context object we can be sure
 *  all classes in that OSGI context will be available for loading.
 */
public class ClassContextAwareObjectInputStream extends ObjectInputStream
{
	//-------------------------------------------------------------------------
	/** construct a new instance and define the context it has to use.
	 *
	 *	@param	aObjectData [IN]
	 *			the stream containing the raw data of the serialized object.
	 *
	 *	@param	aContext [IN]
	 *			the context class loading has to be done.
	 */
    public ClassContextAwareObjectInputStream (final InputStream aObjectData,
    										   final Object      aContext   )
    	throws Exception
    {
        super (aObjectData);

        Validate.notNull(aContext, "No context given.");

        final Class< ? >  aContextClass  = aContext.getClass();
        final ClassLoader aContextLoader = aContextClass.getClassLoader();
        m_aContextLoader = new WeakReference< ClassLoader > (aContextLoader);
    }

	//-------------------------------------------------------------------------
    @Override
    protected Class<?> resolveClass (final ObjectStreamClass aClassDescriptor)
    	throws IOException
    		 , ClassNotFoundException
    {
    	final String      sClass         = aClassDescriptor.getName ();
    	final ClassLoader aContextLoader = m_aContextLoader.get();
    	      Class< ? >  aClass         = aContextLoader.loadClass(sClass);

    	if (aClass == null)
    		aClass = super.resolveClass(aClassDescriptor);

        return aClass;
    }

    //-------------------------------------------------------------------------
    private WeakReference< ClassLoader > m_aContextLoader = null;
}