/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.rest;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.openexchange.ajax.requesthandler.AJAXActionService;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestDataTools;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.office.rt2.core.proxy.RT2DocProxy;
import com.openexchange.office.rt2.core.proxy.RT2DocProxyRegistry;
import com.openexchange.office.rt2.core.ws.RT2ChannelId;
import com.openexchange.office.rt2.core.ws.RT2EnhDefaultWebSocket;
import com.openexchange.office.rt2.core.ws.RT2WSApp;
import com.openexchange.office.rt2.core.ws.RT2WebSocketListener;
import com.openexchange.office.rt2.hazelcast.DistributedDocInfoMap;
import com.openexchange.office.rt2.hazelcast.RT2DocOnNodeMap;
import com.openexchange.office.rt2.protocol.RT2Message;
import com.openexchange.office.rt2.protocol.RT2MessageFactory;
import com.openexchange.office.rt2.protocol.RT2MessageGetSet;
import com.openexchange.office.rt2.protocol.value.RT2CliendUidType;
import com.openexchange.office.rt2.protocol.value.RT2DocUidType;
import com.openexchange.office.rt2.protocol.value.RT2MessageType;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.error.HttpStatusCode;
import com.openexchange.office.tools.common.log.MDCHelper;
import com.openexchange.office.tools.common.string.StringHelper;
import com.openexchange.office.tools.service.cluster.ClusterService;
import com.openexchange.office.tools.service.logging.MDCEntries;
import com.openexchange.tools.session.ServerSession;

@RestController
public class EmergencyLeaveAction implements AJAXActionService {

    // ---------------------------------------------------------------
    private static final Logger log = LoggerFactory.getLogger(EmergencyLeaveAction.class);    
    
    // ---------------------------------------------------------------
    private static final String[] aMandatoryParams = { RESTParameters.PARAMETER_CLIENTUID,
                                                       RESTParameters.PARAMETER_DOCUID,
    		                                           RESTParameters.PARAMETER_CHANNELUID };
    
    public static final String WS_URL_ROOT = "/rt2/v1/default/";
    
    @Autowired
    private RT2WSApp rt2WSApp;
    
    @Autowired
    private RT2WebSocketListener rt2WebSocketListener;
    
    @Autowired
    private RT2DocProxyRegistry rt2DocProxyRegistry;

    @Autowired
    private RT2DocOnNodeMap rt2DocOnNodeMap;

    @Autowired
    private ClusterService clusterService;

    @Autowired
    private DistributedDocInfoMap distributedDocInfoMap;

    // ---------------------------------------------------------------
	@Override
	public AJAXRequestResult perform(AJAXRequestData requestData, ServerSession session) throws OXException {

		if (!ParamValidator.areAllParamsNonEmpty(requestData, aMandatoryParams))
            return ParamValidator.getResultFor(HttpStatusCode.BAD_REQUEST.getStatusCode());

        final String channelUID = requestData.getParameter(RESTParameters.PARAMETER_CHANNELUID);
        
        final String channelUrl = WS_URL_ROOT + channelUID;
        final String clientUID = requestData.getParameter(RESTParameters.PARAMETER_CLIENTUID);
        final String docUID = requestData.getParameter(RESTParameters.PARAMETER_DOCUID);
        final HttpServletRequest servletRequest = requestData.optHttpServletRequest();

        JSONObject leaveData = (JSONObject) requestData.getData();
        if (null == leaveData && (servletRequest != null)) {
        	// try to extract body data using a different approach (e.g.
        	// support POST with preferStream = false)
            requestData.setUploadStreamProvider(null);
            AJAXRequestDataTools.loadRequestBody(requestData);
            leaveData = (JSONObject) requestData.getData();
        }

    	try {
            MDCHelper.safeMDCPut(MDCEntries.DOC_UID, docUID);
            MDCHelper.safeMDCPut(MDCEntries.CLIENT_UID, clientUID);

    		if (leaveData == null) {
    			leaveData = new JSONObject();
    		}

            log.debug("RT2: emergencyLeave request received for com.openexchange.rt2.document.uid {}, com.openexchange.rt2.client.uid {}", docUID, clientUID);

            final RT2EnhDefaultWebSocket clientConnection = rt2WSApp.getWebSocketWithId(new RT2ChannelId(channelUrl));
            if (null != clientConnection) {
            	rt2WebSocketListener.emergencyLeave(clientConnection, docUID, clientUID, session.getSessionID(), leaveData);
            } else {
            	sendEmergencyLeaveViaDocProxy(session, new RT2CliendUidType(clientUID), new RT2DocUidType(docUID), leaveData);
            }
    	} catch (final JSONException e) {
    		log.info("RT2: JSONException caught trying to decode leave data from emergency request", e);
    		return new AJAXRequestResult(ErrorCode.GENERAL_ARGUMENTS_ERROR.getAsJSONResultObject());
    	} catch (final DocProxyNotFoundException e) {
    		log.info("RT2: DocProxyNotFoundException caught trying trying to send emergency request", e);
    		return new AJAXRequestResult(ErrorCode.GENERAL_CLIENT_UID_UNKNOWN_ERROR.getAsJSONResultObject());
    	} catch (final ServiceNotAvailableException e) {
    		log.error("RT2: ServiceNotAvailableException caught trying trying to send emergency request", e);
    		return new AJAXRequestResult(ErrorCode.GENERAL_MISSING_SERVICE_ERROR.getAsJSONResultObject());
    	} catch (final Exception e) {
    		log.warn("RT2: Exception caught trying to send emergency request", e);
    		return new AJAXRequestResult(ErrorCode.GENERAL_UNKNOWN_ERROR.getAsJSONResultObject());
    	} finally {
    	    MDC.remove(MDCEntries.DOC_UID);
    	    MDC.remove(MDCEntries.CLIENT_UID);
    	}

        return ParamValidator.getResultFor(HttpStatusCode.OK.getStatusCode());
	}

    // ---------------------------------------------------------------
	private void sendEmergencyLeaveViaDocProxy(final ServerSession session, RT2CliendUidType clientUID, RT2DocUidType docUID, final JSONObject leaveData) throws Exception {
    	final String docProxyID = RT2DocProxy.calcID(clientUID, docUID);
    	final RT2DocProxy docProxy = rt2DocProxyRegistry.getDocProxy(docProxyID);
    	if (docProxy == null) {
    		log.warn("RT2DocProxy {} not found in registry. {}", docProxyID, getInfoForDocAndClientUidInClusterEnv(docUID, clientUID));
    		throw new DocProxyNotFoundException("RT2DocProxy " + docProxyID + " not found in registry - must drop emergency leave message");
    	}

  		final RT2Message aEmergencyLeaveRequest = RT2MessageFactory.newMessage(RT2MessageType.REQUEST_EMERGENCY_LEAVE, clientUID, docUID);
		RT2MessageGetSet.setSessionID(aEmergencyLeaveRequest, session.getSessionID());
		aEmergencyLeaveRequest.setBody(leaveData);
	
    	docProxy.addMsgFromWebSocket(aEmergencyLeaveRequest);
        docProxy.leave(aEmergencyLeaveRequest);
	}

    // ---------------------------------------------------------------
	private String getInfoForDocAndClientUidInClusterEnv(RT2DocUidType docUid, RT2CliendUidType clientUid) {
		String infoString = "Info for Doc and client could not be retrieved";
		try {
			String thisNode = clusterService.getLocalMemberUuid();
			String docOnNode = rt2DocOnNodeMap.get(docUid.getValue());
			if (StringUtils.isEmpty(docOnNode)) {
				docOnNode = "not found";
			}
			Set<String> knownClients = distributedDocInfoMap.getClientsOfDocUid(docUid);
			infoString = StringHelper.replacePlaceholdersWithArgs("DocOnNodeMap entry for com.openexchange.rt2.document.uid {} = {} and this com.openexchange.rt2.backend.uid = {}, known clients for doc {}", docUid.getValue(), docOnNode, thisNode, knownClients.toString());
		} catch (Exception e) {
			log.info("getInfoForDocAndClientUidInClusterEnv failed due to exception", e);
		}
		return infoString;
	}
}
