/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.rt2.rest;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.office.tools.service.files.FileHelperService;
import com.openexchange.office.tools.service.files.FileHelperServiceFactory;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.session.ServerSession;

public class ParamValidator {

    private ParamValidator() {
        // nothing to do
    }

    // ---------------------------------------------------------------
    protected static final Logger LOG = LoggerFactory.getLogger(ParamValidator.class);

    // ---------------------------------------------------------------
    public static boolean areAllParamsNonEmpty(final AJAXRequestData aRequest, String[] paramNames) {

        return (aRequest != null) &&
            Arrays.asList(paramNames)
                  .stream()
                  .map(p -> aRequest.getParameter(p))
                  .filter(StringUtils::isEmpty)
                  .collect(Collectors.toSet())
                  .isEmpty();
    }

    // ---------------------------------------------------------------
    public static boolean canReadFile(final ServiceLookup services, final ServerSession session, String folderId, String fileId) {
        boolean canRead = false;
        try {
            final int userId = session.getUserId();

            // check document access rights to ensure that the current user has access rights to retrieve document content
            final FileHelperServiceFactory fileHelperServiceRegistry = services.getService(FileHelperServiceFactory.class);
            final FileHelperService fileHelperService = fileHelperServiceRegistry.create(session);
            canRead = fileHelperService.canReadFile(folderId, fileId, userId);
        } catch (Exception e) {
            LOG.error("AjaxRequest GetFileAction exception caught while trying to check access rights", e);
        }

        return canRead;
    }

    // ---------------------------------------------------------------
    public static AJAXRequestResult getResultFor(int nHttpError) {
        final AJAXRequestResult result = new AJAXRequestResult();
        result.setHttpStatusCode(nHttpError);
        return result;
    }
}
