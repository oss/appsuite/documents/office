/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.dcs.registry;

import java.util.HashMap;
import java.util.Map;

//=============================================================================
public class DCSRegistryItem {
    //-------------------------------------------------------------------------
    public static final String DCS_KEY_ID           = "id"       ;
    public static final String DCS_KEY_INTERFACE    = "interface";
    public static final String DCS_KEY_HOST         = "host"     ;
    public static final String DCS_KEY_PORT_JMS     = "port.jms" ;
    public static final String DCS_KEY_PORT_JMS_SSL = "port.jmsssl" ;
    public static final String DCS_KEY_USE_SSL      = "usessl" ;

    private final Map<String, Object> content = new HashMap<>();

    private String id;

    //-------------------------------------------------------------------------
	public DCSRegistryItem () {}

	//-------------------------------------------------------------------------
	public synchronized void setID (final String sID) {
	    this.id = sID;
	    content.put (DCS_KEY_ID, sID);
	}

	//-------------------------------------------------------------------------
    public synchronized String getID () {
        return (String) content.get(DCS_KEY_ID);
    }

    //-------------------------------------------------------------------------
    public synchronized void setHost (final String sHost) {
        content.put (DCS_KEY_HOST, sHost);
    }

    //-------------------------------------------------------------------------
    public synchronized String getHost () {
        return (String) content.get(DCS_KEY_HOST);
    }

    //-------------------------------------------------------------------------
    public synchronized void setInterface (final String sInterface) {
        content.put(DCS_KEY_INTERFACE, sInterface);
    }

    //-------------------------------------------------------------------------
    public synchronized String getInterface () {
        return (String) content.get(DCS_KEY_INTERFACE);
    }

    //-------------------------------------------------------------------------
    public synchronized void setJMSPort(final Integer nPort) {
        content.put (DCS_KEY_PORT_JMS, nPort);
    }

    //-------------------------------------------------------------------------
    public synchronized Integer getJMSPort () {
        return (Integer) content.get(DCS_KEY_PORT_JMS);
    }

    //-------------------------------------------------------------------------
    public synchronized void setUseSSL(final Boolean useSSL) {
        content.put (DCS_KEY_USE_SSL, useSSL);
    }

    //-------------------------------------------------------------------------
    public synchronized Boolean isUseSSL() {
        return (Boolean) content.get(DCS_KEY_USE_SSL);
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DCSRegistryItem other = (DCSRegistryItem) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
