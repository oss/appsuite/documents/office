/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.dcs.registry.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.ExceptionUtils;
import com.openexchange.office.dcs.registry.DCSRegistry;
import com.openexchange.office.dcs.registry.DCSRegistryException;
import com.openexchange.office.dcs.registry.DCSRegistryItem;
import com.openexchange.office.tools.common.TimeStampUtils;
import com.openexchange.office.tools.common.error.ErrorCode;

public class DnsDiscoveryRegistry implements DCSRegistryAccess {
    private static final Logger log = LoggerFactory.getLogger(DnsDiscoveryRegistry.class);
    private static final long MAX_TIME_TO_DETERMINE_HOSTS_FOR_SERVICE = 60 * 1000; // 60 sec
    private static final long TIME_BETWEEN_NEXT_WARNINGS = 60 * 1000; // 60 sec

    private String dcsServiceName;
    private boolean useSSL;

    private AtomicLong lastSuccess = new AtomicLong(0);
    private AtomicLong lastWarnLog = new AtomicLong(0);
    private AtomicReference<String> lastKnownIpAddressesInfo = new AtomicReference<>("");

    public DnsDiscoveryRegistry(String dcsServiceName, boolean useSSL) {
        this.dcsServiceName = dcsServiceName;
        this.useSSL = useSSL;
    }

    @Override
    public Collection<DCSRegistryItem> getRegisteredDCSItems() throws Exception {
        List<DCSRegistryItem> result = new ArrayList<>();

        try {
            InetAddress[] addresses = InetAddress.getAllByName(dcsServiceName);
            if ((addresses != null) && (addresses.length > 0)) {
                for (InetAddress address : addresses) {
                    String hostAddress = address.getHostAddress();
                    DCSRegistryItem item = new DCSRegistryItem();
                    item.setHost(hostAddress);
                    item.setInterface(hostAddress);
                    item.setJMSPort(DCSRegistry.DEFAULT_PORT_JMS);
                    item.setUseSSL(useSSL);
                    result.add(item);
                }

                if (log.isInfoEnabled()) {
                    String currentIpAddressesInfo = getIpAddressString(addresses);
                    String knownIpAddressesInfo = lastKnownIpAddressesInfo.get();
                    boolean updated = false;

                    if (!StringUtils.equals(currentIpAddressesInfo, knownIpAddressesInfo)) {
                        lastKnownIpAddressesInfo.set(currentIpAddressesInfo);
                        updated = true;
                    }

                    if (logSuccess() || updated) {
                        log.info("DCS service hosts retrieved via DNS {}", currentIpAddressesInfo);
                    }
                }

                lastSuccess.set(System.currentTimeMillis());
                lastWarnLog.set(0);
            } else {
                log.warn("Cannot determine ip addresses - empty result by InetAddress.getAllByName() for service " + dcsServiceName);
            }
        } catch (UnknownHostException e) {
            handleThrowableDetermineIPAddresses(e);
            throw e;
        } catch (Throwable e) {
            ExceptionUtils.handleThrowable(e);
            handleThrowableDetermineIPAddresses(e);
            throw new DCSRegistryException(ErrorCode.GENERAL_UNKNOWN_ERROR);
        }

        return result;
    }

    private void handleThrowableDetermineIPAddresses(Throwable e) throws Exception {
        final long now = System.currentTimeMillis();
        final long timeSpanSinceLastSuccess = TimeStampUtils.timeDiff(lastSuccess.get(), now);
        final long timeSpanBetweenWarnLogs = TimeStampUtils.timeDiff(lastWarnLog.get(), now);

        if ((timeSpanSinceLastSuccess > MAX_TIME_TO_DETERMINE_HOSTS_FOR_SERVICE) &&
            (timeSpanBetweenWarnLogs > TIME_BETWEEN_NEXT_WARNINGS)) {
            long secondsFailed = timeSpanSinceLastSuccess / 1000;
            log.warn("Exception caught trying to determine ip addresses for service: " + dcsServiceName + ". Service host(s) could not been determined since " +  secondsFailed + " seconds - check configuration!", e);
            lastWarnLog.set(now);
        } else {
            log.debug("Exception caught trying to determine ip addresses for service: " + dcsServiceName + ". May be the service has not been started yet.", e);
        }
    }

    private boolean logSuccess() {
        final long lastSuccessTimeStamp = lastSuccess.get();
        final long lastWarnLogTimeStamp = lastWarnLog.get();
        return ((lastSuccessTimeStamp == 0) || (lastWarnLogTimeStamp != 0));
    }

    private String getIpAddressString(InetAddress[] addresses) {
        StringBuilder tmp = new StringBuilder(256);
        if ((addresses != null) && (addresses.length > 0)) {
            tmp.append("service=");
            tmp.append(addresses[0].getCanonicalHostName());
            tmp.append(", hosts=");
            for (int i=0; i < addresses.length; i++) {
                InetAddress address = addresses[i];
                tmp.append(address.getHostAddress());
                if (i+1 < addresses.length) {
                    tmp.append(", ");
                }
            }
        }
        return tmp.toString();
    }

}
