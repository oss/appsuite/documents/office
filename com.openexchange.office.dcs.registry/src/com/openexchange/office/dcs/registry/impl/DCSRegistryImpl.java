/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.dcs.registry.impl;

import java.util.Collection;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openexchange.config.ConfigurationService;
import com.openexchange.office.dcs.registry.DCSRegistry;
import com.openexchange.office.dcs.registry.DCSRegistryException;
import com.openexchange.office.dcs.registry.DCSRegistryItem;
import com.openexchange.office.tools.annotation.RegisteredService;
import com.openexchange.office.tools.common.error.ErrorCode;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAware;

@Service
@RegisteredService(registeredClass=DCSRegistry.class)
public class DCSRegistryImpl implements DCSRegistry, InitializingBean, OsgiBundleContextAware {
	private static final String DCS_DISCOVERY_STRATGEY = "com.openexchange.dcs.client.discoveryStrategy";
	private static final String DCS_SERVICE_NAME = "com.openexchange.dcs.client.serviceName";
	private static final String DCS_ENABLE_SSL = "com.openexchange.dcs.client.ssl.enabled";
	private static final String DEFAULT_DCS_SERVICE_NAME = "documents-collaboration";
	private static final String DISCOVERY_STRATEGY_DB = "db";
	private static final String DISCOVERY_STRATEGY_DNS = "dns";

	@Autowired
	private ConfigurationService configService;

	private DCSRegistryAccess dcsRegistryAccess;

	private OsgiBundleContextAndActivator bundleCtx;

	@Override
	public void afterPropertiesSet() throws Exception {
		String dcsDiscoveryStrategy = configService.getProperty(DCS_DISCOVERY_STRATGEY, DISCOVERY_STRATEGY_DNS);
		switch (dcsDiscoveryStrategy) {
		case DISCOVERY_STRATEGY_DNS: {
			String dcsServiceName = configService.getProperty(DCS_SERVICE_NAME, DEFAULT_DCS_SERVICE_NAME);
			Boolean useSSL = configService.getBoolProperty(DCS_ENABLE_SSL, false);
			dcsRegistryAccess = new DnsDiscoveryRegistry(dcsServiceName, useSSL);
		}
		break;

		default: {
			throw new DCSRegistryException(ErrorCode.DCS_DISCOVERY_STRATEGY_UNKNOWN_ERROR);
		}
		}
	}

	@Override
	public Collection<DCSRegistryItem> getDCSInstances() throws DCSRegistryException {
		try {
			return dcsRegistryAccess.getRegisteredDCSItems();
		} catch (Exception e) {
			throw new DCSRegistryException(ErrorCode.DCS_DISCOVERY_NOT_POSSIBLE_ERROR, e);
		}
	}

	@Override
	public void setApplicationContext(OsgiBundleContextAndActivator bundleCtx) {
		this.bundleCtx = bundleCtx;
	}
}
