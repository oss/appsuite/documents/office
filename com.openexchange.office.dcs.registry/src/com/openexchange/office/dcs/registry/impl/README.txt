### DCS configuration #####

The DCS configuration had to be changed, since registering is now done on DCS server side only.

A valid documents-collaboration.properties file could look like the following (tested):

************************************************************************************************************************
************************************************************************************************************************

# Define an unique ID for THIS DCS
#
# If no unique ID is provided, a unique one will be generated,
# that is valid and used for the runtime of this DCS instance.
#
# Note : The ID must contain letters in set [-_A-Za-z0-9] only.
#        A malformed ID exception is thrown otherwise at runtime.
# default = generated runtime UUID
com.openexchange.dcs.id=

# Define a network interface binding for THIS DCS
# Use 127.0.0.1 for private binding on local machine only.
# Use 0.0.0.0 for public binding on all interfaces.
# default = 127.0.0.1
com.openexchange.dcs.interface=
-
# Define the JMS Port for THIS DCS
# default = 61616
com.openexchange.dcs.port.jms=

************************************************************************************************************************
************************************************************************************************************************
