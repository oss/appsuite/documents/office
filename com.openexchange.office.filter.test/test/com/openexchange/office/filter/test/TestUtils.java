/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.json.JSONArray;
import org.mockito.Mockito;

import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;

final public class TestUtils {

	private static OsgiBundleContextAndActivator osgiBundleContextAndActivatorMock = Mockito.mock(OsgiBundleContextAndActivator.class);
	
    final static com.openexchange.office.filter.ooxml.docx.Importer docxImporter;
    final static com.openexchange.office.filter.ooxml.pptx.Importer pptxImporter;
    final static com.openexchange.office.filter.ooxml.xlsx.Importer xlsxImporter;
    final static com.openexchange.office.filter.odf.ods.dom.Importer odsImporter;
    final static com.openexchange.office.filter.odf.odp.dom.Importer odpImporter;
    final static com.openexchange.office.filter.odf.odt.dom.Importer odtImporter;

    final static com.openexchange.office.filter.ooxml.docx.Exporter docxExporter;
    final static com.openexchange.office.filter.ooxml.pptx.Exporter pptxExporter;
    final static com.openexchange.office.filter.ooxml.xlsx.Exporter xlsxExporter;
    final static com.openexchange.office.filter.odf.Exporter odfExporter;

    static {
    	docxImporter = new com.openexchange.office.filter.ooxml.docx.Importer();
    	docxImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
    	pptxImporter = new com.openexchange.office.filter.ooxml.pptx.Importer();
    	pptxImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
    	xlsxImporter = new com.openexchange.office.filter.ooxml.xlsx.Importer();
    	xlsxImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
    	odsImporter = new com.openexchange.office.filter.odf.ods.dom.Importer();
    	odsImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
    	odpImporter = new com.openexchange.office.filter.odf.odp.dom.Importer();
    	odpImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
    	odtImporter = new com.openexchange.office.filter.odf.odt.dom.Importer();
    	odtImporter.setApplicationContext(osgiBundleContextAndActivatorMock);

    	docxExporter = new com.openexchange.office.filter.ooxml.docx.Exporter();
    	docxExporter.setApplicationContext(osgiBundleContextAndActivatorMock);
    	pptxExporter = new com.openexchange.office.filter.ooxml.pptx.Exporter();
    	pptxExporter.setApplicationContext(osgiBundleContextAndActivatorMock);
    	xlsxExporter = new com.openexchange.office.filter.ooxml.xlsx.Exporter();
    	xlsxExporter.setApplicationContext(osgiBundleContextAndActivatorMock);
    	odfExporter = new com.openexchange.office.filter.odf.Exporter();
    	odfExporter.setApplicationContext(osgiBundleContextAndActivatorMock);
    }
    
    
    public static IImporter getImporterForExtension(String _extension) {
        final String extension = _extension.toLowerCase();

        if(extension.equals("docx")) {
            return docxImporter;
        }
        else if(extension.equals("pptx")) {
            return pptxImporter;
        }
        else if(extension.equals("xlsx")) {
            return xlsxImporter;
        }
        else if(extension.equals("odt")) {
            return odtImporter;
        }
        else if(extension.equals("odp")) {
            return odpImporter;
        }
        else if(extension.equals("ods")) {
            return odsImporter;
        }
        return null;
    }

    public static IExporter getExporterForExtension(String _extension) {
        final String extension = _extension.toLowerCase();

        if(extension.equals("docx")) {
            return docxExporter;
        }
        else if(extension.equals("pptx")) {
            return pptxExporter;
        }
        else if(extension.equals("xlsx")) {
            return xlsxExporter;
        }
        else if(extension.equals("odt")) {
            return odfExporter;
        }
        else if(extension.equals("odp")) {
            return odfExporter;
        }
        else if(extension.equals("ods")) {
            return odfExporter;
        }
        return null;
    }

    // returns an empty string if extension is unknown
    public static String getMediaFolderForExtension(String _extension) {
        final String extension = _extension.toLowerCase();
        if(extension.equals("docx")) {
            return "word/media/";
        }
        else if(extension.equals("pptx")) {
            return "ppt/media/";
        }
        else if(extension.equals("xlsx")) {
            return "xl/media/";
        }
        else if(extension.equals("odt") || extension.equals("odp") || extension.equals("ods")) {
            return "";
        }
        return "";
    }

    /*
     * returns a new ResourceManager that is filled with each media that could be found in zipInput/mediaFolder
     * the operations are changed to address resource entries instead of documents media entries
     */
    public static IResourceManager createResourceManagerFromMediaFolder(InputStream zipInput, String mediaFolder, JSONArray operations) {
        final IResourceManager resourceManager = new ResourceManagerMock();
        try(ZipInputStream zis = new ZipInputStream(zipInput)) {
            ZipEntry zipEntry;
            while ((zipEntry = zis.getNextEntry()) != null) {
                if (!zipEntry.isDirectory()) {
                    if(zipEntry.getName().startsWith(mediaFolder)) {
                        final ByteArrayOutputStream out = new ByteArrayOutputStream();
                        byte[] buffer = new byte[1024];
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            out.write(buffer, 0, len);
                        }
                        resourceManager.addResource(buffer);
                    }
                }
                zis.closeEntry();
            }
        } catch(IOException e) {
            // ohoh
        }
        return resourceManager;
    }
}
