/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.test;

import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.exception.OXException;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.imagemgr.Resource;
import com.openexchange.office.imagemgr.ResourceFactory;

public class ResourceManagerMock implements IResourceManager {

    private final Map<String, Resource> m_resources = new HashMap<>();
    private final String uuid = UUID.randomUUID().toString();
    private boolean disposed = false;

    private ResourceFactory resourceFactory = new ResourceFactory();
    
    /**
     * Initializes a new {@link ResourceManagerImpl}.
     *
     * @param services
     */
    public ResourceManagerMock() {
    	
    }

    void touch() {
        for (final Iterator<Resource> resourceIter = m_resources.values().iterator(); resourceIter.hasNext();) {
            resourceIter.next().touch();
        }
    }

    /**
     * @param lockResources
     */
    @Override
    public void lockResources(boolean lock) {
    }

    @Override
    public String addBase64(String base64Data) {
        String uid = null;

        final String basePattern = "base64,";
        int pos = base64Data.indexOf(basePattern);
        if ((pos != -1) && ((pos += basePattern.length()) < (base64Data.length() - 1))) {
            final byte[] byteData = Base64.getDecoder().decode(base64Data.substring(pos));
            if (byteData != null && byteData.length > 0) {
                uid = addResource(byteData);
            }
        }
        return uid;
    }

    @Override
    public String addResource(byte[] resourceData) {
        return addResource(resourceFactory.create(resourceData));
    }

    @Override
    public String addResource(String sha256, byte[] resourceData) {
        return addResource(resourceFactory.create(sha256, resourceData));
    }

    @Override
    public String addResource(Resource resource) {
        final String uid = (null != resource) ? resource.getUID() : null;

        if (StringUtils.isNotEmpty(uid)) {
            if (!m_resources.containsKey(uid)) {
                m_resources.put(uid, resource);
            }
        }

        return ((null != uid) ? uid : null);
    }

    @Override
    public boolean containsResource(String uid) {
        return m_resources.containsKey(uid);
    }

    @Override
    public Resource getResource(String uid) {
        return m_resources.get(uid);
    }

    @Override
    public byte[] getResourceBuffer(String uid) {
		final Resource resource = getResource(uid);

		if (resource!=null) {
			return resource.getBuffer();
		}
		return null;
	}

    @Override
    public Resource createManagedResource(String managedResourceId) {
        try {
			return resourceFactory.create(managedResourceId);
		} catch (OXException e) {
			throw new RuntimeException(e);
		}
    }

    @Override
    public Map<String, byte[]> getByteArrayTable() {
        Map<String, byte[]> ret = null;

        ret = new Hashtable<>(m_resources.size());

        for (final String curKey : m_resources.keySet()) {
            final Resource curResource = m_resources.get(curKey);

            if (null != curResource) {
                ret.put(curKey, curResource.getBuffer());
            }
        }

        return ret;
    }

    @Override
    public void removeResource(String uid) {
    	m_resources.remove(uid);
    }

	@Override
	public String getUUID() {
		return uuid;
	}

	@Override
	public void dispose() {
		disposed = true;
	}

	@Override
	public boolean isDisposed() {
		return disposed;
	}

}
