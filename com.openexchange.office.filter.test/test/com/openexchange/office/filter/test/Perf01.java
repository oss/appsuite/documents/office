/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.test;


import static org.junit.jupiter.api.Assertions.fail;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.util.Vector;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.docx4j.jaxb.Context;
import org.json.JSONArray;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.session.Session;

public class Perf01 {

    final static Logger log = Logger.getAnonymousLogger();

    @Disabled
    @Test
	public void test() {

        // waiting for JAXB contexts
        Context.getJc();

        final long current = System.currentTimeMillis();
        final String fileNames = "test/com/openexchange/office/filter/test/testDocs/testDocs.txt";
        final Vector<String> failedDocuments = new Vector<String>();
        final Vector<String> successfulDocuments = new Vector<String>();

        try (BufferedReader br = new BufferedReader(new FileReader(new File(fileNames)))) {
            String fileName;
            while ((fileName = br.readLine()) != null) {
                long cur = System.currentTimeMillis();
                try {
                    final InputStream in = perfTest( "test/com/openexchange/office/filter/test/testDocs/" + fileName);
                    if(in!=null) {
                        in.close();
                        successfulDocuments.add(fileName + "(ms: " + Long.toString(System.currentTimeMillis()-cur) + ")");
                    }
                }
                catch(Throwable e) {
                    failedDocuments.add(fileName);
                }
            }
        }
        catch(Exception e) {
            fail(e.getMessage());
        }

        final StringBuilder logBuffer = new StringBuilder();
        logBuffer.append("global test execution time:" + " ms: " + Long.toString(System.currentTimeMillis()-current) + "\n");

        if(!successfulDocuments.isEmpty()) {
            logBuffer.append("\nsuccessful documents:\n----------------------------------------\n");
            for(String doc:successfulDocuments) {
                logBuffer.append(doc + "\n");
            }
        }
        if(!failedDocuments.isEmpty()) {
            logBuffer.append("\nfailed documents:\n----------------------------------------\n");
            for(String doc:failedDocuments) {
                logBuffer.append(doc + "\n");
            }
        }

        log.info(logBuffer.toString());

        if(!failedDocuments.isEmpty()) {
            fail();
        }
    }

    final Session session = null;
    final DocumentProperties documentProperties = new DocumentProperties();

    private InputStream perfTest(String fileName) throws FilterException, Exception {

        // the configuration requires osgi usage, so we will turn it off. This prevents us from a lot error log messaging.
        documentProperties.put("useConfiguration", false);

        try (ByteArrayInputStream isDocx = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(fileName)))) {

            final int index = fileName.lastIndexOf('.');
            if(index < 0 || (index + 1 >= fileName.length())) {
                return null;
            }
            final String extension = fileName.substring(index + 1);
            final IImporter importer = TestUtils.getImporterForExtension(extension);
            final IExporter exporter = TestUtils.getExporterForExtension(extension);

            final InputStream emptyDefaultDocument = importer.getDefaultDocument(null, documentProperties);
            final JSONArray operations = importer.createOperations(null, isDocx, new DocumentProperties(), false).getJSONArray("operations");

            isDocx.reset();
            final IResourceManager resourceManager = TestUtils.createResourceManagerFromMediaFolder(isDocx, TestUtils.getMediaFolderForExtension(extension), operations);
            return exporter.createDocument(session, emptyDefaultDocument, operations.toString(), resourceManager, documentProperties, true);
        }
    }
}
