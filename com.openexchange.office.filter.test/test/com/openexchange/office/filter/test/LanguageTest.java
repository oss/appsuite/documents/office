/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.test;

import static org.junit.jupiter.api.Assertions.fail;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;

final public class LanguageTest {

    @Test
    public void test() throws IOException, FilterException, JSONException {

        final DocumentProperties documentProperties = new DocumentProperties();
        documentProperties.put("useConfiguration", false);

        try (ByteArrayInputStream isDocx = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("test/com/openexchange/office/filter/test/testDocs/lang-test.docx")))) {

            final IImporter importer = TestUtils.getImporterForExtension("docx");
            final JSONArray operations = importer.createOperations(null, isDocx, new DocumentProperties(), false).getJSONArray("operations");
            if(operations!=null) {
                for(int i=0; i<operations.length(); i++) {
                    final JSONObject op = operations.optJSONObject(i);
                    final String opName = op.optString(OCKey.NAME.value());
                    if(OCValue.SET_DOCUMENT_ATTRIBUTES.equals(OCValue.fromValue(opName))) {
                        final JSONObject attrs = op.optJSONObject(OCKey.ATTRS.value());
                        if(attrs!=null) {
                            final JSONObject document = attrs.optJSONObject(OCKey.DOCUMENT.value());
                            if(document!=null) {
                                final JSONArray usedLanguages = document.optJSONArray(OCKey.USED_LANGUAGES.value());
                                if(usedLanguages!=null) {
                                    if(contains(usedLanguages, "en-US") || contains(usedLanguages, "de-DE") && contains(usedLanguages, "fr-FR")) {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            fail("used languages could not be found in the operations array");
        }
    }

    private boolean contains(JSONArray array, String v) {
        for(int i=0; i<array.length(); i++) {
            String s = array.optString(i);
            if(v.equals(s)) {
                return true;
            }
        }
        return false;
    }
}
