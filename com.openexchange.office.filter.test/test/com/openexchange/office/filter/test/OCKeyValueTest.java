/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.office.filter.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.openexchange.office.filter.api.DocumentProperties;
import com.openexchange.office.filter.api.FilterException;
import com.openexchange.office.filter.api.IExporter;
import com.openexchange.office.filter.api.IImporter;
import com.openexchange.office.filter.api.OCKey;
import com.openexchange.office.filter.api.OCValue;
import com.openexchange.office.imagemgr.IResourceManager;
import com.openexchange.office.templatemgr.ResourceProvider;
import com.openexchange.office.tools.common.osgi.context.OsgiBundleContextAndActivator;

public class OCKeyValueTest {

    private ResourceProvider resourceProvider = new ResourceProvider();
    private DocumentProperties documentProperties = new DocumentProperties();

    private OsgiBundleContextAndActivator osgiBundleContextAndActivatorMock = Mockito.mock(OsgiBundleContextAndActivator.class);

    private com.openexchange.office.filter.ooxml.docx.Importer docxImporter;
    private com.openexchange.office.filter.ooxml.pptx.Importer pptxImporter;
    private com.openexchange.office.filter.ooxml.xlsx.Importer xlsxImporter;
    private com.openexchange.office.filter.ooxml.docx.Exporter docxExporter;
    private com.openexchange.office.filter.ooxml.pptx.Exporter pptxExporter;
    private com.openexchange.office.filter.ooxml.xlsx.Exporter xlsxExporter;
    private com.openexchange.office.filter.odf.ods.dom.Importer odsImporter;
    private com.openexchange.office.filter.odf.odp.dom.Importer odpImporter;
    private com.openexchange.office.filter.odf.odt.dom.Importer odtImporter;

    private com.openexchange.office.filter.odf.Exporter odfExporter;

    @BeforeEach
    public void init() {
        docxImporter = new com.openexchange.office.filter.ooxml.docx.Importer();
        docxImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
        pptxImporter = new com.openexchange.office.filter.ooxml.pptx.Importer();
        pptxImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
        xlsxImporter = new com.openexchange.office.filter.ooxml.xlsx.Importer();
        xlsxImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
        docxExporter = new com.openexchange.office.filter.ooxml.docx.Exporter();
        docxExporter.setApplicationContext(osgiBundleContextAndActivatorMock);
        pptxExporter = new com.openexchange.office.filter.ooxml.pptx.Exporter();
        pptxExporter.setApplicationContext(osgiBundleContextAndActivatorMock);
        xlsxExporter = new com.openexchange.office.filter.ooxml.xlsx.Exporter();
        xlsxExporter.setApplicationContext(osgiBundleContextAndActivatorMock);
        odsImporter = new com.openexchange.office.filter.odf.ods.dom.Importer();
        odsImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
        odpImporter = new com.openexchange.office.filter.odf.odp.dom.Importer();
        odpImporter.setApplicationContext(osgiBundleContextAndActivatorMock);
        odtImporter = new com.openexchange.office.filter.odf.odt.dom.Importer();
        odtImporter.setApplicationContext(osgiBundleContextAndActivatorMock);

        odfExporter = new com.openexchange.office.filter.odf.Exporter();
        odfExporter.setApplicationContext(osgiBundleContextAndActivatorMock);

    }

    @Test
	public void test() {
        try {
            for(OCKey s : OCKey.values()) {
                for(OCKey d : OCKey.values()) {
                    if(s!=d) {
                        assertFalse(s.value().equals(d.value()), "OCKey." + s.name() + " collides with " + "OCKey." + d.name() + " (" + s.value() + ")");
                        assertFalse(s.value().equals(d.value(true)), "OCKey." + s.name() + " collides with " + "OCKey." + d.name() + " (" + s.value() + ")");
                        assertFalse(s.value(true).equals(d.value()), "OCKey." + s.name() + " collides with " + "OCKey." + d.name() + " (" + s.value(true) + ")");
                        assertFalse(s.value(true).equals(d.value(true)), "OCKey." + s.name() + " collides with " + "OCKey." + d.name() + " (" + s.value(true) + ")");
                    }
                }
            }
            for(OCValue s : OCValue.values()) {
                for(OCValue d : OCValue.values()) {
                    if(s!=d) {
                        assertFalse(s.value().equals(d.value()), "OCValue." + s.name() + " collides with " + "OCValue." + d.name() + " (" + s.value() + ")");
                        assertFalse(s.value().equals(d.value(true)), "OCValue." + s.name() + " collides with " + "OCValue." + d.name() + " (" + s.value() + ")");
                        assertFalse(s.value(true).equals(d.value()), "OCValue." + s.name() + " collides with " + "OCValue." + d.name() + " (" + s.value(true) + ")");
                        assertFalse(s.value(true).equals(d.value(true)), "OCValue." + s.name() + " collides with " + "OCValue." + d.name() + " (" + s.value(true) + ")");
                    }
                }
            }
            final Pair<Integer, Integer> opSizeDOCX = testDOCX();
            final Pair<Integer, Integer> opSizeODT = testODT();
            System.out.println("DOCX operation size has been reduced from: " + opSizeDOCX.getLeft().toString() + " to " + opSizeDOCX.getRight().toString());
            System.out.println("ODT operation size has been reduced from: " + opSizeODT.getLeft().toString() + " to " + opSizeODT.getRight().toString());
        }
        catch(Exception e) {
            e.printStackTrace();
        	fail(e.getMessage());
        }
	}

    private Pair<Integer, Integer> testDOCX() throws FilterException, IOException, JSONException, Exception {
        final Pair<Integer, byte[]> shortDoc = checkDocument(true, docxImporter, docxExporter, "test/com/openexchange/office/filter/test/testDocs/all_ox_text_features.docx", "text");
        final Pair<Integer, byte[]> longDoc = checkDocument(false, docxImporter, docxExporter, "test/com/openexchange/office/filter/test/testDocs/all_ox_text_features.docx", "text");
        compareZip(shortDoc.getRight(), longDoc.getRight());
        return Pair.of(longDoc.getLeft(), shortDoc.getLeft());
    }

    private Pair<Integer, Integer> testODT() throws FilterException, IOException, JSONException, Exception {
        com.openexchange.office.filter.odf.styles.StyleManager.setRandom(1);
        com.openexchange.office.filter.odf.MetaData.updateDate = false;
        final Pair<Integer, byte[]> shortDoc = checkDocument(true, odtImporter, odfExporter, "test/com/openexchange/office/filter/test/testDocs/all_ox_text_features.odt", "text");
        com.openexchange.office.filter.odf.styles.StyleManager.setRandom(1);
        final Pair<Integer, byte[]> longDoc = checkDocument(false, odtImporter, odfExporter, "test/com/openexchange/office/filter/test/testDocs/all_ox_text_features.odt", "text");
        compareZip(shortDoc.getRight(), longDoc.getRight());
        return Pair.of(longDoc.getLeft(), shortDoc.getLeft());
    }

    private Pair<Integer, byte[]> checkDocument(boolean useShortOperations, IImporter importer, IExporter exporter, String testDocument, String documentType) throws FilterException, IOException, JSONException, Exception {

        final IResourceManager resourceManager = new ResourceManagerMock();

        OCKey.debugOnly(useShortOperations);
        OCValue.debugOnly(useShortOperations);
        final String ops = importer.createOperations(null, new ByteArrayInputStream(Files.readAllBytes(new File(testDocument).toPath())), documentProperties, false).getJSONArray("operations").toString();
        final InputStream templateDocument = getTemplateDocument(documentType, importer instanceof com.openexchange.office.filter.odf.Importer);
        final byte[] docArray = IOUtils.toByteArray(exporter.createDocument(null, templateDocument, ops, resourceManager, documentProperties, true));
        return Pair.of(ops.length(), docArray);
    }

    private void compareZip(byte[] z1, byte[] z2) throws IOException {
        try {
            final ByteArrayInputStream i1 = new ByteArrayInputStream(z1);
            final ByteArrayInputStream i2 = new ByteArrayInputStream(z2);
            final ZipInputStream zip1 = new ZipInputStream(i1);
            final ZipInputStream zip2 = new ZipInputStream(i2);

            ZipEntry zip1Entry;
            while((zip1Entry = zip1.getNextEntry())!=null) {
                zip2.getNextEntry();
                if(!zip1Entry.isDirectory()) {

                    final ByteArrayOutputStream o1 = new ByteArrayOutputStream();
                    final ByteArrayOutputStream o2 = new ByteArrayOutputStream();

                    int size;
                    byte[] buffer = new byte[2048];
                    while ((size = zip1.read(buffer, 0, buffer.length)) != -1) {
                        o1.write(buffer, 0, size);
                    }
                    while ((size = zip2.read(buffer, 0, buffer.length)) != -1) {
                        o2.write(buffer, 0, size);
                    }
                    assertTrue(o1.size()==o2.size(), zip1Entry.getName() + ": long/short key/value roundtrip, document length differs");
                }
                zip1.closeEntry();
                zip2.closeEntry();
            }
            zip1.close();
            zip2.close();
        }
        catch(AssertionError e) {
            writeBuffer(z1, "/tmp/short.xxx");
            writeBuffer(z2, "/tmp/long.xxx");
            throw e;
        }
    }

    private void writeBuffer(byte[] in, String fileName) throws IOException {
        final FileOutputStream out = new FileOutputStream(new File(fileName));
        out.write(in);
        out.flush();
        out.close();
    }

    private InputStream getTemplateDocument(String documentType, boolean useOdf) throws IOException {
        if(useOdf) {
            final String odfTemplatePath;
            if(documentType.equals("text")) {
                odfTemplatePath = "test/com/openexchange/office/filter/test/testDocs/empty.odt";
            }
            else if(documentType.equals("presentation")) {
                odfTemplatePath = "test/com/openexchange/office/filter/test/testDocs/empty.odp";
            }
            else {
                odfTemplatePath = "test/com/openexchange/office/filter/test/testDocs/empty.ods";
            }
            return new ByteArrayInputStream(Files.readAllBytes(new File(odfTemplatePath).toPath()));
        }
        return resourceProvider.getResource(resourceProvider.getEntry(documentType, "template", "Default"));
    }
}
