/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author sven.jacobi@open-xchange.com
 */

package com.openexchange.office.odf.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.json.JSONArray;
import org.junit.Test;
import org.odftoolkit.odfdom.component.OdfOperationDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import com.openexchange.office.odf.Border;
import com.openexchange.office.odf.properties.TableCellProperties;
import com.openexchange.office.odf.styles.StyleBase;
import com.openexchange.office.odf.styles.StyleManager;
import com.openexchange.office.odf.styles.StyleTableCell;
import com.openexchange.office.odt.dom.Cell;
import com.openexchange.office.odt.dom.Content;
import com.openexchange.office.odt.dom.JsonOperationConsumer;
import com.openexchange.office.odt.dom.components.CellComponent;
import com.openexchange.office.odt.dom.components.Component;

public class ODTTableBorderTest {

    @Test
    public void testTable(){
        try {

            // exemplary testing the left border of the first cell after inserting a table at 0 with two rows and two cells.
            // A top, left, right and bottom border is applied with a width of 212 (100thmm)

            // loading test document and applying operations
            final OdfOperationDocument operationDocument = TestUtils.loadTextDocument("src/com/openexchange/office/odf/test/testDocs/TableTest.odt", true);
            new JsonOperationConsumer(operationDocument).applyOperations(new JSONArray(
                "[{\"name\":\"insertTable\",\"start\":[0],\"attrs\":{\"table\":{\"tableGrid\":[1000,1000],\"width\":\"auto\",\"exclude\":[\"lastRow\",\"lastCol\",\"bandsVert\"]},\"styleId\":\"_default\"}},{\"name\":\"insertRows\",\"start\":[0,0],\"count\":2,\"insertDefaultCells\":true},{\"name\":\"setAttributes\",\"start\":[0,0,0],\"attrs\":{\"cell\":{\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}}}}},{\"name\":\"setAttributes\",\"start\":[0,0,1],\"attrs\":{\"cell\":{\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}}}}},{\"name\":\"setAttributes\",\"start\":[0,1,0],\"attrs\":{\"cell\":{\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}}}}},{\"name\":\"setAttributes\",\"start\":[0,1,1],\"attrs\":{\"cell\":{\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}}}}},{\"name\":\"setAttributes\",\"start\":[0],\"attrs\":{\"table\":{\"borderLeft\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderInsideVert\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderTop\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderInsideHor\":{\"style\":\"single\",\"width\":17,\"space\":140,\"color\":{\"type\":\"auto\"}}}}},{\"name\":\"setAttributes\",\"start\":[0,0,0],\"attrs\":{\"cell\":{\"borderTop\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}}}}},{\"name\":\"setAttributes\",\"start\":[0,0,1],\"attrs\":{\"cell\":{\"borderTop\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}}}}},{\"name\":\"setAttributes\",\"start\":[0,1,0],\"attrs\":{\"cell\":{\"borderTop\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}}}}},{\"name\":\"setAttributes\",\"start\":[0,1,1],\"attrs\":{\"cell\":{\"borderTop\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderBottom\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderLeft\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}},\"borderRight\":{\"style\":\"single\",\"width\":212,\"space\":140,\"color\":{\"type\":\"auto\"}}}}}]"));

            // saving & reloading the document ...
            final OdfOperationDocument destDocument = TestUtils.saveLoad(operationDocument);

            // accessing the top left cell and checking its left border width
            final Content content = (Content)((OdfTextDocument)destDocument.getDocument()).getContentDom();
            final JSONArray start = new JSONArray(3);
            start.put(0);
            start.put(0);
            start.put(0);
            final CellComponent cell = (CellComponent)Component.getComponent(content.getRootComponent(null), start, start.length());
            final StyleManager styleManager = destDocument.getPackage().getStyleManager();
            final StyleTableCell tableCellStyle = (StyleTableCell)styleManager.getStyle(((Cell)cell.getObject()).getStyleName(), "table-cell", true);
            final TableCellProperties cellProperties = tableCellStyle.getTableCellProperties();
            final Border leftBorder = new Border();
            leftBorder.applyFoBorder(cellProperties.getAttribute("fo:border-left"));
            assertTrue("border width does not match", Integer.valueOf(212).equals(leftBorder.getWidth()));
        }
        catch (Throwable e) {
            fail("odtTableTest failed:" + e.getMessage());
        }
    }
}
